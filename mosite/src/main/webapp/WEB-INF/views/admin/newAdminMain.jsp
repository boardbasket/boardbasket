<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.nayuta.mosite.common.util.FileUtil.FILE_TYPE"%>

<%
	pageContext.setAttribute("fileType", FILE_TYPE.values());
%>

<!DOCTYPE html>
<html>
<head>
<title>MOSITE -관리자 페이지-</title>

<!-- <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0, user-scalable=false"> -->
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!--bootstrap css include  -->
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">
<!-- bootstrap / popper.js / jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>

<link rel="stylesheet" href="${root}/resources/css/admin/admin.css">

<script src="${root}/resources/js/bootstrap/popper.min.js"></script>

<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>

<link rel="stylesheet" href="${root}/resources/css/jqueryUI/jquery-ui.min.css">
<script src="${root}/resources/js/jqueryUI/jquery-ui.min.js"></script>
<script src="${root}/resources/js/jqueryUI/jquery.ui.touch-punch.min.js"></script>
<script src="${root}/resources/js/common/util.js"></script>
<!-- font awsome include -->
<link href="${root }/resources/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet">

<link href="${root }/resources/css/common/common.css" rel="stylesheet">

<meta name="csrf_token" content="${_csrf.token }">
<meta name="csrf_headerName" content="${_csrf.headerName}">


</head>
<body>

	<div>
		<button class="btn btn-primary" onclick="testfunc();">word2vectest</button>
	</div>

	<div class="login">
		<sec:authorize access="isAnonymous()">
			<!-- before logined  -->
			<div class="login__btn">
				<c:choose>
					<c:when test="${sessionScope.isMobile eq true}">
						<button class="btn btn-secondary btn-lg" onclick="location.href='${root }/user/login.do'">
							<i class="fas fa-sign-in-alt"></i>
						</button>
					</c:when>
					<c:otherwise>
						<a href="${root }/user/login.do" class="btn btn-secondary div-loginBtn_a-login">로그인</a>
					</c:otherwise>
				</c:choose>
			</div>
		</sec:authorize>


		<sec:authorize access="isAuthenticated()">
			<!--after login  -->
			<button class="btn btn-secondary btn-lg" type="button" id="afterLoginMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-boundary="window">
				<i class="fas fa-bars fa-lg"></i>
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item dropdown__showNoticeBoard cursor-pointer" data-board-code="${result.noticeBoard.board_code}">공지사항</a>


				<button id="tagPanInfo" class="dropdown-item">TagPAN?</button>
				<a class="dropdown-item" href="${root}/">메인 페이지</a>
				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<a class="dropdown-item" href="${root}/admin/adminMain.do">관리자 페이지</a>
				</sec:authorize>
				<div class="dropdown-divider"></div>
				<form action="${root}/logOut.do" method="post">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }" />
					<input id="btn-logOut" type="submit" class="dropdown-item" value="로그아웃" />
				</form>
			</div>

		</sec:authorize>
	</div>

</body>

<script>
	function testfunc() {

		let word = prompt();
		if (word.length <= 0)
			return;

		$.getJSON('/word2Vec.ajax', {
			targetStr : word
		}).done(function(data) {
			console.log("성공");
		})

	}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.nayuta.mosite.common.util.FileUtil.FILE_TYPE"%>

<%
	pageContext.setAttribute("fileType", FILE_TYPE.values());
%>

<!DOCTYPE html>
<html>
<head>
<title>MOSITE -관리자 페이지-</title>

<!-- <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0, user-scalable=false"> -->
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!--bootstrap css include  -->
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">
<!-- bootstrap / popper.js / jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>

<link rel="stylesheet" href="${root}/resources/css/admin/admin.css">

<script src="${root}/resources/js/bootstrap/popper.min.js"></script>

<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>

<link rel="stylesheet" href="${root}/resources/css/jqueryUI/jquery-ui.min.css">
<script src="${root}/resources/js/jqueryUI/jquery-ui.min.js"></script>
<script src="${root}/resources/js/jqueryUI/jquery.ui.touch-punch.min.js"></script>
<script src="${root}/resources/js/common/util.js"></script>
<!-- font awsome include -->
<link href="${root }/resources/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet">

<link href="${root }/resources/css/common/common.css" rel="stylesheet">

<meta name="csrf_token" content="${_csrf.token }">
<meta name="csrf_headerName" content="${_csrf.headerName}">

<style>
.tr-selected {
	background: #6c757d;
}

.div-boardTableWrapper {
	overflow-x: hidden;
	overflow-y: auto;
	height: 80vh;
	position: relative;
}

table[id*=table-] {
	table-layout: fixed;
}

#table-siteList .th>.i[class*=sort] {
	cursor: pointer;
}

.th>span:hover {
	cursor: pointer;
}

#div-tableMenuWrapper {
	position: fixed;
	top: 70%;
	left: 85%;
	opacity: 0.6;
	display: none;
}

#div-tableMenuWrapper:hover {
	opacity: 1;
}

.input-group .inputOverlay {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	max-width: 33%;
	cursor: pointer;
	display: none;
}

.input-group .div-overlayWrapper {
	position: absolute;
	top: 50%;
	transform: translateY(-50%);
	width: 50%;
	left: 45%;
}

.inputOverlay {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	cursor: pointer;
	max-width: 15%;
}

#modal-addSite_button-deleteFile {
	display: none;
	position: absolute;
	right: 0;
}

#div-tableSearchBar .span-keywordOverlay {
	left: 85%;
}

#div-tableSearchBar .span-orderbyOverlay {
	left: 70%;
}

#div-tableSearchBar {
	display: none;
}

i[data-orderby*=ASC],i[data-orderby*=DESC] {
	display: none;
}

i[data-orderby] {
	cursor: pointer;
}

textarea {
	resize: none;
	width: 100%;
}
</style>


</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<nav class="col-md-2 d-none d-md-block bg-light sidebar">
				<div class="sidebar-sticky">
					<ul class="nav flex-column">
						<li class="nav-item invisible pt-3 pb-2"><h1>side</h1></li>
						<li id="li-showAdminMainPage" class="nav-item"><a class="nav-link active" href="#"> 게시판,사이트 </a></li>

						<c:forEach var="item" items="${result.boardList}">

							<li class="dbBoardListItem nav-item" data-board-id="${item.board_code}"><a class="nav-link" href="#">${item.board_name }</a></li>

						</c:forEach>

						<!-- <li id="li-showNoticeBoard" class="nav-item"><a class="nav-link" href="#"> 공지사항</a></li> -->

					</ul>

					<!-- 	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
						<span>준비중...</span> <a class="d-flex align-items-center text-muted" href="#"> <span data-feather="plus-circle"></span>
						</a>
					</h6>
					<ul class="nav flex-column mb-2">
						<li class="nav-item"><a class="nav-link" href="#"> <span data-feather="file-text"></span> 준비중...
						</a></li>

					</ul> -->
				</div>
			</nav>

			<div role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
					<h1 class="h2">사이트/게시판 설정</h1>
					<div class="btn-toolbar mb-2 mb-md-0">
						<div class="btn-group mr-2">
							<button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
							<button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
						</div>

						<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="btn-dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">메뉴</button>
							<div class="dropdown-menu  dropdown-menu-right" style="min-width: 0px" title="메뉴">
								<a class="dropdown-item" style="cursor: pointer" href="${root }/"><span>메인으로</span><i class="fas fa-home"></i></a> <a class="dropdown-item" style="cursor: pointer" href="/logOut.do"><span>로그아웃</span><i
									class="fas fa-sign-out-alt"
								></i></a>
							</div>
						</div>

					</div>
				</div>
				<div id="div-tableSearchBar">
					<h2>검색</h2>
					<div class="input-group mb-3">
						<input id="input-searchTable" type="text" class="form-control" placeholder="현재 테이블 검색" aria-label="searchTable">

						<div class="div-overlayWrapper">
							<span class="badge badge-danger span-siteCodeOverlay inputOverlay"></span> <span class="badge badge-danger span-keywordOverlay inputOverlay"> </span> <span
								class="badge badge-danger span-orderbyOverlay inputOverlay"
							></span>

						</div>
						<div class="input-group-append">
							<button class="btn btn-outline-secondary btn-searchTable" type="button">검색</button>
						</div>

					</div>
				</div>

				<div id="div-tablePage">



					<div class="accordion" id="dataTables">
						<div class="card">
							<div class="card-header" id="siteTableHeader">
								<h2 class="mb-0">
									<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
										<h4>사이트 목록</h4>
									</button>
								</h2>
							</div>

							<div id="collapseOne" class="collapse" aria-labelledby="boardTableHeader" data-parent="#dataTables">
								<div class="div-boardTableWrapper">
									<table id="table-siteList" class="table table-sm" data-table-loaded="false" data-request-url='/admin/selectSite.ajax' data-search-option='null'>
									</table>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="boardTableHeader">
								<h2 class="mb-0">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										<h4>게시판 목록</h4>
									</button>
								</h2>
							</div>
							<div id="collapseTwo" class="collapse" aria-labelledby="boardTableHeader" data-parent="#dataTables">

								<div class="div-boardTableWrapper">
									<table id="table-boardList" class="table table-sm" data-table-loaded="false" data-request-url='/admin/selectBoard.ajax' data-search-option='null'>

									</table>
								</div>


							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree">
								<h2 class="mb-0">
									<button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">나중에 추가</button>
								</h2>
							</div>
							<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#dataTables"></div>
						</div>
					</div>
				</div>

				<div id="div-dbBoardPage">
					<c:forEach var="item" items="${result.boardList}">
						<table data-table-id="${item.board_code}" data-board-name="${item.board_name}" class="table table-sm" data-table-loaded="false" data-request-url='/admin/selectDbBoardArticle.ajax'
							data-search-option='{"offset":1,"board_code":${item.board_code}}' style="display: none;"
						>
						</table>
					</c:forEach>
				</div>
				<!-- <div id="div-noticeBoard" style="display: none;">
					<table id="table-notice" class="table table-sm" data-table-loaded="false" data-request-url='/admin/selectDbBoardArticle.ajax'
						data-search-option='{"offset":1,"board_code":6}'>
					</table>

				</div> -->

			</div>

		</div>
		<div class="" id="div-tableMenuWrapper">
			<div class="btn-group-vertical" role="group" aria-label="tableMenu" data-table-for="table-boardList">
				<button type="button" class="btn btn-secondary" disabled>게시판 메뉴</button>
				<button type="button" class="btn btn-secondary btn-resetOption">검색옵션 리셋</button>
				<button type="button" class="btn btn-secondary btn-addBoard">게시판 추가</button>
				<button type="button" class="btn btn-secondary btn-updateBoard">게시판 업데이트</button>
				<button type="button" class="btn btn-secondary btn-deleteBoard">게시판 삭제</button>
			</div>
			<div class="btn-group-vertical" role="group" aria-label="tableMenu" data-table-for="table-siteList">
				<button type="button" class="btn btn-secondary" disabled>사이트 메뉴</button>
				<button type="button" class="btn btn-secondary btn-resetOption">검색옵션 리셋</button>
				<button type="button" class="btn btn-secondary btn-addSite">사이트 추가</button>
				<button type="button" class="btn btn-secondary btn-updateSite">사이트 업데이트</button>
				<button type="button" class="btn btn-secondary btn-deleteSite">사이트 삭제</button>
			</div>

			<c:forEach var="item" items="${result.boardList }">
				<div class="btn-group-vertical" role="group" aria-label="tableMenu" data-table-for="${item.board_code}">
					<button type="button" class="btn btn-secondary" disabled>${item.board_name}메뉴</button>
					<button type="button" class="btn btn-secondary btn-resetOption">검색옵션 리셋</button>
					<button type="button" class="btn btn-secondary btn-addDbArticle">${item.board_name}추가</button>
				</div>
			</c:forEach>

		</div>

	</div>

	<!-------------------------------------------------  Modals  ---------------------------------------------------->

	<!--add board -->
	<div class="modal fade" id="modal-addBoard" data-insert-data="" tabindex="-1" role="dialog" aria-labelledby="addBoard" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">게시판 추가</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="form-insertBoard">
						<div class="form-group">
							<label for="siteCodeList ">사이트</label> <select class="form-control" name="site_code" id="select-addBoard-siteCodeList"></select>
						</div>
						<div class="form-group">
							<label for="input-boardName ">게시판 이름</label>
							<input id="input-addBoard-boardName" type="text" class="form-control" name="board_name" placeholder="게시판 이름">
						</div>
						<div class="form-group">
							<label for="input-boardAddress ">게시판 주소</label>
							<input id="input-addBoard-boardAddress" type="text" class="form-control" name="board_address" placeholder="게시판 주소">
						</div>
						<div class="form-group">
							<label for="input-addBoard-boardType">게시판 타입</label> <select class="form-control" name="board_type" id="input-addBoard-boardType">
								<option value="PARSER_TYPE_DEFAULT">DEFAULT</option>
								<option value="PARSER_TYPE_1">TYPE1</option>
								<option value="PARSER_TYPE_2">TYPE2</option>
								<option value="PARSER_TYPE_3">TYPE3</option>
								<option value="PARSER_TYPE_4">TYPE4</option>
								<option value="PARSER_TYPE_5">TYPE5</option>
							</select>
						</div>
						<div class="form-group">
							<label for="input-addBoard-available">사용가능</label> <select class="form-control" name="available" id="input-addBoard-available">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-addBoard_btn-addBoard" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>

	<!--update board -->
	<div class="modal fade" id="modal-updateBoard" data-insert-data="" tabindex="-1" role="dialog" aria-labelledby="updateBoard" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">게시판 수정</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="form-updateBoard">
						<div class="div-updateBoard-singleTarget">
							<div class="form-group">
								<label for="siteCodeList ">사이트</label> <select class="form-control" name="site_code" id="select-updateBoard-siteCodeList" disabled></select>
							</div>
							<div class="form-group">
								<label for="input-boardName ">게시판 이름</label>
								<input id="input-updateBoard-boardName" type="text" class="form-control" name="board_name" placeholder="게시판 이름">
							</div>
							<div class="form-group">
								<label for="input-boardAddress ">게시판 주소</label>
								<input id="input-updateBoard-boardAddress" type="text" class="form-control" name="board_address" placeholder="게시판 주소">
							</div>
							<div class="form-group">
								<label for="input-updateBoard-boardType">게시판 타입</label> <select class="form-control" name="board_type" id="input-updateBoard-boardType">

								</select>
							</div>

						</div>
						<div class="form-group">
							<label for="input-updateBoard-available">사용가능</label> <select class="form-control" name="available" id="input-updateBoard-available">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-updateBoard_btn-updateBoardAjax" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>

	<!--add board -->
	<div class="modal fade" id="modal-deleteBoard" data-insert-data="" tabindex="-1" role="dialog" aria-labelledby="deleteBoard" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">게시판 수정</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<div class="modal-deleteBoard_div-deleteMessage"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-deleteBoard_btn-deleteBoardAjax" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>

	<!--add site -->
	<div class="modal fade" id="modal-addSite" tabindex="-1" role="dialog" aria-labelledby="addSite" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">게시판 수정</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="form-insertSite" enctype="multipart/form-data">
						<div class="form-group">
							<label for="input-siteName ">사이트 이름</label>
							<input id="modal-addSite_input-siteName" type="text" class="form-control" name="site_name" placeholder="사이트 이름" required>
						</div>
						<div class="form-group">
							<label for="input-siteAddress ">사이트 주소</label>
							<input id="modal-addSite_input-siteAddress" type="text" class="form-control" name="site_address" placeholder="사이트 주소" required>
						</div>

						<div class="form-row">
							<div class="col-8">
								<div class="input-group">
									<label for="modal-addSite_input-siteIcon">파일</label>
									<input name="site_icon" type="file" class="form-control-file" id="modal-addSite_input-siteIcon">

									<span class="input-group-append">
										<button id="modal-addSite_button-deleteFile" class="btn btn-danger" type="button">
											<span aria-hidden="true">&times;</span>
										</button>
									</span>
								</div>
							</div>
							<div class="col-4">
								<label for="modal-addSite_select-fileType">파일 타입</label> <select class="form-control" name="fileType" id="modal-addSite_select-fileType">
									<c:forEach var="entry" items="${fileType}">
										<option value="${entry }">${entry}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="div-searchTypeWrapper">
							<div class="mb-2">검색 타입</div>
							<div class="form-row">
								<div class="col">
									<input name="type_name" type="text" class="form-control" placeholder="검색 타입 이름" required>
								</div>
								<div class="col">
									<input name="type_val" type="text" class="form-control" placeholder="검색 타입 값" required>
								</div>
							</div>
						</div>
						<div class="form-row justify-content-end">
							<div class="col-4">
								<button type="button" class="btn btn-primary btn-addSearchType">추가</button>
								<button type="button" class="btn btn-secondary btn-deleteSearchType">삭제</button>
							</div>
						</div>

						<div class="form-group">
							<label for="modal-addSite_input-available">사용가능</label> <select class="form-control" name="available" id="input-addSite-available">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</form>
				</div>
				<div class="form-row d-none mt-1">
					<div class="col">
						<input name="type_name" type="text" class="form-control" placeholder="검색 타입 이름">
					</div>
					<div class="col">
						<input name="type_val" type="text" class="form-control" placeholder="검색 타입 값">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-addSite_btn-addSiteAjax" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>

	<!--update site -->
	<div class="modal fade" id="modal-updateSite" data-insert-data="" tabindex="-1" role="dialog" aria-labelledby="updateSite" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">사이트 업데이트</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">
					<form id="form-updateSite">

						<div class="form-group">
							<label for="input-updateSite-available">사용가능</label> <select class="form-control" name="available" id="input-updateSite-available">
								<option value="Y">Y</option>
								<option value="N">N</option>
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-updateSite_btn-updateSiteAjax" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>

	<!--insert db Article -->
	<div class="modal fade" id="modal-insertDbArticle" data-insert-data="" tabindex="-1" role="dialog" aria-labelledby="insertDbArticle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">글 작성</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body ">

					<form id="form-insertDbArticle">
						<div>
							<label for="input-dbArticle-articleNo">글번호</label>
							<input class="form-control" name="article_no" id="input-dbArticle-articleNo" disabled />
						</div>

						<div>
							<label for="dbBoardCodeList">등록 게시판</label> <select class="form-control" name="board_code" id="select-dbArticle-dbBoardCodeList" data-init="false"></select>
						</div>
						<div class="form-group">
							<label for="input-insertDbArticleTitle ">글 제목</label>
							<input id="input-insertDbArticleTitle" type="text" class="form-control" name="title" placeholder="제목">
						</div>
						<div class="form-group">
							<label for="content">내용</label> <br>
							<textarea name="content" id="" cols="50" rows="5" draggable="false"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
					<button id="modal-insertDbArticle_btn-insertDbArticleAjax" type="button" class="btn btn-primary ">확인</button>
				</div>
			</div>
		</div>
	</div>



	<%@include file="/WEB-INF/views/common/spinnerNToast.jsp"%>

</body>

<script>
	const $searchBar = $("#div-tableSearchBar");//검색바
	const $menuWrapper = $("#div-tableMenuWrapper");
	const $addBoardModal = $("#modal-addBoard");
	const $updateBoardModal = $("#modal-updateBoard");
	const $deleteBoardModal = $("#modal-deleteBoard");
	const $addSiteModal = $("#modal-addSite");
	const $updateSiteModal = $("#modal-updateSite");

	let $targetTable;//현재 열린 collpase의 테이블
	let fetchOption = {
		method : "POST",
		body : {
			"site_code" : "test"
		},
		headers : {
			"Content-Type" : "application/json",
			"${_csrf.headerName }" : "${_csrf.token }",
			"AJAX" : true
		}
	}

	/*
		tableOption
		
		multiselect : 여러개 선택 가능 				(default: false)
		showNum :row 맨 앞에 번호 column 추가			(default: true)
		listOrder : column의 순서(없는것도 추가 가능)	(default: 데이터 순)
		width: column 각각의 넓이					(default :퍼센트 값)
		shortRow : sorting이 가능한 row 			(default :없음)
		content:받은 데이터 외 추가한 column의 content	(default:없음)
		selectable: 선택(클릭) 했을때 하이라이트 되지 않도록	(default:true)
	 */

	const boardTableOption = {
		multiSelect : true,
		showNum : true,
		listOrder : [ "site_code", "board_code", "board_name", "board_address",
				"board_type", "available" ],
		width : [ "5", "10", "10", "20", "20", "15", "10" ],//numRow width까지 입력
		sortRow : [ "site_code", "board_name", "board_code", "available" ],
		content : {
			"site_code" : function(table, siteData) {

				return $(
						"<span class='badge badge-primary cursor-pointer' data-site-code='"+siteData.site_code+"'>"
								+ siteData.site_name + "</span>")
						.on(
								"click",
								function(e) {
									//td 클릭하면
									e.stopPropagation();
									const searchOption = table
											.data("searchOption")
											|| {};
									const siteCode = $(this).data("siteCode");
									const $siteCodeOverlay = $searchBar
											.find(".span-siteCodeOverlay");
									searchOption.site_code = siteCode;
									table.data("searchOption", searchOption);
									$siteCodeOverlay
											.toggle()
											.html(
													siteData.site_name
															+ '<span class="ml-1 text-dark">X</span>');
									ajaxLoadTable(table, drawTable);
								});
			}
		}
	}

	const siteTableOption = {
		showNum : true,
		listOrder : [ "site_code", "site_name", "site_address", "available" ],
		width : [ "10", "10", "20", "40", "20" ],
		sortRow : [ "site_code", "site_name", "available" ],
		multiSelect : true
	}

	const dbBoardOptions = {
		"공지사항" : {
			showNum : false,
			multiSelect : false,
			selectable : false,
			listOrder : [ "article_no", "title", "content", "view_cnt",
					"upload_date", "edit", "delete" ],
			width : [ "15", "20", "25", "15", "15", "5", "5" ],
			content : {
				/* "read" : function(table, data) {
					return $('<button class="btn btn-secondary"><i class="far fa-eye"></i></button>').on("click", function() {
						const $tr = $(this).parents("tr");

					})
				}, */
				"edit" : function(table, data) {
					return $(
							'<button class="btn btn-secondary"><i class="fas fa-edit"></i></button>')
							.on(
									"click",
									function() {
										const data = getTableRowInputData($(
												this).parents("tr"));
										setInputDataToModal(
												$("#modal-insertDbArticle"),
												data);
										$("#modal-insertDbArticle").modal(
												"show");
										$("#modal-insertDbArticle").find(
												"select")
												.attr("disabled", true);
									})

				},
				"delete" : function(table, data) {
					return $(
							'<button class="btn btn-danger"><i class="fas fa-times"></i></button>')
							.on(
									"click",
									function() {

										if (confirm("이 글을 삭제하시겠습니까?")) {
											const $tr = $(this).parents("tr");
											const inputData = getTableRowInputData($tr);
											$
													.post(
															"/admin/deleteDbBoardArticle.ajax",
															inputData)
													.done(
															function() {
																$(
																		"li[data-board-id="
																				+ $tr
																						.parents(
																								"table")
																						.data(
																								"tableId")
																				+ "]")
																		.trigger(
																				"click");
																$(
																		"#modal-insertDbArticle")
																		.modal(
																				"hide");
																alert("삭제 완료");

															})
										}

									})
				}
			},
			offset : 1
		}
	}

	$(function() {
		ajaxSetup();//ajax전역 설정
		modalInit();//modal들의 초기화
		searchBarInit();//검색바 초기화
		sideMenuInit();//사이드 메뉴 초기화
		tablesInit(); //테이블 초기화
		tableMenuInit();//우하단 테이블 메뉴 초기화
	})

	function sideMenuInit() {
		$("nav[class*=sidebar]  li").on("click", function() {
			$menuWrapper.hide().children().hide();
		})

		$("#li-showAdminMainPage").on("click", function(e) {
			e.preventDefault();
			$("#div-tablePage").show();
			$("#div-dbBoardPage").hide();
			$searchBar.hide();
			//$("#div-parsingPage").hide();
		})
		$(".dbBoardListItem").on(
				"click",
				function(e) {
					e.preventDefault();
					const id = $(this).data("boardId");
					$("#div-tablePage").hide();
					$("#div-dbBoardPage").show();
					$("[data-table-id]").hide();
					$("[data-table-id=" + id + "]").show();
					$searchBar.show();

					$targetTable && removeTableRows($targetTable);
					$targetTable = $("[data-table-id=" + id + "]");
					$menuWrapper.show().find("[data-table-for=" + id + "]")
							.show();
					ajaxLoadTable($targetTable, drawTable,
							dbBoardOptions[$targetTable.data("boardName")]);

				})

	}

	function modalInit() {
		//siteCode들을 select에 넣기
		$
				.getJSON("/admin/selectSite.ajax")
				.done(
						function(data) {
							const result = data.result.dataList;

							//const $target = $("[id*=-siteCodeList]");
							$("[id*=-siteCodeList]")
									.each(
											function() {
												const $target = $(this);
												$target
														.on(
																"change",
																function() {
																	const $targetModal = $target
																			.parents(".modal");
																	const $typeSelect = $targetModal
																			.find("select[id*=boardType]");

																	$typeSelect
																			.find(
																					"*")
																			.remove();
																	const boardTypeList = $target
																			.find(
																					"option:selected")
																			.data(
																					"boardTypeList");
																	for ( let i in boardTypeList)
																		$typeSelect
																				.append("<option value='"+boardTypeList[i]+"'>"
																						+ boardTypeList[i]
																						+ "</option>");
																})

												for (let i = 0; i < result.length; ++i) {
													const $option = $("<option value='"+result[i].site_code+"'>"
															+ result[i].site_name
															+ "</option>");
													$target
															.append($option
																	.data(
																			"boardTypeList",
																			result[i].board_type_list));
												}

												$target.trigger("change");

											});

						})
		///////////////////////addBoard modal event////////////////////////
		//addBoard-modal의 게시판 추가 버튼을 누르면        
		$("#modal-addBoard_btn-addBoard").on("click", function() {
			$.ajax({
				type : "POST",
				url : "/admin/insertBoard.ajax",
				data : formDataToJson($("#form-insertBoard").serializeArray()),
				dataType : "json"
			}).done(function(data) {
				$addBoardModal.modal("hide");
				removeTableRows($targetTable);
				ajaxLoadTable($targetTable, drawTable);
				makeToast({
					message : "게시판을 추가했습니다."
				});

			})

		})
		//////////////////////////////////////////////////////////////////////
		///////////////////////updateBoard modal event////////////////////////
		//update modal의 확인 버튼을 눌렀을때
		$("#modal-updateBoard_btn-updateBoardAjax").on(
				"click",
				function() {
					const updateData = $updateBoardModal.data("updateData");
					const formData = formDataToJson($updateBoardModal.find(
							"form").serializeArray());

					for (let i = 0; i < updateData.length; ++i) {

						const keys = Object.keys(formData);
						for (let j = 0; j < keys.length; ++j)
							updateData[i][keys[j]] = formData[keys[j]];

					}

					$.ajax({
						type : "POST",
						url : "/admin/updateBoard.ajax",
						data : JSON.stringify({
							dataList : updateData
						}),
						dataType : "json",
						contentType : "application/json"
					}).done(
							function(data) {

								ajaxLoadTable($targetTable, drawTable);
								alert(data.result.updatedLength
										+ " 개의 게시판을 업데이트 하였습니다.");
								$updateBoardModal.modal("hide");

							})
				})

		//update modal의 확인 버튼을 눌렀을때
		$("#modal-deleteBoard_btn-deleteBoardAjax").on("click", function() {
			$deleteBoardModal.modal("hide");
			$.ajax({
				type : "POST",
				url : "/admin/deleteBoard.ajax",
				data : JSON.stringify({
					dataList : $deleteBoardModal.data("deleteData")
				}),
				dataType : "json",
				contentType : "application/json"
			}).done(function(data) {

				ajaxLoadTable($targetTable, drawTable);
				alert(data.result.deletedLength + " 개의 게시판을 삭제하였습니다");
				$deleteBoardModal.modal("hide");

			})

		});
		////////////////////////////////////////////////////////////////////////
		///////////////////////addSite modal event/////////////////////////////
		//사이트 더하기 모달의 검색타입 더하기 버튼 클릭시
		$("#modal-addSite .btn-addSearchType").on(
				"click",
				function() {
					const $clone = $addSiteModal.find(".form-row.d-none")
							.clone().removeClass("d-none");
					$addSiteModal.find(".div-searchTypeWrapper").append(
							$clone.toggle("highlight"));
				})

		$("#modal-addSite .btn-deleteSearchType").on(
				"click",
				function() {
					const $last = $addSiteModal
							.find(".div-searchTypeWrapper .form-row:last");
					$last.length > 0 && $last.remove();
				})

		const $inputSiteIcon = $("#modal-addSite_input-siteIcon");
		const $deleteSiteIcon = $("#modal-addSite_button-deleteFile");
		//사이트 더하기 모달 파일 삭제 할때
		$("#modal-addSite_button-deleteFile").on(
				"click",
				function() {
					const currFile = $inputSiteIcon.data("currFile");

					const deleteTarget = new Array(currFile);

					$.ajax({
						type : "POST",
						url : "/admin/deleteFiles.ajax",
						data : JSON.stringify({
							dataList : deleteTarget
						}),
						dataType : "json",
						contentType : "application/json"
					}).done(
							function(data) {
								const result = data.result.deletedList[0];
								$inputSiteIcon.removeClass(
										"border border-success").removeData(
										"currFile").attr("disabled", false)
										.val(null);

								$deleteSiteIcon.hide();
							});

				})

		//사이트 더하기 모달 파일 추가 할때
		$inputSiteIcon.on("change", function() {
			const formData = new FormData();
			formData.append("fileType", $("#modal-addSite_select-fileType")
					.val());
			const fileList = this.files;
			const fileLength = fileList.length;
			if (fileLength < 1) {
				$deleteSiteIcon.trigger("click");
				return;
			}

			for (let i = 0; i < fileList.length; ++i)
				formData.append("file_" + i, this.files[i]);

			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false,
				url : "/admin/insertFiles.ajax",
				data : formData,
				dataType : "json"
			}).done(
					function(data) {
						const result = data.result.uploadedFiles[0];
						//makeToast(result.original_file_name + "을 저장했습니다.");
						$inputSiteIcon.addClass("border border-success").data(
								"currFile", result).attr("disabled", true);
						$deleteSiteIcon.show();

					});

		})

		//사이트 더하기 모달의 사이트 더하기 버튼 클릭시
		$("#modal-addSite_btn-addSiteAjax").on("click", function() {
			const formData = $addSiteModal.find("form").serializeArray();
			const currFile = $inputSiteIcon.data("currFile") || null;
			const typeNameArr = new Array();
			const typeValArr = new Array();
			const resultArr = new Array();
			for (let i = 0; i < formData.length; ++i) {

				const data = formData[i];
				if (data.name == "type_name")
					typeNameArr.push(data.value);
				else if (data.name == "type_val")
					typeValArr.push(data.value);
			}

			for (let i = 0; i < typeNameArr.length; ++i) {
				resultArr.push({
					"type_name" : typeNameArr[i],
					"type_val" : typeValArr[i]
				})
			}

			formData.push({
				name : "searchType_list",
				value : resultArr
			});
			currFile && formData.push({
				name : "site_icon",
				value : currFile.file_location
			})

			$addSiteModal.modal("hide");
			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false,
				url : "/admin/insertSite.ajax",
				data : JSON.stringify(formDataToJson(formData)),
				dataType : "json",
				contentType : "application/json"
			}).done(function(data) {

				makeToast("게시판 추가에 성공했습니다");

			});
		})

		$updateSiteModal.on("show.bs.modal", function() {
			const dataList = $updateSiteModal.data("dataList");

			if (dataList.length == 1) {
				$(this).find("#input-updateSite-available").val(
						dataList[0].available);
			} else
				$(this).find("#input-updateSite-available").val("Y");

		})

		$("#modal-updateSite_btn-updateSiteAjax").on(
				"click",
				function() {

					const dataList = $updateSiteModal.modal("hide").data(
							"dataList");

					if (!dataList.length) {
						alert("선택된 사이트가 없습니다.");
						$updateSiteModal.modal("hide");
					} else {
						for (let i = 0; i < dataList.length; ++i) {
							dataList[i].available = $(
									"#input-updateSite-available").val();
						}

						$.ajax({
							method : "POST",
							url : "/admin/updateSite.ajax",
							data : JSON.stringify({
								"dataList" : dataList
							}),
							contentType : "application/json"
						}).done(function() {
							resetAllSearchOption($targetTable);
							makeToast("테이블이 업데이트 되었습니다.");
						})

					}

				})

		//////////////////////////////////////////////////////////////////
		//////////////////////////공지사항 modal 관련 이벤트////////////////////

		//db 게시판 modal이 나타나고 사라질때 이벤트
		$("#modal-insertDbArticle")
				.on(
						"show.bs.modal",
						function() {
							if ($("#select-dbArticle-dbBoardCodeList").data(
									"init") == false) {
								$
										.getJSON(
												"/admin/selectDbBoardList.ajax")
										.done(
												function(data) {
													const result = data.result.boardList;
													const $target = $("#select-dbArticle-dbBoardCodeList");

													for (let i = 0; i < result.length; ++i) {
														$target
																.append("<option value='"+result[i].board_code+"'>"
																		+ result[i].board_name
																		+ "</option>");
													}

												})
								$("#select-dbArticle-dbBoardCodeList").data(
										"init", true);
							}

						}).on("hidden.bs.modal", function() {
					$(this).find("input").each(function() {
						$(this).val("");
					})
					$(this).find("select").removeAttr("disabled");
					$(this).find("textarea").val("");

				})

		//db 게시판 modal 확인 버튼 이벤트
		$("#modal-insertDbArticle_btn-insertDbArticleAjax").on(
				"click",
				function() {
					const disabledInput = $("#form-insertDbArticle").find(
							"input[disabled]");
					$("#form-insertDbArticle").find("input ,select").each(
							function() {
								$(this).removeAttr("disabled");
							})

					const inputData = formDataToJson($("#form-insertDbArticle")
							.serializeArray());
					disabledInput.each(function() {
						$(this).attr("disabled", true);
					})

					if (inputData.article_no) {
						$.post("/admin/updateDbBoardArticle.ajax", inputData)
								.done(function(data) {
									alert("글을 수정했습니다.");
								})

					} else {
						$.post("/admin/insertDbBoardArticle.ajax", inputData)
								.done(function(data) {
									alert("공지사항이 추가되었습니다.");

								}).fail(
										function() {
											alert(inputData.board_name
													+ " 게시판에 글 작성을 실패했습니다.");
										})
					}

					$("li[data-board-id=" + inputData.board_code + "]")
							.trigger("click");
					removeTableRows($targetTable);
					ajaxLoadTable($targetTable, drawTable);
					$("#modal-insertDbArticle").modal("hide");

				})
	}

	//우하단 테이블 메뉴 이벤트 바인딩
	function tableMenuInit() {

		$menuWrapper.find(".btn-resetOption").on("click", function() {
			//$targetTable.data("searchOption", null);
			resetAllSearchOption($targetTable);
			ajaxLoadTable($targetTable, drawTable);
		})

		$menuWrapper.find(".btn-addBoard").on("click", function() {
			$addBoardModal.modal("show");
		})
		//update 버튼 이벤트
		$menuWrapper
				.find(".btn-updateBoard")
				.on(
						"click",
						function() {
							//선택된 row 의 데이터를 array로 가져옴
							const updateData = $.map($targetTable
									.find(".tr-selected"), function(val, i) {
								return $(val).data("data");
							});
							//data에 set
							$updateBoardModal.data("updateData", updateData);
							//data 갯수에 따라 메뉴 변경
							if (updateData.length > 1) {
								$updateBoardModal.find(
										".div-updateBoard-singleTarget").hide()
										.find("input,select").attr("disabled",
												true).hide();
							} else if (updateData.length == 1) {
								const $boardTypeSelect = $("#input-updateBoard-boardType");
								$boardTypeSelect.empty();
								$
										.get("/admin/selectSite.ajax")
										.done(
												function(data) {
													const siteList = data.result.dataList;
													for ( let i in siteList) {
														if (siteList[i].site_code == updateData[0].site_code) {
															for ( let j in siteList[i].board_type_list) {
																const type = siteList[i].board_type_list[j];
																$boardTypeSelect
																		.append('<option '
																				+ (type == updateData[0].board_type ? 'selected'
																						: '')
																				+ ' value="'
																				+ type
																				+ '">'
																				+ type
																				+ '</option>');
															}

														}

													}

												})

								const data = updateData[0];
								$updateBoardModal.find(
										".div-updateBoard-singleTarget").show()
										.find("input,select").attr("disabled",
												false).show();
								$("#input-updateBoard-siteCodeList").find(
										"option[value=" + data.site_code + "]")
										.attr("selected", true);
								$("#input-updateBoard-boardName").val(
										data.board_name);
								$("#input-updateBoard-boardAddress").val(
										data.board_address);
								$("#input-updateBoard-boardType")
										.find(
												"option[value="
														+ data.board_type + "]")
										.attr("selected", true);
								$("#input-updateBoard-available").find(
										"option[value=" + data.available + "]")
										.attr("selected", true);

							} else {
								alert("선택된 게시판이 없습니다.");
								return;
							}

							$updateBoardModal.modal("show");
						})

		//게시판 삭제 메뉴 클릭시	
		$menuWrapper.find(".btn-deleteBoard").on(
				"click",
				function() {
					const deleteData = $.map($targetTable.find(".tr-selected"),
							function(val, i) {
								return $(val).data("data");
							});

					const length = deleteData.length;

					if (length <= 0) {
						alert("선택된 게시판이 없습니다.");
						return;
					}
					const message = length > 1 ? "'" + deleteData[0].board_name
							+ "'" + " 외 " + (length - 1) + " 개의 게시판을 삭제하시겠습니까?"
							: "'" + deleteData[0].board_name + "'"
									+ " 게시판을 삭제하시겠습니까?";
					$deleteBoardModal.data("deleteData", deleteData).find(
							"[class*=Message]").text(message);
					$deleteBoardModal.modal("show");
				})

		/////////////////////////////////////site menu////////////////////////////////////////

		$menuWrapper.find(".btn-deleteSite").on(
				"click",
				function() {

					//const target = $targetTable.find(".tr-selected").data("data");

					const target = $.map($targetTable.find(".tr-selected"),
							function(val, i) {
								return $(val).data("data");
							})

					if (!target)
						alert("선택된 사이트가 없습니다.");
					else if (confirm("(주의) 선택된 사이트를 삭제하겠습니까?")) {
						$.ajax({
							type : "POST",
							url : "/admin/deleteSite.ajax",
							data : JSON.stringify({
								dataList : target
							}),
							dataType : "json",
							contentType : "application/json"
						}).done(function(data) {
							alert(data.result.deletedSite);
							resetAllSearchOption($targetTable);
						})

						// 				fetchOption.body = JSON.stringify(target);

						// 				fetch(getContextPath() + "/admin/deleteSite.ajax", fetchOption).then(res=>res.json()).then(function(response){
						// 					alert(response.result.test);
						// 				}).catch(function(e){
						// 					alert(e);
						// 				})
					}

				})

		//사이트 더하기 메뉴 클릭시
		$menuWrapper.find(".btn-addSite").on("click", function() {

			$("#modal-addSite").modal("show");

		});

		//사이트 업데이트 메뉴 클릭시
		$menuWrapper.find(".btn-updateSite").on(
				"click",
				function() {

					const selected = $.map($targetTable.find(".tr-selected"),
							function(val, i) {

								return $(val).data("data");
							})
					!selected.length ? alert("선택된 사이트가 없습니다.") : $(
							"#modal-updateSite").data("dataList", selected)
							.modal("show");
				})

		// DB게시판 메뉴 클릭시
		$(".btn-addDbArticle").on("click", function() {
			$("#modal-insertDbArticle").modal("show");
		})

	}

	//검색바 이벤트
	function searchBarInit() {

		const $searchBarInput = $("#input-searchTable");//테이블 검색 input
		const $keywordInputOverlay = $searchBar.find(".span-keywordOverlay"); //input overlay 버튼
		const $orderbyInputOverlay = $searchBar.find(".span-orderbyOverlay"); //input overlay 버튼
		const $siteCodeInputOverlay = $searchBar.find(".span-siteCodeOverlay"); //input overlay 버튼
		const $searchBarButton = $searchBar.find(".btn-searchTable");//input 옆에 버튼

		$searchBarButton.on("click", function() {

			//$targetTable.data("tableLoaded", false);
			const option = $targetTable.data("searchOption");
			const keyword = $searchBarInput.val();
			$keywordInputOverlay.toggle().html(
					keyword + '<span class="ml-1 text-dark">X</span>');

			if (option) {
				option.keyword = keyword;
			} else {
				$targetTable.data("searchOption", {
					keyword : keyword
				})
			}

			ajaxLoadTable($targetTable, drawTable);
		});

		//인풋 안의 X버튼 클릭시
		$keywordInputOverlay.on("click", function(e) {
			e.stopPropagation();
			//$targetTable.data("tableLoaded", false);
			$(this).hide();
			resetKeywordOption($targetTable);
			$searchBarInput.val("");
			ajaxLoadTable($targetTable, drawTable);
		});

		//인풋 안의 orderby버튼 클릭시
		$orderbyInputOverlay.on("click", function(e) {
			e.stopPropagation();
			$(this).hide();
			//$targetTable.data("tableLoaded", false);
			resetOrderbyOption($targetTable);
			ajaxLoadTable($targetTable, drawTable);
		});

		//siteCode 버튼 클릭시
		$siteCodeInputOverlay.on("click", function(e) {
			e.stopPropagation();
			$(this).hide();
			resetSiteCodeOption($targetTable);
			ajaxLoadTable($targetTable, drawTable);
		})

	}

	//모든 테이블 검색 옵션 초기화
	function resetAllSearchOption(target) {

		resetOrderbyOption(target);
		resetKeywordOption(target);
		resetSiteCodeOption(target);
		ajaxLoadTable($targetTable, drawTable);
		$searchBar.find(".inputOverlay").hide();

	}

	//currentColumn이 있으면 그것 빼고 리셋 없으면 다 리셋
	function resetOrderbyOption(target, currentColumn) {

		target.data("searchOption") && (function() {
			delete target.data("searchOption").orderby;
		})();

		target.find("thead th").each(function(index, item) {

			const $this = $(this);

			if ($this.is(currentColumn))
				return true;
			else {
				$this.find("[data-orderby*=null]").show();
				$this.find("[data-orderby*=ASC],[data-orderby*=DESC]").hide();
			}
		})
	}

	function resetKeywordOption(target) {
		target.data("searchOption") && (function() {
			delete target.data("searchOption").keyword;
		})();
	}

	function resetSiteCodeOption(target) {
		target.data("searchOption") && (function() {
			delete target.data("searchOption").site_code;
		})();

	}

	function tablesInit() {

		//collapse관련 이벤트
		$("div[id*=collapse]").on(
				"hidden.bs.collapse",
				function() {
					$searchBar.hide();
					$("#dataTables .collapse.show").length < 1 ? $menuWrapper
							.hide().children().hide() : $menuWrapper.children()
							.hide();
				}).on("shown.bs.collapse", function() {
			const id = $(this).find("table").attr("id");
			$searchBar.show();
			$menuWrapper.children().hide();
			$menuWrapper.show().find("[data-table-for=" + id + "]").show();
			$targetTable = $(this).find("table");

		});

		/*site table Collapse 열때  */
		$("div[id=collapseOne]").on("show.bs.collapse", function() {
			ajaxLoadTable($(this).find("table"), drawTable, siteTableOption);
		});

		/* board table Collapse 열때  */
		$("div[id=collapseTwo]").on("show.bs.collapse", function() {
			ajaxLoadTable($(this).find("table"), drawTable, boardTableOption);
		});

		//th sort 버튼 이벤트

		const $orderbyOverlay = $searchBar.find(".span-orderbyOverlay"); //input overlay 버튼
		$("i[data-orderby]").on(
				"click",
				function() {

					const $this = $(this);
					//다음 orderby 할 element를 부르고 없으면 set
					const $nextOrderby = $this.data("next")
							|| $this.data(
									"next",
									$this.next().length ? $this.next()
											: $($this.siblings("i").get(0)))
									.data("next");
					const $table = $this.closest("[id*=table-]");
					const nextOrderbyVal = $nextOrderby.data("orderby");
					//현재컬럼 빼고 다른 orderby 리셋
					resetOrderbyOption($table, $this.closest("th"));
					//searchBar에 orderby 버튼 추가
					nextOrderbyVal ? $orderbyOverlay.show().html(
							nextOrderbyVal
									+ '<span class="ml-1 text-dark">X</span>')
							: $orderbyOverlay.hide();
					let targetData = $table.data("searchOption") || {};
					targetData.orderby = nextOrderbyVal;
					$table.data("searchOption", targetData);
					$this.toggle();
					$nextOrderby.toggle();
					ajaxLoadTable($table, drawTable);
				});

		//테이블 모두 선택 checkbox 이벤트
		$("input[id*=checkAll]").on("change", function() {

			const $table = $(this).closest("table");

			if ($(this).is(":checked"))
				$table.find("tbody").find("tr").addClass("tr-selected");
			else
				$table.find("tbody").find("tr").removeClass("tr-selected");
		})
	}

	//타겟 테이블 body안의 element 삭제
	function removeTableRows(target) {
		$(target).find("tbody tr").remove();
		$(target).siblings(".paginationWrapper").remove();
	}

	function ajaxLoadTable(target, callbackFunc, option) {

		// 		if ($(target).data("tableLoaded"))
		// 			return;

		const searchOption = target.data("searchOption");

		//ajax로 해당 데이터를 받아옴
		$.ajax({
			type : "GET",
			url : target.data("requestUrl"),
			data : searchOption,
			dataType : "json"
		}).done(function(data) {
			removeTableRows(target);
			callbackFunc(target, data, option);
		});

	}

	//sorting을 해주는 element(화살표)를 만듬
	function makeSortingElement($target, propName) {
		const $orderbyOverlay = $searchBar.find(".span-orderbyOverlay"); //input overlay 버튼
		const $frag = $(document.createDocumentFragment());

		$frag
				.append('<i class="fas fa-sort " data-orderby="null">')
				.append(
						'<i class="fas fa-sort-up " data-orderby="'+propName+'ASC"></i>')
				.append(
						'<i class="fas fa-sort-down" data-orderby="'+propName+'DESC"></i>');

		$frag.find("i[data-orderby]").on(
				"click",
				function() {

					const $this = $(this);
					//다음 orderby 할 element를 부르고 없으면 set
					const $nextOrderby = $this.data("next")
							|| $this.data(
									"next",
									$this.next().length ? $this.next()
											: $($this.siblings("i").get(0)))
									.data("next");
					const $table = $this.closest("[id*=table-]");
					const nextOrderbyVal = $nextOrderby.data("orderby");
					//현재컬럼 빼고 다른 orderby 리셋
					resetOrderbyOption($table, $this.closest("th"));
					//searchBar에 orderby 버튼 추가
					nextOrderbyVal ? $orderbyOverlay.show().html(
							nextOrderbyVal
									+ '<span class="ml-1 text-dark">X</span>')
							: $orderbyOverlay.hide();
					let targetData = $table.data("searchOption") || {};
					targetData.orderby = nextOrderbyVal;
					$table.data("searchOption", targetData);
					$this.toggle();
					$nextOrderby.toggle();
					ajaxLoadTable($table, drawTable);
				});

		return $frag;
	}

	function drawTable(target, data, option) {
		const dataList = data.result.dataList;
		const dataListLength = dataList.length;
		const pageInfo = data.result.pageInfo;
		const $table = $(target);
		const searchOption = $table.data("searchOption") || {};
		const $tbody = $table.find("tbody").length ? $table.find("tbody")
				: $("<tbody></tbody>");
		option && $table.data("tableOption", option);
		const _option = option || $table.data("tableOption") || {};

		//option
		const optionListOrder = _option.listOrder;
		const optionWidth = _option.width;
		const optionSortRow = _option.sortRow;
		const optionContent = _option.content;
		const optionShowNum = _option.showNum;
		const optionMultiSelect = _option.multiSelect;
		const optionSelectable = _option.selectable == void (0) && true;

		let keyList = optionListOrder || Object.keys(dataList[0]);

		/* optionShowNum && keyList.unshift("numRow"); */

		(optionShowNum && !keyList.includes("numRow"))
				&& keyList.unshift("numRow");
		//thead draw
		if (!$table.find("thead").length) {

			const keyLength = keyList.length;
			const $thead = $('<thead></thead>');
			const $tr = $("<tr></tr>");
			for (let i = 0; i < keyLength; ++i) {
				const prop = keyList[i];
				let $th = null;
				if (i == 0 && optionShowNum)
					$th = $('<th class="th-0"><input class="input-checkAll" type="checkbox" /></th>');
				else
					$th = $('<th class="th-' + i + '"></th>').append(
							"<span>" + prop + "<span>");

				(optionSortRow && optionSortRow.includes(prop))
						&& $th.append(makeSortingElement($table, prop));

				if (optionWidth)
					$th.css("width", optionWidth[i] + "%" || (100 / keyLength)
							+ "%");

				$tr.append($th);
			}

			$table.append($thead.append($tr));

			//테이블 모두 선택 checkbox 이벤트
			if (optionMultiSelect && optionSelectable) {
				$table.find("input[class*=checkAll]").on(
						"change",
						function() {

							const $table = $(this).closest("table");

							if ($(this).is(":checked"))
								$table.find("tbody").find("tr").addClass(
										"tr-selected");
							else
								$table.find("tbody").find("tr").removeClass(
										"tr-selected");
						})
			} else
				$table.find("input[class*=checkAll]").remove();

		}

		//tbody draw
		for (let i = 0; i < dataListLength; ++i) {
			let $tr = $("<tr></tr>");
			const data = dataList[i];
			$tr.on(
					"click",
					function() {

						(optionMultiSelect && optionSelectable)
								|| $(this).siblings("tr").removeClass(
										"tr-selected");
						optionSelectable && $(this).toggleClass("tr-selected");

					}).data("data", data);

			for (let j = 0; j < keyList.length; ++j) {

				const prop = keyList[j];

				if (optionContent && optionContent[prop])
					$tr.append($("<td>").append(
							optionContent[prop]($table, dataList[i])));
				else if (prop === "numRow")
					$tr.append($("<td><span>" + i + "</span></td>"));
				else
					$tr
							.append($("<td><input class='w-100' name='"+keyList[j]+"'  type='text' val='"+dataList[i][keyList[j]]+"' value='"+dataList[i][keyList[j]]+"' disabled></input></td>"));

			}
			$tbody.append($tr);
		}

		$table.append($tbody);

		//paginationDraw
		if (pageInfo) {
			const $paginationWrapper = $('<div class="container paginationWrapper">'
					+ '<nav aria-label="Page navigation ">'
					+ '<ul class="pagination justify-content-center">'
					+ '</ul></nav></div>');

			if (pageInfo.currentPage == 1)
				$paginationWrapper
						.find("ul")
						.append(
								'<li class="page-item disabled"><button class="page-link" tabIndex=-1>Prev</button></li>');
			else
				$paginationWrapper
						.find("ul")
						.append(
								'<li class="page-item " data-offset="'
										+ (pageInfo.currentPage - 1)
										+ '"><button class="page-link">Prev</button></li>');

			if (pageInfo.maxPage > 0)
				for (let i = pageInfo.startPage; i <= pageInfo.endPage; i++) {
					if (i == pageInfo.currentPage)
						$paginationWrapper.find("ul").append(
								'<li class="page-item active" data-offset="'+i+'"><button class="page-link">'
										+ i + '</button></li>');
					else
						$paginationWrapper.find("ul").append(
								'<li class="page-item " data-offset="'+i+'"><button class="page-link">'
										+ i + '</button></li>');
				}
			else
				$paginationWrapper.find("ul").append(
						'<li class="page-item active" data-offset="'+1+'"><button class="page-link">'
								+ 1 + '</button></li>');
			if (pageInfo.currentPage == pageInfo.maxPage)
				$paginationWrapper
						.find("ul")
						.append(
								'<li class="page-item disabled"><button class="page-link" tabIndex=-1>Next</button></li>');
			else
				$paginationWrapper
						.find("ul")
						.append(
								'<li class="page-item" data-offset="'
										+ (pageInfo.currentPage + 1)
										+ '"><button class="page-link" tabIndex=-1>Next</button></li>');

			$paginationWrapper.find("li")
					.on(
							"click",
							function() {
								$table.data("searchOption").offset = $(this)
										.data("offset");
								ajaxLoadTable($targetTable, drawTable,
										dbBoardOptions[$targetTable
												.data("boardName")]);
							})

			$table.after($paginationWrapper);
		}
	}

	/////////////////////////util//////////////////////////
	function getTableRowInputData(tr) {
		let obj = {};
		$(tr).find("input").each(function() {
			obj[$(this).attr("name")] = $(this).val();
		})
		return obj;

	}

	function setInputDataToModal(modal, dataObj) {
		if (typeof dataObj == "object") {
			$(modal).find("input ,textarea").each(function() {
				const dataName = $(this).attr("name");
				const data = dataObj[dataName];
				data && $(this).val(data);
			})
		} else if (Array.isArray(dataObj)) {
			$(modal).find("input,textarea").each(function() {

				for (let i = 0; i < dataObj.length; ++i) {
					const dataName = $(this).attr("name");
					const data = dataObj[i][dataName];
					data && $(this).val(data);
				}

			})

		}

	}
</script>
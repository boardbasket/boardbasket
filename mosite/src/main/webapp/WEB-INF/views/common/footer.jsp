<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!--chart.js  -->
<!-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.1/dist/chart.min.js"></script> -->
<script src="${root}/resources/js/common/chartjs.min.js"></script>

<!--moment js  -->
<script src="${root}/resources/js/common/moment.js"></script>
<script src="${root}/resources/js/common/ko.js"></script>

<!--chart.js doughnut label plugin -->
<script src="${root}/resources/js/common/chartjs-plugin-datalabels.min.js"></script>
<!--chart.js annotation label plugin -->
<script src="${root}/resources/js/common/chartjs-plugin-annotation.min.js"></script>
<!--chart.js moment adapter -->
<script src="${root}/resources/js/common/chartjs-adapter-moment.min.js"></script>

<script src="${root}/resources/js/common/jquery.scrollbar.js"></script>

<!-- d3 -->
<script src="${root}/resources/js/common/d3.v4.min.js"></script>
<script src="${root}/resources/js/common/d3.layout.force.js"></script>
<script src="${root}/resources/js/common/d3.layout.cloud.js"></script>
<script src="${root}/resources/js/common/d3.legend.js"></script>



<script src="${root}/resources/js/common/lodash.js"></script>

<script src="${root}/resources/js/common/util.js"></script>

<!--contentCommon.js  -->
<script src="${root}/resources/js/content/contentCommon.js"></script>

<!--comment.js  -->
<script src="${root}/resources/js/content/comment.js"></script>

<!--chartObj.js  -->
<script src="${root}/resources/js/content/chartObj.js"></script>

<!--board.js  -->
<script src="${root}/resources/js/content/board.js"></script>

<!--wordCloud.js  -->
<script src="${root}/resources/js/content/wordCloud.js"></script>

<!--wordCloud.js  -->
<script src="${root}/resources/js/content/forceLayout.js"></script>

<!-- relativeBoardSwitcher -->
<script src="${root}/resources/js/content/wordCloudChanger.js"></script>

<!--HeatMap-->
<script src="${root}/resources/js/content/heatMap.js"></script>

<!--radarChart-->
<script src="${root}/resources/js/content/radarChart.js"></script>

<!--searchBar-->
<script src="${root}/resources/js/content/searchBar.js"></script>

<!--tagAnalysis-->
<script src="${root}/resources/js/content/tagAnalysis.js"></script>

<!--tagInfo-->
<script src="${root}/resources/js/content/tagInfo.js"></script>

<!--boardPercentageChart-->
<script src="${root}/resources/js/content/boardPercentageChart.js"></script>

<!--constants.js  -->
<script src="${root}/resources/js/common/constants.js"></script>

<!--option.js  -->
<script src="${root}/resources/js/content/chartMenu.js"></script>

<!--articleCollapse  -->
<script src="${root}/resources/js/content/articleCollapse.js"></script>

<!--articleNumChart-->
<script src="${root}/resources/js/content/articleNumChart.js"></script>

<!--categoricalChart-->
<script src="${root}/resources/js/content/categoricalChart.js"></script>

<!--tagRankChart-->
<script src="${root}/resources/js/content/tagRankChart.js"></script>

<!--tagRankList-->
<script src="${root}/resources/js/content/tagRankList.js"></script>

<!--init-->
<script src="${root}/resources/js/content/init.js"></script>

<!-- kakao adfit -->
<!-- <script type="text/javascript" src="//t1.daumcdn.net/kas/static/ba.min.js" async></script> -->


<footer class="text-center" data-anchor>
	<hr />
	<div class="container">
		<%-- <div class="d-flex justify-content-center">
			<div>
				<c:choose>
					<c:when test="${sessionScope.isMobile eq true }">
						<ins class="kakao_ad_area" style="display: none;" data-ad-unit="DAN-4HkRD7KgovoLXlJZ" data-ad-width="320" data-ad-height="50"></ins>
					</c:when>
					<c:otherwise>
						<ins class="kakao_ad_area" style="display: none;" data-ad-unit="DAN-XkgXvpi6RgarqGYr" data-ad-width="728" data-ad-height="90"></ins>
					</c:otherwise>
				</c:choose>
			</div>
		</div> --%>
		<div class="d-flex justify-content-center">
			<div class="footer__notice text-muted small">
				<span>이 사이트에서 제공하는 정보는 각 커뮤니티 게시판의 글을 일부 분석한 결과이며 해당 사이트/게시판을(를) 완벽히 반영하지 않을 수 있습니다.</span>
			</div>
		</div>
		<div class="d-flex justify-content-center">
			<ul class="horizontal-list">
				<li>
					<button class="btn-tagPanInfo btn btn-sm btn-link text-muted font-small" tabindex="-1" data-toggle="modal" data-target="#siteInfo">사이트 정보</button>
				</li>
				<li class="mx-2">|</li>
				<li><a class="font-small text-muted" href="mailto:patayokr@gmail.com?subject=[Tagpan 문의]:" target="_blank" tabindex="-1">이메일 문의</a></li>
				<sec:authorize access="isAuthenticated()">
					<li class="mx-2">|</li>
					<li>
						<button class="btn btn-sm btn-link text-muted font-small" type="button" tabindex="-1" data-toggle="modal" data-target="#deleteMember">회원 탈퇴</button>
					</li>
				</sec:authorize>
				<li class="mx-2">|</li>
				<li><a target="_blank" class="font-small text-muted" href="https://blog.naver.com/patayokr">공지사항</a></li>
			</ul>
		</div>
		<!-- <div class="d-flex justify-content-center">
			<script type="text/javascript" src="https://cdnjs.buymeacoffee.com/1.0.0/button.prod.min.js" data-name="bmc-button" data-slug="nayuta" data-color="#5F7FFF" data-emoji="" data-font="Inter" data-text="buy me coffee"
				data-outline-color="#000000" data-font-color="#ffffff" data-coffee-color="#FFDD00"
			></script>
		</div> -->
	</div>
	<div class="container">
		<span class="text-muted font-small">Copyright 2022. TagPAN All rights reserved.</span>
	</div>
</footer>


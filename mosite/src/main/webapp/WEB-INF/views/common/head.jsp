<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:eval expression="@const.getProperty('const.defaultNickName')" var="defaultNickName" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.comment')" var="commentPattern" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.nickname')" var="nicknamePattern" scope="page" />
<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />
<c:choose>
	<c:when test="${sessionScope.isMobile == true}">
		<c:set var="sessionIsMobile" value="mobileVer"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="sessionIsMobile" value=""></c:set>
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${sessionScope.isTablet == true}">
		<c:set var="sessionIsTablet" value="tabletVer"></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="sessionIsTablet" value=""></c:set>
	</c:otherwise>
</c:choose>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="csrf_token" content="${_csrf.token }">
<meta name="csrf_headerName" content="${_csrf.headerName}">
<meta name="description" content="게시판 분석 , 태그 탐색">
<meta name="keyword" content="태그판, tagpan, 태그, 게시판, 분석">
<link rel="shortcut icon" href="/resources/image/favicon.png">

<!--bootstrap css include  -->
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">
<!-- bootstrap / popper.js / jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>

<script src="${root}/resources/js/bootstrap/popper.min.js"></script>

<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>

<link rel="stylesheet" href="${root}/resources/css/jqueryUI/jquery-ui.min.css">

<script src="${root}/resources/js/jqueryUI/jquery-ui.min.js"></script>

<script src="${root}/resources/js/jqueryUI/jquery.ui.touch-punch.min.js"></script>

<!--fontawsome-->
<script src="https://kit.fontawesome.com/a8ee2584fa.js" crossorigin="anonymous"></script>

<!-- common css -->
<link rel="stylesheet" href="${root }/resources/css/common/common.css" />
<!--loading placeholder css  -->
<link rel="stylesheet" href="${root}/resources/css/common/placeholder-loading.css" />
<!--custom scroll css  -->
<link rel="stylesheet" href="${root }/resources/css/common/scrollBar.css" />

<!-- commentTable  css -->
<link rel="stylesheet" href="${root}/resources/css/content/comment.css" />

<!-- board  css -->
<link rel="stylesheet" href="${root}/resources/css/content/board.css" />

<!-- graph css -->
<link rel="stylesheet" href="${root}/resources/css/content/graph.css" />

<link rel="stylesheet" href="${root}/resources/css/content/option.css" />

<link rel="stylesheet" href="${root}/resources/css/content/modal.css" />

<link rel="stylesheet" href="${root}/resources/css/content/frame.css" />

<link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">

<!-- google adsense -->
<!-- <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8012502467122218"
     crossorigin="anonymous"></script> -->

<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="nayuta" data-description="Support me on Buy me a coffee!" data-message="ღ'ᴗ'ღ" data-color="#FF813F"
	data-position="Right" data-x_margin="18" data-y_margin="18"
></script>

<!--naver webmaster  -->
<meta name="naver-site-verification" content="97810a8b47fad14d382d0689eaaf7763f7f68382" />
<!--google webmaster  -->
<meta name="google-site-verification" content="WcXH72FLwA1Ae1ptUNmvAJxc8jbTsvlDEb6EGVswyxw" />


</head>
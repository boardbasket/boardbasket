<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@const.getProperty('const.maxWeekOptionNum')" var="maxWeekOptionNum" scope="page" />
<spring:eval expression="@const.getProperty('const.maxMonthOptionNum')" var="maxMonthOptionNum" scope="page" />
<spring:eval expression="@const.getProperty('const.maxYearOptionNum')" var="maxYearOptionNum" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.max_search_keyword_length')" var="max_search_keyword_length" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.search_keyword_num_limit')" var="searchKeywordNumLimit" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.specialChar')" var="specialChar" scope="page" />
<spring:eval expression="@const.getProperty('const.maxChartTagSelect')" var="maxChartTagSelect" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_base_period')" var="baseRadarPeriod" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.comment_load_num')" var="commentLoadNum" scope="page" />

<style>
.searchBar__clearEvalItem {
	float: right;
}

.search__siteWrapper {
	opacity: 0.5;
}

.search__siteWrapper--activated {
	opacity: 1;
}

.title__name span {
	cursor: pointer;
}

.headerMenu {
	display: flex;
	justify-content: end;
}

.search__condition {
	display: flex;
	align-items: center;
}

.search__clone div {
	display: inline-flex;
	align-items: center;
	flex-wrap: wrap;
}

.mobileVer .search__clone div {
	flex-wrap: nowrap;
}

.search__execute {
	justify-content: end;
}

.search__cloneWrapper>* {
	margin-right: 4px;
}

.search__form {
	margin-bottom: 4px;
	margin-top: 4px;
}

.title__name {
	font-weight: 700;
}

#search__detail {
	margin-top: 1rem;
}

.search__condition i {
	display: none;
}

.search__condition button {
	margin-bottom: 5px;
}

.search__condition[aria-expanded=true] i:nth-child(2) {
	display: inline-block;
}

.search__condition[aria-expanded=false] i:nth-child(1) {
	display: inline-block;
}

.mobileVer #search__cloneScroll {
	overflow-x: scroll;
	white-space: nowrap;
}
</style>

<c:set var="searchData" value="${result.searchData}" />

<div class="title jumbotron jumbotron-fluid" data-anchor>
	<div class="${sessionScope.isMobile?'container-fluid':'container' }">
		<div class="headerMenu">
			<!-- <div>
				<button class="btn btn-primary" onclick="testfunc();">word2vectest</button>
			</div> -->
			<div class="login">
				<sec:authorize access="isAnonymous()">
					<!-- before logined  -->
					<div class="login__btn">
						<c:choose>
							<c:when test="${sessionScope.isMobile eq true}">
								<button class="btn btn-secondary btn-lg" onclick="location.href='${root }/user/login.do'">
									<i class="fas fa-sign-in-alt"></i>
								</button>
							</c:when>
							<c:otherwise>
								<a href="${root }/user/login.do" class="btn btn-secondary div-loginBtn_a-login">로그인</a>
							</c:otherwise>
						</c:choose>
					</div>
				</sec:authorize>


				<sec:authorize access="isAuthenticated()">
					<!--after login  -->
					<button class="btn btn-secondary btn-lg" type="button" id="afterLoginMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-boundary="window">
						<i class="fas fa-bars fa-lg"></i>
					</button>
					<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
						<%-- <a class="dropdown-item dropdown__showNoticeBoard cursor-pointer" data-board-code="${result.noticeBoard.board_code}">공지사항</a> --%>
						<button id="tagPanInfo" class="dropdown-item">TagPAN?</button>
						<a href="https://blog.naver.com/patayokr" target="_blank" class="dropdown-item">공지사항</a>
						<sec:authorize access="hasRole('ROLE_ADMIN')">
							<a class="dropdown-item" href="${root}/admin/adminMain.do">관리자 페이지</a>
						</sec:authorize>
						<div class="dropdown-divider"></div>
						<form action="${root}/logOut.do" method="post">
							<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }" />
							<input id="btn-logOut" type="submit" class="dropdown-item" value="로그아웃" />
						</form>
					</div>

				</sec:authorize>
			</div>
		</div>
	</div>
	<div class="${sessionScope.isMobile?'container-fluid':'container' }">

		<a href="/" target="_self"> <img class="main_logo" src="${root}/getIcon?path=TagPAN_Logo.png" alt="" /> <!-- <h1 class="title__name display-4">
			<span>Tagpan</span>
		</h1> --> <!-- <p class="title__lead lead">게시판 분석 , 태그 탐색</p> -->
		</a>
	</div>

	<!-- <form id="search__form" action="/search" class="form flex-grow-1 " method="get"> -->
	<div class="search ${sessionScope.isMobile?'container-fluid':'container' }">


		<div class="search__form">
			<form id="search__form" action="/search" class="form flex-grow-1 " method="get">
				<div class="input-group input-group-lg">
					<div class="input-group-append ">
						<select class="search__type form-control form-control-lg">
							<option value="all" ${searchData.search_target=='all'?'selected':''}>통합</option>
							<option value="board" ${searchData.search_target=='board'?'selected':''}>게시판</option>
							<option value="tag" ${searchData.search_target=='tag'?'selected':''}>태그</option>
							<option value="article" ${searchData.search_target=='article'?'selected':''}>글</option>
						</select>
					</div>

					<input type="text" class="search__input  form-control form-control-lg ui-widget" id="" placeholder="" name="board_name" value="${result.searchData.board_name}" autocomplete="off" maxlength="${max_search_keyword_length}"
						data-state="closed"
					/>

					<div class="input-group-append">
						<button class="search__execute btn btn-primary" type="button">
							<span class="d-block d-lg-none"><i class="fas fa-search"></i></span> <span class="d-none d-lg-block">검색</span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<div class="search__clone no-gutters row">
			<div class="search__condition col-sm-12 col-md-12 col-lg-1" data-toggle="collapse" data-target="#search__detail" aria-expanded="false" aria-controls="search__detail">
				<button class="badge badge-secondary">
					검색 조건 <i class="fa-solid fa-caret-down"></i> <i class="fa-solid fa-caret-up"></i>
				</button>
			</div>
			<div id="search__cloneScroll" class="row col-sm-12 col-md-12 col-lg-11">
				<div class="search__cloneWrapper col-12">
					<c:if test="${result.searchData.keyword!=null}">
						<c:forEach items="${result.searchData.keyword}" var="token">
							<span class="badge badge-secondary option__keyword" data-keyword="${token}"><i class="fas fa-search mr-1" aria-hidden="true"></i>${token}</span>
						</c:forEach>
					</c:if>

				</div>
			</div>
		</div>
		<div id="search__detail" class="search__menu collapse card">
			<div class="card-body">
				<c:set var="simpleMenu" value="${false}" />
				<%@include file="/WEB-INF/views/main/options.jsp"%>
			</div>
		</div>

		<div class="search__trendKeyword d-none">
			<c:forEach var="trendTag" items="${result.trendTag }">
				<span class="badge badge-secondary option__trendTag" data-keyword="${trendTag}">${trendTag}</span>
			</c:forEach>
		</div>

		<div class="d-none">
			<c:if test="${searchData!=null }">
				<input type="hidden" name="offset" value="1" />
			</c:if>
		</div>
	</div>
	<div class="const">
		<input disabled type="hidden" name="maxYearOptionNum" value="${maxYearOptionNum}" />
		<input disabled type="hidden" name="maxWeekOptionNum" value="${maxWeekOptionNum}" />
		<input disabled type="hidden" name="maxMonthOptionNum" value="${maxMonthOptionNum}" />
		<input disabled type="hidden" name="autoCompleteMinLen" value="${autoCompleteMinWordLen}" />
		<input disabled type="hidden" name="maxSearchKeywordLen" value="${max_search_keyword_length}" />
		<input disabled type="hidden" name="searchKeywordNumLimit" value="${searchKeywordNumLimit}" />
		<input disabled type="hidden" name="maxChartTagSelect" value="${maxChartTagSelect}" />
		<input disabled type="hidden" name="cycleInterval" value="${cycleInterval}" />
		<div class="nowTimeSet" data-year="${today.year}" data-month="${today.month}" data-week="${today.week}"></div>
		<input type="hidden" name="isMobile" value="${sessionScope.isMobile }" />
		<input type="hidden" name="baseRadarPeriod" value="${baseRadarPeriod}" />
		<input type="hidden" name="commentLoadNum" value="${commentLoadNum}" />
	</div>

	<!-- </form> -->
</div>
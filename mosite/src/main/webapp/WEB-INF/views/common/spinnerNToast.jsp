<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!--ajax loadingbar  -->
<div class="div-loadingSpinner spinner-border text-primary" role="status">
	<span class="sr-only">Loading...</span>
</div>
<div class="container toastContainer"></div>


<!--알림 토스트 메시지 더미  -->
<div id="toastDummy" class="toast mx-auto" data-delay="3000" data-autohide="true" style="z-index: 5;">
	<div class="toast-header text-white">
		<i class="fas fa-exclamation mr-auto"></i>&nbsp; <strong class="alertType"></strong>
		<button type="button" class="ml-2 mb-1 close" data-dismiss="toast">×</button>
	</div>
	<div class="toast-body" style="display: flex; align-items: center; justify-content: center;"></div>
</div>


<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@patterns.getProperty('patterns.id')" var="idPattern" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.password')" var="pwdPattern" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.nickname')" var="nicknamePattern" scope="page" />
<c:set var="root" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>회원 가입</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>
<script src="${root}/resources/js/bootstrap/popper.min.js"></script>
<!--bootstrap include  -->
<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">


<!-- font awsome include -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

<!--page css include-->
<link rel="stylesheet" href="${root }/resources/css/user/signUp.css" />
<link rel="stylesheet" href="${root }/resources/css/common/common.css" />

<!--input validation  script-->
<script src="${root}/resources/js/common/inputCheck.js"></script>

</head>


<body>

	<spring:eval expression="@patterns.getProperty('patterns.id')" var="idPattern" />
	<div class="container">

		<div class="card row mainCard_size bg-light ">

			<div class="col-12 mt-3">
				<div class="  my-auto">
					<div class="col v-center c-center ">
						<a href="${root }/"> <img src="https://via.placeholder.com/200x60?text=MOSITE" alt="mosite" />
						</a>
					</div>
				</div>
			</div>

			<div class="col-12 mt-3">
				<div class="  my-auto">
					<div class="col v-center c-center ">
						<span class="font-boldAndBig">회원 가입</span>
					</div>
				</div>
			</div>

			<div class="col-12  ">

				<form action="${root}/user/insertMember.do" method="post" id="registMember" onsubmit="return submitValidation()">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token}" />
					<div class="form-group mt-4 ">
						<label for="regist_id" class="v-center c-center bold-font"><span style="color: red;">*</span>아이디</label>

						<div class="input-group ">
							<input type="text" class="form-control" maxlength="15" name="member_id" id="regist_id" autofocus />
							<div class="input-group-prepend">
								<button class="btn btn-primary" type="button" id="checkDuplicatedId">중복확인</button>
								<button class="btn btn-circle" type="button" data-toggle="popover" data-title="info" data-content="영문자,숫자로 이루어진 5~15자리의 문자열 이어야 합니다"
									tabIndex="-1">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">아이디 형식에 맞지 않습니다.</div>
						</div>
					</div>

					<!-- <div class="form-group mt-4 ">
						<label for="regist_email" class="v-center c-center bold-font">이메일</label>

						<div class="input-group ">
							<input type="email" class="form-control" maxlength="30" name="member_email" id="regist_email" placeholder="example@example.com" />
							<div class="invalid-feedback">이메일 형식에 맞춰 입력해 주세요</div>
						</div>
					</div> -->

					<div class="form-group mt-4 ">
						<label for="regist_pwd" class="v-center c-center bold-font"><span style="color: red;">*</span>비밀번호</label>

						<div class="input-group ">
							<input type="password" class="form-control" maxlength="15" name="member_pwd" id="regist_pwd" />
							<div class="input-group-prepend">
								<button class="btn btn-circle" type="button" data-toggle="popover" data-title="info"
									data-content="영문자,숫자 , 특수문자(!@$%^&*)로 이루어진 7~15자리의 문자열 이어야 합니다." tabIndex="-1">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">비밀번호 형식에 맞지 않습니다</div>
						</div>
					</div>

					<div class="form-group mt-4 ">
						<label for="regist_rePwd" class="v-center c-center bold-font"><span style="color: red;">*</span>비밀번호 재입력</label>

						<div class="input-group ">
							<input type="password" class="form-control" maxlength="15" id="regist_rePwd" name="member_rePwd" />
							<div class="input-group-prepend">
								<button class="btn btn-circle" style="visibility: hidden;" type="button" data-toggle="popover" data-title="info" data-content="" tabIndex="-1">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">비밀번호가 같지 않습니다.</div>
						</div>
					</div>

					<!-- <div class="form-group mt-4 ">
						<label for="regist_nickName" class="v-center c-center bold-font">닉네임</label>

						<div class="input-group ">
							<input type="text" class="form-control" maxlength="15" name="member_nickname" id="regist_nickName" />
							<div class="input-group-prepend">
								<button class="btn btn-circle" type="button" data-toggle="popover" data-title="info" data-content="영문자,숫자,한글로 이루어진 3~15자리의 문자열 이어야 합니다.">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">닉네임 형식에 맞지 않습니다.</div>
						</div>
					</div> -->



					<div class="row  justify-content-around mt-4 mb-3">
						<button class="col-3 btn-primary btn" type="submit">가입</button>
						<button type="button" class="col-3 btn-secondary btn cancleSignUp">취소</button>
					</div>

				</form>
			</div>

			<div class="d-flex flex-column justify-content-center errorMsg">
				<c:forEach items="${fieldErrors}" var="error">
					<span class="color-red errorMsg text-center mb-1"><c:out value="${error.getDefaultMessage()}" /></span>
				</c:forEach>
			</div>


		</div>
		<!--card  -->
	</div>

	<script>
		var duplicateCheck = false;
		$(function() {

			bindPatternValidation($("#regist_id"), new RegExp("${idPattern}"));
			bindPatternValidation($("#regist_pwd"), new RegExp("${pwdPattern}"));
			//bindPatternValidation($("#regist_nickName"), ${nicknamePattern});
			rePwdPatternValidation($("#regist_pwd"), $("#regist_rePwd"));

			$("#regist_id").on("keyup", function() {
				if (duplicateCheck) {
					$("#checkDuplicatedId").addClass("btn-primary").removeClass("btn-success");
					duplicateCheck = false;
				}
			});

			$("[data-toggle='popover']").popover({
				delay : {
					"show" : 100,
					"hide" : 300
				},
				trigger : "click hover",
				placement : "bottom"
			});

			$("#checkDuplicatedId").on("click", function() {

				if (!validTest($("#regist_id"), new RegExp("${idPattern}"))) {
					alert("아이디가 형식에 맞지 않습니다.");
					return;
				}

				$.ajax({
					beforeSend : function(xhr) {
						xhr.setRequestHeader("AJAX", "true");
						xhr.setRequestHeader("${_csrf.headerName}", "${_csrf.token}");
					},
					type : "POST",
					url : '/checkDuplicateId.ajax',
					data : {
						"member_id" : $("#regist_id").val()
					},
					dataType : "json",
					success : function(data) {
						if (data.result.status > 0) {
							$("#checkDuplicatedId").removeClass("btn-primary").addClass("btn-success");
							duplicateCheck = true;
							alert("사용 가능한 아이디 입니다.");

						} else {
							duplicateCheck = false;
							$("#checkDuplicatedId").addClass("btn-primary").removeClass("btn-success");
							alert("사용 하고있는 아이디 입니다.");
						}

					},
					error : function(jqXHR, textStatus, errorThrown) {

						alert("아이디 중복 체크 중에 오류가 생겼습니다.");
					}
				});

			});

			$(".cancleSignUp").on("click", function() {
				location.href = "${root}/";
			});

			setTimeout(function() {
				$(".errorMsg").hide(1000, "swing");
			}, 10000);

			var msg = "${msg}";

			if (msg.length > 0)
				alert(msg);

		});

		function submitValidation() {
			var testResult = validTest($("#regist_id"), new RegExp("${idPattern}"))
					&& validTest($("#regist_pwd"), new RegExp("${pwdPattern}"))
					&& $("#regist_pwd").val() === $("#regist_rePwd").val()
			/* && validTest($("#regist_nickname"), $("#regist_nickname").val(),${nicknamePattern} ) */;

			if (testResult) {
				if (duplicateCheck)
					return true;
				else {
					alert("아이디 중복 확인을 해주세요");
					return false;
				}

			} else {
				alert("입력값이 올바르지 않습니다.");
				return false;
			}

		}
	</script>

</body>

</html>
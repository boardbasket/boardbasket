<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<div class=" div-loginPanel ">

	<div>
		<button class="btn btn-success" onclick="location.href='${root}/user/login.do'">네이버로 로그인</button>
	</div>
	<div>
		<button class="btn btn-primary">카카오로 로그인</button>
	</div>
	<div>
		<button class="btn btn-primary">구글로 로그인</button>
	</div>
	<div>
		<button class="btn btn-primary">페이스북으로 로그인</button>
	</div>

</div>
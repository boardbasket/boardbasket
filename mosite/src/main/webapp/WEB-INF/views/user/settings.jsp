<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />
<spring:eval expression="@patterns.getProperty('patterns.id')" var="idPattern" />
<spring:eval expression="@patterns.getProperty('patterns.password')" var="pwdPattern" />
<spring:eval expression="@patterns.getProperty('patterns.nickname')" var="nicknamePattern" />

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>
<script src="${root}/resources/js/bootstrap/popper.min.js"></script>
<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">


<!-- font awsome include -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

<!--page css include  -->
<link rel="stylesheet" href="${root }/resources/css/user/settings.css" />
<link rel="stylesheet" href="${root }/resources/css/common/common.css" />

<!--input validation  script-->
<script src="${root}/resources/js/common/inputCheck.js"></script>
<title>개인정보수정</title>
</head>
<body>
	<div class="container">
		<c:if test="${result.checkPasswordResult==true}">

			<!------------------------------userSetting tab -------------------------------->
			<div class=" card  mainCard_size px-1">
				<div class="card-header">개인정보 수정</div>

				<form:form action="${root }/user/updateMemberData.do" method="post" modelAttribute="memberData" id="updateMemberDataForm">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token}" />


					<div class="form-group mt-4 ">
						<label for="regist_pwd" class="v-center c-center bold-font"><span style="color: red;">*</span>비밀번호</label>

						<div class="input-group ">
							<input type="password" class="form-control" maxlength="15" name="member_pwd" id="regist_pwd" />
							<div class="input-group-prepend">
								<button class="btn btn-circle" type="button" data-toggle="popover" data-title="info" tabIndex="-1"
									data-content="영문자,숫자 , 특수문자(!@$%^&*)로 이루어진 7~15자리의 문자열 이어야 합니다.">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">비밀번호 형식에 맞지 않습니다</div>
							<form:errors path="regist_pwd" />
						</div>
					</div>

					<div class="form-group mt-4 ">
						<label for="regist_rePwd" class="v-center c-center bold-font"><span style="color: red;">*</span>비밀번호 재입력</label>

						<div class="input-group ">
							<input type="password" class="form-control" maxlength="15" id="regist_rePwd" name="member_rePwd" />
							<div class="input-group-prepend">
								<button class="btn btn-circle" style="visibility: hidden;" type="button" data-toggle="popover" data-title="info" data-content="" tabIndex="-1">
									<i class="fas fa-question-circle fa-2x"></i>
								</button>
							</div>
							<div class="invalid-feedback">비밀번호가 같지 않습니다.</div>
							<form:errors path="regist_rePwd" />
						</div>
					</div>

					<!-- 	<div class="form-group mt-4 ">
					<label for="settingNickName " class="col-3 col-form-label ">닉네임</label>
					<input type="text" name="settingNickName" class="col-5" placeholder="" />
				</div> -->
					<!-- <div class="form-group row justify-content-center">
								<label for="settingEmail" class="col-3 col-form-label ">이메일</label>
								<input type="email" name="settingEmail" class="col-5" placeholder="" />
							</div> -->

					<div class="form-group mt-4 ">
						<button type="button" class="btn btn-danger btn-sm deleteMember">회원 탈퇴</button>
					</div>

					<div class="form-group row justify-content-center  mt-3">
						<div class="col-3">
							<input type="submit" value="확인" class="btn btn-primary" />
						</div>

						<div class="col-3">
							<input type="button" value="취소" class="btn btn-secondary cancelSetting" />
						</div>
					</div>
				</form:form>
			</div>
			<!------------------------------------------------------------------------->



			<!-- tab setting -->
			<!-- <div class="tab-pane fade" id="tabSetting" role="tabpanel">
					<form action="">
					----------------------------add tab ------------------------------
						<div class="form-group row ">
							<label for="addTab" class="col-3 col-form-label ">탭 추가</label> <input
								type="text" name="addTab" class="col-3" placeholder="" />
							<div class="col-2">
								<button class="btn btn-primary ">추가</button>
							</div>
							<div class="col-2 col-form-label text-left">11/30</div>

						</div>

						<hr class="custom-border">
						----------------------------edit tab ------------------------------
						<div class="form-group row ">
							<label for="editTabFromName" class="col-3 col-form-label ">탭
								이름 변경</label> <select class="custom-select col-3" >
								<option selected>기본</option>
								<option value="1">One</option>
								<option value="2">Two</option>
								<option value="3">Three</option>
							</select> <span><i class="fas fa-arrow-right col-1 to_center"></i></span>
							<input type="text" name="editTabToName" class="col-3"
								placeholder="변경할 탭 이름" />



							<div class="col-2">
								<button class="btn btn-primary ">변경</button>
							</div>
						</div>
						<hr class="custom-border">
						
						----------------------------delete tab ------------------------------
						<div class="form-group row ">
							<label for="editTabFromName" class="col-3 col-form-label ">탭
								삭제</label> <select class="custom-select col-3" >
								<option selected>기본</option>
								<option value="1">One</option>
								<option value="2">Two</option>
								<option value="3">Three</option>
							</select> 

							<div class="col-2">
								<button class="btn btn-primary ">삭제</button>
							</div>
						</div>
						----------------------------ordering tab ------------------------------
						
						
						
						
						----------------------------ordering tab ------------------------------
						
					</form>
				</div> -->

			<form id="deleteMemberForm" action="${root }/user/deleteMember.do"></form>

		</c:if>

		<c:if test="${result.checkPasswordResult==false||result.checkPasswordResult==null }">
			<div class="d-flex justify-content-center  mt-5">
				<div class="card" style="width: 40%;">
					<div class="card-header">
						<strong>개인정보 수정</strong>
					</div>
					<div class="card-body">
						<form action="" method="post">
							<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token}" />
							<div class="input-group input-group">
								<label for="checkPassword" class="sr-only">비밀번호 입력</label>
								<input type="password" class="form-control" id="checkPassword" placeholder="비밀번호 입력" name="password" autofocus />
								<div class="input-group-append">
									<button class="btn btn-primary" type="submit">확인</button>

								</div>
							</div>
							<c:out value="${result.errorMsg}"></c:out>
						</form>
					</div>
				</div>
			</div>

		</c:if>

	</div>
	<script>
		$(function() {
			bindPatternValidation($("#regist_pwd"), new RegExp("${pwdPattern}"));
			rePwdPatternValidation($("#regist_pwd"), $("#regist_rePwd"));

			$(".deleteMember").on("click", function() {
				if (confirm("탈퇴하시겠습니까?"))
					$("#deleteMemberForm").submit();
			});

			$(".cancelSetting").on("click", function() {
				location.href = "${root}/";
			});

			$("[data-toggle='popover']").popover({
				delay : {
					"show" : 100,
					"hide" : 300
				},
				trigger : "click hover",
				placement : "bottom"
			});

			$("#updateMemberDataForm").submit(function() {

				if (!submitValidation()) {
					alert("입력값이 올바르지 않습니다.");
					return false;
				}

			});

		});

		function submitValidation() {
			return validTest($("#regist_pwd"), new RegExp("${pwdPattern}"))
					&& $("#regist_pwd").val() === $("#regist_rePwd").val();

		}
	</script>
</body>
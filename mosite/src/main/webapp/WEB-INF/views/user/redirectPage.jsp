<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<c:set var="root" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>로그인</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="csrf_token" content="${_csrf.token }">
<meta name="csrf_headerName" content="${_csrf.headerName}">

<style>
.message {
	width: 100vw;
	height: 100vh;
	display: flex;
	align-items: center;
	justify-content: center;
	font-size: 1.1rem;
}

.lds-ring {
	display: inline-block;
	position: relative;
	width: 40px;
	height: 40px;
}

.lds-ring div {
	box-sizing: border-box;
	display: block;
	position: absolute;
	width: 32px;
	height: 32px;
	margin: 8px;
	border: 8px solid #0f4c81;
	border-radius: 50%;
	animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
	border-color: #cef transparent transparent transparent;
}

.lds-ring div:nth-child(1) {
	animation-delay: -0.45s;
}

.lds-ring div:nth-child(2) {
	animation-delay: -0.3s;
}

.lds-ring div:nth-child(3) {
	animation-delay: -0.15s;
}

@
keyframes lds-ring { 0% {
	transform: rotate(0deg);
}
100%
{
transform






:



 



rotate






(360
deg




);
}
}
</style>


</head>
<body>
	<div class="message">
		<div>
			redirecting....
			<div class="lds-ring">
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>
	</div>
</body>

<input type="hidden" name="prevPage" value="${sessionScope.prevPage}" />

<form action="${root}/logOut.do" method="post">
	<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }" />
</form>

<c:choose>
	<c:when test="${rememberMe==true}">
		<script>
			window.onload = function() {
				location.href = "/login/rememberMe.do?remember-me=true"
			}
		</script>
	</c:when>

	<c:when test="${logOut==true}">
		<script>
			window.onload = function() {
				document.querySelector("form").submit();
			}
		</script>
	</c:when>
	<c:otherwise>
		<%--
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<script>
				/* window.onload = function() {
					location.href = "/admin/adminMain.do";
				} */
				window.onload = function() {

					const prevPage = document.querySelector("input[name=prevPage]").value;

					location.href = prevPage || "/";
				}
			</script>
		</sec:authorize>
		
		<sec:authorize access="hasRole('ROLE_USER')">

		</sec:authorize>
 		--%>
		<script>
			window.onload = function() {

				const prevPage = document.querySelector("input[name=prevPage]").value;

				location.href = prevPage || "/";
			}
		</script>
	</c:otherwise>

</c:choose>


</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="root" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<title>로그인</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>

<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">
<script src="${root}/resources/js/common/util.js"></script>

<!-- font awsome include -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

<!--page css include  -->
<link rel="stylesheet" href="${root }/resources/css/common/common.css" />


<c:if test="${sessionScope.isMobile==false}">
	<style>
.btn--socialLogin {
	max-width: 100%;
	max-height: 100%
}

#loginCard {
	dislpay: flex;
	height: 100vh;
	justify-content: center;
	align-items: center;
}

#loginCard .card-body {
	height: 80vh;
	padding-left: 0;
	padding-right: 0;
}
</style>
</c:if>
<c:if test="${sessionScope.isMobile==true}">
	<style>
.btn--socialLogin {
	width: 70vmin;
	height: 16vmin;
}
</style>
</c:if>

</head>

<body>
	<c:choose>
		<c:when test="${sessionScope.isMobile==false}">
			<div id="loginCard" class="container d-flex justify-content-center ">

				<div class="card col-sm-12 col-md-5 col-lg-5">

					<div class="card-body d-flex align-items-center justify-content-center">
						<p class="card-text">
						<div class="row no-gutters text-center justify-content-center">
							<div>
								<a href="${root}/"> <img class="main_logo" src="${root}/getIcon?path=TagPAN_Logo.png" alt="" /> <!-- <span class="font-boldAndBig" style="font-size: xxx-large">TAGPAN</span> -->
								</a>
								<%-- <a class=" mositeLogo" href="${root}/"> <img class=" col-12" src="${root}/getIcon?path=boardbasket.gif" alt="" onerror="this.src='https://via.placeholder.com/200x60?TagPan'"/> 
								</a>--%>
								<h1 class="h4 my-3 font-weight-normal">로그인/회원가입</h1>
							</div>

							<div class="col-7 no-gutters">
								<div class="col-12">
									<button data-provider="naver" class="btn btn-light">
										<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/naverLogin.png' alt="네이버로 로그인" />
									</button>
								</div>
								<%-- <div class="col-12">
									<button data-provider="google" class="btn btn-light">
										<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/googleLogin.png' alt="구글로 로그인" />
									</button>
								</div> --%>
								<div class="col-12">
									<button data-provider="kakao" class="btn btn-light">
										<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/kakaoLogin.png' alt="카카오로 로그인" />
									</button>
								</div>
								<div class="col-12"></div>
								<div class="col-12">
									<div id="rememberMe" class="checkbox mt-2">
										<label> <input type="checkbox" value=""> 로그인 유지
										</label>
									</div>
								</div>


							</div>
							<div class="col-7">
								<div class="d-flex flex-column justify-content-center">
									<%--<div class="p-2">
								<a href="">아이디/비밀번호 찾기</a>
							</div>
							 <div class="p-2">
								<a href="${root}/user/signUp.do">회원 가입</a>
							</div> --%>
								</div>
								<!-- <p class="my-2 text-muted"></p> -->
							</div>



							<div class="col-7 justify-content-center">
								<h4 class="div-errorMsg" style="color: red;">${errMsg}</h4>
							</div>
						</div>


					</div>
				</div>
			</div>

		</c:when>
		<c:otherwise>
			<div class="d-flex flex-column justify-content-center align-items-center" style="height: 100vh">
				<div class="text-center">
					<a href="${root}/"> <img class="main_logo" src="${root}/getIcon?path=TagPAN_Logo.png" alt="" /> <!-- <span class="font-boldAndBig" style="font-size: xxx-large">TAGPAN</span> -->
					</a>
					<%-- <a class=" mositeLogo" href="${root}/"> <img class=" col-12" src="${root}/getIcon?path=boardbasket.gif" alt="" />
					</a> --%>
					<h1 class="h4 my-2 font-weight-normal">로그인/회원가입</h1>
				</div>
				<div class="">
					<div>
						<button data-provider="naver" class="btn btn-light">
							<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/naverLogin.png' alt="네이버로 로그인" />
						</button>
					</div>
					<%-- <div>
						<button data-provider="google" class="btn btn-light">
							<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/googleLogin.png' alt="구글로 로그인" />
						</button>
					</div> --%>
					<div>
						<button data-provider="kakao" class="btn btn-light">
							<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/kakaoLogin.png' alt="카카오로 로그인" />
						</button>
					</div>
				</div>
				<div>
					<div id="rememberMe" class="form-check mt-2">
						<input class="form-check-input" type="checkbox" value="">
						<label class="form-check-label" for="defaultCheck1"> 로그인 유지 </label>
					</div>
					<!-- <div id="rememberMe" class="checkbox mt-2">
						<label> <input type="checkbox" value=""> 로그인 유지
						</label>
					</div> -->
				</div>
			</div>
		</c:otherwise>
	</c:choose>



	<script>
		$(function() {
			sessionStorage.clear();
			var msg = "${msg}"

			if (msg.length > 0)
				alert(msg);

			/* $("#password").bind("focusin change keydown", function(event) {

				if ($(window).capslockstate("state") === true) {
					$(".div-errorMsg").text("<Capslock>이 켜져 있습니다.");
				} else {
					$(".div-errorMsg").text("");
				}

			});

			$(window).capslockstate(); */

			$("button[data-provider]").on("click", function() {
				location.href = "/login/social/" + $(this).data("provider") + ($("#rememberMe input").is(":checked") ? "?remember-me=true" : "?remember-me=false");
			})

		});
	</script>


	<div class="row text-center justify-content-center d-none">
		<div class="col-7">
			<a href="${root }/"><img class="my-4" src="https://via.placeholder.com/200x60?text=MOSITE" alt="mosite" /></a>
			<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
		</div>
		<div class="col-7">
			<form class="form-signin" action="${root}/user/login_check.do" method="POST">
				<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token}" />

				<input name="username" type="text" id="username" class="form-control mb-2" placeholder="id" required autofocus>
				<label for="password" class="sr-only">Password</label>
				<input name="password" type="password" id="password" class="form-control mb-2" placeholder="Password" required>

				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			</form>

		</div>
		<div class="col-7 no-gutters">
			<div class="col-12">
				<button data-provider="naver" class="btn btn-light">
					<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/naverLogin.png' alt="네이버로 로그인" />
				</button>
			</div>
			<%-- <div class="col-12">
				<button data-provider="google" class="btn btn-light">
					<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/googleLogin.png' alt="구글로 로그인" />
				</button>
			</div> --%>
			<div class="col-12">
				<button data-provider="kakao" class="btn btn-light">
					<img class="btn--socialLogin" src='${root}/getIcon?path=buttonImage/kakaoLogin.png' alt="카카오로 로그인" />
				</button>
			</div>
			<div class="col-12">
				<div id="rememberMe" class="checkbox mt-2">
					<label> <input type="checkbox" value=""> 로그인 유지
					</label>
				</div>
			</div>
		</div>
		<div class="col-7">
			<div class="d-flex flex-column justify-content-center">
				<div class="p-2">
					<a href="">아이디/비밀번호 찾기</a>
				</div>
				<div class="p-2">
					<a href="${root}/user/signUp.do">회원 가입</a>
				</div>

			</div>
		</div>



		<div class="col-7 justify-content-center">
			<h4 class="div-errorMsg" style="color: red;">${errMsg}</h4>
		</div>
	</div>
</body>



</html>
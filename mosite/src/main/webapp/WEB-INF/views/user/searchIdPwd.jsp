<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="root" value="${pageContext.request.contextPath }" />

<!DOCTYPE html>
<html>
<head>
<title>아이디 비밀번호 찾기</title>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<script src="${root}/resources/js/common/jquery-3.3.1.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
	integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
	crossorigin="anonymous"></script>
<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="${root}/resources/css/bootstrap/bootstrap.min.css">

<!--include page css  -->
<link rel="stylesheet" href="${root}/resources/css/searchIdPwd.css">
<link rel="stylesheet" href="${root}/resources/css/common/common.css">

<!-- font awsome include -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.1.0/css/all.css">

</head>
<body>

	<div class="container ">
		<div class="card-group h-center  mt-5 rounded" id="searchDiv">

			<div class="card ">
				<div class="card-header font-subTitle c-center">
					<h4 class="bold-font">아이디 찾기</h4>
				</div>

				<div class="card-body ">

					<div class="row">
						<label class="col-12 bold-font">가입시 이메일 입력</label>
						<div class="form-group col-12">

							<div class="input-group">
								<input class="form-control" type="text" name="addTab" class=""
									placeholder="이메일을 입력해 주세요" />

								<div class="input-group-append">
									<button class="btn btn-primary ">확인</button>
								</div>
							</div>

						</div>
						<div class="col-12 ">
							<span id="searchIdStatus" class="bold-font color-grey">가입하신
								아이디는 'patayokr' 입니다.</span>
						</div>
					</div>
				</div>
			</div>

			<div class="card  ">
				<div class="card-header font-subTitle c-center">
					<h4 class="bold-font">비밀번호 찾기</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<label class="col-12 bold-font">가입시 이메일 입력</label>

						<div class="form-group col-12">
							<input class="form-control" type="text" name="addTab" class=""
								placeholder="이메일을 입력해 주세요" />
						</div>

						 <label class="col-12 bold-font">가입시 아이디 입력</label>
						<div class="form-group col-12">
							<input class="form-control" type="text" name="addTab" class=""
								placeholder="아이디를 입력해 주세요" />
						</div>
						<div class="col-12 text-center">
							<button class="btn btn-primary btn-block">확인</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>

</html>
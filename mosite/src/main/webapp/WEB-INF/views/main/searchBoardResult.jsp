<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<c:set var="isBoardResultExist" value="${fn:length(result.boardList)>0}"/>
<c:if test="${not empty result.searchData.keyword }">

	<c:choose>

		<c:when test="${result.searchData.search_target=='all' and isBoardResultExist}">		
			<!--통합검색 -->
			
			<h3 class="content__title cursor-pointer" data-toggle="collapse" data-target="#boardListCollapse" aria-expanded="true">
				<span>커뮤니티 검색 결과</span><i class=" fas fa-chevron-up ml-2 content__graph--hide "></i> <i class="fas  fa-chevron-down ml-2 content__graph--show"></i>

				<button class="search__loadMore btn btn-sm btn-primary" type="button" data-type="board">
					더보기 <i class="fa-solid fa-angle-right"></i>
				</button>
			</h3>
			<!-- <button class="search__detailBtn btn btn-sm btn-secondary" type="button" data-toggle="collapse" data-target="#search__detail" aria-expanded="false" aria-controls="search__detail">
								<i class="fas fa-cog"></i>
							</button> -->
		</c:when>
		<c:otherwise>
			<!-- 게시판 검색 -->
			<div class="content__title">
				<h3 class="mr-auto">커뮤니티 검색 결과</h3>
			</div>			
		</c:otherwise>
	</c:choose>
</c:if>
<hr />

<div class="content__trendBoard no-gutters row">
	<c:choose>
		<c:when test="${isBoardResultExist}">
			<div id="boardListCollapse" class="${result.searchData.search_target=='all'?'collapse show':'' } no-gutters">
				<c:if test="${result.searchData!=null}">
					<div class="content__setting col-12">
						<div class="collapse show" id="search__detail">
							<fieldset class="border p-2 mb-1">
								<legend>게시판 검색 설정</legend>
								<div class="d-flex flex-wrap">
									<div class="search__evalItem ">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="order" value="" ${searchData.order==null||searchData.order==''?'checked':''}>
											<label class="form-check-label" for="">없음</label>
										</div>
									</div>
								</div>
								<hr />
								<div class="d-flex flex-wrap">
									<c:forEach var="eval" items="${result.evalList}" varStatus="vs">
										<c:if test="${fn:length(result.evalList)/2 == vs.index }">
											<div class="w-100"></div>
										</c:if>
										<c:set var="checkTarget" value="${eval.eval_code eq  searchData.eval_code }"></c:set>
										<div class="search__evalItem ${eval.eval_sign}">
											<div class="form-check form-check-inline">

												<input id="${eval.eval_code}" class="form-check-input" type="radio" name="order" ${checkTarget?'checked':'' } value="eval" data-eval-code="${eval.eval_code}">
												<label class="form-check-label" for="${eval.eval_code}"><i class=" ${eval.font_awesome_class} ">${eval.eval_name}</i></label>
												<%-- <c:choose>
											<c:when test="${checkTarget && searchData.order=='eval_ASC' }">
												<span><i class="fas fa-sort-up searchBar__sortElement"><input type="hidden" name="order" value="eval_ASC" /></i></span>
											</c:when>
											<c:when test="${checkTarget && searchData.order=='eval_DESC' }">
												<span><i class="fas fa-sort-down searchBar__sortElement"><input type="hidden" name="order" value="eval_DESC" /></i></span>
											</c:when>
										</c:choose> --%>
											</div>
										</div>
										<input class="form-check-input" type="hidden" name="eval_code" ${checkTarget?'checked':'' } value="${eval.eval_code}" disabled>

									</c:forEach>
								</div>
								<hr />
								<div class="d-flex flex-wrap">

									<div class="search__evalItem ">
										<span class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="order" value="comment" ${searchData.order=='comment'?'checked':'' }><label class="form-check-label"
											for="${eval.eval_code}"
										>코멘트 많은 순</label></span>
									</div>
									<div class="search__evalItem ">
										<span class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="order" value="tag" ${searchData.order=='tag'?'checked':'' }><label class="form-check-label" for="${eval.eval_code}">태그
												많은 순</label></span>
									</div>
									<div class="search__evalItem ">
										<span class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="order" value="positive" ${searchData.order=='positive'?'checked':'' }><label class="form-check-label"
											for="${eval.eval_code}"
										>긍정 비율 많은 순</label></span>
									</div>
									<div class="search__evalItem">
										<span class="form-check form-check-inline"> <input class="form-check-input" type="radio" name="order" value="negative" ${searchData.order=='negative'?'checked':'' }><label class="form-check-label"
											for="${eval.eval_code}"
										>부정 비율 많은 순</label></span>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</c:if>

				<c:set var="targetList" value="${result.boardList}" />
				<%@include file="/WEB-INF/views/main/board.jsp"%>

				<c:if test="${searchData!=null  and fn:length(result.boardList)>0 and searchData.search_target!='all'}">
					<div class="container">
						<nav aria-label="Page navigation ">
							<ul class="pagination justify-content-center">
								<c:if test="${result.pageInfo.startPage>1 }">
									<li class="page-item " data-offset="${result.pageInfo.startPage-1 }">
										<button class="page-link">
											<c:choose>
												<c:when test="${sessionScope.isMobile eq true }">
													<i class="fa-solid fa-angles-left"></i>
												</c:when>
												<c:otherwise>
									Prev
								</c:otherwise>
											</c:choose>
										</button>
									</li>
								</c:if>
								<%-- 
						<c:choose>

						<c:when test="${result.searchData.offset eq 1||result.pageInfo.maxPage eq 0}">
							<li class="page-item disabled"><button class="page-link" tabIndex=-1>Prev</button></li>
						</c:when>
						<c:when test="${result.searchData.offset ne 1}">
							<li class="page-item " data-offset="${result.pageInfo.startPage-1<=0?1:result.pageInfo.startPage-1 }"><button class="page-link">Prev</button></li>
						</c:when>
					</c:choose>
					
					--%>
								<c:forEach begin="${result.pageInfo.startPage}" end="${result.pageInfo.endPage}" step="1" varStatus="status">

									<c:choose>
										<c:when test="${result.searchData.offset eq status.index}">
											<li class="page-item active" data-offset="${status.index}"><button class="page-link">${status.index}</button></li>
										</c:when>
										<c:otherwise>
											<li class="page-item " data-offset="${status.index}"><button class="page-link">${status.index}</button></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>

								<c:if test="${result.pageInfo.maxPage!=0 }">
									<c:choose>
										<c:when test="${result.pageInfo.endPage eq result.pageInfo.maxPage}">
											<li class="page-item disabled"><button class="page-link" tabIndex=-1>Next</button></li>
										</c:when>

										<c:when test="${result.searchData.offset ne result.pageInfo.maxPage}">
											<li class="page-item" data-offset="${result.pageInfo.endPage+1}"><button class="page-link" tabIndex=-1>
													<c:choose>
														<c:when test="${sessionScope.isMobile eq true }">
															<i class="fa-solid fa-angles-right"></i>
														</c:when>
														<c:otherwise>
									Next
									</c:otherwise>
													</c:choose>
												</button></li>
										</c:when>
									</c:choose>
								</c:if>
							</ul>
						</nav>
					</div>
				</c:if>
			</div>

		</c:when>
		<c:otherwise>			
			<div class="content__noResult col-12">
				<h4>검색 결과가 없습니다.</h4>
			</div>
		</c:otherwise>
	</c:choose>

</div>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<spring:eval expression="@const.getProperty('const.defaultNickName')" var="defaultNickName" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.comment')" var="commentPattern" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.nickname')" var="nicknamePattern" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.search_keyword_num_limit')" var="searchKeywordLimit" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.auto_complete_min_str_len')" var="autoCompleteMinWordLen" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.select_hashtag_limit')" var="selectHashtagLimit" scope="page" />

<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />

<!DOCTYPE html>
<html>
<title>TagPAN 커뮤니티 동향 , 태그 탐색</title>
<%@include file="/WEB-INF/views/common/head.jsp"%>

<!--google ad script -->
<!-- <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script> -->
<!-- <script data-ad-client="ca-pub-8012502467122218" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> -->


<body class="${sessionIsMobile} ${sessionIsTablet}">
	<%@include file="/WEB-INF/views/common/newHeader.jsp"%>
	<%@include file="/WEB-INF/views/main/timeSet.jsp"%>

	<fmt:parseNumber integerOnly="true" var="keywordNumVal" value="${searchKeywordLimit}" />
	<div class="content ${sessionScope.isMobile?'container-fluid':'container' }">
		<c:set var="sd" value="${result.searchData }" />

		<c:choose>
			<c:when test="${sd!=null }">

				<%-- 사이트 트렌드 태그들(현재 표시 안함) --%>
				<%--<%@include file="/WEB-INF/views/main/siteTrendTag.jsp"%>--%>
				<c:choose>
					<%-- 태그 검색 결과--%>
					<c:when test="${(sd.search_target=='tag')}">
						<%@include file="/WEB-INF/views/main/searchTagResult.jsp"%>
					</c:when>

					<%-- 게시글,뉴스 검색--%>
					<c:when test="${(sd.search_target=='article' )}">
						<%@include file="/WEB-INF/views/main/searchArticleResult.jsp"%>
					</c:when>

					<%-- 게시판 검색 결과--%>
					<c:when test="${(sd.search_target=='board')}">
						<%@include file="/WEB-INF/views/main/searchBoardResult.jsp"%>
					</c:when>


					<%-- 모두 검색--%>
					<c:otherwise>
						<c:choose>
							<c:when test="${ empty result.searchData.board_name  }">
								<div class="content__noResult col-12">
									<h4>검색 결과가 없습니다.</h4>
								</div>
							</c:when>
							<c:otherwise>
								<%@include file="/WEB-INF/views/main/searchTagResult.jsp"%>

								<%@include file="/WEB-INF/views/main/searchArticleResult.jsp"%>

								<%@include file="/WEB-INF/views/main/searchBoardResult.jsp"%>
							</c:otherwise>
						</c:choose>
					</c:otherwise>

				</c:choose>

				<%-- 태그 랭크(compare) --%>
				<%-- <%@include file="/WEB-INF/views/main/tagRankChartOld.jsp"%> --%>
			</c:when>
			<c:otherwise>

				<h3 class="content__title" data-anchor>
					<span class="mr-auto">오늘의 태그 순위</span>
					<!-- <span class="badge badge-primary">보기 모드 : </span>  -->
					<span class="btn-group  btn-group-toggle" data-toggle="buttons"> <label class="btn btn-sm btn-secondary" data-main-graph-type="list"> <input type="radio" checked> <i class="fa-solid fa-list-ol"></i>
					</label> <label class="btn btn-secondary btn-sm active" data-main-graph-type="wordCloud"> <input type="radio"> <i class="fa-solid fa-braille"></i>
					</label>

					</span>

				</h3>

				<%--워드 클라우드 --%>
				<c:set var="simpleMenu" value="${true}" />
				<c:set var="initTagList" value="${result.rankTagList}" />
				<div class="wordCloud " data-type="main_graph">
					<div class="wordCloud__menu" data-type="graphMenu">
						<%@include file="/WEB-INF/views/main/options.jsp"%>
						<c:if test="${initTagList!=null}">
							<div class="wordCloud__initData" data-init>
								<c:forEach var="tag" items="${initTagList}">
									<div class="hashtag" data-f="${tag.f }" data-t="${tag.t }" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w}" data-d="${tag.d}" data-v="${tag.v}"></div>
								</c:forEach>
							</div>
						</c:if>
					</div>
					<div class="wordCloud__content" data-type="graphContent"></div>
				</div>


				<%--순위표--%>
				<%@include file="/WEB-INF/views/main/tagRankList.jsp"%>


				<%-- 즐겨찾기 게시판들--%>
				<h3 class="content__title">
					<span>즐겨찾기 게시판</span>
				</h3>
				<%@include file="/WEB-INF/views/main/favoriteBoard.jsp"%>
			</c:otherwise>
		</c:choose>

	</div>


	<div class="content__naviBox">
		<button class="content__navigator btn btn-secondary" data-direction="up">
			<i class="fa-solid fa-chevron-up fa-xl"></i>
		</button>
		<button class="content__navigator btn btn-secondary" data-direction="down">
			<i class="fa-solid fa-chevron-down fa-xl"></i>
		</button>
	</div>

	<%@include file="/WEB-INF/views/main/modals.jsp"%>
	<%@include file="/WEB-INF/views/common/spinnerNToast.jsp"%>

</body>
<footer>
	<%@include file="/WEB-INF/views/common/footer.jsp"%>
</footer>

</html>

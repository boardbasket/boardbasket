<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:set var="keywordNum" value="${result.searchData.keyword}" />

<h3 class="content__title cursor-pointer" data-toggle="collapse" data-target="#tagInformationCollapse" aria-expanded="true">
	<span>태그 정보</span><i class=" fas fa-chevron-up ml-2 content__graph--hide"></i> <i class="fas  fa-chevron-down ml-2 content__graph--show"></i>
</h3>
<hr />

<div id="tagInformationCollapse" class="collapse show">
	<div id="hashtagRankCollapse" class="content__graph" data-init="${true}">

		<div class="col-12">
			<%@include file="/WEB-INF/views/main/tagRankChart.jsp"%>
		</div>
	</div>
	<hr />
	<div id="tagAnalysisCollapse" class="content__graph" data-init="${true}">
		<div class="tagAnalysis row">
			<div class="tagAnalysis__menu col-12">
				<c:set var="simpleMenu" value="${true}" />
				<%@include file="/WEB-INF/views/main/options.jsp"%>
			</div>

			<c:choose>
				<c:when test="${sessionScope.isMobile eq true }">
					<div id="tagAnalysisCarousel" class="carousel slide" data-interval="false">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<%@include file="/WEB-INF/views/main/radarChart.jsp"%>
							</div>
							<div class="carousel-item">
								<%@include file="/WEB-INF/views/main/boardPercentageChart.jsp"%>
							</div>
							<div class="carousel-item">
								<%@include file="/WEB-INF/views/main/tagInfo.jsp"%>
							</div>
						</div>
						<button class="carousel-control-prev" type="button" data-target="#tagAnalysisCarousel" data-slide="prev">
							<i class="fa-solid fa-chevron-left ml-1"></i>
						</button>
						<button class="carousel-control-next" type="button" data-target="#tagAnalysisCarousel" data-slide="next">
							<i class="fa-solid fa-chevron-right ml-1"></i>
						</button>
					</div>
				</c:when>
				<c:otherwise>
					<div class="tagAnalysis__radarChart col-6 col-lg-6">
						<%@include file="/WEB-INF/views/main/radarChart.jsp"%>
					</div>
					<div class="tagAnalysis__boardPercentageChart col-6 col-lg-6 ">
						<%@include file="/WEB-INF/views/main/boardPercentageChart.jsp"%>
					</div>
					<div class="col-12">
						<%@include file="/WEB-INF/views/main/tagInfo.jsp"%>
					</div>
				</c:otherwise>
			</c:choose>

		</div>
	</div>
	<c:set var="relativeTag" value="${result.relativeTag }" />
	<h4 class="mt-3">
		<span>같이 언급된 태그</span>
	</h4>
	<hr />
	<div id="relativeTagCollapse" class=" content__graph" data-init="${true}">
		<div class="wordCloudChanger col-12">
			<div class="wordCloudChanger__menu">
				<c:set var="simpleMenu" value="${true}" />
				<%@include file="/WEB-INF/views/main/options.jsp"%>
			</div>
			<div class="wordCloudChanger__content">
				<c:forEach begin="1" end="${fn:length(result.searchData.keyword)}" varStatus="i">
					<div class="wordCloudChanger__item content__graph  ${i.first?'show':''}" data-slide="${i.index}">
						<c:choose>
							<c:when test="${i.first}">
								<c:set var="initTagList" value="${relativeTag}" />
							</c:when>
							<c:otherwise>
								<c:set var="initTagList" value="${null}" />
							</c:otherwise>
						</c:choose>
						<c:set var="simpleMenu" value="${true}" />
						<%@include file="/WEB-INF/views/main/wordCloud.jsp"%>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
	<hr />
	<div id="tagLineGraphCollapse" class="content__graph" data-init="${true}">
		<div class="content__searchedTagLineGraph col-12">
			<c:set var="simpleMenu" value="${true}" />
			<c:set var="initTagList" value="${result.searchedTag}" />
			<c:set var="initArticleNumList" value="${result.articleNum}" />
			<%@include file="/WEB-INF/views/main/chart.jsp"%>
		</div>
	</div>
	<%--
<c:set var="keywordHeatMapFlag" value="${true}"></c:set>
<h3 class="content__title ${keywordHeatMapFlag?'cursor-pointer':'text-muted'}" data-toggle="collapse" data-target="#tagHeatMapCollapse" aria-expanded="${sd.offset<=1}">
	<span>태그 히트맵 
	<c:if test="${fn:length(result.searchData.keyword)>keywordNumVal }">
			<span> <i class="fas fa-info-circle fa-xs" data-toggle="tooltip" data-placement="bottom" title="히트맵 그래프는 검색어 ${selectHashtagLimit}개까지 제공됩니다"></i>
			</span>
		</c:if>
	</span>
	<c:if test="${keywordHeatMapFlag}">
		<i class="fas fa-chevron-up ml-1 content__graph--hide"></i>
		<i class="fas fa-chevron-down ml-1 content__graph--show"></i>
	</c:if>
</h3>
<hr />
<c:choose>
	<c:when test="${keywordHeatMapFlag}">
		<div id="tagHeatMapCollapse" class="collapse content__graph  ${sd.offset>1?'':'show'}" data-init="${sd.offset<=1}">
			<div class="content__tagHeatMap col-10">
				<div class="tagHeatMap" data-type="graph">
					<div class="tagHeatMap__menu" data-type="graphMenu">
						<c:set var="simpleMenu" value="${true}" />
						<%@include file="/WEB-INF/views/main/options.jsp"%>
						<c:set var="initTagList" value="${result.heatMapTag}" />
						<c:if test="${result.heatMapTag!=null }">
							<div class="tagHeatMap__initData" data-init>
								<c:forEach var="tag" items="${result.heatMapTag}">
									<div class="hashtag" data-f="${tag.f }" data-t="${tag.t}" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w}" data-d="${tag.d}"></div>
								</c:forEach>
							</div>
						</c:if>
					</div>
					<div class="tagHeatMap__content" data-type="graphContent"></div>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>		
	</c:otherwise>	 
</c:choose>
--%>
</div>

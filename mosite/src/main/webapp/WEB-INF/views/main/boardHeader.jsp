<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="board__header card-header">

	<div class="board__infoBig d-none d-lg-block">
		<div class="board__info row">
			<div class="board__siteName col-3">
				<div>
					<i class="fa-solid fa-house"></i><a href="${item.site_address}" target="_blank">${item.site_name}</a>
				</div>
				<div class="board__boardName" data-board-name="${item.board_name}">
					<input type="hidden" class="board__searchAddress" value="${item.searchAddress}" />
					<i class="fas fa-external-link-alt"></i><a href="${item.board_address}" target="_blank">${item.board_name}</a>
				</div>
			</div>
			<div class="col">
				<i class="fa-solid fa-ranking-star"></i><span>글 갯수 등급</span>
			</div>
			<div class="col">
				<i class="fa-solid fa-comment-dots"></i><span>피드백 갯수</span>
			</div>
			<div class="col">
				<i class="fa-solid fa-chart-pie"></i><span>긍정 부정 비율</span>
			</div>
			<div class="col">
				<i class="fa-solid fa-rotate"></i><span>최근 업데이트</span>
			</div>
			<div class="col-1 align-items-center">
				<sec:authorize access="isAuthenticated()">
					<div class="board__addFavorite" data-user_board_code="${item.user_board_code }" data-board_code="${item.board_code}">
						<button class="btn btn-sm btn-secondary">
							<c:choose>
								<c:when test="${not empty item.user_board_code  }">
									<i class="fas fa-star board__favorite" data-add="false" style="color: gold"></i>
									<i class="far fa-star board__favorite" data-add="true" style="display: none;"></i>
								</c:when>
								<c:otherwise>
									<i class="fas fa-star board__favorite" data-add="false" style="color: gold; display: none;"></i>
									<i class="far fa-star board__favorite" data-add="true"></i>
								</c:otherwise>
							</c:choose>
						</button>
					</div>
				</sec:authorize>
				<c:choose>
					<c:when test="${!empty hideBoardShowButton && hideBoardShowButton}">
						<button class="btn btn-secondary btn-sm board__backBtn">
							<i class="fas fa-arrow-left"></i>
						</button>
					</c:when>
					<c:otherwise>
						<button class="btn btn-primary btn-sm board__contentBtn" data-show="true">
							<i class="fas fa-chevron-down"></i>
						</button>
						<button class="btn btn-secondary btn-sm board__contentBtn board__contentBtn--selected" data-show="false">
							<i class="fas fa-chevron-up" aria-hidden="true"></i>
						</button>
					</c:otherwise>
				</c:choose>

			</div>
		</div>
		<div class="board__description row">
			<div class="col-3">
				<c:forEach var="popularTag" items="${item.popular_hashtag_code_list }">
					<span class="badge badge-primary board__popularTag" data-tag-name="${popularTag.t }"><i class="fas fa-hashtag"></i>${popularTag.t }</span>
				</c:forEach>
			</div>
			<div class="col">
				<div class="d-flex " style="justify-content: space-between;">
					<div class="">
						<c:set var="boardGrade" value=""></c:set>
						<c:forEach var="grade" items="${result.parseGradeList}">
							<c:if test="${item.boardData.parse_frequency_grade==grade.grade }">
								<strong>${item.boardData.parse_frequency_grade }등급</strong>
								<c:set var="boardGrade" value="${item.boardData.parse_frequency_grade}" />
								<c:set var="refreshInterval" value="${grade.parse_cycle_day}" />
							</c:if>
						</c:forEach>
						<c:set var="gradeDesc" value=""></c:set>
						<c:choose>
							<c:when test="${boardGrade==1 }">
								<c:set var="gradeDesc" value="${boardGrade} 등급 : 매일 태그 통계가 제공됩니다." />
							</c:when>
							<c:when test="${boardGrade>1 and boardGrade<=5 }">
								<c:set var="gradeDesc" value="${boardGrade} 등급 : ${refreshInterval}일마다 대표 태그가 갱신됩니다." />
							</c:when>
							<c:otherwise>
								<c:set var="gradeDesc" value="태그 서비스가 제공되지 않습니다." />
							</c:otherwise>
						</c:choose>

						<i class="fa-solid fa-circle-question text-secondary cursor-pointer" data-toggle="modal" data-target="#board_parse_grade_info_modal" data-html="true" data-placement="top" title="${gradeDesc}"></i>
					</div>
				</div>
			</div>
			<div class="col">
				피드백
				<fmt:parseNumber var="commentLength" value="${item.all_comment_list_lenght}" />
				<c:choose>
					<c:when test="${commentLength>10}">${commentLength} 개 </c:when>
					<c:when test="${commentLength<=10 and commentLength>0 }">10 개 이하</c:when>
					<c:otherwise>0 개</c:otherwise>
				</c:choose>
			</div>
			<div class="col">
				<c:set var="noEval" value="${(item.positive+item.negative)==0?true:false }"></c:set>
				<fmt:formatNumber var="positiveWidth" type="percent" value="${noEval?0:(item.positive/(item.positive+item.negative))  }" pattern="0.0%" />
				<fmt:formatNumber var="negativeWidth" type="percent" value="${noEval?0:(item.negative/(item.positive+item.negative))  }" pattern="0.0%" />
				<div class="board__progress progress">
					<div class="progress-bar ${noEval?'bg-secondary':'bg-danger'}" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
						<c:if test="${noEval==true}">
												feedback 없음
											</c:if>
						<div class="board__progressInner--positive" style="width: ${positiveWidth}">${noEval?'':(positiveWidth=='0.0%'?'':positiveWidth)}</div>
						<div class="board__progressInner--negative" style="width: ${negativeWidth}">${noEval?'':(negativeWidth=='0.0%'?'':negativeWidth)}</div>
					</div>
				</div>

			</div>
			<div class="board__lastUpdate  col">
				<span class="text-muted"> ${item.boardData.article_num_update_date} </span>
			</div>
			<div class="col-1"></div>
		</div>
	</div>

	<div class="board__infoSmall d-sm-block d-md-block d-lg-none">
		<div class="board__info row">
			<div class="col">
				<div class="row">
					<div class="col-9">
						<i class="fa-solid fa-house"></i><a href="${item.site_address}">${item.site_name}</a>
					</div>
					<div class="col-3" style="display:flex;">
						<sec:authorize access="isAuthenticated()">
							<div class="board__addFavorite" data-user_board_code="${item.user_board_code }" data-board_code="${item.board_code}">
								<button class="btn btn-sm btn-secondary">
									<c:choose>
										<c:when test="${not empty item.user_board_code  }">
											<i class="fas fa-star board__favorite" data-add="false" style="color: gold"></i>
											<i class="far fa-star board__favorite" data-add="true" style="display: none;"></i>
										</c:when>
										<c:otherwise>
											<i class="fas fa-star board__favorite" data-add="false" style="color: gold; display: none;"></i>
											<i class="far fa-star board__favorite" data-add="true"></i>
										</c:otherwise>
									</c:choose>
								</button>
							</div>
						</sec:authorize>
						<c:choose>
							<c:when test="${!empty hideBoardShowButton && hideBoardShowButton}">
								<button class="btn btn-secondary board__backBtn">
									<i class="fas fa-arrow-left"></i>
								</button>
							</c:when>
							<c:otherwise>
								<button class="btn btn-primary btn-sm board__contentBtn" data-show="true">
									<i class="fas fa-chevron-down"></i>
								</button>
								<button class="btn btn-secondary btn-sm board__contentBtn board__contentBtn--selected" data-show="false">
									<i class="fas fa-chevron-up" aria-hidden="true"></i>
								</button>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="w-100"></div>
				<div class="row">
					<div class="col">
						<div class="board__boardName" data-board-name="${item.board_name}">
							<input type="hidden" class="board__searchAddress" value="${item.searchAddress}" />
							<i class="fas fa-external-link-alt"></i><a href="${item.board_address}" target="_blank" title="${item.board_name}">${item.board_name}</a>
						</div>
					</div>
				</div>
				<div class="w-100"></div>
			</div>
			<div class="w-100"></div>
		</div>
		<div class="board__description col">
			<div class="row">
				<div class="col-6">
					<i class="fa-solid fa-ranking-star"></i>글 갯수 등급
				</div>
				<div class="col-6">
					<i class="fa-solid fa-comment-dots"></i>피드백 갯수
				</div>

			</div>
			<div class="row">
				<div class="col-6">
					<div class="d-flex " style="justify-content: space-between;">
						<div class="">
							<span><strong> ${boardGrade}등급 </strong><i class="fa-solid fa-circle-question text-secondary cursor-pointer" data-toggle="modal" data-target="#board_parse_grade_info_modal" data-html="true" data-placement="top"></i></span>
						</div>
					</div>
				</div>
				<div class="col-6">
					<strong> <c:choose>
							<c:when test="${commentLength>10}">${commentLength} 개 </c:when>
							<c:when test="${commentLength<=10 and commentLength>0 }">10 개 이하</c:when>
							<c:otherwise>0 개</c:otherwise>
						</c:choose>
					</strong>
				</div>
			</div>
			<div class="w-100" style="border: 1px solid lightgrey"></div>
			<div class="row">
				<div class="col-6">
					<i class="fa-solid fa-chart-pie"></i>긍정 부정 비율
				</div>
				<div class="col-6">
					<i class="fa-solid fa-rotate"></i> 최근 업데이트
				</div>

			</div>
			<div class="row">
				<div class="col-6">
					<div class="board__progress progress">
						<div class="progress-bar ${noEval?'bg-secondary':'bg-danger'}" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
							<c:if test="${noEval==true}">
												feedback 없음
											</c:if>
							<div class="board__progressInner--positive" style="width: ${positiveWidth}">${noEval?'':(positiveWidth=='0.0%'?'':positiveWidth)}</div>
							<div class="board__progressInner--negative" style="width: ${negativeWidth}">${noEval?'':(negativeWidth=='0.0%'?'':negativeWidth)}</div>
						</div>
					</div>
				</div>
				<div class="col-6">
					<strong>${item.boardData.board_parse_update_date}</strong>
				</div>
			</div>

		</div>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
.article__content {
	width: 100%;
}

.article__load {
	text-align: center;
}

.collapse .table {
	table-layout: fixed;
}

[id*=articleCollapse] {
	margin-bottom: 5px;
}

.articles__content {
	margin-top: 5px;
	display: none;
}

.article__group {
	margin-top: 1.5rem;
}

.articles__content .collapse {
	overflow-y: auto;
}

.articles__content.show {
	display: block;
}

.articles__article td:nth-child(2) {
	max-width: 200px;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	white-space: nowrap;
}

@media screen and (max-width: 767px) {
	.articles__article td:nth-child(1) {
		width: 0%;
	}
	.articles__article td:nth-child(2) {
		width: 100%;
	}
	.articles__content .collapse {
		max-height: 200px;
	}
}

@media screen and (min-width: 768px) {
	.articles__article td:nth-child(1) {
		width: 20%;
	}
	.articles__article td:nth-child(2) {
		width: 80%;
	}
	.articles__content .collapse {
		max-height: 300px;
	}
}
</style>
<c:set var="noArticleFlag" value="${fn:length(result.communityContentList)<=0 and fn:length(result.rssContent)<=0  }" />

<c:choose>
	<c:when test="${!noArticleFlag}">
		<h3 class="content__title cursor-pointer" data-toggle="collapse" data-target="#articlesCollapse" aria-expanded="true">
			<span>글 &amp; 뉴스</span> <i class=" fas fa-chevron-up ml-2 content__graph--hide"></i> <i class="fas  fa-chevron-down ml-2 content__graph--show"></i>

			<c:if test="${result.searchData.search_target ne 'article' }">
				<button class="search__loadMore btn btn-sm btn-primary" type="button" data-type="article">
					더보기 <i class="fa-solid fa-angle-right"></i>
				</button>
			</c:if>
		</h3>
	</c:when>
	<c:otherwise>
		<h3 class="content__title cursor-pointer" data-toggle="collapse" data-target="#articlesCollapse" aria-expanded="true">
			<span>글 &amp; 뉴스</span>
		</h3>
	</c:otherwise>
</c:choose>
<hr />

<div id="articlesCollapse" class="collapse show">
	<c:choose>
		<c:when test="${noArticleFlag}">
			<div class="content__noResult col-12">
				<h4>검색 결과가 없습니다.</h4>
			</div>
		</c:when>
		<c:otherwise>

			<div class="articles row no-gutters">
				<div class="articles__menu col-12">
					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<c:choose>
							<c:when test="${sessionScope.isMobile eq true }">
								<label class="btn btn-secondary active"> <input type="radio" name="options" data-type="community" checked> 게시글
								</label>
								<label class="btn btn-secondary"> <input type="radio" name="options" data-type="news"> 기사
								</label>
							</c:when>
							<c:otherwise>
								<label class="btn btn-secondary active"> <input type="radio" name="options" data-type="all" checked> 전부
								</label>
								<label class="btn btn-secondary"> <input type="radio" name="options" data-type="community"> 게시글
								</label>
								<label class="btn btn-secondary"> <input type="radio" name="options" data-type="news"> 기사
								</label>
							</c:otherwise>
						</c:choose>

					</div>
				</div>
				<div class="d-none">
					<div class="articles__keyword">
						<c:forEach var="keyword" items="${result.keyword}">
							<div data-keyword="${keyword}"></div>
						</c:forEach>
					</div>
				</div>
				<c:set var="timeMode" value="${(result.searchData!=null?result.searchData.timeMode:result.timeMode)=='year'?'month':result.searchData.timeMode=='month'?'week':'day' }"></c:set>
				<div class="articles__content col ${sessionScope.isMobile eq true ? '':'show' }" data-type="news">
					<h4>뉴스</h4>

					<c:forEach var="entry" items="${result.rssContentList }" varStatus="st">
						<c:if test="${fn:length(entry.value)>0 }">
							<div class="row no-gutters" data-toggle="collapse" data-target="#articleCollapse_${st.index}" aria-expanded="false" aria-controls="rssCollapse_${st.index}">
								<div class="col">
									<h5 class="article__group">${entry.key}</h5>
								</div>
							</div>
							<div id="rssCollapse_${st.index}" class="collapse show">
								<table class="table table-sm">
									<tbody>
										<c:forEach var="article" items="${entry.value }" varStatus="avs">
											<c:if test="${avs.first}">
												<c:set var="year" value="${article.y }" />
												<c:set var="month" value="${article.m }" />
												<c:set var="week" value="${article.w}" />
												<c:set var="day" value="${article.d}" />
											</c:if>
											<tr class="articles__article" data-y="${article.y }" data-m="${article.m }" data-w="${article.w }" data-d="${article.d }">
												<td class="d-none d-md-table-cell" scope="row"><span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${rss.site_address }"><i class="fas fa-external-link-alt"></i></a></span></td>
												<td>
													<div class="row">
														<div class="col-12 short-text">
															<a class="articles__address" href="${article.content_address }" target="_blank">${article.title }</a>
														</div>
														<div class="col d-xs-block d-sm-block d-md-none">
															<span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${rss.site_address }"><i class="fas fa-external-link-alt"></i></a></span>
														</div>
														<div class="col small text-muted">
															<i class="fa-solid fa-calendar fa-xs"></i>${article.d}</div>
													</div>
												</td>
											</tr>
										</c:forEach>
										<tr>
											<td class="article__load" colspan="3"><button class="btn btn-block btn-light" data-type="news" data-board_name="${result.searchData.board_name}" data-site_code="${result.searchData.site_code }" data-year="${year}"
													data-month="${month}" data-week="${week}" data-day="${day}" data-offset="0" data-time-mode="${timeMode}"
												>
													<i class="fa-solid fa-chevron-down"></i>
												</button></td>
										</tr>
									</tbody>
								</table>
								<c:if test="${!st.last}">
									<div class="d-none articles__notLast w-100 text-center">
										<i class="fa-solid fa-2x fa-ellipsis-vertical"></i>
									</div>
								</c:if>
							</div>
						</c:if>

					</c:forEach>
				</div>
				<div class="articles__content col show" data-type="community">
					<h4>게시글</h4>
					<c:forEach var="entry" items="${result.communityContentList }" varStatus="st">
						<c:if test="${fn:length(entry.value)>0 }">
							<div class="row no-gutters" data-toggle="collapse" data-target="#articleCollapse_${st.index}" aria-expanded="false" aria-controls="communityCollapse_${st.index}">
								<div class="col">
									<h5 class="article__group">${entry.key}</h5>
								</div>
							</div>
							<div id="communityCollapse_${st.index}" class="collapse show">
								<table class="table table-sm">
									<tbody>
										<c:forEach var="article" items="${entry.value }" varStatus="avs">
											<c:if test="${avs.first}">
												<c:set var="year" value="${article.y }" />
												<c:set var="month" value="${article.m }" />
												<c:set var="week" value="${article.w}" />
												<c:set var="day" value="${article.d}" />
											</c:if>
											<tr class="articles__article" data-y="${article.y }" data-m="${article.m }" data-w="${article.w }" data-d="${article.d }">
												<td class="d-none d-md-table-cell" scope="row"><span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span></td>
												<td>
													<div class="row">
														<div class="col-12 short-text">
															<a class="articles__address" href="${article.article_address }" target="_blank">${article.title }</a>
														</div>
														<div class="col-12">
															<div class="row small text-muted">
																<div class="col d-xs-block d-sm-block d-md-none">
																	<span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span>
																</div>
																<div class="col">
																	<span>${article.board_name }</span> <a href="${article.board_address}"> <i class="fas fa-external-link-alt fa-xs"></i></a>
																</div>
																<div class="col">
																	<i class="fa-solid fa-thumbs-up fa-xs"></i> ${article.recommend }
																</div>
																<div class="col">
																	<i class="fa-solid fa-eye fa-xs"></i>${article.view_cnt}</div>
																<div class="col">
																	<i class="fa-solid fa-download fa-xs"></i>${article.d}</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</c:forEach>
										<tr>
											<td class="article__load" colspan="5"><button class="btn btn-block btn-light" data-type="community" data-board_name="${result.searchData.board_name}" data-site_code="${result.searchData.site_code }" data-year="${year}"
													data-month="${month}" data-week="${week}" data-day="${day}" data-offset="0" data-time-mode="${timeMode}" data-site_code="${result.searchData.site_code }"
												>
													<i class="fa-solid fa-chevron-down"></i>
												</button></td>
										</tr>
									</tbody>
								</table>
								<c:if test="${!st.last}">
									<div class="d-none articles__notLast w-100 text-center">
										<i class="fa-solid fa-2x fa-ellipsis-vertical"></i>
									</div>
								</c:if>
							</div>
						</c:if>

					</c:forEach>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div id="content__fixedMenu" class="d-none">
	<button class="btn btn-sm btn-secondary rounded-circle">
		<i class="fas fa-cog fa-2x"></i>
	</button>
</div>


<div id="relativeBoardMenu" class="card">
	<div class="tagMenu__header card-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="card-body">
		<div style="display: flex;">
			<div class="relativeBoardMenu__siteName badge">
				<a href="" target="_blank"></a>
			</div>
			<div class="relativeBoardMenu__boardName">
				<a href="" target="_blank"></a>
			</div>
		</div>
		<div>
			<div class="relativeBoardMenu__popularTag"></div>
			<div class="relativeBoardMenu__feedbackNum"></div>
			<div class="relativeBoardMenu__feedbackRatio">
				<div class="board__progress progress">
					<div class="progress-bar" role="progressbar">
						<div class="relativeBoardMenu__noEval">Feedback 없음</div>
						<div class="board__progressInner--positive"></div>
						<div class="board__progressInner--negative"></div>
					</div>
				</div>
			</div>
			<div class="relativeBoardMenu__lastUpdate"></div>

		</div>
	</div>
	<div class="card-footer">
		<div class="relativeBoardMenu__SearchBoard">
			<a href=""></a>
		</div>
	</div>

</div>

<div id="tagMenu" class="tagMenu ${mobileVer} card">
	<div class="tagMenu__header card-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<div class="accordion tagMenu__arccordion" id="tagMenu__arccordion">
		<!-- <div class="card-header" id="tagMenu__communityBtn">
			<h2 class="mb-0">
				
			</h2>
		</div>
		<div class="card-header tagMenu__news" id="tagMenu__newsBtn">
			<h2 class="mb-0">
				
			</h2>
		</div> -->
		<div class="tagMenu__articleCategory">
			<button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#tagMenu__communityCollapse">커뮤니티 글</button>
			<button class="btn btn-link" disabled> / </button>
			<button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#tagMenu__newsCollapse">뉴스</button>
		</div>
		<div class="card">

			<div id="tagMenu__newsCollapse" class=" collapse" data-parent="#tagMenu__arccordion" data-type="news" data-offset="0">
				<div id="tagMenu__newsContent" class="scrollbar-dynamic">
					<div class="tagMenu__articles" data-type="news">
						<table class='table' style='table-layout: fixed'>
							<tbody></tbody>
						</table>
					</div>
					<div class="tagMenu__loadMore" data-type="news">더보기</div>
					<div class="tagMenu__lastPage" data-type="news">마지막 페이지 입니다.</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div id="tagMenu__communityCollapse" class="collapse" data-parent="#tagMenu__arccordion" data-type="community" data-offset="0">
				<div id="tagMenu__communityContent" class="scrollbar-dynamic">
					<div class="tagMenu__articles" data-type="community" data-offset="0">
						<table class='table' style='table-layout: fixed'>
							<tbody></tbody>
						</table>
					</div>
					<div class="tagMenu__loadMore" data-type="community">더보기</div>
					<div class="tagMenu__lastPage" data-type="community">마지막 페이지 입니다.</div>
				</div>
			</div>
		</div>
	</div>



	<ul class="list-group list-group-flush border-dark">
		<li class="list-group-item tagMenu__menu tagMenu__searchTimeSet"><a class="" href="#" target="_blank"> <span class="tagMenu__timeSet"> </span> '<span class="tagMenu__tagName"></span>' 데이터 불러오기
		</a></li>
		<li class="list-group-item tagMenu__menu tagMenu__searchBoardAddr"><a class="" href="#" target="_blank">'<span class="tagMenu__tagName" data-target="board"></span>'을(를)'<span class="tagMenu__boardName"> </span>' 에서 검색 <i
				class="fas fa-external-link-alt"
			></i></a></li>
		<li class="list-group-item tagMenu__menu tagMenu__searchSubmit"><a class="" href="#">'<span class="tagMenu__tagName" data-target="site"></span>'을(를)'<span>Tagpan</span>' 에서 검색 <i class="fas fa-search"></i></a></li>
		<!-- <li class="list-group-item tagMenu__menu tagMenu__searchRelativeTag"><a class="" href="#">'<span class="tagMenu__tagName" data-target="relative"></span>와(과) 같이 언급된 태그' 검색 <i class="fas fa-search"></i></a></li> -->
	</ul>
</div>


<div class="modal  fade " id="chartModal" data-Obj="" tabindex="-1" role="dialog" aria-labelledby="chartModal" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content ">
			<div class="modal-header d-none">
				<h5 class="modal-title" id="">크게 보기</h5>

			</div>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true"><i class="fa-solid fa-xmark"></i></span>
			</button>

			<div class="modal-body"></div>
		</div>
	</div>
</div>

<input type="hidden" name="commentLoadNum" value="${commentLoadNum}" />
<div class="modal fade" id="evalCountModal" data-Obj="" tabindex="-1" role="dialog" aria-labelledby="evalCountModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="">FEEDBACK 통계</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="evalCountModal__legend">
					<ul class="horizontal-list"></ul>
				</div>
				<canvas id="evalCountModal__graph"></canvas>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="siteInfo">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header bg-info">
				<h5 class="modal-title modal-tagPanInfo_h5-title text-white">Tagpan?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<img class="main_logo--mini" src="${root}/getIcon?path=TagPAN_Logo.png" alt="" />
				<div class="d-flex justify-content-center flex-column">
					<div class="mt-2">커뮤니티 동향 , 태그 탐색</div>
					<div>
						<a href="mailto:patayokr@gmail.com?subject=[Tagpan 문의]" target="_blank">이메일 문의</a>&amp; <a href="mailto:patayokr@gmail.com?subject=[채용]" target="_blank">채용 문의</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="hashtagModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">해시태그 추가</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<input class="form-control hashtagModal__input" type="text" maxlength="10" />

				<div class="text-muted">
					<i class="fas fa-info-circle"></i>게시판당 하루 3개 추가할 수 있습니다.
				</div>
				<div class="text-muted">
					<i class="fas fa-info-circle"></i>동일한 태그는 추가할 수 없습니다.
				</div>
				<div class="text-muted">
					<i class="fas fa-info-circle"></i>추가된 태그는 새로고침 후 적용됩니다.
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="board_parse_grade_info_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">글 갯수 등급?</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div>
					<strong>'TagPAN'</strong> 에서는 <strong>각 사이트를 기준</strong>으로 해당 게시판에서
				</div>
				<div>
					일정 기간동안 작성되는 <strong>게시글 수</strong>를 이용해 등급을 나누어 분석합니다.
				</div>
				<br />
				<ul>
					<c:forEach var="grade" items="${result.parseGradeList}">

						<fmt:formatNumber var="percent" value="${grade.ratio_max*100}" minFractionDigits="0" maxFractionDigits="0" />

						<c:if test="${percent>0}">

							<li><strong>${grade.grade }등급</strong>:<c:choose>
									<c:when test="${grade.parse_cycle_day==1 }">
									게시글수 <strong>상위 ${percent}%</strong>, <strong>매일</strong> 일정 시간마다 분석합니다.
								</c:when>
									<c:when test="${grade.parse_cycle_day==0 }">
									분석하지 않습니다.
								</c:when>
									<c:otherwise>
									게시글수 <strong>상위 ${percent}%</strong>, <strong>${grade.parse_cycle_day }일</strong> 마다 한번 분석합니다.
								</c:otherwise>
								</c:choose></li>
						</c:if>
					</c:forEach>
				</ul>
			</div>

			<div class="modal-footer">
				<div class="font-small text-muted">※해당 사이트 게시판의 갯수에 따라 비율이 변경될 수 있습니다.</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="deleteMember">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<h5 class="modal-title">회원 탈퇴</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<div>
					<span class="text-danger">회원 탈퇴</span> 를 하면 <strong>즐겨찾기</strong>,<strong>댓글</strong>등이 <strong>영구적으로 삭제</strong>됩니다
				</div>
				<div>
					진행하시겠습니까?<i class="fa-solid fa-face-sad-tear"></i>
				</div>
			</div>

			<div class="modal-footer">
				<form id="form-deleteMember" action="${root}/user/deleteMember.do" method="post">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token}" />
					<button class="btn btn-danger" type="submit" tabindex="-1">회원 탈퇴</button>
				</form>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>
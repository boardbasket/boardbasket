<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="tagList" value="${result.rankTagList}" />
<fmt:formatNumber var="loopLength" value="${tagList.size()/10-1}" minFractionDigits="0" maxFractionDigits="0" />

<fmt:formatNumber var="indicatorNum" value="${tagList.size()/10/3}" minFractionDigits="0" maxFractionDigits="0" />

<c:if test="${loopLength>0 }">

	<div class="tagRankList show" data-type="main_graph">

		<div class="tagRankList__inner">
			<c:forEach begin="0" end="${loopLength}" varStatus="outerStatus">
				<div class="tagRankList__item">
					<ul class="list-group list-group-flush ${outerStatus.first?'active':''}">
						<c:set var="innerLoopBase" value="${outerStatus.index*10 }" />
						<c:set var="innerLoopMaxVal" value="${(outerStatus.index+1)*10-1>tagList.size()?tagList.size()-1:(outerStatus.index+1)*10-1 }" />
						<c:forEach begin="${innerLoopBase}" end="${innerLoopMaxVal}" varStatus="innerStatus">
							<c:set var="tag" value="${tagList[innerStatus.index]}" />
							<c:set var="rankDiff" value="${tag.fromTime_rank-tag.toTime_rank}" />							
							<li class="list-group-item" data-tag-name="${tag.t }">
								<div class="col">
									<strong>${innerLoopBase+innerStatus.count }</strong>
								</div>
								<div class="col">
									<span class="badge badge-primary tagRankList__tag" data-from-time-rank="${tag.fromTime_rank}" data-to-time-rank="${tag.toTime_rank}" data-tag-name="${tag.t}" data-direction="${tag.fromTime_rank==null?0:rankDiff>0?1:rankDiff<0?-1:0}">${tag.t }</span>
								</div>
								<div class="col">
									<c:choose>
										<c:when test="${tag.fromTime_rank==null }">
											<span class="badge bg-warning">new</span>
										</c:when>
										<c:when test="${rankDiff>0}">
											<i class="fas fa-arrow-up">${rankDiff }</i>
										</c:when>
										<c:when test="${rankDiff<0 }">
											<i class="fas fa-arrow-down">${rankDiff*-1 }</i>
										</c:when>
										<c:otherwise>
											<i class="fas fa-minus"></i>
										</c:otherwise>
									</c:choose>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
			</c:forEach>
		</div>

		<ol class="tagRankList__indicators">
			<%-- <c:forEach begin="0" end="${indicatorNum}" varStatus="indicatorStatus">
				<li data-slide-to="${indicatorStatus.index}" class="${indicatorStatus.first?'active':''} "><i class="fa-solid fa-circle"></i></li>
			</c:forEach> --%>
		</ol>

		<%-- <c:if test="${sessionScope.isMobile ne true }">
			<button class="carousel-control-prev" type="button" data-target="#tagRanking__tagCarousel" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-target="#tagRanking__tagCarousel" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span>
			</button>
		</c:if>
 --%>
	</div>
</c:if>
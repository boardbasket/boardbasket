<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@serverConst.getProperty('server_const.comment_load_num')" var="commentLoadNum" scope="page" />

<%-- <c:set var="item" value="${result.board}"  /> --%>

<%-- <c:set value="${commentLoadNum}"></c:set> --%>

<table class="table commentTable" data-board-code="${item.board_code }">
	<fmt:parseNumber var="commentLength" value="${item.all_comment_list_lenght}" />

	<caption>
		<button class="btn btn-sm btn-secondary commentTable___showDoughnutGraph">
			<i class="fas fa-chart-pie"></i>
		</button>
	</caption>
	<thead class="commentTable__head">
		<tr>
			<th scope="col">작성자</th>
			<th scope="col">
				<!-- <span>작성일</span> -->
				<div class="commentTable__sortWrapper">
					<div class=" commentTable__sortElement commentTable__sortElement--selected">
						<i class="fas fa-sort"></i>
					</div>
					<div class=" commentTable__sortElement" data-order-by="insert_date" data-order-val="ASC">
						<i class="fas fa-sort-up"></i>
					</div>
					<div class="commentTable__sortElement" data-order-by="insert_date" data-order-val="DESC">
						<i class="fas fa-sort-down"></i>
					</div>
				</div>
			</th>
			<th scope="col">한마디</th>
			<th scope="col">
				<div class="commentTable__sortWrapper">
					<div class=" commentTable__sortElement commentTable__sortElement--selected">
						<i class="fas fa-sort"></i>
					</div>
					<div class=" commentTable__sortElement" data-order-by="up_vote" data-order-val="ASC">
						<i class="fas fa-sort-up"></i>
					</div>
					<div class="commentTable__sortElement" data-order-by="up_vote" data-order-val="DESC">
						<i class="fas fa-sort-down"></i>
					</div>
				</div>
			</th>
			<th scope="col">
				<div class="commentTable__sortWrapper">
					<div class=" commentTable__sortElement commentTable__sortElement--selected">
						<i class="fas fa-sort"></i>
					</div>
					<div class=" commentTable__sortElement" data-order-by="down_vote" data-order-val="ASC">
						<i class="fas fa-sort-up"></i>
					</div>
					<div class="commentTable__sortElement" data-order-by="down_vote" data-order-val="DESC">
						<i class="fas fa-sort-down"></i>
					</div>
				</div>
			</th>
		</tr>
	</thead>
	<tbody>
		<c:set var="myComment" value="${item.my_comment }" />

		<tr class="comment comment--my ${empty myComment ? 'd-none':'' }" data-comment-code="${empty myComment? '' : myComment.comment_code}">
			<td>
				<div class="comment__writer" data-writer="${myComment.writer}">${myComment.writer }</div>
				<div class="w-100 comment__divider"></div>
				<div class="font-small text-muted">(최근 작성)</div>
			</td>
			<%-- <c:set var="year" value="${myComment.insert_date.year+1900}" />
			<fmt:formatNumber var="month" type="number" minIntegerDigits="2" value="${myComment.insert_date.month+1}" />
			<fmt:formatNumber var="day" type="number" minIntegerDigits="2" value="${myComment.insert_date.date}" /> --%>

			<td class="comment__insertDate" data-insert-date="${myComment.insert_date}">${myComment.insert_date}</td>
			<td class=""><div class="comment__userComment" data-comment-content="${myComment.user_comment}">${myComment.user_comment }</div>
				<div class="w-100"></div> <c:set var="evalJoinStr" value=" "></c:set> <c:forEach var="eval" items="${myComment.comment_eval_list}">
					<c:set var="evalJoinStr" value="${evalJoinStr} ${eval.eval_code}"></c:set>
				</c:forEach> <c:forEach var="allEval" items="${result.evalList}">
					<span class="badge ${allEval.eval_sign} ${fn:indexOf(evalJoinStr,allEval.eval_code)>=0?'':' d-none'} comment__evalItem--selected" data-eval-code="${allEval.eval_code}"><i class="${allEval.font_awesome_class}"></i>${allEval.eval_name}</span>
				</c:forEach></td>
			<td>
				<button class="btn btn-sm btn-primary comment__upVote" disabled data-vote-value="1" data-up-vote="${myComment.up_vote }">
					<i class="far fa-thumbs-up"> ${myComment.up_vote}</i>
				</button>
			</td>
			<td>
				<button class="btn btn-sm btn-secondary comment__downVote" disabled data-vote-value="-1" data-down-vote="${myComment.down_vote }">
					<i class="far fa-thumbs-down"> ${myComment.down_vote}</i>
				</button>
			</td>

		</tr>

		<c:forEach var="comment" items="${item.board_comment_list }" varStatus="commentStatus">
			<tr class="comment ${comment.my_comment eq true ? 'board__comment--my':''} ${commentStatus.count>(commentLoadNum/3)? 'd-none':''}" data-comment-code="${comment.comment_code}">
				<td>
					<div class="comment__writer" data-writer="${comment.writer}">${comment.writer}</div>
				</td>
				<%-- <c:set var="year" value="${comment.insert_date.year+1900}" />
				<fmt:formatNumber var="month" type="number" minIntegerDigits="2" value="${comment.insert_date.month+1}" />
				<fmt:formatNumber var="day" type="number" minIntegerDigits="2" value="${comment.insert_date.date}" /> --%>
				<td class="comment__insertDate" data-insert-date="${comment.insert_date}">${comment.insert_date}</td>
				<td class="">
					<div class=" comment__userComment" data-comment-content="${comment.user_comment }">${comment.user_comment }</div>
					<div class="w-100 comment__divider"></div>
					<div>
						<c:forEach var="eval" items="${comment.comment_eval_list}">
							<span class="badge ${eval.eval_sign} comment__evalItem--selected"><i class="${eval.font_awesome_class}"></i>${eval.eval_name}</span>
						</c:forEach>
					</div>
				</td>
				<td>
					<button class="btn btn-sm btn-primary comment__upVote" data-vote-value="1" data-up-vote="${comment.up_vote}">
						<i class="far fa-thumbs-up"> ${comment.up_vote}</i>
					</button>

				</td>
				<td>
					<button class="btn btn-sm btn-secondary comment__downVote" data-vote-value="-1" data-down-vote="${comment.down_vote}">
						<i class="far fa-thumbs-down"> ${comment.down_vote}</i>
					</button>
				</td>
			</tr>
		</c:forEach>

		<c:if test="${(item.board_comment_list==null or empty item.board_comment_list) and (item.my_comment==null or empty item.my_comment)}">
			<tr class="comment comment--empty">
				<td colspan="5">작성된 코멘트가 없습니다.</td>
			</tr>
		</c:if>

		<tr>
			<td class="justify-content-center comment__pagination pagination" colspan="5">
				<%-- <c:if test="${commentLength>=(commentLoadNum/3) }">
					<button class="btn btn-primary comment__showMoreComment" data-review-page="${commentLength>commentLoadNum?'true':'false'}">더 보기</button>
				</c:if></td> --%>
		</tr>
	</tbody>
</table>
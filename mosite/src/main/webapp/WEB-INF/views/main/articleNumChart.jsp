<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<spring:eval expression="@serverConst.getProperty('server_const.article_no_parse_one_cycle_period')" var="cycleInterval" scope="page" />
<div class="articleNumChart">
	<div data-init>
		<div data-num="${item.boardData.four_cycle_ago }" data-date="${item.boardData.update_date_four_cycle_ago}" data-order="1"></div>
		<div data-num="${item.boardData.three_cycle_ago}" data-date="${item.boardData.update_date_three_cycle_ago }" data-order="2"></div>
		<div data-num="${item.boardData.two_cycle_ago }" data-date="${item.boardData.update_date_two_cycle_ago}" data-order="3"></div>
		<div data-num="${item.boardData.one_cycle_ago }" data-date="${item.boardData.update_date_one_cycle_ago }" data-order="4"></div>
		<div data-num="${item.boardData.current_cycle }" data-date="${item.boardData.article_num_update_date }" data-order="5"></div>
		<div data-update_date="${item.boardData.article_num_update_date }"></div>
	</div>
	<div class="articleNumChart__generalOption" data-cycle-interval="${cycleInterval}" data-regular-article-no-flag="${item.boardData.regular_article_no_flag }"></div>
	<div class="articleNumChart__content" data-type="graphContent">
		<canvas></canvas>
	</div>
	<div class="articleNumChart__desc row">
		<c:choose>
			<c:when test="${item.boardData.current_cycle!=null}">
				<div class="articleNumChart__compare col-6">
					글 갯수가 ${cycleInterval}일 전에 비해 <span class="articleNumChart__value"></span>
				</div>
				<div class="articleNumChart__average  col-6">
					<c:set var="averageTitle" value="${cycleInterval==7?'일':cycleInterval}${cycleInterval==7?'주일':'일' }" />
					하루 평균 <span class="articleNumChart__value"></span> 개의 게시물 등록
				</div>
			</c:when>
		</c:choose>
	</div>
</div>
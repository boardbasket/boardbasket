<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="position: relative;">
	<div class="content__noData">
		- 데이터가 없거나 부족합니다 <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="검색어를 변경하거나 검색 기간을 ${(timeMode=='week'||timeMode=='month')?'넓은 범위로':'다른 시간대로' } 바꾸어보세요"></i>
	</div>
</div>

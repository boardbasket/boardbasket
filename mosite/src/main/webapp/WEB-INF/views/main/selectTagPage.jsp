<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />

<!DOCTYPE html>
<html>
<head>
<title>TagPan 커뮤니티 동향 , 태그 탐색</title>

<!-- <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0, user-scalable=false"> -->
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!--bootstrap css include  -->
<link rel="stylesheet" href="${root}/resources/css/bootstrap/bootstrap.min.css">
<!-- bootstrap / popper.js / jquery include -->
<script src="${root}/resources/js/common/jquery-3.3.1.min.js"></script>


<script src="${root}/resources/js/bootstrap/popper.min.js"></script>

<script src="${root}/resources/js/bootstrap/bootstrap.min.js"></script>


<link rel="stylesheet" href="${root}/resources/css/jqueryUI/jquery-ui.min.css">
<script src="${root}/resources/js/jqueryUI/jquery-ui.min.js"></script>
<script src="${root}/resources/js/jqueryUI/jquery.ui.touch-punch.min.js"></script>


<!-- font awsome include -->
<script src="https://kit.fontawesome.com/a15ac20069.js" crossorigin="anonymous"></script>
<%-- <link href="${root }/resources/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet"> --%>

<!-- common css -->
<link rel="stylesheet" href="${root }/resources/css/common/common.css" />
</head>
<!--loading placeholder css  -->
<link rel="stylesheet" href="${root}/resources/css/common/placeholder-loading.css" />

<!-- mainPage_content css -->
<link rel="stylesheet" href="${root}/resources/css/mainPage/mainPage_content.css" />

<!-- header css -->
<link rel="stylesheet" href="${root }/resources/css/common/headerBar.css" />

<!-- login page css -->
<link rel="stylesheet" href="${root}/resources/css/common/user.css" />


<!--input validation-->
<script src="${root}/resources/js/common/inputCheck.js"></script>

<!--custom scroll css  -->
<link rel="stylesheet" href="${root }/resources/css/common/scrollBar.css" />
<script src="${root}/resources/js/common/jquery.scrollbar.js"></script>

<!--jquery ajax cors plugin  -->
<%-- <script src="${root}/resources/js/common/jquery.ajax-cross-origin.min.js"></script> --%>
<!--google ad script -->
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>

<meta name="csrf_token" content="${_csrf.token }">
<meta name="csrf_headerName" content="${_csrf.headerName}">

<script src="${root}/resources/js/common/util.js"></script>
<script src="${root}/resources/js/common/lodash.js"></script>
<!--slick  -->
<script src="${root}/resources/slick/slick.min.js"></script>
<link rel="stylesheet" href="${root }/resources/slick/slick.css" />
<link rel="stylesheet" href="${root }/resources/slick/slick-theme.css" />

<link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon">



</head>
<body>
	


</body>

<script src="${root}/resources/js/common/util.js"></script>
</html>



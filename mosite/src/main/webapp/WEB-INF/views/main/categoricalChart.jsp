<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="categoricalChart">

	<div data-init>
		<c:set var="sumOfValue" value="0" />
		<c:forEach var="category" items="${item.board_category_list }">
			<c:set var="sumOfValue" value="${sumOfValue+category.val }"></c:set>
		</c:forEach>
		<c:if test="${sumOfValue>100 }">
			<c:forEach var="category" items="${item.board_category_list }">
				<div data-category_name="${category.category_name }" data-val="${category.val }"></div>
			</c:forEach>
		</c:if>
	</div>

	<div class="categoricalChart__content" data-type="graphContent">
		<canvas></canvas>
	</div>

</div>
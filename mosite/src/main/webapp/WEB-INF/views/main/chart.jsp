<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@const.getProperty('const.maxChartTagSelect')" var="maxChartTagSelect" scope="page" />

<div class="chart" data-type="graph">
	<div class="chartObj__menu" data-type="graphMenu">
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<c:if test="${initTagList!=null }">
			<div class="chart__initData" data-init>
				<c:forEach var="tag" items="${initTagList}">
					<div class="hashtag" data-f="${tag.f }" data-t="${tag.t }" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w}" data-d="${tag.d}"></div>
				</c:forEach>
				<c:forEach var="tag" items="${initArticleNumList}">
					<div class="articleNum" data-v="${tag.v }" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w}" data-d="${tag.d}"></div>
				</c:forEach>
			</div>

		</c:if>
	</div>
	<div class="chart__content" data-type="graphContent">
		<canvas></canvas>
	</div>
	<%--<div class="chart__htmlTooltip">
		<table>
		</table>
	</div>--%>
</div>
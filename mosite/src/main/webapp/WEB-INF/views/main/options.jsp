<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="option no-gutters" data-simple-menu="${simpleMenu}">
	<c:set var="selectedSite" value="${result.searchData!=null?(result.searchData.site_code):'' }"></c:set>
	<c:set var="keywordList"></c:set>
	<c:set var="siteList" value="${result.siteList }"></c:set>
	<div class="option__sites row option__sites--disabled no-gutters">
		<div class="d-flex">
			<c:if test="${!simpleMenu||selectedSite==null}">
				<div class="badge badge-info option__site ${empty selectedSite?'option__site--selected':'' } ">사이트 전체</div>
			</c:if>
		</div>
		<div class="d-flex">
			<c:forEach var="site" items="${siteList}">
				<c:choose>
					<c:when test="${simpleMenu}">
						<c:if test="${selectedSite==site.site_code}">
							<div class="badge  option__site option__site--selected" data-site-code="${site.site_code}" style="background:${site.site_color}">${site.site_name }</div>
						</c:if>
					</c:when>
					<c:otherwise>
						<div class="badge  option__site ${selectedSite==site.site_code?'option__site--selected':'' }" data-site-code="${site.site_code}" style="background:${site.site_color}">${site.site_name }</div>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</div>
	<div class="w-100"></div>
	<div class="option__times ${sessionIsMobile} row no-gutters" data-simple-menu="${simpleMenu}">
		<div class="col-12">
			<%@include file="/WEB-INF/views/main/timeMenu.jsp"%>
		</div>
	</div>

	<div class="w-100"></div>

	<div class="option__limitWrapper col-12">
		<div class="badge badge-secondary option__limit option__limit--selected" data-limit="10">
			상위 10개 <i class="fas fa-sync-alt"></i>
		</div>
		<div class="badge badge-secondary option__limit" data-limit="20">
			상위 20개 <i class="fas fa-sync-alt"></i>
		</div>
		<div class="badge badge-secondary option__limit" data-limit="${selectHashtagLimit}">
			상위 ${selectHashtagLimit}개 <i class="fas fa-sync-alt"></i>
		</div>
	</div>

	<div class="w-100"></div>
	<div class="option__tagWrapper">
		<div class="col-10 scrollbar-inner"></div>
		<!-- <div class="col-2 ">
			<button type="button" class="close btn btn-sm option__resetTag" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div> -->
	</div>


	<div class="option__wordCloud col-12">
		<input type="range" class="option__wordCloudZoom form-control-range  p-0">
	</div>
	
	<c:if test="${simpleMenu}">
		<div class="option__hoverMenuWrapper ${sessionScope.isMobile eq false?'':'d-none'}">
			<button class="btn btn-secondary btn-sm option__showModal">
				<i class="fas fa-expand-alt"></i>
			</button>
		</div>
	</c:if>

</div>
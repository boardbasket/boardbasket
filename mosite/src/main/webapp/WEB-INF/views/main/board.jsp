<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:forEach var="item" items="${targetList}" varStatus="status">
	<fmt:parseNumber var="commentLength" value="${item.all_comment_list_lenght}" />

	<div class="board col-12" id="${item.board_code}">
		<div class="card ">
			<%@include file="/WEB-INF/views/main/boardHeader.jsp"%>
			<div class="board__content row ">
				<div class="board__generalOption">
					<div data-search-available="${item.search_available=='Y'}"></div>
				</div>
				<div class="col-12">
					<h3>게시판 정보</h3>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" data-content-height="small">
					<%@include file="/WEB-INF/views/main/articleNumChart.jsp"%>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" data-content-height="small">
					<%@include file="/WEB-INF/views/main/categoricalChart.jsp"%>
				</div>
				<div class="col-12" data-content-height="small">
					<h4>게시글 샘플</h4>
					<div class="board__articleSample">
						<table class="table table-sm table-striped" style="table-layout: fixed">
							<tbody>
								<tr>
									<td><button class="board__loadMoreSample btn btn-secondary" data-offset="1">더 불러오기</button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<h3 class="col-12">태그 정보</h3>
				<div class="col-12" data-content="chart" data-content-height="default">
					<c:set var="simpleMenu" value="${true}" />
					<%@include file="/WEB-INF/views/main/chart.jsp"%>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" data-content="cloud" data-content-height="default">

					<%@include file="/WEB-INF/views/main/wordCloud.jsp"%>
				</div>

				<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" data-content="force" data-content-height="default">

					<div class="forceLayout" data-type="graph">
						<div class="forceLayout__menu d-none" data-type="graphMenu">
							<%@include file="/WEB-INF/views/main/options.jsp"%>
						</div>
						<div class="forceLayout__content" data-type="graphContent"></div>
					</div>
				</div>
				<div class="col-12" data-content="comment" data-content-height="default">
					<div class="board__userOpinion">

						<div class="board__commentWrapper">
							<%@include file="/WEB-INF/views/main/commentTable.jsp"%>
						</div>

						<c:set var="codeJoinStr" value=" "></c:set>

						<c:forEach var="myCommentEval" items="${item.my_comment.comment_eval_list}">
							<c:set var="codeJoinStr" value="${codeJoinStr} ${myCommentEval.eval_code}"></c:set>
						</c:forEach>

						<div class="board__evalWrapper">
							<form class="board__commentForm">
								<div class="form-row">
									<input type="hidden" name="board_code" value="${item.board_code}" />
									<div class="col-2">
										<div class="input-group">
											<input name="writer" type="text" class="form-control " placeholder="닉네임" value="${defaultNickName}" maxlength="10" required pattern="${nicknamePattern}">
											<div class="invalid-feedback">닉네임은 한글,영어,숫자로 이루어진 2~10글자여야 합니다.</div>
										</div>
									</div>
									<div class="col-8">
										<input name="user_comment" type="text" class="form-control" placeholder="하루 한번씩 등록가능" pattern="${commentPattern}" maxlength="200" autocomplete="off">
										<div class="invalid-feedback">영어,숫자,한글 특문'!#$%&^*()@'으로 구성된 200글자 이하여야 합니다.</div>
									</div>
									<div class="col-2 d-flex align-items-center">
										<button class="btn btn-secondary board__insertComment" type="button" disabled>
											<span class="d-block d-md-none d-lg-none"><i class="fas fa-pen"></i></span> <span class="d-none d-md-block d-lg-block">등록</span>
										</button>
									</div>
								</div>
							</form>

							<fieldset>
								<legend>카테고리</legend>
								<div class="container">
									<c:set var="half" value="${result.evalList.size()/2 -1}"></c:set>

									<div class="row">
										<c:forEach begin="0" end="${half}" var="eval" items="${result.evalList}">
											<c:set var="evalCount" value="0"></c:set>

											<c:forEach var="boardEval" items="${item.board_evaluation}">
												<c:if test="${eval.eval_code== boardEval.eval_code }">
													<c:set var="evalCount" value="${boardEval.count}"></c:set>
												</c:if>
											</c:forEach>
											<button type="button" class="btn board__evalItem ${fn:indexOf(codeJoinStr,eval.eval_code)>=0 ? 'board__evalItem--selected ':''} ${eval.eval_sign} col-4 col-md-3 col-lg-2" data-eval-code="${eval.eval_code}"
												data-selected="${fn:indexOf(codeJoinStr,eval.eval_code)>=0 ?'true':'false'}"
											>
												<i class="fa-2x ${eval.font_awesome_class}"></i><span>${eval.eval_name}</span>
											</button>

										</c:forEach>
									</div>
									<div class="row">
										<c:forEach begin="${half+1}" var="eval" items="${result.evalList}">
											<c:set var="evalCount" value="0"></c:set>
											<c:forEach var="boardEval" items="${item.board_evaluation}">
												<c:if test="${eval.eval_code== boardEval.eval_code }">
													<c:set var="evalCount" value="${boardEval.count}"></c:set>
												</c:if>
											</c:forEach>
											<button type="button" class="btn board__evalItem ${fn:indexOf(codeJoinStr,eval.eval_code)>=0?'board__evalItem--selected ':''}  ${eval.eval_sign}  col-4 col-md-3 col-lg-2" data-eval-code="${eval.eval_code}"
												data-selected="${fn:indexOf(codeJoinStr,eval.eval_code)>=0 ?'true':'false'}"
											>
												<i class="fa-2x ${eval.font_awesome_class}"></i><span>${eval.eval_name}</span>
											</button>
										</c:forEach>
									</div>

								</div>
							</fieldset>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</c:forEach>
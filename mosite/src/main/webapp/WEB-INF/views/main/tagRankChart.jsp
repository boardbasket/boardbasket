<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:set var="hashtagRank" value="${result.hashtagRank }" />
<div class="tagRankChart" data-type="graph">
	<div class="tagRankChart__menu" data-type="graphMenu">
		<c:set var="simpleMenu" value="${true}" />
		<c:set var="initialTimeMode" value="week" />
		<c:set var="initialTimeSet" value="${result.hashtagRankTimeSet}"/>
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<c:if test="${hashtagRank!=null }">
			<div data-init>
				<c:forEach var="tag" items="${hashtagRank }">
					<div class="hashtag" data-f="${tag.v }" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w }" data-t="${tag.t }" data-d="${tag.d}"></div>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<div class="tagRankChart__content" data-type="graphContent">
		<canvas></canvas>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@const.getProperty('const.maxChartTagSelect')" var="maxChartTagSelect" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_keyword_persistency')" var="persistency" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_keyword_popularity')" var="popularity" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_keyword_universality')" var="universality" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_base_period')" var="basePeriod" scope="page" />
<spring:eval expression="@serverConst.getProperty('server_const.radar_base_time_mode')" var="baseTimeMode" scope="page" />

<div class="radarChart" data-type="graph">
	<div class="radar__menu" data-type="graphMenu">
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<div class="option no-gutters">
			<div class="col-12 option__radarChart d-none">
				<div class="badge badge-primary option__timeUnit selected" data-time-mode="${baseTimeMode}" data-time-unit="${basePeriod}" data-default="${true}">
					최근 ${basePeriod} ${baseTimeMode=="year"?"년":baseTimeMode=="month"?"개월":"주" }<i class="fas fa-check" aria-hidden="true"></i>
				</div>
				<div class="badge option__timeUnit badge-primary" data-time-mode="${baseTimeMode}" data-time-unit="${1}" data-default="${false}">
					최근 ${1} ${baseTimeMode=="year"?"년":baseTimeMode=="month"?"개월":"주" }<i class="fas fa-check" aria-hidden="true"></i>
				</div>
			</div>
		</div>
		<c:if test="${result.radarTag!=null}">
			<div class="radar__initData" data-init>
				<div>
					<c:forEach var="label" items="${result.labels}">
						<div data-label="${label}"></div>
					</c:forEach>
				</div>
				<div data-radarTag>
					<div class="radar__persistenceInit">
						<div class="hashtag" data-type="${persistency}" data-v="${result.radarTag.persistency.v }" data-t="${result.radarTag.persistency.t}"></div>
					</div>
					<div class="radar__popularityInit">
						<div class="hashtag" data-type="${popularity}" data-v="${result.radarTag.popularity.v }" data-t="${result.radarTag.popularity.t}"></div>
					</div>
					<div class="radar__universalityInit">
						<div class="hashtag" data-type="${universality}" data-v="${result.radarTag.universality.v }" data-t="${result.radarTag.universality.t}"></div>
					</div>
				</div>
			</div>
		</c:if>
	</div>
	<div class="radar__content" data-type="graphContent">
		<canvas></canvas>
		<%-- <%@include file="/WEB-INF/views/main/noData.jsp"%> --%>
	</div>
	<div class="radar__footer d-flex justify-content-center align-items-center">
		<span class="cursor-pointer"><i class="fas fa-info-circle fa-xs" data-toggle="tooltip" data-placement="bottom" title="얼마나 많은 게시판에서 언급되는지에 관련된 점수입니다.">보편성</i></span>|<span class="cursor-pointer"><i class="fas fa-info-circle fa-xs"
			data-toggle="tooltip" data-placement="bottom" title="얼마나 많이 나타나는지에 관련된 점수입니다."
		>화제성</i></span>|<span class="cursor-pointer"><i class="fas fa-info-circle fa-xs" data-toggle="tooltip" data-placement="bottom" title="해당 기간에 얼마나 꾸준히 언급되는지에 관련된 점수입니다.">지속성</i></span>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:if test="${result.siteTrendTag!=null && fn:length(result.siteTrendTag)>0}">
	<div class="content__siteTag col-12">
		<div class="content__title col-12">
			<h3>커뮤니티 트렌드 태그</h3>
		</div>
		<hr />
		<div class="chart">
			<c:set var="simpleMenu" value="${true}" />
			<c:set var="initTagList" value="${result.siteTrendTag}" />
			<%@include file="/WEB-INF/views/main/chart.jsp"%>
		</div>

	</div>
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="favorite" value="${result.favoriteBoardList}" />
<c:if test="${favorite!=null and favorite.size()>0 }">

	<div class="content__favorite row">
		<c:set var="targetList" value="${favorite}" />

		<%@include file="/WEB-INF/views/main/board.jsp"%>
	</div>
</c:if>
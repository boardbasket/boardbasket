<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:if test="${fn:length(result.rssContentList)>0}">
	<div class="container">
		<c:set var="rssList" value="${result.rssContentList }" />
		<h3>관련 기사</h3>
		<table class="table table-sm">
			<thead>
				<tr>
					<th scope="col"></th>
					<th scope="col">제목</th>
					<th scope="col">날짜</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="rss" items="${rssList }">
					<tr>
						<th scope="row"><span class="badge" style="background:${rss.site_color};color:white;">${rss.site_name }<a href="${rss.site_address }"><i class="fas fa-external-link-alt"></i></a></span></th>
						<td><a href="${rss.content_address }" target="_blank">${rss.title }</a></td>
						<td>${rss.date}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</c:if>

<c:if test="${fn:length(result.communityContentList)>0}">
	<div class="container ">
		<c:set var="contentList" value="${result.communityContentList }" />

		<h3>관련 커뮤니티 글</h3>
		<table class="table table-sm">
			<thead>
				<tr>
					<th scope="col"></th>
					<th scope="col">게시판</th>
					<th scope="col">제목</th>
					<th scope="col">날짜</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="content" items="${contentList }">
					<tr>
						<th scope="row"><span class="badge" style="background:${content.site_color};color:white;">${content.site_name }<a href="${content.site_address }"><i class="fas fa-external-link-alt"></i></a></span></th>
						<td><a href="${content.board_address }">${content.board_name }</a></td>
						<td><a href="${content.article_address }" target="_blank">${content.title }</a></td>
						<td>${content.date}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</c:if>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@const.getProperty('const.maxChartTagSelect')" var="maxChartTagSelect" scope="page" />

<c:set var="percetageOfBoardList" value="${result.percetageOfBoardList}" />
<div class="boardPercentageChart" data-type="graph">
	<div class="boardPercentageChart__menu" data-type="graphMenu">
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<div style="visibility: hidden">placeholder</div>
		<div class="boardPercentageChart__initData">

			<c:if test="${percetageOfBoardList!=null }">
				<div class="init__boards" data-init>
					<c:forEach var="board" items="${percetageOfBoardList}">
						<c:forEach var="popularTag" items="${board.popular_hashtag_code_list }">
							<c:set var="popularTagJoin" value="${popularTagJoin} ${popularTag.t }"></c:set>
						</c:forEach>

						<div data-site_color="${board.site_color }" data-site_address="${board.site_address }" data-site_name="${board.site_name }" data-board_name="${board.board_name }" data-board_address="${board.board_address }" data-val="${board.val}"
							data-all_comment_list_lenght="${board.all_comment_list_lenght}" data-positive="${board.positive}" data-negative="${board.negative}" data-popular_hashtag_code_list="${fn:trim(popularTagJoin)}"
						></div>
						<c:set var="popularTagJoin" value=""></c:set>
					</c:forEach>
				</div>
			</c:if>

		</div>
	</div>
	<div class="boardPercentageChart__content" data-type="graphContent">
		<canvas></canvas>
	</div>
</div>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="tagInfo" data-type="${sessionScope.isMobile eq true ?'graph':'' }">
	<div class="tagInfo__menu">
		<c:set var="simpleMenu" value="${true}" />
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<c:if test="${result.firstAndLast!=null && result.mostMentionedDate!=null }">
			<div data-init>
				<c:forEach var="item" items="${result.firstAndLast }">
					<div data-key="firstAndLast" data-t="${item.t}" data-d="${item.d }" data-y="${item.y }" data-m="${item.m }" data-w="${item.w }" data-d="${item.d}"></div>
				</c:forEach>
				<c:forEach var="item" items="${result.mostMentionedDate }">
					<div data-key="mostMentionedDate" data-t="${item.t }" data-d="${item.d}" data-y="${item.y }" data-m="${item.m }" data-w="${item.w }"></div>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<div class="tagInfo__content">
		<div class="tagInfo__infoWrapper row">

			<div class="tagInfo__mostMentionedDate mb-2 col-xs-12 col-md-12 col-xl-6 ">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							<i class="fa-solid fa-calendar-days fa-xl"></i>최다 등록일
						</h5>
						<h4 class="tagInfo__desc card-title"></h4>
					</div>
				</div>
			</div>
			<div class="tagInfo__lastRegistered mb-2 col-xs-12 col-md-12 col-xl-6 ">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							<i class="fa-solid fa-calendar-check fa-xl"></i>마지막 등록일
						</h5>
						<h4 class="tagInfo__desc card-title"></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
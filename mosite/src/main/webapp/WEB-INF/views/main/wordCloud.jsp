<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="wordCloud" data-type="graph">
	<div class="wordCloud__menu" data-type="graphMenu">
		<%@include file="/WEB-INF/views/main/options.jsp"%>
		<c:if test="${initTagList!=null}">
			<div class="wordCloud__initData" data-init>
				<c:forEach var="tag" items="${initTagList}">
					<div class="hashtag" data-f="${tag.f }" data-t="${tag.t }" data-y="${tag.y}" data-m="${tag.m}" data-w="${tag.w}" data-d="${tag.d}"></div>
				</c:forEach>
			</div>
		</c:if>
	</div>
	<div class="wordCloud__content" data-type="graphContent">
		<%-- <%@include file="/WEB-INF/views/main/noData.jsp"%> --%>
	</div>
</div>
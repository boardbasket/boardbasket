<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<spring:eval expression="@const.getProperty('const.defaultNickName')" var="defaultNickName" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.comment')" var="commentPattern" scope="page" />
<spring:eval expression="@patterns.getProperty('patterns.nickname')" var="nicknamePattern" scope="page" />

<c:set var="root" value="${pageContext.request.contextPath }" scope="session" />

<!DOCTYPE html>
<html>

<%@include file="/WEB-INF/views/common/head.jsp"%>

<body class="${sessionIsMobile}${sessionIsTablet}">

	<div class="login">
		<sec:authorize access="isAnonymous()">
			<!-- before logined  -->
			<div class="login__btn">
				<c:choose>
					<c:when test="${sessionScope.isMobile eq true}">
						<button class="btn btn-secondary btn-lg" onclick="location.href='${root }/user/login.do'">
							<i class="fas fa-sign-in-alt"></i>
						</button>
					</c:when>
					<c:otherwise>
						<a href="${root }/user/login.do" class="btn btn-secondary div-loginBtn_a-login">로그인</a>
					</c:otherwise>
				</c:choose>
			</div>
		</sec:authorize>


		<sec:authorize access="isAuthenticated()">
			<!--after login  -->
			<button class="btn btn-secondary btn-lg" type="button" id="afterLoginMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-boundary="window">
				<i class="fas fa-bars fa-lg"></i>
			</button>
			<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item dropdown__showNoticeBoard cursor-pointer" data-board-code="${result.noticeBoard.board_code}">공지사항</a>


				<button id="boardBasketInfo" class="dropdown-item">BoardBasket?</button>

				<sec:authorize access="hasRole('ROLE_ADMIN')">
					<a class="dropdown-item" href="${root}/admin/adminMain.do">관리자 페이지</a>
				</sec:authorize>
				<div class="dropdown-divider"></div>
				<form action="${root}/logOut.do" method="post">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }" />
					<input id="btn-logOut" type="submit" class="dropdown-item" value="로그아웃" />
				</form>
			</div>

		</sec:authorize>
	</div>


</body>

</html>



<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<spring:eval expression="@const.getProperty('const.default_time_mode')" var="defaultTimeMode" scope="page" />

<c:set value="${initialTimeSet!=null?initialTimeSet:result.baseTimeSet }" var="baseTimeSet" />
<c:set value="${result.today}" var="today" />
<c:set value="${initialTimeMode!=null?initialTimeMode:result.searchData!=null?result.searchData.timeMode:defaultTimeMode}" var="timeMode" />

<c:choose>
	<c:when test="${simpleMenu}">
		<span class="badge badge-primary option__time d-none" data-initial="${timeMode}" data-mode="${timeMode}"><i class="fas fa-check" aria-hidden="true"></i></span>
		<c:choose>
			<c:when test="${timeMode=='year'}">
				<c:forEach var="year" items="${result.timeOptionYear}">
					<c:set var="yearClass" value="" />
					<c:set var="yearInitial" value="${false}" />
					<c:forEach var="time" items="${baseTimeSet}">
						<c:if test="${time.year==year }">
							<c:set var="yearClass" value="option__year--selected" />
							<c:set var="yearInitial" value="${true}" />
						</c:if>
					</c:forEach>
					<c:if test="${yearInitial }">
						<div class="option__year badge badge-primary ${yearClass}" data-year="${year}" data-initial="${yearInitial}" data-time-mode="year" data-initial="${true}">${year}년<i class="fas fa-check"></i>
						</div>
					</c:if>
				</c:forEach>

			</c:when>
			<c:when test="${timeMode=='month'}">
				<c:set var="lastLoopMonth" value="-1" />
				<c:set var="lastLoopYear" value="-1" />
				<c:forEach var="searchTime" items="${baseTimeSet}">
					<c:if test="${(searchTime.year!=lastLoopYear)||(searchTime.month!=lastLoopMonth)}">
						<c:set var="lastLoopYear" value="${searchTime.year}" />
						<c:set var="lastLoopMonth" value="${searchTime.month}" />

						<div class="option__month badge badge-primary option__month--selected" data-year="${searchTime.year}" data-month="${searchTime.month}" data-time-mode="month" data-initial="${true}">
							${searchTime.year}년
							<fmt:formatNumber minIntegerDigits="2" value="${searchTime.month}" />월 <i class="fas fa-check"></i>
						</div>
					</c:if>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach var="time" items="${baseTimeSet}">
					<c:set var="week" value="${time.week!=null?time.week:0}" />
					<div class="badge badge-primary option__week option__week--selected" data-year="${time.year}" data-month="${time.month}" data-week="${week}" data-time-mode="week" data-initial="${true}">${time.year}년
						<fmt:formatNumber minIntegerDigits="2" value="${time.month}" />월
						<c:if test="${week!=0 }">
					${week}주차 
					</c:if>
						<i class="fas fa-check"></i>
					</div>
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<input class="option__timeMode" type="hidden" value="${timeMode}" />
		<div class="badge badge-danger option__resetTime" title="원래 시간대로 되돌리기">
			<i class="fa-solid fa-rotate-left"></i>
		</div>
	</c:when>
	<c:otherwise>
		<div class="option__yearWrapper">
			<div class="badge badge-primary option__time  ${timeMode=='year'?'option__time--selected':'' }" data-initial="${timeMode=='year'}" data-mode="year">
				연 단위 <i class="fas fa-check" aria-hidden="true"></i>
			</div>
			<c:forEach var="year" items="${result.timeOptionYear}">
				<c:set var="yearClass" value="" />
				<c:set var="yearInitial" value="${false}" />
				<c:forEach var="time" items="${baseTimeSet}" varStatus="yearCheckLast">
					<c:if test="${time.year==year}">
						<c:set var="yearClass" value="option__year--selected" />
						<c:set var="yearInitial" value="${true}" />
					</c:if>
				</c:forEach>
				<div class="option__year badge badge-primary ${yearClass}" data-year="${year}" data-initial="${yearInitial}" data-time-mode="year">${year}년<i class="fas fa-check"></i>
				</div>
			</c:forEach>

		</div>
		<div class="option__monthWrapper">
			<div class="badge badge-primary option__time  ${timeMode=='month'?'option__time--selected':'' }" data-initial="${timeMode=='month'}" data-mode="month">
				월 단위 <i class="fas fa-check" aria-hidden="true"></i>
			</div>
			<c:forEach var="year" items="${result.timeOptionYear}">
				<c:forEach var="baseTime" items="${baseTimeSet}" varStatus="checkLastMonth">
					<c:set var="monthsClass" value="${(timeMode!='year'&&baseTime.year==year&&checkLastMonth.last)?'option__months--selected':''}" />
				</c:forEach>

				<div class="option__months ${monthsClass}" data-year="${year}">
					<c:forEach var="item" items="${result.weekList}" varStatus="outerStatus">
						<c:if test="${year==item.year}">
							<fmt:parseNumber var="month" type="number" value="${item.month}" />
							<fmt:parseNumber var="week" type="number" value="${item.week}" />
							<fmt:parseNumber var="year" type="number" value="${item.year}" />
							<c:set var="monthClass" value="" />
							<c:set var="monthInitial" value="false" />
							<c:forEach var="time" items="${baseTimeSet}" varStatus="checkLastVarStatus">
								<c:if test="${(timeMode!='year')&&time.year==year&&time.month == month}">
									<c:set var="monthClass" value="option__month--selected" />
									<c:set var="monthInitial" value="${time.month == month }" />
								</c:if>
							</c:forEach>
							<div class="option__month badge badge-primary ${monthClass}" data-year="${year}" data-month="${month}" data-initial="${monthInitial}" data-time-mode="month">
								<fmt:formatNumber minIntegerDigits="2" value="${month}" />월 <i class="fas fa-check"></i>
							</div>
						</c:if>
					</c:forEach>
				</div>

			</c:forEach>
		</div>
		<div class="option__weekWrapper d-none">
			<div class="badge badge-primary option__time ${timeMode=='week'?'option__time--selected':'' }" data-initial="${timeMode=='week'}" data-mode="week">
				주 단위 <i class="fas fa-check" aria-hidden="true"></i>
			</div>

			<c:forEach var="item" items="${result.weekList}" varStatus="outerStatus">
				<fmt:parseNumber var="month" type="number" value="${item.month}" />
				<fmt:parseNumber var="week" type="number" value="${item.week}" />
				<fmt:parseNumber var="year" type="number" value="${item.year}" />

				<c:set var="targetMonth" value="${false}" />
				<c:set var="checkLast" value="${false}" />

				<c:forEach var="time" items="${baseTimeSet}" varStatus="checkLastStatus">
					<c:if test="${time.year==year&&time.month == month}">
						<c:set var="targetMonth" value="${true}" />
						<c:set var="checkLast" value="${checkLastStatus.last}" />
					</c:if>
				</c:forEach>

				<div class="option__weeks ${((timeMode=='week')&&targetMonth&&checkLast)?'option__weeks--selected':'' }" data-year="${item.year}" data-month="${month}">
					<c:set var="targetMonth" value="${false}" />

					<c:forEach begin="1" end="${week}" varStatus="innerStatus">

						<c:set var="targetWeek" value="${false}" />
						<c:set var="weekClass" value="" />
						<c:set var="weekText" value="" />
						<c:set var="weekInitial" value="false" />
						<c:forEach var="time" items="${baseTimeSet}">
							<c:if test="${(timeMode=='week')&&time.year==year&&time.month == month && time.week == innerStatus.index}">
								<c:set var="weekClass" value="option__week--selected" />
								<c:set var="weekInitial" value="${timeMode=='week'}" />
							</c:if>
						</c:forEach>

						<c:set var="weekText" value="${innerStatus.index } 주차 " />
						<c:if test="${today.year==year&&month==today.month }">
							<c:choose>
								<c:when test="${today.week<innerStatus.index }">
									<c:set var="weekClass" value="${weekClass} option__week--disabled" />
									<c:set var="weekText" value="${innerStatus.index } 주차 " />
								</c:when>
								<c:when test="${today.week==innerStatus.index }">
									<c:set var="weekText" value="${innerStatus.index } 주차(이번주) " />
								</c:when>
							</c:choose>
						</c:if>
						<div class="badge badge-primary option__week ${weekClass}" data-year="${year}" data-month="${month}" data-week="${innerStatus.index }" data-initial="${weekInitial}" data-time-mode="week">${weekText}
							<i class="fas fa-check"></i>
						</div>
					</c:forEach>
				</div>
			</c:forEach>
		</div>
		<input class="option__timeMode" type="hidden" value="${timeMode}" />
	</c:otherwise>
</c:choose>
<c:if test="${initialTimeMode!=null }">
	<%-- 일회성 적용 위해 초기화   --%>
	<c:set var="initialTimeMode" value="${null}"></c:set>
</c:if>
<c:if test="${initialTimeSet!=null}">
	<%-- 일회성 적용 위해 초기화   --%>
	<c:set var="initialTimeSet" value="${null}"></c:set>
</c:if>
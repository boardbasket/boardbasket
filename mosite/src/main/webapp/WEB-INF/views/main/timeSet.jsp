<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set value="${result.baseTimeSet }" var="baseTimeSet" />
<c:set value="${result.timeSetList}" var="timeSetList" />
<c:set value="${result.today }" var="today"></c:set>

<div id="timeSetList" class="timeSetList d-none">
	<c:forEach var="timeSet" items="${timeSetList}">
		<c:set var="current" value="" />
		<c:forEach var="baseTime" items="${baseTimeSet}">
			<c:set var="week" value="${baseTime.week!=null?baseTime.week:0}" />
			<c:if test="${timeSet.year==year&&timeSet.week==week && timeSet.month== baseTime.month }">
				<c:set var="current" value="timeSet--current" />
			</c:if>
		</c:forEach>
		<div class="timeSet ${current}" data-from-time="${timeSet.fromTime }" data-to-time="${timeSet.toTime }" data-year="${timeSet.year}" data-month="${timeSet.month}" data-week="${timeSet.week }"></div>
	</c:forEach>
</div>
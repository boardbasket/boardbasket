var _saveCurrentStateTimer;

function getContextPath() {
	var hostIndex = location.href.indexOf(location.host) + location.host.length;
	return location.href.substring(hostIndex, location.href.indexOf('/', hostIndex + 1));
};

// serialize -> json
function formDataToJson(formDataArr) {

	var jsonObj = new Object();
	$(formDataArr).each(function(index, obj) {
		jsonObj[obj.name] = obj.value;
	});

	return jsonObj;

}

// max 미만의 랜덤한 integer값 가져옴
function getRandomInt(max) {
	
    return Math.floor(Math.random() * max);
}




// 현재 boardcontainer상태를 저장
function saveCurrentState() {
	// ajax에러가 나왔을땐 sessionStorage에 게시판 관련 정보를 저장하지 않음
	if (!_boardReloadFlag) {
		if (typeof getContentDataFromContainer === "function"
				&& getCurrTabCode()) {
			setItemToSessionStorage(getCurrTabCode(),
					getContentDataFromContainer());
		}
		if (!_isMobile
				&& !$(".iframe-externalContentFrame").data("firstOpen")) {
			setItemToSessionStorage("frameSize", {
				frameWidth : $("#frameDiv").width(),
				mainWidth : $(".mainDiv").data("prevWidth")
						|| $(".mainDiv").width()
			})
		}
	}
}

// loadingSpinner 숨기기 보이기
function showLoadingSpinner(showBoolean) {

	showBoolean ? $(".div-loadingSpinner").show() : $(".div-loadingSpinner").hide();
}

function isLoading(){
	return $(".div-loadingSpinner").is(":visible");
}

function ajaxSetup() {
	// ajax Setup

	/*
	 * $(document).ajaxSend(function(e, xhr, options) {
	 * xhr.setRequestHeader("AJAX", "true");
	 * xhr.setRequestHeader($('meta[name="csrf_headerName"]').attr('content'),
	 * $('meta[name="csrf_token"]').attr('content')); }).ajaxSuccess(
	 * function(e, xhr, options) { // post 요청을 할때 readOnlyFrame을 재 로딩 함 const
	 * readOnlyFrame = $(".iframe-readOnlyFrame");
	 * 
	 * clearTimeout(_saveCurrentStateTimer ); _saveCurrentStateTimer =
	 * setTimeout(function(){ saveCurrentState() ; } ,1000 );
	 * 
	 * showLoadingSpinner(false); }).ajaxError(function(e, xhr, options) {
	 * 
	 * _boardReloadFlag = true; showLoadingSpinner(false);
	 * clearSessionStorage(); ajaxErrorHandler(xhr,e,options);
	 * }).ajaxStart(function() { showLoadingSpinner(true); });
	 */
	
	$(document).ajaxSend(function(e,xhr,options){
		 xhr.setRequestHeader("AJAX", "true"); 
		 (options.type==="POST")&&xhr.setRequestHeader($('meta[name="csrf_headerName"]').attr('content'),$('meta[name="csrf_token"]').attr('content'));
	}).ajaxStart(function(){		
		showLoadingSpinner(true);		
	}).ajaxSuccess(function(){	
		//showLoadingSpinner(false);
	}).ajaxError(function(e,xhr,options){
		showLoadingSpinner(false);
		ajaxErrorHandler(xhr);
	}).ajaxStop(function(){
		showLoadingSpinner(false);
	});
	
	
}

// 토스트 메시지를 만드는 함수
function makeToast(optionObj) {

	let message = "";
	
	
	if(typeof optionObj ==='string')
		message = optionObj;
	else{
		message = optionObj.message||"";
	}
	
	const option = optionObj;
	const type = option ? option.type || "default" : "default";
	const position = option ? (option.position || false) : false;
	const disappear = option ? (option.hasOwnProperty("disappear")?option.disappear :true) : true;
	const delay = option ? (option.delay || 5000) : 5000;
	const conatiner = option ? option.container || ".toastContainer" : ".toastContainer";
	const closeBtn = option ? (option.hasOwnProperty("closeBtn") ? option.closeBtn : true) : true ;
	const $toastContainer = $(conatiner);  
	const allowMultipleToast = option?(option.allowMultipleToast?option.allowMultipleToast:false):false; 
	const title = option ? (option.title || "알림") : "알림"; 
	const $toast = $("#toastDummy").clone().removeAttr("id");
	// const content = option ?(option.content?option.content:null):null;
	
	closeBtn || $toast.find("button").remove();

	// 여러개의 토스트 허용?
	if(!allowMultipleToast){
		if($toastContainer.children(".toast").length>0)// 현재 띄워진 것을 지우고
								// 띄움
			$toastContainer.children(".toast").remove();
	}
		
	
	switch (type) {
	case "danger":
		$toast.addClass("border-danger").find(".toast-header").addClass("bg-danger");
		break;
	case "warning" :
		$toast.addClass("border-warning").find(".toast-header").addClass("bg-warning");
		break;
	default:
		$toast.addClass("border-info").find(".toast-header").addClass("bg-info");
		break;
	}
	$toast.find("i").text(title);
	
	
	if (disappear) {

		$toast.attr("data-delay", delay).toast("show");

		$toast.on('hidden.bs.toast', function() {
			$(this).remove();
			$toastContainer.children().length <= 0 && $toastContainer.hide();
		})

	} else {
		$toast.attr("data-autohide", false);
		$toast.on('hidden.bs.toast', function() {
			$(this).remove();
		})
	}
	
	
	/*
	 * if(content){ if(typeof content ==="string"){
	 * $toast.find(".toast-body").append($(content)); }else
	 * $toast.find(".toast-body").append($.parseHTML(content)); }else
	 */
		
	let messageElem = $.parseHTML(message)
	if(messageElem.length>0)
		$toast.find(".toast-body").append(messageElem);
	else
		$toast.find(".toast-body").text(message);
	
	
	if (conatiner === ".toastContainer"){
		$(".toastContainer:hidden") && $toastContainer.show();
		$toastContainer.prepend($toast);
		$toast.toast("show");
	}else {
		$toastContainer.prepend($toast);
		$toast.toast("show");
		
		$toast.css({
			"position" : "absolute",
			"left" : position ? ((position.x - $toast.width() / 2) < 0 ? 0 : (position.x -$toast.width() / 2)) : 0,
			"top" : position ? (position.y+$toast.height() >= window.innerHeight ? ($toast.height()*1.1): position.y) : 0
		})
	}

	
	
	return $toast;
}



// ajax 에러 처리
function ajaxErrorHandler(xhr) {

	if(xhr.status===403){
		alert("로그인이 필요합니다.");
		location.href="/user/login.do";
	}else{
		const response =  xhr.responseJSON;
		if(response){
			if(response.redirect){
				location.href='/user/login.do';
				return;
			}					
			if(response.msg){
				alert(response.msg);				
			}				
			else{
				location.reload();
			}							
		}else{
			location.reload();
		}						
	}

}

// 현재탭의 코드값 가져오기
function getCurrTabCode() {
	return $("input[name=currTabCode]").val();
}

function getCurrTabName(){

	return $("input[name=currTabName]").val();
}

// 모바일 환경 체크
(function(a) {
	(jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i
			.test(a)
			|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i
					.test(a.substr(0, 4))
})(navigator.userAgent || navigator.vendor || window.opera);

// 모바일 환경인지 확인
function isMobile() {
    	const elem = $("input[name=isMobile]");
    	return elem.length>0 ? JSON.parse(elem.val()) :$.browser.mobile;
}

// 타블렛 환경인지 확인
function isTablet() {
    	const elem = $("input[name=isTablet]");
    	return JSON.parse(elem.val());
}

function isLandscape() {
	if (window.innerHeight > window.innerWidth)
		return false;
	else
		return true;
}

// javascript vh vw vmax vmin - > px 구하기
function vh(v) {
	var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	return (v * h) / 100;
}

function vw(v) {
	var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	return (v * w) / 100;
}

function vmin(v) {
	return Math.min(vh(v), vw(v));
}

function vmax(v) {
	return Math.max(vh(v), vw(v));
}

function convertpxTovh(val){
	
	const temp = parseInt(val);
	
	return (temp/vh(1)).toFixed(1)+"vh";
	
}

function convertpxTovw(val){

	const temp =   parseInt(val);
	 
	return (temp/vw(1)).toFixed(1)+"vw";
}

function convertpxTovmax(val){
	
	const temp =   parseInt(val);
	 
	return (temp/vmax(1)).toFixed(1)+"vmax";
	
}

function convertpxTovmin(val){
	
	const temp =   parseInt(val);
	 
	return (temp/vmin(1)).toFixed(1)+"vmin";
	
}


function vUnitTopx(value) {

	var parts = value.match(/([0-9\.]+)(vh|vw|vmax|vmin)/);

	if (parts.length >= 3) {

		switch (parts[2]) {
		case "vh":
			return vh(parseInt(parts[1]));
		case "vw":
			return vw(parseInt(parts[1]));
		case "vmax":
			return vmax(parseInt(parts[1]));
		case "vmin":
			return vmin(parseInt(parts[1]));
		}
	}

}

// 태그로 검색하고 있는 상태,로그인 안된 상태인지를 체크
function isTempState(){
	return ($("input[name=isTempState]").val()==="true");
}

function isLogined(){
	return $("input[name=authenticated]").length>0;
}

// 항상 필요한 데이터 빼고 세션스토리지에 있는데이터 삭제
function clearSessionStorage(){
	
	let typeVal = sessionStorage.getItem("searchTypeVal");
	let siteList = sessionStorage.getItem("siteList"); 
	try {
		typeVal = JSON.parse(typeVal);
		siteList= JSON.parse(siteList);
		sessionStorage.clear();
		sessionStorage.setItem("searchTypeVal",JSON.stringify(typeVal));
		sessionStorage.setItem("siteList",JSON.stringify(siteList));
		boardReloadFlag=true;
	} catch (e) {
		console.log(e);		
	}
}
// adcount를 1 더함
function addADcount(){
	let ad =  JSON.parse(getCookie("ad"));
	
	ad.adCount = parseInt(ad.adCount)+1;
	setCookie("ad",JSON.stringify(ad));
	
}

// 지금 AD를 추가해야 하는지 확인
function checkShowAdd(){
	let ad = JSON.parse(getCookie("ad"));
	let threshold = parseInt(ad["adThreshold"]);
	let count = parseInt(ad["adCount"]);
	let adThresholdMax = parseInt(ad["adThresholdMax"]);
	 
	if(threshold<=count){
		let nextVal =threshold+count;
 
		threshold = nextVal>=adThresholdMax?adThresholdMax:nextVal;
		ad.adThreshold = nextVal;
		ad.adCount = 0;
		
		setCookie("ad",JSON.stringify(ad));
		return true;
	}else
		return false;
	
}

function getCookie(name) {
	  let matches = document.cookie.match(new RegExp(
	    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	  ));
	  return matches ? decodeURIComponent(matches[1]) : undefined;
	}

function setCookie(name, value, options = {}) {

  options["path"] = '/';

  if(typeof options.expires ==="undefined"||!(options.expires instanceof Date)){
	  options.expires = new Date(new Date().getTime()+86000e3);
  }
  options.expires = options.expires.toUTCString();
	  

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}


function deleteCookie(name) {
  setCookie(name, "", {
    'max-age': -1
  })
}

// 게시판 없음 element를 보여주거나 숨기는 함수
function showNoBoards(flag){
	
	if(flag)
		$("#noBoards").css("display","flex").find("*").css("display","block");
	else
		$("#noBoards").css("display","none");
	
	
}
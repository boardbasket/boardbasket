/**
 * 상수들
 */

const MILLISECOND = 1;
const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;
const WEEK = DAY * 7;
const MONTH = WEEK * 4;

const KEYWORD_DELIMETER = /[\s,]+/;

const maxWeekOptionNum = parseInt($("input[name=maxWeekOptionNum]").val());
const maxMonthOptionNum = parseInt($("input[name=maxMonthOptionNum]").val());
const maxYearOptionNum = parseInt($("input[name=maxYearOptionNum]").val());

const chartColor = (function() {

	var _color = new Array();
	for (let i = 0; i < 12; ++i)
		_color.push(new Array(6));
	var _dataColorScale = new Array();
	var _instance;

	_dataColorScale.push("#1f77b4");
	_dataColorScale.push("#aec7e8");
	_dataColorScale.push("#ff7f0e");
	_dataColorScale.push("#ffbb78");
	_dataColorScale.push("#2ca02c");
	_dataColorScale.push("#98df8a");
	_dataColorScale.push("#d62728");
	_dataColorScale.push("#ff9896");
	_dataColorScale.push("#9467bd");
	_dataColorScale.push("#c5b0d5");
	_dataColorScale.push("#8c564b");
	_dataColorScale.push("#c49c94");
	_dataColorScale.push("#e377c2");
	_dataColorScale.push("#f7b6d2");
	_dataColorScale.push("#7f7f7f");
	_dataColorScale.push("#c7c7c7");
	_dataColorScale.push("#bcbd22");
	_dataColorScale.push("#dbdb8d");
	_dataColorScale.push("#17becf");
	_dataColorScale.push("#9edae5");

	// 1월
	_color[0][0] = "#526E82";
	_color[0][1] = "#5D8392";
	_color[0][2] = "#6998A3";
	_color[0][3] = "#75AFB3";
	_color[0][4] = "#81C3C0";
	_color[0][5] = "#8dd3c7";

	// 2월
	_color[1][0] = "#7E9968";
	_color[1][1] = "#96AD77";
	_color[1][2] = "#AEC285";
	_color[1][3] = "#C8D694";
	_color[1][4] = "#E3EBA4";
	_color[1][5] = "#ffffb3";
	// 3월
	_color[2][0] = "#7E6E84";
	_color[2][1] = "#8C7D96";
	_color[2][2] = "#998CA7";
	_color[2][3] = "#A69BB8";
	_color[2][4] = "#B2ABC9";
	_color[2][5] = "#bebada";
	// 4월
	_color[3][0] = "#997F3E";
	_color[3][1] = "#AD8348";
	_color[3][2] = "#C28552";
	_color[3][3] = "#D6855C";
	_color[3][4] = "#E98367";
	_color[3][5] = "#fb8072";
	// 5월
	_color[4][0] = "#494A82";
	_color[4][1] = "#545C93";
	_color[4][2] = "#5E6FA3";
	_color[4][3] = "#6984B3";
	_color[4][4] = "#759AC3";
	_color[4][5] = "#80b1d3";
	// 6월
	_color[5][0] = "#939934";
	_color[5][1] = "#ADAD3C";
	_color[5][2] = "#C2B645";
	_color[5][3] = "#D6B84E";
	_color[5][4] = "#EBB758";
	_color[5][5] = "#fdb462";
	// 7월
	_color[6][0] = "#3E8B3A";
	_color[6][1] = "#519C43";
	_color[6][2] = "#67AD4C";
	_color[6][3] = "#7FBD55";
	_color[6][4] = "#98CE5F";
	_color[6][5] = "#b3de69";
	// 8월
	_color[7][0] = "#997B79";
	_color[7][1] = "#AD8A8B";
	_color[7][2] = "#C19AA0";
	_color[7][3] = "#D5ABB7";
	_color[7][4] = "#E9BCCE";
	_color[7][5] = "#fccde5";

	// 9월
	_color[8][0] = "#828282";
	_color[8][1] = "#949494";
	_color[8][2] = "#A5A5A5";
	_color[8][3] = "#B6B6B6";
	_color[8][4] = "#C8C8C8";
	_color[8][5] = "#d9d9d9";

	// 10월
	_color[9][0] = "#744A5C";
	_color[9][1] = "#83556E";
	_color[9][2] = "#925F81";
	_color[9][3] = "#A06A95";
	_color[9][4] = "#AF75A9";
	_color[9][5] = "#bc80bd";
	// 11월
	_color[10][0] = "#758F7F";
	_color[10][1] = "#84A18D";
	_color[10][2] = "#94B49A";
	_color[10][3] = "#A5C6A6";
	_color[10][4] = "#B7D9B5";
	_color[10][5] = "#ccebc5";
	// 12월
	_color[11][0] = "#72993C";
	_color[11][1] = "#8EAD46";
	_color[11][2] = "#ACC24F";
	_color[11][3] = "#CCD65A";
	_color[11][4] = "#EBEB64";
	_color[11][5] = "#ffed6f";

	function makeInstance() {

		return {
			getColor : function(month, week) {
				try {

					return _color[month - 1][typeof week === "undefined" ? 0 : week]
				} catch (e) {
					return "#808080";
				}

			},
			getDataColor : function(idx) {

				if (typeof idx !== "undefined")
					return _dataColorScale[idx % _dataColorScale.length];
				else {

					const obj = new Array();
					_dataColorScale.forEach(function(item) {
						obj.push(item);
					})
					return obj;

				}
			}
		}
	}

	return {
		getInstance : function() {

			if (!_instance)
				return _instance = makeInstance();
			else
				return _instance;
		}
	}

})();
function HeatMap(elem, option) {
	HeatMap.index++;
	TagData.call(this);

	var that = this;
	this.parent = (option && option.parent) ? option.parent : null;
	this.id = this.parent ? this.parent.id : HeatMap.index;
	this.boardCode = this.parent && this.parent.id;
	this.element = elem instanceof jQuery ? elem : $(elem);
	this.element.attr("data-heat-map-id", this.id);
	this.labelData = option.labelData;
	this.menu = new ChartMenu(this.element, this.updateOption(option ? option.menu : {}, this.getDefaultOption()));
	this.menuTimer;
	var variable = [ '일', '토', '금', '목', '수', '화', '월' ]; //heatmap의 세로축 
	var svg;
	var legend;
	var xAxis;
	var yAxis;
	var heatMapSelector = "[data-heat-map-id='" + that.id + "']";
	var margin = {
		left : 30,
		right : 30,
		top : 30,
		bottom : 30
	};

	var width = that.element.width();//svg 전체 넓이

	var heatMapWidth = width;//svg에서 heatMap이 차지하는 넓이

	var height = that.element.height() - that.menu.optionElement.parent().outerHeight(true);

	var _selectMenuTimer;// 메뉴 선택할때 딜레이 주는 타이머

	var tooltip;

	// Build color scale
	var scaleColor;

	this.getSvg = function() {
		return svg;
	}

	// Three function that change the tooltip when user hover / move / leave a cell
	var mouseover = function(d) {
		d3.select(this).style("stroke", "black").style("stroke-width", "2");
		tooltip.style("top", d3.event.y + margin.top);
		tooltip.style("display", "block").style("opacity", "1");
	}

	var mouseclick = function(d) {
		const boundingBox = d3.select(this).node().getBBox();
		const bounding2 = that.element[0].getBoundingClientRect();
		const weekList = new Array();
		weekList.push({
			day : moment(d.date).toDate()
		})

		tagMenu.getInstance().showMenu(true, {
			pos : {
				x : bounding2.x + boundingBox.x,
				y : bounding2.y + boundingBox.y
			},
			params : {
				board_code : that.parent ? that.boardCode : null,
				site_code : that.menu.getSelectedSiteData(),
				search_target : "tag",
				board_name : that.menu.getSelectedTagValue().join(' '),
				timeMode : "day",
				weekList : weekList,
				offset : 0
			},
			board : that.parent,
			showMenuFlag : {
				searchTimeSet : false,
				searchBoard : that.parent ? that.parent.searchAvailable ? true : false : false,
				articleArccordian : true
			}
		})

	}

	var mousemove = function(d) {
		tooltip.html(
				"<div class='tooltip__content'><div>" + d.date + "</div><div><div style='border:1px solid grey;display:inline-block;width:1rem;height:1rem;background:" + colorScale(d.value)
						+ "'></div>value: " + d.value + "</div></div>").style("left", (d3.mouse(this)[0] + 70) + "px").style("top", (d3.mouse(this)[1] + margin.top) + "px");
	}
	var mouseleave = function(d) {
		d3.select(this).style("stroke-width", "0");
		tooltip.style("display", "none");
	}
	this.getDraw = function() {
		return draw;
	}

	function drawLegend(domain, color, yItemNum) {

		legend.select("g").remove();
		if (domain == Number.MIN_SAFE_INTEGER)
			return;

		var linear = d3.scaleLinear().domain([ 0, domain ]).range([ "white", color ]);
		const cellNum = 10;
		const legendItemWidth = that.getSvg().node().getBBox().width / cellNum;

		legend.append("g").attr("class", "legendLinear").attr("transform", `translate(0,${yAxis.bandwidth()*yItemNum})`);

		var legendLinear = d3.legendColor().shapeWidth(legendItemWidth).cells(cellNum).orient('horizontal').scale(linear);

		legend.select(".legendLinear").call(legendLinear);
	}

	function draw(init) {

		setTimeout(function() {
			const data = getHeatMapData(init);
			that.menu.addTagElement(data.labelData, data.selectedTag);
			appendNoDataElement(that.element, !data.data || data.data.length <= 0);
			const color = d3.schemeCategory10[data.colorIndex % d3.schemeCategory10.length];
			colorScale = d3.scaleLinear().range([ "white", color ]).domain([ 0, data.domainMax ]);
			// Build X scales and axis:
			xAxis && svg.select(".xAxis").remove();
			xAxis = d3.scaleBand().range([ 0, width - (margin.left + margin.right) ]).domain(data.group).padding(0.01);
			svg.append("g").classed("xAxis", true).attr("transform", "translate(0," + (height - (margin.top + margin.bottom)) + ")").call(d3.axisBottom(xAxis))

			// Build Y scales and axis:
			yAxis && svg.select(".yAxis").remove();
			yAxis = d3.scaleBand().range([ height - (margin.top + margin.bottom), 0 ]).domain(data.variable).padding(0.02);
			svg.append("g").classed("yAxis", true).call(d3.axisLeft(yAxis));

			svg.selectAll("rect").remove();

			const rect = svg.selectAll("rect").data(data.data, function(d) {
				return d.group + ':' + d.variable;
			})

			rect.enter().append("rect").attr("x", function(d) {
				return xAxis(d.group);
			}).attr("y", function(d) {
				return yAxis(d.variable);
			}).attr("width", xAxis.bandwidth()).attr("height", yAxis.bandwidth()).style("fill", function(d) {
				return colorScale(d.value);
			}).on("mouseover", mouseover).on("mousemove", mousemove).on("mouseleave", mouseleave).on("click", mouseclick);

			rect.exit().transition().duration(200).remove();

			drawLegend(data.domainMax, color, data.variable.length);
		}, 0)

	}

	function getHeatMapData(init) {
		const params = that.getHashtagParams();//태그 가져올 파라미터 
		const rawData = that.getTagData(params);//가져온 태그 데이터

		const flattend = Object.keys(rawData).reduce(function(r, k) {//시간대별로 나눠진 값을 하나의 리스트로 (flatMap)
			return rawData[k] ? r.concat(rawData[k]) : r;
		}, []);
		const tagNameListBackup = params.tagNameList;
		params.tagNameList = [];
		const labelData = that.labelData || that.getUniqueTagNameList(params);//태그메뉴 만들 정보
		params.tagNameList = tagNameListBackup;
		const result = new Object();//결과 오브젝트		
		const heatMapTagList = new Array();//
		const groups = new Array();
		const timeMode = that.menu.getInitialTimeMode();
		let colorIndex = 0;//그래프 색
		let domainMax = Number.MIN_SAFE_INTEGER;//값의 최대값(색 범위 지정하기 위한 값)
		let targetList;//처리할 리스트

		params.tagNameList = init ? labelData.slice(0, 1) : params.tagNameList;//최초로 그리는 거라면 맨처음 태그를 임의로 선택해 그림

		if (params.tagNameList.length <= 0) {
			//선택한 태그가 없으면 빈 리스트 리턴 
			targetList = new Array();
		} else if (params.tagNameList.length > 1) {
			//2개 이상의 태그를 선택한경우 날짜별로 묶어서 리턴
			const temp = new Object();

			flattend.reduce(function(acc, item) {
				const date = item.d + '';
				temp[date] = temp[date] ? temp[date] + item.f : item.f;
			})

			targetList = Object.keys(temp).map(function(key) {
				return {
					d : key,
					f : temp[key]
				}
			})
			colorIndex = 0;

		} else {
			targetList = flattend; //하나만 선택한다면 가져온 데이터 그대로 리턴
			const colorTemp = labelData.indexOf(params.tagNameList.length > 0 && params.tagNameList[0]);
			colorIndex = colorTemp < 0 ? 0 : colorTemp;
		}
		/*
		 * 1.그룹 리스트 만들고 2.요일 다 집어넣고(그룹,요일 추가됨) 3.그룹 ,요일이 맞으면 해당 객체에 값 집어넣기
		 */

		targetList.forEach(function(item) {

			const value = item.f;
			const date = moment(item.d).startOf();
			let group = moment(item.d).isoWeek() + "주"; //`${item.m}월 ${item.w?item.w주:''}`;

			//그룹(주 단위)가 추가되면 미리 월~일요일을 다 추가함
			if (!groups.includes(group)) {
				groups.push(group);
				const fromTime = moment(item.d).isoWeekday(1);
				const toTime = moment(item.d).isoWeekday(7);
				const count = toTime.diff(fromTime, 'day');

				for (let i = 0; i <= count; ++i) {
					const wod = moment(fromTime).add(i, 'days');

					heatMapTagList.push({
						group : group,
						variable : wod.format("dd"),
						value : 0,
						date : wod.format("YYYY-MM-DD")
					})
				}

			}
			domainMax = Math.max(value, domainMax);

			//그룹이 같은 list요소에 값을 넣음
			for (let i = 0; i < heatMapTagList.length; ++i)
				if (heatMapTagList[i].group === group && heatMapTagList[i].variable === date.format("dd")) {
					heatMapTagList[i].value = value;
					break;
				}

		})

		result.selectedTag = params.tagNameList;
		result.variable = variable;
		result.labelData = labelData;
		result.group = groups;//12월 3째주 ,2째주 등을 정렬
		result.data = heatMapTagList;
		result.domainMax = domainMax;
		result.colorIndex = colorIndex;

		return result;

	}

	function updateLayout() {
		width = that.element.width();
		height = that.element.height() - that.menu.optionElement.parent().outerHeight(true);
		d3.select(heatMapSelector + " svg").style("width", width).style("height", height).attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox", "0 0 " + width + " " + height);

	}

	function update() {
		updateLayout();
		draw();
	}

	function init() {
		const legendHeight = height * 0.1;
		updateLayout();
		d3.select(heatMapSelector + " svg").remove();
		d3.select(heatMapSelector + " .tagHeatMap__content").append("svg");

		legend = d3.select(heatMapSelector + " .tagHeatMap__content svg").append("g").attr("transform", "translate(" + 0 + "," + margin.top + ")");

		svg = d3.select(heatMapSelector + " .tagHeatMap__content svg").attr("width", width).attr("height", height).attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox",
				"0 0 " + (width) + " " + (height)).append("g").attr("transform", "translate(" + (margin.left) + "," + (margin.top - legendHeight) + ")");

		// create a tooltip
		tooltip = d3.select(heatMapSelector).append("div").style("display", "none").attr("class", "tooltip").style("background-color", "white").style("border", "solid").style("border-width", "2px")
				.style("border-radius", "5px").style("padding", "5px");

		const params = that.getHashtagParams();
		const initData = that.getInitData(that.element, params);

		if (!initData.hashtagData && params.board_name) {

			getTagHeatMapData(params).done(function(data) {
				const targetData = data.result ? data.result.heatMapTag : {};
				data.result = that.getInitData(targetData, params);
				that.setTagData(params, data.result);

				that.setTagData(params, data.result || {});
				draw(true);
			})
		} else {
			that.setTagData(params, initData);
			draw(true);
		}
	}

	if (option && option.init) {
		init();
	}

	return {
		draw : draw,
		init : init,
		update : update
	}

}

HeatMap.prototype = Object.create(TagData.prototype);
HeatMap.prototype.constructor = TagData;
HeatMap.index = 0;

HeatMap.prototype.getDefaultOption = function() {
	const that = this;
	return {
		siteMenu : false,
		tagMenu : true,
		timeMenu : true,
		limitMenu : false,
		modalMenu : false,
		wordCloudMenu : false,
		multiTimeSelect : false,
		multiTagSelect : false,
		initialLimit : 10,
		onTagClick : function(target) {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				const tag = that.menu.getTagWithName(target.data("tagName"));
				const params = that.getHashtagParams();//태그 가져올 파라미터 
				const rawData = that.getTagData(params);//가져온 태그 데이터
				const keys = Object.keys(rawData);

				if (keys.flatMap(function(key) {//외부에서 tagMenu를 넣었고 rawData가 하나도 없는 경우
					return rawData[key];
				}).length <= 0 && that.labelData) {
					getTagHeatMapData(params).done(function(data) {
						const targetData = data.result ? data.result.heatMapTag : {};
						data.result = that.getInitData(targetData, params);
						that.setTagData(params, data.result);
						that.getDraw()();
					})
				} else
					that.getDraw()();
			}, 1000);
		},
		onTimeClick : function() {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				that.getDraw()();
			}, 1000);

		}
	};
}

/**
 * 태그 랭킹 순위를 보여주는 차트
 */

function TagRankChart(element, option) {
	TagData.call(this);

	this.element = element
	this.tagRankChart;
	this.chart;
	this.chartData = {};
	this.canvas = this.element.find("canvas")[0];
	this.menuTimer;
	this.zoomWindow=7;
	this.prevSelectedPoint;//mobile일때 사용하는 이전 클릭 포인트 변수
	this.scrollIndex = {
		min : null,
		max : null
	}
	var that = this;
	this.menu = new ChartMenu(this.element, Object.assign({}, option.menu, {
		onTagClick : function(target) {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				const selectedTag = that.menu.getSelectedTagValue().join("");

				if (selectedTag) {
					const params = that.getHashtagParams();

					let targetData = that.getTagData(params);

					if (targetData) {
						that.labelData = option.labelData;
						that.drawChart();

					} else {

						/*
						 * that.getHashtagRankData(params).done(function(data) {
						 * 
						 * that.setTagData(params, data); that.drawChart(); });
						 */
						that.drawChart();
					}
				} else {
					that.chart && that.chart.destroy();
				}
			}, 1000);

		}
	}));

	this.menu.setTimeMode("week");//일단위 표시위해 변경

	if (option) {

		if (option.init) {

			const params = that.getHashtagParams();
			const initData = that.getInitData(that.element, params);
			this._bindCanvasClickEvent(this.canvas);
			this._bindCanvasScrollEvent(this.canvas);

			if (!initData.hashtagData) {

				/*
				 * that.getHashtagRankData(params).done(function(data) {
				 * that.setTagData(params, data); that.drawChart(option); })
				 */
				that.drawChart(option);
			} else {
				that.setTagData(params, initData);
				that.drawChart(option);
			}
		}

	}

}

TagRankChart.prototype = Object.create(TagData.prototype);
TagRankChart.prototype.constructor = TagData;

TagRankChart.prototype.drawChart = function(option) {
	var that = this;
	const params = this.getHashtagParams();
	params.tagNameList = option && option.keywordInit ? option.keywordInit : params.tagNameList;
	const tagData = option && option.targetData || this.getTagData(params);

	const chartData = this.postDataProcessing(this.commonLineChartDataSet(tagData));

	const selectedTag = _.cloneDeep(params.tagNameList);
	delete params.tagNameList;
	const labelData = this.getUniqueTagNameList(params);

	// 해당 시간대에 맞는 태그들을 다시 추가해줌
	this.menu.addTagElement(labelData, selectedTag);

	appendNoDataElement(that.element, chartData.datasets.length <= 0);
	
	//모바일 형태면 초기 min max를 잡아줌
	if(isMobile()){
		try{
			const maxIdx=that.scrollIndex.max=chartData.labels.length-1;
			const minIdx=that.scrollIndex.min=chartData.labels.length-that.zoomWindow;					
			chartData.chartOption["scales"]["x"]["max"]=chartData.labels[maxIdx];
			chartData.chartOption["scales"]["x"]["min"]=chartData.labels[minIdx];		
		}catch(e){
			
		}
		
	}


	setTimeout(function() {

		that.destroyChart();
		that.chart = new Chart(that.canvas, {
			type : chartData.type,
			data : {
				datasets : chartData.datasets,
				labels : chartData.labels
			},
			options : chartData.chartOption,
			plugins :[ that._writeRankOnPoint(),that._moveChartBtn() ]
		});

	}, 0);

}

TagRankChart.prototype.destroyChart = function(idx) {
	this.chart && this.chart.destroy();
	return this;
}

TagRankChart.prototype.updateChart = function() {
	this.chart && this.chart.destroy();
	this.drawChart();
}

TagRankChart.prototype.getHashtagRankData = function(params) {

	return $.ajax({
		type : "GET",
		url : '/getHashtagRankData.ajax',
		data : getUrlparameter(params),
		dataType : "json",
		traditioanl : true
	});
}

TagRankChart.prototype.parseRawData = function(rawData) {

	const that = this;

	if (!rawData.hashtagRank)
		return;

	var result = {
		labels : [],
		datasets : []
	}

	const labels = rawData.hashtagRank.map(function(data) {
		return moment(data.d);
	})

	const data = rawData.hashtagRank.map(function(item) {
		return {
			x : moment(item.d),
			y : item.v,
			rawData : item
		}
	})

	result.labels = labels;

	result.datasets.push({
		data : data,
		borderWidth : 1,
		spanGaps : true,
		borderColor : "rgb(255, 99, 132)",
		backgroundColor : "rgb(255, 99, 132)",
		pointStyle : 'rectRot',
		pointRadius : 10,
		pointHoverRadius : 15
	})

	return result;

}

TagRankChart.prototype.getConfig = function(additionalOption) {
	const that = this;
	let todayAnnotationList;
	if(additionalOption)
		todayAnnotationList= ChartObj.prototype.makeAnnotation.apply(null,[additionalOption.todayAnnotationData]);//annotation 객체
	
	const config = {		
		maintainAspectRatio : false,
		interaction : {
			intersect : false,
			mode : 'point'
		},
		layout : {
			padding : {
				right : 18,
				bottom : 5
			}
		},onHover : function(event) {
			var point = this.getActiveElements(event);
			if (point.length > 0)
				event["native"]["target"]["style"]["cursor"] = "pointer";
			else
				event["native"]["target"]["style"]["cursor"] = "default";
		},onClick:function(event){
			
			const currSelectedPoint = that.chart.getActiveElements(event)[0];
			let pointData = event.chart.getActiveElements(event)[0];
			const rect = event.chart.canvas.getBoundingClientRect();			
			const weekList = new Array();
			let menuFlag=false;
			
			if (isMobile()) {				
				if (!that.prevSelectedPoint) {
					that.prevSelectedPoint = currSelectedPoint;
				} else {
					if(currSelectedPoint)
						if (that.prevSelectedPoint.index == currSelectedPoint.index) {						
							menuFlag=true;												
							that.prevSelectedPoint = null;
						} else
							that.prevSelectedPoint = currSelectedPoint;
				}
			} else{
				
				if(that.scrollIndex.max!=null && that.scrollIndex.min !=null)
					menuFlag=true;
			}
				
			
			if(menuFlag&&pointData){				
				const rawData = event.chart.data.datasets[pointData.datasetIndex]["data"][pointData.index]["rawData"];
				weekList.push({
					year : rawData.y,
					month : rawData.m,
					week : rawData.w,
					day : rawData.d && moment(rawData.d).toDate()
				})
				
				tagMenu.getInstance().showMenu(true, {
					pos : {
						x : rect.x + pointData.element.x,
						y : rect.y + pointData.element.y
					},
					params : {							
						search_target : "tag",
						board_name : that.chart.data.datasets[currSelectedPoint.datasetIndex].label,
						timeMode : that.menu.getTimeMode() == "year" ? "month" : (that.menu.getTimeMode() == "month" ? "week" : "day"),
						weekList : weekList,
						offset : 0
					},
					showMenuFlag : {
						searchTimeSet : true,
						searchBoard : that.parent ? that.parent.searchAvailable ? true : false : false,
						articleArccordian : true
					}
				})				
			}
			
		},
		scales : {
			x : {
				title : {
					display : true,
					text : "날짜"
				}
			},
			y : {
				title : {
					display : true,
					text : "순위"
				},
				suggestedMin:-1,
				reverse : true,
				ticks:{
					callback:function(value,index,tick){
						if(value<=0)
							return '';
						else
							return value;
					}
				}

			}
		},
		plugins : {
			tooltip : {
				enabled : true,
				position : 'nearest',
				callbacks : {
					label : function(tooltipItem) {
						const rawData = tooltipItem.raw.rawData;
						label = moment(String(rawData.d)).format("YYYY-MM-DD (dd)");
						return rawData.t + " : " + tooltipItem.formattedValue + "위";
					},
					title : function(tooltipItem) {

						const rawData = tooltipItem[0].raw.rawData;
						return moment(String(rawData.d)).format("YYYY-MM-DD (dd)");

					},
					labelColor : function(context) {
						return {
							borderColor : chartColor.getInstance().getDataColor(context.datasetIndex),
							backgroundColor : chartColor.getInstance().getDataColor(context.datasetIndex)
						};
					},
					footer : function(tooltipItem) {
						
						if(that.scrollIndex.max==null&& that.scrollIndex.min ==null)
							return '클릭해서 확대하기';
						else
							return '클릭해서 글 메뉴 보기';
					}
				}
			},
			legend : {
				labels : {
					usePointStyle : true,
				}
			},
			title:{
				display:true,
				text:"태그 순위 그래프"
			},
			subtitle : {
				display : true,
				position : "bottom",
				text:"2000위 까지, 매일 갱신",
				font : {
					size : 11
				}
			},
			annotation : {
				annotations : {}
			}

		}
	};

	config["scales"]["x"]["type"] = "timeseries";
	config["scales"]["x"]["time"] = {
		unit : 'day',
		tooltipFormat : "YYYY-MM-DD",
		displayFormat : {
			day : "MM월 DD일"
		}
	};
	
	todayAnnotationList && todayAnnotationList.forEach(function(annotation, idx) {

		config['plugins']['annotation']['annotations'][annotation.type + (idx + 1)] = annotation;

	})

	config["scales"]["x"]["ticks"] = {
		callback : function(value, index, values) {
			
			const label = moment(values[index].value).format("MM월 DD일");
			let dataExistFlag=false;
			this.chart.data.datasets.forEach(function(item){
				
				item.data.forEach(function(data){
				
					if(moment(values[index].value).isSame(moment(data.x)))
						dataExistFlag = data.y;					
				})												
			})
			
			if(dataExistFlag)
				return label;
			else	
				return "";
		}	

	}

	return config;
}

TagRankChart.prototype.postDataProcessing = function(chartData) {

	if (chartData) {

		if (chartData.datasets)
			chartData.datasets.forEach(function(data) {
				data.pointStyle = 'rectRot';
				data.pointRadius = 10;
				data.pointHoverRadius = 15;
			})			
	}

	return chartData;

}

TagRankChart.prototype.commonLineChartDataSet = function(data) {
	return ChartObj.prototype.commonLineChartDataSet.apply(this, [ data, "line" ]);
}

TagRankChart.prototype.isCompare = function() {
	return false;
}

TagRankChart.prototype.makeTimeLabels = function(data) {

	return ChartObj.prototype.makeTimeLabels.apply(this, [ data ]);
}

TagRankChart.prototype.makeLineChartDataSetInner = function(data) {
	const temp = new ChartObj();
	return temp.makeLineChartDataSetInner(data);
}

TagRankChart.prototype.getLineChartOption = function(additionalOption) {
	return this.getConfig(additionalOption);
}

TagRankChart.prototype._writeRankOnPoint= function() {
	
	const that = this;
	return {
		id : "writeRankOnPoint",
		afterEvent : function(chart, args) {
			const ctx = chart.ctx;
			const ca = chart.chartArea;
			const canvas = chart.canvas;			

		},
		afterDraw : function(chart, args, pluginOptions) {

			const ctx = that.chart.ctx;
			that.chart.config.data.datasets.forEach(function(dataset,datasetIdx){
				const meta =that.chart.getDatasetMeta(datasetIdx);
				
				meta.data.forEach(function(data,idx){
					
					const pointData = that.chart.data.datasets[datasetIdx].data[idx];
					const radius = data.options.radius;
					if(that.scrollIndex.max!=null && that.scrollIndex.min !=null){
						if(pointData.y){
							const xCoordinate = data.x;
		                	const yCoordinate = data.y;
		                	let yCoordinateResult;
		                	const text=  `${pointData.y}위`
		                	ctx.font = "bold 15px Helvetica";
		                	ctx.fillStyle="black";
		                	const textWidth = ctx.measureText(text).width;		                	
		                	ctx.fillText(text, xCoordinate-radius, yCoordinate-radius);	
						}				
					}															
				})								
				
			})
			
			
		}
	}
	
	
}


TagRankChart.prototype._moveChartBtn = function() {
	return {
		id : "moveChart",
		afterEvent : function(chart, args) {
			const ctx = chart.ctx;
			const ca = chart.chartArea;
			const canvas = chart.canvas;

			canvas.addEventListener("mousemove", function(event) {
				const x = args.event.x;
				const y = args.event.y;

				if (x >= ca.left - 15 && x <= ca.left + 15 && y >= (ca.height / 2) + ca.top - 15 && y<=(ca.height/2)+ca.top+15) {
					canvas.style.cursor = 'pointer';					
				}else if (x >= ca.right - 15 && x <= ca.right + 15 && y >= (ca.height / 2) + ca.top - 15 && y<=(ca.height/2)+ca.top+15) {
					canvas.style.cursor = 'pointer';					
				} else
					canvas.style.cursor = 'default';
			})
		},
		afterDraw : function(chart, args, pluginOptions) {

			const ctx = chart.ctx;
			const ca = chart.chartArea;
			const angle = Math.PI / 180;

			function CircleChevron() {

				this.draw = function(ctx, x1, pixel) {

					ctx.beginPath();
					ctx.lineWidth = 3;
					ctx.strokeStyle = 'rgba(102,102,102,0.5)';
					ctx.fillStyle = "white";
					ctx.arc(x1, ca.height / 2 + ca.top, 15, angle * 0, angle * 360, false);
					ctx.stroke();
					ctx.fill();
					ctx.closePath();

					ctx.beginPath();
					ctx.lineWidth = 3;
					ctx.strokeStyle = 'rgba(255,26,104,1)';
					ctx.moveTo(x1 + pixel, ca.height / 2 + ca.top - 7.5);
					ctx.lineTo(x1 - pixel, ca.height / 2 + ca.top);
					ctx.lineTo(x1 + pixel, ca.height / 2 + ca.top + 7.5);
					ctx.stroke();
					ctx.closePath();
				}

			}
			;

			let drawCircleLeft = new CircleChevron();
			drawCircleLeft.draw(ctx, ca.left, 5);
			let drawCircleRight = new CircleChevron();
			drawCircleRight.draw(ctx, ca.right, -5);
		}
	}
}

TagRankChart.prototype._bindCanvasScrollEvent=function(canvas){
	const that = this;
	const touchThreshold = 30;
	let nextIndex;
	if(isMobile()){
		let firstTouch;
		canvas.addEventListener("touchstart",function(e){
			firstTouch= e.touches || e.originalEvent.touches;
		})
		
		canvas.addEventListener("touchend",function(e){
			try{				
			
				const labels = that.chart.data.labels;
				const diff = firstTouch[0].clientX-e.changedTouches[0].clientX;
				let nextIndex;
				if(that.scrollIndex.max==null&& that.scrollIndex.min ==null){
					nextIndex = that._zoomInIndex();
				}else{
					if(Math.abs(diff)>touchThreshold){
						if(diff>0){
							nextIndex=that._scrollNextIndex();						
						}else if(diff<0){
							nextIndex=that._scrollPrevIndex();
						}	
					}					
				}
								
				if(Math.abs(diff)>0){
					that.chart.options.scales.x.min=labels[nextIndex.nextMin];
					that.chart.options.scales.x.max=labels[nextIndex.nextMax];	
					that.scrollIndex.min=nextIndex.nextMin;
					that.scrollIndex.max=nextIndex.nextMax;
					
					that.chart.update();
				}							
			}catch(exeption){
				
			}
			
		})
		
	}else{
		canvas.addEventListener("wheel",function(e){
			
			const labels = that.chart.data.labels;
			if(that.scrollIndex.max==null&& that.scrollIndex.min ==null){
				//nextIndex = that._zoomInIndex();
			}else{
				e.stopPropagation();
				e.preventDefault();
				if(e.deltaY>0){
					nextIndex=that._scrollNextIndex();
				}else
					nextIndex=that._scrollPrevIndex();
				
				that.chart.options.scales.x.min=labels[nextIndex.nextMin];
				that.chart.options.scales.x.max=labels[nextIndex.nextMax];	
				that.scrollIndex.min=nextIndex.nextMin;
				that.scrollIndex.max=nextIndex.nextMax;
				
				that.chart.update();
			}
			
			
			
		})
	}
	
	
	
}

TagRankChart.prototype._bindCanvasClickEvent=function(canvas){
	
	const that = this;
	
	canvas.addEventListener("click",function(e){
		const selectedElem = that.chart.getActiveElements();
	
		
		const chart = that.chart;
		const ctx = chart.ctx; 
		const ca = chart.chartArea;
		const canvas = chart.canvas;
		const labels = chart.data.labels;
		const rect = canvas.getBoundingClientRect();
		const zoomWindow = 7;//확대시 몇개나 보여줄 건지
		const x =e.clientX-rect.left;
		const y = e.clientY-rect.top;
		const currentMin = that.scrollIndex.min;
		const currentMax = that.scrollIndex.max;	
		let nextIndex	
		
		if(currentMin==null&& currentMax ==null){
			// 초기 상태
			nextIndex = that._zoomInIndex(selectedElem);		
		}else if (x >= ca.left - 15 && x <= ca.left + 15 && y >= (ca.height / 2) + ca.top - 15 && y<=(ca.height/2)+ca.top+15) {
			//왼쪽 눌렀을때																
			nextIndex = that._scrollPrevIndex();											
			
		}else if (x >= ca.right - 15 && x <= ca.right + 15 && y >= (ca.height / 2) + ca.top - 15 && y<=(ca.height/2)+ca.top+15) {
			
			//오른쪽 눌렀을때
			nextIndex = that._scrollNextIndex();			
			
		}else{
			// 초기 상태
		
				if(selectedElem.length<=0)
					nextIndex = that._zoomOutIndex();			
					
		}
		
		if(nextIndex){
			that.chart.options.scales.x.min=labels[nextIndex.nextMin];
			that.chart.options.scales.x.max=labels[nextIndex.nextMax];	
			that.scrollIndex.min=nextIndex.nextMin;
			that.scrollIndex.max=nextIndex.nextMax;
			
			chart.update();
		}					
	})
	
	
}
TagRankChart.prototype._scrollNextIndex=function(){
	const labels = this.chart.data.labels;
	let nextMin;
	let nextMax;
	
	if(this.scrollIndex.max+this.zoomWindow>=labels.length-1){		
		nextMax=labels.length-1;
		nextMin=nextMax-this.zoomWindow;
	}
	else{
		nextMin=this.scrollIndex.min+this.zoomWindow;
		nextMax=this.scrollIndex.max+this.zoomWindow;
	}															
	
	return {
		nextMin:nextMin,
		nextMax:nextMax
	}	
	
}

TagRankChart.prototype._scrollPrevIndex=function(){	
	let nextMin;
	let nextMax;	
	
	if(this.scrollIndex.min-this.zoomWindow<=0){
		nextMin=0;
		nextMax=6;				
	}
	else{
		nextMin=this.scrollIndex.min-this.zoomWindow;
		nextMax=this.scrollIndex.max-this.zoomWindow;				
	}							
	
	return {
		nextMin:nextMin,
		nextMax:nextMax
	}	
}

TagRankChart.prototype._zoomInIndex=function(selectedElem){
	let nextMin;
	let nextMax;	
	const labels = this.chart.data.labels;
	if(selectedElem!=null & selectedElem.length>0){
		nextMin = selectedElem[0].index-(this.zoomWindow-1)/2;
		nextMax = selectedElem[0].index+(this.zoomWindow-1)/2;			
		if(nextMin<=0){
			nextMin=0;
			nextMax=6;
		}else if(nextMax>=labels.length){
			nextMin=labels.length-7;
			nextMax=labels.length-1;
		}
			
	}else{
		nextMin=0;
		nextMax=6;
	}	
	return {
		nextMin:nextMin,
		nextMax:nextMax
	}	
}

TagRankChart.prototype._zoomOutIndex=function(){
		
	return {
		nextMin:null,
		nextMax:null
	}	
}


/**
 * 태그의 정보 표시
 */

function BoardPercentageChart(element, options) {

	this.element = element;
	var that = this;
	this.chartData = {};
	this.pieChart;
	this.pieChartElem = element.find("canvas")[0];
	this.labelData = options.labelData;
	this.menu = new ChartMenu(element, options ? options.menuOption ? option.menuOption : this.getDefaultMenuOption() : this.getDefaultMenuOption());
	this.prevPoint;//모바일 전용
	
	if (options) {

		if (options.init) {
			const parsedData = parseInitData();
			const selectedTag = this.labelData.slice(0, 1);
			if (parsedData) {
				this.setChartData(selectedTag, parsedData);
				this.draw(parsedData, selectedTag);

			} else {
				const params = this.getSearchParams();
				this.getBoardPercentageChartData(params).done(function(data) {

					that.setChartData(selectedTag, data);
					that.draw(that.getChartData(selectedTag));

				})
			}

		}
	}
	this.makePieChartData = function(data) {

		const result = {
			labels : [],
			datasets : [ {
				label : "게시판의 비율",
				data : new Array(),
				backgroundColor : new Array(),
				hoverOffset : 4
			} ]
		};

		let sumOfPercentage = 0;

		if(data&&data.length>0){			
			data.forEach(function(data, i) {
				result.labels.push(data.board_name);
				result.datasets[0].data.push(data);
				sumOfPercentage += data.val;
				result.datasets[0].backgroundColor[i] = chartColor.getInstance().getDataColor(i);
			})

			//'기타' 분류를 따로 넣어줌
			result.labels.push("기타");
			result.datasets[0].data.push({
				val : 100 - sumOfPercentage
			});
			result.datasets[0].backgroundColor.push("#808080");

		}
		
	
		return {
			type : "pie",
			data : result,
			plugins : [ ChartDataLabels ],
			options : {
				parsing : {
					key : "val"
				},
				onHover : function(event) {
					const point = this.getActiveElements(event);
					if (point.length > 0)
						event["native"]["target"]["style"]["cursor"] = "pointer";
					else
						event["native"]["target"]["style"]["cursor"] = "default";
				},
				onClick : function(event) {
					const activePoints = this.getActiveElements(event);
					let openMenuFlag = false;
					
					if(activePoints.length>0){
						const raw = activePoints[0].element.$context.raw;
						
						if(isMobile()){	
							if(that.prevPoint){								
								if(that.prevPoint.index == activePoints[0].index)
									openMenuFlag=true;
								else
									that.prevPoint = activePoints[0];
							}else
								that.prevPoint = activePoints[0];
																					
						}else
							openMenuFlag = raw.board_name;								
						
						if(openMenuFlag) {
							if(raw.board_name)
								relativeBoardMenu.showMenu({
									data:raw,
									pos:{
										x:event.native.clientX,
										y:event.native.clientY
									}
								})
							that.prevPoint=null;
						}													
							
					}
						
				},
				maintainAspectRatio : false,
				plugins : {
					datalabels : {
						formatter : function(value, context) {

							if (value.val > 15){
								const board_name = context.chart.data.labels[context.dataIndex];
								let target="";
								board_name.split(" ").forEach(function(item,idx){
									target = idx%2==0? target+(" "+item+"\n"):(target+" "+item); 
								});
								return target;
							}else
								return '';
						},
						anchor : "center",
						align : "center",
						color : 'white',
						font : {
							weight : "bold"
						}
					},
					tooltip : {
						callbacks : {
							label : function(context) {
								let label = context.label;
								const value = context.formattedValue;
								const siteName = context.raw.site_name;
								return (siteName ? `(${context.raw.site_name}) ` : '') + label + " : " + value + "%";
							}
						}
					},
					title:{
						display:true,
						text:"가장 많이 언급한 게시판"
					}
				}
			}
		}
	}

	function parseInitData() {
		if (element.find(".boardPercentageChart__initData").length > 0) {
						
			const percetageOfBoardList = element.find(".init__boards >").map(function() {
				const targetData=$(this).data();
				targetData.popular_hashtag_code_list = targetData.popular_hashtag_code_list.split(' ').map(function(item){					
					return {"t":item};
				})
				return targetData;				
			}).toArray();
					
			return percetageOfBoardList;;
		} else
			return null;

	}

	

}

BoardPercentageChart.prototype.drawPercetageOfBoardListPieChart = function(data) {
	const that = this;
	setTimeout(function() {
		
		that.pieChart && that.pieChart.destroy();
		
		Object.keys(data).length>0? appendNoDataElement(that.element,false):appendNoDataElement(that.element,true);

		that.pieChart = new Chart(that.pieChartElem, that.makePieChartData(data));
	}, 0);

}

BoardPercentageChart.prototype.getSearchParams = function() {
	return {
		board_name : this.menu.getSelectedTagValue().join(""),
		search_garget : "tag",
		timeMode : this.menu.getTimeMode(),
		weekList : this.menu.getSelectedTimeData()
	}
}

BoardPercentageChart.prototype.draw = function(data, selectedTag) {
	
	this.menu.addTagElement(this.labelData, selectedTag);
	this.drawPercetageOfBoardListPieChart(data);

}

BoardPercentageChart.prototype.getChartData = function(tag) {

	const index = parseInt(tag);

	return this.chartData[tag];

}

BoardPercentageChart.prototype.setChartData = function(tag, data) {

	const target = data?data.result?data.result.percetageOfBoardList:data:new Array();
	
	this.chartData[tag] = target;

}

BoardPercentageChart.prototype.getBoardPercentageChartData = function(params) {

	return $.ajax({
		type : "GET",
		url : '/getBoardPercentageChartData.ajax',
		data : getUrlparameter(params),
		dataType : "json",
		traditioanl : true
	});

}

BoardPercentageChart.prototype.getDefaultMenuOption = function() {
	const that = this;
	return {
		tagMenu : false,
		initialLimit : 10,
		multiTagSelect : false,
		onTagClick : function(target) {

			const selectedTag = that.menu.getSelectedTagValue().join("");
			const chartData = that.getChartData(selectedTag);

			if (selectedTag) {
				if (chartData) {
					that.draw(chartData, [ selectedTag ]);
				} else {

					that.getBoardPercentageChartData(that.getSearchParams()).done(function(data) {
						that.setChartData(selectedTag, data);
						that.draw(that.getChartData(selectedTag), [ selectedTag ]);
					})
				}
			} else
				that.pieChart && that.pieChart.destroy();

		}
	};
}

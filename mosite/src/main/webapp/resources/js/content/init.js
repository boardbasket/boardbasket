var boardMgr;
var siteTagChart;
var rankTagChart;
var search;
var tagHeatMap;
var searchMenu;
var radarChart;
var searchedTagLineGraph;
var relativeTagWordCloud;
var wordCloudChanger;
var tagRankChart;
var trendTagWordCloud;
var articleLoadFinished = true;//차트 기사 불러오는게 마무리 되었는지
var articleMgr = new ArticleManager();
var chartModal = new ChartModal();//차트 크게 보는 모달
var nowTimeSet = $(".nowTimeSet").data();
var tagAnalysis;
var tagRankList;

$(document).ready(function() {
	//AJAX 글로벌 세팅
	ajaxSetup();
	const _isMobile = isMobile();
	//moment 글로벌 언어 설정
	moment.locale("ko");

	//boardManager 초기화
	boardMgr = new BoardManager({
		chart : {
			init : false
		},
		wordCloud : {
			init : false,
			isMobile : _isMobile,
		},
		forceLayout : {
			init : false
		}
	});

	//검색바 초기화
	searchBar = new SearchBar($(".search"), {
		isMobile : _isMobile
	});

	$(".title__name span").on("click", function() {
		location.href = "/";
	})

	/*
	 * if ($(".content__siteTag .chart").length > 0) siteTagChart = new
	 * ChartObj( $(".content__siteTag .chart"), { init : true, siteMenu : true
	 * });
	 * 
	 * 
	 * //태그 순위 누를 때 옆에 있는 차트 태그 눌러주기 $(".tagRanking__tag").on("click",
	 * function() {
	 * rankTagChart.menu.getTagWithName($(this).data("tagName")).trigger("click"); })
	 * 
	 */

	$("#tagAnalysisCollapse").on("shown.bs.collapse", function(e) {
		if (!$(e.target).data("init")) {
			$(e.target).data("init", true);
			tagAnalysis.init();
		}
	})

	$("#relativeTagCollapse").on("shown.bs.collapse", function(e) {
		if (!$(e.target).data("init")) {
			$(e.target).data("init", true);
			wordCloudChanger.init();
		}
	})

	//검색결과에서 line 그래프 초기화
	$("#tagLineGraphCollapse").on("shown.bs.collapse", function(e) {
		if (!$(e.target).data("init")) {
			$(e.target).data("init", true);
			searchedTagLineGraph.init();
		}
	});

	//검색결과에서 heatMap 그래프 초기화
	$("#tagHeatMapCollapse").on("shown.bs.collapse", function(e) {
		if (!$(e.target).data("init")) {
			$(e.target).data("init", true);
			tagHeatMap.init();
		}

	});

	//태그 순위 그래프
	if ($(".tagRankChart").length > 0) {
		tagRankChart = new TagRankChart($(".tagRankChart"), {
			init : true,
			keywordInit : searchBar && searchBar.getKeywords().length > 0 ? searchBar.getKeywords().slice(0, 1) : null,
			menu : {
				tagMenu : true
			}
		})
	}

	//태그 분석 초기화
	if ($(".tagAnalysis").length > 0)
		tagAnalysis = new TagAnalysis($(".tagAnalysis"), {
			init : true,
			labelData : searchBar.getKeywords()
		});

	//연관 태그 워드 클라우드 초기화
	if ($(".wordCloudChanger").length > 0) {
		wordCloudChanger = new WordCloudChanger($(".wordCloudChanger"), {
			init : true,
			isMobile : _isMobile,
			labelList : searchBar.getKeywords()
		});
	}

	//검색 키위드 그래프가 있을때 초기화
	if ($(".content__searchedTagLineGraph").length > 0) {
		const elem = $(".content__searchedTagLineGraph");

		searchedTagLineGraph = new ChartObj(elem, {
			init : true,
			keywordInit : searchBar && searchBar.getKeywords().length > 0 ? searchBar.getKeywords().slice(0, 1) : null,
			menu : {
				siteMenu : true,
				tagMenu : true,
				limitMenu : false,
				modalMenu : true
			}

		})

	}

	//첫 페이지 트렌드 wordCloud
	if ($("[data-type=main_graph].wordCloud").length > 0) {

		trendTagWordCloud = new WordCloud($("[data-type=main_graph].wordCloud"), {
			init : true,
			main : true,
			menu : {
				timeMenu : false,
				limitMenu : true,
				limitArr : [ 50, 100, 200, 300 ],
				initialLimit : isMobile() ? 50 : 100
			}
		})
	}

	if ($(".content__relativeTag").length > 0) {
		relativeTagWordCloud = new WordCloud($(".content__relativeTag").find(".wordCloud"), {
			init : true,
			menu : {
				timeMenu : false,
				limitMenu : true
			}
		})

	}

	//태그 히트 맵
	if ($(".content__tagHeatMap").length > 0) {
		tagHeatMap = new HeatMap($(".content__tagHeatMap"), {
			init : $("#tagHeatMapCollapse.show").length > 0,
			labelData : searchBar.getKeywords(),
			menu : {
				timeMenu : true,
				siteMenu : true,
				tagMenu : true
			}
		});
	}

	//랭킹 차트가 있을때 초기화
	if ($(".content__rankChart").length > 0) {
		rankTagChart = new ChartObj($(".content__rankChart"), {
			init : true,
			keywordInit : searchBar && searchBar.getKeywords().length > 0 ? searchBar.getKeywords().slice(0, 1) : null,
			menu : {
				siteMenu : false,
				timeMenu : true,
				tagMenu : true,
				modalMenu : false,
				multiTagSelect : false
			}
		});
	}

	if ($(".tagRankList").length > 0) {
		tagRankList = new TagRankList($(".tagRankList"));
	}

	//윈도우 크기 변경될때 현재 게시판의 차트,wordCloud 다시 그리기
	$(window).on("resize", function() {
		boardMgr.update();

	}).on("click", function(e) {
		const target = $(e.target);
		if (target.closest(".board__commentForm ,.board__evalWrapper").length <= 0) {
			const currBoard = boardMgr.getCurrentBoard();
			currBoard && currBoard.hideEvalWrapper();
		}

		const board = target.closest(".board");
		boardMgr.setCurrentBoard(board);

	})
	//평가 모달 사라지면 안에 있는 차트 삭제
	$("#evalCountModal").on("hide.bs.modal", function() {
		const chart = Chart.getChart("evalCountModal__graph");
		chart && chart.destroy();
	})

	// 게시판 설정,페이지네이션 버튼 클릭 시
	$(".content__setting .search__evalItem .form-check-input,.page-item:not(.disabled)").on("click", function() {

		const evalCode = $(this).data("evalCode");
		const $wrapper = $(".content__setting");
		$wrapper.find("input[name=eval_code]").toArray().map(function(elem) {
			return $(elem).prop("disabled", true);
		}).filter(function(elem) {
			return $(elem).val() == evalCode;
		}).forEach(function(elem) {
			$(elem).prop("disabled", false);
		})

		const order = $wrapper.find("input:checked").val();

		let params = searchBar.getSearchParams();
		params.order = order;
		params.eval_code = evalCode;
		params.search_target = "board";
		if ($(this).hasClass("page-item")) {
			params.offset = $(this).attr("data-offset");
			params.search_target = "board";
		}

		location.href = searchBar.makeQueryString(params);

	})

	// 페이지네이션 번호 클릭시
	/*
	 * $(".page-item:not(.disabled)").on("click", function() { const params =
	 * searchBar.getSearchParams(); params.offset = $(this).attr("data-offset");
	 * 
	 * location.href = searchBar.makeQueryString(params); });
	 */
	// bootstrap tooltip활성화
	$('[data-toggle="tooltip"]').tooltip();

	//검색결과 더보기 버튼 클릭 시
	$(".search__loadMore").on("click", function(e) {
		e.stopPropagation();
		const type = $(this).data("type");
		const params = searchBar.getInitialSearchParams();
		params.search_target = type;
		params.board_name = params.board_name = params.board_name || searchBar.getTrendTag().slice(0, $("input[name=searchKeywordNumLimit]").val()).join(' ');
		switch (type) {
		case "board":
			params.offset = 2;
			//params.order = "tag";
			location.href = searchBar.makeQueryString(params);
			break;
		case "article":
			location.href = searchBar.makeQueryString(params);
			break;
		case "tag":
			location.href = searchBar.makeQueryString(params);
			break;
		}

	})

	//위아래 화살표 클릭
	$("[class*=content__navigator]").on("click", function() {

		const contentList = $(".content__title,[data-anchor]").toArray();
		const direction = $(this).data("direction");

		const currentContent = contentList.reduce(function(acc, curr) {
			if (acc == null)
				return curr;
			else {
				const currVal = Math.abs(curr.getBoundingClientRect().top);
				const accVal = Math.abs(acc.getBoundingClientRect().top);
				return accVal > currVal ? curr : acc;
			}
		}, null);

		let index = contentList.indexOf(currentContent);

		switch (direction) {
		case "up":
			index = index - 1 < 0 ? 0 : index - 1;
			break;
		case "down":
			index = index + 1;
			break;
		}

		const target = contentList[index];
		if (target)
			$('html, body').stop().animate({
				scrollTop : `${window.pageYOffset + target.getBoundingClientRect().top}`
			})
			//$(window).scrollTop(window.pageYOffset + target.getBoundingClientRect().top);
	})

	//메인페이지 그래프 전환 버튼
	$("[data-main-graph-type]").on("click", function() {
		switch ($(this).data("mainGraphType")) {
		case "wordCloud":
			$("[data-type=main_graph].wordCloud").addClass("show");
			$("[data-type=main_graph].tagRankList").removeClass("show");
			break;
		case "list":
			$("[data-type=main_graph].wordCloud").removeClass("show");
			$("[data-type=main_graph].tagRankList").addClass("show");
			tagRankList.updateParam();
			break;
		}
	})

})
window.onbeforeunload = function() {
	sessionStorage.setItem("news", articleMgr.getInstance().getBackupData());
};

function CommentManager(table, option) {

	var id = table.data("boardCode");
	this.table = table;
	this.tableBody = table.find("tbody");
	this.comments = table.find(".comment:not(.comment--my):not(.comment--empty)").map(function(idx, item) {
		return new Comment($(this));
	}).toArray();
	this.emptyComment = table.find(".comment--empty");//댓글 없을때 메시지 보여주는 용도의 tr

	this.commentLoadNum = JSON.parse(option.commentLoadNum) || $("input[name=commentLoadNum]").val();

	var commentSortTimer;
	var that = this;
	this.opinionChartData;
	var _allPageLoaded = false;
	this.myComment = new Comment(table.find(".comment--my"));

	var showMoreCommentRow = this.table.find(".comment__showMoreComment").closest("tr");

	this.getId = function() {
		return id;
	}

	this.pagination = this.table.find(".comment__pagination").on("click", function(e) {
		let target = $(e.target);
		e.preventDefault();
		if (target.hasClass("page-link")) {
			target = $(target).parent();
			that.hideAllComment();
			const pageNum = target.data("pageNum");
			if (!target.data("loaded")) {
				that.getCommentData(that.addComments, pageNum);

				that.setAllPageLoaded(target.siblings().filter("[data-loaded=false]").length <= 0);

			} else {
				that.showComment(pageNum, that.commentLoadNum, that.getAllPageLoaded());
			}

			target.addClass("active").siblings().removeClass("active");

		}

	})

	this.paginationRow = this.pagination.closest("tr");

	this.noMoreComment = $("<tr class='text-center comment__noMoreComment'><td colspan='5'>더이상 코멘트가 없습니다.</td></tr>");

	this.getPaginationRow = function(detach) {

		if (detach) {
			that.setPaginationRow(that.paginationRow.detach());
			return that.paginationRow;
		} else
			return that.paginationRow;

	}

	this.setPaginationRow = function(row) {
		PaginationRow = row;
	}

	// table에 넣기위한 row(comment) element를 만드는 함수
	this.makeComments = function(data, offset) {

		const commentList = data.commentList ? data.commentList.board_comment_list : null;
		if (!commentList)
			return;

		if (typeof offset === 'undefined')
			offset = 0;

		//const revealNum = Math.round($("input[name=commentLoadNum]").val() / 3) || 10;
		const result = new Array();
		commentList.forEach(function(elem, idx) {
			const showFlag = '';//idx < revealNum ? '' : 'd-none';
			const comment = $("<tr class='comment " + showFlag + "' data-comment-code='" + elem.comment_code + "' data-offset=" + (offset + idx) + ">");
			const writer = $("<td>").attr("data-writer", elem.writer).append(`<div>${elem.writer}</div>`);
			elem.my_comment_flag && writer.append(`<div class="font-small text-muted">(내가 작성)</div>`);
			const date = moment(elem.insert_date).format("YYYY-MM-DD");
			const insertDate = $("<td>").text(date).attr("data-insert-date", date);
			const userComment = $("<td>").text(elem.user_comment).attr("data-comment-content", elem.user_comment);
			userComment.append("<div class='w-100 comment__divider'>");
			const evalWrapper = $("<div>");
			elem.comment_eval_list && elem.comment_eval_list.forEach(function(eval) {
				evalWrapper.append("<span class='badge " + eval.eval_sign + " " + eval.font_awesome_class + " comment__evalItem--selected'>" + eval.eval_name + "</span>")
			})
			userComment.append(evalWrapper);

			const upVote = $(
					'<td><button class="btn btn-sm btn-primary comment__upVote" data-vote-value="1" ' + (elem.my_comment_flag ? 'disabled' : '')
							+ ' title="내 한마디는 비/추천할 수 없습니다."><i class="far fa-thumbs-up" aria-hidden="true"> ' + elem.up_vote + '</i></button></td>').attr("data-up-vote", elem.up_vote);
			const downVote = $(
					'<td><button class="btn btn-sm btn-secondary comment__downVote" data-vote-value="-1"' + (elem.my_comment_flag ? 'disabled' : '')
							+ ' title="내 한마디는 비/추천할 수 없습니다."><i class="far fa-thumbs-down" aria-hidden="true"> ' + elem.down_vote + '</i></button></td>').attr("data-down-vote", elem.down_vote);
			comment.append(writer).append(insertDate).append(userComment).append(upVote).append(downVote);

			result.push(new Comment(comment));
		})

		return result;

	}

	// table에 row을 넣는 함수
	this.addCommentElem = function(elem) {

		that.table.find("tbody").append(elem.commentElem ? elem.commentElem : elem);
	}

	// 현재 테이블의 정렬 정보를 가져오는 함수
	this.getCommentSortData = function() {
		let result = new Object();

		that.table.find(".commentTable__sortElement--selected").each(function(i, elem) {
			if ($(elem).data("orderBy"))
				result[$(elem).data("orderBy")] = $(elem).data("orderVal");
		})

		return result;
	}

	// sortelem을 누르면 다른 tr의 sortElem을 default 상태로 전환하는 함수
	this.setDefaultStateToSortElem = function(ignoreElem) {

		const ignoreTargetTh = ignoreElem.closest("th");

		const targetElem = that.table.find(".commentTable__sortElement--selected");

		targetElem.each(function(idx, item) {
			const tempTh = $(item).closest("th");

			if (!tempTh.is(ignoreTargetTh)) {
				tempTh.find(".commentTable__sortElement").removeClass("commentTable__sortElement--selected");
				tempTh.find(".commentTable__sortElement:first-of-type").addClass("commentTable__sortElement--selected")
			}

		})

	}

	//pagination을 만드는 함수
	this.makePagination = function(that, data) {
		const length = data.commentList.all_comment_list_lenght;

		const pageNum = Math.ceil(length / that.commentLoadNum);

		for (let i = 0; i < pageNum; ++i) {
			that.pagination.append(` <li class="page-item ${i==0?'active':''}" data-page-num="${i}" data-loaded=${i==0}><a class="page-link" href="#">${i+1}</a></li>`);
		}

	}

	this.getCommentSortTimer = function() {
		return commentSortTimer;
	}

	this.setCommentSortTimer = function(timer) {
		commentSortTimer = timer;
	}

	this.getAllPageLoaded = function() {
		return _allPageLoaded;
	}
	this.setAllPageLoaded = function(flag) {
		_allPageLoaded = flag;
	}

	this.hideAllComment = function() {
		that.comments.forEach(function(comment) {
			comment.hide();
		})
	}

	this.showComment = function(pageNum, commentLoadNum, ignoreRowNum) {
		const offset = commentLoadNum + (pageNum * commentLoadNum);

		if (ignoreRowNum) {//comment의 rownum이 아닌 순서대로 잘라서 보여주는 플래그(
			const target = that.tableBody.find(`.comment:not(.comment--my)`);
			for (let i = 0; i < that.comments.length; ++i) {
				if (offset - commentLoadNum <= i && i < offset) {
					target.eq(i).css("display", "table");
				}
			}

		} else {

			that.comments.filter(function(comment) {
				return offset - commentLoadNum <= comment.getOffset() && comment.getOffset() < offset;
			}).forEach(function(comment) {
				comment.show();
			})

		}
		that.addNoMoreComment(pageNum);
	}

	that.table.find(".commentTable___showDoughnutGraph").on("click", function() {
		if (that.opinionChartData) {
			that.drawEvalGraph(that.opinionChartData);
			$("#evalCountModal").modal("show");
		} else
			that.getBoardEvalCountData(that.getId());
	})

	// 정렬 아이콘 선택시 이벤트
	that.table.find(".commentTable__sortElement").on("click", function() {
		const elem = this;

		const nextTarget = $(elem).next().length ? $(elem).next() : $(elem).siblings().eq(0);

		//loaded 모두 취소,첫번째 엘리먼트 선택하기
		if (!that.getAllPageLoaded())
			that.pagination.find(".page-item").each(function() {
				$(this).is(":first-of-type") ? $(this).addClass("active") : $(this).removeClass("active");
				$(this).attr("data-loaded", false).data("loaded", false);
			})

		nextTarget.addClass("commentTable__sortElement--selected");
		nextTarget.siblings().removeClass("commentTable__sortElement--selected");

		that.setDefaultStateToSortElem(nextTarget);
		clearTimeout(that.getCommentSortTimer());

		that.setCommentSortTimer(setTimeout(function() {
			that.getAllPageLoaded() || that.clearComments();
			that.getCommentData(that.addComments, that.getPageNum());
		}, 1000));

	})
}

CommentManager.prototype.getComment = function(idx) {

	const result = this.comments.filter(function(k) {
		return k.getCommentCode() === idx;
	});

	return result.length ? result[0] : this.comments[idx];
}

// Comment객체 하나만 넣는 함수
CommentManager.prototype.addComment = function(elem) {

	if (!elem)
		return;

	this.comments.push(elem);
	this.addCommentElem(elem.commentElem);
}

// comment객체 여러개 넣는 함수
CommentManager.prototype.addComments = function(that, data) {

	that.comments = that.comments || new Array();

	const detached = that.getPaginationRow(true);

	if (data && Array.isArray(data)) {

		data.forEach(function(item) {
			that.addComment(item);
		})

	} else
		that.addComment(data);

	data && that.deleteEmptyCommentRow();//댓글이 하나라도 있으면 댓글없음 row 지우기

	that.tableBody.append(detached);
}

CommentManager.prototype.deleteEmptyCommentRow = function() {
	this.emptyComment && this.emptyComment.remove();
}

CommentManager.prototype.bindEvent = function() {
	const that = this;
	that.table.find(".comment__showMoreComment").on("click", function() {
		const target = that.comments.filter(function(item, idx) {
			return item.commentElem.hasClass("d-none");
		});

		if (target.length > 0) {
			const revealNum = Math.round($("input[name=commentLoadNum]").val() / 3);
			target.forEach(function(item, idx) {
				if (idx < revealNum)
					item.commentElem.removeClass("d-none");
			})
			that.getAllPageLoaded() && target.length <= revealNum && that.getPaginationRow().remove();
		} else {

			if ($("input[name=loadByOffset]").val()) {
				that.getCommentData(that.addComments);
			} else {
				if ($(this).data("reviewPage"))
					location.href = "/comment/" + that.getId();
				else {
					$(this).hide()
					that.tableBody.append(that.noMoreComment);
				}
			}

		}

	})

}

// eval doughnut 그래프를 그리는 함수
CommentManager.prototype.drawEvalGraph = function(data) {

	const that = this;

	setTimeout(function() {

		new Chart(document.getElementById("evalCountModal__graph"), {
			type : "doughnut",
			data : {
				datasets : data.dataset,
				labels : data.labels,
				backgroundColor : data.backgroundColor
			},
			plugins : [ ChartDataLabels ],
			options : {
				plugins : {
					datalabels : {
						formatter : function(value, context) {

							return context.chart.data.labels[context.dataIndex];
						},
						anchor : "center",
						align : "center",
						color : 'white',
						font : {
							weight : "bold"
						}
					},
					tooltip : {
						callbacks : {
							label : function(context) {
								let label = context.label;
								const value = context.formattedValue;
								const total = context.dataset.data.reduce(function(acc, curr) {
									return acc + curr;
								}, 0);

								return label + " : " + value + " (" + ((value / total) * 100).toFixed(1) + "%)";
							}
						}
					}
				}

			}
		});

	}, 0)
}

// 추천/비추천 doughnut 그래프를 그리기 위해 데이터를 가져오는 함수(그리기 까지)
CommentManager.prototype.getBoardEvalCountData = function(boardCode) {

	const that = this;

	$.get("/selectBoardEvalCount.ajax", {
		"board_code" : boardCode
	}, function(data) {

		if (!data.result) {
			alert("데이터가 존재하지 않습니다.");
			return;
		}

		const dataset = new Object();

		const dataList = data.result.evalList.sort(function(first, second) {
			return first.eval_sign > second.eval_sign ? 1 : -1;
		}).map(function(k) {

			return k.count;
		})

		const labels = data.result.evalList.map(function(k) {

			return k.eval_name;
		})

		const backgroundColor = data.result.evalList.map(function(k) {

			return k.eval_sign === 'POSITIVE' ? '#28a745' : '#dc3545';
		})

		const fontAwesomeClass = data.result.evalList.map(function(k) {

			return k.font_awesome_class;
		})

		dataset.dataset = new Array({
			backgroundColor : backgroundColor,
			data : dataList
		});

		dataset.labels = labels;
		dataset.fontAwesomeClass = fontAwesomeClass;

		that.opinionChartData = dataset;
		that.drawEvalGraph(dataset);
		$("#evalCountModal").modal("show");
	})

}

// 다음 그룹의 comment를 불러오기 위해 offset이 될 comment의 code를 가져옴
CommentManager.prototype.getOffset = function() {

	return this.comments.length;

	/*
	 * if (this.comments) return this.comments.length > 0 ?
	 * this.comments[this.comments.length - 1].getCommentCode() : null; else
	 * return null;
	 */

}

// comment 를 입력했을때 table에서 myComment에 해당하는 row의 값을 수정해주는 함수
CommentManager.prototype.updateMyComment = function(data) {

	if (!data)
		return;

	this.myComment.commentElem.removeClass("d-none");

	if (data.writer)
		this.myComment.setWriterVal(data.writer);

	if (data.comment_code)
		this.myComment.setCommentCode(data.comment_code);

	this.myComment.setInsertDateVal(moment().format("YYYY-MM-DD"));

	if (data.user_comment)
		this.myComment.setUserCommentVal(data.user_comment);

	if (data.comment_eval_list) {
		let evalList = this.myComment.commentElem.find("span");

		evalList.each(function(e, k) {
			$(k).hasClass("d-none") || $(k).addClass("d-none");

			data.comment_eval_list.forEach(function(x, j) {
				$(k).data("evalCode") == x.eval_code && $(k).removeClass("d-none");
			})

		})
	}
}

CommentManager.prototype.hideMyComment = function() {

	this.myComment.setInsertDateVal('');
	this.myComment.setWriterVal('');
	this.myComment.setUserCommentVal('');
	this.myComment.commentElem.find("span").addClass("d-none");
	this.myComment.commentElem.addClass("d-none");
}

// table의 row(comment) 를 전부 비우는 함수
CommentManager.prototype.clearComments = function() {
	this.setPaginationRow(this.getPaginationRow(true));
	this.table.find("tbody .comment:not(.comment--my)").remove();
	this.comments = new Array();
}

//댓글을 모두 가져왔을때 ajax로 불러오는 과정 없이 존재하는것을 바로 정렬
CommentManager.prototype.orderExistRow = function(param) {
	const that = this;
	const detached = that.comments.map(function(comment) {
		return comment.commentElem.detach();
	})

	return detached.sort(function(p1, p2) {

		const commentObj1 = that.getComment(p1.data("commentCode"));
		const commentObj2 = that.getComment(p2.data("commentCode"));
		let compOrder;
		let comp1;
		let comp2;
		if (param.insert_date) {
			compOrder = param.insert_date;
			const insert_date1 = commentObj1.getInsertDateVal();
			const insert_date2 = commentObj2.getInsertDateVal();

			comp1 = moment(insert_date1);
			comp2 = moment(insert_date2);

		} else if (param.up_vote) {
			compOrder = param.up_vote;
			comp1 = commentObj1.getUpVoteVal();
			comp2 = commentObj2.getUpVoteVal();

		} else if (param.down_vote) {
			compOrder = param.down_vote;
			comp1 = commentObj1.getDownVoteVal();
			comp2 = commentObj2.getDownVoteVal();
		} else {
			comp1 = commentObj1.getOffset();
			comp2 = commentObj2.getOffset();
			compOrder = "ASC";
		}

		if (compOrder === "ASC") {
			if (comp1 > comp2)
				return 1;
			else if (comp1 < comp2)
				return -1;
			else
				return 0;

		} else if (compOrder === "DESC") {
			if (comp1 > comp2)
				return -1;
			else if (comp1 < comp2)
				return 1;
			else
				return 0;
		}
	})

}

// 정보를 이용해서 해당 조건을 가진 comment를 불러옴
CommentManager.prototype.getCommentData = function(callbackfunc, pageNum) {
	const that = this;
	let _pageNum = typeof pageNum === 'undefined' ? 0 : pageNum;

	const offset = _pageNum * that.commentLoadNum + 1;
	let param = {
		board_code : that.getId(),
		offset : offset
	}

	param = Object.assign(param, that.getCommentSortData());

	if (this.getAllPageLoaded()) {

		this.orderExistRow(param).forEach(function(comment) {
			that.tableBody.append(comment);
		})
		that.hideAllComment();
		that.showComment(_pageNum, that.commentLoadNum, true);
		const pagination = that.getPaginationRow(true);
		that.table.find("tbody").append(pagination);
		that.addNoMoreComment(_pageNum);

	} else {

		$.getJSON("/selectBoardComment.ajax", param, function(data) {

			if (data.result.commentList.board_comment_list) {
				callbackfunc(that, that.makeComments(data.result, offset));
				const pagination = that.getPaginationRow(true);
				that.table.find("tbody").append(pagination);

			}
			that.pagination.find(`[data-page-num=${_pageNum}]`).attr("data-loaded", true).data("loaded", true);
			that.addNoMoreComment(_pageNum);

		});
	}

}

CommentManager.prototype.addNoMoreComment = function(pageNum) {

	//마지막 페이지를 알려주는 엘리먼트 삽입
	if (this.pagination.find(`[data-page-num=${pageNum}]`).is(":last-of-type"))
		this.pagination.before(this.noMoreComment);
	else
		this.noMoreComment = this.noMoreComment.detach();
}

CommentManager.prototype.getPageNum = function() {
	return this.pagination.find(".active").data("pageNum");
}

CommentManager.prototype.init = function(data) {
	this.addComments(this, this.makeComments(data));
	this.updateMyComment(data.commentList.my_comment);
	this.makePagination(this, data);
}

// ----------------------------------------comment----------------------------------

function Comment(elem) {

	this.commentElem = elem;
	var _commentCode = this.commentElem.attr("data-comment-code");
	var _commentOffset = this.commentElem.data("offset");
	var that = this;
	var _writerElem = this.commentElem.find("[data-writer]");
	var _userCommentElem = this.commentElem.find("[data-comment-content]");
	var _insertDateElem = this.commentElem.find("[data-insert-date]");
	var _upVoteElem = this.commentElem.find("[data-up-vote]");
	var _downVoteElem = this.commentElem.find("[data-down-vote]");
	var _rowNum = this.commentElem.find("[data-row-num]");
	this.getUpVoteVal = function() {
		return _upVoteElem.data("upVote");
	}

	this.setUpVoteVal = function(val) {
		_upVoteElem.data("upVote", val).find("i").text(val);
	}

	this.getDownVoteVal = function() {
		return _downVoteElem.data("downVote");
	}

	this.setDownVoteVal = function(val) {
		_downVoteElem.data("downVote", val).find("i").text(val);
	}

	this.getCommentCode = function() {
		return _commentCode;
	}

	this.setCommentCode = function(code) {
		this.commentElem.attr("data-comment-code", code);
		_commentCode = code;
	}

	this.getWriterElem = function() {
		return _writerElem;
	}

	this.getWriterVal = function() {
		return _writerElem.data("writer");
	}

	this.setWriterVal = function(val) {
		_writerElem.data("writer", val).attr("data-writer", val).text(val);
	}

	this.getUserCommentElem = function() {
		return _userCommentElem;
	}
	this.getUserCommentVal = function() {
		return _userCommentElem.data("commentContent");
	}

	this.setUserCommentVal = function(val) {
		_userCommentElem.text(val).attr("data-comment-content", val).data("commentContent", val);
	}

	this.getInsertDateElem = function() {
		return _insertDateElem;
	}

	this.getInsertDateVal = function() {
		return _insertDateElem.data("insertDate");
	}

	this.setInsertDateVal = function(val) {
		_insertDateElem.text(val).attr("data-insert-date", val).data("insertDate", val);
	}

	this.getOffset = function() {
		return _commentOffset;
	}

	// 추천/비추천 버튼 눌렀을때 이벤트 적용
	this.commentElem.find("[class*=Vote]").on("click", function(e) {
		that.insertBoardCommentVote(that.getCommentCode(), $(this).data("voteValue"));

	})

}
Comment.prototype.hide = function() {
	this.commentElem.hide();
}

Comment.prototype.show = function() {
	this.commentElem.show();
}

// 값을 리턴받아서 table의 값을 변경해주는 함수
Comment.prototype.insertBoardCommentVote = function(commentCode, voteValue) {
	const that = this;

	$.post("/insertBoardCommentVote.ajax", {
		comment_code : commentCode,
		comment_val : voteValue
	}, function(data) {
		const result = data.result;

		that.setUpVoteVal(that.getUpVoteVal() + result.up_vote);
		that.setDownVoteVal(that.getDownVoteVal() + result.down_vote);

		/*
		 * const upVote = that.commentElem.find(".comment__upVote i"); const
		 * downVote = that.commentElem.find(".comment__downVote i");
		 * 
		 * upVote.text(parseInt(upVote.text()) + result.up_vote);
		 * downVote.text(parseInt(downVote.text()) + result.down_vote);
		 */

	})

}

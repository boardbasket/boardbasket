var articleCollapse = (function(elem) {
	var _instance;
	var elem = elem;
	var menu = elem.find(".article__menu");
	var keyword = elem.find(".articles__keyword >").map(function(val,i){
		return $(this).data("keyword");
	}).toArray();	
	
	//ajax데이터로 article을 만듬
	function makeArticle(data, type) {

		const articleList = data.result ? data.result.articleList : data.articleList;
		const last = data.result?data.result.last:data.last;
		const $fragment = $(document.createDocumentFragment());		

		articleList.forEach(function(data) {

			switch (type) {
			case "news":
				$fragment.append($(`<tr class="articles__article">
										<td scope="row" class="d-none d-md-table-cell"><span class="badge" style="background:${data.site_color};color:white;">${data.site_name}<a href="${data.site_address}"><i class="fas fa-external-link-alt" aria-hidden="true"></i></a></span></td>
										<td>
											<div class="row">											
												<div class="col-12 short-text">												
													<a class="articles__address" href="${data.content_address}" target="_blank">${data.title}</a>
												</div>
												<div class="col d-xs-flex d-sm-flex d-md-none"><span class="badge" style="background:${data.site_color};color:white;">${data.site_name}<a href="${data.site_address}"><i class="fas fa-external-link-alt" aria-hidden="true"></i></a></span></div>												
												<div class="col small text-muted"><i class="fa-solid fa-calendar fa-xs"></i>${data.d}</div>
											</div>
										</td>
									</tr>`));
				
				
				break;
			case "community":
				
				$fragment.append($(`<tr class="articles__article" data-y="${data.y }" data-m="${data.m }" data-w="${data.w }" data-d="${data.d }">
									<td scope="row" class="d-none d-md-table-cell"><span class="badge" style="background:${data.site_color};color:white;">${data.site_name }<a href="${data.site_address }"><i class="fas fa-external-link-alt"></i></a></span></td>
									<td>
										<div class="row">
											<div class="col-12 short-text">
												<a class="articles__address" href="${data.article_address }" target="_blank">${data.title }</a>
											</div>
											<div class="col-12">
												<div class="row small text-muted">
													<div class="col d-xs-block d-sm-block d-md-none">
														<span class="badge" style="background:${data.site_color};color:white;">${data.site_name }<a href="${data.site_address }"><i class="fas fa-external-link-alt" aria-hidden="true"></i></a></span>
													</div>											
													<div class="col">
														<span>${data.board_name }</span> <a href="${data.board_address}"> <i class="fas fa-external-link-alt fa-xs"></i></a>
													</div>
													<div class="col">
														<i class="fa-solid fa-thumbs-up fa-xs"></i> ${data.recommend }
													</div>
													<div class="col">
														<i class="fa-solid fa-eye fa-xs"></i>${data.view_cnt}</div>
													<div class="col">
														<i class="fa-solid fa-download fa-xs"></i>${data.d}</div>
												</div>
											</div>
										</div>
									</td>
								</tr>`));
				break;
			}

			
		});
		
		if(last)
			$fragment.append(`<tr><td class='text-center border-top border-white' colspan='${type=='news'?3:4}'></td></tr>`);		
		
			

		return $fragment;
	}

	//메뉴 이벤트
	elem.find(".articles__menu input").on("click", function() {
		const type = $(this).data("type");
		const targetContent = $(`.articles__content[data-type=${type}]`);
		if (targetContent.length > 0)
			targetContent.addClass("show").siblings(".articles__content").removeClass("show");
		else {
			$(`.articles__content`).addClass("show");
		}

	})
	
	function highlightKeyword(){
		//기사, 게시글에서 검색어를 하이라이트 해줌 
		elem.find(".articles__address").each(function(idx,value){
			const target = keyword||searchBar.getKeywords();
			let text = $(value).text();
			
			target.forEach(function(targetKeyword){
				text= text.replace(new RegExp(targetKeyword, "gi"), `<strong>${targetKeyword}</strong>`);	
			});
			
			$(value).html(text);
		})
	}
	

	elem.find(".article__load >button").on("click", function(e) {
		const target = $(e.currentTarget);
		const params = target.data();
		
		const tbody = target.closest("tbody");
		params.weekList = [ {
			year : params.year,
			month : params.month,
			week : params.week,
			day:moment(params.day+"").toDate()
		} ];
		
		if(!params.board_name)
			params.board_name= keyword;
		
		if(params.board_name!=null &&Array.isArray(params.board_name))			
			params.board_name  = params.board_name.join(' ');

		getNewsCommunityContent(params).then(function(data) {
							
			if (params.offset == 0) {
				tbody.find(".articles__article").remove();
				tbody.find(".article__load").parent().before(makeArticle(data,params.type));
			}else{
				tbody.find(".article__load").parent().before(makeArticle(data,params.type));					
			}				
			params.offset=params.offset+1;
			if(data.result.last){
				target.parent().css("display","none");
				target.closest(".collapse").find(".articles__notLast").remove();
			}
				
			
			highlightKeyword();
			
		});
	})
	
	highlightKeyword();

	function init() {

		return {

		}

	}

	return {
		getInstance : function() {
			if (!_instance)
				_instance = init();
			return _instance;
		}
	}

})($(".articles"));

function RadarChart(elem, option) {
	var that = this;
	this.chartElement = elem;
	this.labelData;
	this.chartData = {};
	this.chart;
	const initData = this.getInitData();
	this.canvas = this.chartElement.find("canvas")[0];
	this.timeUnitOption = this.chartElement.find(".option__timeUnit");

	this.menu = new ChartMenu(this.chartElement, {
		tagMenu : false,
		multiTagSelect : false,
		onTagClick : function(target) {

			const selectedTag = that.menu.getSelectedTagValue().join("");
			const timeUnit = that.getSelectedTimeUnit();
			if (selectedTag) {
				let targetData = that.getChartData(selectedTag, timeUnit);
				if (targetData) {
					that.labelData = option.labelData || that.getLabelFromData(targetData);
					that.drawChart(targetData, selectedTag && [ selectedTag ], timeUnit);

				} else {
					const params = {
						site_code : that.menu.getSelectedSiteData(),
						timeMode : that.menu.getTimeMode(),
						board_name : selectedTag
					}
					if (!that.isDefaultTimeUnit())
						params.weekList = that.menu.getSelectedTimeData();

					that.getRadarChartDataAjax(params).done(function(data) {
						if (data.result) {
							targetData = that.parseRawData(data.result);
							setChartData(targetData, selectedTag, timeUnit);
							that.labelData = option.labelData || that.getLabelFromData(targetData);
						}
						that.drawChart(targetData, [ selectedTag ], timeUnit);
					});
				}
			} else {
				that.chart && that.chart.destroy();
			}

		}
	});

	this.timeUnitOption.on("click", function() {

		$(this).addClass("selected").siblings().removeClass("selected");

		const timeUnit = that.getSelectedTimeUnit();

		const selectedTag = that.menu.getSelectedTagValue().join("");
		let targetData = that.getChartData(selectedTag, timeUnit);
		if (targetData) {
			that.labelData = option.labelData || that.getLabelFromData(targetData);
			that.drawChart(targetData, selectedTag && [ selectedTag ], timeUnit);

		} else {
			const params = {
				site_code : that.menu.getSelectedSiteData(),
				timeMode : that.menu.getTimeMode(),
				board_name : selectedTag
			}
			if (!that.isDefaultTimeUnit())
				params.weekList = that.menu.getSelectedTimeData();

			that.getRadarChartDataAjax(params).done(function(data) {
				if (data.result) {
					targetData = that.parseRawData(data.result);
					setChartData(targetData, selectedTag, timeUnit);
					that.labelData = option.labelData || that.getLabelFromData(targetData);
				}
				that.drawChart(targetData, [ selectedTag ], timeUnit);
			});
		}

	});

	if (option) {
		this.labelData = option.labelData;
		if (option.init && initData) {
			const timeUnit = that.getSelectedTimeUnit();
			const targetData = this.parseRawData(initData);
			this.labelData = this.labelData || this.getLabelFromData(this.chartData);
			const selectedTag = [ this.labelData.slice(0, 1).join('') ];

			setChartData(targetData, selectedTag, timeUnit);
			this.drawChart(this.getChartData(selectedTag), selectedTag, timeUnit);

		} else {
			const targetLabelList = this.labelData;
			const params = {
				site_code : that.menu.getSelectedSiteData(),
				timeMode : that.menu.getTimeMode()
			}
			if (!that.isDefaultTimeUnit())
				params.weekList = that.menu.getSelectedTimeData();

			const selectedTag = params.board_name = targetLabelList && targetLabelList.slice(0, 1).join('');
			if (selectedTag) {
				this.getRadarChartDataAjax(params).done(function(data) {
					if (data.result) {
						const targetData = that.parseRawData(data.result);
						setChartData(targetData, selectedTag, timeUnit);
						this.labelData = option.labelData || this.getLabelFromData(this.getChartData(selectedTag, timeUnit));
					}
					that.drawChart(that.getChartData(selectedTag), [ selectedTag ], timeUnit);
				});
			} else
				that.drawChart({}, null, null);

		}
	}

	function setChartData(data, selectedTag, timeUnit) {

		const depth1 = that.chartData[selectedTag];

		if (!depth1)
			that.chartData[selectedTag] = {};

		if (data) {
			if (!that.chartData[selectedTag][timeUnit])
				that.chartData[selectedTag][timeUnit] = data;
			else
				that.chartData[selectedTag][timeUnit].datasets = that.chartData[selectedTag][timeUnit].datasets.concat(data.datasets);
		} else
			that.chartData[selectedTag][timeUnit] = {};//빈 오브젝트 삽입

		//return that.chartData[selectedTag][timeUnit];
	}

}

RadarChart.prototype.getSelectedTimeUnit = function() {
	const selected = this.timeUnitOption.filter(".selected");
	return selected.length > 0 ? selected.eq(0).data("timeUnit") : null;
}

RadarChart.prototype.getChartData = function(selectedTag, timeUnit) {

	try {
		if (selectedTag)
			return this.chartData[selectedTag][timeUnit];
		else
			return null;
	} catch (e) {
		return null;
	}

}

RadarChart.prototype.isDefaultTimeUnit = function() {
	return this.timeUnitOption.filter(".selected[data-default=true]").length > 0;
}

RadarChart.prototype.drawChart = function(data, selectedTag, timeUnit) {
	var that = this;
	const targetData = data || this.chartData[selectedTag][timeUnit];
	setTimeout(function() {
		that.destroyChart();

		if (typeof targetData.maxVal !== 'undefined' && targetData.maxVal != 0) {
			appendNoDataElement(that.chartElement, false);
			that.chart = new Chart(that.canvas, that.getConfig(targetData));
		} else
			appendNoDataElement(that.chartElement, true);

		that.menu.addTagElement(that.labelData || that.getLabelFromData(), selectedTag);

	}, 0);
}

RadarChart.prototype.destroyChart = function(idx) {
	this.chart && this.chart.destroy();
	return this;
}

RadarChart.prototype.getLabelFromData = function() {
	return this.chartData.datasets.map(function(data) {
		return data.label;
	})
}

RadarChart.prototype.updateChart = function() {
	this.chart && this.chart.destroy();
	this.drawChart();
}

RadarChart.prototype.getRadarChartDataAjax = function(params) {

	return $.ajax({
		type : "GET",
		url : params.weekList && params.weekList.length > 0 ? '/getRadarChartData.ajax' : '/getBaseRadarChartData.ajax',
		data : getUrlparameter(params),
		dataType : "json",
		traditioanl : true
	});
}

RadarChart.prototype.getInitData = function() {

	if (this.chartElement.find("[data-init]").length > 0) {
		const labels = this.chartElement.find("[data-label]").map(function(idx, item) {
			return $(item).data("label");
		}).toArray();

		const hashtags = this.chartElement.find(".hashtag").map(function(idx, item) {
			return $(item).data();
		})

		const groupedData = _.groupBy(hashtags, "type");
		const result = {
			labels : labels,
			radarTag : groupedData
		}

		return result;
	} else
		return null;

}

RadarChart.prototype.parseRawData = function(rawData) {

	const that = this;

	if (!rawData.radarTag)
		return;

	var data = {
		labels : [],
		datasets : [],
		maxVal : 0
	}

	data.labels = rawData.labels;

	const categorys = Object.keys(rawData.radarTag);
	const groupedData = _.groupBy(categorys.flatMap(function(key) {
		return rawData.radarTag[key];
	}).map(function(tag) {
		if (tag.v > data.maxVal)
			data.maxVal = tag.v;
		return tag;
	}), "t");

	const colorKey = that.getChartData() ? Object.keys(that.getChartData()).length : 0;
	const keys = Object.keys(groupedData);
	keys.forEach(function(key, idx) {

		const color = chartColor.getInstance().getDataColor(colorKey);
		data.datasets.push({
			label : key,
			data : groupedData[key].map(function(item, idx) {
				return item.v;
			}),
			fill : true,
			backgroundColor : color + "80",
			borderColor : color,
			pointBackgroundColor : color,
			pointBorderColor : '#fff',
			pointHoverBackgroundColor : '#fff',
			pointHoverBorderColor : color
		})

	})

	return data;

}

RadarChart.prototype.getConfig = function(targetData) {

	return {
		type : 'radar',
		data : targetData,
		options : {
			plugins : {
				title : {
					display : true,
					text : "태그 속성",
					font : {
						weight : "bold"
					}
				}
			},
			maintainAspectRatio : false,
			elements : {
				line : {
					borderWidth : 3
				}
			},
			scales : {
				r : {
					suggestedMin : 0,
					suggestedMax : targetData.maxVal
				}
			}
		},
		plugins : {
			tooltip : {
				enabled : true,
				position : 'nearest'
			}
		}
	};
}
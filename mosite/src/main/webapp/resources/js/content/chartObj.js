function ChartObj(elem, option) {

	ChartObj.index++;
	TagData.call(this);

	this.element = elem instanceof jQuery ? elem : $(elem);
	this.element = this.element.hasClass("chart") ? this.element : this.element.find(".chart");
	this.chart;
	this.chartContent = this.element.find(".chart__elemet");
	if (this.chartContent.length <= 0) {
		this.chartContent = $("<div class='chart__content'></div>");
		this.element.append(this.chartContent);
	}
	this.chartElement = this.element.find("canvas")[0] || this.element.find(".chart__content").append("<canvas>").find("canvas")[0];
	this.compareElem;// 비교,이어보기 토글 버튼
	// chart의 parent(board)
	this.parent = option && option.parent;
	this.boardCode = this.parent ? this.parent.id : null;//ChartObj.index;

	var that = this;

	this.prevSelectedPoint;//mobile일때 사용하는 이전 클릭 포인트 변수

	// menu 객체
	this.menu = new ChartMenu(this.element, this.updateOption(option ? option.menu : {}, this.getDefaultOption()));

	// 메뉴 선택할때 딜레이 주는 타이머
	var _selectMenuTimer;

	// line 차트의 데이터 비어있는 곳을 점선으로 표현하는 함수
	this.skipped = function(ctx, value) {
		const timeMode = that.menu.getTimeMode();

		let rd1;
		let rd2;
		try {
			rd1 = ctx.p0.$context.raw.rawData;
			rd2 = ctx.p1.$context.raw.rawData;
		} catch (e) {
			return value;
		}

		if (typeof rd1 === 'undefined' || typeof rd2 === 'undefined')
			return value;

		const yearDiff = Math.abs(rd2.y - rd1.y);
		const monthDiff = Math.abs(rd2.m - rd1.m);
		const weekDiff = timeMode == "month" && Math.abs(rd2.w - rd1.w);
		const dayDiff = timeMode == "week" && moment.duration(moment(rd2.d).diff(moment(rd1.d))).asDays();

		switch (timeMode) {
		case "year":
			//연도 1차이 이하이고 월 1차이면 이어서 , 아님 skip
			if (((yearDiff == 1) && (monthDiff == 11)) || ((yearDiff == 0) && (monthDiff == 1)))
				return undefined;
			else
				return value;
		case "month":
			if (((yearDiff == 1) && (monthDiff == 11)) || ((yearDiff == 0) && (monthDiff == 0) && (weekDiff == 1)))
				return undefined;
			else
				return value;
		case "week":
			if (dayDiff <= 1)
				return undefined;
			else
				return value;
		}

	}

	// 메뉴 클릭했을때 이벤트
	this.menuClickEvent = function(e) {

		clearTimeout(_selectMenuTimer);
		_selectMenuTimer = setTimeout(function() {
			const filteredParams = that.getNonExistDatasetKey(that.getHashtagParams());

			if (filteredParams == null) {
				that.drawChart();
			} else {
				// 라인모드일때는 검색결과를 불러와야 하므로 parameter에 포함시킴
				if (that.getChartType() === "line")
					filteredParams.keyword = filteredParams.tagNameList; //!= null ? filteredParams.tagNameList.join(",") : null;

				getHashtagData(filteredParams).done(function(data) {
					that.setTagData(filteredParams, data).drawChart();
					that.parent && that.parent.wordCloud.setWordData(filteredParams, data);
				})
			}

		}, 1000);
	}

	//비교보기 버튼<->이어보기 버튼 바꾸는 함수
	this.toggleCompareBtn = function(flag) {
		$(this).data("compare", flag);
		flag ? that.compareElem.attr("title", "이어서 보기").html('<i class="fa-solid fa-arrow-trend-down"></i>') : that.compareElem.attr("title", "비교해 보기").html('<i class="fa-solid fa-shuffle"></i>');
		that.compareElem.data("compare", flag);
	}

	if (that.menu.isSimpleMenu()) {

		that.compareElem = $("<button class='option__toggleCompare btn btn-secondary btn-sm' data-compare='false'><i class='fa-solid fa-shuffle'></button>");
		that.menu.getHoverMenuWrapper().prepend(that.compareElem);

		that.compareElem.on("click", function(e) {
			const nextFlag = !$(this).data("compare");
			that.toggleCompareBtn(nextFlag);
			that.menuClickEvent(e);
		})

	}

	// 데이터셋을 만들어 넘겨줌
	this.makeLineChartDataSetInner = function(data) {
		const tagData = data.tagData;
		const timeMode = data.timeMode;
		const labels = data.labels;
		const colorIdx = data.colorIdx;
		const keyData = data.keyData;
		let todayAnnotationData = null;
		const datasets = new Array();
		let now = moment().startOf('day'); // 오늘 데이터를 포함하는지 확인 위한 moment 변수
		const isCompare = data.isCompare;

		const group = _.groupBy(tagData, "t");// 태그로 그룹핑
		const tags = Object.keys(group);// 그룹핑된 태그들

		tags.forEach(function(k, idx) {
			const dataset = new Object(); // 리턴 할 오브젝트
			const data = new Array();// 가공 다 된 데이터 리스트

			const groupVal = new Array();// x:시간,y:값 오브젝트 리스트

			group[k].forEach(function(j) {// 받아온 데이터를 x,y 형태로 변환
				const temp = new Object();

				switch (timeMode) {
				case "year":
				case "month": {
					const labelObj = that.makeTimeLabels({
						timeMode : timeMode,
						tag : j,
						isCompare : isCompare
					});
					temp.x = labelObj.label;
					if (!todayAnnotationData) {
						const todayLabel = that.makeTimeLabels({
							timeMode : timeMode,
							tag : nowTimeSet,
							isCompare : isCompare
						}).label;
						if (isCompare) {
							if (nowTimeSet.year == j.y && nowTimeSet.month == j.m && nowTimeSet.week == j.w)
								todayAnnotationData = (todayLabel === temp.x) ? {
									x : todayLabel,
									y : j.f,
									type : "label"
								} : null;
						} else
							todayAnnotationData = (todayLabel === temp.x) ? {
								x : todayLabel,
								type : "line"
							} : null;
					}
					break;
				}
				case "week":
					temp.x = isCompare ? new moment(j.d).format("dd") : new moment(j.d).startOf('day');

					if (!todayAnnotationData) {
						if (isCompare) {
							if (temp.x === now.format("dd") && moment(j.d).isSame(now))
								todayAnnotationData = temp.x === now.format("dd") ? {
									x : temp.x,
									y : j.f,
									type : "label"
								} : null;
						} else
							todayAnnotationData = temp.x.isSame(now) ? {
								x : temp.x,
								type : "line"
							} : null;
					}
					break;
				}
				temp.rawData = j;
				temp.y = parseInt(j.f);
				groupVal.push(temp);
			})

			//  x축을 만들고 y값은 undefined로 세팅
			labels.forEach(function(item) {
				const frag = {
					x : item,
					y : undefined
				};
				// x,y형태의 데이터 중 1일단위 x축 값이랑 일치하는 x값이 있으면 y값 세팅
				for (let j = 0; j < groupVal.length; ++j) {
					//groupVal[j].x === item
					var equals = false;
					switch (timeMode) {
					case "year":
					case "month":
						equals = groupVal[j].x == item;
						break;
					case "week":
						equals = isCompare ? groupVal[j].x === item : groupVal[j].x.isSame(item);
					}
					if (equals) {// 값이 존재하면
						if (typeof frag.y === "undefined")
							frag.y = 0;// 더해주기 위해 0으로 초기화
						frag.y += groupVal[j].y;
						frag.rawData = groupVal[j].rawData;
					}
				}

				data.push(frag);
			})

			dataset.borderColor = isCompare ? chartColor.getInstance().getDataColor(colorIdx) : chartColor.getInstance().getDataColor(idx);
			dataset.data = data;
			dataset.label = isCompare ? (timeMode === 'year' ? keyData.year + "년" : (timeMode === "month" ? keyData.year + "년 " + keyData.month + "월" : keyData.year + "년 " + keyData.month + "월 "
					+ keyData.week + "주차")) : k;
			dataset.segment = {
				borderColor : function(ctx) {
					return that.skipped(ctx, 'rgb(0,0,0,0.2)');
				},
				borderDash : function(ctx) {
					return that.skipped(ctx, [ 6, 6 ]);
				}
			}
			dataset.fill = false;
			dataset.spanGaps = true;
			datasets.push(dataset);
		})

		return {
			datasets : datasets,
			todayAnnotationData : todayAnnotationData
		};
	}

	// 옵션의 초기화 설정을 넣으면 데이터를 받아서 그리기까지 함
	if (option) {

		if (option.init) {
			this.init(option);
		}
	}

}

ChartObj.prototype = Object.create(TagData.prototype);
ChartObj.prototype.constructor = TagData;

ChartObj.prototype.init = function(option) {
	const that = this;
	const params = that.getHashtagParams();
	const initData = that.getInitData(that.element, params);

	if (!initData.hashtagData) {

		getHashtagData(params).done(function(data) {

			that.setTagData(params, data);

			that.drawChart(option);

		})
	} else {
		that.setTagData(params, initData);
		that.drawChart(option);
	}
}

// 차트의 타입을 가져오는 함수, 선택된 태그가 있으면 line 아니면 bar 리턴
ChartObj.prototype.getChartType = function() {

	return this.type || this.menu.getSelectedTagValue().length > 0 ? "line" : "bar";

}

ChartObj.prototype.destroyChart = function(idx) {
	this.chart && this.chart.destroy();
	return this;
}

ChartObj.prototype.refreshChart = function() {
	this.chart.update();
	return this;
}

//tagMenu로 불러온 데이터를 TagData에 넣고 , menu 만들고, 키워드 trigger 해서 보여주는 함수
ChartObj.prototype.tagMenuTimeSetBtnCallback = function(data, params, targetKeyword) {
	const that = this;

	that.menu.resetAllTagElement();
	that.menu.getTagWithName(targetKeyword).addClass("option__tag--selected");

	if (data) {
		this.setTagData(params, data);
		this.menu.deselectAllTimeOption();
		this.menu.setTimeMode(params.timeMode);
		params.weekList.forEach(function(time) {

			let elem = that.menu.getTimeOption(time.year, time.month, time.week);
			if (elem == null || elem.length <= 0) {
				elem = that.menu.makeTimeMenu(params.timeMode, time);
				that.menu.addTimeMenu(elem, params.timeMode);
			}

			that.menu.selectTimeOption(time.year, time.month, time.week);

		})

	}
	this.drawChart();

}

// bar 형태의 데이터를 만드는 함수
ChartObj.prototype.makeBarDataSet = function(chartData) {

	const keys = Object.keys(chartData);// 데이터셋의 갯수
	const that = this;
	const timeMode = that.menu.getTimeMode();
	const labels = _.uniq(keys.map(function(key) {
		return chartData[key] || new Array();
	}).flatMap(function(item) {
		return item;
	}).map(function(tag) {
		return tag.t;
	}));

	let result = new Object();// 리턴할 객체
	result.labels = labels;// 차트의 라벨들
	result.datasets = new Array();// 차트의 데이터셋들
	result.type = "bar";// 차트의 타입
	result.isCompare = that.isCompare();
	result.stacked = !result.isCompare;//keys.length > 1 && !that.isCompare() ? true : false;// 데이터셋이 여러개면 stack
	result.chartOption = this.getBarChartOption({
		stacked : result.stacked,
		isCompare : result.isCompare
	})

	keys.forEach(function(key, idx) {
		const keyData = JSON.parse(key);
		const tagList = chartData[key];

		const newData = new Array();

		const group = _.groupBy(tagList, "t");
		const tagNames = Object.keys(group);
		const dataset = new Object();
		const data = Array.apply(null, new Array(labels.length)).map(function() {
			return 0;
		})

		tagNames.forEach(function(k) {
			let val = group[k].reduce(function(acc, curr, i) {
				return acc + parseInt(curr["f"]);
			}, 0);

			const labelIdx = labels.indexOf(k);
			data[labelIdx] = labelIdx >= 0 ? val : 0;

		})

		dataset.data = data;
		switch (timeMode) {
		case "year":
			dataset.label = keyData.year + "년";
			break;
		case "month":
			dataset.label = keyData.month + "월";
			break;
		case "week":
			dataset.label = keyData.month + "월 " + (keyData.week == 0 ? '' : keyData.week + "주차");
			break;
		}

		dataset.backgroundColor = chartColor.getInstance().getDataColor(idx);
		result.datasets.push(dataset);
	})

	return result;

}

// 이어보기 형태의 lineChart 데이터를 만드는 함수
ChartObj.prototype.commonLineChartDataSet = function(rawData) {
	const that = this;
	const keys = Object.keys(rawData);
	const timeMode = that.menu.getTimeMode();//시간 단위	
	let result = new Object();// 리턴할 객체
	result.labels = new Array();
	result.datasets = new Array();// 차트의 데이터셋들
	result.type = "line";// 차트의 타입
	let labelColors = new Array();//timeSet에 따른 라벨의 색
	let hashtagList = new Array();
	let todayAnnotationData = new Array();// 오늘 데이터 포함하는지 flag	
	const isCompare = that.isCompare();//비교보기 flag

	// rawdata들의 데이터들을 합쳐주면서 동시에 시간 label을 만듬
	try {
		keys.forEach(function(key) {

			let start;
			let end;

			switch (timeMode) {
			case "year":
			case "month": {
				let data;
				try {
					data = rawData[key];

					data.forEach(function(tag) {
						const labelObj = that.makeTimeLabels({
							timeMode : timeMode,
							tag : tag,
							isCompare : isCompare
						});
						if (result.labels.filter(function(label) {
							return labelObj.label === label;
						}).length <= 0) {
							result.labels = result.labels.concat(labelObj.label);
							labelColors.push(labelObj.color);
						}
					})
					break;
				} catch (e) {
				}
			}
			case "week": {
				let data;
				try {
					data = rawData[key][0];

					start = moment(data.d).isoWeekday(1);
					end = moment(data.d).isoWeekday(7);
					const nextLabel = that.makeTimeLabels({
						timeMode : timeMode,
						start : start,
						end : end,
						week : data.w,
						month : data.m,
						isCompare : isCompare
					});
					result.labels = result.labels.concat(nextLabel.label);
					labelColors = labelColors.concat(nextLabel.color);

					break;
				} catch (e) {
				}
			}
			}

			if (rawData[key])
				hashtagList = hashtagList.concat(rawData[key]);

		})
	} catch (e) {
	}

	result.labels.sort(function(a, b) {
		if (typeof a === "string")
			return a.localeCompare(b);
		else
			return a.isAfter(b) ? 1 : -1;
	});

	const datasetObj = that.makeLineChartDataSetInner({
		tagData : hashtagList,
		labels : result.labels,
		timeMode : timeMode
	});

	result.datasets = datasetObj.datasets;
	if (datasetObj.todayAnnotationData)
		todayAnnotationData.push(datasetObj.todayAnnotationData);
	result.chartOption = this.getLineChartOption({
		timeMode : timeMode,
		todayAnnotationData : todayAnnotationData,
		labelColors : labelColors,
		isCompare : isCompare
	});// 라인차트 옵션 넣어줌
	return result;

}

//비교보기 형식의 lineChart 데이터를 리턴하는 함수
ChartObj.prototype.compareLineChartData = function(rawData) {
	const that = this;
	const keys = Object.keys(rawData).sort(function(a, b) {
		return a.localeCompare(b);
	});
	const timeMode = that.menu.getTimeMode();//시간 단위	
	let result = new Object();// 리턴할 객체
	result.labels = new Array();
	result.datasets = new Array();// 차트의 데이터셋들
	result.type = "line";// 차트의 타입
	let todayAnnotationData = new Array();// 오늘 데이터 포함하는지 flag	
	let isCompare = that.isCompare();
	let labelColors = new Array();//timeSet에 따른 라벨의 색
	try {
		keys.forEach(function(key, idx) {
			const keyData = JSON.parse(key);
			const dataset = new Object(); // 리턴 할 오브젝트
			let start;
			let end;

			switch (timeMode) {
			case "year": {
				result.labels = [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ];
				for (let i = 1; i <= 12; ++i)
					labelColors.push(chartColor.getInstance().getColor(i));
				break;
			}
			case "month": {
				const data = rawData[key];
				/*
				 * if (result.labels.length < data.length) result.labels =
				 * data.map(function(item) { return item.w + "주차"; })
				 */
				result.labels = [ '1주차', '2주차', '3주차', '4주차', '5주차' ];
				break;
			}
			case "week": {
				result.labels = [ '월', '화', '수', '목', '금', '토', '일' ];
			}
			}

			const datasetObj = that.makeLineChartDataSetInner({
				tagData : rawData[key],
				labels : result.labels,
				timeMode : timeMode,
				isCompare : isCompare,
				colorIdx : idx,
				keyData : keyData
			})
			if (datasetObj.todayAnnotationData)
				todayAnnotationData.push(datasetObj.todayAnnotationData);
			datasetObj.datasets.forEach(function(datasets) {
				result.datasets.push(datasets);
			})

		})
	} catch (e) {
		console.log(e);
	}

	// 라인차트 옵션 넣어줌
	result.chartOption = this.getLineChartOption({
		timeMode : timeMode,
		todayAnnotationData : todayAnnotationData,
		labelColors : labelColors,
		isCompare : isCompare
	});
	return result;
}

// 시간 단위에 맞추어 label 리스트를 만들어 리턴
ChartObj.prototype.makeTimeLabels = function(data) {

	let result = new Object();
	const isCompare = data.isCompare;
	switch (data.timeMode) {
	case "year":
		return {
			label : `${isCompare?'':((data.tag.y||data.tag.year)+'년 ')}` + `${data.tag.m||data.tag.month}`.padStart(2, 0) + `월`,
			color : chartColor.getInstance().getColor(data.tag.m || data.tag.month)
		}
	case "month":
		return {
			label : isCompare ? `${data.tag.w||data.tag.week}주차` : `${((data.tag.y||data.tag.year)+'년 ')}` + `${data.tag.m||data.tag.month}`.padStart(2, 0) + `월 ${data.tag.w||data.tag.week}주차`,
			color : chartColor.getInstance().getColor(data.tag.m || data.tag.month, data.tag.w || data.tag.week)
		}
	case "week": {
		let label = new Array();
		let color = new Array();
		const now = moment().startOf('day');
		const idx = Math.floor(moment.duration(moment(data.end).diff(moment(data.start))).asDays());

		for (let i = idx; i >= 0; i--) {
			label.push(moment(data.end).subtract(i, 'day').startOf('day'));
			color.push(chartColor.getInstance().getColor(data.month, data.week))
		}

		return {
			label : label,
			color : color
		}

	}
	}

}

ChartObj.prototype.makeChartDataSet = function(chartData, type) {
	const that = this;
	let result = new Object();

	result.type = type || this.getChartType();

	switch (result.type) {
	case "bar":
		result = that.makeBarDataSet(chartData);
		break;
	case "line":
		result = that.isCompare() ? that.compareLineChartData(chartData) : result = that.commonLineChartDataSet(chartData);
		break;
	default:
		break;
	}

	return result;
}

// 태그,차트를 그림
ChartObj.prototype.drawChart = function(option) {

	const params = this.getHashtagParams();
	params.tagNameList = option && option.keywordInit ? option.keywordInit : params.tagNameList;
	const tagData = option && option.targetData || this.getTagData(params);
	const chartData = this.makeChartDataSet(tagData, (option && option.keywordInit) && "line");// 차트만들 데이터
	const selectedTag = _.cloneDeep(params.tagNameList);
	delete params.tagNameList;
	const labelData = this.getUniqueTagNameList(params);

	// 해당 시간대에 맞는 태그들을 다시 추가해줌
	this.menu.addTagElement(labelData, selectedTag);
	//데이터가 없으면 없다는 표시 해줌
	appendNoDataElement(this.element, chartData.datasets.length <= 0 || chartData.datasets.flatMap(function(item) {
		return item.data;
	}).length <= 0);

	const that = this;
	setTimeout(function() {

		that.destroyChart();

		that.chart = new Chart(that.chartElement, {
			type : chartData.type,
			data : {
				datasets : chartData.datasets,
				labels : chartData.labels
			},
			options : chartData.chartOption
		});

	}, 0);

}

// bar 형태의 그래프의 옵션을 리턴하는 함수
ChartObj.prototype.getBarChartOption = function(option) {
	const that = this;
	const stacked = option.stacked;
	const isCompare = option.isCompare;

	return {
		maintainAspectRatio : false,
		scales : {
			x : {
				stacked : stacked,
			},
			y : {
				display : true,
				stacked : stacked,
				title : {
					text : "value",
					font : {
						family : 'Times',
						size : 10,
						style : 'normal',
						lineHeight : 1.2
					}
				}
			}
		},
		interaction : {
			intersect : false,
			mode : isCompare ? 'index' : 'point'
		},
		onHover : function(event) {
			var point = this.getActiveElements(event);
			if (point.length > 0)
				event["native"]["target"]["style"]["cursor"] = "pointer";
			else
				event["native"]["target"]["style"]["cursor"] = "default";
		},
		onClick : function(event) {

			const currSelectedPoint = that.chart.getActiveElements(event)[0];
			let dataIdx = currSelectedPoint.index;
			let dataSetIdx = currSelectedPoint.datasetIndex;
			let tagName = that.chart.data.labels[dataIdx];

			if (isMobile()) {
				if (!that.prevSelectedPoint) {
					that.prevSelectedPoint = currSelectedPoint;
				} else {
					if (that.prevSelectedPoint.index == currSelectedPoint.index) {
						that.menu.getTagWithName(tagName).trigger("click");
						that.prevSelectPoint = null;
					} else
						that.prevSelectPoint = currSelectedPoint;

				}
			} else
				that.menu.getTagWithName(tagName).trigger("click");

		},
		plugins : {
			tooltip : {
				enabled : true,
				position : 'nearest',
				callbacks : {
					footer : function(tooltipItem) {

						return isMobile() ? '한번 더 클릭해서 메뉴 보기' : '';

					}
				}

			}
		}

	}
}

// line 형태 차트의 옵션을 리턴하는 함수
ChartObj.prototype.getLineChartOption = function(additionalOption) {
	const toolTip = $("<div>");
	const that = this;
	const todayAnnotationList = that.makeAnnotation(additionalOption.todayAnnotationData);//annotation 객체
	const timeMode = additionalOption.timeMode;
	const labelColors = additionalOption.labelColors;
	const isCompare = additionalOption.isCompare;

	const lineOption = {
		maintainAspectRatio : false,
		interaction : {
			intersect : false,
			mode : isCompare ? 'index' : 'nearest'
		},
		onHover : function(event) {
			var point = this.getActiveElements(event);
			if (point.length > 0)
				event["native"]["target"]["style"]["cursor"] = "pointer";
			else
				event["native"]["target"]["style"]["cursor"] = "default";
		},
		scales : {
			x : {
				title : {
					display : true,
					text : "date"
				}
			},
			y : {
				title : {
					display : true,
					text : "value"
				},
				beginAtZero : true

			}
		},
		onClick : function(event) {
			let pointData = event.chart.getActiveElements(event)[0];

			if (isMobile()) {
				if (!that.prevSelectedPoint) {
					that.prevSelectedPoint = pointData;
					return;
				} else {
					if (that.prevSelectedPoint.index == pointData.index && pointData.datasetIndex == that.prevSelectedPoint.datasetIndex) {
						//do nothing
					} else {
						that.prevSelectedPoint = pointData;
						pointData = null;
					}
				}

			}

			if (pointData) {
				const rect = event.chart.canvas.getBoundingClientRect();
				const rawData = event.chart.data.datasets[pointData.datasetIndex]["data"][pointData.index]["rawData"];
				const tag = isCompare ? rawData.t : event.chart.data.datasets[pointData.datasetIndex].label;
				const weekList = new Array();
				const tagList = new Array();

				tagList.push(tag);
				weekList.push({
					year : rawData.y,
					month : rawData.m,
					week : rawData.w,
					day : rawData.d && moment(rawData.d).toDate()
				})

				tagMenu.getInstance().showMenu(true, {
					pos : {
						x : rect.x + pointData.element.x,
						y : rect.y + pointData.element.y
					},
					params : {
						board_code : that.getBoardCode(),
						site_code : that.menu.getSelectedSiteData(),
						search_target : "tag",
						board_name : tag,
						timeMode : that.menu.getTimeMode() == "year" ? "month" : (that.menu.getTimeMode() == "month" ? "week" : "day"),
						weekList : weekList,
						offset : 0
					},
					chartObj : that,
					board : that.parent,
					showMenuFlag : {
						searchTimeSet : true,
						searchBoard : that.parent ? that.parent.searchAvailable ? true : false : false,
						articleArccordian : true
					}
				})

				that.prevSelectedPoint = null;
			}

		},
		plugins : {
			tooltip : {
				enabled : true,
				position : 'nearest',
				callbacks : {
					label : function(tooltipItem) {

						const rawData = tooltipItem.raw.rawData;
						let label = '';
						switch (timeMode) {
						case "year":
							label = `${rawData.y}년 ${rawData.m}월`;
							break;
						case "month":
							label = `${rawData.y}년 ${rawData.m}월 ${rawData.w}주차`;
							break;
						case "week":
							label = moment(rawData.d).format("YYYY-MM-DD (dd)");
							break;
						}

						return rawData.t + (isCompare ? `(${label})` : '') + " : " + tooltipItem.formattedValue;
					},
					title : function(tooltipItem) {

						const rawData = this.getActiveElements()[0].element.raw.rawData;

						if (!isCompare) {
							switch (timeMode) {
							case "year":
								return `${rawData.y}년 ${rawData.m}월`;
							case "month":
								return `${rawData.y}년 ${rawData.m}월 ${rawData.w}주차`;
							case "week":
								return moment(rawData.d).format("YYYY-MM-DD (dd)");
							}
						} else
							return '';
					},
					labelColor : function(context) {

						return {
							borderColor : chartColor.getInstance().getDataColor(context.datasetIndex),
							backgroundColor : chartColor.getInstance().getDataColor(context.datasetIndex)
						};

					},
					footer : function(tooltipItem) {
						if (!isCompare) {
							const target = tooltipItem[0];
							return `분석된 글 : ${target.element.raw.rawData.articleNum||' - '} 개`;
						}

					}
				}
			},
			annotation : {
				annotations : {}
			}
		}

	}

	switch (timeMode) {
	case "year":
	case "month":

		lineOption["scales"]["x"]["ticks"] = {
			callback : function(value, index, values) {
				const item = this.getLabelForValue(value);
				return item;
			}
		}

		break;
	case "week":
		if (isCompare) {
			lineOption["scales"]["x"]["ticks"] = {
				callback : function(value, index, values) {

					if (typeof this.getLabelForValue(value).format !== "undefined") {
						const label = this.getLabelForValue(value).format("dd");

						return label;

					} else
						return this.getLabelForValue(value);
				}

			}

		} else {
			lineOption["scales"]["x"]["type"] = "timeseries";
			lineOption["scales"]["x"]["time"] = {
				unit : 'day',
				tooltipFormat : "YYYY-MM-DD",
				displayFormat : {
					day : "MM월 DD일"
				}
			};
			lineOption["scales"]["x"]["ticks"] = {
				callback : function(value, index, values) {

					const label = moment(values[index].value).format("MM월 DD일");

					return label;
				}

			}

		}
		break;

	}

	todayAnnotationList && todayAnnotationList.forEach(function(annotation, idx) {

		lineOption['plugins']['annotation']['annotations'][annotation.type + (idx + 1)] = annotation;

	})

	lineOption["scales"]["x"]["ticks"]["font"] = {
		weight : 900
	}

	lineOption["scales"]["x"]["ticks"].source = "data";

	lineOption["scales"]["x"]["ticks"]["color"] = function(value, index, values) {
		return labelColors[value.index];
	}

	return lineOption;
}

ChartObj.prototype.makeAnnotation = function(annotationData) {

	const annotationList = new Array();

	annotationData && annotationData.forEach(function(data) {
		const type = data.type || "line";
		const annotation = new Object();

		annotation.type = data.type;
		if (type === "line") {
			annotation.xMin = annotation.xMax = data.x;
			annotation.borderColor = data.borderColor || "#808080";
			annotation.borderWidth = data.borderWidth || 3;
			annotation.borderDash = [ 6, 6 ];
			annotation.label = {
				content : data.content || '집계중',
				enabled : true
			}
		} else if (type === "label") {
			annotation.xValue = data.x;
			annotation.yValue = data.y;
			annotation.xAdjust = -50;
			//annotation.yAdjust = 10;
			annotation.boardRadius = "30px";
			annotation.color = "white";
			annotation.content = data.content || "집계중";
			annotation.backgroundColor = '#17A2B7';
			annotation.font = {
				size : 15
			}
			annotation.callout = {
				enabled : true,
				side : 10,
				position : "right"
			}
		}

		annotationList.push(annotation);

	})

	/**/
	return annotationList;
}
//부모의 게시판 코드를 리턴하는 함수
ChartObj.prototype.getBoardCode = function() {
	return this.parent ? this.boardCode : null;
}

//비교보기 상태인지를 리턴
ChartObj.prototype.isCompare = function() {
	return this.compareElem ? this.compareElem.data("compare") : false;
}

//
ChartObj.prototype.isCompareCondition = function() {
	//1.태그 한개 선택시에만
	const that = this;
	return this.menu.getSelectedTag().length <= 1;
}

ChartObj.prototype.showCompareMenu = function(flag) {

	flag ? this.compareElem.show() : this.compareElem.hide();
}

ChartObj.prototype.externalHtmlTooltip = function(context) {

	const chart = context.chart;
	const tooltip = context.tooltip;

	let tooltipEl = chart.canvas.parentNode.querySelector('.chart__htmlTooltip');

	// Hide if no tooltip
	if (tooltip.opacity === 0) {
		tooltipEl.style.opacity = 0;
		return;
	}

	// Set Text
	if (tooltip.body) {
		const titleLines = tooltip.title || [];
		const bodyLines = tooltip.body.map(function(b) {
			return b.lines
		});

		const tableHead = document.createElement('thead');

		titleLines.forEach(function(title) {
			const tr = document.createElement('tr');
			tr.style.borderWidth = 0;

			const th = document.createElement('th');
			th.style.borderWidth = 0;
			const text = document.createTextNode(title);

			th.appendChild(text);
			tr.appendChild(th);
			tableHead.appendChild(tr);
		});

		const tableBody = document.createElement('tbody');
		bodyLines.forEach(function(body, i) {
			const colors = tooltip.labelColors[i];

			const span = document.createElement('span');
			span.style.background = colors.backgroundColor;
			span.style.borderColor = colors.borderColor;
			span.style.borderWidth = '2px';
			span.style.marginRight = '10px';
			span.style.height = '10px';
			span.style.width = '10px';
			span.style.display = 'inline-block';

			const tr = document.createElement('tr');
			tr.style.backgroundColor = 'inherit';
			tr.style.borderWidth = 0;

			const td = document.createElement('td');
			td.style.borderWidth = 0;

			const text = document.createTextNode(body);

			td.appendChild(span);
			td.appendChild(text);
			tr.appendChild(td);
			tableBody.appendChild(tr);
		});

		const tableRoot = tooltipEl.querySelector('table');

		// Remove old children
		while (tableRoot.firstChild) {
			tableRoot.firstChild.remove();
		}

		// Add new children
		tableRoot.appendChild(tableHead);
		tableRoot.appendChild(tableBody);
	}
	const positionX = chart.canvas.offsetLeft;
	const positionY = chart.canvas.offsetTop;

	// Display, position, and set styles for font
	tooltipEl.style.opacity = 1;
	tooltipEl.style.left = positionX + tooltip.caretX + 'px';
	tooltipEl.style.top = positionY + tooltip.caretY + 'px';
	tooltipEl.style.font = tooltip.options.bodyFont.string;
	tooltipEl.style.padding = tooltip.options.padding + 'px ' + tooltip.options.padding + 'px';

}

ChartObj.prototype.getDefaultOption = function() {

	const that = this;
	const returnObj = {}

	return {
		siteMenu : false,
		tagMenu : true,
		timeMenu : true,
		limitMenu : true,
		modalMenu : true,
		zoomBtn : false,
		multiTimeSelect : true,
		multiTagSelect : true,
		initialLimit : 10,
		onSiteClick : function(target) {
			that.menuClickEvent(target);
		},
		onLimitClick : function(target) {
			that.menuClickEvent(target);
		},
		onWeekClick : function(target) {
			that.menuClickEvent(target);
		},
		onMonthClick : function(target) {
			if (that.menu.getTimeMode() === 'month')
				that.menuClickEvent(target);
		},
		onYearClick : function(target) {
			that.menuClickEvent(target);
		},
		onResetTime : function() {
			that.drawChart();
		},
		onResetTag : function() {
			that.drawChart();
		},
		onTagClick : function(target) {

			//const tag = that.menu.getTagWithName(target.data("tagName"));
			//tag.parent().scrollLeft(tag.position().left);
			that.prevSelectedPoint = null;
			const compare = that.isCompareCondition();
			compare || that.toggleCompareBtn(compare);
			that.showCompareMenu(compare);

			that.drawChart();
		},
		onModalClick : function() {
			const origin = that.element.parent();

			chartModal.getInstance().attachContent(origin, that.element.detach());
			chartModal.getInstance().showModal();

		}
	}

}
ChartObj.index = 0;
function WordCloudChanger(elem, options) {

	var that = this;
	this.elem = elem;
	//this.menu = elem.find(".wordCloudChanger__menu");
	this.items = elem.find("[data-slide]");
	this.itemContent = new Array();
	this.currentIndex = 0;
	this.currentItem;
	this.currentContent;
	this.menuTimer;
	this.menu = new ChartMenu(that.elem.find(".wordCloudChanger__menu"), {
		tagMenu : true,
		multiTagSelect : false,
		onTagClick : function(target) {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				if (that.menu.getSelectedTagValue().length <= 0) {
					that.hideAllItem();
				} else {
					that.switchTo($(target).data("index"));
				}
			}, 1000);

		}
	})

	this.init = function() {
		if (this.items.length > 0) {
			const targetElem = this.items.eq(0);
			const targetContent = that.graphInit(0);
			this.currentItem = targetElem;
			this.currentContent = targetContent;

			this.menu.addTagElement(options.labelList);
			this.menu.getTagElement().each(function(index, item) {

				$(item).attr("data-index", index);
				if (index == 0)
					$(item).trigger("click");

			})
		} else {
			appendNoDataElement(this.elem, true);
		}

		//appendLabels(makeLabels(options.labelList));
	}

	if (options && options.init) {

		that.init();
		that.isMobile = options.isMobile;
	}

	function appendLabels(target) {
		that.menu.append(target);
	}

}
WordCloudChanger.prototype.getGraph = function(idx) {
	try {
		return this.itemContent[idx];
	} catch (e) {
		return null;
	}

}

WordCloudChanger.prototype.hideAllItem = function() {
	this.items.each(function(index, item) {
		$(item).removeClass("show");
	})
	this.currentIndex = null;
}

WordCloudChanger.prototype.switchTo = function(idx) {

	if (idx == this.currentIndex)
		return;

	this.currentItem.removeClass("show");
	const targetElem = this.items[idx];
	$(targetElem).addClass("show");

	if (!this.itemContent[idx])
		this.itemContent[idx] = this.graphInit(idx);
	else
		this.currentContent = this.itemContent[idx];

	this.currentIndex = idx;
	this.currentItem = this.items.eq(idx);

}

WordCloudChanger.prototype.graphInit = function(slideNum) {
	const targetElem = this.items.eq(slideNum);
	const that = this;
	const params = searchBar.getSearchParams();
	params.board_code = null;
	params.board_name = searchBar.getKeywords()[slideNum];
	params.weekList = searchBar.menu.getInitialTimeData();

	const targetContent = new WordCloud(targetElem, that.getGraphOption({
		initParams : params
	}));
	this.itemContent.push(targetContent);
	const initData = targetContent.getWordData();
	if (initData.length>0) {
		targetContent.update();
	} else
		getRelativeTagData(params).done(function(data) {
			that.currentContent = targetContent;
			targetContent.setWordData(params, TagData.prototype.getInitData.call(null, data.result && data.result.relativeTag, params));
			targetContent.update();
		})

	return targetContent;

}

WordCloudChanger.prototype.setInitData = function(data) {

	const target = this.elem.find(".wordCloud__initData");
	const result = data.result ? data.result : data;
	const hashtagList = result.relativeTag ? result.relativeTag : (result.hashtagData ? result.hashtagData.hashtagList : result.hashtagList);

	hashtagList.forEach(function(item) {

		const keys = Object.keys(item);
		const elem = $("<div class='hashtag'></div>");
		keys.forEach(function(key) {
			elem.attr(`data-${key}`, `${item[key]}`);
		})
		target.append(elem);
	})

}

WordCloudChanger.prototype.getGraphOption = function(data) {
	const that = this;
	return {
		init : true,
		initParams : data.initParams,
		isMobile : that.isMobile,
		menu : {
			modalMenu : false,
			timeMenu : false,
			siteMenu : true,
			tagMenu : false,
			limitMenu : false,			
			initialLimit : 50
		}
	};

}
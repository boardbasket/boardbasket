function ArticleNumChart(element, option) {
	this.element = element;
	this.canvas = element.find("canvas")[0];
	this.chartData;
	this.chart;
	this.compareElem = element.find(".articleNumChart__compare");
	this.averageElem = element.find(".articleNumChart__average");
	this.generalOption = element.find(".articleNumChart__generalOption").data();
	if (option) {
		if (option.init) {
			this.init();
		}
	}

}

ArticleNumChart.prototype.draw = function(data) {

	const targetData = data ? data : this.chartData;
	const that = this;
	this.setAverageElem(targetData);
	this.setCompareElem(targetData);

	if (this.chart)
		this.chart.destroy();

	this.chart = new Chart(this.canvas, {
		type : "line",
		data : targetData,
		options : that.getOption()
	});

}

ArticleNumChart.prototype.getOption = function() {
	const that = this;
	
	return {
		maintainAspectRatio : false,
		interaction : {
			intersect : false,
			mode : 'nearest'
		},
		scales : {
			x : {
				grid : {
					display : false
				},
				title : {
					display : true,
					text : "최신화 날짜"
				}
			},
			y : {
				grid : {
					display : false,
					drawTicks : false,
					drawBorder : false
				},
				ticks : {
					display : false
				},

			}
		},
		plugins : {
			title : {
				display : true,
				text : '게시글 현황' + that.generalOption.regularArticleNoFlag ? '' : '(추정치)'
			},
			tooltip : {
				callbacks : {
					title : function(context) {
						const dataIndex = this.getActiveElements(context)[0];
						const from = dataIndex.index - 1 < 0 ? '' : this.chart.data.labels[dataIndex.index - 1];
						const to = this.chart.data.labels[dataIndex.index];
						return `${from} ~ ${to}`;

					},
					label : function(context) {
						const data = this.dataPoints[0];
						const num = data.raw > 10000 ? Math.round((data.raw / 100)) * 100 : data.raw;
						return `게시글 : 약 ${num}개 ${that.generalOption.regularArticleNoFlag?'':'이하'}`;
					}
				}
			}
		}
	}
}
ArticleNumChart.prototype.setCompareElem = function(data) {

	try {
		const targetData = data.datasets[0].data;
		const labels = data.labels;
		const fromIndex = targetData.length - 2;
		const toIndex = targetData.length - 1;
		let targetVal = Math.round((targetData[toIndex] / targetData[fromIndex] * 100));
		if (isNaN(targetVal))
			targetVal = 100;
		this.compareElem
				.find(".articleNumChart__value")
				.html(
						`${targetVal>100?(targetVal-100)+'%':targetVal==100?'':(100-targetVal)+'%'} <span class="${targetVal>100?'text-success':targetVal==100?'text-secondary':'text-danger'}">${targetVal>100?'증가':targetVal==100?'변동없음':'감소'}</span>`);

	} catch (e) {
		this.compareElem.text(`N/A`);
	}

}
ArticleNumChart.prototype.setAverageElem = function(data) {
	const interval = this.generalOption.cycleInterval;
	const lastData = data.datasets[0].data.reduce(function(acc, curr) {
		return curr
	}, 0);

	this.averageElem.find(".articleNumChart__value").text(`${Math.round(lastData/interval)}`);

}

ArticleNumChart.prototype.init = function() {

	this.chartData = this.parseInitData();
	this.draw();
}

ArticleNumChart.prototype.parseInitData = function() {
	const that = this;
	const initElem = this.element.find(`[data-init]`);
	const data = {
		datasets : new Array()
	};

	const dataList = initElem.find("[data-num]").map(function() {
		return $(this).data();
	}).sort(function(a, b) {
		return a.order - b.order;
	}).toArray();

	data.labels = dataList.filter(function(k) {
		return k.date != null;
	}).map(function(k) {
		const date = k.date;
		return date;
	});

	data.datasets.push({
		label : `게시글 갯수${that.generalOption.regularArticleNoFlag ? '' : ' (추정치)'}`,
		data : dataList.map(function(x) {
			return x.num;
		}),
		fill : false,
		borderColor : "rgb(75, 192, 192)",
		tension : 0.1
	})

	return data;

}
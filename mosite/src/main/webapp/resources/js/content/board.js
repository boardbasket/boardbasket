function BoardManager(option) {
	this.currentBoard;
	this.boardList = $.map($(".board"), function(a, index) {
		return new Board(a, option);
	});
}

BoardManager.prototype.getCurrentBoard = function() {
	return this.currentBoard;
}

BoardManager.prototype.setCurrentBoard = function(board) {

	this.currentBoard = this.getBoard(board.attr("id"));

}

BoardManager.prototype.init = function() {

	this.boardList.forEach(function(item) {
		item.init();
	});
}

BoardManager.prototype.getBoard = function(idx) {
	const result = this.boardList.filter(function(k) {
		return k.id === idx
	});

	return result.length ? result[0] : this.boardList[idx];
}

BoardManager.prototype.addBoard = function(element) {
	this.boardList.push(element);
}

BoardManager.prototype.deleteBoard = function(idx) {
	this.boardList.splice(idx, 1);
}

BoardManager.prototype.update = function() {

	const targetList = this.getActiveBoard();

	if (targetList.length > 0)
		targetList.forEach(function(target) {
			// chart는 자동으로 redraw함
			// target.chart.draw();
			//target.cloud.update();
			//target.force.update();
		})
		//검색결과 그래프 따로 업데이트
		//relativeTagWordCloud && relativeTagWordCloud.update();
	tagHeatMap && tagHeatMap.update();
}

// 컨텐츠가 보여지고 있는 board 가져옴
BoardManager.prototype.getActiveBoard = function() {
	return this.boardList.filter(function(board) {
		return board.isShowContent();
	})

}

// /-------------------------------Board

function Board(boardElement, option) {

	this.boardElem = $(boardElement);
	this.id = this.boardElem.attr("id");
	this.boardName = this.boardElem.find(".board__boardName").data("boardName");
	this.commentTimer;
	this.commentForm = this.boardElem.find(".board__commentForm");
	var _evalWrapper = this.boardElem.find(".board__evalWrapper");
	this.popularTag = this.boardElem.find(".board__popularTag");
	
	this.searchAddress = this.boardElem.find(".board__searchAddress").val();
	this.addFavorite = this.boardElem.find(".board__addFavorite");
	this.cloud = this.boardElem.find("[data-content=cloud]");
	this.chart = this.boardElem.find("[data-content=chart]");
	this.force = this.boardElem.find("[data-content=force]");
	this.comment = this.boardElem.find("[data-content=comment]");
	this.searchAvailable = this.boardElem.find("[data-search-available]");
	this.articleNumChart;//게시글 갯수 그래프
	this.categoricalChart;//게시판 카테고리 그래프
	this.boardCategoryChart;
	this.articleSample = this.boardElem.find(".board__articleSample table");//게시글  샘플
	this.loadMoreSample = this.boardElem.find(".board__loadMoreSample");//샘플 더 불러오기 
	this.commentContentInput = this.boardElem.find("input[name=user_comment]");
	this.commentContentButton = this.boardElem.find(".board__insertComment");
	
	var _initialized = false;
	var that = this;
	
	if(this.boardElem.find(".commentTable").length>0)
		this.commentMgr = new CommentManager(this.boardElem.find(".commentTable"),{
			commentLoadNum : $("input[name=commentLoadNum]").val()
		});

	if(this.boardElem.find(".chart").length>0)
		this.chartObj = new ChartObj(this.boardElem.find(".chart"), Object.assign(option.chart || {}, {
			parent : this
		}));

	if(this.boardElem.find(".wordCloud").length>0)
		this.wordCloud = new WordCloud(this.boardElem.find(".wordCloud"), Object.assign(option.wordCloud || {}, {
			parent : this
		}));

	if(this.boardElem.find(".forceLayout").length>0)
		this.forceLayout = new ForceLayout(this.boardElem.find(".forceLayout"), Object.assign(option.forceLayout || {}, {
			parent : this
		}))

	this.showEvalWrapper = function() {

		_evalWrapper.addClass("board__evalWrapper--show");
	}

	this.hideEvalWrapper = function() {
		that.commentForm.removeClass("was-validated"); 
		_evalWrapper.removeClass("board__evalWrapper--show");
	}
	
	this.getEvalWrapper = function(){
		return _evalWrapper;
	}

	this.getInitialized = function() {
		return _initialized;
	}

	this.setInitialized = function(val) {
		_initialized = val;
	}

	// 평가 버튼 누르면 업데이트
	this.boardElem.find(".board__evalItem").on("click", function(e) {
		$(this).data("selected", !$(this).data("selected")).toggleClass("board__evalItem--selected");

		const evalList = that.getSelectedBoardEval();
		clearTimeout(that.commentTimer);
		
		that.commentTimer = setTimeout(function() {
			
			if(that.commentMgr.myComment.getUserCommentVal()){//userComment가 있으면
				that.insertBoardComment({
					writer : that.getCommentData().writer || '${defaultNickName}',
					board_code : that.id,
					comment_eval_list : evalList.toArray()
				});						
			}else{
				if(evalList.toArray().length>0)								
					that.insertBoardComment({
						writer : that.getCommentData().writer || '${defaultNickName}',
						board_code : that.id,
						comment_eval_list : evalList.toArray()
					});
				else{					
					that.deleteBoardComment({
						comment_code:that.commentMgr.myComment.getCommentCode()
					})
				}
			}
			
		}, 1000)
		

	});

	// 댓글 테이블 스크롤 바 적용
	this.boardElem.find(".scrollbar-inner").scrollbar();

	// 댓글 등록 버튼 누를때
	this.commentContentButton.on("click", function() {	
		const evalList = that.getSelectedBoardEval();
		const formData = that.getCommentData();		
		that.insertBoardComment({
			user_comment : formData.user_comment,
			writer : formData.writer,
			board_code : that.id,
			comment_eval_list : evalList.toArray()
		});

	});
	
	this.popularTag.on("click",function(e){
		const target = $(this).data("tagName");
		const temp  = [target];
		tagMenu.getInstance().showMenu(true, {
					pos : {
						x : e.clientX,
						y : e.clientY
					},
					params : {
						keyword : temp,
						board_name : target,
						timeMode : searchBar.menu.getInitialTimeMode(),
						weekList : searchBar.menu.getInitialTimeData(),
						search_target : "all"
					},
					board : that.parent && that.parent,
					showMenuFlag : {
						searchTagpan : true,
						searchTimeSet : false,
						searchRelativeTag : false,
						searchBoard : false
					}
				});
	})

	// evalWrapper 보이기 ,감추기
	this.boardElem.on("click", function(e) {

		if ($(e.target).closest(".board__commentForm ,.board__evalWrapper").length > 0) {
			that.showEvalWrapper();
		}
	});

	this.boardElem.find(".board__contentBtn[data-show=true]").on("click", function(e) {
		that.showContent(e);
	});

	this.boardElem.find(".board__contentBtn[data-show=false]").on("click", function(e) {
		that.getInitialized() && that.hideContent(e);
	})

	this.addFavorite.on("click", function() {

		const favorite = $(this);
		const params = favorite.data();
		const target = favorite.find(".board__favorite:visible");

		if (target.data("add")) {
			$.post("/insertUserBoard.ajax", params).done(function(data) {
				if (data.result.status > 0) {
					favorite.data("user_board_code", data.result.user_board_code);
					target.hide().siblings().show();
				}

			})
		} else {
			$.post("/deleteUserBoard.ajax", params).done(function(data) {
				if (data.result.status > 0) {
					favorite.data("user_board_code", null);
					target.hide().siblings().show();
				}

			})
		}
	})
	
	this.loadMoreSample.on("click",function(e){
		const offset = $(e.target).data("offset");
		const params = {};
		params.weekList = searchBar.menu.getInitialTimeData();
		params.timeMode=  searchBar.menu.getInitialTimeMode();
		params.type="community";
		params.board_code=that.id;
		params.offset= offset+1;
		params.order="desc";
		getNewsCommunityContent(getUrlparameter(params)).then(function(data){
			const detached = that.articleSample.find(".board__loadMoreSample").closest("tr").detach();
			const articleList = data.result.articleList;
			that.articleSample.find("tbody").append(that._makeArticleSampleList(articleList));
			if(articleList&&articleList.length>0)
				that.articleSample.find("tbody").append(detached);
			else 
				detached.remove();
			$(e.target).data("offset",offset+1);
		}).catch(function(){
			alert("게시글을 가져오는 도중 오류가 생겼습니다.");
		});
		
		
		
	})
	
	//텍스트가 0이면 disabled	
	this.commentContentInput.on("keyup",function(e){
		if($(this).val().length>0)
			that.commentContentButton.attr("disabled",false);
		else
			that.commentContentButton.attr("disabled",true);
		
		if (e.keyCode == 13 && $(this).val().length>0) {
			that.commentContentButton.trigger("click");			
		}
	})
	
}

// 현재 board가 컨텐츠를 보여주고 있는지
Board.prototype.isShowContent = function() {
	return this.boardElem.find(".board__contentBtn[data-show=false]").is(":visible");

}

// 그래프,wordcloud 초기화 해주는 함수
Board.prototype.init = function() {

	const that = this;
	const weekList = new Array();

	let params = {
		board_code : this.id,
		weekList : searchBar.menu.getInitialTimeData(),
		timeMode : searchBar.menu.getInitialTimeMode()
	}

	this.articleNumChart = new ArticleNumChart(that.boardElem.find(".articleNumChart"), {
		init : true
	});
	this.categoricalChart = new CategoricalChart(that.boardElem.find(".categoricalChart"), {
		init : false
	});

	getBoardContent(params).done(function(data) {

		if (data.result.hashtagData.hashtagList) {
			that.wordCloud.init();
			that.chartObj.setTagData(params, data).drawChart();
			that.forceLayout.setData(data).init();
		} else {
			//Hashtag Data가 없으면 chart,forceLayout,wordCloud을 숨김	
			that.chart.hide();
			that.force.hide();
			that.cloud.hide();
		}
		
		that.categoricalChart.init(data.result.categoryList);//게시판 성향 차트 초기화
		
		
		const detached = that.articleSample.find(".board__loadMoreSample").closest("tr").detach();
		if(data.result.articleList&&data.result.articleList.length>0){			
			that.articleSample.find("tbody").append(that._makeArticleSampleList(data.result.articleList,true)).append(detached);
		}else{
			detached.remove();
			that.articleSample.find("tbody").append("<td class='text-center'>글 샘플이 존재하지 않습니다.</td>")
		}
				
		that.wordCloud.setWordData(params, data).update(); //wordCLoud 초기화

		that.commentMgr.init(data.result);//comment 초기화
		
		if(data.result.commentList.my_comment){
			that.myCommentInit(data.result.commentList.my_comment);
			if(data.result.commentList.my_comment.user_comment)
				that.commentContentButton.attr("disabled",false);
		}
		
				
		setTimeout(function() {
			that.setInitialized(true);
		}, 300)
	})

}

// 컨텐츠 보기 버튼을 눌렀을때 내용을 보여주는 함수
Board.prototype.showContent = function(btn) {

	this.boardElem.addClass("board--showContent");
	this.boardElem.find(".board__contentBtn").each(function(i, elem) {
		$(elem).toggleClass("board__contentBtn--selected");
	})

	this.getInitialized() || this.init();
}

Board.prototype.hideContent = function(btn) {

	this.boardElem.removeClass("board--showContent");
	this.boardElem.find(".board__contentBtn").each(function(i, elem) {
		$(elem).toggleClass("board__contentBtn--selected");
	})
}

Board.prototype.initCommentFormData = function(myComment){
	this.commentForm.find("input[name=user_comment]").val(myComment.user_comment);
	this.commentForm.find("input[name=writer]").val(myComment.writer);
}

Board.prototype.evalItemInit = function(evalList){
	
	const evalItem = this.getEvalWrapper().find(".board__evalItem");
	
	evalList&&evalList.forEach(function(eval){
		evalItem.filter(`[data-eval-code=${eval.eval_code}]`).each(function(){
			$(this).data("selected", !$(this).data("selected")).toggleClass("board__evalItem--selected");
		})		
	})	
}

Board.prototype.myCommentInit=function(myComment){
	this.initCommentFormData(myComment);
	this.evalItemInit(myComment.comment_eval_list);
}

// comment 넣기
Board.prototype.insertBoardComment = function(inputData) {
	const that = this;

	const validity = this.commentForm[0].checkValidity();
	this.commentForm.addClass("was-validated");
	if (validity) {
		$.ajax({
			type : "POST",
			url : '/insertBoardComment.ajax',
			data : JSON.stringify(inputData),
			contentType : "application/json",
			dataType : "json",
			traditional : true,
			success : function(data) {				
				inputData.comment_code = data.result.comment_code;
				that.commentMgr.updateMyComment(inputData);
				that.commentMgr.myComment.commentElem.removeClass("d-none");
			}
		});

	}
}

Board.prototype.deleteBoardComment = function(inputData){
	const that = this;
	$.ajax({
		type : "POST",
		url : '/deleteBoardComment.ajax',
		data : inputData,		
		dataType : "json",
		success : function(data) {
			if(data.result.status>0)
				that.commentMgr.hideMyComment();			
		}
	});
}

// comment 입력 form 의 데이터를 json 형식으로 가져오기
Board.prototype.getCommentData = function() {
	return formDataToJson(this.commentForm.serializeArray());
}

Board.prototype.getSelectedBoardEval = function() {
	const that = this;
	return this.boardElem.find(".board__evalItem").filter(function(k, t) {
		return $(t).data("selected")
	}).map(function(k, t) {
		return {
			board_code : this.id,
			eval_code : $(t).data("evalCode")
		}
	});
}

Board.prototype._makeArticleSampleList = function(data,initial) {
	const result = new Array();

	return (data && data.length > 0) ? data.map(function(article) {

		return $(`<tr>
					<td>
						<div class="row">
							<div class="col-12">
								<a href="${article.article_address}" target="_blank">${article.title}</a>
							</div>
							<div class="col-12">
								<div class="row">									
									<div class="col"><i class="fa-solid fa-thumbs-up fa-xs"></i>${article.recommend}</div>
									<div class="col"><i class="fa-solid fa-eye fa-xs"></i>${article.view_cnt}</div>
									<div class="col"><i class="fa-solid fa-calendar fa-xs"></i>${article.d}</div>
								</div>								
							</div>
						</div>
					</td>
				</tr>`);

	}) : [ $(`<tr><td>${initial?'데이터가 존재하지 않습니다.':'마지막 페이지입니다.'} </td></tr>`) ];

}
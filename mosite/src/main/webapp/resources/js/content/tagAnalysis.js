/**
 * 태그 정보를 보여주는 객체
 */

function TagAnalysis(element, options) {
	var element = element;
	var labelData = options && options.labelData;
	var that = this;
	this.radarChart; //= new RadarChart(element.find(".radarChart"), options.radarChartOption);
	this.boardPercentageChart;// = new TagInfo(element.find(".tagInfo"), options.tagInfoOption);
	this.tagInfo;
	this.menuTimer;
	this.menu = new ChartMenu(element.find(".tagAnalysis__menu"), {
		tagMenu : true,
		multiTagSelect : false,
		onTagClick : function(target) {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				const selectedTag = $(target).data("tagName");
				that.radarChart.menu.getTagWithName(selectedTag).trigger("click");
				that.boardPercentageChart.menu.getTagWithName(selectedTag).trigger("click");
				that.tagInfo.menu.getTagWithName(selectedTag).trigger("click");
			}, 1000);

		}
	})

	this.init = function() {
		that.menu.addTagElement(labelData, labelData.slice(0, 1));

		that.radarChart = new RadarChart(element.find(".radarChart"), options.radarChartOption || {
			labelData : labelData,
			init : options.init
		});
		that.boardPercentageChart = new BoardPercentageChart(element.find(".boardPercentageChart"), options.radarChartOption || {
			labelData : labelData,
			init : options.init
		});

		that.tagInfo = new TagInfo(element.find(".tagInfo"), options.radarChartOption || {
			labelData : labelData,
			init : options.init,
			menu : {
				tagMenu : false
			}
		})

	}

	if (options) {

		if (options.init) {
			that.init();
		}

	}

}

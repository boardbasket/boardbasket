function ForceLayout(elem, option) {

	ForceLayout.index++;

	var that = this;
	this.parent = (option && option.parent) ? option.parent : null;
	this.id = this.parent ? this.parent.id : WordCloud.index;
	this.boardCode = this.parent && this.parent.id;
	this.element = elem instanceof jQuery ? elem : $(elem);
	this.element.attr("data-force-layout-id", this.id);
	this.menu = new ChartMenu(this.element, TagData.prototype.updateOption.call(this, option.menu, Object.assign({}, this.defaultMenuOption, {
		onModalClick : function() {
			const cm = chartModal.getInstance();
			const parent = that.element.parent();
			const parentRect = parent[0].getBoundingClientRect();

			cm.showModal();
			cm.attachContent(parent, that.element.detach(), function() {
				// padding-bottom = 세로/가로의 비율을 나타내야하므로 계산해서 넣어줌
				const rect = cm.getDOMElement().find(".modal-body")[0].getBoundingClientRect();
				updateViewBox(rect.width, rect.height, parentRect.width, parentRect.height);
			}, function() {

				const rect = that.element[0].getBoundingClientRect();
				updateViewBox(rect.width, rect.height);
			});
		}
	})));
	var simulation;
	var relativeBoardData;
	var svg;
	var link;
	var node;
	var linkData;
	var initFlag = false;
	var ticked = function() {

		link.attr("x1", function(d) {
			return d.source.x;
		}).attr("y1", function(d) {
			return d.source.y;
		}).attr("x2", function(d) {
			return d.target.x;
		}).attr("y2", function(d) {
			return d.target.y;
		});			
		
		node.attr("transform", function(d) {
			return "translate(" + d.x + "," + d.y + ")";
		});						
		
		
		const rect = that.element[0].getBoundingClientRect();		
		const base = Math.min(rect.width, rect.height)/8;//element 원의 지름 길이
		
		
		if(navigator.userAgent.indexOf("safari")>0||navigator.userAgent.indexOf("Firefox")>0){
			node.select("foreignObject")
			.attr("style",function(){
				
				return `width:${base*2}px;height:${base*2}px;x:${-this.getBBox().width/2}px;y:${-this.getBBox().height/2}px`;	
			});	
		}			
	}

	function dragstarted(d) {
		if (!d3.event.active)
			simulation.alphaTarget(0.3).restart();
		d.fx = d.x;
		d.fy = d.y;
	}

	function dragged(d) {
		d.fx = d3.event.x;
		d.fy = d3.event.y;
	}

	function dragended(d) {
		if (!d3.event.active)
			simulation.alphaTarget(0);
		d.fx = null;
		d.fy = null;
	}

	function updateViewBox(width, height, parentWidth, parentHeight) {
		if (!svg)
			return;

		const targetHeight = height - that.menu.optionElement.parent().outerHeight(true);
		//that.element.find(".forceLayout__content").css("padding-bottom", ((targetHeight / width) * 100) + "%");
		svg.attr("viewBox", "0 0 " + width + " " + targetHeight);
		init();

	}

	function draw() {
		if(!initFlag)
			return;
		
		simulation.nodes(relativeBoardData).on("tick", ticked);		
		if(linkData.length>0)
			simulation.force("link").links(linkData);
	}
	
	function update(){
		init();
		draw();		
	}
	

	if (option) {
		option.init && init();
	}
	
	function init() {

		if(!relativeBoardData)
			return;
		
		svg != null && svg.remove();
		svg = d3.select("[data-force-layout-id='" + that.id + "'] > .forceLayout__content").append("svg");

		const rect = that.element[0].getBoundingClientRect();
		const forceCenterX = rect.width / 2;
		const forceCenterY = (rect.height-that.menu.optionElement.outerHeight(true)) / 2 ;
		const base = Math.min(rect.width, rect.height)/8;//element 원의 지름 길이
		
		const maxVal = relativeBoardData.reduce(function(acc,curr){
			return acc<curr.val? curr.val:acc;
		},0);
		
		const minVal = relativeBoardData.reduce(function(acc,curr){
			return acc>curr.val? curr.val:acc;
		},Number.MAX_SAFE_INTEGER);
		
		const rScale = d3.scaleLinear().domain([minVal,maxVal]).range([base-0.9,base*1.1]);
		
		
		linkData = makeLinks(relativeBoardData);
		
		svg.attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox", "0 0 " + rect.width + " " + (rect.height-that.menu.optionElement.outerHeight(true)));

		relativeBoardData.forEach(function(data) {
			data.x = d3.randomUniform(0, rect.width);
			data.y = d3.randomUniform(0, rect.height);
		})		
		simulation = d3.forceSimulation().force("collide", d3.forceCollide(function(d,idx) {
			return rScale(d.val);
		})).force("link", d3.forceLink().distance(function(d) {			
			return base*3 ;
		}).id(function(d) {
			return d.board_code;
		})).force("charge", d3.forceManyBody().strength(-5)).force("center", d3.forceCenter(forceCenterX, forceCenterY)).force("y", d3.forceY(0).strength(0.2))
				.force("x", d3.forceX(0).strength(0.2));
		link = svg.append("g").attr("class", "links").selectAll("line").data(linkData).enter().append("line").attr("stroke", "#828282").attr("stroke-width",function(d){
			return 2;
		});

		node = svg.append("g").attr("class", "nodes").selectAll("g").data(relativeBoardData).enter().append("g").call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

		
		var circles = node.append("circle").attr("r", function(d,idx) {
			return rScale(d.val);
		}).attr("fill", function(d, i) {
			return d3.schemeCategory20[i % d3.schemeCategory20.length];//chartColor.getInstance().getColor((i % 12) + 1, i % 5);
		})
		
		if(navigator.vendor.match(/apple/i)==null){				
			node.append("foreignObject").style("width", function(d,idx){
				return (base)*2;
			}).style("height",function(d,idx){
				return (base)*2;
			}).style("x",function(){
				return -this.getBBox().width/2;
			}).style("y",function(){
				return -this.getBBox().height/2;
			}).style("transform-origin","0px 0px").on("mouseover focus",function(){
				const target = d3.select(this);
				if(!target.classed("selected")){			
					const rect = this.getBBox();
					target.classed("selected",true).style("width",rect.width*4).style("x",rect.x*4);	
				}			
				target.select("i").classed("d-none",false);
			}).on("mouseout blur",function(){
				const target = d3.select(this);
				if(target.classed("selected")){			
				const rect = this.getBBox();
				target.classed("selected",false).style("width",rect.width/4).style("x",rect.x/4);
				}
				target.select("i").classed("d-none",true);
			}).append("xhtml:div").attr("class","forceLayout__relativeBoard").append("xhtml:div").append("span").attr("class","badge").style("background",function(d){
				return d.site_color;
			}).style("color","white").text(function(d){
				return d.site_name;
			})
		}
		else{			
			/*
			node.append("rect")			
			.style("transform","translate(-45px,-5%)")
			.style("width","90px")
			.style("height","25px")			
			.style("rx","10px")
			.style("ry","10px")
			.style("stroke-linejoin","round")
			.style("fill",function(d){
				return d.site_color;
			})
			
			node.append("text")
			.style("x","50%")
			.style("y","25%")
			.style("text-anchor","middle")
			.style("transform","translate(0%,-1%)")
			.style("stroke","white")
			.style("stroke-width","0.8px")
			.style("font-size","0.8rem")
			.text(function(d){
				return d.site_name;
			}).on("click",function(d){
				 window.open(d.site_address, '_blank').focus();
			}).on("mouseover focus",function(){
				d3.select(this).style("cursor","pointer");
			}).on("mouseout blur",function(){
				d3.select(this).style("cursor","default");
			});*/
			
			node.append("text").style("x","50%")
			.style("y","50%")
			.style("text-anchor","middle")
			.style("transform","translate(0%,2%)")
			.style("font-weight","bold")
			.style("fill","black")
			.style("stroke","white")
			.style("stroke-width","0.3px")
			.text(function(d){
				return d.board_name;
			}).on("click",function(d){
				relativeBoardMenu.showMenu({
					data:d,
					pos:{
						x:d3.event.x,
						y:d3.event.y
					}
				})
			}).on("mouseover focus",function(){
				d3.select(this).style("cursor","pointer");
			}).on("mouseout blur",function(){
				d3.select(this).style("cursor","default");
			});
			
		}
			
	
		node.selectAll(".forceLayout__relativeBoard").append("xhtml:div").on("click",function(d){
			
			relativeBoardMenu.showMenu({
				data:d,
				pos:{
					x:d3.event.x,
					y:d3.event.y
				}
			})
		}).text(function(d) { return d.board_name; }).append("i").attr("class","fas fa-info-circle d-none");
		
		initFlag = true;
		
		draw();
				
	}

	function makeLinks(data) {

		if (!parent || !data)
			return null;

		var linkData = new Array();

		const source = data.filter(function(k) {
			return that.parent.id === k.board_code;
		})

		if(source.length>0)
			for (let i = 0; i < data.length; ++i) {
				if (that.parent.id !== data[i].board_code)
					linkData.push({
						source : source[0].board_code,
						target : data[i].board_code
					})
			}
						
		return linkData;
	}
	
	if(isMobile()){
		$(window).on("orientationchange",function(){
			setTimeout(function(){
				update();
			},50);
		})
	}else{
		$(window).on("resize",function(e){
			setTimeout(function(){
				update();
			},10);
		})
	}
	

	return {
		init : init,
		draw : draw,
		setData : function(data) {
			const target = data.result || data;
			relativeBoardData = target.relativeBoard;
			return this;
		},
		getForceLayoutElem : function() {
			return that.element;
		}
	}

}

ForceLayout.index = 0;

ForceLayout.prototype.defaultMenuOption = {
	siteMenu : false,
	tagMenu : false,
	timeMenu : false,
	limitMenu : false,
	modalMenu : true,
	zoomBtn : false,
	multiTimeSelect : false
}

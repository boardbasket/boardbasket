function WordCloud(elem, option) {
	WordCloud.index++;
	TagData.call(this);

	var that = this;
	this.parent = (option && option.parent) ? option.parent : null;
	this.id = this.parent ? this.parent.id : WordCloud.index;
	this.boardCode = this.parent && this.parent.id;
	this.element = elem instanceof jQuery ? elem : $(elem);
	this.element.attr("data-word-cloud-id", this.id);
	this.scrollWrapper;
	this.tooltip;
	this.fontBaseSizeScale = d3.scaleLinear().domain([ 500, 2500 ]).range([ 7, 15 ]).clamp(true);
	var svg;
	var g;
	var zoom;
	var drag;
	var slider;
	var _selectMenuTimer;// 메뉴 선택할때 딜레이 주는 타이머
	var wordScale;
	var scaleMax = 5;
	this.menu = new ChartMenu(this.element, this.updateOption(option.menu || {}, Object.assign({}, this.defaultMenuOption, {
		onWeekClick : function() {
			timeClickEvent();
		},
		onMonthClick : function() {
			timeClickEvent();
		},
		onLimitClick : function() {
			update();
		},
		onModalClick : function() {
			const cm = chartModal.getInstance();
			const parent = that.element.parent();
			cm.showModal();
			cm.attachContent(parent, that.element.detach(), function() {
				// padding-bottom = 세로/가로의 비율을 나타내야하므로 계산해서 넣어줌
				const rect = cm.getDOMElement().find(".modal-body")[0].getBoundingClientRect();
				updateViewBox(rect.width, rect.height);
				update();

			}, function() {
				const layout = getLayout();
				updateViewBox(layout.width, layout.height);
				update();
			});
		},
		onResetTime : function() {
			update();
		},
		onHashtagModalClick : function() {
			hashtagModal && hashtagModal.setBoardCode(that.boardCode).showModal();
		},
		onTagClick : function(target) {
		}

	})));

	const params = that.getHashtagParams();
	const initData = that.getInitData(that.element, params);
	initData && setWordData(params, initData); //initData가 있다면 객체 생성시 파싱 후 넣어줌

	//wordCloud가  modal<->board 이동할때 viewBox의 크기를 변경하는 함수
	function updateViewBox(width, height, parentWidth, parentHeight) {
		//that.element.find(".wordCloud__content").css("padding-bottom", ((height / width) * 100) + "%");
		svg && svg.attr("viewBox", "0 0 " + width + " " + height);
		/*
		 * const oldViewBox = svg.node().viewBox.baseVal; const widthRatio =
		 * width / oldViewBox.width; const heightRatio = height /
		 * oldViewBox.height;
		 */

		/*
		 * svg.selectAll("text").style("transform", function(d) { return
		 * "translate(" + (d.x * widthRatio) + "," + (d.y * heightRatio) + ")"; })
		 */

		//update();
		/*
		 * svg.selectAll("text").attr("transform", function(d) { const
		 * adjustedWidth = d.x * (width / parentWidth); const adjustedHeight =
		 * d.y * (height / parentHeight); return "translate(" + [ adjustedWidth,
		 * adjustedHeight ] + ")rotate(" + d.rotate + ")"; });
		 */
	}

	function timeClickEvent() {
		clearTimeout(_selectMenuTimer);
		_selectMenuTimer = setTimeout(function() {
			const filteredParam = that.getNonExistDatasetKey(that.getHashtagParams())

			if (!filteredParam) {
				update();
			} else {
				getHashtagData(filteredParam).done(function(data) {
					that.parent && that.parent.chartObj.setTagData(filteredParam, data);

					setWordData(filteredParam, data);
					update();
				})

			}
		}, 1000);
	}

	function getLayout() {
		const optionWidth = that.menu.optionElement.width();
		const optionHeight = that.menu.optionElement.parent().outerHeight(true);
		const layoutWidth = that.element.width();
		const layoutHeight = that.element.height();
		const tpWidth = (layoutWidth) / 2;
		const tpHeight = (layoutHeight - optionHeight) / 2;

		return {
			width : layoutWidth * 2,
			height : (layoutHeight - optionHeight) * 2,
			outerHeight : layoutHeight,
			tpWidth : tpWidth,
			tpHeight : tpHeight
		}
	}

	function getGroup(destroyPrev) {

		const layout = getLayout();

		if (!destroyPrev && g) {
			return g;
		} else {
			destroyWordCloud();
			let prevScaleVal = 1;
			let selector = "[data-word-cloud-id='" + that.id + "'] .wordCloud__content";
			// 게시판 아래에 wordcloud가 없으면 modal에 있는것으로 간주
			selector = $(selector).length <= 0 ? "#chartModal .wordCloud__content" : selector;

			svg = d3.select(selector).append("svg").attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox", "0 0 " + layout.width + " " + layout.height);
			g = svg.append("g");

			zoom = d3.zoom().scaleExtent([ 1, scaleMax ]).extent([ [ 0, 0 ], [ layout.width, layout.height ] ]).translateExtent([ [ 0, 0 ], [ layout.width, layout.height ] ]).on("zoom", function() {

				g.attr("transform", d3.event.transform);
				slider.property("value", d3.event.transform.k);
				if (that.scrollWrapper)
					prevScaleVal != 1 && d3.event.transform.k == 1 ? that.scrollWrapper.css("opacity", 1).show().animate({
						"opacity" : 0.4
					}, 800) : that.scrollWrapper.hide();
				prevScaleVal = d3.event.transform.k;
				tagMenu.getInstance().showMenu(false);

				if (isMobile())
					tooltip.style("display", "none");

			});

			slider = d3.select("[data-word-cloud-id='" + that.id + "'] .option__wordCloudZoom").datum({}).attr("value", zoom.scaleExtent()[0]).attr("min", zoom.scaleExtent()[0]).attr("max",
					zoom.scaleExtent()[1]).attr("step", (zoom.scaleExtent()[1] - zoom.scaleExtent()[0]) / 100).on("input", slided);

			svg.call(zoom);

			var transform = d3.zoomIdentity.translate(-layout.width, -layout.height).scale(scaleMax / 2);

			svg.call(zoom.transform, transform);

			slider.attr("value", scaleMax / 2);

			that.scrollWrapper && that.scrollWrapper.show().css("opacity", 0.4);

			if (isMobile()) {
				svg.on("click", function() {
					if (!$(d3.event.target).is("text"))
						tooltip.style("display", "none");
				})
			}

			return g;

		}

	}

	function draw(words, destroyPrev, bounds) {

		appendNoDataElement(that.element, words.length <= 0);

		let bMidX = 0;
		let bMidY = 0;
		const layout = getLayout();
		let menuFlag = true;

		if (bounds) {
			const bWidth = bounds[1].x - bounds[0].x;
			const bHeight = bounds[1].y - bounds[0].y;
			bMidX = bounds[0].x + bWidth / 2;
			bMidY = bounds[0].y + bHeight / 2;
		}

		var cloud = getGroup(destroyPrev).selectAll("text").data(words, function(d) {
			return d.t;
		})

		// Entering words
		cloud.enter().append("text").style("fill", function(d, i) {
			return chartColor.getInstance().getDataColor(i);
		}).style("font-size", function(d) {
			return wordScale(d.f) + 'px';
		}).style("padding", 0).style("cursor", "pointer").attr("text-anchor", "middle").attr("transform", function(d) {
			return "translate(" + [ bMidX + d.x, bMidY + d.y ] + ")";
		}).text(function(d) {
			return d.t;
		}).attr("data-tag-name", function(d) {
			return d.t;
		}).on("touchstart", function(d) {
			const node = $(d3.select(this).node());
			if (isMobile() && node.is("text"))
				node.data("touchTarget", true);

		}).on(`${isMobile()?'touchend':'click'}`, function(d) {

			const xPos = isMobile() ? d3.event.changedTouches[0].clientX : d3.event.x;
			const yPos = isMobile() ? d3.event.changedTouches[0].clientY : d3.event.y;
			const node = $(d3.select(this).node());
			const temp = new Array();
			temp.push(d.t);

			if (isMobile()) {
				menuFlag = node.data("touchTarget");
				d3.selectAll("text").each(function(elem) {
					$(this).data("touchTarget", false);
				});
			}

			menuFlag && tagMenu.getInstance().showMenu(true, {
				pos : {
					x : xPos,
					y : yPos
				},
				params : {
					keyword : temp,
					board_name : temp.join(''),
					timeMode : that.menu.getTimeMode(),
					weekList : that.menu.getSelectedTimeData(),
					search_target : "all"
				},
				board : that.parent && that.parent,
				showMenuFlag : {
					searchTagpan : true,
					searchTimeSet : false,
					searchRelativeTag : false,
					searchBoard : false
				}
			});

			if (isMobile())
				openRankTooltip(node, {
					xPos : xPos,
					yPos : yPos
				});

		}).on("mouseover", function(d) {
			if (isMobile())
				return;

			const xPos = d3.event.x;
			const yPos = d3.event.y;
			const elem = $(d3.select(this).node());
			const transform = d3.select(this).attr("transform");

			elem.data("oldTransform", transform);
			d3.select(this).attr("transform", transform + "scale(1.5)");

			if (option.main)
				openRankTooltip(elem, {
					xPos : xPos,
					yPos : yPos
				});

		}).on("mouseout", function(d) {
			if (isMobile())
				return;

			const elem = $(d3.select(this).node());
			d3.select(this).attr("transform", elem.data("oldTransform"));
			if (option.main)
				tooltip.style("display", "none");
		});

		// Entering and existing words
		cloud.transition().duration(600).style("font-size", function(d) {
			return wordScale(d.f) + "px";
		}).attr("transform", function(d) {
			return "translate(" + [ bMidX + d.x, bMidY + d.y ] + ")";
		}).style("fill-opacity", 1);

		// Exiting words
		cloud.exit().transition().duration(200).style('fill-opacity', 1e-6).attr('font-size', 1).remove();

	}

	function openRankTooltip(elem, pos) {
		let chartData = that.getTagData(that.getHashtagParams());
		const keyword = elem.data("tagName");
		const keys = Object.keys(chartData);
		const mergedData = keys.filter(function(key) {
			return chartData[key];
		}).flatMap(function(key) {
			return chartData[key];
		}).filter(function(tag) {
			return tag.t == keyword;
		}).reduce(function(acc, curr) {
			return curr;
		}, {});

		tooltip.html(`<div class='tooltip__content'><strong>#${mergedData.v}</strong></div>`);

		elem[0].getBBox().width / 2
		$(tooltip.node()).css({
			top : pos.yPos - elem[0].getBBox().height,
			left : pos.xPos + elem[0].getBBox().width / 2
		})
		tooltip.style("display", "block").style("opacity", "1");
	}

	function update(destroyPrev) {
		const layout = getLayout();
		const wordData = getWordData();
		let max = Math.max.apply(Math, wordData.map(function(d) {
			return d.f;
		}));
		let min = Math.min.apply(Math, wordData.map(function(d) {
			return d.f;
		}));

		let avg = wordData.map(function(d) {
			return d.f
		}).reduce(function(acc, curr) {
			return acc + curr;
		}, 0) / wordData.length;

		if (isNaN(max) || isNaN(min)) {
			max = min = that.default_word_size;
			wordData.forEach(function(word) {
				word.f = that.default_word_size;
			})
		}

		const rangeBase = Math.max(layout.width, layout.height);

		const domain = wordData.map(function(d) {
			return d.f;
		}).filter(function(value, index, self) {
			return self.indexOf(value) === index;
		}).sort();//중복없는 데이터 리스트

		/*
		 * const maxDataLength =
		 * Number.parseInt(that.menu.getLimitElement().toArray().reduce(function(acc,
		 * curr) { if (acc == null) return $(curr).data("limit"); else return
		 * $(acc).data("limit") > $(curr).data("limit") ? $(acc).data("limit") :
		 * $(curr).data("limit"); }, null));
		 */

		/* if(option.main){ */
		const range = getFontSizeRange(that.element.width(), domain.length, wordData.length);
		wordScale = d3.scaleLinear().domain(domain).range(range).clamp(true);
		/*
		 * }else{ const rangeMax=rangeBase / (wordData.length/1.5); const
		 * rangeMin=rangeBase / wordData.length; wordScale =
		 * d3.scaleLinear().domain([ min, max ]).range([ rangeMin, rangeMax
		 * ]).clamp(true); }
		 */

		setTimeout(function() {
			d3.layout.cloud().size([ layout.width, layout.height ]).words(wordData).fontSize(function(d) {
				return wordScale(d.f);
			}).on("end", function(words, bounds) {

				draw(words, destroyPrev, bounds);
				updateViewBox(layout.width, layout.height);
				drag = d3.drag().subject(function(d) {
					return d;
				}).on("start", dragstarted).on("drag", dragged).on("end", dragended);

			}).start();

		}, 0);
	}

	function getWordData() {

		const params = that.getHashtagParams();

		let chartData = that.getTagData(params);

		const keys = Object.keys(chartData);

		const result = new Array();
		const mergedData = keys.filter(function(key) {
			return chartData[key];
		}).flatMap(function(key) {
			return chartData[key];
		})

		const group = _.groupBy(mergedData, "t");
		const tags = Object.keys(group);

		tags.forEach(function(tag) {
			let val = group[tag].reduce(function(acc, curr, i) {
				return acc + parseInt(curr["f"]);
			}, 0);

			result.push({
				t : tag,
				f : val
			})
		})

		return result;
	}

	function checkWordDataExist(fromTime) {
		let flag = true;
		if (Array.isArray(fromTime)) {

			fromTime.forEach(function(time) {
				if (!getWordData(time)) {
					flag = false;
					return;
				}
			})

		} else if (!getWordData(fromTime))
			flag = false;

		return flag;
	}

	function setWordData(params, data) {

		const _params = params || that.getHashtagParams()
		const _data = data || that.getInitData(that.element, _params);

		that.setTagData(_params, _data);
		return this;
	}

	function destroyWordCloud() {
		if (svg) {
			svg.remove();
			that.element.find("svg").remove();
		}

	}

	if (option) {
		option.init && init();
	}

	function getFontSizeRange(screenSize, domainLength, dataLength) {
		let fontBaseSize = 10;
		const MAX_RANGE_NUMBER = 6;//range갯수가 최대 몇개인지

		const result = [];
		let diff = 10;

		if (screenSize <= 576) {
			fontBaseSize = 23;
			diff = 3;

		} else {
			fontBaseSize = 40;
			diff = 5;
		}

		let fontFlag = true;
		let count = 0;
		let sign = 1;
		while (fontFlag) {

			if (count == 0) {
				result.push(fontBaseSize);
				count++;
				continue;
			}
			const fontSize = fontBaseSize + (count * diff * -1);

			result.push(fontSize < 10 ? 10 : fontSize);

			count++;

			if (count >= domainLength)
				break;
		}

		result.sort(function(a, b) {
			return a - b;
		});

		return result;
	}

	function getInitTagMenu(element) {
		const tagMenuInit = element.find("[data-keyword]").map(function(idx, elem) {
			return $(elem).data();
		});
		return tagMenuInit;
	}

	function init() {

		const params = that.getHashtagParams();
		const initData = that.getInitData(that.element, params);

		that.scrollWrapper = $("<div class='wordCloud__scrollWrapper'></div>");

		that.element.find(".wordCloud__content").append(that.scrollWrapper);

		that.scrollWrapper.on("click", function() {
			$(this).css("opacity", 0.7).animate({
				"opacity" : 0
			}, 800, function() {
				that.scrollWrapper.css("display", "none");
			});
		})
		//툴팁
		this.tooltip = d3.selectAll(`.wordCloud`).filter(function(d) {
			return Number.parseInt(this.dataset.wordCloudId) == that.id;
		}).append("div").style("display", "none").style("background-color", "white").style("border", "solid").style("border-width", "2px").style("border-radius", "5px").style("padding", "5px").style(
				"position", "fixed");

		if (!initData.hashtagData) {
			getHashtagData(params).done(function(data) {
				setWordData(params, data);
				update();
			})
		} else {
			that.setTagData(params, initData);
			update();
		}

	}

	function dragstarted(d) {
		d.fx = d.x;
		d.fy = d.y;
	}

	function dragged(d) {

		d3.select(this).attr("transform", `translate(${event.x},${event.y})`);
	}

	function dragended(d) {

		d.fx = null;
		d.fy = null;
	}

	function slided(d) {
		const val = Number.parseFloat(d3.select(this).property("value"));
		zoom.scaleTo(svg, val);
	}

	if (isMobile()) {
		$(window).on("orientationchange", function() {
			setTimeout(function() {
				update();
			}, 50);
		})

	} else {
		$(window).on("resize", function(e) {
			setTimeout(function() {
				update();
			}, 10);
		})
	}

	return {
		checkWordDataExist : checkWordDataExist,
		update : update,
		getWordData : getWordData,
		setWordData : setWordData,
		init : init
	}

}

WordCloud.prototype = Object.create(TagData.prototype);
WordCloud.prototype.constructor = TagData;
WordCloud.index = 0;

WordCloud.prototype.defaultMenuOption = {
	siteMenu : false,
	tagMenu : false,
	timeMenu : true,
	limitMenu : true,
	modalMenu : true,
	zoomRange : true,
	multiTimeSelect : true,
	initialLimit : 50,
	limitArr : null
}

WordCloud.prototype.default_word_size = 8;
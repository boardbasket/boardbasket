function SearchBar(elem, option) {
	var _that = this;
	var _element = _element = elem instanceof jQuery ? elem : $(elem);
	var _formBtn = _element.find(".search__execute");
	var _input = _element.find(".search__input");
	var _cloneWrapper = _element.find(".search__cloneWrapper");
	var _keywordModalBtn = _element.find(".search__KeywordModalBtn");
	var _keywords = _element.find(".option__keyword").map(function() {
		return $(this).data("keyword");
	}).toArray();

	this._trendTag = _element.find(".option__trendTag").map(function() {
		return $(this).data("keyword");
	}).toArray();

	this.searchTargetElem = _element.find(".search__type");

	this.getInput = function() {
		return _input;
	}

	/*
	 * var _sortElemUp = _element.find("input[value=eval_ASC]").parent(); var
	 * _sortElemDown = _element.find("input[value=eval_DESC]").parent();
	 */
	var _siteWrapper = _element.find(".search__siteWrapper");
	var _keywordModal = keywordModal;
	this.menu = new ChartMenu($(".search__menu"), {
		isMobile : option ? option.isMobile : false,
		timeMenu : true,
		siteMenu : true,
		multiTimeSelect : true,
		onSiteClick : function(e) {
			_that.updateOptionClone();
		},
		onYearClick : function() {
			_that.updateOptionClone();
		},
		onWeekClick : function(e) {
			_that.updateOptionClone();
		},
		onMonthClick : function() {
			_that.updateOptionClone();
		},
		onTimeClick : function(e) {
			_that.updateOptionClone();
		},
		onResetTime : function(e) {
			_that.updateOptionClone();
		}
	});

	this.getSortElemUp = function(value) {

		if (_sortElemUp.length)
			return _sortElemUp;
		else
			return _sortElemUp = $('<span><i class="fas fa-sort-up searchBar__sortElement"><input type="hidden" name="order" value="eval_ASC"/></i></span>');

	}

	this.getSortElemDown = function() {
		if (_sortElemDown.length)
			return _sortElemDown;
		else
			return _sortElemDown = $('<span><i class="fas fa-sort-down searchBar__sortElement"><input type="hidden" name="order" value="eval_DESC"/></i></span>');

	}

	this.getElem = function() {
		return _element;
	}

	this.getCloneWrapper = function() {
		return _cloneWrapper;
	}

	this.getKeywords = function() {
		return _keywords;
	}

	//모달이 닫혔을때 불려질 콜백
	this.modalCallback = function(data) {

		_cloneWrapper.find(".option__keyword").remove();
		_keywords = data.dummys.map(function() {

			const _target = $(this);
			const _targetKeyword = _target.data("keyword");
			/*
			 * if (!_keywords.includes(_targetKeyword))
			 * _keywords.push(_targetKeyword);
			 */

			_that.getCloneWrapper().append(_target);
			/*
			 * .on("click", function() { const idx =
			 * _keywords.indexOf(_targetKeyword); if (idx >= 0)
			 * _keywords.splice(idx, 1); })
			 */
			return _targetKeyword;
		}).toArray();

		_btn.find("> button").focus();

	}
	/*
	 * _element.find(".search__evalItem .form-check-input").on("click",
	 * function(e) { _that.toggleSortingElement($(this).parent()); })
	 */

	_element.find("#site_choose").on("click", function() {
		_siteWrapper.addClass("search__siteWrapper--activated");
		_siteWrapper.find("input").attr("disabled", false);
	})

	_element.find("#site_all").on("click", function() {
		_siteWrapper.removeClass("search__siteWrapper--activated");
		_siteWrapper.find("input").attr("disabled", true);
	})

	// 뒤로가기 했을때 input상태를 동일하게 만들기 위해서 trigger
	_siteWrapper.find("#site_choose").is(":checked") && _siteWrapper.find("#site_choose").trigger("click");

	// 뒤로가기 해서 왔을때 체크가 되어있으므로 모두 체크를 해제하기
	_element.find(".searchBar__sortElem").prop("checked", false);

	/*
	 * _btn.on("click", function() { const params = _that.getSearchParams();
	 * 
	 * location.href = _that.makeQueryString(params); })
	 */

	_formBtn.on("click", function(e) {
		const params = _that.getSearchParams();
		e.preventDefault();
		if (params.weekList == null || params.weekList.length <= 0)
			params.weekList = _that.menu.getInitialTimeData();

		location.href = _that.makeQueryString(params);
	})

	_input.bind("keydown", function(e) {

		if (e.keyCode == 13) {
			e.preventDefault();
			_input.autocomplete({
				disabled : true
			});
			_formBtn.trigger("click");
		}

	})

	this.updateTimeDummy = function(mode) {

		_that.getCloneWrapper().find(".search__timeDummy").remove();

		let targetLength = 0;
		let maxTargetLength = 0;
		switch (mode) {
		case 'year':
			targetLength = _that.menu.getSelectedYear().length;
			maxTargetLength = maxYearOptionNum;
			break;
		case 'month':
			targetLength = _that.menu.getSelectedMonth().length;
			maxTargetLength = maxMonthOptionNum;
			break;
		case 'week':
			targetLength = _that.menu.getSelectedWeek().length;
			maxTargetLength = maxWeekOptionNum;
			break;

		}

		_that.getCloneWrapper().append(_that.makeTimeLimitDummy(maxTargetLength, targetLength));

	}

	// 메뉴를 클릭했을때 해당 element의 clone을 만들어서 붙이는 함수
	this.updateOptionClone = function() {
		const site = _that.menu.getSelectedSite();
		const keyword = _that.getCloneWrapper().find(".option__keyword").detach();

		_that.getCloneWrapper().children().remove();

		_that.getCloneWrapper().append(site.clone());

		const mode = _that.menu.getTimeMode();

		switch (mode) {
		case 'year':
			const year = _that.menu.getSelectedYear();
			const yearClone = year.clone(true);
			yearClone.find(".fa-check").remove();
			_that.getCloneWrapper().append(yearClone.addClass("option__dummy").append("<i class='fas fa-times'></i>"));
			break;
		case 'month':
			const months = _that.menu.getSelectedMonth();
			const monthClone = months.clone(true);
			monthClone.find(".fa-check").remove();
			_that.getCloneWrapper().append(monthClone.addClass("option__dummy").html(function() {
				const monthData = $(this).data();
				return monthData.year + "년 " + monthData.month + "월 " + "<i class='fas fa-times'></i>";
			}));
			break;
		case 'week':
			const week = _that.menu.getSelectedWeek();
			const weekClone = week.clone(true);
			_that.getCloneWrapper().append(weekClone.addClass("option__dummy").html(function() {
				const weekData = $(this).data();
				return weekData.year + "년 " + weekData.month + "월 " + weekData.week + "주차 " + "<i class='fas fa-times'></i>";
			}));
			break;
		}
		_that.updateTimeDummy(mode);
		_that.getCloneWrapper().append(keyword);
	}

	//남은 시간 옵션 갯수를 만들어 붙이는 함수
	this.makeTimeLimitDummy = function(maxTargetLength, targetLength) {

		const idx = maxTargetLength - targetLength;
		const base = "<span class='badge badge-light border border-light option__timeDummy'></span>";

		const $frag = $(document.createDocumentFragment());
		for (let i = 1; i <= idx; ++i) {
			const temp = $(base);
			$frag.append(temp.html('&nbsp;&nbsp;' + (targetLength + i) + '&nbsp;&nbsp;'));
		}

		return $frag;

	}

	//태그 검색 을 눌렀을때 modal 띄우기
	if (_keywordModal) {
		_keywordModalBtn.on("click", function() {
			_keywordModal.showModal(_that.getCloneWrapper().find(".option__keyword").clone());
		})
		_keywordModal.setCallback(_that.modalCallback);
	}

	_that.updateOptionClone();

	/*
	 * _cloneWrapper.on("click", function(e) {
	 * 
	 * if ($(e.target).hasClass("option__keyword")) { $(e.target).remove();
	 * const idx = _keywords.indexOf($(e.target).data("keyword"));
	 * _keywords.splice(idx, 1); } else
	 * 
	 * _that.menu.timeWrapperEventFunc(e); })
	 */
	//초기화 할때 키워드 더미를 클릭하면 지워지는 이벤트 바인드
	_cloneWrapper.on("click", function(e) {
		const target = $(e.target);
		if (target.hasClass("option__dummy"))
			_that.menu.timeWrapperEventFunc(e);
	})

	//검색바 자동완성
	_input.autocomplete({
		source : function(request, response) {

			if (!_input.val()) {
				response(_that._trendTag.map(function(data) {
					return {
						value : data,
						label : data
					}
				}));
			} else if (_input.val().length >= $("input[name=autoCompleteMinLen]").val()) {
				$.ajax({
					type : "GET",
					url : '/selectSearchTag.ajax',
					dataType : "json",
					data : {
						board_name : request.term
					},
					success : function(data) {

						let responseObj = new Object();
						const hashtagList = data.result ? data.result.hashtagList : null;
						if (hashtagList && hashtagList.length > 0) {

							responseObj = hashtagList.map(function(data) {
								return {
									value : data.t,
									label : data.t
								}
							})
						}

						response(responseObj);
					}
				});
			} else {
				response({});
			}
		},
		delay : 1000,
		minLength : 0,
		select : function(event, ui) {
			const inputVal = _input.val();
			let targetVal = '';
			const params = _that.getSearchParams();
			const keyword = ui.item.value;

			if (inputVal) {
				event.preventDefault();
				targetVal = params.board_name;
			} else
				targetVal = keyword;

			params.board_name = targetVal;
			_input.val(targetVal);

			location.href = _that.makeQueryString(params);
		},
		open : function() {
			$(this).data('state', 'open');
		},
		close : function() {
			$(this).data('state', 'closed');
		}
	});

	_input.on('focus', function() {
		if ($(this).data('state') != 'open')
			_input.autocomplete("search");
	});

}

SearchBar.prototype.getSearchTargetVal = function() {
	return this.searchTargetElem.find("option:selected").val();
}

SearchBar.prototype.getInputValue = function() {
	return this.getInput().val();
}

SearchBar.prototype.makeQueryString = function(params) {

	const _params = getUrlparameter(params ? params : this.getSearchParams());

	return getContextPath() + "/search?" + $.param(_params);
}

SearchBar.prototype.getSearchParams = function() {
	let params = {
		search_target : this.getSearchTargetVal(),
		board_name : this.getInputValue(),
		site_code : this.menu.getSelectedSiteData(),
		timeMode : this.menu.getTimeMode(),
		weekList : this.menu.getSelectedTimeData()
	}

	/*if (params.search_target != 'board')
		params.order = "tag";*/

	return params;
}

SearchBar.prototype.getInitialSearchParams = function() {
	let params = {
		search_target : this.getSearchTargetVal(),
		board_name : this.getInputValue(),
		site_code : this.menu.getSelectedSiteData(),
		timeMode : this.menu.getInitialTimeMode(),
		weekList : this.menu.getInitialTimeData()
	}

	return params;
}

SearchBar.prototype.getInputValue = function() {
	return this.getInput().val();
}

SearchBar.prototype.clearInputValue = function() {
	this.getInput.val("");
}

SearchBar.prototype.getTrendTag = function() {
	return this._trendTag;
}

// evalItem클릭했을때 sortingItem을 순서대로 toggle 해주는 함수
SearchBar.prototype.toggleSortingElement = function(elem) {
	const target = elem.find(".searchBar__sortElement");
	const value = (target.length > 0) && target.find("input").val();

	switch (value) {
	case "eval_ASC":
		this.getSortElemUp().detach();
		$(elem).append(_that.getSortElemDown().detach());
		break;
	case "eval_DESC":
		this.getSortElemUp().detach();
		this.getSortElemDown().detach();
		elem.find("input").prop("checked", false);
		break;
	default:
		$(elem).append(_that.getSortElemUp().detach());
		break;

	}
}

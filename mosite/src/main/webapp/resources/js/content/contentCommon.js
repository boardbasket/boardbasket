var timeSetMgr = (function TimeSetManager() {
	var timeSet = new Object();
	var timeSetList = $("#timeSetList .timeSet");
	timeSetList.each(function(i, elem) {
		const timeSetData = $(elem).data();

		timeSet[JSON.stringify({
			year : timeSetData.year,
			month : timeSetData.month,
			week : timeSetData.week
		})] = timeSetData;

	})

	var instance;

	function init() {

		return {

			getTimeSet : function(year, month, week) {

				if (month == undefined || week == undefined)
					return timeSetList.filter(".timeSet--current").data();
				else {
					return timeSet[JSON.stringify({
						year : year,
						month : month,
						week : week
					})]

				}

			},
			getTimeSetList : function() {
				return timeSet;
			},
			getTimeSetWithDate : function(date, timeMode) {
				let result;
				const keys = Object.keys(timeSet);

				keys.forEach(function(key) {

					const _timeSet = timeSet[key];
					/*
					 * if (result || !_timeSet.week) return;
					 */

					switch (timeMode) {
					case "year":
						if (_timeSet.month == 0)
							break;
						return;
					case "month":
						if (_timeSet.week == 0 && _timeSet.month != 0)
							break;
						return;
					case "week":
						if (_timeSet.week != 0)
							break;
						return;
					}

					const fromDate = moment().subtract(_timeSet.fromTime, 'day').startOf('day');
					const toDate = moment().subtract(_timeSet.toTime, 'day').startOf('day');
					if (moment(date).isBetween(fromDate, toDate, undefined, '[]'))
						result = _timeSet;

				})
				if (result == undefined)
					console.log(date);

				return result;
			}

		}

	}

	return {
		getInstance : function() {
			if (!instance)
				instance = init();
			return instance;
		}
	}

})();

function TagData() {
	this.chartData = new Object();
}

//가지고있는 태그중 중복되지 않은 태그 이름을 가져옴
TagData.prototype.getUniqueTagNameList = function(params) {
	const _chartData = this.getTagData(params);
	const limit = params.limit;
	const target = Object.keys(_chartData).reduce(function(r, k) {
		return _chartData[k] ? r.concat(_chartData[k]) : r;
	}, []).reduce(function(acc, x) {

		const tag = acc[x.t];
		acc[x.t] = tag ? acc[x.t] + parseInt(x.f) : parseInt(x.f);
		return acc;

	}, {});

	return Object.keys(target).splice(0, limit || 10);

}

//받아온 parameter에 해당하는 tagData가 존재하는지 확인하고 없으면 불러올 것들의 ajax parameter를 리턴
TagData.prototype.getNonExistDatasetKey = function(params) {
	const that = this;
	const targetTime = new Array();
	params.limit = null;//모든 데이터에서 찾기 위함
	const chartData = that.getTagData(params);
	const keys = Object.keys(chartData);
	let targetKeyword = '';
	keys.forEach(function(key) {
		const keyData = JSON.parse(key);
		//키에 해당하는 데이터가  없다면 불러올 ajax parameter로 넣어줌
		if (typeof chartData[key] === 'undefined') {

			targetTime.push({
				year : keyData.year,
				month : keyData.month,
				week : keyData.week
			});
			targetKeyword = params.board_name;
		} else {
			//데이터가 있다면 
			if (params.board_name) {
				const targetBoardName = new Array();
				//해당 데이터가 키워드를 포함하는지 확인하고 없다면 ajax parameter로 넣어줌 

				chartData[key].filter(function(item) {
					return params.board_name.indexOf(item.t) < 0;
				}).forEach(function(item) {
					targetBoardName.indexOf(item) < 0 || targetBoardName.push(item);
				})

				if (targetBoardName.length > 0) {
					targetTime.push({
						year : keyData.year,
						month : keyData.month,
						week : keyData.week
					});
					targetKeyword = targetKeyword + " " + targetBoardName.join(' ');
				}
				/*
				 * if (chartData[key].filter(function(item) { const targetFlag =
				 * params.board_name.indexOf(item.t) < 0; if (targetFlag)
				 * targetBoardName.push(item.t); return targetFlag; }).length <=
				 * 0) { targetTime.push({ year : keyData.year, month :
				 * keyData.month, week : keyData.week }); targetKeyword =
				 * targetBoardName.join(' '); }
				 */

			}
		}
	})

	if (targetTime.length <= 0 && !targetKeyword)
		return null;
	else {

		const returnObj = {
			board_code : params.board_code,
			site_code : params.site_code,
			weekList : targetTime,
			tagNameList : params.tagNameList,
			timeMode : params.timeMode
		}
		if (targetKeyword)
			returnObj["board_name"] = targetKeyword;

		return returnObj;
	}

}

// hashtag를 가져오기 위한 parameter로 가져온 데이터를 저장할 오브젝트의 키를 만드는 함수
TagData.prototype.makeDatasetKey = function(obj) {

	return obj.weekList.map(function(ts) {
		return JSON.stringify({
			board_code : obj.board_code,
			year : ts.year,
			month : ts.month,
			week : ts.week,
			site_code : obj.site_code
		});
	});
}

//가져온 태그 데이터를 세팅
TagData.prototype.setTagData = function(params, data) {
	const that = this;
	const _data = data.result ? data.result : data;
	const keys = this.makeDatasetKey(params);
	const concatFlag = params.board_name || params.keyword && params.keyword.length > 0;
	keys.forEach(function(key) {

		const keyData = JSON.parse(key);

		const tagList = _data.hashtagData ? _data.hashtagData.hashtagList : _data.hashtagList;
		const timeMode = _data.hashtagData ? _data.hashtagData.timeMode : (_data.timeMode ? _data.timeMode : params.timeMode);
		const articleNumList = (_data.hashtagData ? _data.hashtagData.articleNumList : _data.articleNumList) || {};
		let articleNum = null;
		let targetData;
		try {
			if (tagList) {
				let targetArticleNum=null;
				switch (timeMode) {
				case "year": {
					let monthTag = new Array();					
					
					const months = Object.keys(tagList[keyData.year]).forEach(function(month) {								
						
						let target = tagList[keyData.year][month][0];
						try{
							
							targetArticleNum = articleNumList[keyData.year][month][0][0];
							
							target.forEach(function(data){
								data.articleNum = targetArticleNum.v;
							})
							
						}catch(e){							
						}
						
						
						monthTag = monthTag.concat(target);
					})
					
					targetData = monthTag;							
				}
					break;
				case "month": {
					const weeks = Object.keys(tagList[keyData.year][keyData.month]);
					let weekTag = new Array();										
					
					weeks.forEach(function(week) {
						const target = tagList[keyData.year][keyData.month][week];
						try{
							targetArticleNum=articleNumList[keyData.year][keyData.month][week][0];
							
							target.forEach(function(data){
								data.articleNum = targetArticleNum.v;
							})							
						}catch(e){							
						}
											
						weekTag = weekTag.concat(target);
					})
					targetData = weekTag;
				}
					break;
				case "week":
					const target = tagList[keyData.year][keyData.month][keyData.week];
					try{
						targetArticleNum = articleNumList[keyData.year][keyData.month][keyData.week];
						
						target.forEach(function(data){
							
							const targetArticleNum_day = targetArticleNum.filter(function(aData){
								return data.d ==aData.d;
							}).reduce(function(acc,curr){
								return curr;
							},null);
							
							if(targetArticleNum_day)
								data.articleNum=targetArticleNum_day.v;
							
						})
					}
					catch(e){
						
					}
					
					targetData = target;
					break;
				}

				if (concatFlag) {
					if (that.chartData[key])
						that.chartData[key] = that.chartData[key].concat(targetData);
					else
						that.chartData[key] = targetData;
				} else
					that.chartData[key] = targetData;
				
				

			} else {
				if (!that.chartData[key] || that.chartData[key].length <= 0)
					that.chartData[key] = undefined;
			}

		} catch (e) {
			//that.chartData[key] = new Array();
		}

	})

	return that;
}

// 차트를 그릴 데이터를 가져오는 함수
TagData.prototype.getTagData = function(params) {
	const that = this;

	let result = new Object();
	let _keys = this.makeDatasetKey(params);

	let _tagNameList = params.tagNameList;
	const _limit = params && params.limit;
	if (typeof keys === "string") {
		const keyArr = new Array();
		keyArr.push(_keys);
		_keys = keyArr;
	}
	let topRankTagList;
	// 상위 n개 만 옵션이 있으면
	if (_limit) {
		let limitObj = new Object();

		limitObj = _keys.flatMap(function(key) {
			const data = that.chartData[key];
			return data || new Array();
		})

		// 이름으로 그룹핑
		const group = _.groupBy(limitObj, "t");

		const tagNames = Object.keys(group);

		// 그룹핑된 태그들의 frequency
		topRankTagList = tagNames.map(function(tagName) {

			return {
				tag_name : tagName,
				val : group[tagName].reduce(function(acc, curr) {
					return acc + parseInt(curr.f);
				}, 0)
			}

		}).sort(function(a, b) {
			const diff = b.val - a.val;
			return diff;
			//return diff < 0 ? 1 : (diff > 0 ? -1 : 0)
		}).slice(0, _limit).map(function(k) {
			return k.tag_name;
		});
	}

	_keys.forEach(function(key) {

		const hashtagList = that.chartData[key];

		const keyData = JSON.parse(key);

		if (hashtagList != null) {
			result[key] = hashtagList.filter(function(tag) {
				if (topRankTagList != null && topRankTagList.length > 0)
					return topRankTagList.includes(tag.t);
				else
					return tag;
			}).filter(function(tag) {
				if (_tagNameList != null && _tagNameList.length > 0)
					return _tagNameList.includes(tag.t);
				else
					return tag;

			});

		} else
			result[key] = undefined; // 불러온 결과가 없는 경우

	})

	return result;
}

//페이지를 불러왔을때 세팅된 html tag데이터를 취합해 setTagData 에 넣을 수 있는 형태로 만들어 리턴
TagData.prototype.getInitData = function(elem, param) {

	const result = new Object();
	const initTag = $(elem).find(".hashtag");
	const initArticleNum = $(elem).find(".articleNum");
	
	const hashtagList = new Object();
	const articleNumList = new Object();
	
	const timeMode = param.timeMode;
	let target;
	let articleTarget;

	if (Array.isArray(elem)) {
		target = elem;
	} else {
		//init 정보를 가지고있는지 확인하는 attribute
		const initFlag = typeof $(elem).attr("data-init") !== 'undefined' || $(elem).find("[data-init]").length > 0;		
		if (!initFlag)
			return result;
		
		articleTarget = initArticleNum.map(function(idx,item){						
			return $(item).data();
		}).toArray();

		target = initTag.map(function() {
			return $(this).data();
		}).toArray();

	}

	target.forEach(function(data) {
		if (!hashtagList[data.year || data.y])
			hashtagList[data.year || data.y] = new Object();
		if (!hashtagList[data.year || data.y][data.month || data.m])
			hashtagList[data.year || data.y][data.month || data.m] = new Object();
		if (!hashtagList[data.year || data.y][data.month || data.m][data.week || data.w || 0])
			hashtagList[data.year || data.y][data.month || data.m][data.week || data.w || 0] = new Array();
	});

	target.map(function(tag) {
		const data = tag;

		Object.keys(data).forEach(function(key) {

			if (key === "f")
				data[key] = parseInt(data.f);
			else if (key === "d") {
				if (data.d)
					data[key] = data.d + "";
			} else if (data[key] === "")
				delete data[key];
		})
		return data;

	}).forEach(function(data) {
		const insert_date = moment(data.d);
		const year = data.y || data.year || insert_date.year();
		const month = data.m || data.month || (insert_date.month() + 1);
		const week = data.w || data.week || 0;
			
		if(articleTarget){
			const articleNum = articleTarget.filter(function(data){
				return data.y==year&&data.m==month&&data.w==week;				
			}).reduce(function(acc,curr){
				return curr;
			},null);
			
			if(articleNum)
				data.articleNum=articleNum.v;
		}
				
		hashtagList[year][month][week].push(data);
	})

	result.hashtagData = new Object();
	result.hashtagData.hashtagList = hashtagList;
	result.hashtagData.timeMode = timeMode;

	return result;

}

//hashtag정보를 가져오기 위한 ajax parameter를 리턴
TagData.prototype.getHashtagParams = function() {
	const that = this;
	const selectedTagValue = that.menu.getSelectedTagValue();
	let result = {
		board_code : that.boardCode,
		board_name : selectedTagValue.join(' '),
		site_code : that.menu.getSelectedSiteData(),
		tagNameList : selectedTagValue,
		limit : that.menu.getLimitValue(),
		weekList : that.menu.getSelectedTimeData(),
		timeMode : that.menu.getTimeMode()
	};

	return result;
}

// 파라미터로 들어온 옵션과 디폴트 옵션을 비교 merge 하는 함수, 파라미터 옵션에 없는 property는 디폴트 옵션의 prop으로 변경
TagData.prototype.updateOption = function(paramOption, defaultOption) {

	const result = new Object();
	const defaultOptionKeys = Object.keys(defaultOption);

	if (paramOption) {

		defaultOptionKeys.forEach(function(key) {

			result[key] = paramOption[key] === undefined ? defaultOption[key] : paramOption[key];
		})

	} else {
		defaultOptionKeys.forEach(function(key) {
			result[key] = defaultOption[key];
		})
	}

	return result;
}

function appendNoDataElement(elem, flag) {
	
	flag ? elem.find(".content__noData").length<=0&&elem.append("<div class='content__noData'></div>") : elem.find(".content__noData").remove();
}

TagData.prototype.defaultMenuOption = {
	siteMenu : false,
	tagMenu : false,
	timeMenu : false,
	limitMenu : false,
	modalMenu : false,
	multiTimeSelect : false
}

//태그를 눌렀을때 나타나는 메뉴 
var tagMenu = (function TagMenu() {
	var that = this;
	var _instance;
	var _element = $("#tagMenu");
	var _tagNameElement = _element.find(".tagMenu__tagName");
	var _boardNameElement = _element.find(".tagMenu__boardName");
	var _searchBoardElem = _element.find(".tagMenu__searchBoardAddr");//게시판 안에서 검색
	var _searchTimeSetElem = _element.find(".tagMenu__searchTimeSet");//하위 시간 부르기
	var _searchSubmit = _element.find(".tagMenu__searchSubmit");//키워드 검색
	var _searchRelativeTag = _element.find(".tagMenu__searchRelativeTag");//관련 태그 검색
	var _articleWrapper = _element.find(".tagMenu__articles");
	var _loadMoreBtn = _element.find(".tagMenu__loadMore");
	var _lastPageDiv = _element.find(".tagMenu__lastPage");
	var _articleCategoryBtn = _element.find(".tagMenu__articleCategory .btn");
	var _arccordion = $("#tagMenu__arccordion");
	var _posOffset = 5;
	var _appeared = false;//모달이 완전히 보이게 되면 토글되는 변수
	var _contentData;//클릭된 element로 부터 받은 데이터들
	var _prevPos;//collapse show 하기 전의 위치
	var _isMobile= isMobile();

	//해당 게시판 검색 주소 설정
	function setBoardSearchAddress(address, keyword) {
		_searchBoardElem.find("a").attr("href", address.replace(/(___)/g, keyword));
	}

	//tagpan 검색 주소 설정
	function setSearchSubmitAddress(params) {
		
		const _params={};
		_params.keyword=params.keyword;
		_params.board_name = params.board_name;
		_params.search_target="all";
		_params.timeMode=params.timeMode;
		_params.weekList=params.weekList;	
				
		_searchSubmit.find("a").attr("href", searchBar.makeQueryString(_params));
	}

	function getArticleWrapper(type) {
		return type ? _articleWrapper.filter("[data-type=" + type + "]") : _articleWrapper;
	}

	function showArticleWrapper(flag) {
		flag ? _articleWrapper.show() : _articleWrapper.hide();
	}

	function getLoadMoreBtn(type) {
		return type ? _loadMoreBtn.filter("[data-type=" + type + "]") : _loadMoreBtn;
	}

	function getLastPageDiv(type) {
		return type ? _lastPageDiv.filter("[data-type=" + type + "]") : _lastPageDiv;
	}

	function showSearchBoardElem(flag) {
		if (flag) {
			_searchBoardElem.show();
			_searchBoardElem.parent().show();

		} else {
			_searchBoardElem.hide();
			_searchBoardElem.parent().hide();
		}
	}

	function clearArticle(type) {

		if (type) {
			getArticleWrapper(type).find("tbody *").remove();
		} else
			getArticleWrapper().find("tbody *").remove();
	}

	function moveMenu(data) {

		const pos = data.pos ? data.pos : data;
		
		_prevPos = _prevPos || pos;

		let _posX = pos ? (pos.x ? pos.x : 0) : 0;
		let _posY = pos ? (pos.y ? pos.y : 0) : 0;

		if ((_posX + _element.width() >= document.body.clientWidth)||_isMobile)
			_posX = (document.body.clientWidth-_element.width())/2;//window.outerWidth - _element.width() * 1.2;
		
		  if (_posY + _element.height() >= window.innerHeight) 
			  _posY = window.innerHeight - _element.height()-_posOffset; 
		  else		 
			  _posY += _posOffset;

		_element.css({
			left : _posX,
			top : _posY
		});
	}

	//스크롤 설정
	$("#tagMenu__newsContent").scrollbar();
	$("#tagMenu__communityContent").scrollbar();

	//메뉴외 다른곳을 선택하면 사라지도록 
	$(window).on("click", function(e) {

		const target = $(e.target);

		if (_appeared && (target.parent().data("dismiss") === "modal" || target.closest(".tagMenu").length < 1))
			tagMenu.getInstance().showMenu(false);

	})

	//모달이 닫히면 처음 띄울때 세팅한 차트의 parameter를 null로 초기화
	_element.on("hide.bs.modal", function() {
		_articleCategoryBtn.removeClass("active");
		_contentData = null;
	})

	_loadMoreBtn.on("click", function() {
		_contentData.params.offset += 1;
		setArticleContent(_contentData.params);
	})

	//다른 시간대 정보 불러오기 메뉴 클릭할때
	_searchTimeSetElem.on("click", function(e) {
		e.preventDefault();
		returnObj.showMenu(false);
		//const content = $(this).data("content");
		const targetKeyword = _contentData.params.board_name;
		const _params = _contentData.params;
		//delete _params.keyword;
		const targetObj = _contentData.board?_contentData.board.chartObj:_contentData.chartObj;
		const filteredParam =targetObj ? targetObj.getNonExistDatasetKey(_params) : _params;
		
		if (filteredParam != null) {
			filteredParam.keyword = targetKeyword;
			getHashtagData(filteredParam).done(function(data) {
				_contentData.chartObj.tagMenuTimeSetBtnCallback(data, filteredParam, targetKeyword);
			})
		} else {
			//데이터가 있다면 해당 시간 메뉴를 클릭
			_params.weekList.forEach(function(ts) {
				switch (_params.timeMode) {
				case "year":
					targetObj.menu.getYearElement(ts.year, ts.month).trigger("click");
					break;
				case "month":
					targetObj.menu.getMonth(ts.year, ts.month).trigger("click");
					break;
				case "week":
					targetObj.menu.getWeekElement(ts.year, ts.month, ts.week).trigger("click");
					break;
				}
			})

		}

	})
	//collapse 버튼 클릭할때 active 클래스 토글
	_articleCategoryBtn.on("click",function(){
		$(this).toggleClass("active").siblings().removeClass("active");		
	})

	//collapse 이벤트 구현(commnunity, rss collapse 버튼 클릭)
	_element.on("show.bs.collapse", function(e) {
				
		// 펼쳣을때 화면 맨 위로 올림
		moveMenu({
			x:_element[0].getBoundingClientRect().left,
			y:0
		})			
		const target = $(e.target);
		const type = target.data("type");
		const offset = target.data("offset");
		_contentData.params.type = type;
		_contentData.params.offset = offset;
		if (getArticleWrapper(type).find("tr").length <= 0)
			setArticleContent(_contentData.params);

	}).on("shown.bs.collapse", function(e) {

		/*
		 * const event = new Object(); const rect =
		 * _element[0].getBoundingClientRect(); event.pos = { x : rect.x, y :
		 * rect.y } moveMenu(event);
		 */
	}).on("hidden.bs.collapse", function() {
		setTimeout(function(){
			if (_element.find(".show").length <= 0){
				//캐러셀2개 다 숨긴 상태				
				moveMenu(_prevPos);	
			}
				
		},30)		
	})

	function showTagMenuContent(content) {

		const flag = content.showMenuFlag;

		flag.searchBoard ? _searchBoardElem.show() : _searchBoardElem.hide();
		flag.searchTimeSet ? _searchTimeSetElem.show() : _searchTimeSetElem.hide();
		flag.searchTagpan ? _searchSubmit.show() : _searchSubmit.hide();
		flag.articleArccordian ? _arccordion.show() : _arccordion.hide();
		//flag.searchRelativeTag ? _searchRelativeTag.show() : _searchRelativeTag.hide();

	}

	function showArticleBtn(type, flag) {
		const loadMoreBtn = getLoadMoreBtn(type);
		const lastPage = getLastPageDiv(type);
		if (flag) {
			loadMoreBtn.hide();
			lastPage.show();
		} else {
			loadMoreBtn.show();
			lastPage.hide();
		}

	}
	function setBoardContent(content) {
		const board = content.board;
		const board_name = content.params.board_name;
		_contentData = content;
		const _params = _contentData.params;
		//한단계 아래의 timeMode를 세팅해야 제대로 불러와 짐
		//_params.timeMode = _params.timeMode == "year" ? "month" : (_params.timeMode == "month" ? "week" : "day");

		//부모 게시판이 있으면 해당 데이터를 세팅해줌
		if (board) {
			setBoardSearchAddress(board.searchAddress, board_name);//검색 주소 넣기
			_boardNameElement.text(board.boardName).parent().show();
		}

		showTagMenuContent(_contentData);
		(content.showMenuFlag.searchTimeSet && content.params.timeMode != 'day') ? setTimeSetSearchContent(_contentData).show() : _searchTimeSetElem.hide();

		//홈페이지 검색할 address 세팅
		setSearchSubmitAddress(_params);
		_tagNameElement.text(board_name);

	}
	//시간 단위 검색할때 메뉴에 텍스트 넣어주는 함수
	function setTimeSetSearchContent(content) {

		if (content.params) {
			const params = _contentData.params;
			try {
				const timeSet = params.weekList[0];
				const keyword = params.board_name;
				const timeModeForText = params.timeMode;
				_searchTimeSetElem.html((content.board ? `<a href="#">'<strong>${content.board.boardName}</strong>'의 ` : `<a href="#">`)
						+ `'<strong>${timeSet.year}</strong>년 <strong>${timeSet.month}</strong>월` + (timeModeForText == 'week' ? `<strong>${timeSet.week}</strong>주차' ` : `'	`)
						+ ` '<strong>${keyword}</strong>' 그래프 불러오기 <i class="fa-solid fa-down-long"></i>`);
				//_searchTimeSetElem.data("content", content);
			} catch (e) {
				console.log(e);
			}
		}

		return _searchTimeSetElem;
	}

	function setArticleContent(content) {

		const params = _contentData.params;

		const articleData = articleMgr.getInstance().getArticle(params);
		if (articleData) {
			params.offset < 1 && clearArticle(params.type);
			setArticle(articleData.articleList, params);
			//setArticleContent(articleData.articleList, content.params, articleData.last);
			showArticleBtn(params.type, articleData.last);
		} else {
			try {
				getNewsCommunityContent(getUrlparameter(params)).then(function(data) {
					articleMgr.getInstance().setArticle(params, data);
					//setArticleContent(data.result.articleList, content.params, data.result.last);
					params.offset < 1 && clearArticle(params.type);
					setArticle(data.result.articleList, params);
					showArticleBtn(params.type, data.result.last);
				})
			} catch (e) {
				console.log(e);
			}
		}

	}

	function setArticle(articleList, params) {

		const articleWrapper = getArticleWrapper(params.type);
		const $frag = $(document.createDocumentFragment());

		articleList
				&& articleList.forEach(function(article) {

					const row = $("<tr class='article'>");

					const replacedTitle = article.title.replace(params.board_name, "<strong>$&</strong>");
					
					if(params.type=='news')
						row.append(`<td class="d-none" scope="row"><span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span></td>
									<td>
										<div class="row">
											<div class="col-12 short-text">
												<a class="articles__address" href="${article.content_address }" target="_blank">${replacedTitle}</a>
											</div>
											<div class="col d-block">
												<span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span>
											</div>
											<div class="col small text-muted">
												<i class="fa-solid fa-calendar fa-xs"></i>${article.d}</div>
										</div>
									</td>`)
					else
						row.append(`<td class="d-none" scope="row"><span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span></td>
									<td>
										<div class="row">
											<div class="col-12 short-text">
												<a class="articles__address" href="${article.article_address }" target="_blank">${replacedTitle }</a>
											</div>
											<div class="col-12">
												<div class="row small text-muted">
													<div class="col d-block">
														<span class="badge" style="background:${article.site_color};color:white;">${article.site_name }<a href="${article.site_address }"><i class="fas fa-external-link-alt"></i></a></span>
													</div>
													<div class="col short-text">
														<a href="${article.board_address}" title="${article.board_name}"> <i class="fas fa-external-link-alt fa-xs"></i></a><span>${article.board_name }</span> 
													</div>													
													<div class="col">
														<i class="fa-solid fa-download fa-xs"></i>${article.d}</div>
												</div>
											</div>
										</div>
									</td>`);
					
					

					/*
					 * row.append("<td ><span class='badge short-text w-100'
					 * style='background:" + article.site_color +
					 * ";color:white'>" + article.site_name + "</span></td>");
					 * row.append("<td class='short-text'><div
					 * class='short-text'><a href='" + (article.article_address ||
					 * article.content_address) + "' target='_blank' title='" +
					 * article.title + "'>" + replacedTitle + "</a></div></td>");
					 * row.append("<td><i class='fas fa-external-link-alt
					 * text-primary'></i></td>");
					 */

					$frag.append(row);
				})

		articleWrapper.find("tbody").append($frag);

	}

	var returnObj = {
		showMenu : function(bool, data) {
			if (bool) {
				_appeared = false;
				moveMenu(data);
				setBoardContent(data);
				_element.show();
				setTimeout(function() {
					_appeared = true;
				}, 100);
				_params = data;
			} else {
				_element.hide().find(".show").removeClass("show");
				_articleCategoryBtn.removeClass("active");
				_appeared = false;
				_prevPos = null;
				_params = null;
				clearArticle();
			}

		}
	};

	function init() {

		return returnObj;

	}

	return {
		getInstance : function() {
			if (_instance)
				return _instance;
			else
				return _instance = init();
		}
	}

})();

//크게 보기 모달
function ChartModal() {
	var _instance;
	var _element = $("#chartModal");
	var _modalBody = _element.find(".modal-body");
	var _origin;
	var afterHiddenModalCallback;
	var afterShownModalCallBack;

	function checkContentAlreadyExist() {
		return _modalBody.find(">").length > 0;
	}

	function attachContent(origin, content, shownCallback, hiddenCallback) {
		if (checkContentAlreadyExist())
			throw new Error("이미 컨텐츠가 존재합니다.");

		if (!origin)
			throw new Error("origin이 존재하지 않습니다.");
		else
			_origin = origin;

		_modalBody.append(content);
		afterShownModalCallBack = shownCallback;
		afterHiddenModalCallback = hiddenCallback;

	}

	function detachContent() {
		if (checkContentAlreadyExist())
			return _modalBody.find(">").detach();
		else
			return null;
	}

	function attachContentToOrigin() {
		_origin.append(detachContent());
		afterShownModalCallBack = null;
		afterHiddenModalCallBack = null;
		_origin = null;
	}

	_element.on("shown.bs.modal", function() {
		typeof afterShownModalCallBack === "function" && afterShownModalCallBack();
	})

	_element.on("hidden.bs.modal", function() {
		attachContentToOrigin();
		typeof afterHiddenModalCallback === "function" && afterHiddenModalCallback();
	})

	function init() {

		return {
			getDOMElement : function() {
				return _element;
			},
			attachContent : attachContent,
			detachContent : detachContent,
			showModal : function() {
				_element.modal("show");
			},
			hideModal : function() {
				_element.find(".show").removeClass("show");
				_element.modal("hide");
			}
		}

	}

	return {
		getInstance : function() {
			if (_instance)
				return _instance;
			else
				return _instance = init();
		}
	}

}

function ArticleManager() {
	var _container = new Object();

	var temp = sessionStorage.getItem("news");
	try {
		_container = temp != null ? JSON.parse(temp) : new Object();
	} catch (e) {
		_container = new Object();
	}

	var _instance;

	function getArticle(params) {
		return _container[makeKey(params)];
	}

	function setArticle(params, data) {
		const target = data.result ? data.result : data;
		_container[makeKey(params)] = target;
	}

	function makeKey(params) {
		//moment는 tostring 하게되면 init할때의 날짜가 찍히므로 바뀐 날짜를 적용하기위해 바꿔줘야됨
		const weekListReplacement = params.weekList && params.weekList.filter(function(k) {
			return k.day;
		}).length > 0 ? params.weekList.map(function(k) {
			const target = _.cloneDeep(k);
			target.day = moment(target.day).format("yyyy-MM-DD");
			return target;
		}) : params.weekList;

		const keyObj = {
			keyword : params.keyword,
			timeMode : params.timeMode,
			weekList : weekListReplacement,
			offset : params.offset,
			type : params.type
		}

		return JSON.stringify(keyObj);
	}

	function getBackupData() {
		return JSON.stringify(_container);
	}

	function init() {

		return {
			getArticle : getArticle,
			setArticle : setArticle,
			getBackupData : getBackupData,
			getContainer : function() {
				return _container;
			},
			clearContainer : function() {
				_container = new Object();
			},
			setSessionStorage : function() {

				const keys = Object.keys(_container);

				keys.forEach(function(key) {

				})

			}
		}

	}

	return {

		getInstance : function() {

			if (_instance)
				return _instance;
			else
				return _instance = init();

		}

	}

}

//해시태그의 데이터를 가져옴
function getHashtagData(data) {

	return $.ajax({
		type : "GET",
		url : '/getHashtag.ajax',
		data : getUrlparameter(data),
		dataType : "json",
		traditioanl : true
	});

}

//관련 태그의 정보를 가져옴
function getRelativeTagData(data) {
	return $.ajax({
		type : "GET",
		url : '/selectRelativeTag.ajax',
		data : getUrlparameter(data),
		dataType : "json",
		traditioanl : true
	});
}

//게시판 초기화 정보를 가져옴
function getBoardContent(data) {
	return $.ajax({
		type : "GET",
		url : '/getBoardContent.ajax',
		data : getUrlparameter(data),
		dataType : "json",
		traditional : true
	});
}

function getNewsCommunityContent(params) {

	if (!articleLoadFinished)
		return new Promise(function(resolve, reject) {
			reject();
		});
	articleLoadFinished = false;
	return $.getJSON("/selectNewsCommunityContent.ajax", getUrlparameter(params)).then(function(data) {
		articleLoadFinished = true;
		return data;
	});
}

//객체 안의 array를 을 spring이 파싱할 수 있는 형태로 변경해서 property에 넣어줌
function getUrlparameter(obj) {

	const _data = _.cloneDeep(obj);
	if (obj.weekList) {

		delete _data.weekList;

		//spring 이 받는 형식
		obj.weekList.forEach(function(timeSet, idx) {
			if (timeSet.year)
				_data["weekList[" + idx + "].year"] = timeSet.year;
			if (timeSet.month)
				_data["weekList[" + idx + "].month"] = timeSet.month;
			if (timeSet.week)
				_data["weekList[" + idx + "].week"] = timeSet.week;
			if (timeSet.day)
				_data["weekList[" + idx + "].day"] = timeSet.day;
		})
	}

	//parameter에 필요없는 2개의 데이터는 삭제
	delete _data.tagNameList;
	delete _data.keyword;
	return _data;
}

var relativeBoardMenu = (function RelativeBoardMenu() {

	var element = $("#relativeBoardMenu");
	var elementBody = element.find(".card-body");
	var closeReady = false; ///닫을 준비가 되있는지 확인하는 변수
	var boardNameElem = element.find(".relativeBoardMenu__boardName");
	var siteNameElem = element.find(".relativeBoardMenu__siteName");
	var popularTagElem = element.find(".relativeBoardMenu__popularTag");
	var feedBackNumElem = element.find(".relativeBoardMenu__feedbackNum");
	var feedBackRatioElem = element.find(".relativeBoardMenu__feedbackRatio");
	var lastUpdateElem = element.find(".relativeBoardMenu__lastUpdate");
	var searchBoardElem = element.find(".relativeBoardMenu__SearchBoard");

	function setSiteData(data) {
		siteNameElem.css("background", data.site_color).find("a").attr("href", data.site_address).text(data.site_name);
	}

	function setBoardData(data) {
		boardNameElem.find("a").attr("href", data.board_address).text(data.board_name).append("<i class='fas fa-external-link-alt'></i>");
	}

	function setPopularTagData(data) {
		popularTagElem.children().remove();
		data.popular_hashtag_code_list && data.popular_hashtag_code_list.forEach(function(tag) {
			popularTagElem.append(`<span class='badge badge-primary mr-1'><i class='fas fa-hashtag' aria-hidden='true'></i>${tag.t}</span>`);
		})
	}

	function setFeedBackNumData(data) {
		feedBackNumElem.text(`피드백 갯수 : ${data.all_comment_list_lenght} 개`);
	}

	function setSearchBoardData(data) {
		const params = searchBar.getSearchParams();
		params.board_name = data.board_name;
		/*
		 * params.keyword.push(data.board_name); params.keyword =
		 * data.board_name.split(KEYWORD_DELIMETER);
		 */

		params.search_target = "board";
		searchBoardElem.find("a").attr("href", searchBar.makeQueryString(params)).html(`<strong>TagPan</strong> 에서 '${data.board_name}' 검색하기<i class='fas fa-search' aria-hidden='true'></i>`);
	}

	function setFeedBackRatioData(data) {

		const evalSum = (data.positive + data.negative);
		const noEval = evalSum <= 0;

		const evalElem = element.find(".relativeBoardMenu__noEval");
		const progressBar = element.find(".progress-bar")
		const positive = element.find(".board__progressInner--positive");
		const negative = element.find(".board__progressInner--negative");

		if (noEval) {
			positive.hide();
			negative.hide();
			evalElem.show();
			progressBar.removeClass("bg-danger").addClass("bg-secondary");
		} else {
			positive.show();
			negative.show();
			evalElem.hide();
			progressBar.addClass("bg-danger").removeClass("bg-secondary");

			const positiveRatio = Math.round(((data.positive / evalSum) * 100)) + "%";
			const negativeRatio = Math.round(((data.negative / evalSum) * 100)) + "%";
			positive.css("width", positiveRatio).text(positiveRatio === "0%" ? '' : positiveRatio);
			negative.css("width", negativeRatio).text(negativeRatio === "0%" ? '' : negativeRatio);

		}

	}

	function setLastUpdateData(data) {
		lastUpdateElem.text(`마지막 업데이트 : ${moment(data.boardData.update_date).format('YYYY-MM-DD')}`);
	}

	function setHeaderContent(data) {
		setSiteData(data);
		setBoardData(data);
		setPopularTagData(data);
		setFeedBackNumData(data);
		setFeedBackRatioData(data);
		//setLastUpdateData(data);
		setSearchBoardData(data);
	}

	function clearHeaderContent() {

	}

	function positioningMenu(pos) {

		if (!pos)
			return;			
		
		let _posX = pos ? (pos.x ? pos.x : 0) : 0;
		let _posY = pos ? (pos.y ? pos.y : 0) : 0;

		if ((_posX + element.width() >= document.body.clientWidth)||isMobile())
			_posX = (document.body.clientWidth-element.width())/2;//window.outerWidth - _element.width() * 1.2;
		
		 if (_posY + element.height() >= window.innerHeight) 
			  _posY = window.innerHeight - element.height()-10; 
		  else		 
			  _posY -= 10;
		
		element.css({
			left : _posX,
			top : _posY
		});
		

		/*
		 * const xOffset = isMobile()?0:25; const yOffset = isMobile()?0:40;;
		 * let posX = pos.x != undefined ? pos.x : 0; let posY = pos.y !=
		 * undefined ? pos.y + window.scrollY : 0;
		 * 
		 * posX = (posX + element.width()) > window.innerWidth ?
		 * window.innerWidth - element.width() : posX; posY = (posY +
		 * element.height()) > (window.innerHeight + window.scrollY) ?
		 * (window.innerHeight + window.scrollY) - element.height() : posY;
		 * 
		 * element.css("left", posX - xOffset); element.css("top", posY -
		 * yOffset);
		 */

	}

	function showMenu(data) {

		positioningMenu(data.pos);
		setHeaderContent(data.data);
		element.show();
		closeReady = false;
		setTimeout(function() {
			closeReady = !closeReady;
		}, 300);
	}

	function hideMenu() {
		clearHeaderContent();
		element.hide();
		closeReady = false;

	}

	$(window).on("click", function(e) {
		const target = $(e.target);

		if (closeReady)
			if (target.parent().data("dismiss") === "modal" || target.closest("#relativeBoardMenu").length <= 0)
				hideMenu();

	})

	return {
		setHeaderContent : setHeaderContent,
		clearHeaderContent : clearHeaderContent,
		positioningMenu : positioningMenu,
		showMenu : showMenu,
		hideMenu : hideMenu
	}

})();

$("#tagPanInfo").on("click", function() {

	$("#siteInfo").modal("show");
})

function insertHashtag(value) {
	return $.post("/insertUserHashtag.ajax", value);
}

var hashtagModal = (function HashtagModal() {
	var modal = $("#hashtagModal");
	var input = modal.find(".hashtagModal__input");
	var boardCode = null;
	modal.on("hide.bs.modal", function() {
		clearInput();
		boardCode = null;
	});

	input.autocomplete({
		source : function(request, response) {

			$.ajax({
				type : "GET",
				url : '/selectSearchTag.ajax',
				dataType : "json",
				data : {
					board_name : request.term
				},
				success : function(data) {

					let responseObj = new Object();
					const hashtagList = data.result ? data.result.hashtagList : null;
					if (hashtagList && hashtagList.length > 0) {

						responseObj = hashtagList.map(function(data) {
							return {
								value : data.t,
								label : data.t,
								board_code : boardCode
							}
						})
					}

					response(responseObj);
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					alert("결과를 가져오는 도중 문제가 생겼습니다.");
				}
			});

		},
		focus : function(event, ui) {
			return false;
		},
		delay : 1000,
		minLength : $("input[name=autoCompleteMinLen]").val(),
		select : function(event, ui) {
			insertHashtag({
				tag_name : ui.item.value,
				board_code : ui.item.board_code
			}).then(function(data) {
				data.result && data.result.status > 0 && alert("추가되었습니다");
			})
		}
	})

	function getInput() {
		return input;
	}

	function clearInput() {
		input.val("");
	}

	function showModal() {
		modal.modal("show");
	}

	function hideModal() {
		modal.modal("hide");
	}

	return {
		getInput : getInput,
		clearInput : clearInput,
		showModal : showModal,
		hideModal : hideModal,
		setBoardCode : function(data) {
			boardCode = data;
			return this;
		}
	}

})();

function getTagHeatMapData(params) {

	return $.getJSON("/selectTagHeatMapData.ajax", getUrlparameter(params));

}
var keywordModal;
//검색 할때 키워드 추가하는 모달 
if ($("#keywordModal").length > 0)
	keywordModal = (function KeywordModal() {
		var _modal = $("#keywordModal");
		var _input = _modal.find(".keywordModal__input");
		var _confirmBtn = _modal.find(".keywordModal__confirm");
		var _specialChar = new RegExp($("input[name=specialChar]").val(), "g");
		var _maxSearchKeywordLen = parseInt($("input[name=maxSearchKeywordLen]").val());
		var _keywordNumLimit = parseInt($("input[name=searchKeywordNumLimit]").val());
		var _cloneWrapper = _modal.find(".keywordModal__dummyWrapper");
		var _callback;
		var _that = this;

		_input.autocomplete({
			source : function(request, response) {

				$.ajax({
					type : "GET",
					url : '/selectSearchTag.ajax',
					dataType : "json",
					data : {
						tag_name : request.term.replaceAll(ChartMenu.specialChar, '')
					},
					success : function(data) {

						let responseObj = new Object();
						const hashtagList = data.result ? data.result.hashtagList : null;
						if (hashtagList && hashtagList.length > 0) {

							responseObj = hashtagList.map(function(data) {
								return {
									value : data.t,
									label : data.t
								}
							})
						} else {
							const temp = new Array();
							temp.push({
								value : request.term,
								labal : request.term,
								noResult : true
							})
							responseObj = temp;
						}

						response(responseObj);
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						alert("결과를 가져오는 도중 문제가 생겼습니다.");
					}
				});

			},
			focus : function(event, ui) {
				return false;
			},
			delay : 1000,
			minLength : $("input[name=autoCompleteMinLen]").val(),
			select : function(event, ui) {
				const targetKeyword = _that.getKeywordDummyTarget(ui.item.value);

				if (targetKeyword.length > 0) {
					event.preventDefault();
					_cloneWrapper.append(_that.makeSearchTagElement(targetKeyword));
				}
				_input.val("").blur();

			}
		}).autocomplete("instance")._renderItem = function(ul, item) {
			const $wrapper = $("<li>");
			if (!item.noResult) {
				$wrapper.append('<div class="text-secondary mr-1">' + (item.noResult ? "<i class='fas fa-search mr-1'></i>" : "<i class='fas fa-hashtag'></i>")
						+ ("<strong>" + item.value + "</strong>") + '</div>')

				return $wrapper.appendTo(ul);
			} else
				return $wrapper;

		}

		//입력된 값을을 적절히 가공해서 키워드 형태로 만들고 중복,길이 확인해서 최종적으로 추가해야되는 키워드 정보를 리턴
		this.getKeywordDummyTarget = function(value) {

			value = value.split(KEYWORD_DELIMETER); //띄어쓰기,세미콜론으로 나눔

			const targetKeywordList = _that.filteringDuplicate(value);

			return (_that.checkKeywordLength(targetKeywordList) && targetKeywordList.length > 0) ? targetKeywordList : null;

		}

		//현재 dummy들의 캐릭터 길이랑 입력된 값의 길이 합을 합쳐서 리미트를 안 넘는지 확인
		this.checkKeywordLength = function(value) {
			const keywords = _that.getKeywordList();

			if (keywords.length + 1 > _keywordNumLimit) {
				makeToast({
					type : "warning",
					message : "검색어의 갯수는 " + _keywordNumLimit + "개 이하여야 합니다."
				});
				return false;
			} else if (value.join().length > _maxSearchKeywordLen) {//검색어 하나 길이 확인
				makeToast({
					type : "warning",
					message : "검색어 글자 수는 총 " + _maxSearchKeywordLen + "자 이하여야 합니다."
				});
				return false;
			} else
				return true;
		}

		//중복된거 제거 한것들을 리턴
		this.filteringDuplicate = function(value) {
			const keywords = _that.getKeywordList();//현재 추가된 keyword dummy의 리스트
			const result = new Array();

			value.forEach(function(item) {
				if (!keywords.includes(item))
					result.push(item);
			})
			/*
			 * if (!keywords.includes(value)) result.push(value);
			 */
			return result;
		}

		//검색할때 추가할 태그의 dummy를 만드는 함수
		this.makeSearchTagElement = function(data) {

			const elements = $(document.createDocumentFragment());
			if (data) {
				data.forEach(function(data) {

					const element = $("<span class='badge badge-secondary option__keyword' data-keyword='" + data + "'>" + "<i class='fas fa-search mr-1'></i>" + data
							+ "<i class='fas fa-times ml-1'></i></span>");
					element.on("click", function() {
						$(this).remove();
					})

					elements.append(element);
				})
			}

			return elements;
		}

		//추가된 키워드 리스트를 가져옴
		this.getKeywordList = function() {

			return _cloneWrapper.find(".option__keyword").map(function() {
				return $(this).data("keyword");
			}).toArray();
		}

		//확인 버튼 누르면 모달 닫고 데이터 넘겨줌
		_confirmBtn.on("click", function() {
			confirmEvent();
		})

		//엔터 누르면 모달 닫고 데이터 넘겨줌
		_input.keydown(function(e) {
			if (e.keyCode == 13) {
				e.preventDefault();
				confirmEvent();
			}
		})

		this.confirmEvent = function() {
			const value = _input.val();
			if (value) {
				const targetKeyword = _that.getKeywordDummyTarget(value);
				if (targetKeyword && targetKeyword.length > 0) {
					_cloneWrapper.append(_that.makeSearchTagElement(targetKeyword));
					_input.val('');
				}
			}
			_callback && _callback({
				dummys : _cloneWrapper.find(".option__keyword")
			});
			_modal.modal("hide");
		}

		function showModal(dummyData) {
			_cloneWrapper.append(dummyData);
			_modal.modal("show");
		}

		_modal.on("shown.bs.modal", function() {
			_input.focus();
		})

		_modal.on("hide.bs.modal", function() {
			_cloneWrapper.find(".option__keyword").remove();
		})

		_cloneWrapper.on("click", function(e) {
			const target = $(e.target);

			if (target.hasClass("option__keyword"))
				target.remove();
		})

		this.returnObj = {
			setCallback : function(callback) {
				_callback = callback;
				return returnObj;
			},
			showModal : showModal
		}
		return returnObj;

	})();

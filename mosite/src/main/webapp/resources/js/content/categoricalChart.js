function CategoricalChart(elem, option) {

	this._elem = elem;
	this._canvas = this._elem.find("canvas")[0];
	this.chart;
	this._chartData;

	if (option) {
		if (option.init)
			this.init();
	}

}

CategoricalChart.prototype.init = function(rawData) {

	this._chartData = rawData ? this._makeChartData(rawData) : this._makeChartData(this._parseInitData());
	this.draw();
}

CategoricalChart.prototype._makeChartData = function(targetData) {

	const chartData = new Object();
	chartData.datasets = new Array();
	const dataset = new Object();

	targetData = targetData.sort(function(a, b) {
		return b.val - a.val;//순서대로
	});

	const labels = targetData.map(function(item) {
		return item.category_name;
	})

	const data = targetData.map(function(item) {
		return item.val;
	})

	const backgroundColor = targetData.map(function(item, idx) {
		return chartColor.getInstance().getDataColor(idx);
	});

	dataset.data = data;
	dataset.backgroundColor = backgroundColor;
	dataset.hoverOffset = 4;

	chartData.labels = labels;
	if (data.length > 0)
		chartData.datasets.push(dataset);

	return chartData;
}

CategoricalChart.prototype._parseInitData = function(ajaxData) {

	if (this._elem.find("[data-init]").length > 0) {
		return this._elem.find("[data-init] > div").map(function() {
			return $(this).data();
		}).toArray();
	}

}

CategoricalChart.prototype.draw = function(data) {

	if (!this._canvas) {
		Error(this.name + " : canvas가 존재하지 않습니다.");
		return;
	}

	const targetData = data ? data : this._chartData;

	appendNoDataElement(this._elem, this._chartData.datasets.length <= 0);

	if (this.chart)
		this.chart.desroy();

	this.chart = new Chart(this._canvas, {
		type : "doughnut",
		data : targetData,
		plugins : [ ChartDataLabels ],
		options : {
			maintainAspectRatio : false,
			plugins : {
				legend : {
					display : false
				},
				title : {
					display : true,
					text : "게시판 성향"
				},
				tooltip : {
					callbacks : {
						label : function(context) {
							const idxObj = this.getActiveElements(context)[0];
							const label = this.chart.data.labels[idxObj.index];
							const data = this.chart.data.datasets[idxObj.datasetIndex].data;
							const sum = data.reduce(function(acc, curr) {
								return acc + curr;
							}, 0);
							return `${label} : ${Math.round((data[idxObj.index] / sum) * 100)}%`;
						}
					}
				},
				datalabels : {
					formatter : function(value, context) {

						const sum = context.dataset.data.reduce(function(acc, curr) {
							return acc + curr;
						}, 0);

						const targetData = Math.round((context.dataset.data[context.dataIndex] / sum) * 100);

						if (targetData > 8) {
							return context.chart.data.labels[context.dataIndex];
						} else
							return '';
					},
					anchor : "center",
					align : "center",
					color : 'white',
					font : {
						weight : "bold"
					}
				}
			}
		}
	})

}
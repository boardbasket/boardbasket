/**
 * 
 */

function TagInfo(element, option) {
	this.element = element;
	var that = this;
	this.chartData = {};
	this.mostMentionedDateElem = $(".tagInfo__mostMentionedDate");
	this.firstRegisteredElem = $(".tagInfo__firstRegistered");
	this.lastRegisteredElem = $(".tagInfo__lastRegistered");
	this.menuTimer;
	this.menu = new ChartMenu(element.find(".tagInfo__menu"), Object.assign({}, {
		multiTagSelect : false,
		tagMenu : true,
		modalMenu:false,
		onTagClick : function(target) {
			clearTimeout(that.menuTimer);
			that.menuTimer = setTimeout(function() {
				const tag = that.menu.getSelectedTagValueJoin();

				if (!tag)
					return;

				const targetData = that._getData(tag);

				if (targetData)
					that.draw(targetData, tag);
				else {
					const params = {
						weekList : that.menu.getSelectedTimeData(),
						board_name : that.menu.getSelectedTagValueJoin(),
						timeMode : that.menu.getTimeMode()
					}

					that._getTagInfoData(params).done(function(data) {
						const key = params.board_name;
						that._setData({
							key : params.board_name,
							data : data.result
						});//tagInfo 데이터 저장
						const targetData = that._getData(key);
						that.draw(targetData, tag);
					})

				}
			}, 1000);

		}
	}, option.menu || {}));

	if (option) {
		if (option.init) {

			const initData = this.getInitData();
			const key = option.labelData.slice(0, 1);
			this._setData({
				data : initData
			});
			that.menu.addTagElement(option.labelData, key);
			this.draw();

		}
	}

	//날짜를 클릭하면 태그 메뉴 부르기
	element.find(".tagInfo__desc").on("click", function(e) {

		if (!$(this).data("day"))
			return;

		const weekList = [];
		weekList.push({
			"day" : moment($(e.target).data("day") + "").toDate()
		});
		tagMenu.getInstance().showMenu(true, {
			pos : {
				x : e.clientX,
				y : e.clientY
			},
			params : {
				site_code : that.menu.getSelectedSiteData(),
				board_name : that.menu.getSelectedTagValueJoin(),
				timeMode : "day",
				weekList : weekList,
				offset : 0
			},
			showMenuFlag : {
				searchTimeSet : false,
				searchBoard : that.parent ? that.parent.searchAvailable ? true : false : false,
				articleArccordian : true
			}
		})
	})

}

TagInfo.prototype.getInitData = function() {

	const initData = {};

	const firstAndLast = this.element.find("[data-key=firstAndLast]").map(function() {
		return $(this).data();
	}).toArray();

	const mostMentionedDate = this.element.find("[data-key=mostMentionedDate]").map(function() {
		return $(this).data();
	}).toArray();

	initData.firstAndLast = firstAndLast;
	initData.mostMentionedDate = mostMentionedDate;
	return initData;
}

TagInfo.prototype.parseData = function(data) {
	const result = {};
	try {
		const groupedFirstLast = _.groupBy(data.firstAndLast, "t");
		const groupedMostMentioned = _.groupBy(data.mostMentionedDate, "t");

		const keys = Object.keys(groupedFirstLast);

		keys.forEach(function(key) {

			const target = groupedFirstLast[key];

			const sorted = target.sort(function(a, b) {

				if (moment(String(a.d)).isSameOrBefore(moment(String(b.d))))
					return -1;
				else
					return 1;
			});

			const first = sorted[0];
			const last = sorted[1];
			const mostMentioned = groupedMostMentioned[key][0];
			result[key] = {
				firstRegistered : first,
				lastRegistered : last,
				mostMentioned : mostMentioned
			}

		})
	} catch (e) {
	}

	return result;

}

TagInfo.prototype._getTagInfoData = function(params) {

	return $.ajax({
		type : "GET",
		url : '/getTagInfo.ajax',
		data : getUrlparameter(params),
		async : false,
		dataType : "json",
		traditioanl : true
	});

}

TagInfo.prototype._getData = function(key) {

	return this.chartData[key];

}

TagInfo.prototype._setData = function(param) {
	const that = this;
	if (param.key) {
		this.chartData[param.key] = param.data;
	} else if (param.data) {
		const result = this.parseData(param.data);

		const keys = Object.keys(result);

		keys.forEach(function(key) {
			that.chartData[key] = result[key];
		})
	}

	//this.chartData[key] = data.result ? data.result : data;

}

TagInfo.prototype.getLastRegisteredElem = function() {
	return this.lastRegisteredElem;
}

TagInfo.prototype.getFirstRegisteredElem = function() {
	return this.firstRegisteredElem;
}

TagInfo.prototype.getTagRankElem = function() {
	return this.tagRankElem;
}

TagInfo.prototype.setTagInfoText = function(elem, data) {
	if (data)
		elem.find(".tagInfo__desc").attr("data-day", data.d).attr("title", `${data.y}년 ${data.m}월 ${data.w}주차`).text(moment(data.d + "").format("yyyy-MM-DD (dd)")).attr("title",
				data.w ? +data.w + "주차" : '');
	else
		elem.find(".tagInfo__desc").attr("data-day", "").text("데이터가 없습니다").attr("title", "");
}

TagInfo.prototype.getMostMentionedDateElem = function() {
	return this.mostMentionedDateElem;
}

TagInfo.prototype.setLastRegistered = function(data) {

	this.setTagInfoText(this.getLastRegisteredElem(), data);
}

TagInfo.prototype.setFirstRegistered = function(data) {
	this.setTagInfoText(this.getFirstRegisteredElem(), data);
}

TagInfo.prototype.setMostMentionedDate = function(data) {
	this.setTagInfoText(this.getMostMentionedDateElem(), data);
}

TagInfo.prototype.clearTextData = function() {
	this.setLastRegistered("");
	this.setFirstRegistered("");
	this.setMostMentionedDate("");
}

TagInfo.prototype.draw = function(data) {

	const selectedTag = this.menu.getSelectedTagValueJoin();
	const targetData = data ? data : this._getData(selectedTag);
	this.clearTextData();
	if (targetData) {
		this.setLastRegistered(targetData.lastRegistered);
		this.setFirstRegistered(targetData.firstRegistered);
		this.setMostMentionedDate(targetData.mostMentioned);
	} else {
		this.setLastRegistered(null);
		this.setFirstRegistered(null);
		this.setMostMentionedDate(null);
	}

}

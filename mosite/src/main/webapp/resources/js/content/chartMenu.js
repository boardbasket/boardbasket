function ChartMenu(elem, option) {
	var that = this;
	this.optionElement = (elem instanceof jQuery) ? elem : $(elem);
	this.optionElement = this.optionElement.hasClass("option") ? this.optionElement : this.optionElement.find(".option");
	// time element	
	var _timeWrapper = this.optionElement.find(".option__times");
	var _week = this.optionElement.find(".option__week:not(.option__week--disabled)");
	var _weeks = this.optionElement.find(".option__weeks");
	var _month = this.optionElement.find(".option__month");
	var _months = this.optionElement.find(".option__months");
	var _year = this.optionElement.find(".option__year");
	var _timeModeBtn = this.optionElement.find(".option__time");
	var _resetTimeElement = this.optionElement.find(".option__resetTime");
	var _resetTagElement = this.optionElement.find(".option__resetTag");
	var _sites = this.optionElement.find(".option__site");
	var _siteWrapper = this.optionElement.find(".option__sites");// 사이트 감싸고 있는 wrapper
	var _tagWrapper = this.optionElement.find(".option__tagWrapper");
	var _limitWrapper = this.optionElement.find(".option__limitWrapper");
	var _hoverMenuWrapper = this.optionElement.find(".option__hoverMenuWrapper");
	var _cloudMenuWrapper = this.optionElement.find(".option__wordCloud");
	this._modalBtn = this.optionElement.find(".option__showModal");
	var _hashtagModalBtn = this.optionElement.find(".option__openHashtagModal");
	var _timeMode = this.optionElement.find("input.option__timeMode");
	var _openAddTagModalBtn = this.optionElement.find(".option__openHashtagModal");
	var _simpleMenu = _timeWrapper.data("simpleMenu");//게시판 chart,wordCloud등에 들어갈때 true
	var _maxTagSelectNum = parseInt($("[name=maxChartTagSelect]").val());
	this.option = option;
	if (option) {
		const defaultOptionKeys = Object.keys(ChartMenu.defaultOption);

		defaultOptionKeys.forEach(function(key) {
			if (option[key] === undefined)
				option[key] = ChartMenu.defaultOption[key];
		})
	} else
		option = ChartMenu.defaultOption;

	if (option.siteMenu)
		_siteWrapper.removeClass("option__sites--disabled").addClass("show");

	if (option.tagMenu)
		_tagWrapper.addClass("show");

	if (option.limitMenu)
		_limitWrapper.addClass("show");

	if (option.modalMenu)
		that.optionElement.find(".option__hoverMenuWrapper").removeClass("d-none");
	else
		that.optionElement.find(".option__hoverMenuWrapper").addClass("d-none");

	if (option.timeMenu)
		that.optionElement.find(".option__times").addClass("show");

	if (option.openAddTagModalBtn) {
		_cloudMenuWrapper.addClass("show");
		_openAddTagModalBtn.css("display", "inline-block");
	}

	if (option.zoomRange)
		_cloudMenuWrapper.addClass("show");

	if (option.isMobile || typeof isMobile === 'function' ? isMobile() : false) {

		/*
		 * _siteWrapper.find(">").addClass("scrollbar-inner").scrollbar();
		 * _weeks.addClass("scrollbar-inner").scrollbar();
		 * _months.addClass("scrollbar-inner").scrollbar();
		 */

		/*
		 * _weeks.each(function() { $(this).css("white-space",
		 * "nowrap").scrollbar(); })
		 * 
		 * _months.each(function() { $(this).css("white-space",
		 * "nowrap").scrollbar(); })
		 */

	}

	if (option.limitArr) {
		const oldLimit = _limitWrapper.find("*");

		const target = oldLimit.eq(0).clone();

		option.limitArr.forEach(function(elem) {
			const target = oldLimit.eq(0).clone();
			target.attr(`data-limit`, `${elem}`).data("limit", `${elem}`).html(`상위 ${elem}개 <i class="fas fa-sync-alt" aria-hidden="true"></i>`).appendTo(_limitWrapper);
		})
		oldLimit.remove();
	}

	if (typeof option.initialLimit == 'number')
		_limitWrapper.find(".option__limit[data-limit=" + option.initialLimit + "]").addClass("option__limit--selected").siblings().removeClass("option__limit--selected");

	this.getSites = function() {
		return _sites;
	}
	this.getOption = function() {
		return option;
	}

	this.getTimeModeBtn = function() {
		return _timeModeBtn;
	}

	this.getTimeModeElement = function() {
		return _timeMode;
	}

	this.getHoverMenuWrapper = function() {
		return _hoverMenuWrapper;
	}

	this.getMonth = function(year, month) {

		return (typeof month === "undefined" || typeof year === "undefined") ? _month : _month.filter(`[data-year=${year}][data-month=${month}]`);
	}

	this.getMonths = function(year) {
		return typeof year === 'undefined' ? _months : _months.filter(`[data-year=${year}]`);
	}

	this.getWeekElement = function(year, month, week) {

		if (year != undefined && month != undefined)

			if (typeof week !== 'undefined')
				return _week.filter(`[data-year=${year}][data-month=${month}][data-week=${week}]`);
			else
				return _week.filter(`[data-year=${year}][data-month=${month}]`);
		else
			return _week;
	}

	this.setWeekElement = function(elem) {
		if (elem.toArray) {
			elem.each(function(i, item) {
				_week.push(item);
			})
		} else
			_week.push(elem[0]);
	}

	this.setMonth = function(elem) {

		if (elem.toArray) {
			elem.each(function(i, item) {
				_month.push(item);
			})
		} else
			_month.push(elem[0]);

	}

	this.getWeeksElement = function(year, month) {
		if (typeof year !== 'undefined' && typeof month !== 'undefined')
			return _weeks.filter(`[data-year=${year}][data-month=${month}]`);
		else
			return _weeks;
	}

	//
	this.getYearElement = function(val) {

		if (typeof val === "undefined")
			return _year;
		else
			return _year.filter(`[data-year=${val}]`);

	}

	//시간 옵션이 들어있는 div리턴
	this.getTimeWrapper = function() {
		return _timeWrapper;
	}

	// 태그 들어있는 div 리턴
	this.getTagWrapper = function() {
		return _tagWrapper;
	}

	this.getLimitWrapper = function() {
		return _limitWrapper;
	}

	// 시간 초기화 버튼 리턴
	this.getResetTimeElement = function() {
		return _resetTimeElement;
	}

	// 태그 초기화 버튼 리턴
	this.getResetTagElement = function() {
		return _resetTagElement;
	}

	// 태그 엘리먼트를 만드는 함수
	function makeTagElement(data, selectedTag) {

		const dataNotExistTag = new Array();

		if (selectedTag && selectedTag.length > 0) {
			selectedTag.forEach(function(tag) {
				if (data.length > 0 && !data.includes(tag)) {
					data.push(tag);
					dataNotExistTag.push(tag);
				}

			})
		}

		let result = data.map(function(tag, i) {
			const tagElem = $('<span class="option__tag badge badge-primary" data-tag-name="' + tag + '">' + tag + " <i class='fas fa-check'></i>" + '</span>');

			// 선택된 태그이거나 차트 타입이 line이면 선택클래스를 넣어서 태그를 만듬
			if ((selectedTag && selectedTag.includes(tag)))
				tagElem.addClass("option__tag--selected");

			//선택된 태그가 태그 라벨에 존재하지 않는경우 아이콘을 x로 바꿔줌
			if (dataNotExistTag.includes(tag)) {
				tagElem.addClass("bg-danger").find("i").remove();
				tagElem.append('<i class="fa-solid fa-circle-xmark"></i>');
			}

			return tagElem;
		})

		return result;
	}

	// 태그 element들을 리턴
	this.getTagElement = function() {
		return that.optionElement.find(".option__tag");
	}

	// 태그데이터로 태그들을 만들어 차트에 붙이는 함수
	this.addTagElement = function(labelData, selectedTag) {

		_tagWrapper.find(".option__tag").remove();// 모두 삭제

		if (labelData)
			_tagWrapper.find(".scroll-content").append(makeTagElement(labelData, selectedTag));

	}

	// 현재 선택된 사이트 하나를 가져오는 함수
	this.getSelectedSiteData = function() {
		return _siteWrapper.is(":visible") ? that.getSelectedSite().data("siteCode") : undefined;
	}

	// 태그 선택시 이벤트(태그가 수시로 지워지므로 래퍼 클래스 이벤트 캡쳐링 이용
	this.getTagWrapper().on("click", function(e) {
		const target = $(e.target).hasClass("option__tag") ? $(e.target) : $(e.target).parent();

		if (target.hasClass("option__tag")) {
			if (!target.hasClass("option__tag--selected")) {
				if (!option.multiTagSelect)
					$(this).find(".option__tag").removeClass("option__tag--selected");
				else if (that.getSelectedTag().length >= _maxTagSelectNum) {
					makeToast({
						type : "warning",
						message : _maxTagSelectNum + "개 까지 선택 가능합니다."
					})
					return;
				}
			}

			target.toggleClass("option__tag--selected");
			option.onTagClick && option.onTagClick(target);
		}
	})

	// 사이트 클릭하면 선택 이벤트 토글
	_sites.on("click", function(e) {

		const target = $(this).addClass("option__site--selected");
		_sites.each(function() {
			!$(this).is(target) && $(this).removeClass("option__site--selected");
		})

		//$(this).addClass("option__site--selected").siblings().removeClass("option__site--selected");
		option.onSiteClick && option.onSiteClick(e);
	})

	// 연, 월,주차 단위 버튼을 누를 경우 이벤트
	_timeModeBtn.on("click", function(e) {
		const target = $(this);
		const mode = target.data("mode");
		_timeMode.val(mode);
		//that.setTimeToDefaultState();
		target.addClass("option__time--selected");
		_timeModeBtn.not(target).removeClass("option__time--selected");
		let selectedYear = that.getSelectedYear();
		let selectedMonth = that.getSelectedMonth();

		switch (mode) {
		case "year":
			_week.removeClass("option__week--selected");
			_weeks.removeClass("option__weeks--selected");
			_month.removeClass("option__month--selected");
			_months.removeClass("option__months--selected");
			break;
		case "month":
			_week.removeClass("option__week--selected");
			_weeks.removeClass("option__weeks--selected");

			if (selectedYear.length <= 0) {
				selectedYear = that.getYearElement().last();
			} else if (selectedYear.length > 1) {
				selectedYear = that.getSelectedYear().last()
			}
			selectedYear.siblings().removeClass("option__year--selected");
			if (selectedMonth.length <= 0) {
				_months.removeClass("option__months--selected");
				_month.removeClass("option__month--selected");
				selectedMonth = that.getMonths(selectedYear.data("year")).find(".option__month").last();
			}
			that.getMonths(selectedYear.data("year")).addClass("option__months--selected");
			selectedMonth.addClass("option__month--selected");
			break;
		case "week":
			if (selectedYear.length <= 0) {
				selectedYear = that.getYearElement().last();
			} else if (selectedYear.length > 1) {
				selectedYear = that.getSelectedYear().last();
			}

			selectedYear.siblings().removeClass("option__year--selected");
			if (selectedMonth.length <= 0)
				selectedMonth = that.getMonths(selectedYear.data("year")).addClass("option__months--selected").find(".option__month").last();
			else {
				selectedMonth = selectedMonth.last();
				that.getMonth().removeClass("option__month--selected");
			}

			const monthData = selectedMonth.data();
			selectedMonth.addClass("option__month--selected");
			that.getWeeksElement(monthData.year, monthData.month).addClass("option__weeks--selected");
			that.getWeekElement(monthData.year, monthData.month).last().addClass("option__week--selected");
			break;
		}

		option.onTimeClick && option.onTimeClick(e);

	})

	//이벤트 캡쳐링
	_timeWrapper.on("click", function(e) {
		that.timeWrapperEventFunc(e);
	})

	this.timeWrapperEventFunc = function(e) {
		const target = $(e.target).attr("data-time-mode") ? $(e.target) : $(e.target).parent();
		const targetTimeMode = target.data().timeMode;

		if (!targetTimeMode)
			return;

		let mode = that.getTimeMode();

		if (_simpleMenu && typeof targetTimeMode != 'undefined' && targetTimeMode != mode && !target.is("[class*=selected]")) {
			//현재 클릭한 시간 옵션의 timemode가 현재의 세팅된 timeMode와 다른경우 바꿔줌
			that.setTimeMode(targetTimeMode);
			mode = targetTimeMode;

			//현재 클릭된 시간옵션의 타임모드가 아닌것들을 모두 선택 취소
			that.deselectOption(_timeWrapper.find("[data-time-mode]").filter(`:not([data-time-mode=${targetTimeMode}])`));
		}

		if (target.hasClass("option__year")) {
			that.yearClickEvent(target, mode);
			option.onYearClick && option.onYearClick(e);
		} else if (target.hasClass("option__month")) {
			that.monthClickEvent(target, mode);
			option.onMonthClick && option.onMonthClick(e);
		} else if (target.hasClass("option__week")) {
			that.weekClickEvent(target)
			option.onWeekClick && option.onWeekClick(e);
		}

		if (target.hasClass("option__dummy"))
			target.remove();
	}

	this.monthClickEvent = function(elem, mode) {

		const data = elem.data();
		const target = elem.hasClass("option__dummy") ? that.getMonth(data.year, data.month) : elem;

		switch (mode) {
		case 'month':
			if (target.hasClass("option__month--selected")) {
				target.removeClass("option__month--selected");
			} else if (that.getSelectedMonth().length < maxMonthOptionNum)
				target.addClass("option__month--selected");
			else {
				if (_simpleMenu) {
					target.addClass("option__month--selected");
				} else {
					that.getMonth().removeClass("option__month--selected");
					target.addClass("option__month--selected");
				}

			}
			break;
		case 'week':
			target.addClass("option__month--selected").siblings().removeClass("option__month--selected");
			_weeks.removeClass("option__weeks--selected");
			_weeks.filter(`[data-year=${target.data("year")}][data-month=${target.data("month")}]`).addClass("option__weeks--selected");
			break;
		}

	}

	this.weekClickEvent = function(elem) {

		const data = elem.data();
		const target = elem.hasClass("option__dummy") ? that.getWeekElement(data.year, data.month, data.week) : elem;

		if (!option.multiTimeSelect)
			that.getWeekElement().removeClass("option__week--selected");

		if (target.hasClass("option__week--selected"))
			target.removeClass("option__week--selected");
		else if (that.getSelectedWeek().length < maxWeekOptionNum)
			target.addClass("option__week--selected");
		else {
			if (_simpleMenu) {
				target.addClass("option__week--selected");
			} else {
				makeToast({
					type : "warning",
					message : maxWeekOptionNum + "개 까지 선택 가능합니다."
				})
			}
		}

	}

	this.yearClickEvent = function(elem, mode) {

		const year = elem.data("year");
		const targetYear = _year.filter(`[data-year=${year}]`);

		if (mode == 'year') {
			if (targetYear.hasClass("option__year--selected"))
				targetYear.removeClass("option__year--selected");
			else
				targetYear.addClass("option__year--selected");
		} else {
			targetYear.addClass("option__year--selected").siblings().removeClass("option__year--selected");
		}

		if (mode == 'month' || mode == 'week') {

			const targetMonths = _months.filter(`[data-year=${year}]`).addClass("option__months--selected");
			targetMonths.siblings().removeClass("option__months--selected");

			if (mode == 'week') {
				_month.removeClass("option__month--selected");
				targetMonths.find(".option__month").last().addClass("option__month--selected");
				const data = that.getSelectedMonth().last().data();
				_weeks.removeClass("option__weeks--selected");
				const targetWeeks = _weeks.filter(`[data-year=${data.year}][data-month=${data.month}]`).addClass("option__weeks--selected");
				//targetWeeks.find(".option__week").last().addClass("option__week--selected");
			}
		}

	}

	// 시간 초기화 버튼 누를때 발생하는 이벤트
	_resetTimeElement.on("click", function(e) {
		that.setOptionToDefaultState();
		option.onResetTime && option.onResetTime(e);
	})

	// 태그 초기화 버튼 누를때 발생하는 이벤트
	_resetTagElement.on("click", function(e) {
		that.resetAllTagElement();
		option.onResetTag && option.onResetTag(e);
	});

	// 리미트 버튼 클릭시 이벤트
	_limitWrapper.on("click", function(e) {
		if ($(e.target).hasClass("option__limit")) {
			that.toggleClassOfLimitElement($(e.target));
			option.onLimitClick && option.onLimitClick(e);
		}
	})

	// tagList에 스크롤바 플러그인 적용
	_tagWrapper.find(".scrollbar-inner").scrollbar();

	// 모달 버튼 클릭했을때
	this._modalBtn.on("click", function() {
		option.onModalClick && option.onModalClick();
	})

	//wordcloud 태그 추가 버튼을 눌렀을때
	_hashtagModalBtn.on("click", function() {
		option.onHashtagModalClick && option.onHashtagModalClick();
	})

	// 해당 태그를 선택해제 하는 함수
	this.unSelectTagElement = function(tag) {
		const target = tag ? that.getTagElement().filter(function(k) {
			return $(k).data("tagName") === tag;
		}) : that.getTagElement();

		target.removeClass("option__tag--selected");

		return that;
	}

	//해당 태그를 선택하는 함수
	this.selectTagElement = function(tag) {
		const type = typeof tag;

		switch (type) {
		case 'object':
			tag.addClass("option__tag--selected");
			break;
		case 'string':
			that.getTagElementWithName(tag).addClass("option__tag--selected");
			break;
		case 'undefined':
			that.getTagElement().addClass("option__tag--selected");
			break;
		}
	}

	this.deselectOption = function(elem) {
		if (elem.toArray)
			elem.each(function(i, k) {
				deselectOptionInner(k);
			})
		else
			deselectOptionInner(elem);

	}

	function deselectOptionInner(elem) {

		if ($(elem).hasClass("option__year"))
			$(elem).removeClass("option__year--selected");
		else if ($(elem).hasClass("option__month"))
			$(elem).removeClass("option__month--selected");
		else if ($(elem).hasClass("option__week"))
			$(elem).removeClass("option__week--selected");
	}

	this.isSimpleMenu = function() {
		return _simpleMenu;
	}

}
//다른 시간대를 불러왔을때 시간 메뉴를 만듬
ChartMenu.prototype.makeTimeMenu = function(timeMode, ts) {

	const elem = $("<div class='badge badge-primary'><i class='fas fa-check' aria-hidden='true'></i></div>");
	elem.attr(`data-time-mode`, timeMode);
	elem.attr(`data-year`, ts.year);
	ts.month && elem.attr(`data-month`, ts.month);
	ts.week && elem.attr(`data-week`, ts.week);

	switch (timeMode) {
	case "year":
		elem.addClass("option__year").prepend(document.createTextNode(`${ts.year}년`));
		break;
	case "month":
		elem.addClass("option__month").prepend(document.createTextNode(`${ts.year}년` + ` ${ts.month}월`.padStart(2, 0)));
		break;
	case "week":
		elem.addClass("option__week").prepend(document.createTextNode(`${ts.year}년` + ` ${ts.month}월 `.padStart(2, 0) + `${ts.week}주차`));
		break;
	}

	return elem;

}

//year,month,time을 값을 가지거나 가지지 않는 timeOption을 가지고 옴
ChartMenu.prototype.getTimeOption = function(year, month, week) {

	if (year) {
		if (month) {
			if (week) {
				return this.getWeekElement(year, month, week);
			} else
				return this.getMonth(year, month);
		} else
			return this.getYearElement(year);

	} else
		return null;
}

//year,month,week 가지고 해당 element를 찾은뒤 알맞은 선택 클래스를 추가 (simpleMenu용)
ChartMenu.prototype.selectTimeOption = function(year, month, week) {

	try {
		if (year) {
			if (month) {
				if (week) {
					return this.getWeekElement(year, month, week).addClass("option__week--selected");
				} else
					return this.getMonth(year, month).addClass("option__month--selected");
			} else
				return this.getYearElement(year).addClass("option__year--selected");

		} else
			return null;
	} catch (e) {
		console.log(e);
	}

}
//year,month,week 가지고 해당 element를 찾은뒤 알맞은 선택 클래스를 제거 (simpleMenu용)
ChartMenu.prototype.deselectTimeOption = function(year, month, week) {

	try {
		if (year) {
			if (month) {
				if (week) {
					return this.getWeekElement(year, month, week).removeClass("option__week--selected");
				} else
					return this.getMonth(year, month).removeClass("option__month--selected");
			} else
				return this.getYearElement(year).removeClass("option__year--selected");

		} else
			return null;
	} catch (e) {
		console.log(e);
	}

}

ChartMenu.prototype.deselectAllTimeOption = function() {
	const that = this;
	that.getTimeWrapper().find("[class*=option__]").each(function(i, item) {
		const data = $(this).data();

		try {
			that.deselectTimeOption(data.year, data.month, data.week);
		} catch (e) {
		}
	})

}

//다른 시간대를 시간 메뉴를 붙임
ChartMenu.prototype.addTimeMenu = function(elem, timeMode) {
	const detached = this.getResetTimeElement().css("display", "flex").detach();
	switch (timeMode) {
	case "month": {
		this.setMonth(elem);
		break;
	}
	case "week": {
		this.setWeekElement(elem);
		break;
	}
	}

	this.getTimeWrapper().find(">div:first-of-type").append(elem);
	this.sortTimeMenu();
	this.getTimeWrapper().find(">div:first-of-type").append(detached);

}

//새 시간 옵션이 추가 되었을때 sorting 해주는 함수
ChartMenu.prototype.sortTimeMenu = function() {
	if (this.isSimpleMenu()) {

		const target = this.getTimeWrapper().find(">div:first-of-type");
		const detached = target.find("[data-time-mode]").detach();

		detached.sort(function(a, b) {
			return $(a).text().localeCompare($(b).text());
		}).each(function(i, item) {
			target.append(item);
		})
	}
}

//현재 시간 모드가 무슨 값인지(year,month...)
ChartMenu.prototype.getTimeMode = function() {
	return this.getTimeModeElement().val();
}
//초기 시간 모드(페이지 불러올때 설정된 모드)
ChartMenu.prototype.getInitialTimeMode = function() {
	return this.getTimeModeBtn().filter(function() {
		return $(this).data("initial");
	}).data("mode");
}
//시간 모드 값 setting
ChartMenu.prototype.setTimeMode = function(value) {
	this.getTimeModeElement().val(value);
}

ChartMenu.prototype.getSelectedSite = function() {
	return this.getSites().filter("[class*=selected]");
}

ChartMenu.prototype.getSelectedMonth = function() {
	return this.getMonth().filter(".option__month--selected");
}

ChartMenu.prototype.getSelectedYear = function() {
	return this.getYearElement().filter(".option__year--selected");
}

ChartMenu.prototype.getModalBtn = function() {

	return this._modalBtn;
}

ChartMenu.prototype.getLimitElement = function() {
	return this.getLimitWrapper().find(".option__limit");
}

ChartMenu.prototype.toggleClassOfLimitElement = function(elem) {

	$(elem).addClass("option__limit--selected").siblings(".option__limit").removeClass("option__limit--selected");

}

ChartMenu.prototype.getLimitValue = function() {
	return this.getLimitWrapper().find(".option__limit--selected").data("limit");
}

ChartMenu.prototype.toggleClassOfSiteElement = function(elem) {
	$(elem).addClass("option__site--selected").siblings().removeClass("option__site--selected");
}

ChartMenu.prototype.getTagWithName = function(tagName) {
	return tagName ? this.getTagElement().filter("[data-tag-name=" + tagName + "]") : this.getTagElement();
}

ChartMenu.prototype.getSelectedTag = function() {
	return this.getTagElement().filter("[class*=option__tag--selected]");
}

ChartMenu.prototype.getSelectedTagValue = function() {
	return this.getSelectedTag().map(function(idx, element) {
		return $(element).data("tagName");
	}).toArray();
}
ChartMenu.prototype.getSelectedTagValueJoin = function() {
	return this.getSelectedTag().map(function(idx, element) {
		return $(element).data("tagName");
	}).toArray().join(" ");
}

// 선택된 tag element를 선택 안된 상태로 모두 전환,스크롤 초기화
ChartMenu.prototype.resetAllTagElement = function() {

	this.getTagElement().parent().scrollLeft(0);
	this.getTagElement().removeClass("option__tag--selected");

}

// 옵션을 처음 선택된 모습으로 초기화
ChartMenu.prototype.setOptionToDefaultState = function() {

	const that = this;

	const weekLength = this.getWeekElement().length;
	//사이트 초기화
	this.getSites().eq(0).addClass("option__site--selected").siblings().removeClass("option__site--selected");

	//timeMode 초기화
	this.getTimeModeBtn().each(function(idx, element) {

		if ($(element).data("initial")) {
			that.setTimeMode($(element).data("mode"));
			$(element).addClass("option__time--selected")
		} else
			$(element).removeClass("option__time--selected");
	})

	//연도 초기화
	this.getYearElement().removeClass("option__year--selected").filter(`[data-initial=true]`).addClass("option__year--selected");

	//월 초기화
	this.getMonth().removeClass("option__month--selected").filter(`[data-initial=true]`).addClass("option__month--selected");
	this.getMonths().filter(function() {
		const initMonth = $(this).find(`[data-initial=true]`);
		initMonth.addClass("option__month--selected");
		return initMonth.length > 0
	}).last().addClass("option__months--selected").siblings().removeClass("option__months--selected");

	//주 초기화
	this.getWeekElement().removeClass("option__week--selected").filter(`[data-initial=true]`).addClass("option__week--selected");
	this.getWeeksElement().filter(function() {
		const initWeek = $(this).find(`[data-initial=true]`);
		initWeek.addClass("option__week--selected");
		return initWeek.length > 0;
	}).last().addClass("option__weeks--selected").siblings().removeClass("option__weeks--selected");

}

ChartMenu.prototype.getSelectedWeek = function() {
	return this.getWeekElement().filter(".option__week--selected");
}

// 선택 클래스가 있는 time엘리먼트의 데이터를 map 형태로 리턴
ChartMenu.prototype.getSelectedTimeData = function() {
	let target;
	switch (this.getTimeMode()) {
	case "year":
		target = this.getSelectedYear();
		break;
	case "month":
		target = this.getSelectedMonth();
		break;
	case "week":
		target = this.getSelectedWeek();
		break;
	}

	return target.map(function(idx, elem) {

		/*
		 * const data = {}; const year = $(elem).data("year"); const month =
		 * $(elem).data("month"); const week = $(elem).data("week");
		 */

		return $(elem).data();

	}).toArray();

}

// 페이지 불러올때의 설정된 초기 time 데이터를 가져옴
ChartMenu.prototype.getInitialTimeData = function() {
	const that = this;
	const timeMode = this.getInitialTimeMode();
	let targetElement
	switch (timeMode) {
	case "year":
		targetElement = this.getYearElement();
		break;
	case "month":
		targetElement = this.getMonth();
		break;
	case "week":
		targetElement = this.getWeekElement();
		break;
	}

	let target = targetElement.filter(function() {
		return $(this).is(`[data-initial=true]`)
	});

	return target.map(function(idx, elem) {
		return $(elem).data();
	}).toArray();

}

ChartMenu.defaultOption = {
	siteMenu : false,
	tagMenu : false,
	timeMenu : false,
	limitMenu : false,
	modalMenu : false,
	multiTimeSelect : true,
	multiTagSelect : true
}

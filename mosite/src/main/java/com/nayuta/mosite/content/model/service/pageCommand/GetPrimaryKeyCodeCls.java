package com.nayuta.mosite.content.model.service.pageCommand;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import com.nayuta.mosite.common.util.RefCodeConst;
import com.nayuta.mosite.content.model.vo.validatorGroup.BoardCodeGroup;

public abstract class GetPrimaryKeyCodeCls implements Serializable {

	private static final long serialVersionUID = 1L;
	protected Map<String, Object> codeMap = new HashMap<String, Object>();
	protected String user_tab_code;
	protected String user_board_code;
	protected String board_code;
	protected String site_code;
	protected String eval_code;
	protected String comment_code;
	protected String hashtag_code;

	public String getUser_tab_code() {
		return user_tab_code;
	}

	public String getUser_board_code() {
		return user_board_code;
	}

	@NotBlank(groups = { BoardCodeGroup.class })
	public String getBoard_code() {
		return board_code;
	}

	public String getSite_code() {
		return site_code;
	}

	public String getHashtag_code() {
		return hashtag_code;
	}

	public void setUser_tab_code(String user_tab_code) {
		this.user_tab_code = user_tab_code;
	}

	public void setUser_board_code(String user_board_code) {
		this.user_board_code = user_board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public void setHashtag_code(String hashtag_code) {
		this.hashtag_code = hashtag_code;
	}

	public String getEval_code() {
		return eval_code;
	}

	public void setEval_code(String eval_code) {
		this.eval_code = eval_code;
	}

	public String getComment_code() {
		return comment_code;
	}

	public void setComment_code(String comment_code) {
		this.comment_code = comment_code;
	}

	public void pushPrimaryKey(Map<String, Object> codeMap) {

		if (codeMap.containsKey(RefCodeConst.userTabCodeRefMap))
			this.user_tab_code = (String) codeMap.get(RefCodeConst.userTabCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.userBoardCodeRefMap))
			this.user_board_code = (String) codeMap.get(RefCodeConst.userBoardCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.boardCodeRefMap))
			this.board_code = (String) codeMap.get(RefCodeConst.boardCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.siteCodeRefMap))
			this.site_code = (String) codeMap.get(RefCodeConst.siteCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.hashtagCodeRefMap))
			this.hashtag_code = (String) codeMap.get(RefCodeConst.hashtagCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.evalCodeRefMap))
			this.eval_code = (String) codeMap.get(RefCodeConst.evalCodeRefMap);

		if (codeMap.containsKey(RefCodeConst.commentCodeRefMap))
			this.comment_code = (String) codeMap.get(RefCodeConst.commentCodeRefMap);

	}

	public Map<String, Object> pullPrimaryKey() {

		if (user_tab_code != null)
			codeMap.put(RefCodeConst.userTabCodeRefMap, user_tab_code);

		if (user_board_code != null)
			codeMap.put(RefCodeConst.userBoardCodeRefMap, user_board_code);

		if (board_code != null)
			codeMap.put(RefCodeConst.boardCodeRefMap, board_code);

		if (site_code != null)
			codeMap.put(RefCodeConst.siteCodeRefMap, site_code);

		if (hashtag_code != null)
			codeMap.put(RefCodeConst.hashtagCodeRefMap, hashtag_code);

		if (eval_code != null)
			codeMap.put(RefCodeConst.evalCodeRefMap, eval_code);

		if (comment_code != null)
			codeMap.put(RefCodeConst.commentCodeRefMap, comment_code);

		return codeMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((board_code == null) ? 0 : board_code.hashCode());
		result = prime * result + ((codeMap == null) ? 0 : codeMap.hashCode());
		result = prime * result + ((eval_code == null) ? 0 : eval_code.hashCode());
		result = prime * result + ((hashtag_code == null) ? 0 : hashtag_code.hashCode());
		result = prime * result + ((site_code == null) ? 0 : site_code.hashCode());
		result = prime * result + ((user_board_code == null) ? 0 : user_board_code.hashCode());
		result = prime * result + ((user_tab_code == null) ? 0 : user_tab_code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetPrimaryKeyCodeCls other = (GetPrimaryKeyCodeCls) obj;
		if (board_code == null) {
			if (other.board_code != null)
				return false;
		} else if (!board_code.equals(other.board_code))
			return false;
		if (codeMap == null) {
			if (other.codeMap != null)
				return false;
		} else if (!codeMap.equals(other.codeMap))
			return false;
		if (eval_code == null) {
			if (other.eval_code != null)
				return false;
		} else if (!eval_code.equals(other.eval_code))
			return false;
		if (hashtag_code == null) {
			if (other.hashtag_code != null)
				return false;
		} else if (!hashtag_code.equals(other.hashtag_code))
			return false;
		if (site_code == null) {
			if (other.site_code != null)
				return false;
		} else if (!site_code.equals(other.site_code))
			return false;
		if (user_board_code == null) {
			if (other.user_board_code != null)
				return false;
		} else if (!user_board_code.equals(other.user_board_code))
			return false;
		if (user_tab_code == null) {
			if (other.user_tab_code != null)
				return false;
		} else if (!user_tab_code.equals(other.user_tab_code))
			return false;
		return true;
	}

}

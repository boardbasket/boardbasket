package com.nayuta.mosite.content.model.service.pageCommand;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.service.AbstractAdminCls;
import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("pcSelectIcon")
public class PcSelectIcon extends AbstractAdminCls implements CommandInterface {

	public static enum MEDIA_TYPE_ENUM {
		JPG("jpg", MediaType.IMAGE_JPEG), JPEG("jpeg", MediaType.IMAGE_JPEG), PNG("png",
				MediaType.IMAGE_PNG), GIF("gif", MediaType.IMAGE_GIF);

		private final String name;
		private final MediaType mType;

		@Override
		public String toString() {
			return name + " : " + mType;
		}

		private MEDIA_TYPE_ENUM(String name, MediaType mType) {
			this.name = name;
			this.mType = mType;
		}

		public String getName() {
			return name;
		}

		public static MediaType getmType(String type) {
			String temp = type.toLowerCase();
			for (MEDIA_TYPE_ENUM mType : MEDIA_TYPE_ENUM.values())
				if (mType.getName().equals(temp))
					return mType.mType;

			return null;
		}

	}

	@Value("#{fileConfig['fileConfig.path']}")
	private String filePath;

	@Override
	public void execute(ModelMap modelMap) {

		String fileLocation = (String) modelMap.get("fileLocation");
		fileLocation = fileLocation.replace("/", File.separator);

		String fileName = fileLocation.substring(fileLocation.lastIndexOf(File.separator) + 1);
		InputStream in = null;

		ResponseEntity<byte[]> entity = null;

		try {
			String formatType = fileName.substring(fileName.lastIndexOf(".") + 1);

			MediaType mType = MEDIA_TYPE_ENUM.getmType(formatType); // 파일 타입 확인
			HttpHeaders headers = new HttpHeaders();
			in = new FileInputStream(filePath + fileLocation);

			if (mType != null) {
				headers.add("Cache-Control", "private, max-age=31536000,must-revalidate"); // 이미지 캐싱 헤더 적용
				headers.setContentType(mType);// 이미지면 그냥 보냄
			} else {
				// 이미지 아니면 Content-disposition헤더를 attachmemt(다운로드) 설정으로 바꾸고 보냄
				fileName = fileName.substring(0, fileName.indexOf("."));
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
				headers.add("Content-Disposition",
						"attachment; fileName=\"" + new String(fileName.getBytes("UTF-8"), "ISO-8859-1") + "\"");
			}

			entity = new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					throw new MVCServiceException(e);
				}
		}
		modelMap.put("entity", entity);

	}
}

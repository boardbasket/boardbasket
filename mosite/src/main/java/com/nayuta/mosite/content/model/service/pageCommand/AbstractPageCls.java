package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.nayuta.mosite.common.util.CustomObjectMapper;
import com.nayuta.mosite.common.util.JCacheManagerUtil;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.common.util.SessionAttributeUtil;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.ArticleParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.member.model.vo.Member;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;
import net.sf.json.util.PropertyFilter;

public abstract class AbstractPageCls {
	@Autowired
	protected BoardDaoImpl bDao;
	@Autowired
	protected ArticleParserController parser;
	@Autowired
	protected BoardParserController bParser;

	@Autowired
	protected JCacheManagerUtil jCacheUtil;
	
	@Autowired
	protected WeekUtil weekUtil;

	@Autowired
	protected SessionAttributeUtil sau;

	protected Logger log = LogManager.getLogger();

	@Value("#{const['const.ajaxBoardLoadNum']}")
	protected String boardLoadNum; // 스크롤 내릴때 한번에 로드할 게시판
	@Value("#{const['const.searchResultLoadNum']}")
	protected String searchResultLoadNum; // 검색결과 표시할 갯수

	protected PageInfo pageInfo = null;// 페이지네이션 유틸
	// protected JSONObject resultObj = new JSONObject();// 결과 오브젝트

	protected JsonConfig hashtagConfig = new JsonConfig();
	protected CustomObjectMapper com = new CustomObjectMapper();

	/////////////////////////////////////////////////////////////////////////////////////////

	public AbstractPageCls() {
		super();

		hashtagConfig.registerJsonValueProcessor(java.util.Date.class, new JsonValueProcessor() {

			@Override
			public Object processObjectValue(String key, Object value, JsonConfig config) {

				return process(value, config);
			}

			@Override
			public Object processArrayValue(Object value, JsonConfig config) {

				return process(value, config);
			}

			private Object process(Object value, JsonConfig config) {

				if (value != null)
					return ((Date) value).getTime();
				else
					return null;
			}
		});

		hashtagConfig.setJsonPropertyFilter(new PropertyFilter() {
			@Override
			public boolean apply(Object source, String name, Object value) {
				return value == null;
			}
		});

	}

	public String getMemberId() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String memberId = null;

		if (obj instanceof UserDetails)
			memberId = ((UserDetails) obj).getUsername();
		else
			memberId = obj.toString();

		return memberId;
	}

	public String getMemberNo() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (obj instanceof UserDetails)
			return ((Member) obj).getMember_no();
		else
			return null;

	}

	/*
	 * public JSONObject getResultObj() { return resultObj; }
	 * 
	 * public void setResultObj(JSONObject resultObj) { this.resultObj = resultObj;
	 * }
	 */
	/*
	 * public void postProcess() { resultObj.clear(); }
	 */

}

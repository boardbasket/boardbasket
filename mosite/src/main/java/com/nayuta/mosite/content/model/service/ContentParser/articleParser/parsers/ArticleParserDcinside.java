package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;

@Component("articleParserDcinside")
public class ArticleParserDcinside extends ArticleParser {

	private final String categoryAddon;
	private final String ARTICLE_ROOT = "https://gall.dcinside.com";
	private Pattern replyNumRegex;
	private final String UPLOAD_DATE_PATTERN2;

	public ArticleParserDcinside(@Value("#{selectors['DCINSIDE_ROOT']}") String root,
			@Value("#{selectors['DCINSIDE_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeDcinside") BoardTypeAbstract typeList,
			@Value("#{selectors['DCINSIDE_PARSER_NAME']}") String parserName,
			@Value("#{selectors['DCINSIDE_CATEGORY_ADD_ON']}") String categoryAddon,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['DCINSIDE_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['DCINSIDE_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['DCINSIDE_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{selectors['DCINSIDE_REPLY_NUM_REGEX']}") String replyNumRegexStr,
			@Value("#{selectors['DCINSIDE_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{selectors['DCINSIDE_UPLOAD_DATE_REGEX2']}") String uploadDatePattern2,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);
		this.categoryAddon = categoryAddon;
		this.replyNumRegex = Pattern.compile(replyNumRegexStr);
		this.UPLOAD_DATE_PATTERN2 = uploadDatePattern2;
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		MultiValueMap<String, String> queryParams = UriComponentsBuilder.fromUriString(doc.baseUri()).build().getQueryParams();

		if (Integer.parseInt(queryParams.getFirst("page")) != asd.getOffset())
			return true;
		else
			return false;

	}

	public boolean checkIsResultEmpty(Elements elems) {

		return elems.select(".us-post").size() <= 0 ? true : false;
	}

	@Override
	public String getContentAddr(Element elem, String boardType) {

		String location = super.getContentAddr(elem, boardType);

		return ARTICLE_ROOT + location;
	}

	@Override
	public int getReplyNum(Element elem, String boardType) {
		if (replyNum.get(boardType) != null) {
			try {
				Element temp = elem.selectFirst(replyNum.get(boardType));

				String replyNumText = temp.text();

				Matcher m = replyNumRegex.matcher(replyNumText);
				while (m.find())
					return Integer.parseInt(m.group());

			} catch (Exception e) {
			}
		}

		return 0;
	}

	@Override
	public String makeCategoryQuery(String category) {

		try {
			Integer.parseInt(category);

			return category != null ? "&" + categoryAddon + category : "";
		} catch (Exception e) {
			return category != null ? "&" + category : "";
		}

	}

	@Override
	public LocalDate parseUploadDate(String uploadDate) {
		try {
			return super.parseUploadDate(uploadDate);
		} catch (Exception e) {
			try {

				DateTimeFormatterBuilder fomatterBuilder = new DateTimeFormatterBuilder();
				DateTimeFormatter dtf = fomatterBuilder.appendPattern(UPLOAD_DATE_PATTERN2).toFormatter();

				LocalDate ldt = LocalDate.from(dtf.parse(uploadDate));
				return ldt;

			} catch (Exception e2) {
				throw new SiteParseException("날짜값을 파싱하는데 오류가 생겼습니다.");
			}
		}

	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		Elements elements = elem.select(noticeCheck.get(boardType));
		for (Element e : elements)
			if (e.text().equals("공지"))
				return true;

		return false;
	}

	public String getCategoryAddon() {
		return categoryAddon;
	}

}

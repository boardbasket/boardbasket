package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;

@Component(value = "boardParserPpomppu")
public class BoardParserPpomppu extends BoardParserAbstract {

	private final String titleSelector;
	private final String categoryValueRegex;
	private final String checkRESTUrl;
	private final int POPULAR_PAGE_PARSE_NUM = 5;// 인기 게시판을 가져오기 위해서 몇개의 페이지를 파싱할 것인지

	public BoardParserPpomppu(@Value("#{selectors['PPOMPPU_ROOT']}") String root,
			@Value("#{selectors['PPOMPPU_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypePpomppu") BoardTypeAbstract typeList,
			@Value("#{selectors['PPOMPPU_PARSER_NAME']}") String parserName,
			@Value("#{selectors['PPOMPPU_TITLE_SELECTOR']}") String titleSelector,
			@Value("#{selectors['PPOMPPU_CATEGORY_VAL_REGEX']}") String categoryValueRegex,
			@Value("#{selectors['PPOMPPU_CHECK_REST_URL']}") String checkRESTUrl,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['PPOMPPU_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['PPOMPPU_POPULAR_ADDR']}") String popularAddr,
			@Value("#{selectors['PPOMPPU_REDIRECT_URL_REGEX']}") String redirectUrlRegex,
			@Value("#{selectors['PPOMPPU_CHECK_NO_CONTENT']}") String checkNoContent,
			@Value("#{selectors['PPOMPPU_NEW_BOARD']}") String nEW_BOARD,
			@Value("#{selectors['PPOMPPU_NEW_BOARD_ADDR']}") String nEW_BOARD_ADDR,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, checkNoContent, redirectUrlRegex, popularBoard, popularAddr,
				nEW_BOARD, nEW_BOARD_ADDR, sleepMin, sleepMax, retryNum);
		this.titleSelector = titleSelector;
		this.categoryValueRegex = categoryValueRegex;
		this.checkRESTUrl = checkRESTUrl;

	}

	@PostConstruct
	public void init() {
		initializeParser();

	}

	@Override
	public String urlPreProcess(String addr) {

		String address = httpToHttps(addr);

		Pattern checkMobile = Pattern.compile(checkMobileRegex);
		Matcher mobileMatcher = checkMobile.matcher(address);

		// 모바일 주소를 pc 주소로 바꾸는 작업
		if (mobileMatcher.find()) {

			address = address.replace("https://m.", "https://www.").replace("new/bbs_list", "zboard/zboard").replace("new/", "")
					.replace("_bbs", "");
		}

		// 주소에 host 가 없다면
		if (address.indexOf(root) < 0) {
			address = address.substring(0, 1).equals("/") ? address.substring(1) : address;
			address = root + address;

		}

		Pattern checkRESTTypeUrl = Pattern.compile(checkRESTUrl);

		Matcher m2 = checkRESTTypeUrl.matcher(address);

		while (m2.find()) {
			return m2.group();
		}

		Pattern pattern = Pattern.compile(urlFilterRegex);

		Matcher matcher = pattern.matcher(address);

		while (matcher.find()) {
			address = matcher.group(1) + matcher.group(2);
		}

		return address;

	}

	public String getIgnoreBoard() {
		return ignoreBoard;
	}

	public void setIgnoreBoard(String ignoreBoard) {
		this.ignoreBoard = ignoreBoard;
	}

	@Override
	public String getTitle(Document doc) {
		Element elem = doc.selectFirst(titleSelector);

		String title = elem.text();

		return title;

	}

	@Override
	public BoardCategory parseCategoryFunc(Element elem) {

		String cateName = elem.text();
		String cateVal = elem.attr("href");
		Pattern pattern = Pattern.compile(categoryValueRegex);

		Matcher matcher = pattern.matcher(cateVal);

		if (matcher.find()) {
			cateVal = matcher.group();
			return new BoardCategory(cateName, cateVal);
		} else
			return null;

	}

	@Override
	public List<String> getPopularBoardAddr() {

		String prefix = root.substring(0, root.length() - 1);// root 끝에 '/' 빼줌

		List<String> result = new ArrayList<>();

		for (int i = 1; i <= POPULAR_PAGE_PARSE_NUM; ++i) {

			try {
				Document doc = getDocument(popularAddr);

				Elements elems = doc.select(popularBoard);

				for (Element elem : elems) {
					String addr = prefix + elem.attr("href");
					if (!result.contains(addr))
						result.add(urlPreProcess(addr));
				}
			} catch (Exception e) {
				log.info(e.getMessage() + "인기 게시판 주소 가져오는 중 오류 발생 ");
			}

		}

		return result;
	}

}

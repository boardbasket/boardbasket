package com.nayuta.mosite.content.model.service.pageCommand;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardComment;

import net.sf.json.JSONObject;

@Service(value = "pcDeleteBoardComment")
@Transactional(rollbackFor = Exception.class)
public class PcDeleteBoardComment extends AbstractPageCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {

		BoardComment bc = (BoardComment) modelMap.get("boardComment");
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		bc.setMember_no(getMemberNo());

		int status = bDao.deleteBoardComment(bc);

		if (status > 0)
			bDao.deleteMembersBoardCommentEval(bc);
		else
			throw new MVCServiceException("comment를 삭제하는데 문제가 발생했습니다.");

		resultObj.put("status", status);

		modelMap.put("result", resultObj);
	}

}

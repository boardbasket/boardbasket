package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.CommentEvaluation;

import net.sf.json.JSONObject;


//게시판의 eval 값을 갯수와 함께 보내주는 클래스

@Service("pcSelectBoardEvalCount")
public class PcSelectBoardEvalCount extends AbstractPageCls implements CommandInterface {

	@Override

	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		String boardCode = (String) modelMap.get("board_code");

		List<CommentEvaluation> evalList = bDao.selectBoardEvalCount(boardCode);

		resultObj.put("evalList", evalList);

		modelMap.put("result", resultObj);

	}

}

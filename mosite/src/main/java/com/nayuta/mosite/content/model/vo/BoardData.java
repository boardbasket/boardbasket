package com.nayuta.mosite.content.model.vo;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

public class BoardData extends GetPrimaryKeyCodeCls {

	private static final long serialVersionUID = -8121358580658710070L;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate article_num_update_date;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate board_parse_update_date;

	@JsonIgnore
	private Long latest_article_no;// 최신 글번호

	private Long current_cycle;// 한 사이클 동안 쌓인 글수

	private Long one_cycle_ago;

	private Long two_cycle_ago;

	private Long three_cycle_ago;

	private Long four_cycle_ago;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate update_date_one_cycle_ago;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate update_date_two_cycle_ago;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate update_date_three_cycle_ago;

	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate update_date_four_cycle_ago;

	@JsonIgnore
	private int ignore_flag;

	private Integer regular_article_no_flag;

	@JsonIgnore
	private String parent_board_code;// 부모 게시판이 있는지(루리웹 전용)

	private Integer parsed_article_num;//태그 분석할때 파싱한 게시글 갯수

	private Integer parse_frequency_grade;

	public BoardData() {
		super();
	}

	public BoardData(String board_code, Long latest_article_no, Long current_cycle) {
		this.board_code = board_code;
		this.latest_article_no = latest_article_no;
		this.current_cycle = current_cycle;
	}

	public BoardData(String board_code, Long latest_article_no) {
		this.board_code = board_code;
		this.latest_article_no = latest_article_no;
	}

	public BoardData(String board_code, Long latest_article_no, Integer parsed_article_num) {
		this.board_code = board_code;
		this.latest_article_no = latest_article_no;
		this.parsed_article_num = parsed_article_num;
	}

	public Long getLatest_article_no() {
		return latest_article_no;
	}

	public Long getCurrent_cycle() {
		return current_cycle;
	}

	public Integer getParse_frequency_grade() {
		return parse_frequency_grade;
	}

	public void setLatest_article_no(Long latest_article_no) {
		this.latest_article_no = latest_article_no;
	}

	public void setCurrent_cycle(Long current_cycle) {
		this.current_cycle = current_cycle;
	}

	public void setParse_frequency_grade(Integer parse_frequency_grade) {
		this.parse_frequency_grade = parse_frequency_grade;
	}

	public String getParent_board_code() {
		return parent_board_code;
	}

	public void setParent_board_code(String parent_board_code) {
		this.parent_board_code = parent_board_code;
	}

	public int getIgnore_flag() {
		return ignore_flag;
	}

	public void setIgnore_flag(int ignore_flag) {
		this.ignore_flag = ignore_flag;
	}

	public Long getOne_cycle_ago() {
		return one_cycle_ago;
	}

	public Long getTwo_cycle_ago() {
		return two_cycle_ago;
	}

	public Long getThree_cycle_ago() {
		return three_cycle_ago;
	}

	public Long getFour_cycle_ago() {
		return four_cycle_ago;
	}

	public void setOne_cycle_ago(Long one_cycle_ago) {
		this.one_cycle_ago = one_cycle_ago;
	}

	public void setTwo_cycle_ago(Long two_cycle_ago) {
		this.two_cycle_ago = two_cycle_ago;
	}

	public void setThree_cycle_ago(Long three_cycle_ago) {
		this.three_cycle_ago = three_cycle_ago;
	}

	public void setFour_cycle_ago(Long four_cycle_ago) {
		this.four_cycle_ago = four_cycle_ago;
	}

	public LocalDate getArticle_num_update_date() {
		return article_num_update_date;
	}

	public LocalDate getBoard_parse_update_date() {
		return board_parse_update_date;
	}

	public void setArticle_num_update_date(LocalDate article_num_update_date) {
		this.article_num_update_date = article_num_update_date;
	}

	public void setBoard_parse_update_date(LocalDate board_parse_update_date) {
		this.board_parse_update_date = board_parse_update_date;
	}

	public Integer getRegular_article_no_flag() {
		return regular_article_no_flag;
	}

	public void setRegular_article_no_flag(Integer regular_article_no_flag) {
		this.regular_article_no_flag = regular_article_no_flag;
	}

	public LocalDate getUpdate_date_one_cycle_ago() {
		return update_date_one_cycle_ago;
	}

	public LocalDate getUpdate_date_two_cycle_ago() {
		return update_date_two_cycle_ago;
	}

	public LocalDate getUpdate_date_three_cycle_ago() {
		return update_date_three_cycle_ago;
	}

	public LocalDate getUpdate_date_four_cycle_ago() {
		return update_date_four_cycle_ago;
	}

	public void setUpdate_date_one_cycle_ago(LocalDate update_date_one_cycle_ago) {
		this.update_date_one_cycle_ago = update_date_one_cycle_ago;
	}

	public void setUpdate_date_two_cycle_ago(LocalDate update_date_two_cycle_ago) {
		this.update_date_two_cycle_ago = update_date_two_cycle_ago;
	}

	public void setUpdate_date_three_cycle_ago(LocalDate update_date_three_cycle_ago) {
		this.update_date_three_cycle_ago = update_date_three_cycle_ago;
	}

	public void setUpdate_date_four_cycle_ago(LocalDate update_date_four_cycle_ago) {
		this.update_date_four_cycle_ago = update_date_four_cycle_ago;
	}

	public Integer getParsed_article_num() {
		return parsed_article_num;
	}

	public void setParsed_article_num(Integer parsed_article_num) {
		this.parsed_article_num = parsed_article_num;
	}

	@Override
	public String toString() {
		return "BoardData [article_num_update_date=" + article_num_update_date + ", board_parse_update_date="
				+ board_parse_update_date + ", latest_article_no=" + latest_article_no + ", current_cycle=" + current_cycle
				+ ", one_cycle_ago=" + one_cycle_ago + ", two_cycle_ago=" + two_cycle_ago + ", three_cycle_ago=" + three_cycle_ago
				+ ", four_cycle_ago=" + four_cycle_ago + ", update_date_one_cycle_ago=" + update_date_one_cycle_ago
				+ ", update_date_two_cycle_ago=" + update_date_two_cycle_ago + ", update_date_three_cycle_ago="
				+ update_date_three_cycle_ago + ", update_date_four_cycle_ago=" + update_date_four_cycle_ago + ", ignore_flag="
				+ ignore_flag + ", regular_article_no_flag=" + regular_article_no_flag + ", parent_board_code="
				+ parent_board_code + ", parsed_article_num=" + parsed_article_num + ", parse_frequency_grade="
				+ parse_frequency_grade + "]";
	}

}

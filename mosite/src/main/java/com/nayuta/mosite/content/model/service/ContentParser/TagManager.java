package com.nayuta.mosite.content.model.service.ContentParser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.util.KoreanTokenizerUtil;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.ArticleParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserAbstract;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;
import com.vdurmont.emoji.EmojiParser;

@Component
public class TagManager {

	@Autowired
	private ArticleParserController articleParserController;

	@Autowired
	private BoardParserController boardParserController;

	@Autowired
	private KoreanTokenizerUtil koreanTokenizerUtil;

	@Autowired
	private BoardDaoImpl bDao;

	private final int PAGE_THRESHOLD;// 태그 계산 최소 페이지 (페이지 갯수가 이 아래면 태그를 파싱하지 않음)

	private final int INSERT_ARTICLE_NUM; // 파싱하고 필터링해서 저장할 article의 갯수

	private final int INSERT_ARTICLE_MIN_RECOMM;// 게시글을 저장할 기준이 될 최소 추천 갯수

	public TagManager(@Value("#{new Integer(serverConst['server_const.page_threshold'])}") int pAGE_THRESHOLD,
			@Value("#{new Integer(serverConst['server_const.insert_article_num'])}") int insertArticleNum,
			@Value("#{new Integer(serverConst['server_const.insert_article_min_recomm'])}") int iNSERT_ARTICLE_MIN_RECOMM) {
		this.PAGE_THRESHOLD = pAGE_THRESHOLD;
		this.INSERT_ARTICLE_NUM = insertArticleNum;
		this.INSERT_ARTICLE_MIN_RECOMM = iNSERT_ARTICLE_MIN_RECOMM;
	}

	public List<WordFrequencyData> parseWordFrequency(ContentVO board) {

		List<ContentVO> boardList = new ArrayList<>();
		boardList.add(board);
		return processWordFrequncy(boardList);

	}

	public List<WordFrequencyData> parseWordFrequency(List<ContentVO> boardList) {
		return processWordFrequncy(boardList);
	}

	public List<WordFrequencyData> parseWordFrequency() {
		List<WordFrequencyData> result = new ArrayList<>();
		List<BoardParserAbstract> parserList = boardParserController.getParserList();
		for (BoardParserAbstract bpa : parserList)
			result.addAll(parseWordFrequency(bpa));
		return result;
	}

	/*
	 * public List<WordFrequencyData> parseWordFrequency(List<Site> siteList) {
	 * 
	 * List<WordFrequencyData> result = new ArrayList<>(); siteList.forEach(site ->
	 * { BoardParserAbstract bpa =
	 * boardParserController.getParser(site.getSite_address());
	 * result.addAll(parseWordFrequency(bpa));
	 * 
	 * }); return result; }
	 */

	public List<WordFrequencyData> parseWordFrequency(BoardParserAbstract bpa) {
		// List<WordFrequencyData> result = new ArrayList<>();
		List<String> boardAddrList = bpa.getPopularBoardAddr();// 인기있는 게시판 주소 가져오기
		List<ContentVO> boardList = new ArrayList<>();
		String siteCode = null;
		for (String addr : boardAddrList) {

			ContentVO board = bDao.selectIsBoardExist(addr);// 주소가 db에 존재하면 관련정보 가져오고 없으면 INSERT

			if (board == null) {

				board = bpa.parseBoard(addr);
				if (board == null)
					continue;

				bDao.insertBoard(board);
				board = bDao.selectBoard(board.getBoard_address());
			}
			if (siteCode == null)
				siteCode = board.getSite_code();// 게시판중에서 첫 하나의 사이트 코드를 가져옴(contentTarget을 가져오기위해)

			boardList.add(board);
		}

		return processWordFrequncy(boardList);

	}

	private List<WordFrequencyData> processWordFrequncy(List<ContentVO> boardList) {

		List<WordFrequencyData> frequencyDataList = articleParserController.parseWordFrequency(boardList);

		frequencyDataList.forEach(wfd -> {

			ContentVO board = wfd.getBoard();
			List<BoardArticle> articleList = wfd.getArticleList();

			// 실제 파싱된 페이지 구함
			int parsedPage = (int) Math.round(articleList.size() / (double) wfd.getArticleParser().getARTICLE_NUM_PER_PAGE());

			// 일정 페이지 이상 파싱한 경우에만 과정을 진행함
			if (parsedPage >= PAGE_THRESHOLD) {
				// article을 이용해서 토큰을 저장
				List<Hashtag> hashtagList = koreanTokenizerUtil.tokenizeArticle(board, articleList,
						wfd.getArticleParser().getARTICLE_NUM_PER_PAGE(), parsedPage);

				wfd.setHashtagList(hashtagList);

				// 1.태그들을 포함하는 글들만 필터링 해서 가져옴
				// 2.그중에 추천,댓글 순으로 내림차순 정렬해서 상위의 일부 글만 가져옴
				wfd.setArticleList(articleList.stream().map(k -> {
					k.setTitle(EmojiParser.removeAllEmojis(k.getTitle()));
					return k;
				}).filter(article -> {
					for (int i = 0; i < hashtagList.size(); ++i) {
						if (article.getTitle().indexOf(hashtagList.get(i).getTag_name()) >= 0)
							return true;
					}
					return false;
				}).filter(k -> k.getRecommend() >= INSERT_ARTICLE_MIN_RECOMM).sorted((p1, p2) -> {
					int cmp = Integer.compare(p2.getRecommend(), p1.getRecommend());
					if (cmp == 0)
						cmp = Integer.compare(p2.getReplyNum(), p1.getReplyNum());

					return cmp;
				}).limit(INSERT_ARTICLE_NUM).collect(Collectors.toList()));
			} else {
				// 태그가 파싱되지 않으면 article도 집어넣지 않음
				wfd.setArticleList(new ArrayList<BoardArticle>());
			}

		});

		return frequencyDataList;
	}

	public void updateWordFrequencyData(WordFrequencyData wfd) {

		if (wfd.getHashtagList() != null && wfd.getHashtagList().size() > 0)
			bDao.insertHashtag(wfd.getHashtagList());
		if (wfd.getBoardData() != null) {
			BoardData bd = wfd.getBoardData();
			bDao.updateBoardParseHistory(bd);
			if (bd.getParsed_article_num() != null)
				bDao.insertArticleParsedNumHistory(bd);
		}
		if (wfd.getArticleList() != null && wfd.getArticleList().size() > 0)
			bDao.insertCommunityContent(wfd.getBoard(), wfd.getArticleList());

	}

	public void updateWordFrequencyData(List<WordFrequencyData> list) {
		for (WordFrequencyData wfd : list)
			updateWordFrequencyData(wfd);

	}

	public ArticleParserController getArticleParserController() {
		return articleParserController;
	}

	public BoardParserController getBoardParserController() {
		return boardParserController;
	}

	public void setArticleParserController(ArticleParserController articleParserController) {
		this.articleParserController = articleParserController;
	}

	public void setBoardParserController(BoardParserController boardParserController) {
		this.boardParserController = boardParserController;
	}

}

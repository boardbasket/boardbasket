package com.nayuta.mosite.content.model.vo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.vo.validatorGroup.SearchDataTimeOptionLimit;

//게시판 검색을 위한 VO
@SearchDataTimeOptionLimit
public class SearchData extends GetPrimaryKeyCodeCls {

	private static final long serialVersionUID = 1L;
	private String board_name;
	private String search_target;
	private List<String> site_code_list;
	private String order;
	private List<String> searchTags;
	private List<String> searchWords;
	private List<String> keyword;//
	private Integer limit;// 태그를 몇개 가져올 것인지
	private List<TimeSet> weekList;// 태그가져올때 시간 범위들
	private String timeMode;// 검색 시간 단위가 월인지 주인지
	private int offset = 1; // pagination 할때 limit 의 첫번째 변수

	private String type;

	public SearchData() {
		super();
	}

	@Pattern(regexp = "year|YEAR|month|MONTH|week|WEEK|day|DAY")
	public String getTimeMode() {
		return timeMode;
	}

	public void setTimeMode(String timeMode) {
		this.timeMode = timeMode;
	}

	public List<String> getKeyword() {
		return keyword;
	}

	public void setKeyword(List<String> keyword) {
		this.keyword = keyword;
	}

	public SearchData(String board_name, List<String> site_code_list, String order, List<String> searchTags,
			List<String> searchWords) {
		super();
		this.board_name = board_name;
		this.site_code_list = site_code_list;
		this.order = order;
		this.searchTags = searchTags;
		this.searchWords = searchWords;

	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public List<String> getSearchTags() {
		return searchTags;
	}

	public void setSearchTags(List<String> searchTags) {
		this.searchTags = searchTags;
	}

	public List<String> getSearchWords() {
		return searchWords;
	}

	public void setSearchWords(List<String> searchWords) {
		this.searchWords = searchWords;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public List<String> getSite_code_list() {
		return site_code_list;
	}

	public void setSite_code_list(List<String> site_code_list) {
		this.site_code_list = site_code_list;
	}

	public List<TimeSet> getWeekList() {
		return weekList;
	}

	public void setWeekList(List<TimeSet> weekList) {
		this.weekList = weekList;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Min(value = 0)
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getSearch_target() {
		return search_target;
	}

	public void setSearch_target(String search_target) {
		this.search_target = search_target;
	}

	public Map<String, Object> makeParamMap(WeekUtil weekUtil) {

		Map<String, Object> options = new HashMap<>();

		options.put("site_code", site_code);
		options.put("board_code", board_code);
		options.put("board_name", board_name);
		options.put("site_code_list", site_code_list);
		options.put("offset", offset);
		options.put("order", order);
		options.put("tags", searchTags);
		options.put("words", searchWords);
		options.put("timeMode", timeMode);
		options.put("limit", limit);
		options.put("type", type);
		options.put("timeSet", weekUtil.parseDateBaseToTimeMode(weekList, timeMode));
		options.put("eval_code", eval_code);
		if (keyword != null && !keyword.isEmpty())
			options.put("keyword", keyword);

		return options;

	}

	@SuppressWarnings("unchecked")
	@Override
	public void pushPrimaryKey(Map<String, Object> codeMap) {
		super.pushPrimaryKey(codeMap);
		if (codeMap.containsKey("site_code_list"))
			site_code_list = (List<String>) codeMap.get("site_code_list");
	}

	@Override
	public Map<String, Object> pullPrimaryKey() {
		super.pullPrimaryKey();
		codeMap.put("site_code_list", site_code_list);

		return codeMap;
	}

	@Pattern(regexp = "news|community")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

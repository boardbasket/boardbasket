package com.nayuta.mosite.content.model.service.pageCommand;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardComment;

import net.sf.json.JSONObject;

@Service("pcInsertBoardCommentVote")
@Transactional(rollbackFor = Exception.class)
public class PcInsertBoardCommentVote extends AbstractPageCls implements CommandInterface {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		BoardComment bc = (BoardComment) modelMap.get("boardComment");
		bc.setMember_no(getMemberNo());

		String target = bc.getComment_val() > 0 ? "up_vote" : "down_vote";
		String anotherTarget = bc.getComment_val() < 0 ? "up_vote" : "down_vote";

		BoardComment myComment = bDao.selectBoardCommentOnlyWithCode(bc);

		if (myComment != null && myComment.getMember_no().equals(getMemberNo()))
			throw new MVCServiceException("자신의 댓글엔 추천/비추천을 할 수 없습니다.");

		BoardComment exist = bDao.selectCheckBoardCommentVoteExist(bc);
		if (exist != null) {

			if (exist.getComment_val() == bc.getComment_val()) // 값이 같음=>delete
			{
				bDao.deleteBoardCommentVote(bc);
				log.info("유저 " + bc.getMember_no() + "게시판 " + bc.getBoard_code() + " 댓글 평가 삭제");
				resultObj.put(target, -1);
				resultObj.put(anotherTarget, 0);
			} else {
				// 값이 다름 => update
				bDao.upsertBoardCommentVote(bc);
				log.info("유저 " + bc.getMember_no() + "게시판 " + bc.getBoard_code() + " 댓글 평가 업데이트");
				resultObj.put(target, 1);
				resultObj.put(anotherTarget, -1);

			}

		} else {
			// 값이 없음 => insert
			bDao.upsertBoardCommentVote(bc);
			resultObj.put(target, 1);
			resultObj.put(anotherTarget, 0);
		}

		modelMap.put("result", resultObj);

	}
}

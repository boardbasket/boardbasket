package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import org.springframework.stereotype.Component;

@Component("boardTypeFmkorea")
public class BoardTypeFmkorea extends BoardTypeAbstract {
	public final String TYPE_BEST = "TYPE_BEST";

	public BoardTypeFmkorea() {
		typeList.add(TYPE_BEST);
	}

}
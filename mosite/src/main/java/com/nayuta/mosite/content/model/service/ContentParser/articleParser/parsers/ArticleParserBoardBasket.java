package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.util.List;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Component("articleParserBoardBasket")
public class ArticleParserBoardBasket extends ArticleParser {

	private final String boardbasketReadConentPrefix;

	@Autowired
	BoardDaoImpl bDao;

	@Value("#{const['const.dbArticleLoadNum']}")
	protected String dbArticleLoadNum;

	public ArticleParserBoardBasket(@Value("#{selectors['BOARDBASKET_ROOT']}") String root,
			@Value("#{selectors['BOARDBASKET_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeBoardBasket") BoardTypeAbstract typeList,
			@Value("#{selectors['BOARDBASKET_PARSER_NAME']}") String parserName,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['BOARDBASKET_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['BOARDBASKET_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{selectors['BOARDBASKET_READ_CONENT_PREFIX']}") String boardbasketReadConentPrefix,
			@Value("#{new Integer(selectors['BOARDBASKET_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, null, sleepMin, sleepMax, retryNum, uploadDatePattern);
		this.boardbasketReadConentPrefix = boardbasketReadConentPrefix;
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public List<BoardArticle> processParsing(ArticleSearchData asd) {

		return defaultParseFunc(null, asd);
	}

	@Override
	public List<BoardArticle> defaultParseFunc(Document doc, ArticleSearchData asd) {

		List<BoardArticle> baList = bDao.selectDbBoardArticle(asd,
				new PageInfo(asd.getOffset(), Integer.parseInt(dbArticleLoadNum)));

		baList.stream().forEach(k -> {
			k.setContent_address(boardbasketReadConentPrefix + k.getArticle_no());
		});

		return baList;

	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {
		return false;
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {
		return false;
	}

}

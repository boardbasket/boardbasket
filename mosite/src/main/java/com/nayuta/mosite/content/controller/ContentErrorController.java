package com.nayuta.mosite.content.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nayuta.mosite.common.util.JCacheManagerUtil;

@Controller
public class ContentErrorController {

	@Autowired
	JCacheManagerUtil cMgr;

	@RequestMapping(value = "/errors/error400", method = RequestMethod.GET)
	public String error400(RedirectAttributes flash) {

		flash.addFlashAttribute("msg", "잘못된 요청입니다.");
		return "errors/error400";
	}

	@RequestMapping(value = "/errors/error500", method = RequestMethod.GET)
	public String error500(RedirectAttributes flash) {

		//cMgr.clearAllCaches();

		flash.addFlashAttribute("msg", "서버 오류입니다");
		return "errors/error500";
	}

	@RequestMapping(value = "/errors/unknownError", method = RequestMethod.GET)
	public String unknownError(RedirectAttributes flash) {

		flash.addFlashAttribute("msg", "알수 없는 오류입니다.");
		return "errors/unknownError";
	}

}

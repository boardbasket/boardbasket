package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.ParserCommon;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;
import com.nayuta.mosite.content.model.vo.ContentVO;

public abstract class BoardParserAbstract extends ParserCommon {

	/* 게시판 셀렉터 */
	protected String board;
	protected String category;

	/* 사이트 정보 */
	protected String favicon;// favicon 셀렉터 없으면 주소에 /favicon.ico 붙여서 가져옴
	protected String urlFilterRegex;// 입력된 주소에서 필요한 정보만 거르기 위한 정규표현식
	protected String titleRegex; // 타이틀(게시판 제목) 태그를 양식 맞추어 가져올때 필요한 regexp
	protected String ignoreBoard;// 이 엘리먼트가 있으면 게시판 찾아도 무시함
	protected String checkMobileRegex;// 모바일인지 확인하는 정규표현식
	protected final String popularBoard;// 인기있는 게시판 가져오는 셀렉터
	protected final String popularAddr;// 인기있는 게시판을 가져올 페이지
	protected final String newBoard; // 새로운 게시판 가져올 셀렉터
	protected final String newBoardAddr;// 새로운 게시판 가져올 페이지

	// property 파일에 선언된 키의 이름들
	protected static final String ROOT = "ROOT";
	protected static final String BOARD = "BOARD";
	protected static final String CATEGORY = "CATEGORY";
	protected static final String FAVICON = "FAVICON";
	protected static final String URL_FILTER_REGEX = "URL_FILTER_REGEX";
	protected static final String TITLE_REGEX = "TITLE_REGEX";
	protected static final String IGNORE = "IGNORE";
	protected static final String MOBILE_SITE_REGEX = "MOBILE_SITE_REGEX";

	protected Logger log = LogManager.getLogger(BoardParserAbstract.class);

	public BoardParserAbstract(String root, String siteAddrRegex, BoardTypeAbstract typeList, String parserName, String timeOut,
			String checkNoContent, String redirectUrlRegex, String popularBoard, String popularAddr, String newBoard,
			String newBoardAddr, int sleepMin, int sleepMax, int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, checkNoContent, redirectUrlRegex, sleepMin, sleepMax, retryNum);
		this.popularAddr = popularAddr;
		this.popularBoard = popularBoard;
		this.newBoard = newBoard;
		this.newBoardAddr = newBoardAddr;

	}

	abstract public void init();

	public List<String> getPopularBoardAddr() {

		List<String> result = new ArrayList<>();

		try {
			Document doc = getDocument(popularAddr);

			Elements elems = doc.select(popularBoard);

			for (Element elem : elems) {
				result.add(urlPreProcess(elem.attr("href")));
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;

	}

	// 자식 클래스 init함수 (@PostConstruct 어노테이션 추가)에서 호출해 줄것
	public void initializeParser() {

		String _parserName = parserName + "_";

		for (String type : typeList.getTypeList())
			boardTypeCheck.put(type, selectors.getProperty(_parserName + type + "_SELECTOR"));

		// root = selectors.getProperty(_parserName + ROOT);
		board = selectors.getProperty(_parserName + BOARD);
		category = selectors.getProperty(_parserName + CATEGORY);
		favicon = selectors.getProperty(_parserName + FAVICON);
		urlFilterRegex = selectors.getProperty(_parserName + URL_FILTER_REGEX);
		titleRegex = selectors.getProperty(_parserName + TITLE_REGEX);
		// siteAddrRegex = selectors.getProperty(_parserName + SITE_ADDR_REGEX);
		ignoreBoard = selectors.getProperty(_parserName + IGNORE);
		checkMobileRegex = selectors.getProperty(_parserName + MOBILE_SITE_REGEX);
	}

	public ContentVO parseBoard(String addr) {

		try {
			String address = urlPreProcess(addr);

			if (address == null) {
				throw new SiteParseException("지원하지 않는 형식의 주소입니다.");
			}

			Document doc = getDocument(address);

			if (detectBoard(doc)) {
				return parseBoardFunc(doc);
			} else {
				log.error(doc.baseUri() + "주소에서 게시판이 발견되지 않았습니다.");
				return null;
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();

			return null;
		}

	}

	public String checkIsValidLink(String addr) {

		if (addr.isEmpty() || isExternalLink(addr))
			return addr;
		else
			return null;
	}

	public ContentVO parseBoardFunc(Document doc) {

		ContentVO cvo = new ContentVO(root, getTitle(doc), doc.baseUri(), "Y", detectBoardType(doc), parseCategory(doc));

		log.debug("게시판 : " + cvo.getBoard_name() + " 파싱 완료");
		return cvo;

	}

	// element에서 a tag의 href 값을 가져오는 함수
	public Set<String> parseATagLinks(Element elem) {

		Set<String> links = new HashSet<>();

		for (Element e : elem.select("a"))
			links.add(e.attr("href"));

		return links;

	}

	public String parseFavicon(Document doc) {

		if (this.favicon == null || this.favicon.isEmpty()) {
			return root + "favicon.ico";
		} else
			return doc.selectFirst(this.favicon).attr("href");

	}

	// document를 가지고 오기 전 url 을 전처리 하는 함수(쿼리스트링 제거 등)
	abstract public String urlPreProcess(String addr) throws SiteParseException;

	// 사이트 주소를 가지고 자신(파서) 이 파싱해야 하는지 boolean으로 리턴
	public boolean checkParser(String url) {
		Pattern ptn = Pattern.compile(siteAddrRegex);

		Matcher matcher = ptn.matcher(url);

		if (matcher.find())
			return true;
		else
			return false;
	}

	// 다른 사이트의 링크인지 확인
	protected boolean isExternalLink(String address) {
		return address.indexOf(root) >= 0;
	}

	// 조건에 맞으면 게시판을 무시함 (override해서 사용)
	protected boolean ignoreBoardTest(Document doc) {
		return false;
	}

	// 게시판의 분류(말머리) 파싱
	public List<BoardCategory> parseCategory(Document doc) {

		List<BoardCategory> result = new ArrayList<BoardCategory>();

		Elements elems = doc.select(category);

		for (Element elem : elems) {
			BoardCategory bc = parseCategoryFunc(elem);

			if (bc != null) {
				if (bc.getCategory_name() != null)// db에 특문 안들어가서 특문 제거
					bc.setCategory_name(bc.getCategory_name().replaceAll("[^ 가-힣ㄱ-ㅎa-zA-Z0-9]", ""));

				result.add(bc);
			}

		}

		return result;

	}

	// 카테고리 값을 리턴하는 함수
	protected BoardCategory parseCategoryFunc(Element elem) {
		String cateName = elem.text();
		String cateVal = elem.val().isEmpty() ? elem.attr("href") : elem.val();
		return new BoardCategory(cateName, cateVal.substring(cateVal.lastIndexOf("?") + 1));

	}

	// 게시판의 제목(title태그) 가져오기
	public String getTitle(Document doc) {
		Element elem = doc.selectFirst("title");

		String title = elem.text();
		Pattern pattern = Pattern.compile(titleRegex);
		Matcher matcher = pattern.matcher(title);

		if (matcher.find())
			return matcher.group();
		else
			return title;
	}

	public List<String> getNewBoardAddrList() {

		List<String> result = new ArrayList<>();

		if (StringUtils.isEmpty(newBoard) || StringUtils.isEmpty(newBoardAddr))
			return result;
		else {

			try {
				Document doc = getDocument(newBoardAddr);
				Elements elems = doc.select(newBoard);

				for (Element elem : elems) {
					String address = urlPreProcess(elem.attr("href"));
					if (address != null)
						result.add(address);
				}
			} catch (Exception e) {
			}

		}
		return result;

	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String getCategory() {
		return category;
	}

	public Map<String, String> getBoardTypeCheck() {
		return boardTypeCheck;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setBoardTypeCheck(Map<String, String> boardTypeCheck) {
		this.boardTypeCheck = boardTypeCheck;
	}

	public String getUrlFilterRegex() {
		return urlFilterRegex;
	}

	public String getSiteAddrRegex() {
		return siteAddrRegex;
	}

	public void setUrlFilterRegex(String urlFilterRegex) {
		this.urlFilterRegex = urlFilterRegex;
	}

	public String getTitleRegex() {
		return titleRegex;
	}

	public void setTitleRegex(String titleRegex) {
		this.titleRegex = titleRegex;
	}

	public String getRoot() {
		return root;
	}

	public String getFavicon() {
		return favicon;
	}

	public void setFavicon(String favicon) {
		this.favicon = favicon;
	}

	public Properties getSelectors() {
		return selectors;
	}

	public void setSelectors(Properties selectors) {
		this.selectors = selectors;
	}

	public String getNewBoard() {
		return newBoard;
	}

	public String getNewBoardAddr() {
		return newBoardAddr;
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;

import net.sf.json.JSONObject;

//유저가 직접 hashtag를 넣음
@Service("pcInsertUserHashtag")
public class PcInsertUserHashtag extends AbstractPageCls implements CommandInterface {

	private final int INSERT_TAG_LIMIT_PER_DAY;

	public PcInsertUserHashtag(
			@Value("#{new Integer(serverConst['server_const.insert_tag_limit_per_day'])}") int iNSERT_TAG_LIMIT_PER_DAY) {
		super();
		INSERT_TAG_LIMIT_PER_DAY = iNSERT_TAG_LIMIT_PER_DAY;
	}

	@Override
	public void execute(ModelMap modelMap) {

		JSONObject result = new JSONObject();
		modelMap.put("insert_date", LocalDate.now());
		modelMap.put("memberNo", getMemberNo());

		// 1.하루에 3개인데 넘었는지 확인
		List<Hashtag> hashtagList = bDao.selectUserHashtag(modelMap);

		if (hashtagList.stream().filter(
				k -> k.getTag_name().equals(modelMap.get("tag_name")) && k.getBoard_code().equals(modelMap.get("board_code")))
				.count() > 0)
			throw new MVCServiceException("오늘 이미 같은 태그를 추가했습니다.");
		else if (hashtagList.size() >= INSERT_TAG_LIMIT_PER_DAY)
			throw new MVCServiceException("오늘 더이상 태그를 추가할 수 없습니다.");

		result.put("status", bDao.insertUserBoardHashtag(modelMap));
		log.info("유저 " + getMemberNo() + modelMap.get("hashtag_code") + " 태그 추가");
		
		modelMap.put("result", result);

	}

}

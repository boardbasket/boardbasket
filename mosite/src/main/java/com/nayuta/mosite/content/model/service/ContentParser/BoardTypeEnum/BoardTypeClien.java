package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import org.springframework.stereotype.Component;

@Component("boardTypeClien")
public class BoardTypeClien extends BoardTypeAbstract {
	public final String TYPE_GALLERY = "TYPE_GALLERY";
	public final String TYPE_THUMBNAIL = "TYPE_THUMBNAIL";

	public BoardTypeClien() {
		typeList.add(TYPE_GALLERY);
		typeList.add(TYPE_THUMBNAIL);
	}
}
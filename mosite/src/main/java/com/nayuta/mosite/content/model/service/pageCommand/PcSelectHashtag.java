package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.GroupingTagByTimeSetList;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

//ajax로 hashtag에 대한 데이터를 가져올때 

@Service(value = "pcSelectHashtag")
public class PcSelectHashtag extends AbstractPageCls implements CommandInterface {

	private final int SELECT_HASHTAG_LIMIT; // frequency 합 상위 몇개를 가져올 것인지의 값

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	public PcSelectHashtag(@Value("#{new Integer(serverConst['server_const.select_hashtag_limit'])}") int sELECT_HASHTAG_LIMIT) {
		super();
		SELECT_HASHTAG_LIMIT = sELECT_HASHTAG_LIMIT;
	}

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		modelMap.addAttribute("result", selectHashtagFunc(searchData, true));
	}

	public JSONObject selectHashtagFunc(SearchData searchData, boolean grouping) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트

		if (searchData != null) {

			if (StringUtils.isEmpty(searchData.getBoard_code()) && StringUtils.isEmpty(searchData.getSite_code())
					&& searchData.getKeyword().size() <= 0)
				return resultObj;

			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			options.put("keyword", searchData.getKeyword());

			options.put("limit", SELECT_HASHTAG_LIMIT);

			final List<Hashtag> hashtagList = bDao.selectHashtag(options);
			final List<Hashtag> articleNum = bDao.selectArticleParsedNumHistory(options);

			if (grouping) {
				try {
					Map<String, Map<String, Map<String, List<Hashtag>>>> result = new HashMap<>();
					Map<String, Map<String, Map<String, List<Hashtag>>>> articleNumResult = new HashMap<>();
					result = GroupingTagByTimeSetList.groupByTimeSetList(
							weekUtil.parseDateBaseToTimeMode(searchData.getWeekList(), searchData.getTimeMode()), hashtagList,
							searchData.getTimeMode());

					articleNumResult = GroupingTagByTimeSetList.groupByTimeSetList(
							weekUtil.parseDateBaseToTimeMode(searchData.getWeekList(), searchData.getTimeMode()), articleNum,
							searchData.getTimeMode());

					resultObj.put("timeMode", searchData.getTimeMode());
					resultObj.put("hashtagList", com.writeValueAsString(result));
					resultObj.put("articleNumList", com.writeValueAsString(articleNumResult));

				} catch (JsonProcessingException e) {

					e.printStackTrace();
				}
			} else {
				try {
					resultObj.put("searchedTag", com.writeValueAsString(hashtagList));
					resultObj.put("articleNum", com.writeValueAsString(articleNum));
					resultObj.put("timeMode", searchData.getTimeMode());
				} catch (JsonProcessingException e) {
					return resultObj;
				}
			}

			return resultObj;

		} else
			return resultObj;

	}

}

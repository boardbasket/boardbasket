package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service("pcSelectHashtagHeatMap")
public class PcSelectHashtagHeatMap extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		modelMap.addAttribute("result", selectHashtagForHeatMapFunc(searchData, null));

	}

	// limit: 검색할 태그의 이름 갯수를 제한함
	public JSONObject selectHashtagForHeatMapFunc(SearchData searchData, Integer limit) {
		if (searchData != null) {

			JSONObject result = new JSONObject();
			Map<String, Object> options = searchData.makeParamMap(weekUtil);
			List<String> keyword = null;

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return result;
			else {
				keyword = searchTextToKeyword.toKeyword(searchData.getBoard_name());
				if (limit != null)
					keyword = searchData.getKeyword().subList(0, limit);
			}

			options.put("keyword", keyword);

			List<Hashtag> hashtagList = bDao.selectHashtagForHeatMap(options);

			try {
				result.put("heatMapTag", com.writeValueAsString(hashtagList));
			} catch (JsonProcessingException e) {
			}

			return result;

		} else
			return null;
	}

}

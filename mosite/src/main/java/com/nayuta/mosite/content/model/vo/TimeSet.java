package com.nayuta.mosite.content.model.vo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeSet {
	@JsonIgnore
	private Integer fromTime;
	@JsonIgnore
	private Integer toTime;
	@JsonIgnore
	private LocalDate fromDate;
	@JsonIgnore
	private LocalDate toDate;
	@JsonProperty("w")
	private Integer week;
	@JsonProperty("m")
	private Integer month;
	@JsonProperty("y")
	private Integer year;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date day;

	public TimeSet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TimeSet(TimeSet ts) {
		this.day = ts.getDay();
		this.year = ts.getYear();
		this.month = ts.getMonth();
		this.week = ts.getWeek();
	}

	public TimeSet(LocalDate day) {

		this.day = java.util.Date.from(day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public TimeSet(Integer year, Integer month, Integer week, LocalDate day) {
		this.year = year;
		this.month = month;
		this.week = week;
		this.day = java.util.Date.from(day.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public TimeSet(Integer year, Integer month, Integer week) {
		this.year = year;
		this.month = month;
		this.week = week;
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

	public Integer getFromTime() {
		return fromTime;
	}

	public Integer getToTime() {
		return toTime;
	}

	public Integer getWeek() {
		return week;
	}

	public Integer getMonth() {
		return month;
	}

	public void setFromTime(Integer fromTime) {
		this.fromTime = fromTime;
	}

	public void setToTime(Integer toTime) {
		this.toTime = toTime;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((fromTime == null) ? 0 : fromTime.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + ((toTime == null) ? 0 : toTime.hashCode());
		result = prime * result + ((week == null) ? 0 : week.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
	
		TimeSet other = (TimeSet) obj;
		
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
	
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (week == null) {
			if (other.week != null)
				return false;
		} else if (!week.equals(other.week))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TimeSet [fromTime=" + fromTime + ", toTime=" + toTime + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", week=" + week + ", month=" + month + ", year=" + year + ", day=" + day + "]";
	}

}

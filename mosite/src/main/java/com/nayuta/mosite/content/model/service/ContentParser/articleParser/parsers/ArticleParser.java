package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.common.util.FilteringBadWord;
import com.nayuta.mosite.content.model.service.ContentParser.ParserCommon;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;
import com.vdurmont.emoji.EmojiParser;

public abstract class ArticleParser extends ParserCommon {

	@Autowired
	FilteringBadWord bwFilter;

	protected final static String ARTICLES = "ARTICLES";
	protected final static String TITLE = "TITLE";
	protected final static String CONTENT_ADDR = "CONTENT_ADDR";
	protected final static String VIEW_CNT = "VIEW_CNT";
	protected final static String UPLOAD_DATE = "UPLOAD_DATE";
	protected final static String NOTICE_CHECK = "NOTICE_CHECK";
	protected final static String REPLY_NUM = "REPLY_NUM";
	protected final static String RECOMMEND = "RECOMMEND";
	protected final static String WRITER = "WRITER";
	protected final static String ARTICLE_NO = "ARTICLE_NO";
	protected final static String ARTICLE_CATEGORY = "ARTICLE_CATEGORY";

	// address query의 이름들
	protected final static String SEARCH_KEYWORD_QUERY = "SEARCH_KEYWORD_QUERY";
	protected final static String SEARCH_TYPE_QUERY = "SEARCH_TYPE_QUERY";
	protected final static String PAGINATION_QUERY = "PAGINATION_QUERY";

	protected final String articleRefreshTime;// 캐쉬로 리턴할지 새로 파싱할지 판단하는 기준 시간

	protected final String DEFAULT_SEARCH_TYPE; // 검색할때 검색 카테고리의 기본 값

	protected final Pattern articleNoPattern;// 글번호 가져오는 패턴

	private final int REQUIRED_ARTICLE_NUM_TO_PARSE; // 단어빈도 측정하기 위해 필요한 게시글의 수

	private final int ARTICLE_NUM_PER_PAGE; // 페이지 하나에 게시글이 몇개인지

	protected Pattern REMOVE_PAGINATION_PATTERN;// 글 주소에 페이지네이션 쿼리를 제거하기 위한 패턴(같은 글인데 페이지가 달라서 따로 들어가는것 막음)

	private final int WORD_FREQUENCY_PAGE_NUM; // 단어빈도 측정하기 위한 페이지의
												// 수(DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE/ARTICLE_NUM_PER_PAGE)
	protected final String UPLOAD_DATE_PATTERN;// 업로드 날짜를 파싱하는 패턴

	protected final List<Integer> PARSE_GRADE = Collections.unmodifiableList(Arrays.asList(2, 3, 4));
	// Collections.unmodifiableList(Arrays.asList(2, 5, 10, 30, 50, 70, 100, 200,
	// 400, 600, 800, 1000, 1200, 1400));

	@Value("#{patterns['patterns.specialChar']}")
	private String selectSpecialCharPtn;// 특문 제거를 위한 regexp 패턴

	@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE_MARGIN'])}")
	private Integer REQUIRED_ARTICLE_NUM_TO_PARSE_MARGIN;// 게시글 가져올때 갯수 허용 오차 범위

	@Value("#{new Integer(serverConst['server_const.constant_article_num_max'])}")
	private Integer CONSTANT_ARTICLE_NUM_MAX;// constant 타입의 게시판 글 갯수 인정하는 최대 갯수

	public ArticleParser(String root, String siteAddrRegex, BoardTypeAbstract typeList, String parserName,
			String articleRefreshTime, String timeOut, String checkNoContent, String redirectUrlRegex, String articleNoRegex,
			int requiredArticleNumToParse, int articleNumPerPage, String defaultSearchType, int sleepMin, int sleepMax,
			int retryNum, String uploadDatePattern) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, checkNoContent, redirectUrlRegex, sleepMin, sleepMax, retryNum);
		this.articleRefreshTime = articleRefreshTime;
		this.REQUIRED_ARTICLE_NUM_TO_PARSE = requiredArticleNumToParse;
		this.ARTICLE_NUM_PER_PAGE = articleNumPerPage;
		this.DEFAULT_SEARCH_TYPE = defaultSearchType;
		articleNoPattern = Pattern.compile(articleNoRegex);
		this.WORD_FREQUENCY_PAGE_NUM = (int) Math.ceil((double) REQUIRED_ARTICLE_NUM_TO_PARSE / ARTICLE_NUM_PER_PAGE);
		this.UPLOAD_DATE_PATTERN = uploadDatePattern;

	}

	// 마지막 페이지 인지 확인
	public abstract boolean checkResultIsLastPage(Document doc, ArticleSearchData asd);

	// 공지인지 확인
	public abstract boolean checkIsNotice(Element elem, String boardType);

	// 파싱하기
	public List<BoardArticle> processParsing(ArticleSearchData asd) {

		List<BoardArticle> temp = new ArrayList<>();

		try {
			Document doc = getDocument(buildAddress(asd));

			if (!detectBoard(doc))
				return null;

			if (!checkResultIsLastPage(doc, asd)) {
				log.debug(doc.baseUri() + " 파싱중...");
				temp = defaultParseFunc(doc, asd);
			}
		} catch (Exception e) {

			return temp;
		}

		return temp;

		/*
		 * try { if (!checkRequestLimit()) throw new
		 * SiteParseException("요청이 많습니다. 잠시후 시도해 주세요.");
		 * 
		 * Document doc = getDocument(buildAddress(asd)); if
		 * (!checkResultIsLastPage(doc, asd)) temp = defaultParseFunc(doc, asd);
		 * 
		 * return temp; } catch (SiteParseException e) { return temp; }
		 */
	}

	protected Map<String, Map<String, String>> selectorMap;// 게시판 타입에 따른 파싱에 필요한 selector 들을 담아두는 변수

	// 게시글 파싱에 쓰이는 변수
	protected String searchKeywordQuery;// 검색 쿼리
	protected String searchTypeQuery;// 검색 타입 쿼리
	protected String paginationQuery;// 페이지 번호 쿼리

	protected Map<String, String> articles = new HashMap<>(); // 게시판 글들을 선택하는 셀렉터
	protected Map<String, String> title = new HashMap<>(); // 글의 제목 셀렉터
	protected Map<String, String> contentAddr = new HashMap<>(); // 글의 주소 셀렉터
	protected Map<String, String> viewCnt = new HashMap<>(); // 조회수 셀렉터
	protected Map<String, String> uploadDate = new HashMap<>(); // 게시날짜 셀렉터
	protected Map<String, String> noticeCheck = new HashMap<>(); // 공지인지 확인하는 클래스
	protected Map<String, String> replyNum = new HashMap<>(); // 댓글 셀렉터
	protected Map<String, String> recommend = new HashMap<>();// 추천 셀렉터
	protected Map<String, String> writer = new HashMap<>(); // 글쓴이 셀렉터
	protected Map<String, String> articleNo = new HashMap<>(); // 글번호 셀렉터
	protected Map<String, String> articleCategory = new HashMap<>(); // 카테고리 셀렉터

	// 게시판 파싱에 쓰이는 변수
	protected String board;// 게시판 확인하는 셀렉터
	protected String category;// 게시판의 카테고리 리스트를 확인하는 셀렉터
	// protected Map<String, String> boardTypeCheck;// 게시판의 타입이 들어가있는 맵

	// parserInit을 위한 함수, 자식 클래스에서 postconstruct어노테이션 설정,initializeParser()함수 호출해야함
	abstract public void init();

	// 게시판의 게시글 가져오는 함수
	public List<BoardArticle> defaultParseFunc(Document doc, ArticleSearchData asd) {
		String boardType = asd.getBoard_type();
		Elements articles = getArticles(doc, boardType);
		ArrayList<BoardArticle> baArr = new ArrayList<BoardArticle>();

		for (Element elem : articles) {

			BoardArticle ba = new BoardArticle();
			try {
				boolean isNotice = checkIsNotice(elem, boardType);
				ba.setContent_address(getContentAddr(elem, boardType));
				if (ba.getContent_address() == null || ba.getContent_address().isEmpty())
					continue;
				ba.setTitle(getTitle(elem, boardType));
				ba.setView_cnt(getViewCnt(elem, boardType));
				ba.setArticle_no(getArticleNo(elem, boardType));
				ba.setUpload_date(getUploadDate(elem, boardType));
				ba.setIsNotice(isNotice ? "Y" : "N");
				ba.setReplyNum(getReplyNum(elem, boardType));
				ba.setRecommend(getRecommend(elem, boardType));
				baArr.add(ba);
			} catch (Exception e) {
				continue;
			}

		}

		return baArr;
	}

	public int getReplyNum(Element elem, String boardType) {
		String selector = replyNum.get(boardType);
		if (!StringUtils.isEmpty(selector)) {
			Element tmpElem = elem.selectFirst(selector);
			try {
				return Integer.parseInt(tmpElem.text());
			} catch (Exception e) {
				return 0;
			}

		} else
			return 0;
	}

	public int getRecommend(Element elem, String boardType) {
		String selector = recommend.get(boardType);
		if (selector != null && !selector.isEmpty()) {

			String recommendStr = elem.select(recommend.get(boardType)).text();

			try {
				return Integer.parseInt(recommendStr);
			} catch (Exception e) {
				return 0;
			}

		} else
			return 0;
	}

	public String getUploadDate(Element elem, String boardType) {
		String selector = uploadDate.get(boardType);
		return (selector != null && !selector.isEmpty()) ? elem.select(uploadDate.get(boardType)).text() : "";
	}

	public LocalDate parseUploadDate(String uploadDate) {
		try {

			DateTimeFormatterBuilder fomatterBuilder = new DateTimeFormatterBuilder();
			DateTimeFormatter dtf = fomatterBuilder.appendPattern(UPLOAD_DATE_PATTERN)
					// 현재 연도를 기준으로 파싱함
					.parseDefaulting(ChronoField.YEAR, LocalDate.now().getYear()).toFormatter();

			LocalDate ldt = LocalDate.from(dtf.parse(uploadDate));
			return ldt;

		} catch (Exception e) {
			throw new SiteParseException("날짜값을 파싱하는데 오류가 생겼습니다.");
		}
	}

	public int getViewCnt(Element elem, String boardType) {
		String selector = viewCnt.get(boardType);
		if (!StringUtils.isEmpty(selector)) {
			try {
				String target = elem.select(selector).text();

				return Integer.parseInt(target.trim());
			} catch (Exception e) {
				return 0;
			}
		} else
			return 0;

	}

	public String getContentAddr(Element elem, String boardType) {
		String selector = contentAddr.get(boardType);

		String addr = !StringUtils.isEmpty(selector) ? elem.select(contentAddr.get(boardType)).attr("href") : "";

		try {
			return REMOVE_PAGINATION_PATTERN.matcher(addr).replaceFirst("");
		} catch (Exception e) {
			return addr;
		}

	}

	// title element의 값을 가져와서 전처리를 하는 함수
	public String getTitlePreProcess(Element elem, String boardType) {

		String selector = title.get(boardType);
		Element titleElem = selector != null && !selector.isEmpty() ? elem.selectFirst(title.get(boardType)) : null;
		String title = titleElem != null ? EmojiParser.removeAllEmojis(titleElem.ownText()) : "";

		return title;
	}

	public String getTitle(Element elem, String boardType) {

		return getTitlePreProcess(elem, boardType);
	}

	public long getArticleNo(Element elem, String boardType) {
		// String selector = articleNo.get(boardType);
		Matcher matcher = articleNoPattern.matcher(elem.select(articleNo.get(boardType)).attr("href"));

		if (matcher.find()) {

			try {

				return Long.parseLong(matcher.group());
			} catch (NumberFormatException e) {
				return 0;
			}
		} else
			return 0;
	}

	public Elements getArticles(Document doc, String boardType) {
		String selector = articles.get(boardType);
		return (selector != null && !selector.isEmpty()) ? doc.select(articles.get(boardType)) : null;
	}

	public String buildAddress(ArticleSearchData asd) throws SiteParseException {
		String keyword = makeKeywordQuery(asd.getKeyword());
		String searchType = "";
		if (!keyword.isEmpty())
			searchType = makeSearchTypeQuery(asd.getType_val());
		String pagination = makePaginationQuery(asd.getOffset());
		String category = makeCategoryQuery(asd.getCategory_query());
		String query = keyword + searchType + pagination + category;

		String boardAddress = asd.getBoard_address();

		if (boardAddress.indexOf("?") < 0)
			query = query.replaceFirst("&", "?");

		String fullAddress = boardAddress + query;

		return fullAddress;
	}

	public String makeKeywordQuery(String keyword) {
		return ((keyword != null) && (!keyword.isEmpty())) ? "&" + searchKeywordQuery + keyword : "";
	}

	public String makeSearchTypeQuery(String searchType) {
		return "&" + searchTypeQuery + (searchType != null ? searchType : DEFAULT_SEARCH_TYPE);
	}

	public String makePaginationQuery(int pagination) {
		return "&" + paginationQuery + pagination;
	}

	public String makeCategoryQuery(String category) {
		return category != null ? "&" + category : "";
	}

	public void initializeParser() {

		String _parserName = parserName + "_";

		// 게시판의 종류에 따라서 selector 가 다르기 때문에 종류에 따라 다른 selector를 넣어줌
		// String list = selectors.getProperty(_parserName + "TYPE_LIST");

		List<String> _typeList = typeList.getTypeList();

		for (String type : _typeList)
			boardTypeCheck.put(type, selectors.getProperty(_parserName + type + "_SELECTOR"));
		searchKeywordQuery = selectors.getProperty(_parserName + SEARCH_KEYWORD_QUERY);
		searchTypeQuery = selectors.getProperty(_parserName + SEARCH_TYPE_QUERY);
		paginationQuery = selectors.getProperty(_parserName + PAGINATION_QUERY);

		for (String typeName : _typeList) {

			articles.put(typeName, selectors.getProperty(_parserName + typeName + "_" + ARTICLES));
			title.put(typeName, selectors.getProperty(_parserName + typeName + "_" + TITLE));
			contentAddr.put(typeName, selectors.getProperty(_parserName + typeName + "_" + CONTENT_ADDR));
			viewCnt.put(typeName, selectors.getProperty(_parserName + typeName + "_" + VIEW_CNT));
			uploadDate.put(typeName, selectors.getProperty(_parserName + typeName + "_" + UPLOAD_DATE));
			noticeCheck.put(typeName, selectors.getProperty(_parserName + typeName + "_" + NOTICE_CHECK));
			replyNum.put(typeName, selectors.getProperty(_parserName + typeName + "_" + REPLY_NUM));
			recommend.put(typeName, selectors.getProperty(_parserName + typeName + "_" + RECOMMEND));
			writer.put(typeName, selectors.getProperty(_parserName + typeName + "_" + WRITER));
			articleNo.put(typeName, selectors.getProperty(_parserName + typeName + "_" + ARTICLE_NO));
			articleCategory.put(typeName, selectors.getProperty(_parserName + typeName + "_" + ARTICLE_CATEGORY));
		}

		this.REMOVE_PAGINATION_PATTERN = Pattern.compile("&?" + paginationQuery + "\\d+");

	}

	public String urlPreProcess(String uri) {

		return uri.charAt(uri.length() - 1) != '/' ? uri : uri.substring(0, uri.length() - 1); // 주소 끝에 '/' 있으면 빼줌
	}

	public List<WordFrequencyData> parseWordFrequency(List<ContentVO> boardList) {
		List<WordFrequencyData> result = new ArrayList<>();

		for (ContentVO board : boardList) {
			result.add(parseWordFrequency(board));
		}

		return result;

	}

	public WordFrequencyData parseWordFrequency(ContentVO board) {

		WordFrequencyData wordFrequencyData = new WordFrequencyData();
		Map<String, BoardArticle> daoArticleMap = new HashMap<>();
		ArticleSearchData asd = board.makeSearchArticleData();

		long last_article_no = 0;
		boolean returnFlag = false; // 기준 글갯수를 채우면 break를 하기위한 flag
		int totalArticleNum = 0;

		try {
			long dBlastArticleNo = board.getBoardData().getLatest_article_no();// 저장된 가장 최신의 글 번호 가져오기
			for (int i = 1; i <= WORD_FREQUENCY_PAGE_NUM; ++i) {// 파싱할 페이지 갯수만큼 반복

				asd.setOffset(i);
				List<BoardArticle> tempList = new ArrayList<>();

				tempList = processParsing(asd);// 게시글 가져오기

				if (tempList == null || tempList.size() == 0)// 게시글 없으면 continue
					continue;

				// 파싱한 글 갯수가 기준 갯수 이상이라면 초과되는 글들은 덜어내고 파싱
				if (daoArticleMap.size() + tempList.size() > REQUIRED_ARTICLE_NUM_TO_PARSE) {
					tempList = tempList.subList(0, (REQUIRED_ARTICLE_NUM_TO_PARSE - daoArticleMap.size()) + 1);
					returnFlag = true;
				}

				totalArticleNum += tempList.size();// 가져온 article 갯수를 더함

				for (int j = 0; j < tempList.size(); ++j) {

					BoardArticle ba = tempList.get(j);
					ba.setBoard_code(board.getBoard_code());

					if (!ba.getIsNotice().equals("Y")) {

						if ((dBlastArticleNo != 0) && (dBlastArticleNo >= ba.getArticle_no())) {
							throw new SiteParseException(i + " 페이지는 파싱된 페이지 입니다.");
						} else {

							if (ba.getArticle_no() > last_article_no)
								last_article_no = ba.getArticle_no();

							daoArticleMap.put(ba.getTitle(), ba);

						}

					}
				}
			}
		} catch (Exception e) {
			log.debug(e.getMessage() + " 파싱 완료");
		}

		wordFrequencyData.setBoard(board);
		// 요구 게시글 갯수-여유 갯수<=게시글 갯수<=요구 게시글 갯수+여유갯수 일때만 데이터 넘김
		returnFlag = (REQUIRED_ARTICLE_NUM_TO_PARSE - REQUIRED_ARTICLE_NUM_TO_PARSE_MARGIN) <= totalArticleNum;

		if (returnFlag) {
			wordFrequencyData
					.setArticleList(daoArticleMap.entrySet().stream().map(k -> k.getValue()).collect(Collectors.toList()));
			wordFrequencyData.setBoardData(new BoardData(board.getBoard_code(), last_article_no, totalArticleNum));
		} else {
			// log.info(board.getBoard_name() + "파싱 안됨(게시글 갯수 부족) - " + totalArticleNum + "
			// 개");
		}

		wordFrequencyData.setArticleParser(this);
		return wordFrequencyData;
	}

	// 글 번호가 일정한 값(1) 차이가 나는지 확인
	private boolean checkArticleNoIsConstant(List<BoardArticle> articleList) {

		int inconstantCnt = 0;

		if (articleList.size() >= 2)// 맨 앞 2개 가져옴
		{
			for (int i = 1; i < articleList.size(); ++i) {
				long diff = Math.abs(articleList.get(i).getArticle_no() - articleList.get(i - 1).getArticle_no());
				// 두 글 번호 차가 1이 넘는경우 1씩더함
				if (diff > 1)
					inconstantCnt++;
			}

			// 글번호 차이가 1이 넘는 글들이 한 페이지의 절반 이상이라면 글번호가 일정하지 않다고 판정
			if ((float) inconstantCnt / articleList.size() > 0.5)
				return false;
			return true;

		} else
			return true;
	}

	// 글 번호가 일정하지 않을때 쌓인 글의 근사치를 가져오는 함수
	public BoardData parseApproximateLatestArticleNo(ContentVO board) {

		BoardData result = board.getBoardData();
		List<BoardArticle> articleList = null;		
		result.setCurrent_cycle(null);// 파싱전 초기화
		
		int i = 0;// 몇바퀴 돌았는지 확인하는 변수

		for (i = 0; i < PARSE_GRADE.size(); ++i) {
			try {

				int k = PARSE_GRADE.get(i);
				ArticleSearchData asd = new ArticleSearchData(board.getBoard_code(), k, null, 1);// 특정 페이지의 글들을 가져옴

				asd.applyBoardData(board);

				articleList = processParsing(asd);

				if (articleList != null && articleList.size() > 0) {
					// 공지가 아닌 첫번째 게시글이 있다면
					List<BoardArticle> filteredList = articleList.stream().filter(a -> a.getIsNotice().equals("N"))
							.collect(Collectors.toList());
					long db_latest_article_no = result.getLatest_article_no();

					if (filteredList.size() > 0) {

						if (filteredList.stream().filter(ba -> ba.getArticle_no() <= db_latest_article_no).count() > 0) {
							result.setCurrent_cycle(new Long(k * getARTICLE_NUM_PER_PAGE()));
							break;
						}
					} else {
						result.setCurrent_cycle(new Long(PARSE_GRADE.get(Math.max(0, i - 1)) * getARTICLE_NUM_PER_PAGE()));
						break;
					}

				} else {
					result.setCurrent_cycle(new Long(PARSE_GRADE.get(Math.max(0, i - 1)) * getARTICLE_NUM_PER_PAGE()));
					break;
				}
			} catch (Exception e) {
				return null;
			}
		}

		// 에러없이 마지막까지 파싱되지 않았으면 PARSE_GRADE 최대치로 추정
		if (i == PARSE_GRADE.size() && result.getCurrent_cycle() == null) {
			result.setCurrent_cycle(new Long(PARSE_GRADE.get(PARSE_GRADE.size() - 1) * getARTICLE_NUM_PER_PAGE()));
		}

		return result;
	}

	// 글번호가 일정할때 최신 글번호를 가져오는 함수 (최신글번호 - 이전 최신글번호=쌓인 글의 수)
	public BoardData parseLatestArticleNo(ContentVO board) {
		ArticleSearchData asd = new ArticleSearchData(board.getBoard_code(), 1, null, 1);// 1페이지 1개 가져옴
		asd.applyBoardData(board);

		BoardData bd = board.getBoardData();
		bd.setBoard_code(board.getBoard_code());

		if (bd.getParent_board_code() != null || bd.getIgnore_flag() > 0) {// 무시하는 게시판이거나, 부모 게시판을 가지고 있다면(루리웹 전용) 파싱을 건너뜀
			bd.setCurrent_cycle((long) 0);
			return bd;
		}

		List<BoardArticle> articleList = processParsing(asd);

		if (articleList == null || articleList.size() <= 0) {
			board.setAvailable("N");
			bd.setCurrent_cycle((long) 0);
			bd.setLatest_article_no((long) 0);
		} else {

			board.setAvailable("Y");
			List<BoardArticle> excludeNoticeList = articleList.stream().filter(a -> a.getIsNotice().equals("N"))
					.collect(Collectors.toList());

			// 글번호가 일정하지 않아서 근사치를 측정해야 하는지
			boolean isConstant = checkArticleNoIsConstant(excludeNoticeList);
			bd.setRegular_article_no_flag(isConstant ? 1 : 0);

			Integer cnt = null;// 첫 페이지에서 db에 저장된 이 게시판의 과거 최신 글번호까지 몇개의 글이 쌓였는지
			for (int i = 0; i < excludeNoticeList.size(); ++i) {
				if (isConstant) {
					if (excludeNoticeList.get(i).getArticle_no() == bd.getLatest_article_no()) {
						cnt = i;
						break;
					}
				} else {
					// 글번호를 파싱한다음 같은게 있으면 리턴
					if (excludeNoticeList.get(i).getArticle_no() == bd.getLatest_article_no()) {
						cnt = i;
						break;
					}
				}

			}

			if (cnt != null) {// 첫페이지 이하로 글이 쌓였다는 뜻

				bd.setLatest_article_no(bd.getLatest_article_no());
				bd.setCurrent_cycle((long) cnt);

			} else {
				BoardArticle firstArticle = excludeNoticeList.stream().filter(a -> a.getIsNotice().equals("N")).findAny()
						.orElse(new BoardArticle(0));

				if (isConstant) {
					if (bd.getLatest_article_no() == null || bd.getLatest_article_no() == 0) {
						bd.setCurrent_cycle((long) 0);
					} else {
						Long targetNum = firstArticle.getArticle_no() - bd.getLatest_article_no();

						if (targetNum >= CONSTANT_ARTICLE_NUM_MAX || targetNum < 0) {// 허용하는 최대갯수를 넘을때(터무니없이 클때),혹은 0보다 작으면
							bd = parseApproximateLatestArticleNo(board);// 근사치 구하는것으로 대체
						} else {
							bd.setCurrent_cycle(targetNum < 0 ? 0 : targetNum);// 아니면 구한값 그대로 넘기기
						}
					}

				} else {
					if (bd.getLatest_article_no() == null || bd.getLatest_article_no() == 0) {
						bd.setCurrent_cycle((long) 0);
					} else
						bd = parseApproximateLatestArticleNo(board);
				}
				bd.setLatest_article_no(firstArticle.getArticle_no());// cycle 계산이끝나고 글번호 최신화
			}
		}

		return bd;

	}

	public Map<String, Map<String, String>> getSelectorMap() {
		return selectorMap;
	}

	public void setSelectorMap(Map<String, Map<String, String>> selectorMap) {
		this.selectorMap = selectorMap;
	}

	public String getSearchKeywordQuery() {
		return searchKeywordQuery;
	}

	public String getSearchTypeQuery() {
		return searchTypeQuery;
	}

	public String getPaginationQuery() {
		return paginationQuery;
	}

	public void setSearchKeywordQuery(String searchKeywordQuery) {
		this.searchKeywordQuery = searchKeywordQuery;
	}

	public void setSearchTypeQuery(String searchTypeQuery) {
		this.searchTypeQuery = searchTypeQuery;
	}

	public void setPaginationQuery(String paginationQuery) {
		this.paginationQuery = paginationQuery;
	}

	public Map<String, String> getArticles() {
		return articles;
	}

	public Map<String, String> getTitle() {
		return title;
	}

	public Map<String, String> getContentAddr() {
		return contentAddr;
	}

	public Map<String, String> getViewCnt() {
		return viewCnt;
	}

	public Map<String, String> getUploadDate() {
		return uploadDate;
	}

	public Map<String, String> getNoticeCheck() {
		return noticeCheck;
	}

	public Map<String, String> getReplyNum() {
		return replyNum;
	}

	public Map<String, String> getRecommend() {
		return recommend;
	}

	public Map<String, String> getWriter() {
		return writer;
	}

	public Map<String, String> getArticleNo() {
		return articleNo;
	}

	public Map<String, String> getArticleCategory() {
		return articleCategory;
	}

	public void setArticles(Map<String, String> articles) {
		this.articles = articles;
	}

	public void setTitle(Map<String, String> title) {
		this.title = title;
	}

	public void setContentAddr(Map<String, String> contentAddr) {
		this.contentAddr = contentAddr;
	}

	public void setViewCnt(Map<String, String> viewCnt) {
		this.viewCnt = viewCnt;
	}

	public void setUploadDate(Map<String, String> uploadDate) {
		this.uploadDate = uploadDate;
	}

	public void setNoticeCheck(Map<String, String> noticeCheck) {
		this.noticeCheck = noticeCheck;
	}

	public void setReplyNum(Map<String, String> replyNum) {
		this.replyNum = replyNum;
	}

	public void setRecommend(Map<String, String> recommend) {
		this.recommend = recommend;
	}

	public void setWriter(Map<String, String> writer) {
		this.writer = writer;
	}

	public void setArticleNo(Map<String, String> articleNo) {
		this.articleNo = articleNo;
	}

	public void setCategory(Map<String, String> articleCategory) {
		this.articleCategory = articleCategory;
	}

	public int getWORD_FREQUENCY_PAGE_NUM() {
		return WORD_FREQUENCY_PAGE_NUM;
	}

	public String getBoard() {
		return board;
	}

	public String getCategory() {
		return category;
	}

	public void setArticleCategory(Map<String, String> articleCategory) {
		this.articleCategory = articleCategory;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public Properties getSelectors() {
		return selectors;
	}

	public void setSelectors(Properties selectors) {
		this.selectors = selectors;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getArticleRefreshTime() {
		return articleRefreshTime;
	}

	public int getARTICLE_NUM_PER_PAGE() {
		return ARTICLE_NUM_PER_PAGE;
	}

	public int getREQUIRED_ARTICLE_NUM_TO_PARSE() {
		return REQUIRED_ARTICLE_NUM_TO_PARSE;
	}

}

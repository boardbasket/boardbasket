package com.nayuta.mosite.content.controller;

import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nayuta.mosite.admin.model.service.AdmUpsertParsedSite;
import com.nayuta.mosite.content.model.service.CommandContainer;
import com.nayuta.mosite.content.model.service.Word2VecCommand.WvTestWord2Vec;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectIcon;
import com.nayuta.mosite.member.model.service.MmMemberCheck;

/*
 * 예전 기능들이 있는 controller (거의 쓰지않음)
 * 
 * 
 * 
 * */

@Controller
@Validated
public class ContentController {

	@Autowired
	private CommandContainer cController;

	@Autowired
	@Qualifier("pcSelectIcon")
	private PcSelectIcon pcSelectIcon;

	@Autowired
	@Qualifier("wvTestWord2Vec")
	private WvTestWord2Vec wvTestWord2Vec;

	@Autowired
	@Qualifier("mmMemberCheck")
	private MmMemberCheck mmMemberCheck;

	@Autowired
	@Qualifier("admUpsertParsedSite")
	private AdmUpsertParsedSite admUpsertParsedSite;

	private Map<String, Object> result;

	@RequestMapping(value = { "/oldMain" }, method = RequestMethod.GET)
	public String setTabBoards(ModelMap modelMap, HttpSession session, Authentication auth, Device device, Principal principal) {

		// cController.execute(pcSelectUserTabs, modelMap);
		// cController.execute(pcSelectMainDivBoard, modelMap);
		// result.put("result", modelMap.get("result"));
		return "main/newMain";
	}

	@GetMapping(value = "/checkSession.do")
	@ResponseBody
	public boolean checkSession(HttpServletRequest request, HttpSession session) {
		if (session == null || !request.isRequestedSessionIdValid()) {
			return false;
		} else
			return true;

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = { "/getIcon" }, method = RequestMethod.GET)
	public ResponseEntity<byte[]> selectIcon(@RequestParam("path") String path, ModelMap modelMap) {

		modelMap.put("fileLocation", path);
		cController.execute(pcSelectIcon, modelMap);

		return (ResponseEntity<byte[]>) modelMap.get("entity");

	}

	@RequestMapping(value = { "/boardInsertTest.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> testFunc(ModelMap modelMap) {

		cController.execute(admUpsertParsedSite, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/word2Vec.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> word2Vec(ModelMap modelMap, @ModelAttribute("targetStr") String targetStr,

			@ModelAttribute("targetAddr") String targetAddr) {

		cController.execute(wvTestWord2Vec, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}

}

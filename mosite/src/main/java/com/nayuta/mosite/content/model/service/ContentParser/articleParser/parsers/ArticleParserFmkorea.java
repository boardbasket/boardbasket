package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;

@Component("articleParserFmkorea")
public class ArticleParserFmkorea extends ArticleParser {

	private final String contentAddrPrepix;
	private final Pattern replyNumPattern;// 정규식으로 빼야됨
	private final String checkLastPage;
	// 에펨은 10000페이지 이상의 검색을 허용하지 않음
	private final int PAGE_LIMIT = 10000;

	/*
	 * private static long request_end_time = 0;// 리퀘스트가 끝난 시간 private final long
	 * REQUEST_NUM_RESET_TIME; private static int current_continuous_request_num =
	 * 0;
	 */

	public ArticleParserFmkorea(@Value("#{selectors['FMKOREA_ROOT']}") String root,
			@Value("#{selectors['FMKOREA_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeFmkorea") BoardTypeAbstract typeList,
			@Value("#{selectors['FMKOREA_PARSER_NAME']}") String parserName,
			@Value("#{selectors['FMKOREA_ROOT']}") String contentAddrPrepix,
			@Value("#{selectors['FMKOREA_REPLY_NUM_REGEX']}") String replyNumRegex,
			@Value("#{selectors['FMKOREA_CHECK_LAST_PAGE']}") String checkLastPage,
			@Value("#{selectors['FMKOREA_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['FMKOREA_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['FMKOREA_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['FMKOREA_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{new Long(serverConst['server_const.page_threshold'])}") long request_num_reset_time,
			@Value("#{selectors['FMKOREA_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{new Integer(selectors['FMKOREA_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['FMKOREA_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);
		/* this.REQUEST_NUM_RESET_TIME = request_num_reset_time * 60 * 1000; */
		this.checkLastPage = checkLastPage;
		this.contentAddrPrepix = (String) contentAddrPrepix.subSequence(0, contentAddrPrepix.length() - 1);
		replyNumPattern = Pattern.compile(replyNumRegex);
	}

	@PostConstruct
	@Override
	public void init() {

		initializeParser();

	}

	@Override
	public List<BoardArticle> processParsing(ArticleSearchData asd) {
		/*
		 * long currentTime = System.currentTimeMillis();// 현재시간 구하기
		 * 
		 * // 현재시간 - 마지막 리퀘스트 시간 >= 리셋 하는 기준 시간 이면 if (currentTime - request_end_time >=
		 * REQUEST_NUM_RESET_TIME) { current_continuous_request_num = 0;// 리퀘스트 수를 초기화 }
		 * else { current_continuous_request_num++;// 아니면 리퀘스트 수를 더하고 request_end_time =
		 * currentTime;// 마지막 리퀘스트 시간을 현재 시간으로 } // 연속적인 리퀘스트 수가 일정 갯수를 넘으면 빈 리스트를 넘겨줌
		 * if (current_continuous_request_num >= CONTINUOUS_REQUEST_LIMIT) return new
		 * ArrayList<BoardArticle>(); else
		 */
		return super.processParsing(asd);
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		Elements elems = doc.select(checkLastPage);

		String href = elems.get(1).attr("href");

		return (asd.getOffset() > PAGE_LIMIT) || StringUtils.isEmpty(href);
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		return elem.hasClass(noticeCheck.get(boardType));
	}

	@Override
	public String getContentAddr(Element elem, String boardType) {
		String target = super.getContentAddr(elem, boardType);
		// String selector = contentAddr.get(boardType);

		return contentAddrPrepix + target;
	}

	@Override
	public int getReplyNum(Element elem, String boardType) {

		int result = 0;
		String selector = replyNum.get(boardType);
		if (selector == null || selector.isEmpty())
			return result;

		Element tmpElem = elem.selectFirst(selector);
		if (tmpElem == null)
			return result;

		String target = elem.text();

		if (!elem.hasClass("replyNum")) {
			Matcher matcher = replyNumPattern.matcher(target);

			while (matcher.find())
				target = matcher.group();
		}

		try {
			return Integer.parseInt(target);
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public String getTitle(Element elem, String boardType) {

		String target = super.getTitle(elem, boardType);
		Matcher matcher = replyNumPattern.matcher(target);

		if (matcher.find()) {
			String removeTarget = matcher.group(0);
			int idx = target.indexOf(removeTarget);
			return target.substring(0, idx);
		} else
			return target;

	}

	public WordFrequencyData parseWordFrequency(ContentVO board) {

		WordFrequencyData wordFrequencyData = new WordFrequencyData();
		Map<String, BoardArticle> daoArticleMap = new HashMap<>();
		ArticleSearchData asd = board.makeSearchArticleData();
		long last_article_no = 0;
		int totalArticleNum = 0;
		try {
			long dBlastArticleNo = board.getBoardData().getLatest_article_no();// 저장된 가장 최신의 글 번호 가져오기
			for (int i = 1; i <= getWORD_FREQUENCY_PAGE_NUM(); ++i) {// 파싱할 페이지 갯수만큼 반복

				asd.setOffset(i);
				List<BoardArticle> tempList = new ArrayList<>();

				tempList = processParsing(asd);// 게시글 가져오기
				if (tempList == null || tempList.size() == 0)// 게시글 없으면 continue
					continue;

				if (daoArticleMap.size() + tempList.size() > getREQUIRED_ARTICLE_NUM_TO_PARSE())
					tempList = tempList.subList(0, getREQUIRED_ARTICLE_NUM_TO_PARSE() - daoArticleMap.size());

				totalArticleNum += tempList.size();

				for (int j = 0; j < tempList.size(); ++j) {

					BoardArticle ba = tempList.get(j);
					ba.setBoard_code(board.getBoard_code());

					if (!ba.getIsNotice().equals("Y")) {

						if ((dBlastArticleNo != 0) && (dBlastArticleNo == ba.getArticle_no()))
							throw new SiteParseException("파싱된 페이지 입니다.");
						else {

							if (ba.getArticle_no() > last_article_no)
								last_article_no = ba.getArticle_no();

							daoArticleMap.put(ba.getTitle(), ba);
						}

					}
				}

			}
		} catch (Exception e) {
			log.debug("파싱 마무리 ");
		}
		wordFrequencyData.setBoard(board);
		wordFrequencyData.setArticleList(daoArticleMap.entrySet().stream().map(k -> k.getValue()).collect(Collectors.toList()));
		wordFrequencyData.setBoardData(new BoardData(board.getBoard_code(), last_article_no, totalArticleNum));
		wordFrequencyData.setArticleParser(this);
		return wordFrequencyData;
	}

}

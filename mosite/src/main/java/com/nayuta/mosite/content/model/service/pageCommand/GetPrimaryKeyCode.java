package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public interface GetPrimaryKeyCode {

	// public String pullPrimaryKey();
	public void pushPrimaryKey(Map<String, Object> codeMap);

	public Map<String, Object> pullPrimaryKey();
}

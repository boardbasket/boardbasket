package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;

@Component(value = "boardParserFmkorea")
public class BoardParserFmkorea extends BoardParserAbstract {

	private final String titleSelector;
	private final String categoryValueRegex;
	private final String checkRESTUrl;
	private final String queryBaseUrl;
	private final String ROOT_TO_BEST;
	private final int MAX_POPULAR_BOARD_NUM;
	private final int POPULAR_PAGE_PARSE_NUM;
	private final int REQUIRED_ARTICLE_NUM_TO_PARSE; // 단어빈도 측정하기 위해 필요한 게시글의 수
	private final int ARTICLE_NUM_PER_PAGE; // 페이지 하나에 게시글이 몇개인지
	private final int WORD_FREQUENCY_PAGE_NUM;
	private final int PARSE_TIME_LIMIT = 3000000;// 파싱하는데 걸리는 총 시간의 제한(밀리초)

	public BoardParserFmkorea(@Value("#{selectors['FMKOREA_ROOT']}") String root,
			@Value("#{selectors['FMKOREA_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeFmkorea") BoardTypeAbstract typeList,
			@Value("#{selectors['FMKOREA_PARSER_NAME']}") String parserName,
			@Value("#{selectors['FMKOREA_TITLE_SELECTOR']}") String titleSelector,
			@Value("#{selectors['FMKOREA_CATEGORY_VAL_REGEX']}") String categoryValueRegex,
			@Value("#{selectors['FMKOREA_CHECK_REST_URL']}") String checkRESTUrl,
			@Value("#{selectors['FMKOREA_QUERY_BASE_URL']}") String queryBaseUrl,
			@Value("#{selectors['FMKOREA_FALSE_ROOT']}") String falseRoot,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['FMKOREA_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['FMKOREA_POPULAR_ADDR']}") String popularAddr,
			@Value("#{new Integer(selectors['FMKOREA_POPULAR_PAGE_PARSE_NUM'])}") int popular_page_parse_num,
			@Value("#{new Integer(selectors['FMKOREA_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['FMKOREA_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['FMKOREA_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, popularBoard, popularAddr, null, null, sleepMin,
				sleepMax, retryNum);
		this.titleSelector = titleSelector;
		this.categoryValueRegex = categoryValueRegex;
		this.checkRESTUrl = checkRESTUrl;
		this.queryBaseUrl = queryBaseUrl;
		this.ROOT_TO_BEST = falseRoot;
		this.POPULAR_PAGE_PARSE_NUM = popular_page_parse_num;
		this.REQUIRED_ARTICLE_NUM_TO_PARSE = requiredArticleNumToParse;
		this.ARTICLE_NUM_PER_PAGE = articleNumPerPage;
		this.WORD_FREQUENCY_PAGE_NUM = (int) Math.ceil((double) REQUIRED_ARTICLE_NUM_TO_PARSE / ARTICLE_NUM_PER_PAGE);
		// MAX_POPULAR_BOARD_NUM = 게시판 수(x) * 페이지 수(8) * 평균 시간(초) <3000초(50분) 이하가 되는 게시판
		// 수
		this.MAX_POPULAR_BOARD_NUM = (int) Math
				.ceil(((double) PARSE_TIME_LIMIT / (WORD_FREQUENCY_PAGE_NUM * (SLEEP_PER_PAGE_MIN + (SLEEP_PER_PAGE_MAX) / 2))));
	}

	@PostConstruct
	public void init() {
		initializeParser();

	}

	@Override
	public String urlPreProcess(String addr) {

		String address = httpToHttps(addr);
		Pattern checkMobile = Pattern.compile(checkMobileRegex);

		Matcher mobileMatcher = checkMobile.matcher(address);

		// 모바일 주소를 pc 주소로 바꾸는 작업
		if (mobileMatcher.find())
			address = address.replace("https://m.", "https://www.");

		Pattern checkRESTTypeUrl = Pattern.compile(checkRESTUrl);

		Matcher m2 = checkRESTTypeUrl.matcher(address);

		while (m2.find()) {
			// ex) http://www.fmkorea.com/battleground 같은 주소 형식
			String mid = m2.group(3);
			try {
				// 주소 마지막 부분이 숫자로만 이루어졌는지 확인(숫자로만 이루어지면 게시판이 아니라 글주소이기 때문)
				Float.parseFloat(mid);
			} catch (NumberFormatException e) {

				address = queryBaseUrl + mid;// mid값 될부분
				// return address;
			}
			// return m2.group(1);

		}

		Pattern pattern = Pattern.compile(urlFilterRegex);

		Matcher matcher = pattern.matcher(address);
		// ex)http://www.fmkorea.com/index.php?mid=battleground 같은 주소 형식
		while (matcher.find()) {

			/*
			 * if (matcher.group(3) == null) return uri.toString();
			 */
			address = queryBaseUrl + matcher.group(3);

			// return address;
		}

		/*
		 * Document doc = getDocument(address);
		 * 
		 * String title = getTitle(doc); // 에펨은 검색한 주소가 존재하지 않으면 무조건 메인으로 보내기 때문에 게시판
		 * 이름이 '포텐 터진 게시판'(메인)이면 전부 메인 주소로 치환 if (title.equals(CHECK_ROOT)) return
		 * falseRoot; else
		 */
		return address;

	}

	public String getIgnoreBoard() {
		return ignoreBoard;
	}

	public void setIgnoreBoard(String ignoreBoard) {
		this.ignoreBoard = ignoreBoard;
	}

	@Override
	public String getTitle(Document doc) {
		Element elem = doc.selectFirst(titleSelector);

		String title = elem.text();

		return title;

	}

	@Override
	public BoardCategory parseCategoryFunc(Element elem) {

		String cateName = elem.text();
		String cateVal = elem.attr("href");
		Pattern pattern = Pattern.compile(categoryValueRegex);

		Matcher matcher = pattern.matcher(cateVal);

		if (matcher.find()) {
			cateVal = matcher.group();
			return new BoardCategory(cateName, cateVal);
		} else
			return null;

	}

	@Override
	public List<String> getPopularBoardAddr() {

		List<String> result = new ArrayList<>();
		try {

			for (int i = 1; i <= POPULAR_PAGE_PARSE_NUM; ++i) {
				Document doc = getDocument(popularAddr + i);

				Elements elems = doc.select(popularBoard);

				for (Element elem : elems) {

					String addr = elem.attr("href");
					if (addr.equals("/"))
						addr = ROOT_TO_BEST;
					else
						addr = queryBaseUrl + addr;

					if (!result.contains(addr))
						result.add(urlPreProcess(addr));
				}

			}

			return result.stream().distinct().limit(MAX_POPULAR_BOARD_NUM).collect(Collectors.toList());

		} catch (SiteParseException e) {
			return result;
		}

	}

}

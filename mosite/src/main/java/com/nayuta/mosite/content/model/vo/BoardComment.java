package com.nayuta.mosite.content.model.vo;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nayuta.mosite.common.util.LocalDateSerializer;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.vo.validatorGroup.InsertGroup;

public class BoardComment extends GetPrimaryKeyCodeCls {

	private static final long serialVersionUID = -5286787083234456766L;
	private String member_no;
	@NotBlank
	@Pattern(message = "작성자 이름 형식이 잘못 되었습니다.", regexp = "^[a-zA-Z0-9가-힣ㄱ-ㅎ]{2,10}$", groups = { InsertGroup.class })
	private String writer;
	@Pattern(message = "comment 형식이 잘못 되었습니다.", regexp = "^[\\w\\W~!#$%&^*()@]{0,100}", groups = { InsertGroup.class })
	private String user_comment;
	private int up_vote;
	private int down_vote;
	private int comment_val;
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate insert_date;
	private List<CommentEvaluation> comment_eval_list;
	private int my_comment_flag;

	public int getComment_val() {
		return comment_val;
	}

	public void setComment_val(int comment_val) {
		if (comment_val > 0)
			this.comment_val = 1;
		else
			this.comment_val = -1;
	}

	@Override
	public void setComment_code(String comment_code) {
		super.setComment_code(comment_code);
		if (comment_eval_list != null) {
			for (CommentEvaluation ce : comment_eval_list)
				ce.setComment_code(comment_code);
		}

	}

	public String getMember_no() {
		return member_no;
	}

	public String getWriter() {
		return writer;
	}

	public void setMember_no(String member_no) {
		this.member_no = member_no;

		if (comment_eval_list != null) {
			for (CommentEvaluation ce : comment_eval_list)
				ce.setMember_no(member_no);
		}
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getUser_comment() {
		return user_comment;
	}

	public void setUser_comment(String user_comment) {
		this.user_comment = user_comment;
	}

	public int getUp_vote() {
		return up_vote;
	}

	public int getDown_vote() {
		return down_vote;
	}

	public void setUp_vote(int up_vote) {
		this.up_vote = up_vote;
	}

	public void setDown_vote(int down_vote) {
		this.down_vote = down_vote;
	}

	public LocalDate getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(LocalDate insert_date) {
		this.insert_date = insert_date;
	}

	public List<CommentEvaluation> getComment_eval_list() {
		return comment_eval_list;
	}

	public void setComment_eval_list(List<CommentEvaluation> comment_eval_list) {
		this.comment_eval_list = comment_eval_list;
	}

	public int getMy_comment_flag() {
		return my_comment_flag;
	}

	public void setMy_comment_flag(int my_comment_flag) {
		this.my_comment_flag = my_comment_flag;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void pushPrimaryKey(Map<String, Object> codeMap) {

		if (codeMap.containsKey("commentEvalListRefMap"))
			this.comment_eval_list = (List<CommentEvaluation>) codeMap.get("commentEvalListRefMap");
		super.pushPrimaryKey(codeMap);
	}

	@Override
	public Map<String, Object> pullPrimaryKey() {
		if (comment_eval_list != null)
			codeMap.put("commentEvalListRefMap", comment_eval_list);

		return super.pullPrimaryKey();
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardCategory;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service("pcSelectBoardCategoricalData")
public class PcSelectBoardCategoricalData extends AbstractPageCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {
		SearchData sd = (SearchData) modelMap.get("searchData");
		modelMap.put("result", selectBoardCategoricalDataFunc(sd));
	}

	public JSONObject selectBoardCategoricalDataFunc(SearchData searchData) {

		JSONObject resultObj = new JSONObject();// 결과 오브젝트

		Map<String, Object> params = searchData.makeParamMap(weekUtil);

		List<BoardCategory> categoryList = bDao.selectBoardCategoricalData(params);

		resultObj.put("categoryList", categoryList);

		return resultObj;

	}

}

package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

//각 사이트의 게시글 검색 타입(글,내용,작성자)등을 가져오는 VO
public class SearchType extends GetPrimaryKeyCodeCls implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String site_name;
	private String type_name;
	private String type_val;

	public String getSite_code() {
		return site_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}

	public String getType_val() {
		return type_val;
	}

	public void setType_val(String type_val) {
		this.type_val = type_val;
	}

	@Override
	public String toString() {
		return "SearchType [site_code=" + site_code + ", site_name=" + site_name + ", type_name=" + type_name
				+ ", type_val=" + type_val + "]";
	}

	public SearchType(String site_code, String site_name, String type_name, String type_val) {
		super();
		this.site_code = site_code;
		this.site_name = site_name;
		this.type_name = type_name;
		this.type_val = type_val;
	}

	public SearchType() {
		super();

	}

}

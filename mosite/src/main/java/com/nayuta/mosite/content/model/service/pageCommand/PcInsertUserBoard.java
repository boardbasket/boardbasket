package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.ContentVO;

import net.sf.json.JSONObject;

//즐겨찾기

@Service("pcInsertUserBoard")
public class PcInsertUserBoard extends AbstractPageCls implements CommandInterface {

	private final int FAVORITE_BOARD_MAX_NUM;

	public PcInsertUserBoard(
			@Value("#{new Integer(serverConst['server_const.favorite_board_max_num'])}") int fAVORITE_BOARD_MAX_NUM) {
		super();
		FAVORITE_BOARD_MAX_NUM = fAVORITE_BOARD_MAX_NUM;
	}

	@Override
	public void execute(ModelMap modelMap) {

		JSONObject result = new JSONObject();
		modelMap.put("memberNo", getMemberNo());

		List<ContentVO> userBoardList = bDao.selectMembersAllUserBoard(modelMap);

		if (userBoardList.size() >= FAVORITE_BOARD_MAX_NUM)
			throw new MVCServiceException("즐겨찾기는 최대 " + FAVORITE_BOARD_MAX_NUM + "개 까지 가능합니다.");
		else if (userBoardList.stream().filter(k -> k.getBoard_code().equals(modelMap.get("board_code"))).count() > 0)
			throw new MVCServiceException("이미 즐겨찾기에 추가된 게시판입니다.");

		result.put("status", bDao.insertUserBoard(modelMap) > 0 ? 1 : 0);
		result.put("user_board_code", modelMap.get("user_board_code"));
		log.info("유저 " + getMemberNo()+"가 " + modelMap.get("board_code") + " 게시판 추가");
		modelMap.put("result", result);

	}

}

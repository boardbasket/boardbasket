package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;

@Component(value = "boardParserBoardBasket")
public class BoardParserBoardBasket extends BoardParserAbstract {

	@PostConstruct
	public void init() {
		initializeParser();
	}

	public BoardParserBoardBasket(@Value("#{selectors['BOARDBASKET_ROOT']}") String root,
			@Value("#{selectors['BOARDBASKET_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeBoardBasket") BoardTypeAbstract typeList,
			@Value("#{selectors['BOARDBASKET_PARSER_NAME']}") String parserName,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, null, null, null, null, sleepMin, sleepMax,retryNum);

	}

	@Override
	public String urlPreProcess(String addr) {

		String targetAddress = httpsTohttp(addr);

		Pattern pattern = Pattern.compile(urlFilterRegex);

		Matcher matcher = pattern.matcher(targetAddress);

		if (matcher.find())
			return matcher.group();
		else
			return null;

	}

	@Override
	public List<String> getPopularBoardAddr() {
		return new ArrayList<String>();
	}
}

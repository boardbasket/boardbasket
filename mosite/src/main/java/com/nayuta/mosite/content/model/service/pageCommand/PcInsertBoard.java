package com.nayuta.mosite.content.model.service.pageCommand;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.AutoCompleteResult;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

//유저가 주소를 입력하면 게시판이 db에 존재할때 데이터 리턴,없으면 넣고 확인메시지 돌려줌...
@Service("pcInsertBoard")
public class PcInsertBoard extends AbstractPageCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		SearchData searchData = (SearchData) modelMap.get("searchData");
		String originStr = searchData.getBoard_name();
		String noSpaceStr = originStr.replaceAll("\\s", "");
		URI uri = bParser.checkStringIsAddress(noSpaceStr);
		List<AutoCompleteResult> arList = new ArrayList<>();

		ContentVO result = bDao.selectIsBoardExist(bParser.getParsedAddress(uri.toString()));
		
		if (result == null) {
			result = bParser.parseBoard(uri.toString());
			if (result == null)
				throw new SiteParseException("지원되지 않거나 게시판을 찾을 수 없습니다.");

			Site site = bDao.selectSiteWithAddress(result.getSite_address());
			result.applySiteData(site);
			bDao.insertBoard(result);
		}

		arList.add(new AutoCompleteResult(result));
		resultObj.put("searchResult", arList);

	}

}

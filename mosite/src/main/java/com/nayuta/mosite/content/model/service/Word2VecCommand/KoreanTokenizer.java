package com.nayuta.mosite.content.model.service.Word2VecCommand;

public class KoreanTokenizer /*implements Tokenizer*/ {

	/*private Iterator<String> tokenIter;
	private List<String> tokenList;
	private TokenPreProcess preProcess;

	public KoreanTokenizer(String toTokenize) {
		// need normalize?

		// Tokenize
		Seq<org.openkoreantext.processor.tokenizer.KoreanTokenizer.KoreanToken> tokens = OpenKoreanTextProcessorJava
				.tokenize(toTokenize);
		tokenList = new ArrayList<>();
		Iterator<KoreanTokenJava> iter = OpenKoreanTextProcessorJava.tokensToJavaKoreanTokenList(tokens).iterator();

		while (iter.hasNext()) {
			tokenList.add(iter.next().getText());
		}
		tokenIter = tokenList.iterator();

	}

	@Override
	public boolean hasMoreTokens() {
		return tokenIter.hasNext();
	}

	@Override
	public int countTokens() {

		return tokenList.size();
	}

	@Override
	public String nextToken() {
		if (hasMoreTokens() == false) {
			throw new NoSuchElementException();
		}
		return this.preProcess != null ? this.preProcess.preProcess(tokenIter.next()) : tokenIter.next();
	}

	@Override
	public List<String> getTokens() {
		return tokenList;
	}

	@Override
	public void setTokenPreProcessor(TokenPreProcess tokenPreProcessor) {
		this.preProcess = tokenPreProcessor;
	}*/

}

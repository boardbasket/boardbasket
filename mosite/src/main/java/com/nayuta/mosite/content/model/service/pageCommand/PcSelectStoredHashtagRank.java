package com.nayuta.mosite.content.model.service.pageCommand;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;
import com.nayuta.mosite.content.model.vo.TimeSet;

import net.sf.json.JSONObject;

@Service("pcSelectStoredHashtagRank")
public class PcSelectStoredHashtagRank extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	private final String TAG_RANK_DEFAULT_TIME_MODE = WeekUtil.WEEK;

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		JSONObject result = new JSONObject();
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		result.putAll(selectStoredHashtagRankFunc(searchData, false));
		modelMap.addAttribute("result", result);
	}

	public JSONObject selectStoredHashtagRankFunc(SearchData searchData, boolean includeToday) {
		if (searchData != null) {
			JSONObject result = new JSONObject();

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return result;

			List<TimeSet> timeSetList = null;
			List<Hashtag> hashtagList = null;

			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			try {

				if (searchData.getTimeMode().equals(WeekUtil.YEAR)) {
					// year일때는 한번 month방식으로 파싱한후 week로 파싱해야됨(year는 week값이 없음)
					timeSetList = weekUtil.parseDateBaseToTimeMode(searchData.getWeekList(), WeekUtil.YEAR);
					timeSetList = weekUtil.parseDateBaseToTimeMode(timeSetList, WeekUtil.MONTH);
					timeSetList = weekUtil.parseDateBaseToTimeMode(timeSetList, WeekUtil.WEEK);
				} else if (searchData.getTimeMode().equals(WeekUtil.MONTH)) {
					timeSetList = weekUtil.parseDateBaseToTimeMode(searchData.getWeekList(), WeekUtil.MONTH);
					timeSetList = weekUtil.parseDateBaseToTimeMode(timeSetList, WeekUtil.WEEK);
				} else
					timeSetList = weekUtil.parseDateBaseToTimeMode(searchData.getWeekList(), WeekUtil.WEEK);

				// timeSetList = weekUtil.parseDateBaseToTimeMode(timeSetList,
				// TAG_RANK_DEFAULT_TIME_MODE);

				options.put("keyword", searchData.getKeyword());
				options.put("timeSet", timeSetList);
				options.put("timeMode", TAG_RANK_DEFAULT_TIME_MODE);
				hashtagList = bDao.selectStoredHashtagRank(options);// 어제까지 저장된 순위를 가져옴

				if (includeToday) {
					Map<String, Object> today = new HashMap<>();
					today.put("timeSet", weekUtil.parseDateBaseToTimeMode(LocalDate.now(), WeekUtil.DAY));
					today.put("timeMode", WeekUtil.DAY);
					List<Hashtag> todayHashtagRank = bDao.selectHashtagRank(today, "TODAY");// 캐쉬된 오늘 순위를 가져옴
					// keyword에 포함된 것만 filter
					todayHashtagRank = todayHashtagRank.stream().filter(k -> searchData.getKeyword().contains(k.getTag_name()))
							.collect(Collectors.toList());

					hashtagList.addAll(todayHashtagRank);
				}

				result.put("hashtagRankTimeSet", timeSetList);
				result.put("hashtagRank", com.writeValueAsString(hashtagList));

			} catch (Exception e) {

				return result;
			}

			return result;

		}

		else
			return null;
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import com.nayuta.mosite.content.model.vo.ArticleSearchData;

public interface MakeSearchArticleData {
	public ArticleSearchData makeSearchArticleData();

}

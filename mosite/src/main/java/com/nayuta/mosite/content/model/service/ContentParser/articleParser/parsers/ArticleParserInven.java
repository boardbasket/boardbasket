package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;

@Component("articleParserInven")
public class ArticleParserInven extends ArticleParser {

	private final String lastPageSelector;
	private final Pattern replyNumRegex;

	public ArticleParserInven(@Value("#{selectors['INVEN_ROOT']}") String root,
			@Value("#{selectors['INVEN_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeInven") BoardTypeAbstract typeList,
			@Value("#{selectors['INVEN_PARSER_NAME']}") String parserName,
			@Value("#{selectors['INVEN_CHECK_LAST_PAGE']}") String lastPageSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['INVEN_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['INVEN_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{selectors['INVEN_REPLY_NUM_REGEX']}") String replyNumRegex,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['INVEN_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['INVEN_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);
		this.lastPageSelector = lastPageSelector;
		this.replyNumRegex = Pattern.compile(replyNumRegex);
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		Elements elem = doc.select(lastPageSelector);
		return elem.size() > 0 ? true : false;

	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		return elem.hasClass(noticeCheck.get(boardType));
	}

	@Override
	public int getReplyNum(Element elem, String boardType) {
		if (replyNum.get(boardType) != null) {
			try {
				Element temp = elem.selectFirst(replyNum.get(boardType));

				String replyNumText = temp.text();

				Matcher m = replyNumRegex.matcher(replyNumText);
				if (m.find())
					return Integer.parseInt(m.group());

			} catch (Exception e) {
			}
		}

		return 0;
	}

}

package com.nayuta.mosite.content.model.service.ContentParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;

public class ParserCommon {
	protected Random random = new Random();
	protected Logger log = LogManager.getLogger();

	protected final int TIME_OUT_VAL;

	@Resource(name = "userAgent")
	protected Properties userAgent;

	@Resource(name = "selectors")
	protected Properties selectors;

	protected final String root;

	protected final String siteAddrRegex;

	protected final BoardTypeAbstract typeList;

	protected final String parserName;

	protected final String CHECK_NO_CONTENT;

	protected final String REDIRECT_URL_REGEX;

	protected final int SLEEP_PER_PAGE_MIN;

	protected final int SLEEP_PER_PAGE_MAX;

	protected final int PAGE_LOAD_RETRY_NUM;

	private Pattern redirectUrlPattern;

	@Value("#{serverConst['server_const.article_no_parse_one_cycle_period']}")
	private int ARTICLE_NO_PARSE_ONE_CYCLE_PERIOD;// 모든 게시판의 최신글번호를 파싱하는데 며칠이 걸리는지

	public ParserCommon(String root, String siteAddrRegex, BoardTypeAbstract typeList, String parserName, String timeOut,
			String noContent, String redirectUrlRegex, int sLEEP_PER_PAGE_MIN, int sLEEP_PER_PAGE_MAX, int retryNum) {
		super();
		this.root = root;
		this.siteAddrRegex = siteAddrRegex;
		this.typeList = typeList;
		this.parserName = parserName;
		this.TIME_OUT_VAL = Integer.parseInt(timeOut);
		this.CHECK_NO_CONTENT = noContent;
		this.REDIRECT_URL_REGEX = redirectUrlRegex;
		this.PAGE_LOAD_RETRY_NUM = retryNum;
		if (redirectUrlRegex != null)
			redirectUrlPattern = Pattern.compile(redirectUrlRegex);

		this.SLEEP_PER_PAGE_MIN = sLEEP_PER_PAGE_MIN;
		this.SLEEP_PER_PAGE_MAX = sLEEP_PER_PAGE_MAX;
	}

	protected Map<String, String> boardTypeCheck = new HashMap<>();

	public Document getDocument(String address) throws SiteParseException {

		if (CHECK_NO_CONTENT != null && REDIRECT_URL_REGEX != null)
			return getDocumentFllowRedirect(address);
		else {
			try {
				Document doc = null;
				for (int i = 0; i < 1 + PAGE_LOAD_RETRY_NUM; ++i) {
					// 유저에이전트 중 1개를 랜덤으로 가져옴
					int userAgentId = (int) Math.ceil(1 + random.nextInt(userAgent.size()));
					String _userAgent = (String) userAgent.get("USER_AGENT_" + userAgentId);

					try {
						Thread.sleep(SLEEP_PER_PAGE_MIN + random.nextInt(SLEEP_PER_PAGE_MAX));
						doc = Jsoup.connect(address).timeout(TIME_OUT_VAL).userAgent(_userAgent).get();						
						if (doc == null)
							throw new IOException();
						else
							break;
					} catch (IOException e) {						
						log.warn(address + " 파싱 실패 재시도 합니다...");
						continue;
					}
				}
				if (doc == null) {
					log.error(address + " 페이지를 불러오는데 실패했습니다.");
					throw new SiteParseException(address + " 페이지를 불러오는데 실패했습니다.");
				} else
					return doc;
			} catch (InterruptedException e) {
				throw new SiteParseException(address + " 페이지를 불러오던 도중 InterruptedException 발생");
			}
		}

	}

	private Document getDocumentFllowRedirect(String address) throws SiteParseException {
		try {

			// 유저에이전트 9개 중 1개를 랜덤으로 가져옴
			String _userAgent = (String) userAgent.get("USER_AGENT_" + (random.nextInt(8) + 1));

			Document doc = null;
			Connection.Response response = Jsoup.connect(address).timeout(TIME_OUT_VAL).userAgent(_userAgent)
					.followRedirects(true).execute();

			// response parse 진행
			doc = response.parse();

			// contents 클래스를 가진 element가 있는지 확인하고 없으면 redirect page로 간주
			Element elem = doc.selectFirst(CHECK_NO_CONTENT);

			if (elem == null) {
				String redirectAddr = null;
				// location.href 뒤의 주소를 찾는다.
				Matcher matcher = redirectUrlPattern.matcher(doc.toString());
				while (matcher.find()) {
					redirectAddr = matcher.group();
				}
				if (redirectAddr != null)
					// 찾았으면 document를 가져와서 리턴,없다면 그냥 파싱한 것 리턴
					doc = Jsoup.connect(redirectAddr).timeout(TIME_OUT_VAL).userAgent(_userAgent).get();
			}
			Thread.sleep(SLEEP_PER_PAGE_MIN + random.nextInt(SLEEP_PER_PAGE_MAX));
			return doc;

		} catch (IOException | InterruptedException e) {
			throw new SiteParseException(address + " 페이지를 불러오는데 실패했습니다.");
		}
	}

	public boolean checkParser(String url) {
		Pattern ptn = Pattern.compile(siteAddrRegex);

		Matcher matcher = ptn.matcher(url);

		if (matcher.find())
			return true;
		else
			return false;
	}

	// 게시판이 있는지 확인
	public Boolean detectBoard(Document doc) {

		List<String> _typeList = typeList.getTypeList();

		for (String type : _typeList) {
			Element elem = doc.selectFirst(boardTypeCheck.get(type));
			if (elem != null)
				return true;
		}
		return false;

	}

	// 게시판이 어떤식으로 파싱해야하는지 알려주는 타입을 리턴해주는 함수
	public String detectBoardType(Document doc) {

		for (Map.Entry<String, String> entry : boardTypeCheck.entrySet()) {
			if (doc.selectFirst(entry.getValue()) != null)
				return entry.getKey();

		}
		throw new MVCServiceException("지원하지 않는 형태의 게시판 입니다.");
	}

	public String getRoot() {
		return root;
	}

	public String getSiteAddrRegex() {
		return siteAddrRegex;
	}

	public String httpToHttps(String addr) {

		return addr.replace("http://", "https://");
	}

	public String httpsTohttp(String addr) {

		return addr.replace("https://", "http://");
	}

	public int getARTICLE_NO_PARSE_ONE_CYCLE_PERIOD() {
		return ARTICLE_NO_PARSE_ONE_CYCLE_PERIOD;
	}

}

package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;

@Component("boardParserDcinside")
public class BoardParserDcinside extends BoardParserAbstract {

	protected final String recommend;
	protected final String boardQueryRegex;// 어떤 게시판인지 쿼리스트링을 가져오기 위한 regex
	private final String categoryValueRegex; // 어떤 카테고리인지 쿼리스트링을 가져오기 위한 regex
	private final String dcinsideTitleSelector;

	public BoardParserDcinside(@Value("#{selectors['DCINSIDE_ROOT']}") String root,
			@Value("#{selectors['DCINSIDE_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeDcinside") BoardTypeAbstract typeList,
			@Value("#{selectors['DCINSIDE_PARSER_NAME']}") String parserName,
			@Value("#{selectors['DCINSIDE_RECOMM_CATEGORY']}") String recommend,
			@Value("#{selectors['DCINSIDE_BOARD_QUERY_ID_REGEX']}") String boardQueryRegex,
			@Value("#{selectors['DCINSIDE_CATEGORY_VAL_REGEX']}") String categoryValueRegex,
			@Value("#{selectors['DCINSIDE_TITLE_SELECTOR']}") String dcinsideTitleSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['DCINSIDE_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['DCINSIDE_POPULAR_ADDR']}") String popularAddr,
			@Value("#{selectors['DCINSIDE_NEW_BOARD']}") String newBoard,
			@Value("#{selectors['DCINSIDE_NEW_BOARD_ADDR']}") String newBoardAddr,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, popularBoard, popularAddr, newBoard, newBoardAddr,
				sleepMin, sleepMax, retryNum);
		this.recommend = recommend;
		this.boardQueryRegex = boardQueryRegex;
		this.categoryValueRegex = categoryValueRegex;
		this.dcinsideTitleSelector = dcinsideTitleSelector;
	}

	@PostConstruct
	public void init() {
		initializeParser();

	}

	@Override
	protected boolean isExternalLink(String address) {
		return checkParser(address);
	}

	@Override
	public String urlPreProcess(String addr) {

		String targetAddress = httpToHttps(addr);

		Pattern checkMobile = Pattern.compile(checkMobileRegex);
		Matcher mobileMatcher = checkMobile.matcher(targetAddress);

		// 모바일 주소를 pc 주소로 바꾸는 작업
		if (mobileMatcher.find()) {
			Document doc = getDocument(targetAddress);
			targetAddress = doc.baseUri();
		}

		Pattern findHost = Pattern.compile(urlFilterRegex);
		Matcher hostMatcher = findHost.matcher(targetAddress);

		String host = null;
		String id = null;
		if (hostMatcher.find())
			host = hostMatcher.group();
		else
			return null;

		Pattern findBoardQuery = Pattern.compile(boardQueryRegex);
		Matcher queryMatcher = findBoardQuery.matcher(targetAddress);
		if (queryMatcher.find())
			id = queryMatcher.group();
		else
			return null;

		return host + "?" + id;

	}

	@Override
	public List<BoardCategory> parseCategory(Document doc) {
		List<BoardCategory> bcList = super.parseCategory(doc);
		bcList.add(new BoardCategory("개념글", recommend));
		return bcList;

	}

	@Override
	public BoardCategory parseCategoryFunc(Element elem) {

		String cateName = elem.text();
		String cateVal = "";
		Pattern pattern = Pattern.compile(categoryValueRegex);

		Matcher matcher = pattern.matcher(elem.attr("onclick"));

		if (matcher.find()) {
			cateVal = matcher.group();
			return new BoardCategory(cateName, cateVal);
		} else
			return null;

	}

	public String getIgnoreBoard() {
		return ignoreBoard;
	}

	public String getRecommend() {
		return recommend;
	}

	public String getBoardQueryRegex() {
		return boardQueryRegex;
	}

	public void setIgnoreBoard(String ignoreBoard) {
		this.ignoreBoard = ignoreBoard;
	}

	public String getCategoryValueRegex() {
		return categoryValueRegex;
	}

	@Override
	public String getTitle(Document doc) {
		Element elem = doc.selectFirst(dcinsideTitleSelector);
		String title = elem.ownText();

		if (title == null || title.isEmpty()) {

			title = super.getTitle(doc);
		}

		return title;
	}

}

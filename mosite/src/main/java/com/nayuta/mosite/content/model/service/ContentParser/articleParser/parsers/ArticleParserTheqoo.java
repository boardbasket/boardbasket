package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.vdurmont.emoji.EmojiParser;

@Component("articleParserTheqoo")
public class ArticleParserTheqoo extends ArticleParser {

	protected final String lastPageSelector;
	protected final String UPLOAD_DATE_PATTERN_2;// 연도가 달라서 앞에 연도가 붙은 형식의 날짜

	public ArticleParserTheqoo(@Value("#{selectors['THEQOO_ROOT']}") String root,
			@Value("#{selectors['THEQOO_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeTheqoo") BoardTypeAbstract typeList,
			@Value("#{selectors['THEQOO_PARSER_NAME']}") String parserName,
			@Value("#{selectors['THEQOO_CATEGORY_ADD_ON']}") String categoryAddon,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['THEQOO_CHECK_LAST_PAGE']}") String lastPageSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['THEQOO_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['THEQOO_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['THEQOO_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{selectors['THEQOO_REPLY_NUM_REGEX']}") String replyNumRegexStr,
			@Value("#{selectors['THEQOO_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{selectors['THEQOO_UPLOAD_DATE_REGEX2']}") String uploadDatePattern2,
			@Value("#{new Integer(selectors['THEQOO_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['THEQOO_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);
		this.lastPageSelector = lastPageSelector;
		this.UPLOAD_DATE_PATTERN_2 = uploadDatePattern2;
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {
		return doc.selectFirst(lastPageSelector) != null;
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {
		return elem.hasClass(noticeCheck.get(boardType));
	}

	@Override
	public String getContentAddr(Element elem, String boardType) {
		String addr = super.getContentAddr(elem, boardType);
		addr = addr.substring(1);
		return root + addr;
	}

	@Override
	public String getTitlePreProcess(Element elem, String boardType) {
		String selector = title.get(boardType);
		Element titleElem = selector != null && !selector.isEmpty() ? elem.selectFirst(title.get(boardType)) : null;
		String title = titleElem != null ? EmojiParser.removeAllEmojis(titleElem.text()) : "";

		return title;
	}

	@Override
	public int getViewCnt(Element elem, String boardType) {
		String selector = viewCnt.get(boardType);
		if (!StringUtils.isEmpty(selector)) {
			try {
				String target = elem.select(selector).text();
				int result = 0;
				NumberFormat nf = NumberFormat.getInstance();
				if (target.indexOf("만") > 0) {
					result = (int) (nf.parse(target).floatValue() * 10000);
				} else
					result = nf.parse(target).intValue();

				return result;
			} catch (Exception e) {
				return 0;
			}
		} else
			return 0;

	}

	@Override
	public LocalDate parseUploadDate(String uploadDate) {
		try {
			DateTimeFormatterBuilder fomatterBuilder = new DateTimeFormatterBuilder();
			DateTimeFormatter dtf = fomatterBuilder.appendPattern(UPLOAD_DATE_PATTERN)
					// 현재 연도를 기준으로 파싱함
					.parseDefaulting(ChronoField.YEAR, LocalDate.now().getYear()).toFormatter();

			LocalDate ldt = LocalDate.from(dtf.parse(uploadDate));
			return ldt;

		} catch (Exception e) {
			try {
				DateTimeFormatterBuilder fomatterBuilder = new DateTimeFormatterBuilder();
				DateTimeFormatter dtf = fomatterBuilder.appendPattern(UPLOAD_DATE_PATTERN_2).toFormatter();

				LocalDate ldt = LocalDate.from(dtf.parse(uploadDate));
				return ldt;
			} catch (Exception e2) {
				throw new SiteParseException("날짜값을 파싱하는데 오류가 생겼습니다.");
			}

		}
	}

}

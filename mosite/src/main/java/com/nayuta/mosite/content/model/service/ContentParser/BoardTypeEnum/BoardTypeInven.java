package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import org.springframework.stereotype.Component;

@Component("boardTypeInven")
public class BoardTypeInven extends BoardTypeAbstract {
	public final static String TYPE_GALLERY = "TYPE_GALLERY";

	public BoardTypeInven() {
		typeList.add(TYPE_GALLERY);

	}

}
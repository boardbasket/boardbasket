package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.text.NumberFormat;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;

@Component("articleParserClien")
public class ArticleParserClien extends ArticleParser {

	protected final String lastPageSelector;
	protected final String HOST;

	public ArticleParserClien(@Value("#{selectors['CLIEN_ROOT']}") String root,
			@Value("#{selectors['CLIEN_URL_HOST']}") String host,
			@Value("#{selectors['CLIEN_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeClien") BoardTypeAbstract typeList,
			@Value("#{selectors['CLIEN_PARSER_NAME']}") String parserName,
			@Value("#{selectors['CLIEN_CHECK_LAST_PAGE']}") String lastPageSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['CLIEN_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['CLIEN_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['CLIEN_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['CLIEN_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);

		this.lastPageSelector = lastPageSelector;
		this.HOST = host;
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		return doc.selectFirst(lastPageSelector) == null;
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		return elem.hasClass(noticeCheck.get(boardType));
	}

	@Override
	public String getContentAddr(Element elem, String boardType) {
		String result = super.getContentAddr(elem, boardType);

		if (result.indexOf(HOST) < 0)
			result = HOST + result;
		
		return result;

	}

	@Override
	public String buildAddress(ArticleSearchData asd) throws SiteParseException {
		// 클리앙 page는 0부터 시작함
		asd.setOffset(Math.max(0, asd.getOffset() - 1));
		return super.buildAddress(asd);
	}

	@Override
	public int getViewCnt(Element elem, String boardType) {
		String selector = viewCnt.get(boardType);
		if (!StringUtils.isEmpty(selector)) {
			try {
				String target = elem.select(selector).text();
				int result = 0;
				NumberFormat nf = NumberFormat.getInstance();
				if (target.indexOf("k") > 0 || target.indexOf("K") > 0) {
					result = nf.parse(target).intValue() * 1000;
				} else
					result = nf.parse(target).intValue();

				return result;
			} catch (Exception e) {
				return 0;
			}
		} else
			return 0;

	}

}

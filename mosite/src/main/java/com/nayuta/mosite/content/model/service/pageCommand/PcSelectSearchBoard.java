package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.common.util.SessionAttributeUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.ArticleParserController;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParser;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

//검색결과중 게시판 결과를 리턴하는 함수
@Service("pcSelectSearchBoard")
public class PcSelectSearchBoard extends AbstractPageCls implements CommandInterface {

	@Autowired
	private ArticleParserController apc;

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Autowired
	private SessionAttributeUtil sau;

	private final int BOARD_PER_PAGE_NUM;// 게시판을 페이지당 몇개 표시할 것인지

	private final int COMMENT_LOAD_NUM; // 게시판 댓글을 한번에 몇개 select 할 것인지

	private final int MOBILE_PAGINATION_COUNT_NUM;// 모바일일떄 pagination번호 몇개 표시할건지

	private final int DESKTOP_PAGINATION_COUNT_NUM;// 데스크탑일때 pagination번호 몇개 표시할건지

	public PcSelectSearchBoard(@Value("#{new Integer(serverConst['server_const.comment_load_num'])}") int cOMMENT_LOAD_NUM,
			@Value("#{new Integer(serverConst['server_const.board_per_page_num'])}") int bOARD_PER_PAGE_NUM,
			@Value("#{new Integer(serverConst['server_const.mobile_pagination_count_num'])}") int mOBILE_PAGINATION_COUNT_NUM,
			@Value("#{new Integer(serverConst['server_const.desktop_pagination_count_num'])}") int dESKTOP_PAGINATION_COUNT_NUM) {
		super();
		this.BOARD_PER_PAGE_NUM = bOARD_PER_PAGE_NUM;
		this.COMMENT_LOAD_NUM = cOMMENT_LOAD_NUM;
		this.MOBILE_PAGINATION_COUNT_NUM = mOBILE_PAGINATION_COUNT_NUM;
		this.DESKTOP_PAGINATION_COUNT_NUM = dESKTOP_PAGINATION_COUNT_NUM;
	}

	@Override
	public void execute(ModelMap modelMap) {
		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		modelMap.addAttribute("result", selectBoardFunc(searchData));
	}

	public JSONObject selectBoardFunc(SearchData searchData) {

		JSONObject resultObj = new JSONObject();
		if (searchData != null) {
			Map<String, Object> options = searchData.makeParamMap(weekUtil);
			List<ContentVO> boardList = new ArrayList<>(); // 게시판 리스트
			List<ContentVO> favoriteBoardList = new ArrayList<>();
			options.put("comment_load_num", COMMENT_LOAD_NUM);// 게시판에 댓글을 몇개 로드해 놓을 것인지
			Integer offset = 1;

			if (getMemberNo() != null) {
				options.put("memberNo", getMemberNo());
				favoriteBoardList = bDao.selectMembersAllUserBoard(options);
			}

			try {
				offset = (int) options.get("offset");
			} catch (Exception e) {
				offset = 1;
			}

			options.put("BOARD_PER_PAGE_NUM", BOARD_PER_PAGE_NUM);

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty()) {
				// do nothing
			} else {
				int listCount = bDao.selectMainDivBoardCount(options); // 검색결과의 총 갯수
				int paginationCnt = DESKTOP_PAGINATION_COUNT_NUM;
				if (sau.getAttribute("isMobile") != null ? (boolean) sau.getAttribute("isMobile") : false)
					paginationCnt = MOBILE_PAGINATION_COUNT_NUM;

				PageInfo pageInfo = new PageInfo(offset, listCount, BOARD_PER_PAGE_NUM, paginationCnt);
				options.put("pageInfo", pageInfo);
				boardList = bDao.selectMainDivBoard(options);// 검색한 게시판들
				Map<String, String> board_user = favoriteBoardList.stream()// 게시판코드 - 유저게시판코드 맵
						.collect(Collectors.toMap(ContentVO::getBoard_code, ContentVO::getUser_board_code));

				/*
				 * if (init) boardList = boardList.stream().filter(board -> { // 즐겨찾기에 포함되지 않은
				 * 게시판만 첫 화면에 보여줌 return !board_user.containsKey(board.getBoard_code());
				 * }).limit(BOARD_PER_PAGE_NUM / 2).collect(Collectors.toList());
				 */

				// 게시판 키워드 검색 주소 각각 넣어주기
				boardList.forEach(board -> {
					ArticleParser ap = apc.getParser(board.getBoard_address());
					ArticleSearchData asd = board.makeSearchArticleData();
					asd.setKeyword("___");
					board.setSearchAddress(ap.buildAddress(asd));

					if (board_user.containsKey(board.getBoard_code()))
						board.setUser_board_code(board_user.get(board.getBoard_code()));
				});

				resultObj.put("pageInfo", pageInfo);
			}

			try {
				resultObj.put("favoriteBoardList", com.writeValueAsString(favoriteBoardList));
				resultObj.put("boardList", com.writeValueAsString(boardList));
				return resultObj;
			} catch (JsonProcessingException e) {
				return resultObj;
			}
		} else
			return null;
	}

}

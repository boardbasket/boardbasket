package com.nayuta.mosite.content.model.vo.validatorGroup;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Value;

import com.nayuta.mosite.content.model.vo.SearchData;

public class TimeSetLimitValidator implements ConstraintValidator<SearchDataTimeOptionLimit, SearchData> {

	@Value("#{new Integer(const['const.maxYearOptionNum'])}")
	private int maxYearOptionNum;

	@Value("#{new Integer(const['const.maxMonthOptionNum'])}")
	private int maxMonthOptionNum;

	@Value("#{new Integer(const['const.maxWeekOptionNum'])}")
	private int maxWeekOptionNum;

	@Value("#{new Integer(const['const.maxDayOptionNum'])}")
	private int maxDayOptionNum;

	@Override
	public boolean isValid(SearchData target, ConstraintValidatorContext arg1) {

		if (target.getWeekList() == null || target.getWeekList().size() == 0)
			return false;

		if (target.getTimeMode() == null)
			return false;
		else if (target.getTimeMode().equals("year"))
			return target.getWeekList().size() <= maxYearOptionNum;
		else if (target.getTimeMode().equals("month"))
			return target.getWeekList().size() <= maxMonthOptionNum;
		else if (target.getTimeMode().equals("week"))
			return target.getWeekList().size() <= maxWeekOptionNum;
		else if (target.getTimeMode().equals("day"))
			return (target.getWeekList().size() <= maxDayOptionNum);

		return false;
	}

}

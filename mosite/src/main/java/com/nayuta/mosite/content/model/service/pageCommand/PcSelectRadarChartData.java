package com.nayuta.mosite.content.model.service.pageCommand;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;
import com.nayuta.mosite.content.model.vo.TimeSet;

import net.sf.json.JSONObject;

//검색결과중 hashtag의 정보를 리턴하는 함수

@Service("pcSelectRadarChartData")
public class PcSelectRadarChartData extends AbstractPageCls implements CommandInterface {

	private final String PERSISTENCY_VALUE;
	private final String POPULARITY_VALUE;
	private final String UNIVERSALITY_VALUE;

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	public PcSelectRadarChartData(@Value("#{serverConst['server_const.radar_keyword_persistency']}") String pERSISTENCY_VALUE,
			@Value("#{serverConst['server_const.radar_keyword_popularity']}") String pOPULARITY_VALUE,
			@Value("#{serverConst['server_const.radar_keyword_universality']}") String uNIVERSALITY_VALUE) {
		super();
		PERSISTENCY_VALUE = pERSISTENCY_VALUE;
		POPULARITY_VALUE = pOPULARITY_VALUE;
		UNIVERSALITY_VALUE = uNIVERSALITY_VALUE;
	}

	@Override
	public void execute(ModelMap modelMap) {
		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		modelMap.addAttribute("result", selectTagRadarFunc(searchData, modelMap.containsKey("init")));
	}

	public JSONObject selectTagRadarFunc(SearchData searchData, boolean init) {

		JSONObject resultObj = new JSONObject();
		if (searchData != null) {

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return resultObj;

			Hashtag popularityVal = null;
			Hashtag persistencyVal = null;
			Hashtag universalityVal = null;
			Map<String, Object> options = searchData.makeParamMap(weekUtil);
			options.put("keyword", searchData.getKeyword().subList(0, 1));

			// 1.화제성(얼마나 많이 언급되는지), 지속성(얼마나 오래 언급되는지),OO (얼마나 많은 게시판에서 언급되는지) 삼각 그래프
			if (init) {// init 플래그를 주거나 weekList가 없으면 저장된걸 불러옴
				// 저장된것 불러오기(4주단위)
				popularityVal = linearScalingHashtagVal(bDao.selectStoredHashtagPopularityVal(options));
				persistencyVal = linearScalingHashtagVal(bDao.selectStoredHashtagPersistencyVal(options));
				universalityVal = linearScalingHashtagVal(bDao.selectStoredHashtagUniversalityVal(options));
			} else {
				// 계산해서 불러오기(1주단위)

				Map<String, Object> tempOptions = new HashMap<>();
				TimeSet ts = new TimeSet();
				ts.setFromDate(LocalDate.now().minusDays(7));// 7일 전
				ts.setToDate(LocalDate.now());// 부터 오늘까지
				List<TimeSet> timeSet = Arrays.asList(ts);

				tempOptions.put("keyword", options.get("keyword"));
				tempOptions.put("timeSet", timeSet);
				tempOptions.put("timeMode", WeekUtil.WEEK);

				popularityVal = linearScalingHashtagVal(bDao.selectHashtagPopularityVal(tempOptions));
				persistencyVal = linearScalingHashtagVal(bDao.selectHashtagPersistencyVal(tempOptions));
				universalityVal = linearScalingHashtagVal(bDao.selectHashtagUniversalityVal(tempOptions));
			}

			try {
				JSONObject radarTagList = new JSONObject();

				radarTagList.put(PERSISTENCY_VALUE, com.writeValueAsString(persistencyVal));
				radarTagList.put(POPULARITY_VALUE, com.writeValueAsString(popularityVal));
				radarTagList.put(UNIVERSALITY_VALUE, com.writeValueAsString(universalityVal));
				resultObj.put("labels", Arrays.asList("지속성", "화제성", "보편성"));

				resultObj.put("radarTag", radarTagList);

			} catch (JsonProcessingException e) {
			}

			return resultObj;

		} else
			return null;
	}

	// 태그의 특정 값의 범위를 0~100으로 변환시키는 함수(하나만 가능)
	public Hashtag linearScalingHashtagVal(List<Hashtag> hashtagList) {
		try {
			Hashtag targetTag = hashtagList.stream().reduce((a, b) -> b).get();

			int minVal = targetTag.getMin_val();
			int maxVal = targetTag.getMax_val();
			int targetVal = targetTag.getVal();

			int oldRange = maxVal - minVal;
			int newMin = 0;
			int newMax = 100;
			int newRange = newMax - newMin;

			targetTag.setVal((((targetVal - minVal) * newRange) / oldRange) + newMin);

			return targetTag;
		} catch (Exception e) {
			return null;
		}

	}

}

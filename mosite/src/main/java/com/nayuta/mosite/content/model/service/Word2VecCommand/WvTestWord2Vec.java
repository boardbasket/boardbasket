package com.nayuta.mosite.content.model.service.Word2VecCommand;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openkoreantext.processor.KoreanTokenJava;
import org.openkoreantext.processor.OpenKoreanTextProcessorJava;
import org.openkoreantext.processor.tokenizer.KoreanTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.dao.AdminDaoImpl;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.scheduledTask.ScheduledTask;
import com.nayuta.mosite.common.util.FilteringBadWord;
import com.nayuta.mosite.common.util.KoreanTokenizerUtil;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.service.ContentParser.TagManager;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.ArticleParserController;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParser;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserClien;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserDcinside;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserFmkorea;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserInven;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserPpomppu;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserRuliweb;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParserTheqoo;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserBoardBasket;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserClien;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserDcinside;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserFmkorea;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserInven;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserPpomppu;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserRuliweb;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserTheqoo;
import com.nayuta.mosite.content.model.service.ContentParser.rssParser.RssParser;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.TimeSet;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;
import com.nayuta.mosite.member.model.dao.MemberDaoImpl;

import scala.collection.Seq;

@Service(value = "wvTestWord2Vec")
public class WvTestWord2Vec implements CommandInterface {

	@Autowired
	private FilteringBadWord filter;

	@Autowired
	private BoardDaoImpl bDao;

	@Resource(name = "userAgent")
	protected Properties userAgent;

	@Autowired
	private ArticleParserController apc;

	// private Word2Vec word2vec;

	@Autowired
	private AdminDaoImpl aDao;

	@Autowired
	private MemberDaoImpl mDao;

	@Autowired
	private TagManager tMgr;

	@Autowired
	private WeekUtil weekUtil;

	@Autowired
	private BoardParserController bpc;

	@Autowired
	private BoardParserPpomppu ppdc;

	@Autowired
	private BoardParserDcinside dpdc;

	@Autowired
	private ArticleParserRuliweb apr;

	@Autowired
	private ArticleParserFmkorea apf;

	@Autowired
	private ArticleParserPpomppu appp;

	@Autowired
	private ArticleParserTheqoo apt;

	@Autowired
	private ArticleParserDcinside apdc;

	@Autowired
	private ArticleParserClien apClien;

	@Autowired
	private BoardParserFmkorea bpf;

	@Autowired
	private BoardParserRuliweb bpr;

	@Autowired
	private BoardParserBoardBasket bpbb;

	@Autowired
	private BoardParserTheqoo bpt;

	@Autowired
	private BoardParserInven bpi;

	@Autowired
	private ArticleParserInven api;

	@Autowired
	private ScheduledTask scheduledTask;

	@Autowired
	private RssParser rssParser;

	@Autowired
	private KoreanTokenizerUtil ktu;

	@Autowired
	private BoardParserClien bpClien;

	private Logger log = LogManager.getLogger();

	@PostConstruct
	public void init() {

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		String targetStr = (String) modelMap.get("targetStr");

		if (targetStr.equals("latest")) {
			ContentVO board = bDao.selectBoardWithCode("31455");

			ArticleParser ap = apc.getParser(board.getBoard_address());

			BoardData bd = ap.parseLatestArticleNo(board);
			System.out.println("latestArticleNo : " + bd.getLatest_article_no());
			System.out.println("currentCycle : " + bd.getCurrent_cycle());

		} else if (targetStr.equals("parseTarget")) {

			Site site = bDao.selectSiteWithCode("2");
			List<Site> siteList = new ArrayList<>();
			siteList.add(site);

			List<ContentVO> boardList = bDao.selectArticleNumParseTarget(7, 8, null, siteList);
			System.out.println(boardList);

		} else if (targetStr.equals("commonTarget")) {
			Site site = bDao.selectSiteWithCode("2");
			List<Site> siteList = new ArrayList<>();
			siteList.add(site);

			List<ContentVO> temp = bDao.selectCommonBoardParseTarget(8, siteList);
			System.out.println(temp);

		} else if (targetStr.equals("test")) {

			ContentVO board = bDao.selectBoardWithCode("1495");

			ArticleParser ap = apc.getParser(board.getBoard_address());

			WordFrequencyData wfd = ap.parseWordFrequency(board);

			tMgr.updateWordFrequencyData(wfd);

		} else if (targetStr.equals("now")) {

			LocalDate now = LocalDate.now();
			LocalDate target = LocalDate.of(2022, 6, 13);
			while (target.compareTo(now) <= 0) {

				target = target.plusDays(1);
				Map<String, Object> today = new HashMap<>();
				today.put("timeSet", weekUtil.parseDateBaseToTimeMode(target, WeekUtil.DAY));
				today.put("timeMode", WeekUtil.DAY);
				today.put("limit", 2000);
				bDao.insertHashtagRank(today);

			}

		}
		System.out.println("파싱끝");
	}

}
package com.nayuta.mosite.content.model.vo;

import java.util.ArrayList;
import java.util.List;

import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParser;

public class WordFrequencyData {
	private ContentVO board;
	private List<Hashtag> hashtagList = new ArrayList<>();
	private List<BoardArticle> articleList = new ArrayList<>();
	private ArticleParser articleParser;
	private BoardData boardData;

	public ArticleParser getArticleParser() {
		return articleParser;
	}

	public void setArticleParser(ArticleParser articleParser) {
		this.articleParser = articleParser;
	}

	public BoardData getBoardData() {
		return boardData;
	}

	public void setBoardData(BoardData boardData) {
		this.boardData = boardData;
	}

	public List<Hashtag> getHashtagList() {
		return hashtagList;
	}

	public List<BoardArticle> getArticleList() {
		return articleList;
	}

	public void setHashtagList(List<Hashtag> hashtagList) {
		this.hashtagList = hashtagList;
	}

	public void setArticleList(List<BoardArticle> articleList) {
		this.articleList = articleList;
	}

	public ContentVO getBoard() {
		return board;
	}

	public void setBoard(ContentVO board) {
		this.board = board;
	}

	public WordFrequencyData(List<Hashtag> hashtagList, BoardData boardData) {
		super();
		this.hashtagList = hashtagList;
		this.boardData = boardData;
	}

	public WordFrequencyData() {
		super();
	}

}

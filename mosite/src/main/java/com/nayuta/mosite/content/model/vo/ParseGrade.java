package com.nayuta.mosite.content.model.vo;

public class ParseGrade {

	private Integer grade;
	private Float ratio_min;
	private Float ratio_max;
	private Integer parse_cycle_day;
	private Integer parse_target;

	public Integer getGrade() {
		return grade;
	}

	public Float getRatio_min() {
		return ratio_min;
	}

	public Float getRatio_max() {
		return ratio_max;
	}

	public Integer getParse_cycle_day() {
		return parse_cycle_day;
	}

	public Integer getParse_target() {
		return parse_target;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public void setRatio_min(Float ratio_min) {
		this.ratio_min = ratio_min;
	}

	public void setRatio_max(Float ratio_max) {
		this.ratio_max = ratio_max;
	}

	public void setParse_cycle_day(Integer parse_cycle_day) {
		this.parse_cycle_day = parse_cycle_day;
	}

	public void setParse_target(Integer parse_target) {
		this.parse_target = parse_target;
	}

}

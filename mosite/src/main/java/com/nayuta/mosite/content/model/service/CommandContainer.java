package com.nayuta.mosite.content.model.service;

import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

@Component
public class CommandContainer {

	private CommandInterface command;

	public CommandInterface getCommand() {
		return command;
	}

	public void setCommand(CommandInterface command) {
		this.command = command;
	}

	public void execute(CommandInterface command, ModelMap modelMap) {

		this.command = command;

		command.execute(modelMap);
	}

}

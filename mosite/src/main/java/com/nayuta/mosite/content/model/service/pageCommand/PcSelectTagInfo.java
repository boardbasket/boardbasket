package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service("pcSelectTagInfo")
public class PcSelectTagInfo extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		JSONObject result = new JSONObject();
		result.putAll(selectTagInfoFunc(searchData));
		modelMap.addAttribute("result", result);

	}

	public JSONObject selectTagInfoFunc(SearchData searchData) {

		JSONObject result = new JSONObject();
		if (searchData != null) {

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return result;

			List<Hashtag> firstAndLast = null;

			List<Hashtag> mostMentionedDate = null;

			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			options.put("keyword", searchData.getKeyword());

			firstAndLast = bDao.selectFirstAndLastRegisteredDateOfTag(options);

			firstAndLast.forEach(k -> {
				if (k != null)
					k.applyTimeSet(weekUtil.getWeekNum(k.getInsert_date()));
			});
			mostMentionedDate = bDao.selectMostFrequentyMentionedDay(options);
			mostMentionedDate.forEach(k -> {
				if (k != null)
					k.applyTimeSet(weekUtil.getWeekNum(k.getInsert_date()));
			});

			try {

				result.put("mostMentionedDate", com.writeValueAsString(mostMentionedDate));
				result.put("firstAndLast", com.writeValueAsString(firstAndLast));

			} catch (Exception e) {

				return result;
			}

			return result;
		} else
			return null;
	}

}

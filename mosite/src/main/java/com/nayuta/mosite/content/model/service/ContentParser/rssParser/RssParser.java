package com.nayuta.mosite.content.model.service.ContentParser.rssParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.common.util.KoreanTokenizerUtil;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;
import com.vdurmont.emoji.EmojiParser;

@Component
public class RssParser {
	protected static Logger log = LogManager.getLogger();

	@Autowired
	private KoreanTokenizerUtil koreanTokenizerUtil;

	@Resource(name = "userAgent")
	protected Properties userAgent;

	protected Random random = new Random();

	private static final String ITEM_SELECTOR = "item";

	private static final String TITLE_SELECTOR = "title";

	private static final String LINK_SELECTOR = "link";

	private static final String DESC_SELECTOR = "description";

	protected final int SLEEP_PER_PAGE_MIN;

	protected final int SLEEP_PER_PAGE_MAX;

	protected final int PAGE_LOAD_RETRY_NUM;

	private final Pattern filterPattern;

	public RssParser(@Value("#{selectors['DEFAULT_SLEEP_PER_REQUEST_MIN']}") int sLEEP_PER_PAGE_MIN,
			@Value("#{selectors['DEFAULT_SLEEP_PER_REQUEST_MAX']}") int sLEEP_PER_PAGE_MAX,
			@Value("#{selectors['DEFAULT_PAGE_LOAD_RETRY_NUM']}") int pAGE_LOAD_RETRY_NUM,
			@Value("#{wordList['wordList.news_filtering_regex']}") String filterPattern) {
		super();
		this.filterPattern = Pattern.compile(filterPattern);
		SLEEP_PER_PAGE_MIN = sLEEP_PER_PAGE_MIN;
		SLEEP_PER_PAGE_MAX = sLEEP_PER_PAGE_MAX;
		PAGE_LOAD_RETRY_NUM = pAGE_LOAD_RETRY_NUM;

	}

	public Document getDocument(String address) {
		Document doc = null;
		try {

			for (int i = 0; i < 1 + PAGE_LOAD_RETRY_NUM; ++i) {
				// 유저에이전트 중 1개를 랜덤으로 가져옴
				int userAgentId = (int) Math.ceil(1 + random.nextInt(userAgent.size()));
				String _userAgent = (String) userAgent.get("USER_AGENT_" + userAgentId);

				try {
					Thread.sleep(SLEEP_PER_PAGE_MIN + random.nextInt(SLEEP_PER_PAGE_MAX));
					doc = Jsoup.connect(address).userAgent(_userAgent).get();
					if (doc == null)
						throw new IOException();
					else
						break;
				} catch (IOException e) {
					log.warn(address + " 파싱 실패 재시도 합니다...");
					continue;
				}
			}
			if (doc == null) {
				log.error(address + " 페이지를 불러오는데 실패했습니다.");
				throw new SiteParseException(address + " 페이지를 불러오는데 실패했습니다.");
			} else
				return doc;
		} catch (InterruptedException e) {
			throw new SiteParseException(address + " 페이지를 불러오던 도중 InterruptedException 발생");
		}

	}

	public String getXMLString(String address) {

		Document doc = getDocument(address);
		log.debug(address + " document 가져옴");

		return doc.toString();

	}

	public Elements getItem(Document doc) {

		return doc.select(ITEM_SELECTOR);

	}

	public Element getTitle(Element elem) {

		return elem.selectFirst(TITLE_SELECTOR);

	}

	public String getTitleText(Element elem) {

		return getTitle(elem).text();
	}

	public Element getLink(Element elem) {

		return elem.selectFirst(LINK_SELECTOR);

	}

	public String getLinkAddress(Element elem) {
		return getLink(elem).text();
	}

	public Elements getElement(Document doc, String selector) {

		return doc.select(selector);

	}

	public Element getDesc(Element elem) {
		return elem.selectFirst(DESC_SELECTOR);
	}

	public String getDescText(Element elem) {
		return getDesc(elem).text();
	}

	public List<WordFrequencyData> parseWordFrequencyData(List<ContentVO> boardList) {

		List<WordFrequencyData> result = new ArrayList<>();

		boardList.forEach(board -> {
			WordFrequencyData wfd = new WordFrequencyData();
			try {

				List<BoardArticle> articleList = getRssArticle(board);
				//System.out.println(board.getBoard_address() + "파싱중...");
				articleList.forEach(article -> article.setIsNotice("N"));
				List<Hashtag> hashtagList = koreanTokenizerUtil.tokenizeArticle(board, articleList, articleList.size(), 3, true);

				wfd.setArticleList(articleList.stream().map(k -> {
					k.setTitle(EmojiParser.removeAllEmojis(k.getTitle()));
					return k;
				}).filter(article -> {
					for (int i = 0; i < hashtagList.size(); ++i) {
						if (article.getTitle().indexOf(hashtagList.get(i).getTag_name()) >= 0)
							return true;
					}
					return false;
				}).collect(Collectors.toList()));

				wfd.setHashtagList(hashtagList);
				result.add(wfd);
			} catch (Exception e) {
				log.error("rss " + board.getBoard_address() + " 파싱중 오류 발생");
			}

		});

		return result;
	}

	public List<BoardArticle> getRssArticle(ContentVO board) {
		Document doc = getDocument(board.getBoard_address());
		List<BoardArticle> rssArticleList = new ArrayList<>();
		if (doc != null) {
			Elements elems = getItem(doc);
			// 기사 만들기
			List<BoardArticle> temp = elems.stream().map(elem -> {
				BoardArticle ba = new BoardArticle();
				ba.setTitle(getTitleText(elem));
				ba.setBoard_code(board.getBoard_code());
				ba.setContent_address(getLinkAddress(elem));
				ba.setContent(getDescText(elem));
				return ba;
			}).filter(article -> {
				// 기사 제목에 특정 단어,패턴이 있으면 걸러내기
				String title = article.getTitle();

				Matcher m = filterPattern.matcher(title);
				while (m.find())
					return false;

				return true;

			}).collect(Collectors.toList());

			rssArticleList.addAll(temp);
		}

		return rssArticleList;
	}

	// 기사를 파싱해서 기사 리스트를 리턴하는 함수
	public List<BoardArticle> parseRss(List<ContentVO> boardList) {

		List<BoardArticle> rssArticleList = new ArrayList<>();
		boardList.forEach(board -> {
			List<BoardArticle> boardArticle = getRssArticle(board);

			rssArticleList.addAll(boardArticle);

		});
		return rssArticleList;
	}

}

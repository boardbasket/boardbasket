package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeRuliweb;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Component("articleParserRuliweb")
public class ArticleParserRuliweb extends ArticleParser {

	private final String typeNewsViewCntPattern;

	private final String typeNewsUploadDatePattern;

	private final String typeNewsReplyNumPattern;

	private final Pattern titleCategoryPattern; // 게시글 제목의 카테고리를 지우는 패턴

	private final String lastPageSelector;

	private final String bestCheck;// best 라고 일반 게시판에 공지처럼 항상 떠있는 글들이 있음

	public ArticleParserRuliweb(@Value("#{selectors['RULIWEB_ROOT']}") String root,
			@Value("#{selectors['RULIWEB_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeRuliweb") BoardTypeAbstract typeList,
			@Value("#{selectors['RULIWEB_PARSER_NAME']}") String parserName,
			@Value("#{selectors['RULIWEB_TYPE_NEWS_VIEW_CNT_PATTERN']}") String typeNewsViewCntPattern,
			@Value("#{selectors['RULIWEB_TYPE_NEWS_UPLOAD_DATE_PATTERN']}") String typeNewsUploadDatePattern,
			@Value("#{selectors['RULIWEB_TYPE_NEWS_REPLY_NUM_PATTERN']}") String typeNewsReplyNumPattern,
			@Value("#{selectors['RULIWEB_CHECK_LAST_PAGE']}") String lastPageSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['RULIWEB_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['RULIWEB_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{selectors['RULIWEB_TITLE_CATEGORY_REGEX']}") String titleCategoryPattern,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['RULIWEB_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['RULIWEB_TYPE_DEFAULT_BEST_CHECK']}") String bestCheck,
			@Value("#{selectors['RULIWEB_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, null, null, articleNoRegex,
				requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum, uploadDatePattern);
		this.typeNewsViewCntPattern = typeNewsViewCntPattern;
		this.typeNewsUploadDatePattern = typeNewsUploadDatePattern;
		this.typeNewsReplyNumPattern = typeNewsReplyNumPattern;
		this.lastPageSelector = lastPageSelector;
		this.bestCheck = bestCheck;
		this.titleCategoryPattern = Pattern.compile(titleCategoryPattern);
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public List<BoardArticle> defaultParseFunc(Document doc, ArticleSearchData asd) {
		String boardType = asd.getBoard_type();
		Elements articles = getArticles(doc, boardType);
		ArrayList<BoardArticle> baArr = new ArrayList<BoardArticle>();

		for (Element elem : articles) {

			BoardArticle ba = new BoardArticle();
			try {
				boolean isNotice = checkIsNotice(elem, boardType);
				boolean isBest = elem.hasClass(bestCheck);
				ba.setContent_address(getContentAddr(elem, boardType));
				if (ba.getContent_address() == null || ba.getContent_address().isEmpty())
					continue;
				ba.setTitle(getTitle(elem, boardType));
				ba.setView_cnt(getViewCnt(elem, boardType));
				ba.setArticle_no(getArticleNo(elem, boardType));
				ba.setUpload_date(getUploadDate(elem, boardType));
				ba.setIsNotice(isNotice || isBest ? "Y" : "N");
				ba.setReplyNum(getReplyNum(elem, boardType));
				ba.setRecommend(getRecommend(elem, boardType));
				baArr.add(ba);
			} catch (Exception e) {

				continue;
			}

		}

		return baArr;
	}

	@Override
	public String getTitle(Element elem, String boardType) {
		String title = super.getTitle(elem, boardType);
		Matcher matcher = titleCategoryPattern.matcher(title);

		return matcher.replaceFirst("");
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		if (!asd.getBoard_type().equals(BoardTypeRuliweb.TYPE_NEWS)) {
			Elements elem = doc.select(lastPageSelector);
			return elem.size() > 0 ? true : false;
		}
		return false;

	}

	@Override
	public String buildAddress(ArticleSearchData asd) {

		// type news 일때는 searchtype이 다름
		// type_news의 제목 쿼리 = title
		// type_default의 제목 쿼리 =subject
		// 그래서 제목+내용의 all로 모두 바꿔서 검색
		if (asd.getBoard_type() != null && asd.getBoard_type().equals(BoardTypeRuliweb.TYPE_NEWS)) {

			String keyword = makeKeywordQuery(asd.getKeyword());
			String searchType = "";
			if (!keyword.isEmpty())
				searchType = makeSearchTypeQuery("title");
			String pagination = makePaginationQuery(asd.getOffset());
			// String category = makeCategoryQuery(asd.getCategory_query());
			String query = keyword + searchType + pagination;

			String boardAddress = asd.getBoard_address();

			if (boardAddress.indexOf("?") < 0)
				query = query.replaceFirst("&", "?");

			String fullAddress = boardAddress + query;

			return fullAddress;

		} else
			return super.buildAddress(asd);
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		return elem.hasClass(noticeCheck.get(boardType));
	}

	@Override
	public int getViewCnt(Element elem, String boardType) {

		if (BoardTypeRuliweb.TYPE_NEWS.equals(boardType)) {
			String target = "";
			String text = elem.text();

			Pattern pattern = Pattern.compile(typeNewsViewCntPattern);

			Matcher matcher = pattern.matcher(text);

			while (matcher.find())
				target = matcher.group();

			try {
				return Integer.parseInt(target);
			} catch (Exception e) {
				return 0;
			}
		} else
			return super.getViewCnt(elem, boardType);
	}

	@Override
	public String getUploadDate(Element elem, String boardType) {

		if (BoardTypeRuliweb.TYPE_NEWS.equals(boardType)) {

			String text = elem.text();

			Pattern pattern = Pattern.compile(typeNewsUploadDatePattern);

			Matcher matcher = pattern.matcher(text);

			while (matcher.find())
				return matcher.group();

			return null;
		} else
			return super.getUploadDate(elem, boardType);
	}

	@Override
	public int getReplyNum(Element elem, String boardType) {

		if (BoardTypeRuliweb.TYPE_NEWS.equals(boardType)) {
			String target = "";
			String text = elem.text();

			Pattern pattern = Pattern.compile(typeNewsReplyNumPattern);

			Matcher matcher = pattern.matcher(text);

			while (matcher.find())
				target = matcher.group();

			try {
				return Integer.parseInt(target);
			} catch (Exception e) {
				return 0;
			}

		} else
			return super.getReplyNum(elem, boardType);

	}

	@Override
	public String getContentAddr(Element elem, String boardType) {
		String addr = super.getContentAddr(elem, boardType);

		int index = addr.indexOf("?");

		if (index > 0)
			addr = addr.substring(0, index);
		
		return addr;
	}

	@Override
	public LocalDate parseUploadDate(String uploadDate) {
		try {

			DateTimeFormatterBuilder fomatterBuilder = new DateTimeFormatterBuilder();
			DateTimeFormatter dtf = fomatterBuilder.appendPattern(UPLOAD_DATE_PATTERN).toFormatter();

			LocalDate ldt = LocalDate.from(dtf.parse(uploadDate));
			return ldt;

		} catch (Exception e) {
			throw new SiteParseException("날짜값을 파싱하는데 오류가 생겼습니다.");
		}
	}

	@Override
	public long getArticleNo(Element elem, String boardType) {
		return super.getArticleNo(elem, boardType);
	}

}

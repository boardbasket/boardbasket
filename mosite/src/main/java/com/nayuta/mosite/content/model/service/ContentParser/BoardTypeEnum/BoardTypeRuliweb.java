package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import org.springframework.stereotype.Component;

@Component("boardTypeRuliweb")
public class BoardTypeRuliweb extends BoardTypeAbstract {
	public final static String TYPE_GALLERY = "TYPE_GALLERY";
	public final static String TYPE_NEWS = "TYPE_NEWS";

	public BoardTypeRuliweb() {
		typeList.add(TYPE_GALLERY);
		typeList.add(TYPE_NEWS);
	}

}
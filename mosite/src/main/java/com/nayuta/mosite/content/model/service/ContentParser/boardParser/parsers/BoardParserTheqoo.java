package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;

@Component(value = "boardParserTheqoo")
public class BoardParserTheqoo extends BoardParserAbstract {

	protected final String titleSelector;
	protected final Pattern CATEGORY_PATTERN;// 카테고리 주소에서 카테고리 쿼리만 따로 분류하는 정규식
	protected final Pattern QUERY_BASE_URL;
	protected final Pattern CHECK_REST_URL;

	@PostConstruct
	public void init() {
		initializeParser();
	}

	public BoardParserTheqoo(@Value("#{selectors['THEQOO_ROOT']}") String root,
			@Value("#{selectors['THEQOO_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeTheqoo") BoardTypeAbstract typeList,
			@Value("#{selectors['THEQOO_PARSER_NAME']}") String parserName,
			@Value("#{selectors['THEQOO_TITLE_SELECTOR']}") String titleSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['THEQOO_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['THEQOO_POPULAR_ADDR']}") String popularAddr,
			@Value("#{selectors['THEQOO_NEW_BOARD']}") String newBoard,
			@Value("#{selectors['THEQOO_NEW_BOARD_ADDR']}") String newBoardAddr,
			@Value("#{selectors['THEQOO_CHECK_REST_URL']}") String checkRESTUrl,
			@Value("#{selectors['THEQOO_CATEGORY_REGEX']}") String categoryRegex,
			@Value("#{selectors['THEQOO_QUERY_BASE_URL']}") String queryBaseUrl,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, popularBoard, popularAddr, newBoard, newBoardAddr,
				sleepMin, sleepMax, retryNum);
		this.titleSelector = titleSelector;
		this.CATEGORY_PATTERN = Pattern.compile(categoryRegex);
		this.QUERY_BASE_URL = Pattern.compile(queryBaseUrl);
		this.CHECK_REST_URL = Pattern.compile(checkRESTUrl);
	}

	@Override
	public String urlPreProcess(String addr) {

		if (addr.indexOf(root) < 0) {
			addr = root + addr.replace("/", "");
		}

		System.out.println(1);
		String address = httpToHttps(addr);

		Matcher restMatcher = CHECK_REST_URL.matcher(address);
		System.out.println(2);
		if (restMatcher.find()) {
			return QUERY_BASE_URL + restMatcher.group();
		} else {
			Pattern pattern = Pattern.compile(urlFilterRegex);
			Matcher filteredUrl = pattern.matcher(address);

			if (filteredUrl.find())
				return filteredUrl.group();
		}

		return null;

	}

	@Override
	public String getTitle(Document doc) {
		Element elem = doc.selectFirst(titleSelector);

		String title = elem.text();

		return title;
	}

	@Override
	public List<BoardCategory> parseCategory(Document doc) {
		List<BoardCategory> result = new ArrayList<BoardCategory>();

		Elements elems = doc.select(category);

		elems.stream().forEach(elem -> {
			BoardCategory bc = parseCategoryFunc(elem);

			if (bc != null) {
				if (bc.getCategory_name() != null) {
					// db에 특문 안들어가서 특문 제거
					String filteredCategory = bc.getCategory_name().replaceAll("[^ 가-힣ㄱ-ㅎa-zA-Z0-9]", "");
					if (!filteredCategory.isEmpty()) {
						bc.setCategory_name(filteredCategory);

						Matcher matcher = CATEGORY_PATTERN.matcher(bc.getCategory_address());
						if (matcher.find()) {
							bc.setCategory_address(matcher.group());
							result.add(bc);
						}
					}
				}
			}

		});

		return result;

	}

	public List<String> getInitialBoard() {

		List<String> result = new ArrayList<>();

		try {
			Document doc = getDocument(newBoardAddr);
			Elements elems = doc.select(newBoard);

			elems.stream().filter(k -> !k.hasClass("menu-promo")).forEach(k -> {
				result.add("https://www.clien.net" + k.attr("href"));
			});

		} catch (Exception e) {
		}
		return result;
	}

	@Override
	public List<String> getNewBoardAddrList() {
		
		return new ArrayList<String>();
	}
	
	

}

package com.nayuta.mosite.content.model.service.pageCommand;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardComment;
import com.vdurmont.emoji.EmojiParser;

import net.sf.json.JSONObject;

@Service("pcInsertBoardComment")
@Transactional(rollbackFor = Exception.class)
public class PcInsertBoardComment extends AbstractPageCls implements CommandInterface {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		BoardComment bc = (BoardComment) modelMap.get("boardComment");
		int status = -1;
		bc.setMember_no(getMemberNo());
		if (bc.getUser_comment() != null)
			bc.setUser_comment(EmojiParser.removeAllEmojis(bc.getUser_comment()));

		if ((bc.getComment_eval_list() == null || bc.getComment_eval_list().size() <= 0)
				&& StringUtils.isEmpty(bc.getUser_comment()))
			throw new MVCServiceException("내용이 있어야 합니다.");

		if (bDao.selectMyBoardCommentExist(bc) > 0) {

			status = bDao.updateBoardComment(bc);
			status = bDao.deleteMembersBoardCommentEval(bc);
			status = bDao.insertCommentEval(bc.getComment_eval_list());
		} else {

			status = bDao.insertBoardComment(bc);
			resultObj.put("comment_code", bc.getComment_code());
		}

		resultObj.put("status", status);
		modelMap.put("result", resultObj);

	}
}

package com.nayuta.mosite.content.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nayuta.mosite.content.model.service.CommandContainer;
import com.nayuta.mosite.content.model.service.pageCommand.PcDeleteBoardComment;
import com.nayuta.mosite.content.model.service.pageCommand.PcDeleteUserBoard;
import com.nayuta.mosite.content.model.service.pageCommand.PcInsertBoardComment;
import com.nayuta.mosite.content.model.service.pageCommand.PcInsertBoardCommentVote;
import com.nayuta.mosite.content.model.service.pageCommand.PcInsertUserBoard;
import com.nayuta.mosite.content.model.service.pageCommand.PcInsertUserHashtag;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectAutoCompleteResult;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectBoardComment;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectBoardContent;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectBoardEvalCount;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectHashtag;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectHashtagHeatMap;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectMainDivBoard;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectNewsCommunityContent;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectPercentageOfBoard;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectRadarChartData;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectRelativeTag;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectStoredHashtagRank;
import com.nayuta.mosite.content.model.service.pageCommand.PcSelectTagInfo;
import com.nayuta.mosite.content.model.vo.BoardComment;
import com.nayuta.mosite.content.model.vo.SearchData;
import com.nayuta.mosite.content.model.vo.validatorGroup.InsertGroup;

/*
 * 새로 만들어진 controller 
 * 
 * 
 * */

@Controller
@Validated
public class MainController {

	@Autowired
	private CommandContainer cController;

	@Autowired
	@Qualifier("pcSelectMainDivBoard")
	private PcSelectMainDivBoard pcSelectMainDivBoard;

	@Autowired
	@Qualifier("pcSelectHashtag")
	private PcSelectHashtag pcSelectHashtag;

	@Autowired
	@Qualifier("pcInsertBoardComment")
	private PcInsertBoardComment pcInsertBoardComment;
	
	@Autowired
	@Qualifier("pcDeleteBoardComment")
	private PcDeleteBoardComment pcDeleteBoardComment;

	@Autowired
	@Qualifier("pcInsertBoardCommentVote")
	private PcInsertBoardCommentVote pcInsertBoardCommentVote;

	@Autowired
	@Qualifier("pcSelectBoardEvalCount")
	private PcSelectBoardEvalCount pcSelectBoardEvalCount;

	@Autowired
	@Qualifier("pcSelectBoardComment")
	private PcSelectBoardComment pcSelectBoardComment;

	@Autowired
	@Qualifier("pcSelectNewsCommunityContent")
	private PcSelectNewsCommunityContent pcSelectNewsCommunityContent;

	@Autowired
	@Qualifier("pcSelectBoardContent")
	private PcSelectBoardContent pcSelectBoardContent;

	@Autowired
	@Qualifier("pcInsertUserBoard")
	private PcInsertUserBoard pcInsertUserBoard;

	@Autowired
	@Qualifier("pcDeleteUserBoard")
	private PcDeleteUserBoard pcDeleteUserBoard;

	@Autowired
	@Qualifier("pcSelectAutoCompleteResult")
	private PcSelectAutoCompleteResult pcSelectAutoCompleteResult;

	@Autowired
	@Qualifier("pcInsertUserHashtag")
	private PcInsertUserHashtag pcInsertUserHashtag;

	@Autowired
	@Qualifier("pcSelectHashtagHeatMap")
	private PcSelectHashtagHeatMap pcSelectHashtagHeatMap;

	@Autowired
	@Qualifier("pcSelectRadarChartData")
	private PcSelectRadarChartData pcSelectRadarChartData;

	@Autowired
	@Qualifier("pcSelectRelativeTag")
	private PcSelectRelativeTag pcSelectRelativeTag;

	@Autowired
	@Qualifier("pcSelectTagInfo")
	private PcSelectTagInfo pcSelectTagInfo;

	@Autowired
	@Qualifier("pcSelectPercentageOfBoard")
	private PcSelectPercentageOfBoard pcSelectPercentageOfBoard;

	@Autowired
	@Qualifier("pcSelectStoredHashtagRank")
	private PcSelectStoredHashtagRank pcSelectStoredHashtagRank;

	private Map<String, Object> result = new HashMap<>();

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String setTabBoards(ModelMap modelMap, HttpSession session, Authentication auth, Device device, Principal principal) {

		session.removeAttribute("prevPage");

		cController.execute(pcSelectMainDivBoard, modelMap);

		// return "main/testMain";
		return "main/newMain";
	}

	@RequestMapping(value = { "/getHashtag.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getHashtag(ModelMap modelMap, @Validated @ModelAttribute("searchData") SearchData searchData,
			BindingResult br) {

		cController.execute(pcSelectHashtag, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/getBoardContent.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getBoardContent(ModelMap modelMap, @Validated @ModelAttribute("searchData") SearchData searchData,
			BindingResult br) {

		cController.execute(pcSelectBoardContent, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/insertBoardComment.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertBoardComment(ModelMap modelMap,
			@RequestBody @Validated({ InsertGroup.class }) BoardComment bc, BindingResult br) {

		modelMap.put("boardComment", bc);
		cController.execute(pcInsertBoardComment, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/deleteBoardComment.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteBoardComment(ModelMap modelMap, @ModelAttribute("boardComment")BoardComment bc, BindingResult br) {
		
		cController.execute(pcDeleteBoardComment, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/insertBoardCommentVote.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> pcInsertBoardCommentVote(ModelMap modelMap, @ModelAttribute("boardComment") BoardComment bc) {

		cController.execute(pcInsertBoardCommentVote, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/selectBoardEvalCount.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> pcSelectBoardEvalCount(ModelMap modelMap, @RequestParam("board_code") String boardCode) {
		modelMap.put("board_code", boardCode);

		cController.execute(pcSelectBoardEvalCount, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/selectBoardComment.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectBoardCommentAjax(ModelMap modelMap, @ModelAttribute("board_code") String boardCode,
			@Min(value = 0) @ModelAttribute("offset") Integer offset, @ModelAttribute("insert_date") String insert_date,
			@ModelAttribute("up_vote") String up_vote, @ModelAttribute("down_vote") String down_vote) {

		cController.execute(pcSelectBoardComment, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/selectNewsCommunityContent.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectNewsCommunityContentAjax(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData) {

		cController.execute(pcSelectNewsCommunityContent, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/search", "/search/{searchTarget}" }, method = RequestMethod.GET)
	public String searchBoard(ModelMap modelMap, @PathVariable(required = false, name = "searchTarget") String searchTarget,
			@Validated @ModelAttribute("searchData") SearchData searchData, Device device, HttpSession session) {

		modelMap.addAttribute("searchTarget", searchTarget);
		modelMap.addAttribute("autoComplete", false);
		cController.execute(pcSelectMainDivBoard, modelMap);

		return "main/newMain";
	}

	@RequestMapping(value = "/insertUserBoard", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertUserBoard(ModelMap modelMap, @NotEmpty @ModelAttribute("board_code") String boardCode,
			@ModelAttribute("user_board_code") String userBoardCode) {

		cController.execute(pcInsertUserBoard, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/deleteUserBoard.ajax", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteUserBoard(ModelMap modelMap,
			@NotEmpty @ModelAttribute("user_board_code") String userBoardCode) {

		cController.execute(pcDeleteUserBoard, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/insertUserHashtag.ajax", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertUserHashtag(ModelMap modelMap, @NotEmpty @ModelAttribute("board_code") String board_code,
			@NotEmpty @ModelAttribute("tag_name") String tag_name) {

		cController.execute(pcInsertUserHashtag, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/selectSearchTag.ajax", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectSearchTag(ModelMap modelMap, @ModelAttribute("searchData") SearchData searchData) {

		cController.execute(pcSelectAutoCompleteResult, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/selectTagHeatMapData.ajax", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectTagHeatMapData(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData) {

		cController.execute(pcSelectHashtagHeatMap, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/getBaseRadarChartData.ajax", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectBaseRadarChartData(ModelMap modelMap, @ModelAttribute("searchData") SearchData searchData) {

		modelMap.addAttribute("init");
		cController.execute(pcSelectRadarChartData, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/getRadarChartData.ajax", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectRadarChartData(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData) {

		cController.execute(pcSelectRadarChartData, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = "/selectRelativeTag.ajax", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectRelativeTag(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData) {

		cController.execute(pcSelectRelativeTag, modelMap);

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/getBoardPercentageChartData.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectPercentageOfBoard(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData, BindingResult br) {

		cController.execute(pcSelectPercentageOfBoard, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/getTagInfo.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectTagInfo(ModelMap modelMap, @Validated @ModelAttribute("searchData") SearchData searchData,
			BindingResult br) {

		cController.execute(pcSelectTagInfo, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

	@RequestMapping(value = { "/getHashtagRankData.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectStoredHashtagRank(ModelMap modelMap,
			@Validated @ModelAttribute("searchData") SearchData searchData, BindingResult br) {

		cController.execute(pcSelectStoredHashtagRank, modelMap);

		result.put("result", modelMap.get("result"));

		return result;

	}

}
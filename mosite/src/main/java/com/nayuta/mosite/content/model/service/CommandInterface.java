package com.nayuta.mosite.content.model.service;

import org.springframework.ui.ModelMap;
  

public interface CommandInterface {
	
	public void execute(ModelMap modelMap);
	
}

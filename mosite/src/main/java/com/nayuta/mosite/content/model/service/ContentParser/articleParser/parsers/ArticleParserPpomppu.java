package com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypePpomppu;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.vdurmont.emoji.EmojiParser;

@Component("articleParserPpomppu")
public class ArticleParserPpomppu extends ArticleParser {

	private final String addressPrefix;
	private final Pattern addrPattern;
	private final Pattern recommendPattern;
	private final String checkLastPage;
	private final String checkDefaultTypeAD;

	public ArticleParserPpomppu(@Value("#{selectors['PPOMPPU_ROOT']}") String root,
			@Value("#{selectors['PPOMPPU_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypePpomppu") BoardTypeAbstract typeList,
			@Value("#{selectors['PPOMPPU_PARSER_NAME']}") String parserName,
			@Value("#{selectors['PPOMPPU_ADDRESS_PREFIX']}") String addressPrefix,
			@Value("#{selectors['PPOMPPU_CONTENT_ADDRESS_REGEX']}") String contentAddressRegex,
			@Value("#{selectors['PPOMPPU_CHECK_LAST_PAGE']}") String checkLastPage,
			@Value("#{selectors['DEFAULT_ARTICLE_REFRESH_TIME']}") String articleRefreshTime,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['PPOMPPU_ARTICLE_NO_REGEX']}") String articleNoRegex,
			@Value("#{selectors['PPOMPPU_UPLOAD_DATE_REGEX']}") String uploadDatePattern,
			@Value("#{new Integer(selectors['DEFAULT_REQUIRED_ARTICLE_NUM_TO_PARSE'])}") int requiredArticleNumToParse,
			@Value("#{new Integer(selectors['PPOMPPU_ARTICLE_NUM_PER_PAGE'])}") int articleNumPerPage,
			@Value("#{selectors['PPOMPPU_TYPE_DEFAULT_AD']}") String checkDefaultTypeAD,
			@Value("#{selectors['PPOMPPU_CHECK_NO_CONTENT']}") String checkNoContent,
			@Value("#{selectors['PPOMPPU_REDIRECT_URL_REGEX']}") String redirectUrlRegex,
			@Value("#{selectors['PPOMPPU_DEFAULT_SEARCH_TYPE']}") String defaultSearchType,
			@Value("#{selectors['PPOMPPU_RECOMMEND_REGEX']}") String ppomppu_recommend_regex,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {

		super(root, siteAddrRegex, typeList, parserName, articleRefreshTime, timeOut, checkNoContent, redirectUrlRegex,
				articleNoRegex, requiredArticleNumToParse, articleNumPerPage, defaultSearchType, sleepMin, sleepMax, retryNum,
				uploadDatePattern);
		this.addressPrefix = addressPrefix;
		this.checkLastPage = checkLastPage;
		addrPattern = Pattern.compile(contentAddressRegex);
		recommendPattern = Pattern.compile(ppomppu_recommend_regex);
		this.checkDefaultTypeAD = checkDefaultTypeAD;
	}

	@PostConstruct
	@Override
	public void init() {
		initializeParser();
	}

	@Override
	public boolean checkResultIsLastPage(Document doc, ArticleSearchData asd) {

		Element elem = doc.selectFirst(checkLastPage);

		return asd.getOffset() != Integer.parseInt(elem.text());
	}

	@Override
	public boolean checkIsNotice(Element elem, String boardType) {

		String selector = noticeCheck.get(boardType);

		//default 타입의 게시판 첫글이 광고글인 경우에 첫칸이 게시글번호가 아니라 이미지임
		//게시글번호 파싱해서 파싱되면 글번호이므로 광고(공지)아님 아니면 광고 혹은 공지
		if (boardType.equals(BoardTypePpomppu.TYPE_DEFAULT)) {
			String articleNo = elem.select(checkDefaultTypeAD).text();

			try {
				Long.parseLong(articleNo);
				return false;
			} catch (NumberFormatException e) {
				return true;
			}

		}

		if (selector != null)
			return elem.hasClass(noticeCheck.get(boardType));
		else
			return false;
	}

	@Override
	public String getContentAddr(Element elem, String boardType) {
		String selector = contentAddr.get(boardType);

		Element targetElem = elem.select(selector).stream().reduce((a, b) -> b).orElse(null);

		String addr = targetElem.attr("href");

		Matcher m = addrPattern.matcher(addr);

		while (m.find()) {
			addr = m.group(2);
		}
		addr = REMOVE_PAGINATION_PATTERN.matcher(addr).replaceFirst("");
		return selector != null ? addressPrefix + addr : "";
	}

	@Override
	public String getTitlePreProcess(Element elem, String boardType) {

		String selector = title.get(boardType);

		Elements titleElem = selector != null && !selector.isEmpty() ? elem.select(title.get(boardType)) : null;

		Element targetElem = titleElem.stream().reduce((a, b) -> b).orElse(null);

		String title = targetElem != null ? EmojiParser.removeAllEmojis(targetElem.text()) : "";

		return title;
	}

	@Override
	public long getArticleNo(Element elem, String boardType) {

		String selector = contentAddr.get(boardType);

		Element targetElem = elem.select(selector).stream().reduce((a, b) -> b).orElse(null);

		String addr = targetElem.attr("href");

		Matcher matcher = articleNoPattern.matcher(addr);

		if (matcher.find()) {

			try {

				return Long.parseLong(matcher.group());
			} catch (NumberFormatException e) {
				return 0;
			}
		} else
			return 0;
	}

	@Override
	public int getRecommend(Element elem, String boardType) {
		String selector = recommend.get(boardType);
		if (selector != null && !selector.isEmpty()) {

			String recommendStr = elem.select(recommend.get(boardType)).text();
			try {
				if (BoardTypePpomppu.TYPE_GALLERY.equals(boardType)) {
					return Integer.parseInt(recommendStr);
				} else {
					Matcher m = recommendPattern.matcher(recommendStr);

					while (m.find()) {
						return Integer.parseInt(m.group());
					}
					return 0;
				}
			} catch (Exception e) {
				return 0;
			}
		} else
			return 0;
	}

}

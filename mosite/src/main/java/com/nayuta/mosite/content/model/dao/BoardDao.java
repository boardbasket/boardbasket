package com.nayuta.mosite.content.model.dao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.BoardCategory;
import com.nayuta.mosite.content.model.vo.BoardComment;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.CommentEvaluation;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.ParseGrade;
import com.nayuta.mosite.content.model.vo.SearchType;

public interface BoardDao {

	public ContentVO selectIsBoardExist(String address);

	public ContentVO selectIsBoardExist(ContentVO cvo);

	public Site selectSiteWithAddress(String address);

	public ContentVO selectBoard(String boardAddress);

	public List<Hashtag> selectAutoCompleteResult(Map<String, Object> params);

	public List<BoardArticle> selectDbBoardArticle(ArticleSearchData asd, PageInfo pageInfo);

	public int updateDbBoardArticleViewCnt(String articleNo);

	public BoardArticle selectDbBoardArticle(String articleNo);

	public ContentVO selectOneDbBoard(String boardNameOrCode);

	public List<Site> selectAllSite();

	public Site selectSiteWithCode(String siteCode);

	public List<Site> selectSiteWithThreadNum(Integer threadNum);

	public List<SearchType> selectSiteSearchType(String siteCode);

	public int insertBoard(ContentVO cvo);

	public int insertHashtag(Hashtag hashtag);

	public int insertHashtag(List<Hashtag> hashtagList);

	public int insertRssHashtag(List<Hashtag> hashtagList);

	/*-----------------------------위에도 몇개 있음-----------------------------*/

	public int updateBoardData(BoardData boardData);

	public int updateBoardData(List<BoardData> boardData);

	public int insertBoardTitleData(List<BoardArticle> articleList);

	public List<ContentVO> selectMainDivBoard(Map<String, Object> options);

	public int selectMainDivBoardCount(Map<String, Object> options);

	public List<CommentEvaluation> selectEvalValues();

	public List<ContentVO> selectSimilarBoard(ContentVO board);

	public List<BoardArticle> selectArticleFewHoursBeforeCurrentTime(int hour);

	public int insertBoardData(ContentVO insertBoardData);

	public List<Hashtag> selectHashtag(Map<String, Object> params);

	public List<Hashtag> selectTrendHashtag(Map<String, Object> params);

	public int insertCommentEval(List<CommentEvaluation> bc);

	public int deleteMembersBoardCommentEval(BoardComment bc);

	public int insertBoardComment(BoardComment bc);

	public int deleteBoardComment(BoardComment bc);

	public int upsertBoardCommentVote(BoardComment bc);

	public int deleteBoardCommentVote(BoardComment bc);

	public BoardComment selectCheckBoardCommentVoteExist(BoardComment bc);

	public BoardComment selectBoardComment(BoardComment bc);

	public int selectBoardCommentLength(String boardCode);

	public BoardComment selectMyBoardCommentList(String boardCode, String memberNo);

	public List<BoardComment> selectBoardCommentList(Map<String, Object> param);

	public int selectMyBoardCommentExist(BoardComment bc);

	public int updateBoardComment(BoardComment bc);

	public List<CommentEvaluation> selectBoardEvalCount(String boardCode);

	public int insertCommunityContent(ContentVO board, List<BoardArticle> baList);

	public List<Site> selectRssSite(String site_code);

	public List<ContentVO> selectRssBoard(String board_code);

	public int insertRssContent(List<BoardArticle> rssArticleList);

	public List<BoardArticle> selectRssContent(Map<String, Object> params);

	public List<BoardArticle> selectCommunityContent(Map<String, Object> params);

	public List<Hashtag> selectCompareHashtagBetweenTwoDays(LocalDate fromDate, LocalDate toDate);

	public List<BoardArticle> selectNewsCommunityContent(Map<String, Object> params);

	public long selectLastDateOfHashtag();

	public List<Hashtag> selectRelativeTag(Map<String, Object> params);

	public BoardComment selectBoardCommentOnlyWithCode(BoardComment bc);

	public List<ContentVO> selectRelativeBoard(Map<String, Object> params);

	public List<ContentVO> selectAllBoardTitle();

	public int insertUserBoard(Map<String, Object> params);

	public int deleteUserBoard(String userBoardCode, String memberNo);

	public List<ContentVO> selectMembersAllUserBoard(Map<String, Object> params);

	public int insertWordToDictionary(List<String> wordList);

	public int deleteWordFromDictionary(String word);

	public List<String> selectWordDictionary();

	public List<Hashtag> selectUserHashtag(Map<String, Object> params);

	public List<Hashtag> selectHashtagForHeatMap(Map<String, Object> params);

	public int insertUserBoardHashtag(Map<String, Object> params);

	public List<ContentVO> tempSelectRuliwebAllBoard();

	public int tempUpdateRuliwebBoardAddress(List<ContentVO> boardList);

	public List<ContentVO> selectArticleNumParseTarget(int period, int frequency, List<Site> excludeSiteCodeList,
			List<Site> inclusiveSiteCodeList);

	public int updateArticleNoForGrade(BoardData boardData);

	public int updateParseFrequencyGrade(Site site);

	public List<ContentVO> selectCommonBoardParseTarget(int parse_per_day, List<Site> includeSiteList);

	public List<ContentVO> selectPopularBoardParseTarget(Site site);

	public ContentVO selectBoardWithCode(String boardCode);

	public int insertArticleNumHistory(String boardCode);

	public int updateArticleNumHistory(BoardData bd);

	public int updateArticleNumHistory(List<BoardData> bdList);

	public int updateBoardParseHistory(BoardData bd);

	public List<LocalDate> selectTimeOptionTarget();

	public int updateParseGradeToPopular(BoardData boardData);

	public List<HashMap<String, Integer>> selectParseGradeStatus();

	public int updateBoardAvailable(ContentVO board);

	public int updateBoardAvailable(List<ContentVO> boardList);

	public List<ParseGrade> selectParseGrade();

	public List<Hashtag> selectFirstAndLastRegisteredDateOfTag(Map<String, Object> params);

	public List<Hashtag> selectMostFrequentyMentionedDay(Map<String, Object> params);

	public List<ContentVO> selectPercentageOfTagMentionInBoard(Map<String, Object> params);

	public List<ContentVO> selectCheckBoardInBoardGroup(String board_code);

	public int insertBoardToGroup(String parent_board_code, List<String> child_board_code_list);

	public List<BoardArticle> selectCommunitySearchContent(Map<String, Object> params);

	public List<BoardArticle> selectRssSearchContent(Map<String, Object> params);

	public List<BoardCategory> selectBoardCategoricalData(Map<String, Object> params);

	public int insertHashtagRank(Map<String, Object> params);

	public List<Hashtag> selectStoredHashtagRank(Map<String, Object> params);

	public List<Hashtag> selectHashtagPopularityVal(Map<String, Object> params);

	public List<Hashtag> selectHashtagPersistencyVal(Map<String, Object> params);

	public List<Hashtag> selectHashtagUniversalityVal(Map<String, Object> params);

	public List<Hashtag> selectHashtagRank(Map<String, Object> params, String cacheKey);

	public int insertHashtagPopularityVal(Map<String, Object> params);

	public int insertHashtagPersistencyVal(Map<String, Object> params);

	public int insertHashtagUniversalityVal(Map<String, Object> params);

	public List<Hashtag> selectStoredHashtagPopularityVal(Map<String, Object> params);

	public List<Hashtag> selectStoredHashtagPersistencyVal(Map<String, Object> params);

	public List<Hashtag> selectStoredHashtagUniversalityVal(Map<String, Object> params);

	public int deleteAllTagRadarData();

	public int deleteAllUniqueRssHahstag();

	public int insertUniqueRssHashtag();

	public int insertBoardArticleGradeHistory();

	public List<BoardData> selectBoardArticleGradeHistory(Map<String, Object> params);

	public int insertArticleParsedNumHistory(BoardData bd);

	public List<Hashtag> selectArticleParsedNumHistory(Map<String, Object> params);
}

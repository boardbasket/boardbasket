package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.nayuta.mosite.common.util.GetCacheName;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

public class ArticleSearchData extends GetPrimaryKeyCodeCls implements GetCacheName, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// boardCode,siteCode

	private int offset = 1;
	private String board_address;
	private String board_name;
	private String site_name;
	private String site_address;
	private String keyword;
	private String favicon_address;
	private String type_val;
	private String board_type;
	private int board_num = 1;
	private String available;
	private String category_query;
	private String show_notice = "N";
	private String db_board = "N";
	private boolean refresh = false;

	public String getShow_notice() {
		return show_notice;
	}

	public void setShow_notice(String show_notice) {
		this.show_notice = show_notice;
	}

	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public String getSite_name() {
		return site_name;
	}

	public String getAvailable() {
		return available;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	@Min(value = 1, message = "페이지 번호는 1이상이어야 합니다.")
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getBoard_address() {
		return board_address;
	}

	public void setBoard_address(String board_address) {
		this.board_address = board_address;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public String getSite_code() {
		return site_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getType_val() {
		return type_val;
	}

	public void setType_val(String type_val) {
		this.type_val = type_val;
	}

	public String getBoard_type() {
		return board_type;
	}

	public void setBoard_type(String board_type) {
		this.board_type = board_type;
	}

	// @Min(value = 1, message = "가져올 게시판 갯수는 1개 이상이어야 합니다.")
	public int getBoard_num() {
		return board_num;
	}

	public void setBoard_num(int board_num) {
		this.board_num = board_num;
	}

	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public String getUser_board_code() {

		return user_board_code;
	}

	public String getCategory_query() {
		return category_query;
	}

	public void setCategory_query(String category_query) {
		this.category_query = category_query;
	}

	public String getFavicon_address() {
		return favicon_address;
	}

	public void setFavicon_address(String favicon_address) {
		this.favicon_address = favicon_address;
	}

	public String getDb_board() {
		return db_board;
	}

	public void setDb_board(String db_board) {
		this.db_board = db_board;
	}

	@Override
	public String toString() {
		return "ArticleSearchData [offset=" + offset + ", board_address=" + board_address + ", board_name=" + board_name
				+ ", site_name=" + site_name + ", site_address=" + site_address + ", keyword=" + keyword + ", favicon_address="
				+ favicon_address + ", type_val=" + type_val + ", board_type=" + board_type + ", board_num=" + board_num
				+ ", available=" + available + ", category_query=" + category_query + ", show_notice=" + show_notice
				+ ", db_board=" + db_board + ", refresh=" + refresh + "]";
	}

	public ArticleSearchData(String board_code, String user_board_code, int offset, String board_address, String board_name,
			String site_code, String site_name, String keyword, String type_val, String board_type, String show_notice,
			boolean refresh, String category_query, String site_address, String db_board) {
		super();
		this.offset = offset;
		this.board_address = board_address;
		this.board_name = board_name;
		this.site_name = site_name;
		this.keyword = keyword;
		this.type_val = type_val;
		this.board_type = board_type;
		this.show_notice = show_notice;
		this.refresh = refresh;
		this.user_board_code = user_board_code;
		this.board_code = board_code;
		this.category_query = category_query;
		this.site_address = site_address;
		this.db_board = db_board;
	}

	// tag를 얻기위해서 게시판 파싱할때 쓰는 생성자
	public ArticleSearchData(String board_code, String user_board_code, int offset, String board_address, String board_name,
			String site_code, String site_name, String keyword, String type_val, String board_type, boolean refresh,
			String category_query, String site_address) {
		super();
		this.offset = offset;
		this.board_address = board_address;
		this.board_name = board_name;
		this.site_name = site_name;
		this.keyword = keyword;
		this.type_val = type_val;
		this.board_type = board_type;
		this.refresh = refresh;
		this.user_board_code = user_board_code;
		this.board_code = board_code;
		this.category_query = category_query;
		this.site_address = site_address;
	}

	public ArticleSearchData(@NotBlank(message = "게시판 코드가 비어있습니다.") String board_code,
			@Min(value = 1, message = "페이지 번호는 1이상이어야 합니다.") int offset, String keyword,
			@Min(value = 1, message = "가져올 게시판 갯수는 1개 이상이어야 합니다.") int board_num) {
		super();
		this.board_code = board_code;
		this.offset = offset;
		this.keyword = keyword;
		this.board_num = board_num;
	}

	public ArticleSearchData applyBoardData(ContentVO cvo) {
		this.board_code = cvo.getBoard_code();
		this.user_board_code = cvo.getUser_board_code();
		this.board_name = cvo.getBoard_name();
		this.board_type = cvo.getBoard_type();
		this.board_address = cvo.getBoard_address();
		this.available = cvo.getAvailable();
		this.site_code = cvo.getSite_code();
		this.site_name = cvo.getSite_name();
		this.site_address = cvo.getSite_address();

		return this;
	}

	public ArticleSearchData() {
		super();
	}

	public String makeCacheName() {

		return board_address + offset + (keyword != null ? keyword : "") + (category_query != null ? category_query : "");

	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((board_address == null) ? 0 : board_address.hashCode());
		result = prime * result + ((board_name == null) ? 0 : board_name.hashCode());
		result = prime * result + board_num;
		result = prime * result + ((board_type == null) ? 0 : board_type.hashCode());
		result = prime * result + ((category_query == null) ? 0 : category_query.hashCode());
		result = prime * result + ((db_board == null) ? 0 : db_board.hashCode());
		result = prime * result + ((favicon_address == null) ? 0 : favicon_address.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + offset;
		result = prime * result + (refresh ? 1231 : 1237);
		result = prime * result + ((show_notice == null) ? 0 : show_notice.hashCode());
		result = prime * result + ((site_address == null) ? 0 : site_address.hashCode());
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		result = prime * result + ((type_val == null) ? 0 : type_val.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticleSearchData other = (ArticleSearchData) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (board_address == null) {
			if (other.board_address != null)
				return false;
		} else if (!board_address.equals(other.board_address))
			return false;
		if (board_name == null) {
			if (other.board_name != null)
				return false;
		} else if (!board_name.equals(other.board_name))
			return false;
		if (board_num != other.board_num)
			return false;
		if (board_type == null) {
			if (other.board_type != null)
				return false;
		} else if (!board_type.equals(other.board_type))
			return false;
		if (category_query == null) {
			if (other.category_query != null)
				return false;
		} else if (!category_query.equals(other.category_query))
			return false;
		if (db_board == null) {
			if (other.db_board != null)
				return false;
		} else if (!db_board.equals(other.db_board))
			return false;
		if (favicon_address == null) {
			if (other.favicon_address != null)
				return false;
		} else if (!favicon_address.equals(other.favicon_address))
			return false;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (offset != other.offset)
			return false;
		if (refresh != other.refresh)
			return false;
		if (show_notice == null) {
			if (other.show_notice != null)
				return false;
		} else if (!show_notice.equals(other.show_notice))
			return false;
		if (site_address == null) {
			if (other.site_address != null)
				return false;
		} else if (!site_address.equals(other.site_address))
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		if (type_val == null) {
			if (other.type_val != null)
				return false;
		} else if (!type_val.equals(other.type_val))
			return false;
		return true;
	}

}

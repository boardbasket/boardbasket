package com.nayuta.mosite.content.model.service.pageCommand;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.CustomObjectMapper;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;
import com.nayuta.mosite.content.model.vo.TimeSet;

import net.sf.json.JSONObject;

/*
 * 메인페이지 컨텐츠 , 검색하는 서비스
 * 
 * */

@Service("pcSelectMainDivBoard")
public class PcSelectMainDivBoard extends AbstractPageCls implements CommandInterface {

	@Autowired
	private PcSelectHashtag pcSelectHashtag;

	@Autowired
	private PcSelectRadarChartData pcSelectRadarChartData;

	@Autowired
	private PcSelectSearchBoard pcSelectSearchBoard;

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Autowired
	private PcSelectRelativeTag pcSelectRelativeTag;

	/*
	 * @Autowired private PcSelectHashtagHeatMap pcSelectHashtagHeatMap;
	 */

	@Autowired
	private PcSelectSearchArticle pcSelectSearchArticle;

	@Autowired
	private PcSelectPercentageOfBoard pcSelectPercentageOfBoard;

	@Autowired
	private PcSelectTagInfo pcSelectTagInfo;

	@Autowired
	private PcSelectStoredHashtagRank pcSelectStoredHashtagRank;

	private final Integer HASHTAG_RANK_LIMIT;// 태그 순위 계산해서 가져올 갯수

	private final Integer MAIN_PAGE_HASHTAG_RANK_LIMIT;// 가져온 태그 순위에서 몇개를 보여줄지 (메인페이지 wordcloud trendtag)

	public PcSelectMainDivBoard(

			@Value("#{new Integer(serverConst['server_const.main_page_hashtag_rank_limit'])}") Integer main_page_hashtag_rank_limit,
			@Value("#{new Integer(serverConst['server_const.hashtag_rank_limit'])}") Integer hashtag_rank_limit) {
		super();

		this.MAIN_PAGE_HASHTAG_RANK_LIMIT = main_page_hashtag_rank_limit;
		this.HASHTAG_RANK_LIMIT = hashtag_rank_limit;
	}

	@Override
	public void execute(ModelMap modelMap) {

		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		SearchData searchData = modelMap.containsKey("searchData") && modelMap.get("searchData") != null
				? (SearchData) modelMap.get("searchData")
				: null;
		CustomObjectMapper com = new CustomObjectMapper(); // json변환을위한 objectmapper
		LocalDate now = LocalDate.now(); // 오늘

		TimeSet nowTimeSet = weekUtil.getWeekNum(now); // 오늘의 타임셋

		Map<String, Object> today = new HashMap<>();
		today.put("timeSet", weekUtil.parseDateBaseToTimeMode(now, WeekUtil.DAY));
		today.put("timeMode", WeekUtil.DAY);
		today.put("limit", HASHTAG_RANK_LIMIT);
		today.put("mainPageLimit", MAIN_PAGE_HASHTAG_RANK_LIMIT);

		List<Hashtag> _tagRankingList = bDao.selectHashtagRank(today, "TODAY");//오늘 랭크를 가져
		List<Hashtag> _yesterDayTagRankingList = null;

		today.put("timeSet", weekUtil.parseDateBaseToTimeMode(now.minusDays(1), WeekUtil.DAY));// 하루전으로 세팅
		_yesterDayTagRankingList = bDao.selectHashtagRank(today, "YESTERDAY");// 하루전의 랭크를 가져옴

		LocalTime lt = LocalTime.now();
		int currentHour = lt.getHour();

		if (currentHour >= 0 && currentHour <= 6) {
			// 0~6시 까지는 데이터가 많이 안쌓여서 랭크가 부정확하므로 어제껄 그대로 가져옴
			_tagRankingList = _yesterDayTagRankingList;
		}

		Map<String, Integer> temp = _yesterDayTagRankingList.stream()
				.collect(Collectors.toMap(k -> k.getTag_name(), k -> k.getVal()));// {이름,어제랭크} 형태 map

		_tagRankingList.forEach(k -> {
			k.setToTime_rank(k.getVal());
			Integer val = temp.get(k.getTag_name());
			if (val != null)
				k.setFromTime_rank(val); // 이전 순위를 세팅해줌
			else
				k.setFromTime_rank(null);
		});

		_tagRankingList = _tagRankingList.subList(0, Math.min(_tagRankingList.size(), MAIN_PAGE_HASHTAG_RANK_LIMIT));
		List<String> trendTag = _tagRankingList.stream().map(k -> k.getTag_name()).limit(10).collect(Collectors.toList());
		// 검색 조건이 존재
		if (searchData != null) {
			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			// 검색어 있으면
			if (!StringUtils.isEmpty(searchData.getBoard_name())) {
				searchData.setKeyword(searchTextToKeyword.toKeyword(options.get("board_name")));
				String searchTarget = searchData.getSearch_target() == null ? "all" : searchData.getSearch_target();

				switch (searchTarget) {
				case "tag":
					resultObj.putAll(pcSelectHashtag.selectHashtagFunc(searchData, false));// 검색한 태그(keyword)의 line 그래프 데이터
					resultObj.putAll(pcSelectRelativeTag.selectRelativeTagFunc(searchData));// 연관태그
					// resultObj.putAll(pcSelectHashtagHeatMap.selectHashtagForHeatMapFunc(searchData,
					// 1));// 히트맵
					resultObj.putAll(pcSelectPercentageOfBoard.selectPercentageOfBoard(searchData, 1));// 가장많이 언급한 게시판 파이그래프
					resultObj.putAll(pcSelectTagInfo.selectTagInfoFunc(searchData));// 가장 많이,가장 최근에 등록된 날짜
					resultObj.putAll(pcSelectStoredHashtagRank.selectStoredHashtagRankFunc(searchData, true));// 랭크 그래프
					resultObj.putAll(pcSelectRadarChartData.selectTagRadarFunc(searchData, true));// 태그 속성
					break;
				case "board":
					resultObj.putAll(pcSelectSearchBoard.selectBoardFunc(searchData));															
					break;
				case "article":
					resultObj.putAll(pcSelectSearchArticle.selectSearchArticleFunc(searchData));
					break;
				default:
					// all
					resultObj.putAll(pcSelectHashtag.selectHashtagFunc(searchData, false));// 검색한 태그(keyword)의 line 그래프 데이터
					resultObj.putAll(pcSelectRelativeTag.selectRelativeTagFunc(searchData));// 연관태그
					resultObj.putAll(pcSelectPercentageOfBoard.selectPercentageOfBoard(searchData, 1));// 가장많이 언급한 게시판 파이그래프
					resultObj.putAll(pcSelectTagInfo.selectTagInfoFunc(searchData));// 가장 많이,가장 최근에 등록된 날짜
					resultObj.putAll(pcSelectStoredHashtagRank.selectStoredHashtagRankFunc(searchData, true));// 랭크 그래프
					resultObj.putAll(pcSelectRadarChartData.selectTagRadarFunc(searchData, true));// 태그 속성
					/*---------------------------------------------------------------------------------------------------*/
					resultObj.putAll(pcSelectSearchBoard.selectBoardFunc(searchData));
					resultObj.putAll(pcSelectSearchArticle.selectSearchArticleFunc(searchData));
					break;
				}

			} else {

			}
			resultObj.put("baseTimeSet", options.get("timeSet"));// 페이지 초기 timeSet/검색조건 timeSet
			resultObj.put("searchData", searchData);
		} else {

			SearchData sd = new SearchData();
			sd.setWeekList(weekUtil.getInitialTimeSet(now));
			sd.setBoard_name(String.join(" ", trendTag));
			sd.setTimeMode(weekUtil.getDEFAULT_TIME_MODE());

			resultObj.putAll(pcSelectSearchBoard.selectBoardFunc(sd));

			resultObj.put("baseTimeSet", weekUtil.getInitialTimeSet(now));// 페이지 초기의 선택된 timeSet
		}

		List<LocalDate> timeOptionTarget = bDao.selectTimeOptionTarget();// 태그가 존재하는 년도 ,월을 가져옴(검색할때 시간 선택하는 용도)

		resultObj.put("today", nowTimeSet);

		resultObj.put("timeOptionYear", /* 옵션 년도 리스트 */
				timeOptionTarget.stream().map(k -> k.getYear()).distinct().sorted().collect(Collectors.toList()));

		try {
			resultObj.put("trendTag", trendTag);
			resultObj.put("parseGradeList", bDao.selectParseGrade());
			resultObj.put("weekList", weekUtil.getTimeOption(timeOptionTarget));// 옵션에 주차 표시위한 정보
			resultObj.put("evalList", bDao.selectEvalValues());// evalItem UI 표시 위해
			resultObj.put("siteList", bDao.selectAllSite()); // 사이트 리스트(검색 조건)
			resultObj.put("rankTagList", com.writeValueAsString(_tagRankingList));

		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		modelMap.put("result", resultObj);

	}

}

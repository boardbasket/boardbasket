package com.nayuta.mosite.content.model.service.pageCommand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

//게시판 선택했을때 관련 데이터를 가져오는 서비스
@Service("pcSelectBoardContent")
public class PcSelectBoardContent extends AbstractPageCls implements CommandInterface {

	@Autowired
	private PcSelectHashtag pcSelectHashtag;

	@Autowired
	private PcSelectRelativeBoard pcSelectRelativeBoard;

	@Autowired
	private PcSelectBoardComment pcSelectBoardComment;

	@Autowired
	private PcSelectNewsCommunityContent pcSelectNewsCommunityContent;

	@Autowired
	private PcSelectBoardCategoricalData pcSelectBoardCategoricalData;

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Override
	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();
		SearchData searchData = modelMap.containsKey("searchData") ? (SearchData) modelMap.get("searchData") : null;
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));

		JSONObject hashtagObj = pcSelectHashtag.selectHashtagFunc(searchData, true);
		JSONObject relativeTag = pcSelectRelativeBoard.selectRelativeBoardFunc(searchData);
		JSONObject boardComment = pcSelectBoardComment.selectBoardCommentListFunc(searchData);
		searchData.setType("community");
		searchData.setOrder("desc");
		JSONObject articleSample = pcSelectNewsCommunityContent.selectNewsCommunityContent(searchData);
		JSONObject categoricalData = pcSelectBoardCategoricalData.selectBoardCategoricalDataFunc(searchData);

		resultObj.put("commentList", boardComment.get("commentList"));
		resultObj.put("hashtagData", hashtagObj);
		resultObj.put("relativeBoard", relativeTag.get("relativeBoard"));
		resultObj.putAll(articleSample);
		resultObj.putAll(categoricalData);

		modelMap.put("result", resultObj);

	}

}

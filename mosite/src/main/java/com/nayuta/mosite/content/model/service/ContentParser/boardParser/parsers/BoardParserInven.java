package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.BoardCategory;

@Component(value = "boardParserInven")
public class BoardParserInven extends BoardParserAbstract {

	protected final String titleSelector;
	protected final String newBoardSub;
	protected Pattern titlePattern;

	@PostConstruct
	public void init() {
		initializeParser();
	}

	public BoardParserInven(@Value("#{selectors['INVEN_ROOT']}") String root,
			@Value("#{selectors['INVEN_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeInven") BoardTypeAbstract typeList,
			@Value("#{selectors['INVEN_PARSER_NAME']}") String parserName,
			@Value("#{selectors['INVEN_TITLE_SELECTOR']}") String titleSelector,
			@Value("#{selectors['INVEN_TITLE_REGEX']}") String invenTitleRegex,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['INVEN_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['INVEN_POPULAR_ADDR']}") String popularAddr,
			@Value("#{selectors['INVEN_NEW_BOARD']}") String newBoard,
			@Value("#{selectors['INVEN_NEW_BOARD_SUB']}") String newBoardSub,
			@Value("#{selectors['INVEN_NEW_BOARD_ADDR']}") String newBoardAddr,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, popularBoard, popularAddr, newBoard, newBoardAddr,
				sleepMin, sleepMax, retryNum);
		this.titleSelector = titleSelector;
		titlePattern = Pattern.compile(invenTitleRegex);
		this.newBoardSub = newBoardSub;
	}

	@Override
	public String urlPreProcess(String addr) {

		String address = httpToHttps(addr);

		Pattern pattern = Pattern.compile(urlFilterRegex);

		Matcher matcher = pattern.matcher(address);

		if (matcher.find())
			return matcher.group();
		else
			return null;

	}

	public String getIgnoreBoard() {
		return ignoreBoard;
	}

	public void setIgnoreBoard(String ignoreBoard) {
		this.ignoreBoard = ignoreBoard;
	}

	@Override
	public String getTitle(Document doc) {
		String title = "";
		Element elem = doc.selectFirst(titleSelector);
		String rawTitle = elem.attr("content");
		rawTitle = rawTitle.replace(parserName, "");

		Matcher matcher = titlePattern.matcher(rawTitle);

		if (matcher.find()) {
			title = matcher.group();
		}

		return title;
	}

	@Override
	public List<BoardCategory> parseCategory(Document doc) {
		List<BoardCategory> result = new ArrayList<BoardCategory>();

		Elements elems = doc.select(category);

		// 맨처음 의미없는 기본값 '카테고리를 스킵'
		elems.stream().skip(1).forEach(elem -> {
			BoardCategory bc = parseCategoryFunc(elem);

			if (bc != null) {
				if (bc.getCategory_name() != null) {
					// db에 특문 안들어가서 특문 제거
					String filteredCategory = bc.getCategory_name().replaceAll("[^ 가-힣ㄱ-ㅎa-zA-Z0-9]", "");
					if (!filteredCategory.isEmpty()) {
						bc.setCategory_name(filteredCategory);
						result.add(bc);
					}
				}
			}

		});

		return result;

	}

	@Override
	public List<String> getNewBoardAddrList() {

		List<String> boardAddrList = new ArrayList<>();

		try {

			Document doc = getDocument(newBoardAddr);

			Elements elems = doc.select(newBoard);

			elems.forEach(elem -> {
				try {

					String targetAddr = elem.attr("href");

					Document subDoc = getDocument(targetAddr);

					Elements subBoard = subDoc.select(newBoardSub);
					List<String> subBoardList = new ArrayList<>();
					subBoard.stream().forEach(aTag -> {
						String rawAddr = aTag.attr("href");
						String filteredAddr = urlPreProcess(rawAddr);
						if (filteredAddr != null)
							subBoardList.add(filteredAddr);
					});

					subBoardList.stream().filter(k -> !boardAddrList.contains(k)).forEach(k -> {						
						boardAddrList.add(k);
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			return boardAddrList;
		} catch (Exception e) {
			return boardAddrList;
		}

	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

//특정 태그에 대한 게시판의 비율을 가져오는 클래스

@Service(value = "pcSelectPercentageOfBoard")
public class PcSelectPercentageOfBoard extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Override
	public void execute(ModelMap modelMap) {
		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		JSONObject result = new JSONObject();
		result.putAll(selectPercentageOfBoard(searchData, 1));
		modelMap.addAttribute("result", result);

	}

	public JSONObject selectPercentageOfBoard(SearchData searchData, Integer limit) {
		if (searchData != null) {
			JSONObject result = new JSONObject();
			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return result;
			else {
				if (limit != null)
					options.put("keyword", searchData.getKeyword().subList(0, limit));
			}

			try {
				result.put("percetageOfBoardList", com.writeValueAsString(bDao.selectPercentageOfTagMentionInBoard(options)));

			} catch (JsonProcessingException e) {

				return result;
			}
			return result;

		} else
			return null;
	}

}

package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nayuta.mosite.common.util.CustomDateSerializer;
import com.nayuta.mosite.common.util.GetCacheName;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.vo.validatorGroup.DeleteGroup;
import com.nayuta.mosite.content.model.vo.validatorGroup.InsertGroup;

//HashTag를 가져오기위한 VO
public class Hashtag extends GetPrimaryKeyCodeCls implements GetCacheName, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("t")
	private String tag_name;
	@JsonProperty("f")
	private Float frequency;
	@JsonProperty("y")
	private Integer year;// 주차 계산시의 년도(실제 연도와 다를 수 있음)
	@JsonProperty("m")
	private Integer month;// 주차 계산시의 월 (실제 월과 다를 수 있음)
	@JsonProperty("w")
	private Integer week;// 몇주차인지
	@JsonProperty("d")
	@JsonSerialize(using = CustomDateSerializer.class)
	private LocalDate insert_date;// 날짜

	// 아래부터 순위 확인할때 필요한 변수
	private Integer toTime_rank;
	private Integer fromTime_rank;
	private Float toTime_percent;
	private Float fromTime_percent;

	@JsonIgnore
	private Integer min_val;// 태그 radar함수 만들때 각 축에 대한 최대 ,최소값
	@JsonIgnore
	private Integer max_val;// 태그 radar함수 만들때 각 축에 대한 최대 ,최소값
	@JsonProperty("v")
	private Integer val; // 다용도로 쓰이는 값

	// word frequency를 만들고 저장하기위해 태그를 만드는데 사용
	public Hashtag(String boardCode, String tag_name, long frequency) {
		this.board_code = boardCode;
		this.tag_name = tag_name;
		this.frequency = (float) frequency;
	}

	public String getUser_tab_code() {
		return user_tab_code;
	}

	public void applyTimeSet(TimeSet ts) {
		if (ts.getYear() != null)
			this.year = ts.getYear();
		if (ts.getMonth() != null)
			this.month = ts.getMonth();
		if (ts.getWeek() != null)
			this.week = ts.getWeek();
		if (ts.getDay() != null)
			this.insert_date = ts.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public void setUser_tab_code(String user_tab_code) {
		this.user_tab_code = user_tab_code;
	}

	@NotBlank(message = "태그 코드가 비어있습니다.", groups = { DeleteGroup.class })
	public String getHashtag_code() {
		return hashtag_code;
	}

	public void setHashtag_code(String hashtag_code) {
		this.hashtag_code = hashtag_code;
	}

	public String getUser_board_code() {
		return user_board_code;
	}

	public void setUser_board_code(String user_board_code) {
		this.user_board_code = user_board_code;
	}

	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	@Pattern(regexp = "^[a-zA-Z가-힣0-9ㄱ-ㅎㅏ-ㅣ]{1,30}$", message = "태그 형식에 맞지 않습니다.", groups = { InsertGroup.class })
	public String getTag_name() {
		return tag_name;
	}

	public void setTag_name(String tag_name) {
		this.tag_name = tag_name;
	}

	public Hashtag() {
		super();
	}

	public Float getFrequency() {
		return frequency;
	}

	public void setFrequency(Float frequency) {
		this.frequency = frequency;
	}

	public LocalDate getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(LocalDate insert_date) {
		this.insert_date = insert_date;
	}

	public Integer getToTime_rank() {
		return toTime_rank;
	}

	public Integer getFromTime_rank() {
		return fromTime_rank;
	}

	public Float getToTime_percent() {
		return toTime_percent;
	}

	public Float getFromTime_percent() {
		return fromTime_percent;
	}

	public void setToTime_rank(Integer toTime_rank) {
		this.toTime_rank = toTime_rank;
	}

	public void setFromTime_rank(Integer fromTime_rank) {
		this.fromTime_rank = fromTime_rank;
	}

	public void setToTime_percent(float toTime_percent) {
		this.toTime_percent = toTime_percent;
	}

	public void setFromTime_percent(float fromTime_percent) {
		this.fromTime_percent = fromTime_percent;
	}

	public Integer getMin_val() {
		return min_val;
	}

	public Integer getMax_val() {
		return max_val;
	}

	public void setMin_val(Integer min_val) {
		this.min_val = min_val;
	}

	public void setMax_val(Integer max_val) {
		this.max_val = max_val;
	}

	public Integer getVal() {
		return val;
	}

	public void setVal(Integer val) {
		this.val = val;
	}

	public Hashtag(@NotBlank(message = "태그 코드가 비어있습니다.", groups = DeleteGroup.class) String hashtag_code, String user_board_code,
			String user_tab_code, String board_code,
			@Pattern(regexp = "^[a-zA-Z가-힣0-9ㄱ-ㅎㅏ-ㅣ]{1,30}$", message = "태그 형식에 맞지 않습니다.", groups = InsertGroup.class) String tag_name) {
		super();
		this.hashtag_code = hashtag_code;
		this.user_board_code = user_board_code;
		this.user_tab_code = user_tab_code;
		this.board_code = board_code;
		this.tag_name = tag_name;

	}

	@Override
	public String makeCacheName() {

		return user_board_code != null ? user_board_code + "_cache" : user_tab_code + "_cache";
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "Hashtag [tag_name=" + tag_name + ", frequency=" + frequency + ", year=" + year + ", month=" + month + ", week="
				+ week + ", insert_date=" + insert_date + ", toTime_rank=" + toTime_rank + ", fromTime_rank=" + fromTime_rank
				+ ", toTime_percent=" + toTime_percent + ", fromTime_percent=" + fromTime_percent + ", min_val=" + min_val
				+ ", max_val=" + max_val + ", val=" + val + "]";
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.SearchData;
import com.nayuta.mosite.content.model.vo.TimeSet;

import net.sf.json.JSONObject;

/*
 * 검색 결과중 글 검색 결과를 리턴해주는 서비스
 * */

@Service("pcSelectSearchArticle")
public class PcSelectSearchArticle extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	private final Integer SEARCH_PAGE_ARTICLE_TIME_SET_NUM;

	public PcSelectSearchArticle(
			@Value("#{new Integer(serverConst['server_const.search_page_article_time_set_num'])}") Integer sEARCH_PAGE_ARTICLE_TIME_SET_NUM) {
		super();
		SEARCH_PAGE_ARTICLE_TIME_SET_NUM = sEARCH_PAGE_ARTICLE_TIME_SET_NUM;
	}

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));
		modelMap.addAttribute("result", selectSearchArticleFunc(searchData));

	}

	public JSONObject selectSearchArticleFunc(SearchData searchData) {
		JSONObject result = new JSONObject();

		if (searchData != null) {

			List<BoardArticle> rssContentList = new ArrayList<>();
			List<BoardArticle> communityContentList = new ArrayList<>();
			Map<String, List<BoardArticle>> groupedCommunity = null;
			Map<String, List<BoardArticle>> groupedRss = null;
			Map<String, Object> options = searchData.makeParamMap(weekUtil);

			try {
				// 모든 검색결과에선 다 보여주지 않고 부분만 보여줌
				if (searchData.getSearch_target() != null && searchData.getSearch_target().equals("all")) {

					@SuppressWarnings("unchecked")
					List<TimeSet> originalTimeSet = (List<TimeSet>) options.get("timeSet");
					List<TimeSet> limitedTimeSet = new ArrayList<>();
					TimeSet todayTimeSet = weekUtil.getWeekNum(LocalDate.now());

					for (int i = 0; i < originalTimeSet.size(); ++i) {

						TimeSet current = originalTimeSet.get(i);

						limitedTimeSet.add(originalTimeSet.get(i));

						if (current.getYear().equals(todayTimeSet.getYear()) && current.getMonth().equals(todayTimeSet.getMonth())
								&& current.getWeek().equals(todayTimeSet.getWeek()))
							break;
					}

					options.put("timeSet", limitedTimeSet.subList(limitedTimeSet.size() <= SEARCH_PAGE_ARTICLE_TIME_SET_NUM ? 0
							: limitedTimeSet.size() - SEARCH_PAGE_ARTICLE_TIME_SET_NUM, limitedTimeSet.size()));

				}

				if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty()) {
					return result;
				} else {
					options.put("keyword", searchData.getKeyword());

					String timeMode = (String) options.get("timeMode");
					communityContentList = bDao.selectCommunitySearchContent(options);
					rssContentList = bDao.selectRssSearchContent(options);

					switch (timeMode) {
					case "year":
						groupedCommunity = communityContentList.stream().collect(Collectors.groupingBy(k -> {
							return k.getYear() + "년 " + k.getMonth() + "월";
						}, LinkedHashMap::new, Collectors.toList()));

						groupedRss = rssContentList.stream().collect(Collectors.groupingBy(k -> {
							return k.getYear() + "년 " + k.getMonth() + "월";
						}, LinkedHashMap::new, Collectors.toList()));
						break;
					case "month":
						groupedCommunity = communityContentList.stream().collect(Collectors.groupingBy(k -> {
							return k.getMonth() + "월 " + k.getWeek() + "주차";
						}, LinkedHashMap::new, Collectors.toList()));

						groupedRss = rssContentList.stream().collect(Collectors.groupingBy(k -> {
							return k.getMonth() + "월 " + k.getWeek() + "주차";
						}, LinkedHashMap::new, Collectors.toList()));
						break;
					case "week":
						groupedCommunity = communityContentList.stream()
								.collect(Collectors.groupingBy(
										k -> k.getDate().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일")), LinkedHashMap::new,
										Collectors.toList()));

						groupedRss = rssContentList.stream()
								.collect(Collectors.groupingBy(
										k -> k.getDate().format(DateTimeFormatter.ofPattern("yyyy년 MM월 dd일")), LinkedHashMap::new,
										Collectors.toList()));
						break;
					}

					result.put("keyword", options.get("keyword"));
					result.put("communityContentList", com.writeValueAsString(groupedCommunity));
					result.put("rssContentList", com.writeValueAsString(groupedRss));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return result;

		} else
			return null;

	}

}

package com.nayuta.mosite.content.model.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.executor.BatchResult;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.BoardCategory;
import com.nayuta.mosite.content.model.vo.BoardComment;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.CommentEvaluation;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.ParseGrade;
import com.nayuta.mosite.content.model.vo.SearchType;

@Repository
public class BoardDaoImpl implements BoardDao {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSession sqlSession;

	@Autowired
	@Qualifier("batchSqlSessionTemplate")
	private SqlSession batchSqlSession;

	public BoardDaoImpl() {
	}

	// 검색 자동완성 리스트 가져오기
	@Override
	public List<Hashtag> selectAutoCompleteResult(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectAutoCompleteResult", params);
	}

	// 게시판이 db에 존재하는지 주소로 확인
	@Override
	public ContentVO selectIsBoardExist(String address) {

		return sqlSession.selectOne("mainPage.selectIsBoardExist", address);

	}

	// 게시판이 db에 존재하는지 사이트주소,게시판이름,게시판 주소로 확인
	@Override
	public ContentVO selectIsBoardExist(ContentVO cvo) {

		return sqlSession.selectOne("mainPage.selectIsBoardExistWithCvo", cvo);

	}

	// 모든 사이트 종류 가져오기
	@Cacheable(value = "siteList")
	@Override
	public List<Site> selectAllSite() {

		return sqlSession.selectList("mainPage.selectAllSite");
	}

	// 사이트가 있는지 주소로 확인
	@Override
	public Site selectSiteWithAddress(String address) {

		return sqlSession.selectOne("mainPage.selectSiteWithAddress", address);
	}

	// 태그 검색결과 페이지네이션 적용위해서 검색결과 총 갯수 가져오기
	public int selectTagBoardCount(String tag, String memberNo) {
		Map<String, String> parameter = new HashMap<String, String>();
		parameter.put("member_no", memberNo);
		parameter.put("tag_name", tag);

		return sqlSession.selectOne("mainPage.selectTagBoardCount", parameter);
	}

	// 사이트의 검색타입을 리턴 (글쓴이,제목,닉네임 ....)
	@Override
	public List<SearchType> selectSiteSearchType(String siteCode) {

		return sqlSession.selectList("mainPage.selectSiteSearchType", siteCode);
	}

	// 게시판 하나의 정보를 주소로 가져옴
	@Override
	public ContentVO selectBoard(String boardAddress) {

		return sqlSession.selectOne("mainPage.selectBoard", boardAddress);
	}

	// 게시판 하나의 정보를 코드로 가져옴
	@Override
	public ContentVO selectBoardWithCode(String boardCode) {

		return sqlSession.selectOne("mainPage.selectBoardWithCode", boardCode);
	}

	@Override
	public Site selectSiteWithCode(String siteCode) {

		return sqlSession.selectOne("mainPage.selectSiteWiteCode", siteCode);
	}

	// db에서 게시글들을 페이지 단위로 가져옴 (자체 게시판들 -> 공지 등)
	@Override
	public List<BoardArticle> selectDbBoardArticle(ArticleSearchData asd, PageInfo pageInfo) {

		Map<String, Object> param = new HashMap<>();
		param.put("asd", asd);
		param.put("pageInfo", pageInfo);

		return sqlSession.selectList("mainPage.selectDbBoardArticle", param);
	}

	// db에서 게시글의 내용을 가져옴 (자체 게시판들 -> 공지 등)
	@Override
	public BoardArticle selectDbBoardArticle(String articleNo) {

		return sqlSession.selectOne("mainPage.selectOneDbBoardArticle", articleNo);

	}

	// dbboard의 게시글 조회수를 늘리는 쿼리
	@Override
	public int updateDbBoardArticleViewCnt(String articleNo) {

		return sqlSession.update("mainPage.updateDbBoardArticleViewCnt", articleNo);
	}

	// 메인페이지 드롭다운 공지사항을 보여주기 위해 공지사항 게시판 정보를 가져옴(기타 db게시판 정보 가져올때도 활용)
	@Override
	public ContentVO selectOneDbBoard(String boardNameOrCode) {

		return sqlSession.selectOne("mainPage.selectOneDbBoard", boardNameOrCode);
	}

	// 게시판의 정보 (마지막 글 번호 등) 업데이트
	@Override
	public int updateBoardData(BoardData boardData) {

		return sqlSession.update("mainPage.updateBoardData", boardData);

	}

	@Override
	public int updateBoardData(List<BoardData> boardData) {

		boardData.forEach(k -> batchSqlSession.update("mainPage.updateBoardData", k));

		List<BatchResult> ba = batchSqlSession.flushStatements();

		System.out.println(ba);

		return 1;
	}

	// 게시판의 파싱 빈도를 결정하는 grade를 계산하기 위한 최신 글번호를 boardData table에 업데이트 하기 위한 함수
	@Override
	public int updateArticleNoForGrade(BoardData boardData) {

		return sqlSession.update("mainPage.updateArticleNoForGrade", boardData);

	}

	// db에다가 파싱한 게시판의 게시글 제목 넣기 (안씀)
	@Override
	public int insertBoardTitleData(List<BoardArticle> articleList) {

		articleList.stream().forEach(k -> {
			batchSqlSession.insert("mainPage.insertBoardTitleData", k);
		});

		batchSqlSession.flushStatements();

		return 1;
	}

	// 첫페이지의 게시판 리스트 가져오기

	@Override
	public List<ContentVO> selectMainDivBoard(Map<String, Object> options) {

		return sqlSession.selectList("mainPage.selectMainDivBoard", options);
	}

	// 게시판 평가에 쓰일 eval리스트들 가져오기
	@Cacheable(value = "evalList")
	@Override
	public List<CommentEvaluation> selectEvalValues() {

		return sqlSession.selectList("mainPage.selectEvalValues");
	}

	// 비슷한 게시판 가져오기
	@Override
	public List<ContentVO> selectSimilarBoard(ContentVO board) {

		return sqlSession.selectList("mainPage.selectSimilarBoard", board);
	}

	// 현재 시간으로부터 해당 시간까지의 파싱된 게시글 제목을 가져옴(안씀)
	@Override
	public List<BoardArticle> selectArticleFewHoursBeforeCurrentTime(int hour) {

		return sqlSession.selectList("mainPage.selectArticleFewHoursBeforeCurrentTime", hour);
	}

	@Override
	public int insertHashtag(Hashtag hashtag) {

		return sqlSession.insert("mainPage.insertHashTag", hashtag);
	}

	@Override
	public int insertHashtag(List<Hashtag> hashtagList) {

		hashtagList.stream().forEach(x -> {
			batchSqlSession.insert("mainPage.insertHashTag", x);
		});
		batchSqlSession.flushStatements();
		return 1;
	}

	@Override
	public int insertRssHashtag(List<Hashtag> hashtagList) {

		hashtagList.stream().forEach(x -> {
			batchSqlSession.insert("mainPage.insertRssHashTag", x);
		});
		batchSqlSession.flushStatements();
		return 1;
	}

	// 파싱된 게시판 하나를 집어넣음
	@Override
	public int insertBoard(ContentVO cvo) {
		int status = 0;
		sqlSession.insert("mainPage.insertBoard", cvo);
		sqlSession.insert("mainPage.insertBoardData", cvo);
		sqlSession.insert("mainPage.insertArticleNumHistory", cvo);
		sqlSession.insert("mainPage.insertBoardParseHistory", cvo);
		if (cvo.getBoard_category_list() == null || cvo.getBoard_category_list().size() < 1)
			status = 1;
		else {
			for (BoardCategory bc : cvo.getBoard_category_list())
				bc.setBoard_code(cvo.getBoard_code());

			status = sqlSession.insert("mainPage.insertBoardCategory", cvo);
		}

		// board_group address가 존재하면(루리웹)
		List<String> childBoardAddr = cvo.getChild_board_addr();

		if (childBoardAddr != null) {

			String parentBoardCode = sqlSession.selectOne("mainPage.selectBoardGroupParent", childBoardAddr);
			Map<String, Object> params = new HashMap<>();
			// parent가 있으면 해당 그룹에 삽입
			if (parentBoardCode != null) {

				params.put("parent_board_code", parentBoardCode);
				params.put("child_board_code", cvo.getBoard_code());

				sqlSession.insert("mainPage.insertBoardToGroup", params);
			}

			else {
				// parent가 없으면 현재 삽입된 게시판을 부모로 해서 다른 게시판을 집어넣음
				params.put("parent_board_code", cvo.getBoard_code());
				params.put("child_board_address_list", /* 자기 자신과 같지 않은 것들만 필터링 */
						childBoardAddr.stream().filter(k -> !k.equals(cvo.getBoard_address())).collect(Collectors.toList()));
				sqlSession.insert("mainPage.insertBoardToGroup", params);
			}
		}

		return status;

	}

	// 게시판 넣을때 같이 넣어주는 게시판정보들
	@Override
	public int insertBoardData(ContentVO board) {

		return sqlSession.insert("mainPage.insertBoardData", board);
	}

	// 게시판의 태그를 옵션을 통해서 가져옴
	@Override
	public List<Hashtag> selectHashtag(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectHashtag", params);
	}

	// 게시판의 댓글에 eval값을 넣어줌
	@Override
	public int insertCommentEval(List<CommentEvaluation> ceList) {
		int result = -1;
		for (CommentEvaluation ce : ceList)
			result = batchSqlSession.insert("mainPage.insertCommentEval", ce);

		batchSqlSession.flushStatements();
		return result;
	}

	// 현재 comment의 eval 값을 전부 삭제(다시 insert 하기 위해)
	@Override
	public int deleteMembersBoardCommentEval(BoardComment bc) {

		return sqlSession.delete("mainPage.deleteMembersBoardCommentEval", bc);
	}

	@Override
	public int deleteBoardComment(BoardComment bc) {
		return sqlSession.delete("mainPage.deleteBoardComment", bc);
	}

	// 게시판에 댓글 집어넣기
	@Override
	public int insertBoardComment(BoardComment bc) {

		int result = -1;
		result = sqlSession.insert("mainPage.insertBoardComment", bc);

		if (result > 0) {
			List<CommentEvaluation> ceList = bc.getComment_eval_list();

			if (ceList != null) {
				for (CommentEvaluation ce : ceList) {
					result = sqlSession.insert("mainPage.insertCommentEval", ce);
				}
			}
		}
		return result;
	}

	// 게시판의 댓글 가져오기
	@Override
	public BoardComment selectBoardComment(BoardComment bc) {

		return sqlSession.selectOne("mainPage.selectBoardComment", bc);
	}

	// 게시판의 댓글 가져오기
	@Override
	public List<BoardComment> selectBoardCommentList(Map<String, Object> param) {

		return sqlSession.selectList("mainPage.selectBoardComment", param);
	}

	@Override
	public BoardComment selectMyBoardCommentList(String boardCode, String memberNo) {
		Map<String, Object> param = new HashMap<>();
		param.put("board_code", boardCode);
		param.put("member_no", memberNo);

		return sqlSession.selectOne("mainPage.selectMyBoardComment", param);
	}

	// 게시판의 comment 총 갯수를 가져오기
	@Override
	public int selectBoardCommentLength(String boardCode) {

		Map<String, Object> param = new HashMap<>();
		param.put("board_code", boardCode);

		return sqlSession.selectOne("mainPage.selectBoardCommentLength", param);
	}

	// 게시판에 현재 멤버가 댓글을 달았나 확인
	@Override
	public int selectMyBoardCommentExist(BoardComment bc) {

		return sqlSession.selectOne("mainPage.selectMyBoardCommentExist", bc);
	}

	// 현재 멤버의 해당 게시판 댓글을 업데이트
	@Override
	public int updateBoardComment(BoardComment bc) {

		return sqlSession.update("mainPage.updateBoardComment", bc);
	}

	// 댓글 평가를 insert
	@Override
	public int upsertBoardCommentVote(BoardComment bc) {

		return sqlSession.insert("mainPage.upsertBoardCommentVote", bc);
	}

	// 댓글 평가가 존재하는지
	@Override
	public BoardComment selectCheckBoardCommentVoteExist(BoardComment bc) {

		return sqlSession.selectOne("mainPage.selectCheckBoardCommentVoteExist", bc);
	}

	// 댓글 평가를 insert
	@Override
	public int deleteBoardCommentVote(BoardComment bc) {

		return sqlSession.insert("mainPage.deleteBoardCommentVote", bc);
	}

	@Override
	public List<CommentEvaluation> selectBoardEvalCount(String boardCode) {

		return sqlSession.selectList("mainPage.selectBoardEvalCount", boardCode);
	}

	@Override
	public List<Hashtag> selectTrendHashtag(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectTrendHashtag", params);
	}

	@Override
	public int selectMainDivBoardCount(Map<String, Object> options) {

		return sqlSession.selectOne("mainPage.selectMainDivBoardCount", options);
	}

	@Override
	public int insertCommunityContent(ContentVO board, List<BoardArticle> baList) {

		for (BoardArticle ba : baList) {

			ba.setBoard_code(board.getBoard_code());
			ba.setSite_code(board.getSite_code());
			batchSqlSession.insert("mainPage.insertCommunityContent", ba);

		}

		batchSqlSession.flushStatements();

		return 1;
	}

	@Override
	public List<Site> selectRssSite(String site_code) {
		return sqlSession.selectList("mainPage.selectRssSite", site_code);
	}

	@Override
	public List<ContentVO> selectRssBoard(String board_code) {
		return sqlSession.selectList("mainPage.selectRssBoard", board_code);
	}

	@Override
	public int insertRssContent(List<BoardArticle> rssArticleList) {

		rssArticleList.forEach(article -> batchSqlSession.insert("mainPage.insertRssContent", article));
		List<BatchResult> brList = batchSqlSession.flushStatements();
		return brList.size();
	}

	@Override
	public List<BoardArticle> selectRssContent(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectRssContent", params);
	}

	@Override
	public List<BoardArticle> selectCommunityContent(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectCommunityContent", params);
	}

	@Cacheable(value = "compareHashtag")
	@Override
	public List<Hashtag> selectCompareHashtagBetweenTwoDays(LocalDate fromDate, LocalDate toDate) {

		Map<String, Object> params = new HashMap<>();

		params.put("fromDate", fromDate);
		params.put("toDate", toDate);

		return sqlSession.selectList("mainPage.selectCompareHashtagBetweenTwoDays", params);
	}

	@Override
	public long selectLastDateOfHashtag() {

		return sqlSession.selectOne("mainPage.selectLastDateOfHashtag");
	}

	@Override
	public List<BoardArticle> selectNewsCommunityContent(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectNewsCommunityContent", params);
	}

	@Override
	public List<Hashtag> selectRelativeTag(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectRelativeTag", params);
	}

	@Override
	public BoardComment selectBoardCommentOnlyWithCode(BoardComment bc) {

		return sqlSession.selectOne("mainPage.selectBoardCommentOnlyWithCode", bc);
	}

	@Override
	public List<ContentVO> selectRelativeBoard(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectRelativeBoard", params);
	}

	@Override
	public List<ContentVO> selectAllBoardTitle() {

		return sqlSession.selectList("mainPage.selectAllBoardTitle");
	}

	@Override
	public int insertUserBoard(Map<String, Object> params) {

		return sqlSession.insert("mainPage.insertUserBoard", params);
	}

	@Override
	public int deleteUserBoard(String userBoardCode, String memberNo) {

		Map<String, Object> params = new HashMap<>();
		params.put("userBoardCode", userBoardCode);
		params.put("memberNo", memberNo);

		return sqlSession.delete("mainPage.deleteUserBoard", params);
	}

	@Override
	public List<ContentVO> selectMembersAllUserBoard(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectMembersAllUserBoard", params);
	}

	@Override
	public int insertWordToDictionary(List<String> wordList) {
		Map<String, Object> params = new HashMap<>();
		params.put("wordList", wordList);

		return sqlSession.insert("mainPage.insertWordToDictionary", params);
	}

	@Override
	public int deleteWordFromDictionary(String word) {

		return sqlSession.delete("mainPage.deleteWordFromDictionary", word);
	}

	@Override
	public List<String> selectWordDictionary() {

		return sqlSession.selectList("mainPage.selectWordDictionary");
	}

	@Override
	public List<Hashtag> selectUserHashtag(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectUserHashtag", params);
	}

	@Override
	public int insertUserBoardHashtag(Map<String, Object> params) {

		return sqlSession.insert("mainPage.insertUserBoardHashtag", params);
	}

	@Override
	public List<Hashtag> selectHashtagForHeatMap(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectHashtagForHeatMap", params);
	}

	@Override
	public List<ContentVO> tempSelectRuliwebAllBoard() {

		return sqlSession.selectList("mainPage.tempSelectRuliwebAllBoard");
	}

	@Override
	public int tempUpdateRuliwebBoardAddress(List<ContentVO> boardList) {

		List<ContentVO> errorBoard = new ArrayList<>();
		for (int i = 0; i < boardList.size(); ++i) {
			try {
				sqlSession.update("mainPage.tempUpdateRuliwebBoardAddress", boardList.get(i));
			} catch (Exception e) {
				errorBoard.add(boardList.get(i));
			}

		}

		System.out.println(errorBoard);

		return 0;
	}

	@Override
	public List<ContentVO> selectArticleNumParseTarget(int period, int frequency, List<Site> excludeSiteCodeList,
			List<Site> inclusiveSiteCodeList) {

		Map<String, Object> params = new HashMap<>();

		params.put("period", period);
		params.put("frequency", frequency);
		params.put("excludeSiteCodeList", excludeSiteCodeList);
		params.put("inclusiveSiteCodeList", inclusiveSiteCodeList);

		return sqlSession.selectList("mainPage.selectArticleNumParseTarget", params);

	}

	@Override
	public int updateParseFrequencyGrade(Site site) {

		return sqlSession.update("mainPage.updateParseFrequencyGrade", site);
	}

	@Override
	public List<ContentVO> selectCommonBoardParseTarget(int parse_per_day, List<Site> includeSiteList) {

		Map<String, Object> params = new HashMap<>();

		params.put("parse_per_day", parse_per_day);
		params.put("includeSiteList", includeSiteList);

		return sqlSession.selectList("mainPage.selectCommonBoardParseTarget", params);
	}

	@Override
	public int insertArticleNumHistory(String board_code) {

		return sqlSession.insert("mainPage.insertArticleNumHistory", board_code);
	}

	@Override
	public int updateArticleNumHistory(BoardData bd) {
		return sqlSession.update("mainPage.updateArticleNumHistory", bd);
	}

	@Override
	public int updateArticleNumHistory(List<BoardData> bdList) {
		bdList.stream().forEach(bd -> {
			batchSqlSession.insert("mainPage.updateArticleNumHistory", bd);
		});

		List<BatchResult> ba = batchSqlSession.flushStatements();

		System.out.println(ba);
		return 1;
	}

	@Override
	public int updateBoardParseHistory(BoardData bd) {
		return sqlSession.update("mainPage.updateBoardParseHistory", bd);
	}

	@Cacheable(value = "timeOptionList")
	@Override
	public List<LocalDate> selectTimeOptionTarget() {

		return sqlSession.selectList("mainPage.selectTimeOptionTarget");
	}

	@Override
	public List<Site> selectSiteWithThreadNum(Integer threadNum) {
		Map<String, Integer> param = new HashMap<>();
		param.put("threadNum", threadNum);
		return sqlSession.selectList("mainPage.selectSiteWithThreadNum", param);
	}

	@Override
	public int updateParseGradeToPopular(BoardData boardData) {

		return sqlSession.update("updateParseGradeToPopular", boardData);
	}

	@Override
	public List<HashMap<String, Integer>> selectParseGradeStatus() {
		return sqlSession.selectList("mainPage.selectParseGradeStatus");
	}

	@Override
	public int updateBoardAvailable(ContentVO board) {

		return sqlSession.update("mainPage.updateBoardAvailable", board);
	}

	@Override
	public int updateBoardAvailable(List<ContentVO> boardList) {
		boardList.stream().forEach(board -> {
			batchSqlSession.insert("mainPage.updateBoardAvailable", board);
		});
		batchSqlSession.flushStatements();
		return 1;
	}

	@Override
	public List<ContentVO> selectPopularBoardParseTarget(Site site) {

		return sqlSession.selectList("mainPage.selectPopularBoardParseTarget", site);
	}

	@Cacheable(value = "parseGrade")
	@Override
	public List<ParseGrade> selectParseGrade() {

		return sqlSession.selectList("mainPage.selectParseGrade");
	}

	@Override
	public List<Hashtag> selectHashtagPopularityVal(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectHashtagPopularityVal", params);
	}

	@Override
	public List<Hashtag> selectHashtagPersistencyVal(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectHashtagPersistencyVal", params);
	}

	@Override
	public List<Hashtag> selectHashtagUniversalityVal(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectHashtagUniversalityVal", params);
	}

	@Cacheable(value = "hashtagRank", key = "#cacheKey", condition = "#cacheKey!=null")
	@Override
	public List<Hashtag> selectHashtagRank(Map<String, Object> params, String cacheKey) {
		return sqlSession.selectList("mainPage.selectHashtagRank", params);
	}

	@Override
	public List<Hashtag> selectFirstAndLastRegisteredDateOfTag(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectFirstAndLastRegisteredDateOfTag", params);
	}

	@Override
	public List<Hashtag> selectMostFrequentyMentionedDay(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectMostFrequentyMentionedDay", params);
	}

	@Override
	public List<ContentVO> selectPercentageOfTagMentionInBoard(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectPercentageOfTagMentionInBoard", params);
	}

	@Override
	public List<ContentVO> selectCheckBoardInBoardGroup(String board_code) {
		return sqlSession.selectList("mainPage.selectCheckBoardInBoardGroup", board_code);
	}

	@Override
	public int insertBoardToGroup(String parent_board_code, List<String> child_board_code_list) {

		child_board_code_list.stream().forEach(child_board_code -> {
			Map<String, String> params = new HashMap<>();
			params.put("parent_board_code", parent_board_code);
			params.put("child_board_code", child_board_code);
			batchSqlSession.insert("mainPage.insertBoardToGroup", params);
		});
		batchSqlSession.flushStatements();
		return 1;

	}

	@Override
	public List<BoardArticle> selectCommunitySearchContent(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectCommunitySearchContent", params);
	}

	@Override
	public List<BoardArticle> selectRssSearchContent(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectRssSearchContent", params);
	}

	@Override
	public List<BoardCategory> selectBoardCategoricalData(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectBoardCategoricalData", params);
	}

	@Override
	public int insertHashtagRank(Map<String, Object> params) {

		return sqlSession.insert("mainPage.insertHashtagRank", params);
	}

	@Override
	public List<Hashtag> selectStoredHashtagRank(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectStoredHashtagRank", params);
	}

	@Override
	public int insertHashtagPopularityVal(Map<String, Object> params) {

		return sqlSession.insert("mainPage.insertHashtagPopularityVal", params);
	}

	@Override
	public int insertHashtagPersistencyVal(Map<String, Object> params) {
		return sqlSession.insert("mainPage.insertHashtagPersistencyVal", params);
	}

	@Override
	public int insertHashtagUniversalityVal(Map<String, Object> params) {
		return sqlSession.insert("mainPage.insertHashtagUniversalityVal", params);
	}

	@Override
	public int deleteAllTagRadarData() {
		return sqlSession.delete("mainPage.deleteAllTagRadarData");
	}

	@Override
	public List<Hashtag> selectStoredHashtagPopularityVal(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectStoredHashtagPopularityVal", params);
	}

	@Override
	public List<Hashtag> selectStoredHashtagPersistencyVal(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectStoredHashtagPersistencyVal", params);
	}

	@Override
	public List<Hashtag> selectStoredHashtagUniversalityVal(Map<String, Object> params) {
		return sqlSession.selectList("mainPage.selectStoredHashtagUniversalityVal", params);
	}

	@Override
	public int deleteAllUniqueRssHahstag() {

		return sqlSession.delete("mainPage.deleteAllUniqueCategoricalRssTag");
	}

	@Override
	public int insertUniqueRssHashtag() {

		return sqlSession.insert("mainPage.insertUniqueCategoricalRssTag");
	}

	@Override
	public int insertBoardArticleGradeHistory() {

		return sqlSession.insert("mainPage.insertBoardArticleGradeHistory");
	}

	@Override
	public List<BoardData> selectBoardArticleGradeHistory(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.insertBoardArticleGradeHistory", params);
	}

	@Override
	public int insertArticleParsedNumHistory(BoardData bd) {

		return sqlSession.insert("mainPage.insertArticleParsedNumHistory", bd);
	}

	@Override
	public List<Hashtag> selectArticleParsedNumHistory(Map<String, Object> params) {

		return sqlSession.selectList("mainPage.selectArticleParsedNumHistory", params);
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.util.CustomObjectMapper;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

// 검색어를 이용해서 커뮤니티,뉴스 정보를 가져옴
@Service("pcSelectNewsCommunityContent")
public class PcSelectNewsCommunityContent extends AbstractPageCls implements CommandInterface {

	private final int CONTENT_NUM_LIMIT;

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	public PcSelectNewsCommunityContent(
			@Value("#{new Integer(serverConst['server_const.content_num_limit'])}") int content_num_limit) {
		super();
		this.CONTENT_NUM_LIMIT = content_num_limit;
	}

	@Override
	public void execute(ModelMap modelMap) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		SearchData searchData = modelMap.get("searchData") != null ? (SearchData) modelMap.get("searchData") : null;

		if (searchData != null) {
			searchData.setLimit(CONTENT_NUM_LIMIT);
			Map<String, Object> params = searchData.makeParamMap(weekUtil);
			resultObj.putAll(selectNewsCommunityContent(params));
		}

		modelMap.put("result", resultObj);

	}

	public JSONObject selectNewsCommunityContent(Map<String, Object> options) {
		JSONObject result = new JSONObject();
		List<BoardArticle> baList = new ArrayList<>();
		try {
			String type = (String) options.get("type");
			options.put("limit", CONTENT_NUM_LIMIT);
			CustomObjectMapper co = new CustomObjectMapper();

			List<String> keyword = searchTextToKeyword.toKeyword(options.get("board_name"));

			options.put("keyword", keyword);

			baList = type.equals("news") ? bDao.selectRssContent(options) : bDao.selectCommunityContent(options);

			result.put("articleList", co.writeValueAsString(baList));
		} catch (Exception e) {
			result.put("articleList", baList);
		}

		result.put("last", baList.size() < CONTENT_NUM_LIMIT);

		return result;
	}

	public JSONObject selectNewsCommunityContent(SearchData searchData) {
		if (searchData != null)
			return selectNewsCommunityContent(searchData.makeParamMap(weekUtil));
		else
			return null;
	}

}

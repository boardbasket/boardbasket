package com.nayuta.mosite.content.model.service.ContentParser.boardParser;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserAbstract;
import com.nayuta.mosite.content.model.vo.ContentVO;

/*
*SITE의 BOARD 들을 PARSING하는 클래스
*
*/
@Component
public class BoardParserController {

	Logger log = LogManager.getLogger();

	private List<BoardParserAbstract> parserList;

	@Autowired
	private BoardDaoImpl bDao;

	// 주소를 파싱해 어떤 사이트의 주소인지 확인하고 그에따른 게시판 파서를 가져오는 함수
	public BoardParserAbstract getParser(String address) {

		for (BoardParserAbstract bpa : parserList)
			if (bpa.checkParser(address))
				return bpa;

		return null;
	}

	// 게시판을 파싱하는 함수
	public ContentVO parseBoard(String address) {

		BoardParserAbstract bpa = null;

		URI uri = checkStringIsAddress(address);
		if (uri != null) {

			bpa = getParser(address);

			if (bpa == null)
				return null;

			return bpa.parseBoard(address);
		} else
			return null;

	}

	// 이미 저장된 게시판들의 최신화를 위한 함수
	public ContentVO maintenanceBoard(String address) {
		BoardParserAbstract bpa = null;
		try {
			bpa = getParser(address);
			return bpa.parseBoard(address);
		} catch (Exception e) {
			return null;
		}

	}

	// 각 사이트의 인기 게시판들을 가져오는 함수
	public List<String> getPopularBoards() {

		List<String> result = new ArrayList<>();

		for (BoardParserAbstract bpa : parserList) {

			result.addAll(bpa.getPopularBoardAddr());

		}

		return result;
	}

	// 각 사이트의 새로운 게시판을 파싱해서 db에 넣는 함수
	public int parseNewBoard() {
		int count = 0;
		for (BoardParserAbstract bpa : parserList) {

			int cnt = parseNewBoard(bpa);

			count += cnt;
		}
		return count;
	}

	public int parseNewBoard(BoardParserAbstract bpa) {

		List<String> newBoardAddr = bpa.getNewBoardAddrList();
		int count = 0;
		for (int j = 0; j < newBoardAddr.size(); ++j) {
			String targetAddr = newBoardAddr.get(j);
			ContentVO board = bDao.selectBoard(targetAddr);

			if (board == null) {
				ContentVO targetBoard = bpa.parseBoard(targetAddr);				
				if (targetBoard == null)
					continue;
				Site site = bDao.selectSiteWithAddress(targetBoard.getSite_address());
				targetBoard.applySiteData(site);
				bDao.insertBoard(targetBoard);
				log.info("- 새 게시판 추가 " + targetBoard.getBoard_name() + ", 주소 : " + targetBoard.getBoard_address());
				count++;
			}

		}
		return count;
	}

	// db에 저장되는 형태의 정제된 address를 리턴하는 함수
	public String getParsedAddress(String address) {
		URI uri = checkStringIsAddress(address);
		BoardParserAbstract bpa = null;

		if (uri != null) {

			bpa = getParser(address);
			if (bpa == null)
				throw new MVCServiceException("지원하지 않는 사이트 이거나 주소입니다.");
			return bpa.urlPreProcess(address);
		} else
			throw new MVCServiceException("지원하지 않는 사이트 이거나 주소입니다.");

	}

	public List<BoardParserAbstract> getParserList() {
		return parserList;
	}

	public void setParserList(List<BoardParserAbstract> parserList) {
		this.parserList = parserList;
	}

	// String 변수값이 주소인지 확인하는 함수
	public URI checkStringIsAddress(String address) {

		try {
			URI uri = new URI(address);

			if (uri.getHost() == null)
				return null;
			else
				return uri;
		} catch (URISyntaxException e) {
			return null;
		}
	}

	// 중복 사이트인지 체크할때 필요한 데이터를 가져오는 함수
	/*
	 * public ContentVO getDuplicateCheckData(String address) { String _address =
	 * getParsedAddress(address);
	 * 
	 * BoardParserAbstract parser = getParser(_address);
	 * 
	 * String title = parser.getTitle(parser.getDocument(_address)); String root =
	 * parser.getRoot(); ContentVO data = new ContentVO();
	 * data.setBoard_name(title); data.setSite_address(root);
	 * data.setBoard_address(_address);
	 * 
	 * return data; }
	 */

}

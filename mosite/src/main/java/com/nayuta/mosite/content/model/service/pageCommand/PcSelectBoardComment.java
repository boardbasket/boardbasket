package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardComment;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

/*
 * board의 comment를 가져오는 클래스
 * 
 * */

@Service(value = "pcSelectBoardComment")
public class PcSelectBoardComment extends AbstractPageCls implements CommandInterface {

	@Value("#{new Integer(serverConst['server_const.comment_load_num'])}")
	private int COMMENT_LOAD_NUM;

	@Override
	public void execute(ModelMap modelMap) {
		modelMap.addAttribute("getBoardData", true);
		modelMap.put("result", selectBoardCommentListFunc(modelMap));
	}

	// 게시판 정보보기 눌렀을때 댓글 가져오기
	public JSONObject selectBoardCommentListFunc(SearchData searchData) {
		return selectBoardCommentListFunc(searchData.makeParamMap(weekUtil));
	}

	public JSONObject selectBoardCommentListFunc(Map<String, Object> params) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		String memberNo = getMemberNo();
		// Map<String, Object> params = searchData.makeParamMap();
		params.put("comment_load_num", COMMENT_LOAD_NUM);
		params.put("member_no", memberNo);

		List<BoardComment> bcList = bDao.selectBoardCommentList(params);

		String boardCode = (String) params.get("board_code");

		ContentVO cvo = new ContentVO();

		if (memberNo != null)
			cvo.setMy_comment(bDao.selectMyBoardCommentList(boardCode, memberNo));

		cvo.setBoard_comment_list(bcList);
		cvo.setBoard_code(boardCode);

		// comment_code(offset)가 없을때(처음 가져올때) 댓글의 총 갯수를 넘겨줘서 일정 갯수 이상이면 comment 더보기 버튼을
		// 누를시 commentPage로 넘어갈 수 있도록 함
		if (!params.containsKey("comment_code"))
			cvo.setAll_comment_list_lenght(bDao.selectBoardCommentLength(boardCode));

		try {
			resultObj.put("commentList", com.writeValueAsString(cvo));
			resultObj.put("evalList", bDao.selectEvalValues());// evalItem UI 표시 위해
			resultObj.put("parseGradeList", bDao.selectParseGrade());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 가져온 리스트의 갯수가 원래 가져와야할 갯수보다 작으면 마지막 로드로 간주
		resultObj.put("lastLoad", bcList.size() < COMMENT_LOAD_NUM);

		return resultObj;
	}

}

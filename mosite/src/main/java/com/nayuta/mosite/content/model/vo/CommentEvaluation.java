package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;
import java.util.Date;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

//게시판의 HashTag를 가져오기위한 VO
//개별적으로 쓰진 않고 boardContent,ContentVO 에 포함해서 사용한다
public class CommentEvaluation extends GetPrimaryKeyCodeCls implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4532426225350328533L;
	private String eval_name;
	private String eval_sign;
	private int count;
	private String member_no;
	private Date insert_date;
	private String font_awesome_class;

	public String getEval_name() {
		return eval_name;
	}

	public String getEval_sign() {
		return eval_sign;
	}

	public int getCount() {
		return count;
	}

	public void setEval_name(String eval_name) {
		this.eval_name = eval_name;
	}

	public void setEval_sign(String eval_sign) {
		this.eval_sign = eval_sign;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(Date insert_date) {
		this.insert_date = insert_date;
	}

	public String getMember_no() {
		return member_no;
	}

	public void setMember_no(String member_no) {
		this.member_no = member_no;
	}

	public String getFont_awesome_class() {
		return font_awesome_class;
	}

	public void setFont_awesome_class(String font_awesome_class) {
		this.font_awesome_class = font_awesome_class;
	}

	public CommentEvaluation(String eval_name, String eval_sign, int count, Date insert_date) {
		super();
		this.eval_name = eval_name;
		this.eval_sign = eval_sign;
		this.count = count;
		this.insert_date = insert_date;
	}

	public CommentEvaluation() {
		super();
		
	}

	@Override
	public String toString() {
		return "BoardEvaluation [eval_name=" + eval_name + ", eval_sign=" + eval_sign + ", count=" + count
				+ ", insert_date=" + insert_date + ", eval_code=" + eval_code + "]";
	}

}

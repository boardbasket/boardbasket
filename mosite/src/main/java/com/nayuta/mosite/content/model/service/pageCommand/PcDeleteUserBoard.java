package com.nayuta.mosite.content.model.service.pageCommand;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;

import net.sf.json.JSONObject;

//유저가 주소를 입력하면 게시판이 db에 존재할때 데이터 리턴,없으면 넣고 확인메시지 돌려줌...
@Service("pcDeleteUserBoard")
public class PcDeleteUserBoard extends AbstractPageCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {

		JSONObject result = new JSONObject();
		String userBoardCode = (String) modelMap.get("user_board_code");

		result.put("status", bDao.deleteUserBoard(userBoardCode, getMemberNo()) > 0 ? 1 : 0);
		modelMap.put("result", result);
	}

}

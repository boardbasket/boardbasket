package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service(value = "pcSelectAutoCompleteResult")
public class PcSelectAutoCompleteResult extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	private final String SELECT_SPECIAL_CHAR_PTN;

	private final int AUTO_COMPLETE_MIN_STR_LEN;

	public PcSelectAutoCompleteResult(@Value("#{patterns['patterns.specialChar']}") String sELECT_SPECIAL_CHAR_PTN,
			@Value("#{new Integer(serverConst['server_const.auto_complete_min_str_len'])}") Integer aUTO_COMPLETE_MIN_STR_LEN) {
		super();
		this.SELECT_SPECIAL_CHAR_PTN = sELECT_SPECIAL_CHAR_PTN;
		this.AUTO_COMPLETE_MIN_STR_LEN = aUTO_COMPLETE_MIN_STR_LEN;
	}

	@Override
	public void execute(ModelMap modelMap) {
		SearchData searchData = (SearchData) modelMap.get("searchData");
		JSONObject resultObj = new JSONObject();
		List<Hashtag> hashtagList = new ArrayList<>();
		if (searchData != null) {

			String keyword = searchData.getBoard_name();
			searchData.setTimeMode("week");
			Map<String, Object> params = searchData.makeParamMap(weekUtil); // 기본 시간대

			if (!StringUtils.isEmpty(keyword) && keyword.length() >= AUTO_COMPLETE_MIN_STR_LEN) {// 글자가 1개라도 있으면 관련 결과 리턴
				keyword = keyword.replaceAll(SELECT_SPECIAL_CHAR_PTN, " ");

				List<String> termList = searchTextToKeyword.toKeyword(keyword);

				params.put("keyword", termList);

				hashtagList = bDao.selectAutoCompleteResult(params);

			}

			try {
				resultObj.put("hashtagList", com.writeValueAsString(hashtagList));
			} catch (JsonProcessingException e) {
			}

		}
		modelMap.put("result", resultObj);

	}

}

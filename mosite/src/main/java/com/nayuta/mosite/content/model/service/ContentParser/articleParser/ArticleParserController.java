package com.nayuta.mosite.content.model.service.ContentParser.articleParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.SiteParseException;
import com.nayuta.mosite.common.util.JCacheManagerUtil;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParser;
import com.nayuta.mosite.content.model.vo.ArticleSearchData;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;

//각 사이트에 알맞은 게시판 파서를 불러주고 파싱된 결과를 리턴하는 클래스 입니다.
@Component
public class ArticleParserController {

	private Logger log = LogManager.getLogger();

	private List<ArticleParser> parserList;

	@Value("#{const['const.parsePageNum']}")
	protected String parsePageNumStr;

	@Autowired
	protected ArticleParserController bpc; // 메소드 내부의 메소드를 aop하기 위해 자기 자신 참조

	@Autowired
	protected BoardDaoImpl bDao;

	@Autowired
	private JCacheManagerUtil cacheUtil;

	private int parsePageNum; // 한번에 리턴할 페이지의 갯수

	protected static final String ARTICLE_CACHE_NAME = "articles";

	// protected static final long ARTICLE_REFRESH_THRESHOLD_TIME = 60000;// 1분

	protected static final String ARICLE_REFRESH_TIME_CACHE_NAME = "articlesRefreshTime";

	@PostConstruct
	public void init() {
		parsePageNum = Integer.parseInt(parsePageNumStr);
	}

	protected Map<Integer, List<BoardArticle>> articleWithIndex;

	public ArticleParserController() {
	}

	// key:보드이름 value=게시글리스트 형태로 가져오기
	public LinkedHashMap<String, Map<Integer, List<BoardArticle>>> getBoardsArticleMap(List<ContentVO> dataList,
			boolean firstLoad) {

		LinkedHashMap<String, Map<Integer, List<BoardArticle>>> articleList = new LinkedHashMap<String, Map<Integer, List<BoardArticle>>>();

		if (dataList.size() > 0) {

			for (ContentVO cvo : dataList) {

				if (cvo.getAvailable().equals("N"))// 사용 가능하지 않으면 파싱 안하고 continue
					continue;
				ArticleSearchData asd = cvo.makeSearchArticleData();

				try {
					articleList.putAll(getOneBoardsArticleMap(asd, firstLoad));

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		}

		return articleList;
	}

	// key:보드이름 value=게시글리스트 형태로 가져오기

	public LinkedHashMap<String, Map<Integer, List<BoardArticle>>> getOneBoardsArticleMap(ArticleSearchData asd,
			boolean firstLoad) {

		LinkedHashMap<String, Map<Integer, List<BoardArticle>>> boardArticleList = new LinkedHashMap<String, Map<Integer, List<BoardArticle>>>();
		int _parsePageNum = firstLoad ? 1 : (asd.getBoard_num() < 0 ? parsePageNum : asd.getBoard_num());
		articleWithIndex = new HashMap<Integer, List<BoardArticle>>();

		for (int i = 0; i < _parsePageNum; ++i) {
			asd.setOffset(asd.getOffset() + (i >= 1 ? 1 : 0));
			// 게시글 정보를 가져옴
			List<BoardArticle> articleList = bpc.getBoardArticles(asd);

			articleWithIndex.put(asd.getOffset(), articleList);
		}
		boardArticleList.put(asd.getBoard_name(), articleWithIndex);

		return boardArticleList;
	}

	// 태그 파싱할때 쓰는 함수
	/*
	 * public List<BoardArticle> getBoardsArticleList(ArticleSearchData asd, int
	 * pageNum, String lastArticleAddress) { List<BoardArticle> result = new
	 * ArrayList<>();
	 * 
	 * for (int i = 0; i < pageNum; ++i) { asd.setOffset(asd.getOffset() + (i >= 1 ?
	 * 1 : 0)); List<BoardArticle> articleList = bpc.getBoardArticles(asd); Long
	 * currArticleNo = articleList.stream().mapToLong(k ->
	 * k.getArticle_no()).max().orElse(new Long(0));
	 * 
	 * result.addAll(articleList);
	 * 
	 * if (currArticleNo < thresholdArticleNo) break; } // 이전 최신 글 번호보다 더 큰 글 번호를 가진
	 * 글만 리턴 return result.stream().filter(k -> k.getArticle_no() >
	 * thresholdArticleNo).collect(Collectors.toList());
	 * 
	 * }
	 */

	// 게시글을 가져오는 함수
	// @Cacheable(value = "articles", key = "#asd.makeCacheName()")
	@SuppressWarnings("unchecked")
	public List<BoardArticle> getBoardArticles(ArticleSearchData asd) {
		List<BoardArticle> articles = null;
		ArticleParser articleParser = getParser(asd.getSite_address());

		if (asd.isRefresh())
			removeCache(asd, articleParser);

		if (cacheUtil.getCacheEntryValueExist(ARTICLE_CACHE_NAME, asd.makeCacheName()))
			return (List<BoardArticle>) cacheUtil.getCacheEntryValue(ARTICLE_CACHE_NAME, asd.makeCacheName());
		else {
			articles = articleParser.processParsing(asd);
			cacheUtil.insertEntryToCache(ARTICLE_CACHE_NAME, asd.makeCacheName(), articles);
		}

		return articles;
	}

	// @CacheEvict(value = "articles", key = "#asd.makeCacheName()")
	public void removeCache(ArticleSearchData asd, ArticleParser articleParser) {
		// 새로고침의 요청의 사이가 각 파서가 가지는 시간 간격 이상이 되야 새로고침을 함
		long currentTime = System.currentTimeMillis();
		long refreshTime = Long.parseLong(articleParser.getArticleRefreshTime());

		Object time = cacheUtil.getCacheEntryValue(ARICLE_REFRESH_TIME_CACHE_NAME, asd.getBoard_address());

		if (time == null || (currentTime - (long) time > refreshTime)) {
			cacheUtil.insertEntryToCache(ARICLE_REFRESH_TIME_CACHE_NAME, asd.getBoard_address(), currentTime);
			cacheUtil.evictCacheEntry(ARTICLE_CACHE_NAME, asd.makeCacheName());
		}

	}

	public ArticleParser getParser(String address) {

		for (ArticleParser ap : parserList)
			if (ap.checkParser(address))
				return ap;

		throw new SiteParseException("지원하지 않는 사이트 입니다.");
	}

	public List<ArticleParser> getParserList() {
		return parserList;
	}

	public void setParserList(List<ArticleParser> parserList) {
		this.parserList = parserList;
	}

	public List<WordFrequencyData> parseWordFrequency(List<ContentVO> boardList) {

		List<WordFrequencyData> result = new ArrayList<>();

		for (int i = 0; i < boardList.size(); ++i) {

			ContentVO board = boardList.get(i);
			ArticleParser ap = getParser(board.getBoard_address());
			log.debug(board.getBoard_name() + "파싱중 ..." + (i + 1) + " /" + boardList.size());
			result.add(ap.parseWordFrequency(board));

		}

		return result;
	}

}

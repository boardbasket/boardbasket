package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.SearchTextToKeyword;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service("pcSelectRelativeTag")
public class PcSelectRelativeTag extends AbstractPageCls implements CommandInterface {

	@Autowired
	private SearchTextToKeyword searchTextToKeyword;

	@Override
	public void execute(ModelMap modelMap) {

		SearchData searchData = (SearchData) modelMap.get("searchData");
		searchData.setKeyword(searchTextToKeyword.toKeyword(searchData.getBoard_name()));

		modelMap.addAttribute("result", selectRelativeTagFunc(searchData));

	}

	public JSONObject selectRelativeTagFunc(SearchData searchData) {
		JSONObject result = new JSONObject();
		if (searchData != null) {

			if (searchData.getKeyword() == null || searchData.getKeyword().isEmpty())
				return result;

			List<Hashtag> relativeTag = new ArrayList<>();
			List<String> keyword = searchTextToKeyword.toKeyword(searchData.getBoard_name());

			Map<String, Object> options = searchData.makeParamMap(weekUtil);
			options.put("relativeTagKeywordTarget", keyword.get(0));// relativeTag는 키워드 하나만 검색가능
			relativeTag = bDao.selectRelativeTag(options);// 연관 태그

			try {
				result.put("relativeTag", com.writeValueAsString(relativeTag));
			} catch (JsonProcessingException e) {
			}

			return result;
		}

		else
			return null;
	}

}

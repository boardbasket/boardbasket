package com.nayuta.mosite.content.model.vo;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

public class BoardCategory extends GetPrimaryKeyCodeCls {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2166981693271018123L;
	private String category_name;
	private String category_address;
	private Integer val;

	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public String getCategory_address() {
		return category_address;
	}

	public void setCategory_address(String category_address) {
		this.category_address = category_address;
	}

	public Integer getVal() {
		return val;
	}

	public void setVal(Integer val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return "BoardCategory [category_name=" + category_name + ", category_address=" + category_address + ", val=" + val + "]";
	}

	public BoardCategory(String board_code, String category_name, String category_address) {
		super();
		this.board_code = board_code;
		this.category_name = category_name;
		this.category_address = category_address;
	}

	public BoardCategory(String category_name, String category_address) {
		super();
		this.category_name = category_name;
		this.category_address = category_address;
	}

	public BoardCategory() {
		super();
	}

}

package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import org.springframework.stereotype.Component;

@Component("boardTypePpomppu")
public class BoardTypePpomppu extends BoardTypeAbstract {

	public static final String TYPE_GALLERY = "TYPE_GALLERY";
	public static final String TYPE_MARKET = "TYPE_MARKET";
	public static final String TYPE_BEST = "TYPE_BEST";

	public BoardTypePpomppu() {
		typeList.add(TYPE_GALLERY);
		typeList.add(TYPE_MARKET);
		typeList.add(TYPE_BEST);
	}

}
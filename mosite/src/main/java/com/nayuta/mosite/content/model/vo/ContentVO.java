package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.service.pageCommand.MakeSearchArticleData;
import com.nayuta.mosite.content.model.vo.validatorGroup.InsertGroup;
import com.nayuta.mosite.content.model.vo.validatorGroup.OrderGroup;
import com.nayuta.mosite.content.model.vo.validatorGroup.UpdateGroup;

/*
 * 대부분의  board 관련 데이터들을 처리 가능한 VO
 * 
 * */
public class ContentVO extends GetPrimaryKeyCodeCls implements MakeSearchArticleData, Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	@JsonIgnore
	private int offset = 1;
	private String board_name;
	private String board_address;
	private String board_type;
	private String site_name;
	private String site_address;
	private String site_color;
	private String available;
	private String search_available;

	private List<Hashtag> hashtag_code_list;// 사용자의 게시판 해시태그
	private List<Hashtag> popular_hashtag_code_list; // 이 게시판의 인기 해시태그 3개
	private BoardData boardData;// 게시판의 일반적인 데이터(article_no 등등...)
	private List<CommentEvaluation> comment_evaluation_list;// 이 게시판(board)의 평가
	private List<CommentEvaluation> eval_count_list;// 이 게시판의 각 eval 의 갯수
	private List<ContentVO> similar_board_list;// 비슷한 게시판 리스트
	private List<BoardComment> board_comment_list;// 게시판의 comment 리스트(내것 제외)
	private BoardComment my_comment;// 나의 comment
	private int all_comment_list_lenght; // 게시판 comment의 총 갯수
	private int positive; // 긍정 평가의 갯수
	private int negative; // 부정 평가의 갯수
	private String searchAddress;
	private List<BoardCategory> board_category_list;
	private int val; // 관련성 점수
	private List<String> child_board_addr;// 같은 게시판 그룹 주소(루리웹 전용)

	public ContentVO(String board_name, String board_address) {
		super();
		this.board_name = board_name;
		this.board_address = board_address;

	}

	public int getVal() {
		return val;
	}

	public void setVal(int val) {
		this.val = val;
	}

	public String getSearchAddress() {
		return searchAddress;
	}

	public void setSearchAddress(String searchAddress) {
		this.searchAddress = searchAddress;
	}

	public List<Hashtag> getHashtag_code_list() {
		return hashtag_code_list;
	}

	public List<BoardCategory> getBoard_category_list() {
		return board_category_list;
	}

	public void setBoard_category_list(List<BoardCategory> board_category_list) {
		this.board_category_list = board_category_list;
	}

	public void setHashtag_code_list(List<Hashtag> hashtag_code_list) {
		this.hashtag_code_list = hashtag_code_list;
	}

	public int getPositive() {
		return positive;
	}

	public int getNegative() {
		return negative;
	}

	public void setPositive(int positive) {
		this.positive = positive;
	}

	public void setNegative(int negative) {
		this.negative = negative;
	}

	@NotBlank(message = "탭 코드가 비어있습니다.", groups = { InsertGroup.class, OrderGroup.class, UpdateGroup.class })
	public String getUser_tab_code() {
		return user_tab_code;
	}

	public void setUser_tab_code(String user_tab_code) {
		this.user_tab_code = user_tab_code;
	}

	@NotBlank(message = "유저 게시판 코드가 비어있습니다.", groups = { OrderGroup.class, UpdateGroup.class })
	public String getUser_board_code() {
		return user_board_code;
	}

	public void setUser_board_code(String user_board_code) {
		this.user_board_code = user_board_code;
	}

	// @NotBlank(message = "게시판 코드가 비어있습니다.", groups = { InsertGroup.class })
	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public String getBoard_address() {
		return board_address;
	}

	public void setBoard_address(String board_address) {
		this.board_address = board_address;
	}

	public String getBoard_type() {
		return board_type;
	}

	public void setBoard_type(String board_type) {
		this.board_type = board_type;
	}

	public String getSite_code() {
		return site_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	@Min(value = 1, message = "페이지 번호가 1보다 작습니다.")
	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getSite_color() {
		return site_color;
	}

	public void setSite_color(String site_color) {
		this.site_color = site_color;
	}

	public BoardData getBoardData() {
		return boardData;
	}

	public void setBoardData(BoardData boardData) {
		this.boardData = boardData;
	}

	public List<CommentEvaluation> getComment_evaluation_list() {
		return comment_evaluation_list;
	}

	public void setComment_evaluation_list(List<CommentEvaluation> comment_evaluation_list) {
		this.comment_evaluation_list = comment_evaluation_list;
	}

	public List<ContentVO> getSimilar_board_list() {
		return similar_board_list;
	}

	public void setSimilar_board_list(List<ContentVO> similar_board_list) {
		this.similar_board_list = similar_board_list;
	}

	public ContentVO() {
		super();
	}

	public List<Hashtag> getPopular_hashtag_code_list() {
		return popular_hashtag_code_list;
	}

	public void setPopular_hashtag_code_list(List<Hashtag> popular_hashtag_code_list) {
		this.popular_hashtag_code_list = popular_hashtag_code_list;
	}

	public List<BoardComment> getBoard_comment_list() {
		return board_comment_list;
	}

	public BoardComment getMy_comment() {
		return my_comment;
	}

	public void setBoard_comment_list(List<BoardComment> board_comment_list) {
		this.board_comment_list = board_comment_list;
	}

	public void setMy_comment(BoardComment my_comment) {
		this.my_comment = my_comment;
	}

	public int getAll_comment_list_lenght() {
		return all_comment_list_lenght;
	}

	public void setAll_comment_list_lenght(int all_comment_list_lenght) {
		this.all_comment_list_lenght = all_comment_list_lenght;
	}

	public List<CommentEvaluation> getEval_count_list() {
		return eval_count_list;
	}

	public void setEval_count_list(List<CommentEvaluation> eval_count_list) {
		this.eval_count_list = eval_count_list;
	}

	public String getSearch_available() {
		return search_available;
	}

	public void setSearch_available(String search_available) {
		this.search_available = search_available;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void pushPrimaryKey(Map<String, Object> codeMap) {

		super.pushPrimaryKey(codeMap);
		if (codeMap.containsKey("hashtagCodeList"))
			this.hashtag_code_list = (List<Hashtag>) codeMap.get("hashtagCodeList");

		if (codeMap.containsKey("boardCategoryList"))
			this.board_category_list = (List<BoardCategory>) codeMap.get("boardCategoryList");

		if (codeMap.containsKey("popularHashtagCodeList"))
			this.popular_hashtag_code_list = (List<Hashtag>) codeMap.get("popularHashtagCodeList");
	}

	@Override
	public Map<String, Object> pullPrimaryKey() {
		super.pullPrimaryKey();
		if (hashtag_code_list != null)
			codeMap.put("hashtagCodeList", hashtag_code_list);

		if (board_category_list != null)
			codeMap.put("boardCategoryList", board_category_list);

		if (popular_hashtag_code_list != null)
			codeMap.put("popularHashtagCodeList", popular_hashtag_code_list);

		return codeMap;

	}

	public ContentVO(String board_name, String board_address, String board_type, String site_name, String site_address,
			String available) {
		super();
		this.board_name = board_name;
		this.board_address = board_address;
		this.board_type = board_type;
		this.site_name = site_name;
		this.site_address = site_address;
		this.available = available;
	}

	// 게시판 파싱할때(집어넣으려는 목적) 만드는 생성자
	public ContentVO(String root, String title, String baseUri, String available, String boardType,
			List<BoardCategory> boardCategory) {
		this.site_address = root;
		this.board_name = title;
		this.board_address = baseUri;
		this.available = available;
		this.board_type = boardType;
		this.board_category_list = boardCategory;

	}

	public void applySiteData(Site s) {
		this.site_name = s.getSite_name();
		this.site_address = s.getSite_address();
		this.site_color = s.getSite_color();
	}

	public ArticleSearchData makeSearchArticleData() {

		return new ArticleSearchData(board_code, user_board_code, offset, board_address, board_name, site_code, site_name, null,
				null, board_type, false, null, site_address);
	}

	@Override
	public ContentVO clone() {
		ContentVO cvo = null;
		try {
			cvo = (ContentVO) super.clone();
		} catch (CloneNotSupportedException e) {

			e.printStackTrace();
		}
		return cvo;
	}

	public List<String> getChild_board_addr() {
		return child_board_addr;
	}

	public void setChild_board_addr(List<String> child_board_addr) {
		this.child_board_addr = child_board_addr;
	}

}

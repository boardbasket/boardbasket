package com.nayuta.mosite.content.model.vo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.nayuta.mosite.common.util.CustomDateSerializer;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

public class BoardArticle extends GetPrimaryKeyCodeCls implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private long article_no;
	private String title;
	private int view_cnt = 0;
	private String upload_date;
	private String content_address; // 웹에서 가져올때의 게시글 주소
	private String article_address;// db에서 가져올때의 게시글 주소
	private String content;
	private String isNotice;
	@JsonIgnore
	private Integer replyNum = 0;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private int offset;
	@JsonProperty("y")
	private Integer year;// 주차 계산시의 년도(실제 연도와 다를 수 있음)
	@JsonProperty("m")
	private Integer month;// 주차 계산시의 월 (실제 월과 다를 수 있음)
	@JsonProperty("w")
	private Integer week;// 몇주차인지
	@JsonProperty("d")
	@JsonSerialize(using = CustomDateSerializer.class)
	private LocalDate date;
	private String site_name;
	private String site_color;
	private String site_address;
	private String board_name;
	private String board_address;
	private Integer recommend;
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private boolean lastArticle = false;
	private List<String> tagList = new ArrayList<>();

	public BoardArticle(long article_no) {
		super();
		this.article_no = article_no;
	}

	public BoardArticle() {
		super();
	}

	public Integer getRecommend() {
		return recommend;
	}

	public void setRecommend(Integer recommend) {
		this.recommend = recommend;
	}

	public String getBoard_address() {
		return board_address;
	}

	public void setBoard_address(String board_address) {
		this.board_address = board_address;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public boolean getLastArticle() {
		return lastArticle;
	}

	public void setLastArticle(boolean lastArticle) {
		this.lastArticle = lastArticle;
	}

	public String getTitle() {
		return title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUpload_date() {
		return upload_date;
	}

	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}

	public String getContent_address() {
		return content_address;
	}

	public void setContent_address(String content_address) {
		this.content_address = content_address;
	}

	public String getIsNotice() {
		return isNotice;
	}

	public void setIsNotice(String isNotice) {
		this.isNotice = isNotice;
	}

	public int getView_cnt() {
		return view_cnt;
	}

	public void setView_cnt(int view_cnt) {
		this.view_cnt = view_cnt;
	}

	public void setReplyNum(Integer replyNum) {
		this.replyNum = replyNum;
	}

	public int getOffset() {
		return offset;
	}

	public Integer getReplyNum() {
		return replyNum;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public BoardArticle(boolean lastArticle) {
		super();
		this.lastArticle = lastArticle;
	}

	public long getArticle_no() {
		return article_no;
	}

	public void setArticle_no(long article_no) {
		this.article_no = article_no;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getArticle_address() {
		return article_address;
	}

	public void setArticle_address(String article_address) {
		this.article_address = article_address;
	}

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_color() {
		return site_color;
	}

	public void setSite_color(String site_color) {
		this.site_color = site_color;
	}

	public BoardArticle(String title, int view_cnt, String upload_date, String content_address, String isNotice, int replyNum,
			int offset, boolean lastArticle) {
		super();
		this.title = title;
		this.view_cnt = view_cnt;
		this.upload_date = upload_date;
		this.content_address = content_address;
		this.isNotice = isNotice;
		this.replyNum = replyNum;
		this.offset = offset;
		this.lastArticle = lastArticle;
	}

	public BoardArticle(String title, int view_cnt, String upload_date, String content_address, String content, String isNotice,
			int replyNum, int offset, boolean lastArticle) {
		super();
		this.title = title;
		this.view_cnt = view_cnt;
		this.upload_date = upload_date;
		this.content_address = content_address;
		this.content = content;
		this.isNotice = isNotice;
		this.replyNum = replyNum;
		this.offset = offset;
		this.lastArticle = lastArticle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((article_address == null) ? 0 : article_address.hashCode());
		result = prime * result + (int) (article_no ^ (article_no >>> 32));
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((content_address == null) ? 0 : content_address.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((isNotice == null) ? 0 : isNotice.hashCode());
		result = prime * result + (lastArticle ? 1231 : 1237);
		result = prime * result + offset;
		result = prime * result + replyNum;
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((upload_date == null) ? 0 : upload_date.hashCode());
		result = prime * result + view_cnt;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardArticle other = (BoardArticle) obj;
		if (article_address == null) {
			if (other.article_address != null)
				return false;
		} else if (!article_address.equals(other.article_address))
			return false;
		if (article_no != other.article_no)
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (content_address == null) {
			if (other.content_address != null)
				return false;
		} else if (!content_address.equals(other.content_address))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (isNotice == null) {
			if (other.isNotice != null)
				return false;
		} else if (!isNotice.equals(other.isNotice))
			return false;
		if (lastArticle != other.lastArticle)
			return false;
		if (offset != other.offset)
			return false;
		if (replyNum != other.replyNum)
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		if (tagList == null) {
			if (other.tagList != null)
				return false;
		} else if (!tagList.equals(other.tagList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (upload_date == null) {
			if (other.upload_date != null)
				return false;
		} else if (!upload_date.equals(other.upload_date))
			return false;
		if (view_cnt != other.view_cnt)
			return false;
		return true;
	}

	public Integer getYear() {
		return year;
	}

	public Integer getMonth() {
		return month;
	}

	public Integer getWeek() {
		return week;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

}

package com.nayuta.mosite.content.model.service.pageCommand;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nayuta.mosite.common.util.CustomObjectMapper;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.SearchData;

import net.sf.json.JSONObject;

@Service(value = "pcSelectRelativeBoard")
public class PcSelectRelativeBoard extends AbstractPageCls implements CommandInterface {

	private final float RELATVE_BOARD_RATIO;
	private final int RELATIVE_BOARD_MAX_NUM;

	public PcSelectRelativeBoard(
			@Value("#{new Float(serverConst['server_const.relative_board_ratio'])}") float rELATVE_BOARD_RATIO,
			@Value("#{new Integer(serverConst['server_const.relative_board_max_num'])}") int rELATIVE_BOARD_MAX_NUM) {
		super();
		RELATVE_BOARD_RATIO = rELATVE_BOARD_RATIO;
		RELATIVE_BOARD_MAX_NUM = rELATIVE_BOARD_MAX_NUM;
	}

	@Override
	public void execute(ModelMap modelMap) {

		SearchData sd = (SearchData) modelMap.get("searchData");

		modelMap.put("result", selectRelativeBoardFunc(sd));
	}

	public JSONObject selectRelativeBoardFunc(SearchData sd) {
		JSONObject resultObj = new JSONObject();// 결과 오브젝트
		Map<String, Object> options = sd.makeParamMap(weekUtil);
		options.put("RELATIVE_BOARD_MAX_NUM", RELATIVE_BOARD_MAX_NUM);
		options.put("RELATVE_BOARD_RATIO", RELATVE_BOARD_RATIO);
		List<ContentVO> relativeBoardList = bDao.selectRelativeBoard(options);

		CustomObjectMapper customObjectMapper = new CustomObjectMapper();

		try {
			resultObj.put("relativeBoard", customObjectMapper.writeValueAsString(relativeBoardList));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultObj;

	}

}

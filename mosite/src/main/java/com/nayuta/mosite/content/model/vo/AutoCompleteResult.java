package com.nayuta.mosite.content.model.vo;

import java.util.List;
import java.util.Map;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.service.pageCommand.MakeSearchArticleData;

public class AutoCompleteResult extends GetPrimaryKeyCodeCls implements MakeSearchArticleData {

	private static final long serialVersionUID = -8888802172360208857L;
	private String site_name;
	private String board_name;
	private String board_address;
	private String board_type;
	private String site_address;
	private String favicon_address;
	private String site_color;
	private String exist;
	private int addCount;
	private String available;
	private String db_board;
	private List<Hashtag> hashtag_code_list;

	public AutoCompleteResult() {
		super();

	}

	public AutoCompleteResult(String site_code, String site_name, String board_name, String board_address, String board_code,
			String board_type, String site_address, String exist, int addCount, String available,
			List<Hashtag> hashtag_code_list) {
		super();
		this.site_code = site_code;
		this.site_name = site_name;
		this.board_name = board_name;
		this.board_address = board_address;
		this.board_code = board_code;
		this.board_type = board_type;
		this.site_address = site_address;
		this.exist = exist;
		this.addCount = addCount;
		this.available = available;
		this.hashtag_code_list = hashtag_code_list;
	}

	public AutoCompleteResult(ContentVO b) {
		setBoardData(b);
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getBoard_type() {
		return board_type;
	}

	public void setBoard_type(String board_type) {
		this.board_type = board_type;
	}

	public String getSite_code() {
		return site_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public String getDb_board() {
		return db_board;
	}

	public void setDb_board(String db_board) {
		this.db_board = db_board;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_color() {
		return site_color;
	}

	public void setSite_color(String site_color) {
		this.site_color = site_color;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public String getBoard_address() {
		return board_address;
	}

	public void setBoard_address(String board_address) {
		this.board_address = board_address;
	}

	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		this.site_address = site_address;
	}

	public String getExist() {
		return exist;
	}

	public void setExist(String exist) {
		this.exist = exist;
	}

	public int getAddCount() {
		return addCount;
	}

	public void setAddCount(int addCount) {
		this.addCount = addCount;
	}

	public List<Hashtag> getHashtag_code_list() {
		return hashtag_code_list;
	}

	public void setHashtag_code_list(List<Hashtag> hashtag_code_list) {
		this.hashtag_code_list = hashtag_code_list;
	}

	@Override
	public Map<String, Object> pullPrimaryKey() {

		super.pullPrimaryKey();
		if (hashtag_code_list != null)
			codeMap.put("hashtagCodeList", hashtag_code_list);

		return codeMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void pushPrimaryKey(Map<String, Object> codeMap) {
		super.pushPrimaryKey(codeMap);

		if (codeMap.containsKey("hashtagCodeList"))
			this.hashtag_code_list = (List<Hashtag>) codeMap.get("hashtagCodeList");
	}

	@Override
	public ArticleSearchData makeSearchArticleData() {

		return new ArticleSearchData(board_code, user_board_code, 1, board_address, board_name, site_code, site_name, null, null,
				board_type, "N", false, null, null, db_board);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + addCount;
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((board_address == null) ? 0 : board_address.hashCode());
		result = prime * result + ((board_name == null) ? 0 : board_name.hashCode());
		result = prime * result + ((board_type == null) ? 0 : board_type.hashCode());
		result = prime * result + ((db_board == null) ? 0 : db_board.hashCode());
		result = prime * result + ((exist == null) ? 0 : exist.hashCode());
		result = prime * result + ((favicon_address == null) ? 0 : favicon_address.hashCode());
		result = prime * result + ((hashtag_code_list == null) ? 0 : hashtag_code_list.hashCode());
		result = prime * result + ((site_address == null) ? 0 : site_address.hashCode());
		result = prime * result + ((site_color == null) ? 0 : site_color.hashCode());
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutoCompleteResult other = (AutoCompleteResult) obj;
		if (addCount != other.addCount)
			return false;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (board_address == null) {
			if (other.board_address != null)
				return false;
		} else if (!board_address.equals(other.board_address))
			return false;
		if (board_name == null) {
			if (other.board_name != null)
				return false;
		} else if (!board_name.equals(other.board_name))
			return false;
		if (board_type == null) {
			if (other.board_type != null)
				return false;
		} else if (!board_type.equals(other.board_type))
			return false;
		if (db_board == null) {
			if (other.db_board != null)
				return false;
		} else if (!db_board.equals(other.db_board))
			return false;
		if (exist == null) {
			if (other.exist != null)
				return false;
		} else if (!exist.equals(other.exist))
			return false;
		if (favicon_address == null) {
			if (other.favicon_address != null)
				return false;
		} else if (!favicon_address.equals(other.favicon_address))
			return false;
		if (hashtag_code_list == null) {
			if (other.hashtag_code_list != null)
				return false;
		} else if (!hashtag_code_list.equals(other.hashtag_code_list))
			return false;
		if (site_address == null) {
			if (other.site_address != null)
				return false;
		} else if (!site_address.equals(other.site_address))
			return false;
		if (site_color == null) {
			if (other.site_color != null)
				return false;
		} else if (!site_color.equals(other.site_color))
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		return true;
	}

	public String getFavicon_address() {
		return favicon_address;
	}

	public void setFavicon_address(String favicon_address) {
		this.favicon_address = favicon_address;
	}

	// 파싱된 보드 ContentVO 를 AutoCompleteResult 형태로 바꾸기 위한 생성자
	public void setBoardData(ContentVO b) {
		this.board_name = b.getBoard_name();
		this.site_name = b.getSite_name();
		this.board_address = b.getBoard_address();
		this.site_code = b.getSite_code();
		this.site_color = b.getSite_color();
		this.board_code = b.getBoard_code();
		this.site_address = b.getSite_address();

	}

}

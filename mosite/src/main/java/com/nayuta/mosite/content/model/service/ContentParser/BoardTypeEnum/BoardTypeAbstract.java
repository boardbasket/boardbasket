package com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BoardTypeAbstract {

	public static final String TYPE_DEFAULT = "TYPE_DEFAULT";

	protected List<String> typeList = new ArrayList<>(Arrays.asList(new String[] { TYPE_DEFAULT }));

	public List<String> getTypeList() {

		return typeList;
	}

}

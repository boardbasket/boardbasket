package com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.ContentParser.BoardTypeEnum.BoardTypeAbstract;
import com.nayuta.mosite.content.model.vo.ContentVO;

@Component(value = "boardParserRuliweb")
public class BoardParserRuliweb extends BoardParserAbstract {

	protected final String boardTitleSelector;
	private final String NEW_BOARD_PATTERN;
	private final String NEW_BOARD_SWAP_WORD;
	private final Pattern URL_REMOVE_FILTER_1;
	private final Pattern URL_REMOVE_FILTER_2;
	private final String URL_REMOVE_REPLACE_2;
	private final String BOARD_GROUP;// 시리즈 인지 확인하는 셀렉트

	@PostConstruct
	public void init() {
		initializeParser();
	}

	public BoardParserRuliweb(@Value("#{selectors['RULIWEB_ROOT']}") String root,
			@Value("#{selectors['RULIWEB_SITE_ADDR_REGEX']}") String siteAddrRegex,
			@Autowired @Qualifier("boardTypeRuliweb") BoardTypeAbstract typeList,
			@Value("#{selectors['RULIWEB_PARSER_NAME']}") String parserName,
			@Value("#{selectors['RULIWEB_TITLE_SELECTOR']}") String boardTitleSelector,
			@Value("#{selectors['DEFAULT_ARTICLE_TIME_OUT']}") String timeOut,
			@Value("#{selectors['RULIWEB_POPULAR_BOARD']}") String popularBoard,
			@Value("#{selectors['RULIWEB_POPULAR_ADDR']}") String popularAddr,
			@Value("#{selectors['RULIWEB_NEW_BOARD']}") String newBoard,
			@Value("#{selectors['RULIWEB_NEW_BOARD_ADDR']}") String newBoardAddr,
			@Value("#{selectors['RULIWEB_NEW_BOARD_REGEX']}") String newBoardRegex,
			@Value("#{selectors['RULIWEB_NEW_BOARD_SWAP_WORD']}") String newBoardSwapWord,
			@Value("#{selectors['RULIWEB_URL_REMOVE_FILTER_1']}") String uRL_REMOVE_FILTER_1,
			@Value("#{selectors['RULIWEB_URL_REMOVE_FILTER_2']}") String uRL_REMOVE_FILTER_2,
			@Value("#{selectors['RULIWEB_URL_REMOVE_REPLACE_2']}") String uRL_REMOVE_REPLACE_2,
			@Value("#{selectors['RULIWEB_BOARD_GROUP']}") String bOARD_GROUP,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MIN'])}") int sleepMin,
			@Value("#{new Integer(selectors['DEFAULT_SLEEP_PER_REQUEST_MAX'])}") int sleepMax,
			@Value("#{new Integer(selectors['DEFAULT_PAGE_LOAD_RETRY_NUM'])}") int retryNum) {
		super(root, siteAddrRegex, typeList, parserName, timeOut, null, null, popularBoard, popularAddr, newBoard, newBoardAddr,
				sleepMin, sleepMax, retryNum);
		this.boardTitleSelector = boardTitleSelector;
		this.NEW_BOARD_PATTERN = newBoardRegex;
		this.NEW_BOARD_SWAP_WORD = newBoardSwapWord;
		this.URL_REMOVE_FILTER_1 = Pattern.compile(uRL_REMOVE_FILTER_1);
		this.URL_REMOVE_FILTER_2 = Pattern.compile(uRL_REMOVE_FILTER_2);
		this.URL_REMOVE_REPLACE_2 = uRL_REMOVE_REPLACE_2;
		this.BOARD_GROUP = bOARD_GROUP;

	}

	@Override
	public String urlPreProcess(String addr) {

		String targetAddress = httpToHttps(addr);

		Pattern checkMobile = Pattern.compile(checkMobileRegex);
		Matcher mobileMatcher = checkMobile.matcher(targetAddress);

		// 모바일 주소를 pc 주소로 바꾸는 작업
		if (mobileMatcher.find())
			targetAddress = targetAddress.replace("https://m.", "https://bbs.");

		Matcher m1 = URL_REMOVE_FILTER_1.matcher(targetAddress);
		Matcher m2 = URL_REMOVE_FILTER_2.matcher(targetAddress);

		if (m1.find())
			targetAddress = m1.replaceAll("");
		else if (m2.find())
			targetAddress = m2.replaceAll(URL_REMOVE_REPLACE_2);

		Pattern pattern = Pattern.compile(urlFilterRegex);

		Matcher matcher = pattern.matcher(targetAddress);

		if (matcher.find())
			return matcher.group();
		else
			return null;

	}

	public String getIgnoreBoard() {
		return ignoreBoard;
	}

	public void setIgnoreBoard(String ignoreBoard) {
		this.ignoreBoard = ignoreBoard;
	}

	@Override
	public String getTitle(Document doc) {

		Element elem = doc.selectFirst(boardTitleSelector);

		if (elem != null)
			return elem.text();
		else
			return super.getTitle(doc);
	}

	@Override
	public List<String> getNewBoardAddrList() {

		List<String> result = new ArrayList<>();

		if (StringUtils.isEmpty(newBoard) || StringUtils.isEmpty(newBoardAddr))
			return result;
		else {

			try {
				Document doc = getDocument(newBoardAddr);

				Elements elems = doc.select(newBoard);

				for (Element elem : elems) {

					String addr = elem.attr("href");
					String swapped = addr.replaceAll(NEW_BOARD_PATTERN, NEW_BOARD_SWAP_WORD);
					result.add(urlPreProcess(swapped));
				}
			} catch (Exception e) {
				log.info(e.getMessage() + "새 게시판 주소 파싱중 오류 발생");
			}

		}
		return result;

	}

	@Override
	public ContentVO parseBoardFunc(Document doc) {

		ContentVO board = super.parseBoardFunc(doc);

		Elements elems = doc.select(BOARD_GROUP);
		if (elems.size() > 0) {

			board.setChild_board_addr(elems.stream().map(k -> {

				return urlPreProcess(k.attr("value"));

			}).collect(Collectors.toList()));

		}

		return board;
	}

}

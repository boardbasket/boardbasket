package com.nayuta.mosite.common.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.nayuta.mosite.admin.model.vo.FileVO;
import com.nayuta.mosite.common.exceptions.FileException;

@Component
public class FileUtil {

	@Value("#{fileConfig['fileConfig.path']}")
	private String filePath;

	@Value("#{fileConfig['fileConfig.iconFileExtension']}")
	private String iconFileExtension;

	@Value("#{fileConfig['fileConfig.etcFileExtension']}")
	private String etcFileExtension;

	protected Logger log = LogManager.getLogger();

	public enum FILE_TYPE {

		ICON("ICON"), ETC("ETC");
		private final String name;

		@Override
		public String toString() {
			return name;
		}

		private FILE_TYPE(String name) {
			this.name = name;
		}

	}

	public FileVO insertFile(MultipartFile multipartFile, FILE_TYPE fileType) throws FileException {

		String originalFileName = null;
		String originalFileExtension = null;
		String storedFileName = null;
		Pattern extensionPattern = fileType.equals(FILE_TYPE.ETC) ? Pattern.compile(etcFileExtension)
				: Pattern.compile(iconFileExtension);

		String datePath = getDatePath();
		File folder = new File(filePath + datePath);

		if (!folder.exists())
			folder.mkdirs();

		if (!multipartFile.isEmpty()) {
			try {
				originalFileName = new String(multipartFile.getOriginalFilename().getBytes("8859_1"), "utf-8");
			} catch (UnsupportedEncodingException e1) {
				throw new FileException("파일을 업로드 하는중 문제가 생겼습니다.");
			}

			originalFileExtension = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);

			Matcher matcher = extensionPattern.matcher(originalFileExtension);
			if (!matcher.find())
				throw new FileException("허용되지 않는 확장자 입니다.");

			String fileId = ShorthenUUID.getInstance().getShorthenUUID();
			storedFileName = fileId + "." + originalFileExtension;

			File file = new File(folder, storedFileName);
			try {
				multipartFile.transferTo(file);
			} catch (Exception e) {
				throw new FileException("파일 저장중 오류가 생겼습니다.", e);
			}

			log.debug("----file upload----");
			log.debug(file + "가 업로드 됨");
			log.debug("-------------------");
			return new FileVO(fileId, originalFileName, storedFileName, datePath + File.separator + storedFileName, fileType,
					multipartFile.getSize());

		} else
			return null;

	}

	public boolean deleteFile(FileVO fileVo) {

		File file = new File(filePath + fileVo.getStored_file_name());
		boolean result = false;
		if (file.exists()) {
			if (file.delete()) {
				result = true;
			} else {
				result = false;
				log.error("파일" + fileVo.getOriginal_file_name() + "이 존재하지만 삭제에 실패했습니다.");
			}
		} else
			log.error("파일" + fileVo.getOriginal_file_name() + "가 존재하지 않습니다.");

		if (!result)
			throw new FileException("삭제에 실패했습니다.");
		else
			return result;
	}

	private String getDatePath() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
		String path = sdf.format(new Date());

		return path;
	}

}

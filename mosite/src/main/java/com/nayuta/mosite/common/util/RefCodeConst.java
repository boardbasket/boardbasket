package com.nayuta.mosite.common.util;

public interface RefCodeConst {

	// tab,userBoard,board,hashtag,eval 순서

	// refCodeMapName
	public static final String userTabCodeRefMap = "userTabCodeRefMap";
	public static final String userBoardCodeRefMap = "userBoardCodeRefMap";
	public static final String boardCodeRefMap = "boardCodeRefMap";
	public static final String siteCodeRefMap = "siteCodeRefMap";
	public static final String hashtagCodeRefMap = "hashtagCodeRefMap";
	public static final String evalCodeRefMap = "evalCodeRefMap";
	public static final String commentCodeRefMap = "commentCodeRefMap";
	public static String[] refCodeNames = { userTabCodeRefMap, userBoardCodeRefMap, boardCodeRefMap, siteCodeRefMap,
			hashtagCodeRefMap, evalCodeRefMap, commentCodeRefMap, userTabCodeRefMap, userBoardCodeRefMap,
			boardCodeRefMap, siteCodeRefMap, hashtagCodeRefMap, evalCodeRefMap, commentCodeRefMap };

	// view->controller 변수
	public static final String userTabCode = "user_tab_code";
	public static final String userBoardCode = "user_board_code";
	public static final String boardCode = "board_code";
	public static final String siteCode = "site_code";
	public static final String hashtagCode = "hashtag_code";
	public static final String evalCode = "eval_code";
	public static final String commentCode = "comment_code";
	public static final String userTabCodeList = "user_tab_code_list";
	public static final String userBoardCodeList = "user_board_code_list";
	public static final String boardCodeList = "board_code_list";
	public static final String siteCodeList = "site_code_list";
	public static final String hashtagCodeList = "hashtag_code_list";
	public static final String evalCodeList = "eval_code_list";
	public static final String commentCodeList = "comment_code_list";
	public static String[] viewToCtrlTargetStr = { userTabCode, userBoardCode, boardCode, siteCode, hashtagCode,
			evalCode, commentCode, userTabCodeList, userBoardCodeList, boardCodeList, siteCodeList, hashtagCodeList,
			evalCodeList, commentCodeList };

	// controller->view 변수
	public static final String user_tab_code = "user_tab_code";
	public static final String user_board_code = "user_board_code";
	public static final String board_code = "board_code";
	public static final String site_code = "site_code";
	public static final String hashtag_code = "hashtag_code";
	public static final String eval_code = "eval_code";
	public static final String comment_code = "comment_code";
	public static final String user_tab_code_list = "user_tab_code_list";
	public static final String user_board_code_list = "user_board_code_list";
	public static final String board_code_list = "board_code_list";
	public static final String site_code_list = "site_code_list";
	public static final String hashtag_code_list = "hashtag_code_list";
	public static final String eval_code_list = "eval_code_list";
	public static final String comment_code_list = "comment_code_list";
	public static String[] ctrlToViewTargetStr = { user_tab_code, user_board_code, board_code, site_code, hashtag_code,
			eval_code, comment_code, user_tab_code_list, user_board_code_list, board_code_list, site_code_list,
			hashtag_code_list, eval_code_list, comment_code_list };

	// 변수의 이름으로 해당하는 refCodeMap의 이름을 가져옴
	public static String getRefCodeMapName(String refCodeName) {
		int length = refCodeNames.length;

		for (int i = 0; i < length; ++i) {

			if (refCodeName.equals(viewToCtrlTargetStr[i]) || refCodeName.equals(ctrlToViewTargetStr[i])
					|| refCodeName.equals(refCodeNames[i]))
				return refCodeNames[i];
		}
		return null;
	}

}

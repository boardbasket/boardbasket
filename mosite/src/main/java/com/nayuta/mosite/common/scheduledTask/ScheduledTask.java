package com.nayuta.mosite.common.scheduledTask;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.admin.model.dao.AdminDao;
import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.common.util.KoreanTokenizerUtil;
import com.nayuta.mosite.common.util.WeekUtil;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.ContentParser.TagManager;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.ArticleParserController;
import com.nayuta.mosite.content.model.service.ContentParser.articleParser.parsers.ArticleParser;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.content.model.service.ContentParser.rssParser.RssParser;
import com.nayuta.mosite.content.model.vo.BoardData;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.TimeSet;
import com.nayuta.mosite.content.model.vo.WordFrequencyData;

@Component
public class ScheduledTask {

	private Logger log = LogManager.getLogger(ScheduledTask.class);

	@Autowired
	private AdminDao aDao;

	@Autowired
	private TagManager tMgr;

	@Autowired
	private BoardDaoImpl bDao;

	@Autowired
	private RssParser rssParser;

	@Autowired
	private BoardParserController bpc;

	@Autowired
	private ArticleParserController apc;

	@Autowired
	private WeekUtil weekUtil;

	@Autowired
	private KoreanTokenizerUtil ktu;

	private static final int THREAD_1 = 1; // main thread 1번

	private static final int THREAD_2 = 2; // main thread 2번

	private static final int THREAD_3 = 3; // main thread 에 속하지 않은 것들

	private final String GOOGLE_TREND_URL;

	private final int ARTICLE_NO_PARSE_ONE_CYCLE_PERIOD;

	private final int ARTICLE_NO_PARSE_PER_DAY;

	private final int HASHTAG_RANK_LIMIT;

	private final int RADAR_BASE_PERIOD;// radar chart 데이터를 저장할때 계산하는 기간

	private final String RADAR_BASE_TIMEMODE;

	public ScheduledTask(@Value("#{serverConst['server_const.google_trend_url']}") String googleTrendUrl,
			@Value("#{new Integer(serverConst['server_const.article_no_parse_one_cycle_period'])}") int aRTICLE_NO_PARSE_ONE_CYCLE_PERIOD,
			@Value("#{new Integer(serverConst['server_const.article_no_parse_per_day'])}") int aRTICLE_NO_PARSE_PER_DAY,
			@Value("#{new Integer(serverConst['server_const.hashtag_rank_limit'])}") int hASHTAG_RANK_LIMIT,
			@Value("#{new Integer(serverConst['server_const.radar_base_period'])}") int rADAR_BASE_PERIOD,
			@Value("#{serverConst['server_const.radar_base_time_mode']}") String rADAR_BASE_TIMEMODE) {
		super();
		this.GOOGLE_TREND_URL = googleTrendUrl;
		this.ARTICLE_NO_PARSE_ONE_CYCLE_PERIOD = aRTICLE_NO_PARSE_ONE_CYCLE_PERIOD;
		this.ARTICLE_NO_PARSE_PER_DAY = aRTICLE_NO_PARSE_PER_DAY;
		this.HASHTAG_RANK_LIMIT = hASHTAG_RANK_LIMIT;
		this.RADAR_BASE_PERIOD = rADAR_BASE_PERIOD;
		this.RADAR_BASE_TIMEMODE = rADAR_BASE_TIMEMODE;
	}

	// 인기 게시판의 태그/커뮤니티 글을 파싱하는 함수
	public void parsePopularBoardTag(Integer threadNum) {
		log.info("----------schedule parsePopularBoardTag thread : " + threadNum + " 실행중----------");

		List<Site> siteList = bDao.selectSiteWithThreadNum(threadNum);
		List<WordFrequencyData> wfDataList = new ArrayList<>();
		siteList.forEach(site -> {
			List<ContentVO> boardList = bDao.selectPopularBoardParseTarget(site);

			List<WordFrequencyData> targetWfDataList = tMgr.parseWordFrequency(boardList);

			targetWfDataList.forEach(wfd -> {
				tMgr.updateWordFrequencyData(wfd);
			});

			wfDataList.addAll(targetWfDataList);

		});

		// tMgr.updateWordFrequencyData(wfDataList);

		long hashtagCount = wfDataList.stream().flatMap(k -> k.getHashtagList().stream()).count();
		long parsedBoardNum = wfDataList.size();
		long articleListNum = wfDataList.stream().flatMap(k -> k.getArticleList().stream()).count();
		log.info("----------schedule parsePopularBoardTag thread : " + threadNum + " 종료----------");
		log.info("-추가된 태그 갯수	: " + hashtagCount + " 개");
		log.info("-파싱된 게시판 갯수: " + parsedBoardNum + "개");
		log.debug("-게시판 목록");
		wfDataList.stream().forEach(k -> log.debug(k.getBoard().getBoard_name()));
		log.info("-추가된 게시글 갯수: " + articleListNum + " 개");
		log.info("--------------------------------------------------------------------------------");

	}

	public void parsePopularBoardTag_thread_1() {
		parsePopularBoardTag(THREAD_1);
	}

	public void parsePopularBoardTag_thread_2() {
		parsePopularBoardTag(THREAD_2);
	}

	public void parsePopularBoardTag_thread_3() {
		parsePopularBoardTag(THREAD_3);
	}

	// 등급에 따라서 게시판을 파싱하기
	public void parseCommonBoardTag(int threadNum) {

		log.info("----------schedule parseCommonBoardTag thread :" + threadNum + " 실행중----------");

		// LocalDate now = LocalDate.now();
		// LocalDate mondayOfWeek = now.minusDays(now.getDayOfWeek().getValue() - 1);
		List<Site> targetSite = bDao.selectSiteWithThreadNum(threadNum);
		List<ContentVO> commonBoardList = bDao.selectCommonBoardParseTarget(ARTICLE_NO_PARSE_PER_DAY, targetSite);

		List<WordFrequencyData> wfDataList = tMgr.parseWordFrequency(commonBoardList);

		tMgr.updateWordFrequencyData(wfDataList);

		long hashtagCount = wfDataList.stream().flatMap(k -> k.getHashtagList().stream()).count();
		long parsedBoardNum = wfDataList.size();
		long articleListNum = wfDataList.stream().flatMap(k -> k.getArticleList().stream()).count();

		log.info("----------schedule parseCommonBoardTag thread :" + threadNum + "종료----------");
		log.info("-타겟 게시판 갯수 : " + commonBoardList.size() + " 개");
		log.info("-파싱된 게시판 갯수: " + parsedBoardNum + "개");
		log.debug("-게시판 목록");
		wfDataList.stream().map(k -> k.getBoard().getBoard_name()).forEach(k -> log.debug(k));
		log.info("-추가된 태그 갯수	: " + hashtagCount + " 개");
		log.info("-추가된 게시글 갯수: " + articleListNum + " 개");
		log.info("-----------------------------------------------------------------------------");

	}

	public void parseCommonBoardTag_thread_1() {
		parseCommonBoardTag(THREAD_1);
	}

	public void parseCommonBoardTag_thread_2() {
		parseCommonBoardTag(THREAD_2);
	}

	public void parseCommonBoardTag_thread_3() {
		parseCommonBoardTag(THREAD_3);
	}

	// 정해진 날짜 간격으로 게시판의 최신 글 번호를 파싱함
	public void parseArticleNum(Integer threadNum) {

		log.info("----------schedule parseLatestArticleNo thread : " + threadNum + " 시작----------");

		// LocalDate now = LocalDate.now();
		// LocalDate mondayOfWeek = now.minusDays(now.getDayOfWeek().getValue() - 1);
		List<Site> targetSite = bDao.selectSiteWithThreadNum(threadNum);

		List<ContentVO> targetList = bDao.selectArticleNumParseTarget(ARTICLE_NO_PARSE_ONE_CYCLE_PERIOD, ARTICLE_NO_PARSE_PER_DAY,
				null, targetSite);
		AtomicInteger counter = new AtomicInteger(0);

		List<BoardData> parsedData = targetList.stream().map(k -> {
			ArticleParser ap = apc.getParser(k.getSite_address());

			BoardData bd = ap.parseLatestArticleNo(k);

			if (bd != null) {
				counter.getAndIncrement();
				return bd;
			} else {
				log.info(k.getBoard_name() + " 파싱 안됨");
				return null;
			}

		}).filter(bd -> bd != null).collect(Collectors.toList());
		bDao.updateBoardData(parsedData);
		bDao.updateArticleNumHistory(parsedData);
		bDao.updateBoardAvailable(targetList);

		/*
		 * targetList.forEach(k -> {
		 * 
		 * ArticleParser ap = apc.getParser(k.getSite_address());
		 * 
		 * BoardData bd = ap.parseLatestArticleNo(k); if (bd != null) {
		 * bDao.updateArticleNumHistory(bd); } else log.info(k.getBoard_name() +
		 * " 파싱 안됨"); bDao.updateBoardAvailable(k); counter.getAndIncrement();
		 * 
		 * });
		 */
		log.info("----------schedule parseLatestArticleNo thread : " + threadNum + " 종료----------");
		log.info("게시글 번호 최신화 대상 게시판 :" + targetList.size() + " 개");
		log.info("게시글 번호 최신화 된 게시판 갯수 : " + counter.get() + "개");
		log.debug("파싱 목록");
		targetList.stream().map(k -> k.getBoard_name()).forEach(k -> log.debug(k));
		log.info("비활성화 된 게시판: ");
		targetList.stream().filter(k -> k.getAvailable().equals("N")).forEach(k -> log.info(k.getBoard_name()));
		log.info("--------------------------------------------------------------------------------");

	}

	public void parseArticleNum_thread_1() {
		parseArticleNum(THREAD_1);
	}

	public void parseArticleNum_thread_2() {
		parseArticleNum(THREAD_2);
	}

	public void parseArticleNum_thread_3() {
		parseArticleNum(THREAD_3);
	}

	// 트렌드 단어들을 파싱하는 함수
	public void parseTrendWord() {
		log.info("----------schedule trendWord 파싱중----------");
		List<String> trendWordList = new ArrayList<>();

		try {
			Document doc = rssParser.getDocument(GOOGLE_TREND_URL);

			Elements elems = rssParser.getElement(doc, "title");

			trendWordList = elems.stream().flatMap(k -> Arrays.asList(k.text().split(" ")).stream()).skip(3)
					.collect(Collectors.toList());

			List<String> filteredWordList = ktu.filteringTrendWordNotInDictionary(trendWordList);

			if (filteredWordList.size() > 0) {
				bDao.insertWordToDictionary(filteredWordList);
				ktu.updateWordToDictionary(filteredWordList);
			}
			log.info("1.추가된 사전 단어 : " + filteredWordList.size() + " 개");
		} catch (Exception e) {
			log.error("단어 사전 추가중 오류 발생");
		}

		log.info("----------schedule trendWord 종료----------");

	}

	// 새 게시판이 추가되는 사이트의 경우 주기적으로 새 게시판을 파싱함
	public void parseNewBoard() {
		log.info("----------schedule newBoard 파싱중----------");
		int count = bpc.parseNewBoard();
		log.info("1.추가된 새로운 게시판 : " + count + " 개");
		log.info("----------schedule newBoard 종료----------");

	}

	// rss 뉴스를 파싱하는 함수
	public void parseRss() {
		log.info("----------schedule parseRss 실행중----------");
		List<ContentVO> rssBoardList = bDao.selectRssBoard(null);
		List<WordFrequencyData> wfdList = rssParser.parseWordFrequencyData(rssBoardList);

		wfdList.forEach(k -> {

			bDao.insertRssContent(k.getArticleList());
			bDao.insertRssHashtag(k.getHashtagList());
		});

		long hashtagCount = wfdList.stream().flatMap(k -> k.getHashtagList().stream()).count();
		long articleListNum = wfdList.stream().flatMap(k -> k.getArticleList().stream()).count();
		log.info("----------schedule parseRss 종료----------");
		log.info("1.추가된 태그 갯수	: " + hashtagCount + " 개");
		log.info("2.추가된 게시글 갯수: " + articleListNum + " 개");
		log.info("-----------------------------------------------------------------------------");

	}

	public void calcBoardParseGrade() {

		log.info("----------schedule calcBoardParseGrade 실행중----------");
		log.info("-----1.계산 전 등급 현황 ");
		// (x,y)->y :키가 겹치면 뒤에것을 쓰겠다는것
		Map<Object, Object> status = bDao.selectParseGradeStatus().stream().flatMap(k -> k.entrySet().stream())
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (x, y) -> y, LinkedHashMap::new));

		status.entrySet().stream().forEach(k -> log.info(k.getKey() + " 등급 : " + k.getValue() + " 개"));

		List<Site> siteList = bDao.selectAllSite();
		// 각 사이트별로 계산
		siteList.forEach(s -> bDao.updateParseFrequencyGrade(s));

		bDao.insertBoardArticleGradeHistory();// 해당 사이클의 parse_frequency_grade,쌓인 글 수를 저장함

		log.info("----2.계산 후 등급 현황 ");
		status = bDao.selectParseGradeStatus().stream().flatMap(k -> k.entrySet().stream())
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (x, y) -> y, LinkedHashMap::new));

		status.entrySet().stream().forEach(k -> log.info(k.getKey() + " 등급 : " + k.getValue() + " 개"));
		log.info("-----------------------------------------------------");

	}

	// 하루에 한번 해시태그의 랭크들을 저장하는 스케줄
	public void insertHashtagRank() {
		Map<String, Object> params = new HashMap<>();

		params.put("timeSet", weekUtil.parseDateBaseToTimeMode(LocalDate.now(), WeekUtil.DAY));
		params.put("timeMode", WeekUtil.DAY);
		params.put("limit", HASHTAG_RANK_LIMIT);

		int count = bDao.insertHashtagRank(params);
		log.info("----------schedule insertHashtagRank 실행중----------");
		if (count > 0) {
			log.info("----------schedule insertHashtagRank 종료----------");
		} else
			log.error("----------schedule insertHashtagRank 실행중 오류 발생----------");
	}

	// 하루에 한번 해시태그의 radar 데이터를 저장하는 스케줄
	public void insertTagRadarData() {
		// 4주 분량의 태그를 가지고 저장함

		bDao.deleteAllTagRadarData();

		LocalDate now = LocalDate.now();

		int dayofweekLength = DayOfWeek.values().length;

		List<TimeSet> weekNumList = new ArrayList<>();
		List<TimeSet> targetTimeSet = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		for (int i = 1; i <= RADAR_BASE_PERIOD; ++i) {

			LocalDate targetDate = now.minusDays(i * dayofweekLength);

			weekNumList.add(weekUtil.getWeekNum(targetDate));
		}

		targetTimeSet = weekUtil.parseDateBaseToTimeMode(weekNumList, RADAR_BASE_TIMEMODE);
		params.put("timeSet", targetTimeSet);

		log.info("----------schedule insertTagRadarData 실행중----------");
		bDao.insertHashtagPopularityVal(params);
		bDao.insertHashtagPersistencyVal(params);
		bDao.insertHashtagUniversalityVal(params);

	}

	public void insertUniqueRssHashtag() {

		log.info("----------schedule insertUniqueRssHashtag 실행중----------");

		bDao.deleteAllUniqueRssHahstag();
		bDao.insertUniqueRssHashtag();

	}

	// 게시판들의 상태(카테고리,접속 가능한지)를 업데이트
	/* @Scheduled(cron = "0 30 4 * * ?") */
	public void updateBoardStatus() {

		// 모든 게시판 rownum을 7(일주일)로 나눠서 나머지값(1~6)이 같은 그룹의 게시판들이 최신화 되도록함

		log.debug("-----------------------------------maintenance board 시작-------------------------");

		Calendar cal = Calendar.getInstance();

		int day = cal.get(Calendar.DAY_OF_WEEK);
		day -= 1;/* 1~6 */

		List<Board> boardList = aDao.selectMaintenanceTargetBoard(day);

		for (Board b : boardList) {
			ContentVO parsedBoard = tMgr.getBoardParserController().maintenanceBoard(b.getBoard_address());
			if (parsedBoard == null)
				b.setAvailable("N");
			else {
				b.setBoard_name(parsedBoard.getBoard_name());
				b.setBoard_category_list(parsedBoard.getBoard_category_list());
			}

		}
		aDao.insertOrMergeParsedBoard(boardList);
		aDao.insertParsedBoardCategory(boardList);

		log.debug("-----------------------------------maintenance board 끝-------------------------");
	}

	// 현재 사용 불가능한 게시판들을 주기적으로 재확인 하는 함수 10800000 밀리초 = 3시간
	/* @Scheduled(fixedDelay = 10800000) */
	public void restoreBoardStatus() {
		List<Board> boardList = aDao.selectCurrentlyUnavailableBoard();

		log.debug("-----------------------------------unavailable restore 시작-------------------------");

		for (Board b : boardList) {
			ContentVO parsedBoard = tMgr.getBoardParserController().maintenanceBoard(b.getBoard_address());
			if (parsedBoard == null)
				b.setAvailable("N");
			else {
				b.setBoard_name(parsedBoard.getBoard_name());
				b.setAvailable("Y");
			}

		}
		aDao.insertOrMergeParsedBoard(boardList);

		log.debug("-----------------------------------unavailable restore 끝-------------------------");
	}

}

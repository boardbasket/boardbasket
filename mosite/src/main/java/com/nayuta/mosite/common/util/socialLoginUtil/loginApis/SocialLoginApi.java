package com.nayuta.mosite.common.util.socialLoginUtil.loginApis;

import com.github.scribejava.core.builder.api.DefaultApi20;

public abstract class SocialLoginApi extends DefaultApi20 {

	protected final String ACCESS_TOKEN_END_POINT;

	protected final String AUTHORIZATION_BASE_URL;

	public SocialLoginApi(String aCCESS_TOKEN_END_POINT, String aUTHORIZATION_BASE_URL) {
		super();
		ACCESS_TOKEN_END_POINT = aCCESS_TOKEN_END_POINT;
		AUTHORIZATION_BASE_URL = aUTHORIZATION_BASE_URL;
	}

	@Override
	public String getAccessTokenEndpoint() {
		return ACCESS_TOKEN_END_POINT;
	}

	@Override
	protected String getAuthorizationBaseUrl() {
		return AUTHORIZATION_BASE_URL;
	}

	/*
	 * private static class InstanceHolder { private static final SocialLoginApi
	 * INSTANCE = new SocialLoginApi(); }
	 * 
	 * public static SocialLoginApi instance() { return InstanceHolder.INSTANCE; }
	 */

}

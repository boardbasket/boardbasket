package com.nayuta.mosite.common.exceptions;

//view -> controller 혹은 controller->view 간의 refCode 변환이 실패했을때 던지는 오류

public class RefCodeException extends InternalServerException {

	private static final long serialVersionUID = 8725886946591323037L;

	public RefCodeException() {
		super();

	}

	public RefCodeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public RefCodeException(String message, Throwable cause) {
		super(message, cause);

	}

	public RefCodeException(String message) {
		super(message);

	}

	public RefCodeException(Throwable cause) {
		super(cause);

	}

}

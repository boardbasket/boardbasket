package com.nayuta.mosite.common.AOP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.DaoProcessException;

/*
 * BoardDaoImpl 함수중 Update,Delete 리턴값이 0미만일때(정상처리 되지 않았을때) exception을 throw 하는 클래스
 *
 * 혹은 모든 BoardDaoImpl 함수가 예외가 발생한 경우 daoprocessException을 발생시킨다.
 * */

@Component
@Aspect
public class DaoUpdateProcessAOP {

	private Logger log = LogManager.getLogger();

	@Pointcut("execution(* com.nayuta.mosite.content.model.dao.BoardDaoImpl.update*(..))")
	public void pcUpdateFunctions() {
	}

	@Pointcut("execution(* com.nayuta.mosite.content.model.dao.BoardDaoImpl.delete*(..))")
	public void pcDeleteFunctions() {
	}

	@AfterReturning(pointcut = "pcUpdateFunctions()", returning = "retVal")
	public Object ifUpdateFailed(JoinPoint jp, Object retVal) {

		if (retVal instanceof Integer) {

			int returnValue = ((Integer) retVal).intValue();

			if (returnValue < 0) {
				throw new DaoProcessException(jp.getSignature().getName() + " 에서 update 실패 returnValue : " + returnValue);
			} else {
				log.debug(jp.getSignature().getName() + " is successfully processed");
			}
		}
		return jp.getArgs();

	}

	@AfterReturning(pointcut = "pcDeleteFunctions()", returning = "retVal")
	public Object ifDeleteFailed(JoinPoint jp, Object retVal) {

		if (retVal instanceof Integer) {

			int returnValue = ((Integer) retVal).intValue();

			if (returnValue < 0) {
				log.error(jp.getSignature().getName() + " dao failed");
				throw new DaoProcessException(jp.getSignature().getName() + " 에서 delete 실패 returnValue : " + returnValue);
			} else {
				log.debug(jp.getSignature().getName() + " is successfully processed");

			}

		}
		return jp.getArgs();
	}

	@Around("execution(* com.nayuta.mosite.content.model.dao.BoardDaoImpl.*(..))||execution(* com.nayuta.mosite.member.model.dao.MemberDaoImpl.*(..))")
	public Object invokeBoardDaoError(ProceedingJoinPoint pjp) {

		try {
			String methodName = pjp.getSignature().toShortString();
			long curr = System.currentTimeMillis();
			log.debug("<!-----------" + methodName + "시작------------");
			Object value = pjp.proceed();
			long last = System.currentTimeMillis();
			log.debug("<!----------" + methodName + " 끝  (" + ((last - curr)) + ") 밀리초 걸림 -----------");
			return value;
		} catch (Throwable e) {
			log.debug(pjp.getSignature().getName() + " dao failed because " + e.getMessage());
			log.error(pjp.getSignature().getName() + " dao failed because " + e.getMessage());
			throw new DaoProcessException(pjp.getSignature().getName() + " dao falid because " + e.getMessage());
		}

	}

}

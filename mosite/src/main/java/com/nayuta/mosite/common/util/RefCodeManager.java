package com.nayuta.mosite.common.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/*controller ->view view->controller 간의 primarykey와 대응되는 임시 코드를 저장하는 클래스*/

@Component
public class RefCodeManager implements RefCodeConst {

	private Map<String, Map<String, String>> refCodeMaps = new HashMap<>();

	public RefCodeManager() {
	}

	public RefCodeManager(Map<String, Map<String, String>> refCodeMaps) {
		super();
		this.refCodeMaps = refCodeMaps;
	}

	// 모든 refCodeMap을 Map<String,Map<String,String>>형태로 가지고옴
	public Map<String, Map<String, String>> getAllRefCodeMap() {
		return refCodeMaps;
	}

	// refCodeMap의 이름으로 session에서 refCodeMap을 가져옴
	public Map<String, String> getRefCodeMap(String refCodeNameOrMapName) {

		if (refCodeNameOrMapName == null || refCodeNameOrMapName.isEmpty())
			return null;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(refCodeNameOrMapName) || viewToCtrlTargetStr[i].equals(refCodeNameOrMapName)
					|| ctrlToViewTargetStr[i].equals(refCodeNameOrMapName)) {

				return refCodeMaps.get(refCodeNames[i]); // (Map<String, String>) session.getAttribute(refCodeNames[i]);
			}
		}

		return null;
	}

	// RefCodeConst 클래스의 상수 배열 번호로 그에 해당하는 refCodeMap을 가져옴
	public Map<String, String> getRefCodeMap(int targetCodeEnum) {

		if (targetCodeEnum > (RefCodeConst.refCodeNames.length - 1) || targetCodeEnum < 0)
			return null;

		return refCodeMaps.get(refCodeNames[targetCodeEnum]);

	}

	// refCodeMap의 이름으로 session에 refCodeMap을 addAttribute 함
	public boolean setRefCodeMap(String refCodeMapName, Map<String, String> refCodeMap) {

		if (refCodeMapName == null || refCodeMapName.isEmpty() || refCodeMap == null)
			return false;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(refCodeMapName)) {
				refCodeMaps.put(refCodeMapName, refCodeMap);
				return true;
			}

		}
		return false;
	}

	// session 에서 refCodeMap이름을 가진 Attribute를 remove한다.
	public boolean removeRefMap(String refCodeMapName) {

		if (refCodeMapName == null || refCodeMapName.isEmpty())
			return false;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(refCodeMapName)) {
				refCodeMaps.remove(refCodeMapName);
				return true;
			}

		}

		return false;
	}

	// 모든 refCodeMap을 session에서 remove한다.
	public void removeAllRefMap() {

		refCodeMaps = new HashMap<>();
	}

	// session에 refCodeMapName을 가진 attribute가 있는지 확인한다.
	public boolean isRefMapNull(String refCodeMapName) {

		if (refCodeMapName == null || refCodeMapName.isEmpty())
			return false;

		if (!refCodeMaps.containsKey(refCodeMapName)/* session.getAttribute(refCodeMapName) == null */)
			return true;
		else
			return false;

	}

	// refCode를 가지고있는 refCodeMap을 session에서 찾는다.
	public Map<String, String> findCodeMapWithCode(String refCode) {

		for (int i = 0; i < refCodeNames.length; ++i) {

			Map<String, String> tempRefMap = getRefCodeMap(refCodeNames[i]);

			if (tempRefMap != null) {

				if (tempRefMap.containsKey(refCode)) {
					return getRefCodeMap(refCodeNames[i]);
				}
			}
		}
		return null;
	}

	public Map<String, String> findCodeMapWithValue(String value) {

		for (int i = 0; i < refCodeNames.length; ++i) {

			Map<String, String> tempRefMap = getRefCodeMap(refCodeNames[i]);

			if (tempRefMap != null) {

				if (tempRefMap.containsValue(value)) {
					return getRefCodeMap(refCodeNames[i]);
				}
			}
		}
		return null;
	}

	// refCode를 해당 이름을 가진 refCodeMap에 넣고 session에 addAttribute한다.
	public boolean insertRefCodeToRefCodeMap(String refCodeMapName, String refCode, String originalVal) {

		if (refCodeMapName == null || refCodeMapName.isEmpty())
			return false;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(refCodeMapName)) {

				Map<String, String> temp = getRefCodeMap(refCodeMapName);
				temp.put(refCode, originalVal);
				setRefCodeMap(refCodeMapName, temp);

				return true;
			}

		}
		return false;

	}

	// destRefMapName을 가진 refMap에 sourceRefMap을 merge하고 session에 addAttribute한다.
	public boolean mergeCodeRefMap(String destRefMapName, Map<String, String> sourceRefMap) {

		if (destRefMapName == null || destRefMapName.isEmpty())
			return false;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(destRefMapName)) {

				if (sourceRefMap != null) {
					Map<String, String> temp = getRefCodeMap(destRefMapName);

					for (String key : sourceRefMap.keySet()) {

						if (!temp.containsValue(sourceRefMap.get(key)))
							temp.put(key, sourceRefMap.get(key));
					}

					setRefCodeMap(destRefMapName, temp);
					return true;
				} else
					return false;
			}

		}
		return false;

	}

	public boolean mergeCodeRefMap(String destRefMapName, String refCode, String originalCode) {

		if (destRefMapName == null || destRefMapName.isEmpty())
			return false;

		for (int i = 0; i < refCodeNames.length; ++i) {

			if (refCodeNames[i].equals(destRefMapName)) {

				Map<String, String> temp = getRefCodeMap(destRefMapName);
				temp.put(refCode, originalCode);
				setRefCodeMap(destRefMapName, temp);
				return true;

			}

		}
		return false;

	}
}

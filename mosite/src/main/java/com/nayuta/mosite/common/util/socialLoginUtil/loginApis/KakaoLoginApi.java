package com.nayuta.mosite.common.util.socialLoginUtil.loginApis;

import com.github.scribejava.core.oauth2.clientauthentication.ClientAuthentication;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;

public class KakaoLoginApi extends SocialLoginApi {

	public KakaoLoginApi(String aCCESS_TOKEN_END_POINT, String aUTHORIZATION_BASE_URL) {
		super(aCCESS_TOKEN_END_POINT, aUTHORIZATION_BASE_URL);

	}

	@Override
	public ClientAuthentication getClientAuthentication() {
		return RequestBodyAuthenticationScheme.instance();
	}

}

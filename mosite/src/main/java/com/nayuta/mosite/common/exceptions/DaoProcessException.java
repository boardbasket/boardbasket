package com.nayuta.mosite.common.exceptions;

public class DaoProcessException extends InternalServerException {

	private static final long serialVersionUID = -1442738235708552434L;

	public DaoProcessException(String message) {
		super(message);

	}

	public DaoProcessException() {
		super();
	}

	public DaoProcessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DaoProcessException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoProcessException(Throwable cause) {
		super(cause);
	}
	
}

package com.nayuta.mosite.common.util;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.UUID;

public class ShorthenUUID {

	private static ShorthenUUID instance;

	private ShorthenUUID() {
	}

	public static ShorthenUUID getInstance() {
		if (instance == null) {
			instance = new ShorthenUUID();
		}
		return instance;
	}

	public String getShorthenUUID(UUID uuid) {
		Encoder encoder = Base64.getUrlEncoder();

		// Create byte[] for base64 from uuid
		byte[] src = ByteBuffer.wrap(new byte[16]).putLong(uuid.getMostSignificantBits())
				.putLong(uuid.getLeastSignificantBits()).array();

		// Encode to Base64 and remove trailing ==
		return encoder.encodeToString(src).substring(0, 22);
	}

	public String getShorthenUUID() {

		UUID uuid = UUID.randomUUID();
		Encoder encoder = Base64.getUrlEncoder();
		byte[] src = ByteBuffer.wrap(new byte[16]).putLong(uuid.getMostSignificantBits())
				.putLong(uuid.getLeastSignificantBits()).array();

		// Encode to Base64 and remove trailing ==
		return encoder.encodeToString(src).substring(0, 22);

	}

	// public Map<String, String> getCodeReference(List<?> objList) {
	// Map<String, String> codeReference = new HashMap<String, String>();
	//
	// for (Object obj : objList) {
	//
	// String code = null;
	//
	// if (obj instanceof UserTab)
	// code = ((UserTab) obj).getUser_tab_code();
	//
	// codeReference.put(ShorthenUUID.getInstance().getShorthenUUID(UUID.randomUUID()).toString(),
	// code);
	//
	// }
	//
	// return codeReference;
	// }
	//
	// public Map<String, String> getCodeReference(Object obj) {
	// Map<String, String> codeReference = new HashMap<String, String>();
	// String code =null;
	// if(obj instanceof UserTab)
	// code = ((UserTab)obj).getUser_tab_code();
	//
	// codeReference.put(ShorthenUUID.getInstance().getShorthenUUID(UUID.randomUUID()).toString(),
	// code);
	// return codeReference;
	// }

}

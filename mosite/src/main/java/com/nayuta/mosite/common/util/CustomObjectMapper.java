package com.nayuta.mosite.common.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class CustomObjectMapper extends ObjectMapper {

	private static final long serialVersionUID = -6510144277297190063L;

	public CustomObjectMapper() {
		super();
		configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		setSerializationInclusion(Include.NON_EMPTY);

	}

}

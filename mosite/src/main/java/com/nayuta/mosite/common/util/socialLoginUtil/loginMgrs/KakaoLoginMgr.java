package com.nayuta.mosite.common.util.socialLoginUtil.loginMgrs;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.nayuta.mosite.common.util.socialLoginUtil.loginApis.KakaoLoginApi;

@Component("kakaoLoginMgr")
public class KakaoLoginMgr extends SocialLoginMgr {

	public KakaoLoginMgr(String sOCIAL_LOGIN_PROVIDER, String cLIEND_ID, String cLIENT_SECRET, String rEDIRECT_URL,
			String pROFILE_API_URL, String vERIFY_ACCESS_TOKEN_API_URL, KakaoLoginApi api) {
		super(sOCIAL_LOGIN_PROVIDER, cLIEND_ID, cLIENT_SECRET, rEDIRECT_URL, pROFILE_API_URL,
				vERIFY_ACCESS_TOKEN_API_URL);
		this.api = api;
	}

	@Override
	public void initService() {
		service = new ServiceBuilder(CLIEND_ID).callback(REDIRECT_URL).build(api);
	}

	@Override
	public OAuth2AccessToken getAccessToken(String code) throws IOException, InterruptedException, ExecutionException {

		return service.getAccessToken(code);

	}

	@Override
	public OAuth2AccessToken refreshAccessToken(String refreshToken)
			throws IOException, InterruptedException, ExecutionException {

		return service.refreshAccessToken(refreshToken);

	}

	@Override
	public boolean verifyAccessToken(String accessToken) throws IOException, InterruptedException, ExecutionException {

		JSONObject obj = getDataFromApi(VERIFY_ACCESS_TOKEN_API_URL, accessToken);
		if (obj == null)
			return false;
		else {
			JSONObject idObj = (JSONObject) obj.get("response");
			if (idObj.get("id") != null)
				return true;
		}

		return false;
	}

	@Override
	public String getUserProfile(String accessToken) throws IOException, InterruptedException, ExecutionException {

		String response = getDataFromApiScribe(PROFILE_API_URL, accessToken);

		JSONObject jObj = parseJson(response);

		Object idObj = jObj.get("id");

		return (String) String.valueOf(idObj);

	}

}

package com.nayuta.mosite.common.util;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.scribejava.core.exceptions.OAuthException;
import com.nayuta.mosite.common.exceptions.BadRequestException;
import com.nayuta.mosite.common.exceptions.BindingException;
import com.nayuta.mosite.common.exceptions.DaoProcessException;
import com.nayuta.mosite.common.exceptions.FileException;
import com.nayuta.mosite.common.exceptions.InternalServerException;
import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.common.exceptions.RefCodeException;
import com.nayuta.mosite.common.exceptions.SiteParseException;

/**
 * @author typ
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends RuntimeException {

	private static final long serialVersionUID = 6421244414618251899L;
	protected Logger log = LogManager.getLogger();

	@Autowired
	JCacheManagerUtil cMgr;

	@ExceptionHandler({ InternalServerException.class, SQLException.class, DaoProcessException.class, SiteParseException.class,
			RefCodeException.class })
	public Object internalServiceError(Model model, HttpServletRequest request, HttpServletResponse response,
			InternalServerException e) throws Exception {
		log.error("SQLException " + e.getMessage());
		request.getSession().setAttribute("prevPage", null);
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		response.setCharacterEncoding("UTF-8");
		if (request.getHeader("AJAX") == null) {
			response.setContentType("text/html;charset=utf-8");
			model.addAttribute("errorMsg", "데이터 처리 중에 문제가 생겼습니다.");
			return new ModelAndView("errors/error500", "model", model);
		} else {
			response.setContentType("application/json");
			response.getWriter().print("{\"msg\":\"오류가 발생했습니다\",\"redirect\":true}");
			return null;
		}

	}

	@ExceptionHandler({ BadRequestException.class, BindingException.class, FileException.class, MVCServiceException.class })
	@ResponseBody
	public Object badRequestException(BadRequestException ex, HttpServletResponse response, Model model,
			HttpServletRequest request) throws Exception {

		log.error(ex.getMessage());
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		response.setCharacterEncoding("UTF-8");
		request.getSession().setAttribute("prevPage", null);
		if (request.getHeader("AJAX") == null) {

			response.setContentType("text/html;charset=utf-8");
			return new ModelAndView("errors/error400", "model", model);
		} else {

			Map<String, Object> result = new HashMap<String, Object>();
			result.put("msg", ex.getMessage());
			result.put("toastType", "danger");

			// response.setContentType("application/json");

			// response.getWriter().print("{\"msg\":\"" + ex.getMessage() +
			// "\",\"toastType\":\"danger\"}\"");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

	}

	@ExceptionHandler(BindException.class)
	@ResponseBody
	public Object bindExeption(BindException ex, HttpServletResponse response, Model model, HttpServletRequest request)
			throws Exception {

		log.error(ex.getMessage());
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		response.setCharacterEncoding("UTF-8");
		request.getSession().setAttribute("prevPage", null);
		if (request.getHeader("AJAX") == null) {

			response.setContentType("text/html;charset=utf-8");
			return new ModelAndView("errors/error400", "model", model);
		} else {

			Map<String, Object> result = new HashMap<String, Object>();
			result.put("msg", ex.getMessage());
			result.put("toastType", "danger");

			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}

	}

	@ExceptionHandler(OAuthException.class)
	@ResponseBody
	public Object oauthErrorHandler(OAuthException ex, HttpServletResponse response, Model model, HttpServletRequest request) {

		log.error(ex.getMessage());
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		request.getSession().setAttribute("prevPage", null);
		if (request.getHeader("AJAX") == null) {

			response.setContentType("text/html;charset=utf-8");
			return new ModelAndView("errors/error500", "model", model);
		} else {
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("msg", ex.getMessage());
			result.put("redirect", true);
			return result;
		}

	}

}

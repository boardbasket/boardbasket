package com.nayuta.mosite.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class SessionAttributeUtil {

	public HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	}

	public HttpServletResponse getResponse() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();
	}

	public HttpSession getSession() {
		return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getSession();
	}
	
	public boolean containsKey(String attrName) {
		return getSession().getAttribute(attrName) != null;
	}

	public Object getAttribute(String attrName) {

		return getSession().getAttribute(attrName);
	}

	public void setAttribute(String attrName, Object obj) {
		getSession().setAttribute(attrName, obj);
	}

	public void removeAttribute(String attrName) {
		getSession().removeAttribute(attrName);
	}

	

}

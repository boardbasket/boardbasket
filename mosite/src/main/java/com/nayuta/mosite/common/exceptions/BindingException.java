package com.nayuta.mosite.common.exceptions;

public class BindingException extends BadRequestException {

	private static final long serialVersionUID = 1510383037306142194L;

	public BindingException() {
		super();
	}

	public BindingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BindingException(String message, Throwable cause) {
		super(message, cause);
	}

	public BindingException(String message) {
		super(message);
	}

	public BindingException(Throwable cause) {
		super(cause);
	}

}

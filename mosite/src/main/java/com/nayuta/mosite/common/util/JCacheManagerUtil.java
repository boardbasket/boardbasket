package com.nayuta.mosite.common.util;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.jcache.JCacheCacheManager;

public class JCacheManagerUtil {

	@Autowired
	private JCacheCacheManager cacheMgr;

	public void clearAllCaches() {

		for (String str : cacheMgr.getCacheManager().getCacheNames()) {
			cacheMgr.getCache(str).clear();
			// log.debug(str + " 캐시를 지움");
		}

	}

	// 캐시가 존재하는지 확인
	public boolean isCacheExist(String cacheName) {

		Cache<Object, Object> cache = cacheMgr.getCacheManager().getCache(cacheName);
		if (cache != null)
			return true;
		else
			return false;
	}

	// 캐시에 해당 값이 존재하는지 확인
	public boolean getCacheEntryValueExist(String cacheName, Object key) {

		if (isCacheExist(cacheName)) {
			Cache<Object, Object> cache = getCache(cacheName);

			Object target = cache.get(key);

			if (target != null)
				return true;
			else
				return false;
		} else
			return false;

	}

	// 해당캐시를 삭제
	public boolean destroyCache(String cacheName) {
		cacheMgr.getCacheManager().destroyCache(cacheName);

		return true;
	}

	// 모든 캐시를 삭제
	public void destroyAllCache() {
		CacheManager cacheManager = cacheMgr.getCacheManager();

		for (String str : cacheManager.getCacheNames())
			cacheManager.destroyCache(str);
	}

	// 캐시를 만들거나 있는경우 가져옴
	public Cache<Object, Object> createCache(String cacheName) {

		Cache<Object, Object> returning = null;

		returning = (Cache<Object, Object>) cacheMgr.getCacheManager().getCache(cacheName);
		if (returning != null) {

			return returning;
		} else {
			MutableConfiguration<Object, Object> configuration = new MutableConfiguration<Object, Object>()
					.setTypes(Object.class, Object.class).setStoreByValue(false)
					.setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(Duration.FIVE_MINUTES));
			return cacheMgr.getCacheManager().createCache(cacheName, configuration);
		}
	}

	// 해당 캐시의 키를 이용해 값을 가져옴
	public Object getCacheEntryValue(String cacheName, Object key) {

		Cache<Object, Object> cache = getCache(cacheName);

		if (cache != null && key != null) {
			// log.debug(cacheName + "에서 엔트리를 키 " + key + "로 찾음");
			return cache.get(key);
		} else
			return null;
	}

	// 해당 캐시에 키를 이용해 값을 집어넣음
	public boolean insertEntryToCache(String cacheName, Object key, Object obj) {

		try {
			getCache(cacheName).put(key, obj);
			// log.debug("Cache " + cacheName + " 에 " + key + " 로" + obj + "가 들어감");
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	// 캐시의 값을 가져옴
	public Object getValueFromCache(String cacheName, Object key) {

		try {
			return getCache(cacheName).get(key);
		} catch (Exception e) {
			return null;
		}

	}

	public Cache<Object, Object> getCache(String cacheName) {
		return cacheMgr.getCacheManager().getCache(cacheName);
	}

	public boolean evictCacheEntry(String cacheName, Object key) {

		Cache<Object, Object> targetCache = getCache(cacheName);
		boolean result = false;
		if (targetCache == null)
			return result;
		else {

			if (targetCache.remove(key)) {
				// log.debug("Cache " + "\'" + cacheName + "\'" + " 에서 " + key + " 로 엔트리를 지움");
				result = true;
			} else {
				// log.debug("Cache " + "\'" + cacheName + "\'" + " 에서 " + key + " 를 가진 캐시가
				// 없음");
			}
			return result;

		}

	}

	public boolean evictCacheAllEntries(String cacheName) {
		Cache<Object, Object> targetCache = getCache(cacheName);

		if (targetCache == null)
			return false;
		else {
			targetCache.clear();
			// log.debug("Cache " + cacheName + " 의 모든 엔트리를 지움");
			return true;
		}

	}

	public JCacheCacheManager getCacheMgr() {
		return cacheMgr;
	}

	public void setCacheMgr(JCacheCacheManager cacheMgr) {
		this.cacheMgr = cacheMgr;
	}

}

package com.nayuta.mosite.common.exceptions;

public class SiteParseException extends InternalServerException {

	private static final long serialVersionUID = -1189505400319688183L;

	public SiteParseException() {
		super();
	}

	public SiteParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SiteParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public SiteParseException(String message) {
		super(message);
	}

	public SiteParseException(Throwable cause) {
		super(cause);
	}

}

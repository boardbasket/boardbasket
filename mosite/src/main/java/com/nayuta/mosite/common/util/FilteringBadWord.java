package com.nayuta.mosite.common.util;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.MVCServiceException;

@Component
public class FilteringBadWord {

	@Resource(name = "wordList")
	protected Properties bwList;

	@Value("#{patterns['patterns.removeNumber']}")
	private String removeNumber;
	private Pattern removeNumberPattern;
	// #일치하면 차단하는 리스트
	private List<String> bwType1;
	// #포함하면 차단하는 리스트
	private List<String> bwType2;
	// #단독사용이면 차단하는 리스트
	private List<String> bwType3;

	// #최소한의 제약(범죄 ,마약 관련)
	// private List<String> bwType4;

	// 단어빈도 가져올때 의미없는 단어 지우기(기본 욕설 포함)
	private List<String> bwType5;

	@PostConstruct
	public void init() {
		String type1 = (String) bwList.get("bwType1");
		String type2 = (String) bwList.get("bwType2");
		String type3 = (String) bwList.get("bwType3");
		// String type4 = (String) bwList.get("bwType4");
		String type5 = (String) bwList.get("bwType5");
		bwType1 = Arrays.asList(type1.split(","));
		bwType2 = Arrays.asList(type2.split(","));
		bwType3 = Arrays.asList(type3.split(","));
		// bwType4 = Arrays.asList(type4.split(","));
		bwType5 = Arrays.asList(type5.split(","));
		removeNumberPattern = Pattern.compile(removeNumber);
	}

	public boolean filteringBadWordFunc(String word) {

		Matcher matcher = removeNumberPattern.matcher(word);

		String target = matcher.replaceAll("");

		if (target.length() >= 0) {
			for (String badWord : bwType2) {
				if (target.indexOf(badWord) > 0)
					throw new MVCServiceException("사용할 수 없는 단어 입니다.");
			}
			if (bwType1.contains(target))
				throw new MVCServiceException("사용할 수 없는 단어 입니다.");
			else if (bwType3.contains(target))
				throw new MVCServiceException("사용할 수 없는 단어 입니다.");

			/*
			 * if (bwType4.contains(target)) throw new
			 * MVCServiceException("사용할 수 없는 단어 입니다.");
			 */
		}

		return true;
	}

	public boolean filteringHasMeaninglessWord(String word) {
		return bwType5.contains(word);
	}

}

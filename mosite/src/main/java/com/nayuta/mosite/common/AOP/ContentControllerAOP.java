package com.nayuta.mosite.common.AOP;

import java.util.HashMap;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.nayuta.mosite.common.exceptions.BindingException;
import com.nayuta.mosite.common.util.SessionAttributeUtil;
import com.nayuta.mosite.content.controller.ContentController;

//binding result의 결과에 에러가 있을때 exception throw해주는 클래스
//controller 함수 시작전에 responsebody return에 쓰일 hashMap을 clear해줌
@Component
@Aspect
public class ContentControllerAOP {

	@Autowired
	private SessionAttributeUtil sau;

	@Before("execution(* com.nayuta.mosite.*.controller.*Controller.*(..))")
	public void controllerAopFunction(JoinPoint jp) {
		// 모델맵 가져오기
		Object[] args = jp.getArgs();
		BindingResult br = null;
		Device device = null;
		Object target = jp.getTarget();

		if (target instanceof ContentController) {
			ContentController cc = (ContentController) target;
			cc.setResult(new HashMap<String, Object>());
		}

		for (Object obj : args) {
			if (obj instanceof Device)
				device = (Device) obj;
			else if (obj instanceof BindingResult)
				br = (BindingResult) obj;
		}
		if (device != null) {// 타블렛 아니고 핸드폰일때만
			sau.setAttribute("isMobile", device.isMobile());
			sau.setAttribute("isTablet", device.isTablet());
		}
		// 바인딩 관련해서 에러가 있으면 에러 throw 해줌
		if (br != null && br.hasErrors())
			throw new BindingException(br.getAllErrors().get(0).toString());

	}

}

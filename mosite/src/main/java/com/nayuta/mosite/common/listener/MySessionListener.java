package com.nayuta.mosite.common.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MySessionListener implements HttpSessionListener {

	//refcode salt 값 생성을 위한 시간값을 넣어주는 리스너
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		arg0.getSession().setAttribute("time", System.currentTimeMillis());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		arg0.getSession().setAttribute("time", System.currentTimeMillis());

	}

}

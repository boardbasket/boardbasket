package com.nayuta.mosite.common.exceptions;

public class FileException extends BadRequestException {

	private static final long serialVersionUID = -1442738235708552434L;

	public FileException() {
		super();

	}

	public FileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public FileException(String message, Throwable cause) {
		super(message, cause);

	}

	public FileException(String message) {
		super(message);

	}

	public FileException(Throwable cause) {
		super(cause);

	}

}

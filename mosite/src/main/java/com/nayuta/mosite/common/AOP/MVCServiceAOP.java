package com.nayuta.mosite.common.AOP;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.service.pageCommand.AbstractPageCls;

//abstractPageCls를 상속한 클래스의 execute 함수가 시작하기 전에 몇몇 변수를 초기화 해줌(이전 결과가 남아있으므로)
@Component
@Aspect
public class MVCServiceAOP {

	@Before("execution(* com.nayuta.mosite.content.model.service.pageCommand.Pc*.execute(..))&&target(com.nayuta.mosite.content.model.service.pageCommand.AbstractPageCls)")
	public void clearResultObj(JoinPoint jp) {
		Object target = jp.getTarget();

		if (target instanceof AbstractPageCls) {
			//AbstractPageCls apc = (AbstractPageCls) target;
			//apc.postProcess();
		}

	}

}

package com.nayuta.mosite.common.util.socialLoginUtil.loginMgrs;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.nayuta.mosite.common.util.socialLoginUtil.loginApis.GoogleLoginApi;

@Component("googleLoginMgr")
public class GoogleLoginMgr extends SocialLoginMgr {

	public GoogleLoginMgr(String pROVIDER, String cLIEND_ID, String cLIENT_SECRET, String rEDIRECT_URL,
			String pROFILE_API_URL, String vERIFY_ACCESS_TOKEN_API_URL, GoogleLoginApi api) {
		super(pROVIDER, cLIEND_ID, cLIENT_SECRET, rEDIRECT_URL, pROFILE_API_URL, vERIFY_ACCESS_TOKEN_API_URL);
		this.api = api;
	}

	@Override
	public void initService() {
		service = new ServiceBuilder(CLIEND_ID).apiSecret(CLIENT_SECRET).defaultScope("profile").callback(REDIRECT_URL)
				.build(api);

	}

	@Override
	public OAuth2AccessToken refreshAccessToken(String refreshToken)
			throws IOException, InterruptedException, ExecutionException {

		return service.refreshAccessToken(accessToken.getRefreshToken());
	}

	@Override
	public boolean verifyAccessToken(String accessToken) throws IOException, InterruptedException, ExecutionException {

		String responseBody = getDataFromApiScribe(VERIFY_ACCESS_TOKEN_API_URL, accessToken);

		JSONObject jObj = parseJson(responseBody);

		if (jObj.containsKey("user_id"))
			return true;
		return false;

	}

	@Override
	public String getUserProfile(String accessToken) throws IOException, InterruptedException, ExecutionException {

		String responseBody = getDataFromApiScribe(VERIFY_ACCESS_TOKEN_API_URL, accessToken);

		JSONObject jObj = parseJson(responseBody);

		if (jObj.containsKey("user_id"))
			return (String) jObj.get("user_id");
		return null;
	}

}

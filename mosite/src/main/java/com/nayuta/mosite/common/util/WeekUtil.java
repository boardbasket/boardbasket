package com.nayuta.mosite.common.util;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.vo.TimeSet;

/*
 * 월 주차를 계산하는 유틸 클래스
 * 
 * ex) 2021 9월 1주차 ->  08-30 , 09-05 리턴
 * 
 * 
 * */
@Component
public class WeekUtil {

	private final String DEFAULT_TIME_MODE;

	public static final String YEAR = "year";
	public static final String MONTH = "month";
	public static final String WEEK = "week";
	public static final String DAY = "day";
	private final int MAX_YEAR_OPTION_NUM;
	private int MAX_MONTH_OPTION_NUM;
	private int MAX_WEEK_OPTION_NUM;
	private int MAX_DAY_OPTION_NUM;

	public WeekUtil(@Value("#{const['const.default_time_mode']}") String default_time_mode,
			@Value("#{new Integer(const['const.maxYearOptionNum'])}") int mAX_YEAR_OPTION_NUM,
			@Value("#{new Integer(const['const.maxMonthOptionNum'])}") int mAX_MONTH_OPTION_NUM,
			@Value("#{new Integer(const['const.maxWeekOptionNum'])}") int mAX_WEEK_OPTION_NUM,
			@Value("#{new Integer(const['const.maxDayOptionNum'])}") int mAX_DAY_OPTION_NUM) {
		super();
		this.DEFAULT_TIME_MODE = default_time_mode;
		this.MAX_YEAR_OPTION_NUM = mAX_YEAR_OPTION_NUM;
		this.MAX_MONTH_OPTION_NUM = mAX_MONTH_OPTION_NUM;
		this.MAX_WEEK_OPTION_NUM = mAX_WEEK_OPTION_NUM;
		this.MAX_DAY_OPTION_NUM = mAX_DAY_OPTION_NUM;
	}

	// targetMonth로 부터 beforeMonthNum 떨어진 월 들의 timeSet 가져오기(targetMonth 포함)
	public List<TimeSet> getTimeSetList(int year, int targetMonth, int beforeMonthNum, boolean includeMonth) {

		List<TimeSet> result = new ArrayList<TimeSet>();

		LocalDate target = LocalDate.of(year, targetMonth, 1); // 해당 달의 1일

		for (int i = 0; i < beforeMonthNum; ++i) {

			LocalDate temp = target.minusMonths(i);

			int maxWeekNum = getMaxWeekNumOfMonth(temp.getYear(), temp.getMonthValue());

			for (int j = includeMonth ? 0 : 1; j <= maxWeekNum; ++j)
				result.add(weekProcess(temp.getYear(), temp.getMonthValue(), j));

		}

		return result;
	}

	public List<TimeSet> getTimeSetList(List<LocalDate> timeOptionList) {
		List<TimeSet> result = new ArrayList<TimeSet>();

		// 연도
		/*
		 * timeOptionList.stream().map(k -> k.getYear()).distinct().forEach(k -> {
		 * TimeSet ts = new TimeSet(); ts.setYear(k); result.addAll(yearProcess(ts));
		 * });
		 */

		timeOptionList.stream().sorted((k1, k2) -> k1.isAfter(k2) ? 1 : -1).forEach(k -> {
			int maxWeekNum = getMaxWeekNumOfMonth(k.getYear(), k.getMonthValue());

			for (int j = 1; j <= maxWeekNum; ++j)
				/*
				 * if (j == 0) result.addAll(monthProcess(k.getYear(), k.getMonthValue())); else
				 */
				result.add(weekProcess(k.getYear(), k.getMonthValue(), j));
		});

		return result;
	}

	// 1년 기준으로 몇번째 주인지(1~52)
	public int getYearWeekNum(LocalDate today) {

		LocalDate firstWeekDayOfYear = getFirstWeekStartDateOfMonth(today.getYear(), 1);

		// 해당 년도의 날짜가 해당 년도의 첫주 첫일보다 적다면 이전년도 주차로 계산
		// ex)2022년은 1주차는 2022-01-03일부터 시작 하므로 2022-01-01은 2021 년의 52번째 주
		if (today.isBefore(firstWeekDayOfYear))
			firstWeekDayOfYear = getFirstWeekStartDateOfMonth(today.getYear() - 1, 1);

		for (int i = 1; i <= 52; ++i) {

			LocalDate compare = firstWeekDayOfYear.plusWeeks(i).minusDays(1);
			if (today.isEqual(compare) || today.isBefore(compare)) {
				return i;
			}

		}

		return 0;
	}

	public List<TimeSet> getTimeOption(List<LocalDate> dateList) {

		// 1.sorting-> 년,월만 가지고 timeset 만듬
		LocalDate now = LocalDate.now();
		List<TimeSet> result = dateList.stream().sorted((k1, k2) -> k1.isAfter(k2) ? 1 : -1).map(k -> {
			LocalDate originDate = k;
			TimeSet timeSet = getWeekNum(k);
			List<TimeSet> innerResult = new ArrayList<>();

			if (originDate.equals(now) || originDate.isAfter(now)) {
				// 두가지 경우가 있음
				if (originDate.getMonthValue() > timeSet.getMonth()) {
					// ex) 2022 4월 27일 이지만 2022 3월 5주차임=>주차 변환된 월이 원래 월보다 작은경우 이번달을 포함하지 않음
					return innerResult;
				} else if (originDate.getMonthValue() < timeSet.getMonth()) {
					// ex) 2022 5월 30일 이지만 2022 6월 1주차임 =>주차 변환된 월이 원래 월보다 큰경우 다음달을 추가해줌
					innerResult.add(
							new TimeSet(k.getYear(), k.getMonthValue(), getMaxWeekNumOfMonth(k.getYear(), k.getMonthValue())));
					innerResult.add(new TimeSet(k.getYear(), k.getMonthValue() + 1, 1));

				} else {
					innerResult.add(
							new TimeSet(k.getYear(), k.getMonthValue(), getMaxWeekNumOfMonth(k.getYear(), k.getMonthValue())));

				}
			} else
				innerResult
						.add(new TimeSet(k.getYear(), k.getMonthValue(), getMaxWeekNumOfMonth(k.getYear(), k.getMonthValue())));

			return innerResult;
		}).flatMap(k -> k.stream()).distinct().collect(Collectors.toList());

		return result;

	}

	// baseMonth로부터 beforeNum떨어진 month 까지 week의 갯수를 map형태로 리턴
	public List<TimeSet> getMonthWeekNumList(LocalDate target, int beforeNum) {

		TimeSet targetTimeSet = getWeekNum(target);

		List<TimeSet> result = new ArrayList<>();

		LocalDate copy = LocalDate.of(target.getYear(), target.getMonth(), target.getDayOfMonth());

		// 현재 월이 주차계산으로 구한 월보다 작은경우 1달을 더해줌(ex 2021-11-29일은 11월이지만 12월 첫째주에 속함)
		if (targetTimeSet.getYear() == target.getYear()) {
			if (targetTimeSet.getMonth() > target.getMonthValue())
				copy = copy.plusMonths(1);
			// 현재 월이 주차계산으로 구한 월보다 큰경우 1달을 빼줌(ex 2021-10-01은 10월이지만 9월 5주차에 속함)
			else if (targetTimeSet.getMonth() < target.getMonthValue())
				copy = copy.minusMonths(1);
		} else
			copy = copy.minusMonths(1);

		for (int i = beforeNum; i >= 0; --i) {

			LocalDate temp = copy.minusMonths(i);

			TimeSet timeSet = new TimeSet(temp.getYear(), temp.getMonthValue(),
					getMaxWeekNumOfMonth(temp.getYear(), temp.getMonthValue()));

			// int targetMonth = baseMonth - i;
			// result.put(temp.getMonthValue(), getMaxWeekNumOfMonth(temp.getYear(),
			// temp.getMonthValue()));
			result.add(timeSet);
		}

		return result;
	}

	// 해당 달의 첫주 첫날을 LocalDate 형태로 가져옴
	// ex) 2021,9 -> 2021-08-30
	public LocalDate getFirstWeekStartDateOfMonth(Integer year, int month) {

		year = getYear(year);

		if (month > Month.DECEMBER.getValue())
			throw new MVCServiceException("시간 값이 올바르지 않습니다.");

		LocalDate ld = LocalDate.of(year, month, 1);
		DayOfWeek dow = ld.getDayOfWeek();

		if (dow.getValue() <= DayOfWeek.THURSDAY.getValue()) {
			return ld.minusDays(dow.getValue()).plusDays(1);
		} else
			return ld.plusDays(DayOfWeek.SUNDAY.getValue() - dow.getValue() + 1);

	}

	// 해당 달의 마지막주 마지막 날을 LocalDate 형태로 가져옴
	// ex) 2021,9 -> 2021-10-03
	public LocalDate getLastWeekEndDateOfMonth(Integer year, int month) {
		int idx = getMaxWeekNumOfMonth(year, month);

		LocalDate firstDay = getFirstWeekStartDateOfMonth(year, month);

		return firstDay.plusWeeks(idx).minusDays(1);

	}

	// 해당 날짜가 몇월의 몇주차인지 확인
	public TimeSet getWeekNum(Integer year, int month, int day) {
		year = getYear(year);
		TimeSet timeSet = new TimeSet();
		try {
			LocalDate target = LocalDate.of(year, month, day);
			LocalDate lastMonth = target.minusMonths(1);// 비교를 위해 이전 월을 가져오기

			// 이전월의 마지막주 마지막날을 가져오기
			LocalDate lastMonthLastWeekDay = getLastWeekEndDateOfMonth(lastMonth.getYear(), lastMonth.getMonthValue());

			// 오늘이 이전 달 마지막 주의 날인지 확인(ex 2021-10-01은 10월이지만 9월 5주차에 속함)
			if (target.isEqual(lastMonthLastWeekDay) || target.isBefore(lastMonthLastWeekDay)) {
				// 주차에 알맞게 변수를 수정
				month = lastMonth.getMonthValue();
				year = lastMonth.getYear();
			} else {
				// 그렇지 않다면 오늘이 다음달 첫째주 인지 확인(ex 2021-11-29일은 11월이지만 12월 첫째주에 속함)
				LocalDate nextMonth = target.plusMonths(1);
				LocalDate nextMonthFirstWeekDay = getFirstWeekStartDateOfMonth(nextMonth.getYear(), nextMonth.getMonthValue());

				if (target.isEqual(nextMonthFirstWeekDay) || target.isAfter(nextMonthFirstWeekDay)) {
					month = nextMonth.getMonthValue();
					year = nextMonth.getYear();
				}
			}

			LocalDate firstWeekDay = getFirstWeekStartDateOfMonth(year, month);

			int idx = getMaxWeekNumOfMonth(year, month);
			for (int i = 1; i <= idx; ++i) {

				LocalDate compare = firstWeekDay.plusWeeks(i).minusDays(1);
				if (target.isEqual(compare) || target.isBefore(compare)) {
					TimeSet result = new TimeSet(year, month, i, target);
					return result;
				}

			}

			return timeSet;

		} catch (Exception e) {
			return timeSet;
		}

	}

	// 해당 날짜가 몇월의 몇주차인지 확인
	public TimeSet getWeekNum(LocalDate targetDate) {

		return getWeekNum(targetDate.getYear(), targetDate.getMonthValue(), targetDate.getDayOfMonth());

	}

	// 해당 달의 마지막 주차의 수를 리턴
	// ex) 2021,9 -> 5, 2021 ,8 ->4
	public int getMaxWeekNumOfMonth(Integer year, int month) {

		year = getYear(year);

		LocalDate firstDayOfMonth = getFirstWeekStartDateOfMonth(year, month);
		LocalDate fourWeeksLater = firstDayOfMonth.plusWeeks(4);

		LocalDate ld = LocalDate.of(year, month, 1);
		int lastDay = ld.lengthOfMonth();
		LocalDate ldLast = LocalDate.of(year, month, lastDay);

		if (ldLast.isBefore(fourWeeksLater))
			return 4;
		else {
			DayOfWeek dow = ldLast.getDayOfWeek();

			return dow.getValue() >= DayOfWeek.THURSDAY.getValue() ? 5 : 4;
		}

	}

	// 특정 달 주차의 시작일과 끝일을 현재 시간과 비교해서 각각 얼마나 떨어져 있는지 일단위로 리턴
	/*
	 * public List<TimeSet> getWeekOfMonthDiffFromNow(LocalDate date, String
	 * timeMode) {
	 * 
	 * if (date == null) date = LocalDate.now();
	 * 
	 * return getWeekOfMonthDiffFromNow(date, timeMode); }
	 */

	// searchData에서 들어온 timeSet을 timeMode에 맞춰서 파싱
	public List<TimeSet> parseDateBaseToTimeMode(List<TimeSet> weekNumList, String timeMode) {

		String targetTimeMode = timeMode != null ? timeMode : DEFAULT_TIME_MODE;

		List<TimeSet> result = new ArrayList<>();

		if (weekNumList == null || weekNumList.size() <= 0) {
			weekNumList = getInitialTimeSet(LocalDate.now(), targetTimeMode);
			// result.addAll(getInitialTimeSet(LocalDate.now(), targetTimeMode));
		}

		weekNumList.forEach(k -> {
			result.addAll(parseDateBaseToTimodeInner(k, targetTimeMode));
		});

		return result;
	}

	// parseDateBaseToTimeMode의 내부 함수
	private List<TimeSet> parseDateBaseToTimodeInner(TimeSet time, String timeMode) {

		List<TimeSet> result = new ArrayList<>();
		if (timeMode.equals("year")) {
			result.addAll(yearProcess(time));
		} else if (timeMode.equals("month"))
			result.addAll(monthProcess(time.getYear(), time.getMonth()));
		else if (timeMode.equals("week"))
			result.add(weekProcess(time.getYear(), time.getMonth(), time.getWeek()));
		else if (timeMode.equals("day")) {
			result.add(dayProcess(time));
		}
		return result;
	}

	private List<TimeSet> yearProcess(TimeSet timeSet) {

		TimeSet result = new TimeSet();

		result.setYear(timeSet.getYear());
		result.setMonth(0);

		List<TimeSet> timeSetList = new ArrayList<>();

		for (int i = Month.JANUARY.getValue(); i <= Month.DECEMBER.getValue(); ++i) {
			TimeSet startDate = weekProcess(timeSet.getYear(), i, 1);
			TimeSet endDate = weekProcess(timeSet.getYear(), i, getMaxWeekNumOfMonth(timeSet.getYear(), i));
			TimeSet ts = new TimeSet();
			ts.setYear(timeSet.getYear());
			ts.setMonth(i);
			ts.setToTime(endDate.getToTime());
			ts.setToDate(endDate.getToDate());
			ts.setFromTime(startDate.getFromTime());
			ts.setFromDate(startDate.getFromDate());

			timeSetList.add(ts);
		}

		return timeSetList;

	}

	public List<TimeSet> parseDateBaseToTimeMode(LocalDate ld) {

		return parseDateBaseToTimeMode(ld, DEFAULT_TIME_MODE);
	}

	public List<TimeSet> parseDateBaseToTimeMode(LocalDate ld, String timeMode) {
		List<TimeSet> result = new ArrayList<>();

		result.addAll(parseDateBaseToTimodeInner(getWeekNum(ld), timeMode));
		return result;
	}

	private TimeSet dayProcess(TimeSet timeSet) {
		timeSet.setFromDate(timeSet.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		timeSet.setToDate(timeSet.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

		return timeSet;
	}

	// 해당 월의 모든 week 시작일과 끝일을 timeset으로 리턴
	public List<TimeSet> monthProcess(Integer year, int month) {

		List<TimeSet> result = new ArrayList<>();

		int maxWeekNum = getMaxWeekNumOfMonth(year, month);

		for (int i = 1; i <= maxWeekNum; ++i) {
			result.add(weekProcess(year, month, i));
		}

		return result;
	}

	// 해당 월의 특정 주차 시작일과 끝일을 TimeSet으로 리턴
	public TimeSet weekProcess(Integer year, int month, Integer weekNum) {
		year = getYear(year);// year 가져오기 (null이면 현재 년도)
		TimeSet timeSet = new TimeSet();

		LocalDate initDate = getFirstWeekStartDateOfMonth(year, month);// 현재 월의 첫주 첫날 가져오기

		LocalDate fromDate;
		LocalDate toDate;

		// 주차에 해당하는 값이 없으면 해당 월의 시작일과 마지막날을 넘겨줌
		/*
		 * if (weekNum == null || weekNum == 0) { fromDate =
		 * getFirstWeekStartDateOfMonth(year, month); toDate =
		 * getLastWeekEndDateOfMonth(year, month); } else {
		 */
		if (weekNum < 1)
			weekNum = 1;
		else if (weekNum > getMaxWeekNumOfMonth(year, month))
			weekNum = getMaxWeekNumOfMonth(year, month);
		toDate = initDate.plusWeeks(weekNum).minusDays(1);
		fromDate = toDate.minusWeeks(1).plusDays(1);
		/* } */

		Duration diffToTime = Duration.between(toDate.atStartOfDay(), LocalDate.now().atStartOfDay());
		Duration diffFromTime = Duration.between(fromDate.atStartOfDay(), LocalDate.now().atStartOfDay());

		timeSet.setYear(year);
		timeSet.setFromDate(fromDate);
		timeSet.setToDate(toDate);
		timeSet.setFromTime((int) diffFromTime.toDays());
		timeSet.setToTime((int) diffToTime.toDays());
		timeSet.setWeek(weekNum != null ? weekNum.intValue() : 0);// 주차가 없으면 0주차로 처리(월단위)
		timeSet.setMonth(month);

		return timeSet;

	}

	private int getYear(Integer year) {
		return year == null ? Year.now().getValue() : year;
	}

	// map형태인 timeSet을 list에 집어넣어서 리턴해줌(sql query parameter 형식)
	public List<TimeSet> timeSetToList(TimeSet timeSet) {

		List<TimeSet> result = new ArrayList<>();
		if (timeSet != null)
			result.add(timeSet);
		return result;

	}

	public List<TimeSet> getInitialTimeSet(LocalDate now) {
		return getInitialTimeSet(now, DEFAULT_TIME_MODE);
	}

	public List<TimeSet> getInitialTimeSet(LocalDate now, String timeMode) {

		List<TimeSet> result = new ArrayList<>();
		// LocalDate now = LocalDate.now();
		TimeSet nowTimeSet = getWeekNum(now);

		switch (timeMode) {
		case "year":
			result.add(new TimeSet(nowTimeSet.getYear(), 0, 0));
			break;
		case "month":
			TimeSet target = nowTimeSet;
			for (int i = 1; i < MAX_MONTH_OPTION_NUM; ++i) {

				TimeSet prevTimeSet = null;
				if (target.getMonth() == Month.JANUARY.getValue()) {
					prevTimeSet = new TimeSet(target.getYear() - 1, Month.DECEMBER.getValue(), 0);
				} else {
					prevTimeSet = new TimeSet(target.getYear(), target.getMonth() - 1, 0);
				}
				if (prevTimeSet != null)
					result.add(0, prevTimeSet);
				// result.add(prevTimeSet);

				target = prevTimeSet;
			}

			/*
			 * if (nowTimeSet.getWeek() - 1 <= 2) {// 이번달의 3번째 주를 초과 하기 전까진 저번달을 포함함 TimeSet
			 * prevTimeSet = null; if (nowTimeSet.getMonth() == Month.JANUARY.getValue()) {
			 * prevTimeSet = new TimeSet(nowTimeSet.getYear() - 1,
			 * Month.DECEMBER.getValue(), 0); } else { prevTimeSet = new
			 * TimeSet(nowTimeSet.getYear(), nowTimeSet.getMonth() - 1, 0); } if
			 * (prevTimeSet != null) result.add(prevTimeSet); }
			 */
			nowTimeSet.setWeek(0);
			result.add(nowTimeSet);
			break;
		case "week":
			// 현재 요일이 일정 값 이하라면(ex 목요일 이전) 이전주를 timeSetList에 추가해줌
			if (now.getDayOfWeek().getValue() < DayOfWeek.THURSDAY.getValue()) {

				// 지금이 첫째주라면 지난달 마지막 주를 같이 가져옴
				if (nowTimeSet.getWeek() - 1 <= 0) {

					LocalDate previousMonth = now.minusWeeks(1).minusDays(now.getDayOfWeek().getValue()).plusDays(1);

					TimeSet prevTimeSet = new TimeSet(previousMonth.getYear(), previousMonth.getMonthValue(),
							getMaxWeekNumOfMonth(previousMonth.getYear(), previousMonth.getMonthValue()));
					result.add(prevTimeSet);

				} else {
					// 아니라면 그냥 현재 주차에서 1을빼서 저번주를 리턴
					TimeSet prevTimeSet = new TimeSet(now.getYear(), now.getMonthValue(), nowTimeSet.getWeek() - 1);
					result.add(prevTimeSet);

				}

			}
			result.add(nowTimeSet);

			break;

		}

		return result;

	}

	public String getDEFAULT_TIME_MODE() {
		return DEFAULT_TIME_MODE;
	}

	public Date convertLocalDatetoDate(LocalDate localDate) {
		return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

}

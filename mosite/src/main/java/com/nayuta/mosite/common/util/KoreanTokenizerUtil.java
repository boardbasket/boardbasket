package com.nayuta.mosite.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.openkoreantext.processor.KoreanTokenJava;
import org.openkoreantext.processor.OpenKoreanTextProcessorJava;
import org.openkoreantext.processor.tokenizer.KoreanTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.ContentVO;
import com.nayuta.mosite.content.model.vo.Hashtag;

import scala.collection.Seq;

@Component
public class KoreanTokenizerUtil {

	@Autowired
	FilteringBadWord bwFilter;

	// 페이지에서 유효한 태그로 판단하는 등장횟수의 비율기준 ex)비율이 0.2이고 한 페이지 50개의 글이 있다면 50*0.2 =>10번
	// 등장하면 유효한 태그로 인정해 저장
	private final double VALID_WORD_FREQUENCY_RATIO;

	private final int MIN_WORD_NUM; // 유효한 태그로 인정하는 최소 갯수

	private final double MAX_FREQUENCY_RATIO;// 일정 갯수 이상이면 도배로 간주하고 처리 안함

	private final int MIN_WORD_LENGTH;// 단어(토큰)의 최소 길이

	private final int MIN_WORD_LENGTH_RSS;// 단어(토큰)의 최소 길이(rss 일때)

	private final Pattern REPEATED_CHAR_PATTERN;// 반복되는 글자를 필터링하기 위한 패턴

	@Autowired
	private BoardDaoImpl bDao;

	public KoreanTokenizerUtil(
			@Value("#{new Double(serverConst['server_const.valid_word_frequency_ratio'])}") double vALID_WORD_FREQUENCY_RATIO,
			@Value("#{new Integer(serverConst['server_const.min_word_length'])}") int mIN_WORD_LENGTH,
			@Value("#{new Integer(serverConst['server_const.min_word_num'])}") int mIN_WORD_NUM,
			@Value("#{new Integer(serverConst['server_const.min_word_num_rss'])}") Integer mIN_WORD_NUM_RSS,
			@Value("#{new Double(serverConst['server_const.max_frequency_ratio'])}") double mAX_FREQUENCY_RATIO,
			@Value("#{serverConst['server_const.repeated_char_pattern']}") String repeatedCharPattern) {

		this.VALID_WORD_FREQUENCY_RATIO = vALID_WORD_FREQUENCY_RATIO;
		this.MIN_WORD_LENGTH = mIN_WORD_LENGTH;
		this.MIN_WORD_NUM = mIN_WORD_NUM;
		this.MAX_FREQUENCY_RATIO = mAX_FREQUENCY_RATIO;
		this.REPEATED_CHAR_PATTERN = Pattern.compile(repeatedCharPattern);
		this.MIN_WORD_LENGTH_RSS = mIN_WORD_NUM_RSS;
	}

	@PostConstruct
	public void init() {
		// db에서 사전 추가 단어들을 가져와서 사전에 넣어줌
		updateWordToDictionary(bDao.selectWordDictionary());
	}

	// 단어 사전에 단어를 집어넣음(1개)
	public void updateWordToDictionary(String word) {
		List<String> temp = new ArrayList<>();
		temp.add(word);
		OpenKoreanTextProcessorJava.addNounsToDictionary(temp);
	}

	// 단어 사전에 단어를 집어넣음(여러개)
	public void updateWordToDictionary(List<String> wordList) {
		OpenKoreanTextProcessorJava.addNounsToDictionary(wordList);
	}

	// 게시판에서 파싱된 글들을 필터링해서 단어 형태로 가져옴
	public List<Hashtag> tokenizeArticle(ContentVO board, List<BoardArticle> codeArticleList, int articleNumPerPage,
			int wordFrequencyPageNum) {
		Map<String, List<BoardArticle>> temp = new HashMap<>();
		temp.put(board.getBoard_code(), codeArticleList);
		return tokenizeArticle(temp, articleNumPerPage, wordFrequencyPageNum);
	}

	// isRss(뉴스 컨텐츠인지 옵션 추가)
	public List<Hashtag> tokenizeArticle(ContentVO board, List<BoardArticle> codeArticleList, int articleNumPerPage,
			int wordFrequencyPageNum, boolean isRss) {

		Map<String, List<BoardArticle>> temp = new HashMap<>();
		temp.put(board.getBoard_code(), codeArticleList);
		return tokenizeArticle(temp, articleNumPerPage, wordFrequencyPageNum, isRss);
	}

	// 게시판 텍스트를 단어로 바꾸어 리턴
	public List<Hashtag> tokenizeArticle(Map<String, List<BoardArticle>> codeArticleList, int articleNumPerPage,
			int wordFrequencyPageNum, boolean isRss) {

		long minThreshold = isRss ? MIN_WORD_LENGTH_RSS : MIN_WORD_NUM;
		/*
		 * Math.max(isRss ? MIN_WORD_LENGTH_RSS : MIN_WORD_NUM,
		 * Math.round(articleNumPerPage * wordFrequencyPageNum *
		 * VALID_WORD_FREQUENCY_RATIO));
		 */

		long maxThreshold = Math.round(articleNumPerPage * wordFrequencyPageNum * MAX_FREQUENCY_RATIO);

		List<Hashtag> result = new ArrayList<>();

		codeArticleList.entrySet().stream().forEach(k -> {

			String boardCode = k.getKey();
			List<BoardArticle> articleList = k.getValue();

			// 각 게시글 별로 중복되지 않는 단어를 tokenize 하고 필터링을 함
			articleList.stream().filter(o -> o.getIsNotice().equals("N")).forEach(article -> {

				List<String> nounList = tokenizeStr(article.getTitle());

				article.setTagList(nounList);

			});

			// tokenize된 단어들을 모아서 갯수를 세고 특정 갯수 이상인 것을 가져옴
			result.addAll(articleList.stream().map(article -> article.getTagList()).flatMap(tagList -> tagList.stream())
					.collect(Collectors.groupingBy(j -> j, HashMap::new, Collectors.counting())).entrySet().stream()
					.filter(j -> j.getValue() >= minThreshold).filter(j -> j.getValue() < maxThreshold).sorted((p1, p2) -> {
						return p2.getValue().compareTo(p1.getValue());
					}).map(j -> new Hashtag(boardCode, j.getKey(), j.getValue())).collect(Collectors.toList()));

		});

		return result;

	}

	// isRss(뉴스 컨텐츠인지 옵션 추가)
	public List<Hashtag> tokenizeArticle(Map<String, List<BoardArticle>> codeArticleList, int articleNumPerPage,
			int wordFrequencyPageNum) {

		return tokenizeArticle(codeArticleList, articleNumPerPage, wordFrequencyPageNum, false);

	}

	// 2글자 이하,중복,욕설 및 의미없는 단어 제거한 명사 리스트 리턴
	public List<String> tokenizeStr(String str) {

		List<KoreanTokenJava> koreanTokens = strToKoreanToken(str);

		return filteringNounList(koreanTokens);

	}

	// string을 nomalize해서 koreanToken으로 리턴해주는 함수
	public List<KoreanTokenJava> strToKoreanToken(String str) {
		str = str.toUpperCase();
		CharSequence normalized = OpenKoreanTextProcessorJava.normalize(str);
		Seq<KoreanTokenizer.KoreanToken> tokens = OpenKoreanTextProcessorJava.tokenize(normalized);
		List<KoreanTokenJava> koreanTokens = OpenKoreanTextProcessorJava.tokensToJavaKoreanTokenList(tokens);
		return koreanTokens;
	}

	// koreanToken을 필터링 해주는 함수
	public List<String> filteringNounList(List<KoreanTokenJava> koreanTokens) {
		return koreanTokens.stream().filter(j -> j.getPos().toString().equals("Noun") || j.getPos().toString().equals("Alpha"))
				.map(j -> {
					if (j.getPos().toString().equals("Alpha")) {
						return j.getText().toUpperCase();// 영어일 경우 대문자로 변경해줌
					} else
						return j.getText();
				}).filter(j -> !bwFilter.filteringHasMeaninglessWord(j)).filter(j -> j.toString().length() >= MIN_WORD_LENGTH)
				.filter(j -> {
					Matcher m = REPEATED_CHAR_PATTERN.matcher(j);
					while (m.find())
						return false;// 반복되는게 있으므로 false를 리턴해 필터링
					return true;
				}).distinct().collect(Collectors.toList());
	}

	public List<String> tokenizeStrList(List<String> strList) {

		List<String> result = new ArrayList<>();

		strList.forEach(k -> result.addAll(tokenizeStr(k)));

		return result;
	}

	public List<String> tokenizeStrArr(String[] strList) {
		List<String> result = new ArrayList<>();
		Arrays.stream(strList).forEach(k -> result.addAll(tokenizeStr(k)));
		return result;
	}

	// google 트렌드 단어중 사전에 없는 것들을 체크해 리턴해줌
	public List<String> filteringTrendWordNotInDictionary(List<String> targetList) {

		// 트렌드에 올라간 단어들은 띄어쓰기로 구분된 고유한 '단어'들이라고 가정하고 시작

		// 1. 문장 부호를 삭제해줌 (콜론 ,쉼표 등)
		return targetList.stream().map(k -> {
			Pattern filteringPunctuation = Pattern.compile("[^\\w가-힣ㄱ-ㅎ\\s]");
			Matcher matcher = filteringPunctuation.matcher(k);
			String temp = matcher.find() ? matcher.replaceAll(" ") : k;
			return temp;

		}).map(k -> Arrays.asList(k.split(" "))).flatMap(k -> k.stream()).filter(k -> {
			// 2.띄어쓰기로 나눈 단어를 tokenized 했을때 해당 단어가 2개이상의 다른 요소의 조합인 경우 새로운 단어로 간주하고 리턴해줌
			// ex) 아케인 -> 아케(명사) + 인(조사)으로 tokenize 됨 => 2개 이상 토큰 조합 => '아케인' 등록
			// ex) 검수완박 -> 검(명사) + 수완(명사) + 박(명사) 됨 => 2개 이상의 토큰 조합 => '검수완박' 등록
			List<KoreanTokenJava> list = strToKoreanToken(k);
			list = list.stream().filter(token -> !token.getPos().toString().equals("Punctuation")).collect(Collectors.toList());
			return list.size() > 1;
		}).collect(Collectors.toList());

	}

}

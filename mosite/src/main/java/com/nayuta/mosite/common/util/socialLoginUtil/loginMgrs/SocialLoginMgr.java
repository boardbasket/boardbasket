package com.nayuta.mosite.common.util.socialLoginUtil.loginMgrs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

abstract public class SocialLoginMgr {

	// private final static String DOMAIN = "http://localhost:8088/";
	protected final String SOCIAL_LOGIN_PROVIDER;
	protected final String CLIEND_ID;
	protected final String CLIENT_SECRET;
	protected final String REDIRECT_URL;
	protected final String PROFILE_API_URL;
	protected final String VERIFY_ACCESS_TOKEN_API_URL;
	protected static final int SECURE_NUM_LENGTH = 30;
	protected OAuth20Service service;
	protected DefaultApi20 api;
	protected OAuth2AccessToken accessToken;

	public SocialLoginMgr(String sOCIAL_LOGIN_PROVIDER, String cLIEND_ID, String cLIENT_SECRET, String rEDIRECT_URL,
			String pROFILE_API_URL, String vERIFY_ACCESS_TOKEN_API_URL) {
		super();
		SOCIAL_LOGIN_PROVIDER = sOCIAL_LOGIN_PROVIDER;
		CLIEND_ID = cLIEND_ID;
		CLIENT_SECRET = cLIENT_SECRET;
		REDIRECT_URL = rEDIRECT_URL;
		PROFILE_API_URL = pROFILE_API_URL;
		VERIFY_ACCESS_TOKEN_API_URL = vERIFY_ACCESS_TOKEN_API_URL;
	}

	@PostConstruct
	public void initService() {
		service = new ServiceBuilder(CLIEND_ID).apiSecret(CLIENT_SECRET).callback(REDIRECT_URL).build(api);
	}

	abstract public String getUserProfile(String code) throws IOException, InterruptedException, ExecutionException;

	abstract public OAuth2AccessToken refreshAccessToken(String refreshToken)
			throws IOException, InterruptedException, ExecutionException;

	abstract public boolean verifyAccessToken(String accessToken)
			throws IOException, InterruptedException, ExecutionException;

	public String getAccessTokenUrl(HttpSession session) {

		String state = getSecureRandomStr();

		session.setAttribute("state", state);

		String requestUrl = service.getAuthorizationUrl(state);

		return requestUrl;

	}

	public OAuth2AccessToken getAccessToken(String code) throws IOException, InterruptedException, ExecutionException {

		return service.getAccessToken(code);

	}

	public String getSecureRandomStr() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(SECURE_NUM_LENGTH, random).toString();
	}

	public OAuth20Service getService() {
		return service;
	}

	public DefaultApi20 getApi() {
		return api;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public void setService(OAuth20Service service) {
		this.service = service;
	}

	public void setApi(DefaultApi20 api) {
		this.api = api;
	}

	public void setAccessToken(OAuth2AccessToken accessToken) {
		this.accessToken = accessToken;
	}

	public String getSOCIAL_LOGIN_PROVIDER() {
		return SOCIAL_LOGIN_PROVIDER;
	}

	public String getCLIEND_ID() {
		return CLIEND_ID;
	}

	public String getCLIENT_SECRET() {
		return CLIENT_SECRET;
	}

	public String getREDIRECT_URL() {
		return REDIRECT_URL;
	}

	public String getPROFILE_API_URL() {
		return PROFILE_API_URL;
	}

	public JSONObject parseJson(String jsonStr) {
		JSONParser jsonParser = new JSONParser();
		try {
			JSONObject obj = (JSONObject) jsonParser.parse(jsonStr);
			return obj;
		} catch (ParseException e) {
			return null;
		}

	}

	public String getDataFromApiScribe(String apiURL, String accessToken)
			throws IOException, InterruptedException, ExecutionException {
		final OAuthRequest request = new OAuthRequest(Verb.GET, VERIFY_ACCESS_TOKEN_API_URL);
		service.signRequest(accessToken, request);

		try (Response response = service.execute(request)) {
			return response.getBody();
		}

	}

	public JSONObject getDataFromApi(String apiURL, String accessToken) {
		String token = accessToken;// 네이버 로그인 접근 토큰;
		String header = "Bearer " + token; // Bearer 다음에 공백 추가
		try {
			String _apiURL = apiURL;
			URL url = new URL(_apiURL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Authorization", header);
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			int responseCode = con.getResponseCode();
			BufferedReader br;
			if (responseCode == 200) {
				// 정상 호출
				br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			} else {
				// 에러 발생
				br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
			}

			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
			}
			br.close();
			JSONObject obj = parseJson(response.toString());
			// JSONObject idObj = (JSONObject) obj.get("response");

			return obj;
		} catch (Exception e) {
			return null;
		}

	}

}

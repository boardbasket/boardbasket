package com.nayuta.mosite.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.nayuta.mosite.content.model.vo.Hashtag;
import com.nayuta.mosite.content.model.vo.TimeSet;


//태그를 timeSet에 맞춰서 그룹핑하는 클래스
public class GroupingTagByTimeSetList {

	private static final String EMPTY_TIME = "0";

	public static Map<String, Map<String, Map<String, List<Hashtag>>>> groupByTimeSetList(List<TimeSet> timeSetList,
			List<Hashtag> hashtagList, String timeMode) {
		Map<String, Map<String, Map<String, List<Hashtag>>>> result = new HashMap<>();

		// 시간대별로 나열된 태그를 연도,월,주차로 나누어 묶음
		timeSetList.forEach(k -> {

			List<Hashtag> filteredList = null;
			String year = k.getYear().toString();

			if (!result.containsKey(year))
				result.put(year, new HashMap<String, Map<String, List<Hashtag>>>());

			switch (timeMode) {
			case "year": {

				hashtagList.stream().filter(tag -> {
					return (tag.getYear().equals(k.getYear()));
				}).collect(Collectors.groupingBy(tag -> tag.getMonth() + "")).entrySet().stream().forEach(entry -> {
					String month = entry.getKey().toString();
					if (!result.get(year).containsKey(month))
						result.get(year).put(month, new HashMap<String, List<Hashtag>>());

					result.get(year).get(month).put(EMPTY_TIME, entry.getValue());
				});
				break;
			}
			case "month": {

				String month = k.getMonth().toString();
				String week = k.getWeek().toString();

				filteredList = hashtagList.stream().filter(tag -> {
					return (tag.getWeek() == k.getWeek()) && (tag.getMonth() == k.getMonth());
				}).collect(Collectors.toList());
				if (!result.get(year).containsKey(month))
					result.get(year).put(month, new HashMap<String, List<Hashtag>>());

				result.get(year).get(month).put(week, filteredList);
				break;
			}

			case "week": {

				String month = k.getMonth().toString();
				String week = k.getWeek().toString();
				filteredList = hashtagList.stream().filter(tag -> {
					return (tag.getWeek() == k.getWeek()) && (tag.getWeek() == k.getWeek()) && (tag.getMonth() == k.getMonth());
				}).collect(Collectors.toList());

				if (!result.get(year).containsKey(month))
					result.get(year).put(month, new HashMap<String, List<Hashtag>>());

				result.get(year).get(month).put(week, filteredList);
				break;
			}

			}

		});

		return result;

	}

}

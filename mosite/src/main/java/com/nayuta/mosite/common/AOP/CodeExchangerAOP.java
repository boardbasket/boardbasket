package com.nayuta.mosite.common.AOP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.RefCodeException;
import com.nayuta.mosite.common.util.RefCodeConst;
import com.nayuta.mosite.common.util.SessionAttributeUtil;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.member.model.vo.Member;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/*
 * 	CodeExchangerAOP Class

	execute 함수 '실행 전' 에 view에서 들어온 임시코드(refCode)들을 진짜 코드(originalCode)로 변경 하고 
	execute 함수 '실행 후' 결과의 진짜 코드들을 임시 코드로 변경 해주는 클래스
*/

@Component
@Aspect
@SuppressWarnings("unchecked")
public class CodeExchangerAOP implements RefCodeConst {
	private Logger log = LogManager.getLogger();

	/*
	 * @Autowired private RefCodeManager rcMgr;
	 */

	@Autowired
	private SessionAttributeUtil sau;

	@Value("#{serverConst['server_const.firstHashidsSalt']}")
	private String firstHashidsSalt;// prefix

	@Value("#{serverConst['server_const.secondHashidsSalt']}")
	private String secondHashidsSalt;// code

	@Value("#{serverConst['server_const.firstHashidsLength']}")
	private String firstHashidsLength;

	@Value("#{serverConst['server_const.secondHashidsLength']}")
	private String secondHashidsLength;

	// private int firstHashidsLengthNum;

	private int secondHashidsLengthNum;

	// private Hashids firstHashids;

	@PostConstruct
	public void init() {

		secondHashidsLengthNum = Integer.parseInt(secondHashidsLength);
		// firstHashidsLengthNum = Integer.parseInt(firstHashidsLength);

		// firstHashids = new Hashids(firstHashidsSalt, firstHashidsLengthNum);

	}

	public String getTimeSaltPrefix() {

		long time = (Long) sau.getSession().getAttribute("time");

		Authentication au = SecurityContextHolder.getContext().getAuthentication();
		// String salt = (String) sau.getAttribute("memberNo") + firstHashidsSalt;
		if (!(au instanceof AnonymousAuthenticationToken)) {
			Member m = (Member) au.getPrincipal();
			return m.getMember_no() + time;

		} else {
			return String.valueOf(time);
		}
	}

	// view -> controller (refCode->originalCode)
	@Before("execution(* com.nayuta.mosite.content.model.service.pageCommand.Pc*.execute(..))")
	public void exchangeRefCodeToOriginalCode(JoinPoint jp) {
		// 모델맵 가져오기
		Object[] args = jp.getArgs();
		ModelMap modelMap = null;

		for (Object obj : args) {
			if (obj instanceof ModelMap) {
				modelMap = (ModelMap) obj;
				break;
			}
		}
		//long start = System.currentTimeMillis();
		// modelMap에 저장되어있는 키를 이용하여 Object들을 가져온다.
		for (String attrName : modelMap.keySet()) {

			Object targetObj = modelMap.get(attrName);
			Object returnVal = null;

			if (checkViewToCtrlTarget(targetObj, attrName))
				returnVal = getOriginalCodeProcess(targetObj, attrName, getTimeSaltPrefix());

			if (returnVal != null)
				modelMap.replace(attrName, returnVal);

		}
		//long end = System.currentTimeMillis();
		//System.out.println("view->ctrl refcode aop 실행 시간 : " + (end - start) / 1000.0);

	}

	// 자동 변경 되는 경우
	// 1. 클래스가 GetPrimaryKeyCodeCls 인터페이스를 상속받는 경우 인터페이스를 구현한 메소드에 명시되어있는 변수들
	// 2. 클래스 변수 리스트가 List<GetPrimaryKeyCodeCls>형태인 경우 인터페이스 명시 상관없이 변환
	// 3. String 변수이름이 특정 값인 경우(RefCodeConst클래스에 정의되어있는 이름인 경우)
	// 4. String 리스트

	// refCode->originalCode 변경이 끝난 Object를 리턴
	public Object getOriginalCodeProcess(Object targetObj, String attrName, String timePrefix) {

		if (targetObj instanceof GetPrimaryKeyCodeCls) {
			// GetPrimaryKeyCodeCls 클래스 하나만 들어왔을때
			GetPrimaryKeyCodeCls tempObj = (GetPrimaryKeyCodeCls) targetObj;

			Map<String, Object> originalCodes = new HashMap<String, Object>();
			for (String targetName : tempObj.pullPrimaryKey().keySet())
				// attrName은 여기서 정해진다...
				originalCodes.put(targetName,
						getOriginalCodeProcess(tempObj.pullPrimaryKey().get(targetName), targetName, timePrefix));
			tempObj.pushPrimaryKey(originalCodes);
			return tempObj;

		} else if (targetObj instanceof String) {
			String refCode = (String) targetObj;

			if (refCode == null || refCode.isEmpty())
				return refCode;

			return getDecodedRefCodeValue(refCode, attrName, timePrefix);

		} else if (targetObj instanceof List<?>) {
			// 리스트가 들어왔을때
			List<?> tempList = (List<?>) targetObj;

			if (!tempList.isEmpty()) {
				if (tempList.get(0) instanceof GetPrimaryKeyCodeCls) {

					List<Object> targetList = (List<Object>) tempList;

					for (int i = 0; i < targetList.size(); ++i)
						targetList.set(i, getOriginalCodeProcess(targetList.get(i), attrName, timePrefix));

					return targetList;

				} else if (tempList.get(0) instanceof String) {
					List<String> refCodeList = (List<String>) targetObj;
					List<String> originalCodeList = new ArrayList<String>();

					for (String refCode : refCodeList)
						originalCodeList.add(getDecodedRefCodeValue(refCode, attrName, timePrefix));

					return originalCodeList;
				} else
					return targetObj;
			}

		}

		return targetObj;
	}

	public String getDecodedRefCodeValue(String target, String attrName, String timePrefix) {

		String saltPrefix = RefCodeConst.getRefCodeMapName(attrName);

		/*
		 * if (target.length() < (firstHashidsLengthNum + secondHashidsLengthNum))
		 * return null;
		 * 
		 * String prefix = target.substring(0, firstHashidsLengthNum); String refCode =
		 * target.substring(firstHashidsLengthNum, target.length());
		 * 
		 * long[] decodedPrefix = firstHashids.decode(prefix);
		 * 
		 * if (decodedPrefix.length <= 0)// prefix 가 디코딩 되지 않으면 null 리턴 return null;
		 */

		Hashids second = new Hashids(timePrefix + saltPrefix + secondHashidsSalt, secondHashidsLengthNum);

		// log.debug("attrName: " + attrName + " target : " + target + " 디코딩 하는중");

		long[] result = second.decode(target);

		if (result.length <= 0)
			throw new RefCodeException("refCode : " + target + " decoding 실패");
		else
			return String.valueOf(result[0]);

	}

	public String getEncodedRefCodeValue(String target, String attrName, String timePrefix) {

		String saltPrefix = RefCodeConst.getRefCodeMapName(attrName);

		/*
		 * Calendar cal = Calendar.getInstance(); int prefix =
		 * cal.get(Calendar.MILLISECOND);
		 * 
		 * String encodedPrefix = firstHashids.encode(prefix);
		 */

		Hashids second = new Hashids(timePrefix + saltPrefix + secondHashidsSalt, secondHashidsLengthNum);

		String result = second.encode(Integer.parseInt(target));

		// log.debug("attrName: " + attrName + " target: " + target + " -> " + result);
		return result;

	}

	// map의 키 값이 특정 변수값인지 확인하는 함수
	public Boolean checkViewToCtrlTarget(Object targetObj, String str) {

		if (str == null || str.isEmpty())
			return false;

		if (targetObj instanceof GetPrimaryKeyCodeCls)
			return true;

		for (int i = 0; i < viewToCtrlTargetStr.length; ++i) {
			if (str.equals(viewToCtrlTargetStr[i])) {
				return true;
			}
		}

		if (targetObj instanceof List<?>) {

			List<?> tempList = (List<?>) targetObj;

			if (tempList.size() > 0 && tempList.get(0) instanceof GetPrimaryKeyCodeCls)
				return true;
			else
				return false;
		}

		return false;
	}

	// 컨트롤러 -> view (originalCode->refCode)
	/*
	 * JSONObject의 key 값이 RefCodeConst 클래스에 정의되어있는 상수 값을 가질경우 변경해주는 함수
	 */

	@After("execution(* com.nayuta.mosite.content.model.service.pageCommand.Pc*.execute(..))")
	public void ctrlToViewExchange(JoinPoint jp) {
		// 모델맵 가져오기
		Object[] args = jp.getArgs();
		ModelMap modelMap = null;

		for (Object obj : args) {
			if (obj instanceof ModelMap) {
				modelMap = (ModelMap) obj;
				break;
			}
		}
		// long start = System.currentTimeMillis();
		// result(JSONObject)안에 있는 것들을 타겟으로함
		JSONObject resultObj = (JSONObject) modelMap.get("result");

		if (resultObj != null) {
			Iterator<String> iter = resultObj.keySet().iterator();

			while (iter.hasNext()) {
				String attrName = iter.next();

				Object obj = resultObj.get(attrName);

				resultObj.replace(attrName, processRefCodeCtrlToView(attrName, obj, getTimeSaltPrefix()));
			}
		}
		// long end = System.currentTimeMillis();
		/* System.out.println("ctrl->view refcode실행 시간 : " + (end - start) / 1000.0); */

	}

	public Object processRefCodeCtrlToView(String attrName, Object targetObj, String timePrefix) {

		if (targetObj instanceof JSONArray) {
			JSONArray jObjArr = (JSONArray) targetObj;

			if (jObjArr != null && jObjArr.size() > 0) {
				for (int i = 0; i < jObjArr.size(); ++i)
					jObjArr.set(i, processRefCodeCtrlToView(attrName, jObjArr.get(i), timePrefix));
			}
			return jObjArr;

		} else if (targetObj instanceof JSONObject) {
			JSONObject originalJObj = (JSONObject) targetObj;
			JSONObject processedjObj = new JSONObject();

			for (Object keys : originalJObj.keySet()) {
				processedjObj.put(keys, processRefCodeCtrlToView((String) keys, originalJObj.get((String) keys), timePrefix));
			}

			return processedjObj;

		} else if (targetObj instanceof String) {

			String strObj = (String) targetObj;

			if (!strObj.isEmpty()) {

				for (String targetStr : ctrlToViewTargetStr) {

					if (attrName != null && attrName.equals(targetStr)) {

						// decode된 값이 null이면 타겟이 encode 되지 않았다고 가정하고 encode
						// decode값이 있으면 그대로 리턴
						try {
							return getDecodedRefCodeValue(strObj, attrName, timePrefix);

						} catch (RefCodeException e) {
							return getEncodedRefCodeValue(strObj, attrName, timePrefix);
						}

					}

				}

			} else
				return targetObj;

		}

		return targetObj;

	}

	// map<String,String> (refcode,originalValue) 에서 originalValue로 refCode를 가져오는 함수
	public String getKeyFromValue(Map<String, String> refCodeMap, String value) {

		try {
			for (Map.Entry<String, String> entry : refCodeMap.entrySet()) {
				if (entry.getValue().equals(value))
					return entry.getKey();
			}
			throw new RefCodeException("getKeyFromValue -> originalCode : " + value + " 를 이용해서 refCode를 refCodeMap + "
					+ refCodeMap + " 에서 찾을 수 없습니다.");

		} catch (Exception e) {
			log.error("originalCode : " + value + " 를 이용해서 refCode를 refCodeMap + " + refCodeMap + " 에서 찾을 수 없습니다.");
			throw new RefCodeException("getKeyFromValue -> originalCode : " + value + " 를 이용해서 refCode를 refCodeMap + "
					+ refCodeMap + " 에서 찾을 수 없습니다.");
		}

	}

}

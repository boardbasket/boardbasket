package com.nayuta.mosite.common.util.socialLoginUtil.loginApis;

import com.github.scribejava.core.oauth2.bearersignature.BearerSignature;
import com.github.scribejava.core.oauth2.bearersignature.BearerSignatureURIQueryParameter;

public class NaverLoginApi extends SocialLoginApi {

	public NaverLoginApi(String aCCESS_TOKEN_END_POINT, String aUTHORIZATION_BASE_URL) {
		super(aCCESS_TOKEN_END_POINT, aUTHORIZATION_BASE_URL);
	}

	@Override
	public BearerSignature getBearerSignature() {
		return BearerSignatureURIQueryParameter.instance();
	}
}

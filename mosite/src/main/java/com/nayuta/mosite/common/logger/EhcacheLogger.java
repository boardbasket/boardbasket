package com.nayuta.mosite.common.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;

public class EhcacheLogger implements CacheEventListener<Object, Object> {

	private Logger log = LogManager.getLogger();

	@Override
	public void onEvent(CacheEvent<? extends Object, ? extends Object> cacheEvent) {
		log.debug("");
		// log.debug("cache event logger getKey: " + cacheEvent.getKey() + " /
		// getOldValue: "+ cacheEvent.getOldValue() + " / getNewValue: " +
		// cacheEvent.getNewValue());

	}

}

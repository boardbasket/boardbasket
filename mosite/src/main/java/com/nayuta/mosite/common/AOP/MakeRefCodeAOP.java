package com.nayuta.mosite.common.AOP;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.nayuta.mosite.common.exceptions.RefCodeException;
import com.nayuta.mosite.common.util.RefCodeConst;
import com.nayuta.mosite.common.util.RefCodeManager;
import com.nayuta.mosite.common.util.ShorthenUUID;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;

//dao 함수에서 데이터를 불러올때 필요한 경우 임시코드맵을 생성해 주는 aop 클래스
//임시코드를 만드는 경우
//1.GetPrimaryKeyCodeCls 인터페이스를 상속한 vo를 dao가 리턴할때
//2.List<GetPrimaryKeyCodeCls> 형태를 dao가 리턴할때

@Component
@Aspect
@SuppressWarnings("unchecked")
public class MakeRefCodeAOP implements RefCodeConst {

	@Autowired
	RefCodeManager rcMgr;

	@Value("#{patterns['patterns.splitCamelCase']}")
	String splitCamelPattern;

	Pattern camelPattern = null;
	private Logger log = LogManager.getLogger();

	@PostConstruct
	public void init() {

		camelPattern = Pattern.compile(splitCamelPattern);
	}

	// @Pointcut("execution(*
	// com.nayuta.mosite.content.model.dao.BoardDaoImpl.select*(..))")
	public void selectFunc() {
	}

	// @Around("execution(*
	// com.nayuta.mosite.content.model.dao.BoardDaoImpl.insert*(..))")
	public Object insertFunc(ProceedingJoinPoint pjp) {

		GetPrimaryKeyCodeCls gk = null;

		try {

			Object[] args = pjp.getArgs();

			for (Object obj : args) {
				if (obj instanceof GetPrimaryKeyCodeCls) {
					gk = (GetPrimaryKeyCodeCls) obj;
					break;
				}
			}

			Object result = pjp.proceed();

			makeAndSetRefCodeProcess(gk);

			return result;
		} catch (Throwable e) {
			throw new RefCodeException("insert후 refCode를 만들거나 병합중 오류발생 " + e.getMessage());
			// return pjp.getArgs();
		}

	}

	// @AfterReturning(pointcut = "selectFunc()", returning = "retVal")
	public void makeRefCode(JoinPoint jp, Object retVal) {

		//long start = System.currentTimeMillis();
		if (retVal instanceof List) {
			List<?> returning = (List<?>) retVal;

			if (returning.size() > 0) {
				if (returning.get(0) instanceof GetPrimaryKeyCodeCls) {

					List<? extends GetPrimaryKeyCodeCls> targetList = (List<? extends GetPrimaryKeyCodeCls>) returning;

					for (GetPrimaryKeyCodeCls targetObj : targetList) {

						makeAndSetRefCodeProcess(targetObj);

					}
				}
			}

		} else if (retVal instanceof GetPrimaryKeyCodeCls) {
			GetPrimaryKeyCodeCls targetObj = (GetPrimaryKeyCodeCls) retVal;

			makeAndSetRefCodeProcess(targetObj);

		}
		//long end = System.currentTimeMillis();
		//log.debug("make refCode 실행 시간 : " + (end - start) / 1000.0);

	}

	// @After("execution(*
	// com.nayuta.mosite.member.model.service.UserLoginSuccessHandler.*(..))")
	public void refCodeResetPoint(JoinPoint jp) {

		rcMgr.removeAllRefMap();

	}

	// delete로 시작하는 함수의 refCode를 지우기위한 aop,delete 다음에 나오는 단어들을 포함하는 refCodeMap이 있는경우
	// 그 refcode만 지운다
	// @Around("execution(*
	// com.nayuta.mosite.content.model.dao.BoardDaoImpl.delete*(..))")
	public Object daoDeleteFunctions(ProceedingJoinPoint pjp) {

		Object returnValue = null;
		Object[] args = pjp.getArgs();

		String funcName = pjp.getSignature().getName();
		String prefix = "delete";
		String targetCode = funcName.substring(funcName.indexOf(prefix) + prefix.length()).toLowerCase();
		String refcodeMapName = null;
		for (String str : RefCodeConst.refCodeNames) {
			if (str.toLowerCase().contains(targetCode)) {
				refcodeMapName = str;
				break;
			}
		}

		try {
			returnValue = (int) pjp.proceed();

			for (Object obj : args)
				deleteRefCodeProcess(obj, refcodeMapName);

		} catch (Throwable e) {
			throw new RefCodeException("refCode를 지우는 도중 문제가 생겼습니다.");
		}

		return returnValue;

	}

	////////////////////////////////////////////// utilfunc////////////////////////////////////////////////////////

	// dao 클래스의 delete로 시작하는 함수 파라미터 값을 찾아서 그 값을 refCodeMap에서 지움
	public void deleteRefCodeProcess(Object obj, String targetRefCodeMapName) {

		if (obj instanceof String) {
			removeRefCodeWithValue(targetRefCodeMapName, (String) obj);
		} else if (obj instanceof GetPrimaryKeyCodeCls) {
			GetPrimaryKeyCodeCls gk = (GetPrimaryKeyCodeCls) obj;
			for (String refCodeMapName : ((GetPrimaryKeyCodeCls) obj).pullPrimaryKey().keySet()) {

				if (targetRefCodeMapName == null)
					deleteRefCodeProcess(gk.pullPrimaryKey().get(refCodeMapName), refCodeMapName);
				else {
					if (refCodeMapName.equals(targetRefCodeMapName))
						deleteRefCodeProcess(gk.pullPrimaryKey().get(targetRefCodeMapName), targetRefCodeMapName);
				}

			}

		} else if (obj instanceof List<?>) {
			List<?> tempList = (List<?>) obj;
			if (tempList.size() > 0 && tempList.get(0) instanceof GetPrimaryKeyCodeCls) {
				List<? extends GetPrimaryKeyCodeCls> targetList = (List<? extends GetPrimaryKeyCodeCls>) tempList;

				for (GetPrimaryKeyCodeCls gk : targetList)
					deleteRefCodeProcess(gk, targetRefCodeMapName);

			}
		}

	}

	public void makeAndSetRefCodeProcess(GetPrimaryKeyCodeCls returnVal) {

		for (String refCodeMapName : returnVal.pullPrimaryKey().keySet()) {

			Object targetObj = returnVal.pullPrimaryKey().get(refCodeMapName);

			if (targetObj instanceof String) {
				String strRefCode = getRefCodeString((String) targetObj);

				Map<String, String> refCode = new HashMap<String, String>();
				refCode.put(strRefCode, (String) targetObj);
				if (rcMgr.isRefMapNull(refCodeMapName)) {
					rcMgr.setRefCodeMap(refCodeMapName, refCode);
					log.debug(refCodeMapName + "을 생성함" + rcMgr.getRefCodeMap(refCodeMapName));
				} else {
					rcMgr.mergeCodeRefMap(refCodeMapName, refCode);
					log.debug(refCodeMapName + "을 추가함" + (String) targetObj + " -> " + refCode);
				}
			} else if (targetObj instanceof GetPrimaryKeyCodeCls) {
				makeAndSetRefCodeProcess((GetPrimaryKeyCodeCls) targetObj);
			} else if (targetObj instanceof List<?>) {
				List<?> tempList = (List<?>) targetObj;
				if (tempList.size() > 0 && tempList.get(0) instanceof GetPrimaryKeyCodeCls) {
					List<? extends GetPrimaryKeyCodeCls> targetList = (List<? extends GetPrimaryKeyCodeCls>) tempList;
					for (GetPrimaryKeyCodeCls target : targetList)
						makeAndSetRefCodeProcess(target);

				}
			}

		}
	}

	public static String getRefCodeString(String originalCode) {

		return ShorthenUUID.getInstance().getShorthenUUID(UUID.randomUUID());
	}

	// refCodeMap 이름으로
	public boolean removeRefCodeWithValue(String refCodeMapName, String value) {

		Map<String, String> refCodeMap = rcMgr.getRefCodeMap(refCodeMapName);
		if (refCodeMap == null)
			return false;

		Map<String, String> swapped = refCodeMap.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey, (p1, p2) -> p1));

		String key = swapped.get(value);

		if (key != null) {

			if (refCodeMap.containsKey(key)) {
				refCodeMap.remove(key);

				//System.out.println(key + " is removed From " + refCodeMapName);
				return true;
			} else
				return false;

		} else
			return false;

	}

	// refCodeMap 으로
	public boolean removeRefCodeWithValue(Map<String, String> refCodeMap, String value) {

		if (refCodeMap == null)
			return false;

		Map<String, String> swapped = refCodeMap.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey, (p1, p2) -> p1));

		String key = swapped.get(value);

		if (key != null) {

			if (refCodeMap.containsKey(key)) {
				refCodeMap.remove(key);

				System.out.println(key + " is removed From " + refCodeMap);
				return true;
			} else
				return false;

		} else
			return false;

	}

	public boolean removeRefCode(String refCodeMapName, String refCode) {
		HttpSession session = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
				.getSession();
		Map<String, String> refCodeMap = (Map<String, String>) session.getAttribute(refCodeMapName);

		if (refCodeMap == null)
			return false;

		if (refCodeMap.containsKey(refCode)) {
			refCodeMap.remove(refCode);
			System.out.println(refCode + " is removed From " + refCodeMapName + " : " + refCodeMap);
			return true;
		} else
			return false;
	}

	public boolean removeRefCode(Map<String, String> refCodeMap, String refCode) {

		if (refCodeMap == null)
			return false;

		if (refCodeMap.containsKey(refCode)) {
			refCodeMap.remove(refCode);
			System.out.println(refCode + " is removed From " + refCodeMap + " : " + refCodeMap);
			return true;
		} else
			return false;
	}
}

package com.nayuta.mosite.common.exceptions;

/*service 부분에서 에러가 났을 경우에 발생하는 함수*/

public class MVCServiceException extends BadRequestException {

	private static final long serialVersionUID = -1442738235708552434L;

	public MVCServiceException() {
		super();

	}

	public MVCServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public MVCServiceException(String message, Throwable cause) {
		super(message, cause);

	}

	public MVCServiceException(String message) {
		super(message);

	}

	public MVCServiceException(Throwable cause) {
		super(cause);

	}

}

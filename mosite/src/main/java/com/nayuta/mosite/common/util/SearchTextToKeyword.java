package com.nayuta.mosite.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

//검색어를 필터링 해서 실제 검색에 쓰일 단어를 리턴해줌

@Component
public class SearchTextToKeyword {

	private final int MAX_SEARCH_KEYWORD_LENGTH;
	private final int MIN_SEARCH_KEYWORD_LENGTH;
	private final int SEARCH_KEYWORD_NUM_LIMIT;

	private final String SELECT_SPECIAL_CHAR_PTN;

	public SearchTextToKeyword(@Value("#{patterns['patterns.specialChar']}") String sELECT_SPECIAL_CHAR_PTN,
			@Value("#{new Integer(serverConst['server_const.default_top_tag_num'])}") Integer default_top_tag_num,
			@Value("#{new Integer(serverConst['server_const.search_keyword_num_limit'])}") Integer sEARCH_KEYWORD_NUM_LIMIT,
			@Value("#{new Integer(serverConst['server_const.max_search_keyword_length'])}") Integer mAX_SEARCH_KEYWORD_LENGTH,
			@Value("#{new Integer(serverConst['server_const.min_word_length'])}") Integer mIN_SEARCH_KEYWORD_LENGTH) {

		this.MAX_SEARCH_KEYWORD_LENGTH = mAX_SEARCH_KEYWORD_LENGTH;
		this.SELECT_SPECIAL_CHAR_PTN = sELECT_SPECIAL_CHAR_PTN;
		this.SEARCH_KEYWORD_NUM_LIMIT = sEARCH_KEYWORD_NUM_LIMIT;
		this.MIN_SEARCH_KEYWORD_LENGTH = mIN_SEARCH_KEYWORD_LENGTH;
	}

	public List<String> toKeyword(String board_name) {
		List<String> keyword = new ArrayList<>();

		if (!StringUtils.isEmpty(board_name)) {
			LinkedHashSet<String> removeDuplicate = new LinkedHashSet<>();
			// 길이로 자르고 특수문자를 모두 빈칸으로 치환
			String filteredStr = board_name.substring(0, Math.min(board_name.length(), MAX_SEARCH_KEYWORD_LENGTH))
					.replaceAll(SELECT_SPECIAL_CHAR_PTN, " ");

			// 빈칸으로 split 한 단어들 중에 최소길이를 만족하는 것들만 걸러서 최종적으로 검색에 쓰일 키워드를 저장
			List<String> result = Arrays.stream(filteredStr.split("\\s+"))// .filter(k -> k.length() >= MIN_SEARCH_KEYWORD_LENGTH)
					.collect(Collectors.toList());

			// 영어단어면 대문자로 치환
			result = result.stream().map(s -> {
				if (s.matches("^[a-zA-Z]*$"))
					return s.toUpperCase();
				else
					return s;
			}).collect(Collectors.toList());

			//단어들을 저장
			keyword.addAll(result);

			// 중복제거
			keyword.forEach(k -> removeDuplicate.add(k));

			keyword = removeDuplicate.stream().collect(Collectors.toList());

			return keyword.subList(0, Math.min(keyword.size(), SEARCH_KEYWORD_NUM_LIMIT));
		} else
			return keyword;

	}

	public List<String> toKeyword(Object board_name) {
		try {
			return toKeyword((String) board_name);
		} catch (Exception e) {
			return null;
		}

	}

	public int getMAX_SEARCH_KEYWORD_LENGTH() {
		return MAX_SEARCH_KEYWORD_LENGTH;
	}

}

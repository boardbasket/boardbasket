package com.nayuta.mosite.common.util;

//해당 클래스의 캐시 이름을 리턴하도록 하는 인터페이스
public interface GetCacheName {

	public String makeCacheName();
}

package com.nayuta.mosite.common.util;

import org.springframework.stereotype.Component;

@Component
public class PageInfo {

	private int page_count = 10; // 한 화면에 몇 페이지가 보여질 건지
	private int startPage; // 한번에 표시될 게시글들의 시작 페이지
	private int endPage; // 한번에 표시될 게시글들의 마지막 페이지
	private int maxPage; // 전체 페이지의 마지막 페이지
	private int limit; // 한 페이지당 게시글 수
	private int currentPage; // 현재 페이지
	private int listCount;// 게시글의 총 갯수
	private int startRow;// limit 시작
	private int endRow;// 안씀 (oracle)
	private boolean isMaxLoad = false;// 마지막 페이지 로드인지

	public PageInfo(int currPage, int listCount, int limit, int pageCount) {
		this.page_count = pageCount;
		this.currentPage = currPage;
		this.listCount = listCount;
		this.limit = limit;
		initFunction();
	}

	public PageInfo(int currPage, int listCount, int limit) {
		this.currentPage = currPage;
		this.listCount = listCount;
		this.limit = limit;

		initFunction();
	}

	private void initFunction() {
		maxPage = (int) ((double) listCount / limit + 0.9);
		maxPage = listCount / limit;

		if (listCount % limit > 0)
			maxPage++;

		// 현재 화면에 표시할 페이지 갯수
		// 첫 페이지의 번호
		// 한 화면에 10개의 페이지를 표시하고 싶다.
		startPage = (((int) ((double) currentPage / limit + 0.9)) - 1) * limit + 1;
		startPage = ((currentPage - 1) / page_count) * page_count + 1;

		// 한 화면에 표시할 마지막 페이지 번호
		endPage = startPage + limit - 1;
		endPage = startPage + page_count - 1;
		// 만약에 1,2,3,4,5,6,7,8,9,10을 보여주려고 한다면
		// 하지만 7페이지 밖에 없을때
		if (maxPage < endPage)
			endPage = maxPage;

		if (currentPage >= maxPage)
			isMaxLoad = true;
		else
			isMaxLoad = false;

		startRow = (currentPage - 1) * limit; // + 1; mysql은 limit를 쓰는데 0부터 시작임
		endRow = startRow + (limit - 1);
	}

	public PageInfo(int currentPage, int limit) {

		this.currentPage = currentPage;
		this.limit = limit;
		this.startRow = (currentPage - 1) * limit; // +1;

	}

	public PageInfo() {
		super();
	}

	public PageInfo(int startPage, int endPage, int maxPage, int limit, int currentPage, int listCount, int startRow, int endRow,
			boolean isMaxLoad) {
		super();
		this.startPage = startPage;
		this.endPage = endPage;
		this.maxPage = maxPage;
		this.limit = limit;
		this.currentPage = currentPage;
		this.listCount = listCount;
		this.startRow = startRow;
		this.endRow = endRow;
		this.isMaxLoad = isMaxLoad;
	}

	public int getStartPage() {
		return startPage;
	}

	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}

	public int getEndPage() {
		return endPage;
	}

	public void setEndPage(int endPage) {
		this.endPage = endPage;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getListCount() {
		return listCount;
	}

	public void setListCount(int listCount) {
		this.listCount = listCount;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public boolean isMaxLoad() {
		return isMaxLoad;
	}

	public void setMaxLoad(boolean isMaxLoad) {
		this.isMaxLoad = isMaxLoad;
	}

	public int getPage_count() {
		return page_count;
	}

	public void setPage_count(int page_count) {
		this.page_count = page_count;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + currentPage;
		result = prime * result + endPage;
		result = prime * result + endRow;
		result = prime * result + (isMaxLoad ? 1231 : 1237);
		result = prime * result + limit;
		result = prime * result + listCount;
		result = prime * result + maxPage;
		result = prime * result + page_count;
		result = prime * result + startPage;
		result = prime * result + startRow;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PageInfo other = (PageInfo) obj;
		if (currentPage != other.currentPage)
			return false;
		if (endPage != other.endPage)
			return false;
		if (endRow != other.endRow)
			return false;
		if (isMaxLoad != other.isMaxLoad)
			return false;
		if (limit != other.limit)
			return false;
		if (listCount != other.listCount)
			return false;
		if (maxPage != other.maxPage)
			return false;
		if (page_count != other.page_count)
			return false;
		if (startPage != other.startPage)
			return false;
		if (startRow != other.startRow)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PageInfo [page_count=" + page_count + ", startPage=" + startPage + ", endPage=" + endPage + ", maxPage=" + maxPage
				+ ", limit=" + limit + ", currentPage=" + currentPage + ", listCount=" + listCount + ", startRow=" + startRow
				+ ", endRow=" + endRow + ", isMaxLoad=" + isMaxLoad + "]";
	}

}

package com.nayuta.mosite.member.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.vo.MemberData;

@Service("mmUpdateUserData")
public class MmUpdateUserData extends AbstractMemberCls implements CommandInterface {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public void execute(ModelMap modelMap) {

		MemberData memberData = (MemberData) modelMap.get("memberData");

		memberData.setMember_pwd(passwordEncoder.encode(memberData.getMember_pwd()));
		mDao.updateMemberData(memberData);

		modelMap.put("result", resultObj);

	}

}

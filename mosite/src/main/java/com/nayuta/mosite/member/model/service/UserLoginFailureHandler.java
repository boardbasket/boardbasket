package com.nayuta.mosite.member.model.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class UserLoginFailureHandler implements AuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse res, AuthenticationException arg2)
			throws IOException, ServletException {
		
		req.setAttribute("errMsg", "입력 정보가 올바르지 않습니다.");
		req.getRequestDispatcher("/WEB-INF/views/user/login.jsp").forward(req, res);
	}

}

package com.nayuta.mosite.member.controller;

import java.nio.file.AccessDeniedException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nayuta.mosite.content.model.service.CommandContainer;
import com.nayuta.mosite.member.model.service.MmCheckDuplicateId;
import com.nayuta.mosite.member.model.service.MmDeleteMember;
import com.nayuta.mosite.member.model.service.MmInsertMember;
import com.nayuta.mosite.member.model.service.MmMemberCheck;
import com.nayuta.mosite.member.model.service.MmPasswordCheck;
import com.nayuta.mosite.member.model.service.MmUpdateUserData;
import com.nayuta.mosite.member.model.vo.Member;
import com.nayuta.mosite.member.model.vo.MemberData;
import com.nayuta.mosite.member.model.vo.ValidateMember;

@Controller
@Validated
public class MemberController {

	@Autowired
	private CommandContainer cc;

	@Autowired
	@Qualifier("mmMemberCheck")
	private MmMemberCheck mmMemberCheck;

	@Autowired
	@Qualifier("mmInsertMember")
	private MmInsertMember mmInsertMember;

	@Autowired
	@Qualifier("mmPasswordCheck")
	private MmPasswordCheck mmPasswordCheck;

	@Autowired
	@Qualifier("mmUpdateUserData")
	private MmUpdateUserData mmUpdateUserData;

	@Autowired
	@Qualifier("mmCheckDuplicateId")
	private MmCheckDuplicateId mmCheckDuplicateId;

	@Autowired
	@Qualifier("mmDeleteMember")
	private MmDeleteMember mmDeleteMember;

	@Autowired
	private ValidateMember validateMember;

	@RequestMapping("/user/login.do")
	public String login(Device device, HttpServletRequest request, HttpSession session) {

		// 로그인 페이지 들어오기 전 페이지 저장
		if (session.getAttribute("prevPage") != null)
			session.setAttribute("prevPage", request.getHeader("referer"));

		return "user/login";
	}

	@RequestMapping("/user/signUp.do")
	public String regist(ModelMap modelMap) {

		return "user/signUp";
	}

	@RequestMapping(value = "/user/updateMemberData.do", method = RequestMethod.POST)
	public String updateMemberData(ModelMap modelMap, Authentication au, @ModelAttribute("memberData") MemberData memberData,
			BindingResult bResult, RedirectAttributes flash) {

		memberData.setMember_id(((Member) au.getPrincipal()).getUsername());

		validateMember.validate(memberData, bResult);
		String errorMsg = "";
		if (bResult.hasErrors()) {

			for (FieldError fe : bResult.getFieldErrors()) {
				errorMsg = errorMsg + fe.getDefaultMessage() + " ";
			}
			flash.addFlashAttribute("msg", "유저 정보 수정에 실패했습니다. [" + errorMsg + "]");
		} else {
			cc.execute(mmUpdateUserData, modelMap);
			flash.addFlashAttribute("msg", "정보 수정을 완료 하였습니다.");
		}
		return "redirect:userSetting.do";
	}

	@RequestMapping(value = "/user/userSetting.do", method = { RequestMethod.POST, RequestMethod.GET })
	public String userSetting(ModelMap modelMap, @ModelAttribute("password") String password, Authentication au) {

		modelMap.addAttribute("username", au.getName());
		cc.execute(mmPasswordCheck, modelMap);

		return "user/settings";
	}

	@RequestMapping("/accessDenied.do")
	public String accessDenied(ModelMap modelMap, Authentication auth, HttpServletRequest request) {

		AccessDeniedException ade = (AccessDeniedException) request.getAttribute(WebAttributes.ACCESS_DENIED_403);
		modelMap.put("errMsg", ade);

		return "user/denied";
	}

	@RequestMapping(value = "/user/insertMember.do", method = RequestMethod.POST)
	public String insertMember(ModelMap modelMap, @ModelAttribute("memberData") MemberData memberData, BindingResult bResult,
			RedirectAttributes flash, HttpServletRequest request, HttpSession session) {

		String views = "redirect:/user/login.do";

		validateMember.validate(memberData, bResult);

		cc.execute(mmCheckDuplicateId, modelMap);

		if (session.getAttribute("checkedId") == null) {
			flash.addFlashAttribute("msg", "중복된 아이디 입니다.");
			views = "redirect:/user/signUp.do";
		} else if (((String) session.getAttribute("checkedId")).equals(memberData.getMember_id())) {

			if (!bResult.hasErrors()) {
				cc.execute(mmInsertMember, modelMap);
				flash.addFlashAttribute("msg", "가입을 환영합니다.");
			} else {
				flash.addFlashAttribute("fieldErrors", bResult.getFieldErrors());

				views = "redirect:/user/signUp.do";
			}

		} else {
			flash.addFlashAttribute("msg", "아이디 중복 체크를 해주세요.");
			views = "redirect:/user/signUp.do";
		}

		session.removeAttribute("checkedId");

		return views;
	}

	@RequestMapping(value = "/checkDuplicateId.ajax", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> checkDuplicateId(ModelMap modelMap, @ModelAttribute("memberData") MemberData memberData) {

		cc.execute(mmCheckDuplicateId, modelMap);

		Map<String, Object> obj = new HashMap<String, Object>();

		obj.put("result", modelMap.get("result"));

		return obj;
	}

	@RequestMapping("/admin.do")
	public String admin(ModelMap modelMap) {

		return "admin/main";
	}

	@RequestMapping(value = "/user/deleteMember.do", method = RequestMethod.POST)
	public String deleteMember(ModelMap modelMap, HttpSession session, HttpServletResponse response) {

		cc.execute(mmDeleteMember, modelMap);
		session.setAttribute("prevPage", null);
		modelMap.addAttribute("logOut", true);
		return "user/redirectPage";
	}

	/*
	 * @RequestMapping(value = "/logOut.do", method = RequestMethod.GET) public
	 * String logOut(ModelMap modelMap, HttpSession session, HttpServletResponse
	 * response) {
	 * 
	 * return "redirect:/"; }
	 */

}

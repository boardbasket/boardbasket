package com.nayuta.mosite.member.model.vo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component("validateMember")
public class ValidateMember implements Validator {

	
	@Value("#{patterns['patterns.id']}")
	protected String idPattern; 
	
	@Value("#{patterns['patterns.password']}")
	protected String pwdPattern; 
	
	@Value("#{patterns['patterns.nickname']}")
	protected String nicknamePattern; 
	
	@Override
	public boolean supports(Class<?> clazz) {

		return (MemberData.class.isAssignableFrom(clazz));
	}

	@Override
	public void validate(Object m, Errors errors) {
		MemberData member = (MemberData) m;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "member_id", "required", "필수 입력 항목입니다.");

		if (member.getMember_id() != null && !member.getMember_id().matches(idPattern))
			errors.rejectValue("member_id", "NotMatch", "아이디 형식에 맞지 않습니다.");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "member_pwd", "required", "필수 입력 항목입니다.");

		if (member.getMember_pwd() != null
				&& !member.getMember_pwd().matches(pwdPattern)) 
			errors.rejectValue("member_pwd", "NotMatch", "비밀번호 형식에 맞지 않습니다.");

		/* ValidationUtils.rejectIfEmptyOrWhitespace(errors,"member_nickname","") */

		if (member.getMember_nickname() != null && !member.getMember_nickname().matches(nicknamePattern))
			errors.rejectValue("member_nickname", "NotMatch", "닉네임 형식에 맞지 않습니다.");

		if (member.getMember_rePwd() != null && member.getMember_pwd() != null
				&& !member.getMember_pwd().equals(member.getMember_rePwd())) {
			errors.reject("passwordMismatch", "비밀번호가 같지 않습니다.");
		}

	}

}

package com.nayuta.mosite.member.model.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

/*
 * 유저 로그인 이후 권한이 충족되지 않을때 -> accessdeniedhandler
 * 로그인 되지 않은 상태에서 권한이 충족되지 않을때 ->authenticationEntrypoint
 * 
 * */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {

		String ajaxHeader = request.getHeader("AJAX");
		String result = "";

		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.setCharacterEncoding("UTF-8");

		if (ajaxHeader == null) { // null로 받은 경우는 X-Ajax-call 헤더 변수가 없다는 의미이기 때문에 ajax가 아닌 일반적인 방법으로 접근했음을 의미한다
			request.setAttribute("접근이 거부되었습니다.", authException);
			request.getRequestDispatcher("/WEB-INF/views/user/denied.jsp").forward(request, response);
		} else {
			if ("true".equals(ajaxHeader)) { // true로 값을 받았다는 것은 ajax로 접근했음을 의미한다
				result = "{\"result\" : {\"status\": -1, \"msg\" : \"" + "로그인이 필요합니다." + "\"}}";
			} else { // 헤더 변수는 있으나 값이 틀린 경우이므로 헤더값이 틀렸다는 의미로 돌려준다
				result = "{\"result\" : {\"status\": -1, \"msg\" : \"" + "Header Value Mismatch" + "\"}}";
			}
			response.getWriter().print(result);
			response.getWriter().flush();
			return;
		}

	}

}

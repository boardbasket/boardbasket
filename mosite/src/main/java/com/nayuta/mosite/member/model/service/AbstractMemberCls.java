package com.nayuta.mosite.member.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.nayuta.mosite.member.model.dao.MemberDaoImpl;
import com.nayuta.mosite.member.model.vo.Member;

import net.sf.json.JSONObject;

public abstract class AbstractMemberCls {

	@Autowired
	protected MemberDaoImpl mDao;
	
	protected JSONObject resultObj = new JSONObject();
	
	public String getMemberId() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String memberId = null;

		if (obj instanceof UserDetails)
			memberId = ((UserDetails) obj).getUsername();
		else
			memberId = obj.toString();

		return memberId;
	}
	
	public String getMemberNo() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (obj instanceof UserDetails)
			return ((Member) obj).getMember_no();
		else
			return null;

	}
	
}

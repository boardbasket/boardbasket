package com.nayuta.mosite.member.model.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.nayuta.mosite.member.model.vo.MemberData;

@Repository
public class MemberDaoImpl implements MemberDao {

	@Override
	public int selectCheckMemberExist(String memberId) {

		return sqlSession.selectOne("member.selectCheckMemberExist", memberId);
	}

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSession sqlSession;

	public MemberData selectOneMember(String id) {

		return sqlSession.selectOne("member.memberCheck", id);
	}

	@Override
	public int insertMember(MemberData member) {

		return sqlSession.insert("member.insertMember", member);
	}

	@Override
	public int insertSocialMember(MemberData member) {

		return sqlSession.insert("member.insertSocialMember", member);
	}

	@Override
	public int checkDuplicateId(String username) {

		return sqlSession.selectOne("member.checkDuplicateId", username);
	}

	@Override
	public int deleteMember(String username) {

		return sqlSession.delete("member.deleteMember", username);
	}

	@Override
	public int updateMemberData(MemberData memberData) {

		return sqlSession.update("member.updateMemberData", memberData);
	}

	@Override
	public int updateRefreshToken(String id, String refreshToken) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("member_id", id);
		param.put("refresh_token", refreshToken);

		return sqlSession.update("member.updateRefreshToken", param);
	}

	@Override
	public String selectRefreshToken(String id) {

		return sqlSession.selectOne("member.selectRefreshToken", id);
	}

}

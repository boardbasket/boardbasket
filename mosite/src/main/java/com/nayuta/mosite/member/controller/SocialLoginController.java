package com.nayuta.mosite.member.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.scribejava.core.exceptions.OAuthException;
import com.nayuta.mosite.common.util.socialLoginUtil.loginMgrs.SocialLoginMgr;
import com.nayuta.mosite.content.model.service.CommandContainer;
import com.nayuta.mosite.member.model.service.MmInsertMember;
import com.nayuta.mosite.member.model.service.MmMemberCheck;

@Controller
@Validated
public class SocialLoginController {

	@Autowired
	private CommandContainer cc;

	@Autowired
	@Qualifier("naverLoginMgr")
	private SocialLoginMgr naverLoginMgr;

	@Autowired
	@Qualifier("googleLoginMgr")
	private SocialLoginMgr googleLoginMgr;

	@Autowired
	@Qualifier("kakaoLoginMgr")
	private SocialLoginMgr kakaoLoginMgr;

	@Autowired
	@Qualifier("mmInsertMember")
	private MmInsertMember mmInsertMember;

	@Autowired
	@Qualifier("mmMemberCheck")
	private MmMemberCheck mmMemberCheck;

	@Autowired
	@Qualifier("rememberMeServices")
	PersistentTokenBasedRememberMeServices myRemembermeService;

	protected Logger log = LogManager.getLogger();

	@RequestMapping(value = "/login/social/{target}", method = RequestMethod.GET)
	public String requestSocialLogin(HttpSession session, @PathVariable(name = "target") String target,
			HttpServletRequest request, @RequestParam(value = "remember-me", required = false) String rememberMe)
			throws UnsupportedEncodingException {

		session.setAttribute("rememberMe", rememberMe);

		switch (target) {
		case "naver":
			return "redirect:" + naverLoginMgr.getAccessTokenUrl(session);
		case "google":
			return "redirect:" + googleLoginMgr.getAccessTokenUrl(session);
		case "kakao":
			return "redirect:" + kakaoLoginMgr.getAccessTokenUrl(session);
		default:
			break;

		}

		return "errors/erorr500";

	}

	@RequestMapping(value = "/login/callback/{provider}", method = { RequestMethod.GET, RequestMethod.POST })
	@Transactional(rollbackFor = Exception.class)
	public String naverLoginCallback(String state, @ModelAttribute("code") String code, ModelMap modelMap,
			@PathVariable(name = "provider") String provider, HttpSession session, HttpServletRequest request) {

		String sessionState = (String) session.getAttribute("state");
		try {
			if (StringUtils.pathEquals(sessionState, state)) {
				session.removeAttribute("state");
				switch (provider) {
				case "naver":
					modelMap.put("loginMgr", naverLoginMgr);
					break;
				case "google":
					modelMap.put("loginMgr", googleLoginMgr);
					break;
				case "kakao":
					modelMap.put("loginMgr", kakaoLoginMgr);
					break;
				default:
					return "errors/error500";
				}

				// 멤버 로그인 처리
				cc.execute(mmMemberCheck, modelMap);
				// 로그인 할수 없으면(memberData가 존재하지 않으면) 가입 과정 거친후 다시 로그인
				if (modelMap.get("memberData") != null) {
					cc.execute(mmInsertMember, modelMap);
					cc.execute(mmMemberCheck, modelMap);
				}

				Object rememberMe = session.getAttribute("rememberMe");

				if (rememberMe != null && rememberMe.equals("true")) {
					session.removeAttribute("rememberMe");
					modelMap.addAttribute("rememberMe", true);
					return "user/redirectPage";
				} else
					return "user/redirectPage";
			} else
				throw new OAuthException("로그인 도중 문제가 생겼습니다.");
		} catch (Exception e) {
			throw new OAuthException("로그인 도중 문제가 생겼습니다.");
		}

	}

	@RequestMapping(value = "/login/rememberMe.do", method = { RequestMethod.GET })
	public String rememberMe(@RequestParam(value = "remember-me", required = false) String rememberMe, HttpServletRequest request,
			HttpServletResponse response, Authentication auth) {

		log.debug("rememberMe! : " + rememberMe);

		// rememberme 토큰을 db에 집어넣음
		if (!StringUtils.isEmpty(rememberMe) && rememberMe.equals("true") && auth.isAuthenticated())
			myRemembermeService.loginSuccess(request, response, auth);
		return "redirect:/";
	}

}

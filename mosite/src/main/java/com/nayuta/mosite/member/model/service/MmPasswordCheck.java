package com.nayuta.mosite.member.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.vo.MemberData;

import net.sf.json.JSONObject;

@Service("mmPasswordCheck")
public class MmPasswordCheck extends AbstractMemberCls implements CommandInterface {

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Override
	public void execute(ModelMap modelMap) {

		String password = (String) modelMap.get("password");
		String username = (String) modelMap.get("username");
		JSONObject resultObj = new JSONObject();

		if (password == null || password.isEmpty()) {
			resultObj.put("checkPasswordResult", false);
		} else {
			MemberData member = mDao.selectOneMember(username);

			if (passwordEncoder.matches(password, member.getMember_pwd()))
				resultObj.put("checkPasswordResult", true);
			else {
				resultObj.put("errorMsg", "패스워드가 일치하지 않습니다 ");
				resultObj.put("checkPasswordResult", false);
			}
		}
		modelMap.addAttribute("result", resultObj);

	}

}

package com.nayuta.mosite.member.model.service;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.nayuta.mosite.common.util.SessionAttributeUtil;
import com.nayuta.mosite.common.util.socialLoginUtil.loginMgrs.SocialLoginMgr;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.dao.MemberDao;
import com.nayuta.mosite.member.model.vo.Member;
import com.nayuta.mosite.member.model.vo.MemberData;

@Component(value = "mmMemberCheck")
public class MmMemberCheck implements CommandInterface, UserDetailsService {

	@Autowired
	private MemberDao mDao;

	@Autowired
	private SessionAttributeUtil sau;

	@Value("#{serverConst['server_const.remember_me_security_key']}")
	private String rememberMeKey;

	@Value("#{serverConst['server_const.social_member_pwd']}")
	private String socialMemberPwd;

	@Autowired
	@Qualifier("theAuthenticationManager")
	AuthenticationManager authenticationManager;

	protected Logger log = LogManager.getLogger();

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		SocialLoginMgr loginMgr = (SocialLoginMgr) modelMap.get("loginMgr");
		String code = (String) modelMap.get("code");

		OAuth2AccessToken accessTokenObj = sau.getAttribute("accessTokenObj") != null
				? (OAuth2AccessToken) sau.getAttribute("accessTokenObj")
				: null;
		String _accessToken = accessTokenObj != null ? accessTokenObj.getAccessToken() : null;
		String profile = null;

		try {

			// url을 이용해서 accessToken 을 가져온다
			if (_accessToken == null) {
				accessTokenObj = loginMgr.getAccessToken(code);
				sau.setAttribute("accessTokenObj", accessTokenObj);
				_accessToken = accessTokenObj.getAccessToken();
			}

			// 유효한 토큰인지 확인 refresh 토큰을 이용해서 accesstoken을 다시 가져옴
			/*
			 * if (!loginMgr.verifyAccessToken(_accessToken)) {
			 * loginMgr.refreshAccessToken(mDao.selectRefreshToken(profile)); _accessToken =
			 * accessTokenObj.getAccessToken();
			 * 
			 * }
			 */

			profile = loginMgr.getUserProfile(_accessToken);
			if (mDao.selectCheckMemberExist(profile) <= 0)
				throw new UsernameNotFoundException("존재하지 않는 유저");

			UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(profile, socialMemberPwd);
			Authentication auth = authenticationManager.authenticate(authReq);

			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(auth);
			HttpSession session = sau.getRequest().getSession(true);
			session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);

		} catch (UsernameNotFoundException e) {

			// 유저 못찾으면 가입정보를 컨트롤러에 보내줌
			MemberData memData = new MemberData(profile, loginMgr.getSOCIAL_LOGIN_PROVIDER(), profile);
			modelMap.put("memberData", memData);

		} catch (Exception e) {
			throw new OAuthException(e.getMessage());
		}
	}

	public void securityLoginWithoutLoginForm(Member member) {

		// 로그인 세션에 들어갈 권한을 설정합니다.
		// List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		// list.add(new SimpleGrantedAuthority("ROLE_USER"));

		Object rememberMeFlag = sau.getAttribute("rememberMe");

		Authentication auth = null;

		SecurityContext sc = SecurityContextHolder.getContext();
		// Authentication authenticatedUser = authenticationManager.authenticate(auth);

		if (rememberMeFlag != null && ((String) rememberMeFlag).equals("true")) {
			auth = new RememberMeAuthenticationToken(rememberMeKey, member, member.getAuthorities());
		} else {
			auth = new UsernamePasswordAuthenticationToken(member, member.getPassword());
		}

		sc.setAuthentication(auth);

		HttpSession session = sau.getRequest().getSession(true);

		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
		log.info("유저 " + member.getMember_no() + " 로그인");
		session.setAttribute("memberNo", member.getMember_no());

	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		MemberData memberData = mDao.selectOneMember(username);
		if (memberData == null)
			throw new UsernameNotFoundException("유저가 존재하지 않습니다.");

		Member userDetail = new Member(memberData.getMember_id(), memberData.getMember_pwd(),
				Integer.valueOf(memberData.getEnabled()) == 1, memberData.getAuthority(), memberData.getMember_no(),
				memberData.getSocial_login_provider());

		return userDetail;

	}

}

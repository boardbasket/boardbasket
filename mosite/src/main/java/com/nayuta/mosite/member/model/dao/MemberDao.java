package com.nayuta.mosite.member.model.dao;

import com.nayuta.mosite.member.model.vo.MemberData;

public interface MemberDao {

	public MemberData selectOneMember(String id);

	public int selectCheckMemberExist(String memberId);

	public int insertMember(MemberData member);

	public int insertSocialMember(MemberData member);

	public int checkDuplicateId(String username);

	public int deleteMember(String username);

	public int updateMemberData(MemberData memberData);

	public int updateRefreshToken(String id, String refreshToken);

	public String selectRefreshToken(String id);

}

package com.nayuta.mosite.member.model.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.nayuta.mosite.member.model.vo.Member;

public class UserLoginSuccessHandler implements AuthenticationSuccessHandler {

	private Logger log = LogManager.getLogger(UserLoginSuccessHandler.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication authentication)
			throws IOException, ServletException {

		HttpSession session = req.getSession();

		Member member = (Member) authentication.getPrincipal();

		session.setAttribute("memberNo", member.getMember_no());

		session.setAttribute("loginTime", System.currentTimeMillis());

		log.info(member.getMember_no() + " 로그인");
		String msg = authentication.getName() + "님 환영합니다.";

		if (req.isUserInRole("ROLE_ADMIN")) {
			res.sendRedirect(req.getContextPath() + "/admin/adminMain.do");
		} else {
			req.setAttribute("msg", msg);
			res.sendRedirect(req.getContextPath() + "/");
		}

	}

}

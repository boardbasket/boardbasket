package com.nayuta.mosite.member.model.vo;

public class MemberData {
	private String member_id;
	private String member_pwd;
	private String member_rePwd;
	private String member_no;
	private String member_email;
	private String member_nickname;
	private String out_flag;
	private String social_login_provider;
	private String authority;
	private String enabled;

	public MemberData(String member_id, String member_pwd, String member_rePwd, String member_no, String member_email,
			String member_nickname, String out_flag) {
		super();
		this.member_id = member_id;
		this.member_pwd = member_pwd;
		this.member_rePwd = member_rePwd;
		this.member_no = member_no;
		this.member_email = member_email;
		this.member_nickname = member_nickname;
		this.out_flag = out_flag;
	}

	public MemberData(String member_id, String social_login_provider, String member_nickname) {
		super();
		this.member_id = member_id;
		this.member_nickname = member_nickname;
		this.social_login_provider = social_login_provider;
	}

	public MemberData() {
		super();
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getMember_pwd() {
		return member_pwd;
	}

	public void setMember_pwd(String member_pwd) {
		this.member_pwd = member_pwd;
	}

	public String getMember_rePwd() {
		return member_rePwd;
	}

	public void setMember_rePwd(String member_rePwd) {
		this.member_rePwd = member_rePwd;
	}

	public String getMember_no() {
		return member_no;
	}

	public void setMember_no(String member_no) {
		this.member_no = member_no;
	}

	public String getMember_email() {
		return member_email;
	}

	public void setMember_email(String member_email) {
		this.member_email = member_email;
	}

	public String getMember_nickname() {
		return member_nickname;
	}

	public void setMember_nickname(String member_nickname) {
		this.member_nickname = member_nickname;
	}

	public String getOut_flag() {
		return out_flag;
	}

	public void setOut_flag(String out_flag) {
		this.out_flag = out_flag;
	}

	public String getSocial_login_provider() {
		return social_login_provider;
	}

	public void setSocial_login_provider(String social_login_provider) {
		this.social_login_provider = social_login_provider;
	}

	public String getAuthority() {
		return authority;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authority == null) ? 0 : authority.hashCode());
		result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
		result = prime * result + ((member_email == null) ? 0 : member_email.hashCode());
		result = prime * result + ((member_id == null) ? 0 : member_id.hashCode());
		result = prime * result + ((member_nickname == null) ? 0 : member_nickname.hashCode());
		result = prime * result + ((member_no == null) ? 0 : member_no.hashCode());
		result = prime * result + ((member_pwd == null) ? 0 : member_pwd.hashCode());
		result = prime * result + ((member_rePwd == null) ? 0 : member_rePwd.hashCode());
		result = prime * result + ((out_flag == null) ? 0 : out_flag.hashCode());
		result = prime * result + ((social_login_provider == null) ? 0 : social_login_provider.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberData other = (MemberData) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (enabled == null) {
			if (other.enabled != null)
				return false;
		} else if (!enabled.equals(other.enabled))
			return false;
		if (member_email == null) {
			if (other.member_email != null)
				return false;
		} else if (!member_email.equals(other.member_email))
			return false;
		if (member_id == null) {
			if (other.member_id != null)
				return false;
		} else if (!member_id.equals(other.member_id))
			return false;
		if (member_nickname == null) {
			if (other.member_nickname != null)
				return false;
		} else if (!member_nickname.equals(other.member_nickname))
			return false;
		if (member_no == null) {
			if (other.member_no != null)
				return false;
		} else if (!member_no.equals(other.member_no))
			return false;
		if (member_pwd == null) {
			if (other.member_pwd != null)
				return false;
		} else if (!member_pwd.equals(other.member_pwd))
			return false;
		if (member_rePwd == null) {
			if (other.member_rePwd != null)
				return false;
		} else if (!member_rePwd.equals(other.member_rePwd))
			return false;
		if (out_flag == null) {
			if (other.out_flag != null)
				return false;
		} else if (!out_flag.equals(other.out_flag))
			return false;
		if (social_login_provider == null) {
			if (other.social_login_provider != null)
				return false;
		} else if (!social_login_provider.equals(other.social_login_provider))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MemberData [member_id=" + member_id + ", member_pwd=" + member_pwd + ", member_rePwd=" + member_rePwd
				+ ", member_no=" + member_no + ", member_email=" + member_email + ", member_nickname=" + member_nickname
				+ ", out_flag=" + out_flag + ", social_login_provider=" + social_login_provider + ", authority="
				+ authority + ", enabled=" + enabled + "]";
	}

}

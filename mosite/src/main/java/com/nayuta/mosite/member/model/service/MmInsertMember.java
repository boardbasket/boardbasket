package com.nayuta.mosite.member.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;

import com.nayuta.mosite.content.model.dao.BoardDao;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.vo.MemberData;

@Service("mmInsertMember")
public class MmInsertMember extends AbstractMemberCls implements CommandInterface {

	@Autowired
	private BoardDao bDao;
	/*
	 * @Autowired private RefCodeManager rcMgr;
	 */
	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Value("#{const['const.randomTabNum']}")
	private String randomTabNum;

	@Value("#{serverConst['server_const.social_member_pwd']}")
	private String socialMemberPwd;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		MemberData member = (MemberData) modelMap.get("memberData");

		member.setMember_nickname(member.getMember_id());
		// 비밀번호가 없는 경우(소셜계정)
		if (StringUtils.isEmpty(member.getMember_pwd()))
			member.setMember_pwd(passwordEncoder.encode(socialMemberPwd));

		mDao.insertMember(member);

	}

}

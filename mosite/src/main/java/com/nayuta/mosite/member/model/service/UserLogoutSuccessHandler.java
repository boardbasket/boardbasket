package com.nayuta.mosite.member.model.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.nayuta.mosite.member.model.vo.Member;

@Component("userLogoutSucessHandler")
public class UserLogoutSuccessHandler implements LogoutSuccessHandler {

	private Logger logger = LogManager.getLogger();

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		request.getSession().invalidate();
		request.getSession().setAttribute("logOutTime", System.currentTimeMillis());

		try {
			Member member = (Member) auth.getDetails();
			logger.info("유저 " + member.getMember_no() + " 로그아웃");
		} catch (Exception e) {
			// TODO: handle exception
		}

		response.sendRedirect(request.getContextPath() + "/user/login.do");
	}

}

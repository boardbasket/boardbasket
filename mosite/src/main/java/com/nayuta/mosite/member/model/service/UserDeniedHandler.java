package com.nayuta.mosite.member.model.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

public class UserDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest req, HttpServletResponse res, AccessDeniedException accessDeniedException)
			throws IOException, ServletException {

		String ajaxHeader = req.getHeader("AJAX");
		String result = "";

		res.setStatus(HttpServletResponse.SC_FORBIDDEN);
		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		
		if (ajaxHeader == null) { 
			req.setAttribute("접근이 거부되었습니다.", accessDeniedException);
			req.getRequestDispatcher("/WEB-INF/views/user/denied.jsp").forward(req, res);
		} else {
			if ("true".equals(ajaxHeader)) { // true로 값을 받았다는 것은 ajax로 접근했음을 의미한다
				result = "{\"result\" : {\"status\": -1, \"msg\" : \"" + "로그인 해주세요" + "\"}}";
			} else { // 헤더 변수는 있으나 값이 틀린 경우이므로 헤더값이 틀렸다는 의미로 돌려준다
				result = "{\"result\" : {\"status\": -1, \"msg\" : \"" + "Header Value Mismatch" + "\"}}";
			}
			res.getWriter().print(result);
			res.getWriter().flush();
			return;
		}

	}
}

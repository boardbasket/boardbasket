package com.nayuta.mosite.member.model.service;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.vo.MemberData;

import net.sf.json.JSONObject;

@Service("mmCheckDuplicateId")
public class MmCheckDuplicateId extends AbstractMemberCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {

		int status = -1;
		HttpSession session = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()
				.getSession();
		String checkedId = null;

		MemberData member = (MemberData) modelMap.get("memberData");

		if (mDao.checkDuplicateId(member.getMember_id()) <= 0) {
			checkedId = member.getMember_id();
			status = 1;
		}

		resultObj = new JSONObject();
		resultObj.put("status", status);
		modelMap.addAttribute("result", resultObj);
		session.setAttribute("checkedId", checkedId);

	}

}

package com.nayuta.mosite.member.model.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.member.model.vo.Member;

@Service("mmDeleteMember")
public class MmDeleteMember extends AbstractMemberCls implements CommandInterface {
	private Logger logger = LogManager.getLogger();

	@Override
	public void execute(ModelMap modelMap) {

		Member member = (Member) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		resultObj.put("status", mDao.deleteMember(member.getUsername()));

		logger.info("유저 " + member.getMember_no() + " 탈퇴");

		modelMap.addAttribute("result", resultObj);

	}

}

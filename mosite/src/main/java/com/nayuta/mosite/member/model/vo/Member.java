package com.nayuta.mosite.member.model.vo;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class Member extends User implements java.io.Serializable {

	private static final long serialVersionUID = -3829622617517655734L;

	private String member_no;
	private String provider;
	private long loginTime;

	public Member(String username, String password, boolean enabled, String authority, String member_no,
			String provider) {

		super(username, password, enabled, true, true, true,
				new ArrayList<GrantedAuthority>(Arrays.asList(new SimpleGrantedAuthority(authority))));

		this.member_no = String.valueOf(member_no);
		this.loginTime = System.currentTimeMillis();
		this.provider = provider;

	}

	public String getMember_no() {
		return member_no;
	}

	public void setMember_no(String member_no) {
		this.member_no = member_no;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}

	@Override
	public String toString() {
		return "Member [member_no=" + member_no + "]";
	}

}

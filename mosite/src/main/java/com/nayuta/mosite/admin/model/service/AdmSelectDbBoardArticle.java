package com.nayuta.mosite.admin.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Service("admSelectDbBoardArticle")
public class AdmSelectDbBoardArticle extends AbstractAdminCls implements CommandInterface {

	@Autowired
	protected BoardDaoImpl bDao;

	@Value("#{const['const.AdminDbArticleLoadNum']}")
	protected String dbArticleLoadNum;

	@Override
	public void execute(ModelMap modelMap) {

		TableSearchData tsd = (TableSearchData) modelMap.get("searchData");

		int articleCnt = aDao.selectDBBoardArticleCnt(tsd);
		PageInfo pageInfo = new PageInfo(tsd.getOffset(), articleCnt, Integer.parseInt(dbArticleLoadNum));
		List<BoardArticle> baList = aDao.selectDBBoardArticle(tsd, pageInfo);

		resultObj.put("dataList", baList);
		resultObj.put("pageInfo", pageInfo);

		modelMap.addAttribute("result", resultObj);

	}

}

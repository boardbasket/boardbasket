package com.nayuta.mosite.admin.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nayuta.mosite.admin.model.service.AdmDeleteBoard;
import com.nayuta.mosite.admin.model.service.AdmDeleteDbBoardArticle;
import com.nayuta.mosite.admin.model.service.AdmDeleteFiles;
import com.nayuta.mosite.admin.model.service.AdmDeleteSite;
import com.nayuta.mosite.admin.model.service.AdmInsertBoard;
import com.nayuta.mosite.admin.model.service.AdmInsertDbBoardArticle;
import com.nayuta.mosite.admin.model.service.AdmInsertFiles;
import com.nayuta.mosite.admin.model.service.AdmInsertSite;
import com.nayuta.mosite.admin.model.service.AdmSelectBoard;
import com.nayuta.mosite.admin.model.service.AdmSelectDbBoardArticle;
import com.nayuta.mosite.admin.model.service.AdmSelectDbBoardList;
import com.nayuta.mosite.admin.model.service.AdmSelectSite;
import com.nayuta.mosite.admin.model.service.AdmUpdateBoard;
import com.nayuta.mosite.admin.model.service.AdmUpdateDbBoardArticle;
import com.nayuta.mosite.admin.model.service.AdmUpdateSite;
import com.nayuta.mosite.admin.model.service.AdmUpsertParsedSite;
import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.admin.model.vo.BoardList;
import com.nayuta.mosite.admin.model.vo.FileVOList;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.admin.model.vo.SiteList;
import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.common.util.FileUtil.FILE_TYPE;
import com.nayuta.mosite.content.model.service.CommandContainer;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Controller
@RequestMapping("/admin")
public class adminController {

	@Autowired
	@Qualifier("admSelectBoard")
	private AdmSelectBoard admSelectBoard;

	@Autowired
	@Qualifier("admSelectSite")
	private AdmSelectSite admSelectSite;

	@Autowired
	@Qualifier("admInsertBoard")
	private AdmInsertBoard admInsertBoard;

	@Autowired
	@Qualifier("admUpdateBoard")
	private AdmUpdateBoard admUpdateBoard;

	@Autowired
	@Qualifier("admDeleteBoard")
	private AdmDeleteBoard admDeleteBoard;

	@Autowired
	@Qualifier("admInsertSite")
	private AdmInsertSite admInsertSite;

	@Autowired
	@Qualifier("admInsertFiles")
	private AdmInsertFiles admInsertFiles;

	@Autowired
	@Qualifier("admDeleteFiles")
	private AdmDeleteFiles admDeleteFiles;

	@Autowired
	@Qualifier("admDeleteSite")
	private AdmDeleteSite admDeleteSite;

	@Autowired
	@Qualifier("admUpdateSite")
	private AdmUpdateSite admUpdateSite;

	@Autowired
	@Qualifier("admUpsertParsedSite")
	private AdmUpsertParsedSite admUpsertParsedSite;

	@Autowired
	@Qualifier("admSelectDbBoardArticle")
	private AdmSelectDbBoardArticle admSelectDbBoardArticle;

	@Autowired
	@Qualifier("admSelectDbBoardList")
	private AdmSelectDbBoardList admSelectDbBoardList;

	@Autowired
	@Qualifier("admInsertDbBoardArticle")
	private AdmInsertDbBoardArticle admInsertDbBoardArticle;

	@Autowired
	@Qualifier("admUpdateDbBoardArticle")
	private AdmUpdateDbBoardArticle admUpdateDbBoardArticle;

	@Autowired
	@Qualifier("admDeleteDbBoardArticle")
	private AdmDeleteDbBoardArticle admDeleteDbBoardArticle;

	@Autowired
	private CommandContainer cController;

	private Map<String, Object> result = null;

	@RequestMapping(value = { "/adminMain.do" }, method = RequestMethod.GET)
	public String adminMain(ModelMap modelMap) {

		//cController.execute(admSelectDbBoardList, modelMap);

		return "admin/newAdminMain";
	}

	@RequestMapping(value = { "/selectBoard.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectBoard(ModelMap modelMap,
			@ModelAttribute("searchData") TableSearchData searchData) {

		cController.execute(admSelectBoard, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/selectSite.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectAllSite(ModelMap modelMap,
			@ModelAttribute("searchData") TableSearchData searchData) {

		cController.execute(admSelectSite, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/parseSite.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> parseSite(ModelMap modelMap, @ModelAttribute("site") Site site) {

		cController.execute(admUpsertParsedSite, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/deleteSite.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteSite(ModelMap modelMap, @RequestBody SiteList dataList) {

		modelMap.put("siteList", dataList.getDataList());
		cController.execute(admDeleteSite, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/updateSite.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> updateSite(ModelMap modelMap, @RequestBody SiteList dataList) {

		modelMap.put("dataList", dataList.getDataList());
		cController.execute(admUpdateSite, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/insertBoard.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertBoard(ModelMap modelMap, @ModelAttribute("insertData") Board board) {

		cController.execute(admInsertBoard, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/updateBoard.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertBoard(ModelMap modelMap, @RequestBody BoardList dataList) {

		modelMap.put("dataList", dataList.getDataList());

		cController.execute(admUpdateBoard, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/deleteBoard.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> deleteBoard(ModelMap modelMap, @RequestBody BoardList dataList) {

		modelMap.put("dataList", dataList.getDataList());

		cController.execute(admDeleteBoard, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/insertSite.ajax" }, method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> insertSite(ModelMap modelMap, Site site, HttpServletRequest request) {

		modelMap.put("site", site);
		modelMap.put("request", request);
		cController.execute(admInsertSite, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/insertFiles.ajax" }, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> insertFiles(ModelMap modelMap, MultipartHttpServletRequest mpReq,
			@RequestParam(value = "fileType", required = false) FILE_TYPE fileType) {

		modelMap.put("mpReq", mpReq);
		modelMap.put("fileType", fileType);
		cController.execute(admInsertFiles, modelMap);

		result = new HashMap<String, Object>();
		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/deleteFiles.ajax" }, method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> deleteFiles(ModelMap modelMap, @RequestBody FileVOList dataList) {

		modelMap.put("dataList", dataList.getDataList());

		cController.execute(admDeleteFiles, modelMap);

		result = new HashMap<String, Object>();
		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/selectDbBoardArticle.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectNoticeBoard(ModelMap modelMap, @ModelAttribute("searchData") TableSearchData tsd) {

		cController.execute(admSelectDbBoardArticle, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@RequestMapping(value = { "/selectDbBoardList.ajax" }, method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectDbArticleBoardList(ModelMap modelMap) {

		cController.execute(admSelectDbBoardList, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@PostMapping(value = { "/insertDbBoardArticle.ajax" })
	@ResponseBody
	public Map<String, Object> insertDbBoardArticle(ModelMap modelMap, @ModelAttribute("insertData") BoardArticle ba,
			@ModelAttribute("board_code") String board_code) {

		cController.execute(admInsertDbBoardArticle, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@PostMapping(value = { "/updateDbBoardArticle.ajax" })
	@ResponseBody
	public Map<String, Object> updateDbBoardArticle(ModelMap modelMap, @ModelAttribute("insertData") BoardArticle ba) {

		cController.execute(admUpdateDbBoardArticle, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

	@PostMapping(value = { "/deleteDbBoardArticle.ajax" })
	@ResponseBody
	public Map<String, Object> deleteDbBoardArticle(ModelMap modelMap, @ModelAttribute("insertData") BoardArticle ba) {

		cController.execute(admDeleteDbBoardArticle, modelMap);

		result = new HashMap<String, Object>();

		result.put("result", modelMap.get("result"));
		return result;
	}

}

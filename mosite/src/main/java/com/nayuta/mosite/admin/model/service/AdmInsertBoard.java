package com.nayuta.mosite.admin.model.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admInsertBoard")
public class AdmInsertBoard extends AbstractAdminCls
		implements
			CommandInterface {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		Board board = (Board) modelMap.get("insertData");

		resultObj.put("status", aDao.insertBoard(board));

		modelMap.put("result", resultObj);

	}

}

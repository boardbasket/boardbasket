package com.nayuta.mosite.admin.model.vo;

import java.util.ArrayList;
import java.util.List;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.vo.SearchType;

public class Site extends GetPrimaryKeyCodeCls {

	private static final long serialVersionUID = 1L;
	private String site_name;
	private String site_address;
	private String favicon_address;
	private String available;
	private String site_color;
	private int parse_thread;
	private String search_available;
	private List<SearchType> searchType_list;
	private List<String> board_type_list = new ArrayList<>();

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public String getSite_address() {
		return site_address;
	}

	public void setSite_address(String site_address) {
		// 사이트 이름 끝에 /가 있으면 빼줌 (파싱할때 오류를 막기위해)
		this.site_address = site_address.charAt(site_address.length() - 1) != '/' ? site_address
				: site_address.substring(0, site_address.length() - 1);
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public List<SearchType> getSearchType_list() {
		return searchType_list;
	}

	public void setSearchType_list(List<SearchType> searchType_list) {
		this.searchType_list = searchType_list;
	}

	public List<String> getBoard_type_list() {
		return board_type_list;
	}

	public void setBoard_type_list(List<String> board_type_list) {
		this.board_type_list = board_type_list;
	}

	public String getSite_color() {
		return site_color;
	}

	public void setSite_color(String site_color) {
		this.site_color = site_color;
	}

	public String getFavicon_address() {
		return favicon_address;
	}

	public void setFavicon_address(String favicon_address) {
		this.favicon_address = favicon_address;
	}

	public String getSearch_available() {
		return search_available;
	}

	public void setSearch_available(String search_available) {
		this.search_available = search_available;
	}

	public Site(String site_code, String site_name, String site_address, String available, List<SearchType> searchType_list) {
		super();
		this.site_code = site_code;
		this.site_name = site_name;
		this.site_address = site_address;
		this.available = available;
		this.searchType_list = searchType_list;
	}

	public Site(String site_name, String site_address, String favicon_address, String available, String site_color,
			List<SearchType> searchType_list, List<String> board_type_list) {
		super();
		this.site_name = site_name;
		this.site_address = site_address;
		this.favicon_address = favicon_address;
		this.available = available;
		this.site_color = site_color;
		this.searchType_list = searchType_list;
		this.board_type_list = board_type_list;
	}

	public int getParse_thread() {
		return parse_thread;
	}

	public void setParse_thread(int parse_thread) {
		this.parse_thread = parse_thread;
	}

	@Override
	public String toString() {
		return "Site [site_name=" + site_name + ", site_address=" + site_address + ", favicon_address=" + favicon_address
				+ ", available=" + available + ", site_color=" + site_color + ", searchType_list=" + searchType_list
				+ ", board_type_list=" + board_type_list + "]";
	}

	public Site() {
		super();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((board_type_list == null) ? 0 : board_type_list.hashCode());
		result = prime * result + ((favicon_address == null) ? 0 : favicon_address.hashCode());
		result = prime * result + ((searchType_list == null) ? 0 : searchType_list.hashCode());
		result = prime * result + ((site_address == null) ? 0 : site_address.hashCode());
		result = prime * result + ((site_color == null) ? 0 : site_color.hashCode());
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Site other = (Site) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (board_type_list == null) {
			if (other.board_type_list != null)
				return false;
		} else if (!board_type_list.equals(other.board_type_list))
			return false;
		if (favicon_address == null) {
			if (other.favicon_address != null)
				return false;
		} else if (!favicon_address.equals(other.favicon_address))
			return false;
		if (searchType_list == null) {
			if (other.searchType_list != null)
				return false;
		} else if (!searchType_list.equals(other.searchType_list))
			return false;
		if (site_address == null) {
			if (other.site_address != null)
				return false;
		} else if (!site_address.equals(other.site_address))
			return false;
		if (site_color == null) {
			if (other.site_color != null)
				return false;
		} else if (!site_color.equals(other.site_color))
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		return true;
	}

}

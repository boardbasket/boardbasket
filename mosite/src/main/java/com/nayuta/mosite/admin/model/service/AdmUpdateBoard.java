package com.nayuta.mosite.admin.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admUpdateBoard")
public class AdmUpdateBoard extends AbstractAdminCls
		implements
			CommandInterface {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		@SuppressWarnings("unchecked")
		List<Board> updateList = (ArrayList<Board>) modelMap.get("dataList");

		for (Board b : updateList)
			aDao.updateBoard(b);

		resultObj.put("updatedLength", updateList.size());
		modelMap.addAttribute("result", resultObj);

	}

}

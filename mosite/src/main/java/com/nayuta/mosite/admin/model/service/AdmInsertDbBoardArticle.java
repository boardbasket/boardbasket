package com.nayuta.mosite.admin.model.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.common.exceptions.MVCServiceException;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Service("admInsertDbBoardArticle")
public class AdmInsertDbBoardArticle extends AbstractAdminCls implements CommandInterface {

	@Value("#{patterns['patterns.dbBoardTitle']}")
	private String dbBoardTitle;

	@Value("#{patterns['patterns.dbBoardContent']}")
	private String dbBoardContent;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		BoardArticle ba = (BoardArticle) modelMap.get("insertData");
		String boardCode = (String) modelMap.get("board_code");
		boolean title = false;
		boolean content = false;

		Pattern titlePattern = Pattern.compile(dbBoardTitle);
		Pattern contentPattern = Pattern.compile(dbBoardContent);
		Matcher titleMatcher = titlePattern.matcher(ba.getTitle());
		Matcher contentMatcher = contentPattern.matcher(ba.getContent());

		if (titleMatcher.find())
			title = true;

		if (contentMatcher.find())
			content = true;

		ba.getContent().replaceAll("/\r\n?|\n/g", "<br/>");
		
		if (title && content)
			aDao.insertDbBoardArticle(ba, boardCode, getMemberNo());
		else
			throw new MVCServiceException("글 형식이 올바르지 않습니다.");

		modelMap.put("result", resultObj);

	}

}

package com.nayuta.mosite.admin.model.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admSelectDbBoardList")
public class AdmSelectDbBoardList extends AbstractAdminCls implements CommandInterface {

	//db에 저장된 자체 게시판의 종류를 가져옴
	
	//private Log log = LogFactory.getLog(AdmSelectDbBoardList.class);


	@Override
	public void execute(ModelMap modelMap) {

		resultObj.put("boardList",aDao.selectDbBoardList());

		modelMap.addAttribute("result",resultObj);
	}

	
}

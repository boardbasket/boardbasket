package com.nayuta.mosite.admin.model.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Service("admUpdateDbBoardArticle")
public class AdmUpdateDbBoardArticle extends AbstractAdminCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {

		BoardArticle ba = (BoardArticle) modelMap.get("insertData");

		aDao.updateDbBoardArticle(ba);
		
		modelMap.addAttribute("result", resultObj);

	}

}

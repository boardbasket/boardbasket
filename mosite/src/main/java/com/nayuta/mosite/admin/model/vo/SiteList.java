package com.nayuta.mosite.admin.model.vo;

import java.util.ArrayList;
import java.util.List;

public class SiteList {

	List<Site> dataList = new ArrayList<>();

	public List<Site> getDataList() {
		return dataList;
	}

	public void setDataList(List<Site> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "SiteList [dataList=" + dataList + "]";
	}

	public SiteList() {
		super();
	}

	public SiteList(List<Site> dataList) {
		super();
		this.dataList = dataList;
	}

}

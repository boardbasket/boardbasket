package com.nayuta.mosite.admin.model.vo;

import java.util.List;

import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCodeCls;
import com.nayuta.mosite.content.model.vo.BoardCategory;

//게시판을 넣거나 뺄때 사용하는 VO, ContentVO로 대체 가능
public class Board extends GetPrimaryKeyCodeCls {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String site_name;

	// private String board_code;
	// private String site_code;
	private String board_name;
	private String board_address;
	private String board_type;
	private String available;
	private String db_board;
	private List<BoardCategory> board_category_list;

	public String getDb_board() {
		return db_board;
	}

	public void setDb_board(String db_board) {
		this.db_board = db_board;
	}

	public List<BoardCategory> getBoard_category_list() {
		return board_category_list;
	}

	public void setBoard_category_list(List<BoardCategory> board_category_list) {
		this.board_category_list = board_category_list;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public void setBoard_code(String board_code) {
		super.board_code = board_code;
		if (board_category_list != null)
			for (BoardCategory bc : board_category_list) {
				bc.setBoard_code(board_code);
			}

	}

	public String getBoard_name() {

		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public String getBoard_address() {
		return board_address;
	}

	public void setBoard_address(String board_address) {
		this.board_address = board_address;
	}

	public String getBoard_type() {
		return board_type;
	}

	public void setBoard_type(String board_type) {
		this.board_type = board_type;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public Board() {
		super();
	}

	public Board(String siteCode, String boardName, String boardAddress) {
		this.board_name = boardName;
		this.board_address = boardAddress;
		super.site_code = siteCode;
	}

	public Board(String siteCode, String boardName, String boardAddress, String available) {
		super.site_code = siteCode;
		this.board_name = boardName;
		this.board_address = boardAddress;
		this.available = available;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((board_address == null) ? 0 : board_address.hashCode());
		result = prime * result + ((board_category_list == null) ? 0 : board_category_list.hashCode());
		result = prime * result + ((board_name == null) ? 0 : board_name.hashCode());
		result = prime * result + ((board_type == null) ? 0 : board_type.hashCode());
		result = prime * result + ((db_board == null) ? 0 : db_board.hashCode());
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (board_address == null) {
			if (other.board_address != null)
				return false;
		} else if (!board_address.equals(other.board_address))
			return false;
		if (board_category_list == null) {
			if (other.board_category_list != null)
				return false;
		} else if (!board_category_list.equals(other.board_category_list))
			return false;
		if (board_name == null) {
			if (other.board_name != null)
				return false;
		} else if (!board_name.equals(other.board_name))
			return false;
		if (board_type == null) {
			if (other.board_type != null)
				return false;
		} else if (!board_type.equals(other.board_type))
			return false;
		if (db_board == null) {
			if (other.db_board != null)
				return false;
		} else if (!db_board.equals(other.db_board))
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		return true;
	}

}

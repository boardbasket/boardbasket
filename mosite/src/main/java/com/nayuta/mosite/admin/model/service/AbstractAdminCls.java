package com.nayuta.mosite.admin.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.gson.Gson;
import com.nayuta.mosite.admin.model.dao.AdminDaoImpl;
import com.nayuta.mosite.member.model.vo.Member;

import net.sf.json.JSONObject;

@Component
public abstract class AbstractAdminCls {

	@Autowired
	protected AdminDaoImpl aDao;
	@JsonInclude(Include.NON_NULL)
	protected JSONObject resultObj = new JSONObject();
	protected Gson gson = new Gson();

	public JSONObject getResultObj() {
		return resultObj;
	}

	public void setResultObj(JSONObject resultObj) {
		this.resultObj = resultObj;
	}

	public String getMemberNo() {
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (obj instanceof UserDetails)
			return ((Member) obj).getMember_no();
		else
			return null;

	}

}

package com.nayuta.mosite.admin.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admDeleteSite")
public class AdmDeleteSite extends AbstractAdminCls implements CommandInterface {

	@Autowired
	protected BoardDaoImpl bDao;

	@Override
	public void execute(ModelMap modelMap) {

		@SuppressWarnings("unchecked")
		List<Site> siteList = (List<Site>) modelMap.get("siteList");

		// saveParamChanges();
		String deletedSite = "";
		for (Site site : siteList) {
			// aDao.deleteSite(site);
			deletedSite += site.getSite_name() + "  ";
		}

		resultObj.put("deletedSite", deletedSite + "가 삭제 되었습니다(삭제 X)");

		modelMap.addAttribute("result", resultObj);

	}

}

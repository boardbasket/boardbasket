package com.nayuta.mosite.admin.model.vo;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.nayuta.mosite.common.util.FileUtil.FILE_TYPE;

@JsonInclude(Include.NON_NULL)
public class FileVO {

	private String file_id;
	private String original_file_name;
	private String stored_file_name;
	private long file_size;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Date upload_date = null;
	private String file_path;
	private String del_flag;
	private String file_type;

	public String getFile_path() {
		return file_path;
	}

	public void setFile_full_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public void setFile_size(long file_size) {
		this.file_size = file_size;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getOriginal_file_name() {
		return original_file_name;
	}

	public void setOriginal_file_name(String original_file_name) {
		this.original_file_name = original_file_name;
	}

	public String getStored_file_name() {
		return stored_file_name;
	}

	public void setStored_file_name(String stored_file_name) {
		this.stored_file_name = stored_file_name;
	}

	public Long getFile_size() {
		return file_size;
	}

	public void setFile_size(Long file_size) {
		this.file_size = file_size;
	}

	public Date getUpload_date() {
		return upload_date;
	}

	public void setUpload_date(Date upload_date) {
		this.upload_date = upload_date;
	}

	public String getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}

	@Override
	public String toString() {
		return "FileVO [file_id=" + file_id + ", original_file_name=" + original_file_name + ", stored_file_name="
				+ stored_file_name + ", file_size=" + file_size + ", upload_date=" + upload_date + ", file_path="
				+ file_path + ", del_flag=" + del_flag + ", file_type=" + file_type + "]";
	}

	public FileVO(String file_id, String original_file_name, String stored_file_name, String file_path,
			FILE_TYPE file_type, Long file_size) {
		super();
		this.file_id = file_id;
		this.original_file_name = original_file_name;
		this.stored_file_name = stored_file_name;
		this.file_size = file_size;
		this.file_type = file_type.toString();
		this.file_path = file_path;
	}

	public FileVO() {
		super();
	}

}

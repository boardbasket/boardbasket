package com.nayuta.mosite.admin.model.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.DefaultPropertiesPersister;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.SearchType;

@Service("admInsertSite")
public class AdmInsertSite extends AbstractAdminCls implements CommandInterface {

	private Logger log = LogManager.getLogger();

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		Site site = (Site) modelMap.get("site");

		// insert site data

		aDao.insertSite(site);
		List<SearchType> typeList = site.getSearchType_list();

		for (SearchType st : typeList) {
			st.setSite_code(site.getSite_code());
			st.setSite_name(site.getSite_name());
			aDao.insertSearchType(st);
		}
		log.debug("site " + site + "is inserted");
		resultObj.put("status", 1);
		modelMap.put("result", resultObj);
	}

	public void saveParamChanges() {
		try {
			// create and set properties into properties object
			Properties props = new Properties();
			props.setProperty("Prop1", "toto");
			props.setProperty("Prop2", "test");
			props.setProperty("Prop3", "tata");
			// get or create the file
			File f = new File("app-properties.properties");
			OutputStream out = new FileOutputStream(f);
			// write into it
			DefaultPropertiesPersister p = new DefaultPropertiesPersister();
			p.store(props, out, "Header COmment");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

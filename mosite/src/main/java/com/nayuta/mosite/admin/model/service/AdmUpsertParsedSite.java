package com.nayuta.mosite.admin.model.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.util.DefaultPropertiesPersister;

import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserAbstract;
import com.nayuta.mosite.content.model.vo.ContentVO;

@Service("admUpsertParsedSite")
public class AdmUpsertParsedSite extends AbstractAdminCls implements CommandInterface {

	// private Log log = LogFactory.getLog(AdmUpsertParsedSite.class);

	@Autowired
	private BoardParserController spc;

	@Autowired
	private BoardDaoImpl bDao;

	@Override
	public void execute(ModelMap modelMap) {

		// Site site = (Site) modelMap.get("site");

		// List<Site> siteList = aDao.selectSite(new TableSearchData());

		// BoardParserAbstract spa = spc.getBoardParser(siteList.get(0));
		// spa.setSite(siteList.get(0));
		// List<Board> boardList = spa.parseSite(siteList.get(0).getSite_address());

		// makeFile("C:\\mosite\\", "ruliwebBoards.txt", boardList);

		/*
		 * List<Board> storedBoard = getFileToList("C:\\mosite\\", "ruliwebBoards.txt");
		 * 
		 * Pattern pattern = Pattern.compile("^[^|]+");
		 * 
		 * for (Board b : storedBoard) { Matcher matcher =
		 * pattern.matcher(b.getBoard_name()); if (matcher.find()) {
		 * b.setBoard_name(matcher.group()); } }
		 * aDao.insertOrMergeParsedBoard(storedBoard);
		 * aDao.insertParsedBoardCategory(storedBoard);
		 */

		File file = new File("C:\\mosite\\boardList\\error.txt");
		// List<ContentVO> boardList = new ArrayList<>();
		int errorCount = 0;
		int parseCount = 0;
		try {
			PrintWriter pw = new PrintWriter(
					new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8")));

			List<String> test = getFileToStringArray("C:\\mosite\\boardList\\", "dcminorPopular.txt");

			BoardParserAbstract bp = spc.getParser(test.get(0));

			for (int i = 1; i < test.size(); ++i) {
				try {

					String address = bp.urlPreProcess(test.get(i));
					ContentVO exist = bDao.selectIsBoardExist(address);
					if (exist != null)
						continue;
					else {
						// pw.println(test.get(i) + ",");

						ContentVO board = bp.parseBoard(address);

						bDao.insertBoard(board);
						parseCount++;
					}

				} catch (Exception e) {
					System.out.println(e.getMessage());
					errorCount++;
					pw.println(test.get(i) + ",");
				}

			}

			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		resultObj.put("errorCount", errorCount);
		resultObj.put("parseCount", parseCount);
		// resultObj.put("boardList", boardList);
		modelMap.addAttribute("result", resultObj);
	}

	public void makeFile(String filePath, String fileName, Map<String, String> dataList) {
		try {
			// create and set properties into properties object
			Properties props = new Properties();

			for (Map.Entry<String, String> entry : dataList.entrySet()) {

				props.put(entry.getKey(), entry.getValue());
			}

			// get or create the file
			File f = new File(filePath + fileName);
			OutputStream out = new FileOutputStream(f);
			// write into it
			DefaultPropertiesPersister p = new DefaultPropertiesPersister();
			p.store(props, out, "Header Comment");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void makeFile(String filePath, String fileName, List<Board> dataList) {

		try {
			// create and set properties into properties object
			Properties props = new Properties();

			for (Board b : dataList) {
				props.put(b.getBoard_name(), gson.toJson(b));
			}

			// get or create the file
			File f = new File(filePath + fileName);
			OutputStream out = new FileOutputStream(f);
			// write into it
			DefaultPropertiesPersister p = new DefaultPropertiesPersister();
			p.store(props, out, "Header Comment");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Map<String, String> getFileToMap(String filePath, String fileName) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			Properties props = new Properties();
			FileInputStream fis = new FileInputStream(filePath + fileName);

			props.load(new java.io.BufferedInputStream(fis));

			for (Entry<Object, Object> entry : props.entrySet()) {
				result.put(entry.getKey().toString(), entry.getValue().toString());
			}

			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<String> getFileToStringArray(String filePath, String fileName) {

		List<String> array = new ArrayList<>();
		String content = "";
		try {

			content = new String(Files.readAllBytes(Paths.get(filePath + fileName)));
			content = content.replaceAll("\\r\\n", "");

			String[] addrArray = content.split(",");

			array = Arrays.asList(addrArray);

		} catch (IOException e) {
			
			e.printStackTrace();
		}

		return array;

	}

	public List<Board> getFileToList(String filePath, String fileName) {
		List<Board> result = new ArrayList<>();
		try {
			Properties props = new Properties();
			FileInputStream fis = new FileInputStream(filePath + fileName);

			props.load(new java.io.BufferedInputStream(fis));

			for (Entry<Object, Object> entry : props.entrySet()) {

				result.add(gson.fromJson(entry.getValue().toString(), Board.class));

			}

			return result;
		} catch (Exception e) {

			e.printStackTrace();
		}
		return null;
	}

	public void makeFileTest() {
		List<Site> siteList = aDao.selectSite(new TableSearchData());

		// BoardParserAbstract spa = spc.getBoardParser(siteList.get(0));

		// spa.setSite(siteList.get(0));
		Map<String, String> mainMenu = getFileToMap("C:\\mosite\\", "ruliwebMainMenu.txt");
		// spa.parseMainMenu();
		/// makeFile("C:\\mosite\\", "ruliwebMainMenu.txt", mainMenu);

		Map<String, String> test = new HashMap<>();

		test.put("만화가 지망생", "https://bbs.ruliweb.com/family/212/board/300066");

		// Map<String, String> testResult = spa.parseSubMenu(test);

		// List<Board> testList = spa.parseBoard(test);

		Map<String, String> subMenu = getFileToMap("C:\\mosite\\", "ruliwebSubMenu.txt");
		// spa.parseSubMenu(mainMenu);
		// makeFile("C:\\mosite\\", "ruliwebSubMenu.txt", subMenu);
		Map<String, String> pagination = getFileToMap("C:\\mosite\\", "ruliwebPagenation.txt");
		// ((SiteParserRuliweb)spa).parsePagenation(mainMenu);
		// makeFile("C:\\mosite\\", "ruliwebPagenation.txt", pagenation);

		subMenu.putAll(pagination);
		List<Board> boardListWithoutFamilyBoard = getFileToList("C:\\mosite\\", "boardListWithoutFamilyBoard.txt");

		boolean exist = subMenu.containsValue("인기 만화");
		// ((SiteParserRuliweb) spa).parseBoard(subMenu);

		// makeFile("C:\\mosite\\", "boardListWithoutFamilyBoard.txt",
		// boardListWithoutFamilyBoard);

		// aDao.insertOrMergeParsedBoard(boardListWithoutFamilyBoard);// 파싱한 게시판을 집어넣기
		// aDao.insertParsedBoardCategory(boardListWithoutFamilyBoard);
		// Map<String, String> familyMenu = ((SiteParserRuliweb)
		// spa).parseFamilySubMenu(boardListWithoutFamilyBoard);

		// makeFile("C:\\mosite\\", "familyMenu.txt", familyMenu);
	}

}

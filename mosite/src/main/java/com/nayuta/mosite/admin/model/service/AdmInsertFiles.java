package com.nayuta.mosite.admin.model.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nayuta.mosite.admin.model.vo.FileVO;
import com.nayuta.mosite.common.util.FileUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admInsertFiles")
public class AdmInsertFiles extends AbstractAdminCls implements CommandInterface {

	@Autowired
	FileUtil fileUtil;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {
		FileUtil.FILE_TYPE fileType = (FileUtil.FILE_TYPE) modelMap.get("fileType");
		MultipartHttpServletRequest req = (MultipartHttpServletRequest) modelMap.get("mpReq");
		Iterator<String> iterator = req.getFileNames();
		List<FileVO> fileList = new ArrayList<>();
		while (iterator.hasNext()) {
			MultipartFile multipartFile = req.getFile(iterator.next());

			FileVO fileVO = fileUtil.insertFile(multipartFile, fileType);

			aDao.insertFile(fileVO);
			fileList.add(fileVO);
		}
		
		
		resultObj.put("uploadedFiles", gson.toJson(fileList));

		modelMap.put("result", resultObj);

	}

}

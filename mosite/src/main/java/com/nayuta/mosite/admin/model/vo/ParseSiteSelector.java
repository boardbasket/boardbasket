package com.nayuta.mosite.admin.model.vo;

//사이트의 게시판을 파싱할 selector 들을 담는 VO (admin 전용)

public class ParseSiteSelector {

	private Site site;
	private String main_menu_selector;
	private String sub_menu_selector;
	private String category_selector;
	private String pagenation_selector;
	private String pagenation_item_selector;
	private String pagenation_submit_selector;
	private String board_selector;

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getPagenation_item_selector() {
		return pagenation_item_selector;
	}

	public void setPagenation_item_selector(String pagenation_item_selector) {
		this.pagenation_item_selector = pagenation_item_selector;
	}

	public String getPagenation_submit_selector() {
		return pagenation_submit_selector;
	}

	public void setPagenation_submit_selector(String pagenation_submit_selector) {
		this.pagenation_submit_selector = pagenation_submit_selector;
	}

	public String getMain_menu_selector() {
		return main_menu_selector;
	}

	public void setMain_menu_selector(String main_menu_selector) {
		this.main_menu_selector = main_menu_selector;
	}

	public String getSub_menu_selector() {
		return sub_menu_selector;
	}

	public void setSub_menu_selector(String sub_menu_selector) {
		this.sub_menu_selector = sub_menu_selector;
	}

	public String getCategory_selector() {
		return category_selector;
	}

	public void setCategory_selector(String category_selector) {
		this.category_selector = category_selector;
	}

	public String getPagenation_selector() {
		return pagenation_selector;
	}

	public void setPagenation_selector(String pagenation_selector) {
		this.pagenation_selector = pagenation_selector;
	}

	public String getBoard_selector() {
		return board_selector;
	}

	public void setBoard_selector(String board_selector) {
		this.board_selector = board_selector;
	}

	public ParseSiteSelector(Site site, String main_menu_selector, String sub_menu_selector, String category_selector,
			String pagenation_selector, String pagenation_item_selector, String pagenation_submit_selector,
			String board_selector) {
		super();
		this.site = site;
		this.main_menu_selector = main_menu_selector;
		this.sub_menu_selector = sub_menu_selector;
		this.category_selector = category_selector;
		this.pagenation_selector = pagenation_selector;
		this.pagenation_item_selector = pagenation_item_selector;
		this.pagenation_submit_selector = pagenation_submit_selector;
		this.board_selector = board_selector;
	}

	public ParseSiteSelector() {
		super();
	}

}

package com.nayuta.mosite.admin.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.BoardParserController;
import com.nayuta.mosite.content.model.service.ContentParser.boardParser.parsers.BoardParserAbstract;

@Service("admSelectSite")
public class AdmSelectSite extends AbstractAdminCls implements CommandInterface {

	@Autowired
	protected BoardDaoImpl bDao;

	@Autowired
	protected BoardParserController bpc;

	@Override
	public void execute(ModelMap modelMap) {

		TableSearchData tsd = (TableSearchData) modelMap.get("searchData");

		List<Site> result = aDao.selectSite(tsd);

		for (Site s : result) { 
			if (s.getSite_name().equals("BOARDBASKET"))
				continue;
			BoardParserAbstract bpa = bpc.getParser(s.getSite_address());
			List<String> test = (new ArrayList<>(bpa.getBoardTypeCheck().keySet()));
		
			s.setBoard_type_list(test);
		}

		resultObj.put("dataList", result);

		modelMap.addAttribute("result", resultObj);

	}

}

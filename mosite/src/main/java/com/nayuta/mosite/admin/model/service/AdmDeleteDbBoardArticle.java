package com.nayuta.mosite.admin.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.content.model.dao.BoardDaoImpl;
import com.nayuta.mosite.content.model.service.CommandInterface;
import com.nayuta.mosite.content.model.vo.BoardArticle;

@Service("admDeleteDbBoardArticle")
public class AdmDeleteDbBoardArticle extends AbstractAdminCls implements CommandInterface {

	@Autowired
	protected BoardDaoImpl bDao;

	@Override
	public void execute(ModelMap modelMap) {

		BoardArticle ba = (BoardArticle) modelMap.get("insertData");

		aDao.deleteDbBoardArticle(ba);

		modelMap.addAttribute("result", resultObj);

	}

}

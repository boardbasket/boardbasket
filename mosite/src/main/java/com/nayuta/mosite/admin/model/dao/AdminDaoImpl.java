package com.nayuta.mosite.admin.model.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.admin.model.vo.FileVO;
import com.nayuta.mosite.admin.model.vo.ParseSiteSelector;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.SearchType;

@Repository
public class AdminDaoImpl extends AdminDao {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSession sqlSession;

	@Autowired
	@Qualifier("batchSqlSessionTemplate")
	private SqlSession batchSqlSession;

	@Override
	public List<Board> selectBoard(TableSearchData tsd) {

		return sqlSession.selectList("admin.selectBoard", tsd);
	}

	@Override
	public int insertBoard(Board board) {

		return sqlSession.insert("admin.insertBoard", board);
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void insertOrMergeParsedBoard(List<Board> boardList) {

		for (Board b : boardList)
			batchSqlSession.insert("admin.insertOrMergeParsedBoard", b);

	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void insertParsedBoardCategory(List<Board> boardList) {

		for (Board board : boardList) {
			if (board.getBoard_category_list() != null && board.getBoard_category_list().size() > 0) {
				// 현재 board의 category 리스트에 없는것들은 삭제된 category로 간주하고 지움
				batchSqlSession.delete("admin.deleteBoardCategory", board);
				// 있는것들은 upsert를 함
				batchSqlSession.insert("admin.insertParsedBoardCategory", board);
			}
		}

	}

	@Override
	public int updateBoard(Board board) {

		return sqlSession.update("admin.updateBoard", board);
	}

	@Override
	public int deleteBoard(Board board) {

		return sqlSession.delete("admin.deleteBoard", board);
	}

	// @Cacheable(value = "siteList")
	@Override
	public List<Site> selectSite(TableSearchData tsd) {
		return sqlSession.selectList("admin.selectSite", tsd);
	}

	@Override
	public int insertSite(Site site) {

		return sqlSession.insert("admin.insertSite", site);
	}

	@Override
	public int insertSearchType(SearchType st) {
		return sqlSession.insert("admin.insertSearchType", st);
	}

	@Override
	public int insertFile(FileVO fileVo) {

		return sqlSession.insert("admin.insertFile", fileVo);
	}

	@Override
	public int deleteFile(FileVO fileVo) {
		return sqlSession.delete("admin.deleteFile", fileVo);
	}

	@Override
	public int deleteSite(Site site) {
		return sqlSession.delete("admin.deleteSite", site);
	}

	@Override
	public int updateSite(Site site) {

		return sqlSession.update("admin.updateSite", site);
	}

	@Override
	public List<ParseSiteSelector> selectParseSiteSelector(Site site) {

		return sqlSession.selectList("admin.selectSiteWithSelector", site);
	}

	@Override
	public List<ParseSiteSelector> selectParseSiteSelector() {
		return sqlSession.selectList("admin.selectSiteWithSelector");
	}

	@Override
	public int updateSitesAllBoardAvailable(Site site) {

		return sqlSession.update("admin.updateSitesAllBoardAvailable", site);
	}

	@Override
	public List<BoardArticle> selectDBBoardArticle(TableSearchData tsd, PageInfo pageInfo) {

		Map<String, Object> param = new HashMap<>();
		param.put("tsd", tsd);
		param.put("pageInfo", pageInfo);

		return sqlSession.selectList("admin.selectDBBoardArticle", param);
	}

	@Override
	public int selectDBBoardArticleCnt(TableSearchData tsd) {

		return sqlSession.selectOne("admin.selectDBBoardArticleCnt", tsd);
	}

	@Override
	public List<Board> selectDbBoardList() {

		return sqlSession.selectList("admin.selectDbBoardList");
	}

	@Override
	@CacheEvict(value = "dbArticlePage", allEntries = true)
	public int insertDbBoardArticle(BoardArticle ba, String board_code, String memberNo) {

		Map<String, Object> param = new HashMap<>();
		param.put("data", ba);
		param.put("memberNo", memberNo);
		param.put("boardCode", board_code);

		return sqlSession.insert("admin.insertDbBoardArticle", param);
	}

	@Override
	@CacheEvict(value = "dbArticlePage", allEntries = true)
	public int updateDbBoardArticle(BoardArticle ba) {

		return sqlSession.update("admin.updateDbBoardArticle", ba);
	}

	@Override
	@CacheEvict(value = "dbArticlePage", allEntries = true)
	public int deleteDbBoardArticle(BoardArticle ba) {

		return sqlSession.delete("admin.deleteDbBoardArticle", ba);
	}

	@Override
	public int updateTabContentValue() {

		return sqlSession.update("admin.updateTabContentValue");
	}

	@Override
	public List<Board> selectMaintenanceTargetBoard(int day) {

		return sqlSession.selectList("admin.selectMaintenanceTargetBoard", day);
	}

	@Override
	public List<Board> selectCurrentlyUnavailableBoard() {

		return sqlSession.selectList("admin.selectCurrentlyUnavailableBoard");
	}

}

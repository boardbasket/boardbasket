package com.nayuta.mosite.admin.model.vo;

import java.util.ArrayList;
import java.util.List;

public class FileVOList {

	private List<FileVO> dataList = new ArrayList<>();

	public List<FileVO> getDataList() {
		return dataList;
	}

	public void setDataList(List<FileVO> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "FileVOList [dataList=" + dataList + "]";
	}

	public FileVOList(List<FileVO> dataList) {
		super();
		this.dataList = dataList;
	}

	public FileVOList() {
		super();
	}

}

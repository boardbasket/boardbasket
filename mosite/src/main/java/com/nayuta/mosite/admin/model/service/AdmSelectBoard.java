package com.nayuta.mosite.admin.model.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admSelectBoard")
public class AdmSelectBoard extends AbstractAdminCls implements CommandInterface {

	@Override
	public void execute(ModelMap modelMap) {

		TableSearchData searchData = (TableSearchData) modelMap.get("searchData");

		resultObj.put("dataList", aDao.selectBoard(searchData));

		modelMap.addAttribute("result", resultObj);

	}

}

package com.nayuta.mosite.admin.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.FileVO;
import com.nayuta.mosite.common.util.FileUtil;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admDeleteFiles")
public class AdmDeleteFiles extends AbstractAdminCls implements CommandInterface {

	@Autowired
	FileUtil fileUtil;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		@SuppressWarnings("unchecked")
		List<FileVO> fileVoList = (List<FileVO>) modelMap.get("dataList");

		for (FileVO vo : fileVoList)
			aDao.deleteFile(vo);

		for (FileVO vo : fileVoList)
			fileUtil.deleteFile(vo);

		resultObj.put("deletedList", gson.toJson(fileVoList));
		modelMap.put("result", resultObj);

	}

}

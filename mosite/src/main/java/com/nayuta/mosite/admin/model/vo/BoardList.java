package com.nayuta.mosite.admin.model.vo;

import java.util.ArrayList;
import java.util.List;

//게시판을 넣거나 뺄때 사용하는 VO, ContentVO로 대체 가능
public class BoardList {

	List<Board> dataList = new ArrayList<Board>();

	public List<Board> getDataList() {
		return dataList;
	}

	public void setDataList(List<Board> dataList) {
		this.dataList = dataList;
	}

	public BoardList(List<Board> dataList) {
		super();
		this.dataList = dataList;
	}

	public BoardList() {
		super();
	}

	@Override
	public String toString() {
		return "BoardList [dataList=" + dataList + "]";
	}

}

package com.nayuta.mosite.admin.model.vo;

import java.util.HashMap;
import java.util.Map;

import com.nayuta.mosite.common.util.RefCodeConst;
import com.nayuta.mosite.content.model.service.pageCommand.GetPrimaryKeyCode;

//게시판 검색할때 vo
public class TableSearchData implements GetPrimaryKeyCode, RefCodeConst {

	private String board_code;
	private String site_code;
	private String site_name;
	private String board_name;
	private String board_type;
	private String available;
	private String keyword;
	private String orderby;
	private int offset;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}

	public String getSite_name() {
		return site_name;
	}

	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public TableSearchData(String board_code, String site_code, String site_name, String board_name, String board_type,
			String available, String keyword, String orderby) {
		super();
		this.board_code = board_code;
		this.site_code = site_code;
		this.site_name = site_name;
		this.board_name = board_name;
		this.board_type = board_type;
		this.available = available;
		this.keyword = keyword;
		this.orderby = orderby;
	}

	public String getBoard_code() {
		return board_code;
	}

	public void setBoard_code(String board_code) {
		this.board_code = board_code;
	}

	public String getSite_code() {
		return site_code;
	}

	public void setSite_code(String site_code) {
		this.site_code = site_code;
	}

	public String getBoard_type() {
		return board_type;
	}

	public void setBoard_type(String board_type) {
		this.board_type = board_type;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	public String getBoard_name() {
		return board_name;
	}

	public void setBoard_name(String board_name) {
		this.board_name = board_name;
	}

	public TableSearchData() {
		super();
	}

	@Override
	public String toString() {
		return "TableSearchData [board_code=" + board_code + ", site_code=" + site_code + ", site_name=" + site_name
				+ ", board_name=" + board_name + ", board_type=" + board_type + ", available=" + available
				+ ", keyword=" + keyword + ", orderby=" + orderby + ", offset=" + offset + "]";
	}

	@Override
	public Map<String, Object> pullPrimaryKey() {

		Map<String, Object> codeMap = new HashMap<String, Object>();

		if (board_code != null)
			codeMap.put(RefCodeConst.boardCodeRefMap, board_code);

		if (site_code != null)
			codeMap.put(RefCodeConst.siteCodeRefMap, site_code);

		return codeMap;
	}

	@Override
	public void pushPrimaryKey(Map<String, Object> primaryKey) {

		this.board_code = (String) primaryKey.get(RefCodeConst.boardCodeRefMap);
		this.site_code = (String) primaryKey.get(RefCodeConst.siteCodeRefMap);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((available == null) ? 0 : available.hashCode());
		result = prime * result + ((board_code == null) ? 0 : board_code.hashCode());
		result = prime * result + ((board_name == null) ? 0 : board_name.hashCode());
		result = prime * result + ((board_type == null) ? 0 : board_type.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + offset;
		result = prime * result + ((orderby == null) ? 0 : orderby.hashCode());
		result = prime * result + ((site_code == null) ? 0 : site_code.hashCode());
		result = prime * result + ((site_name == null) ? 0 : site_name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TableSearchData other = (TableSearchData) obj;
		if (available == null) {
			if (other.available != null)
				return false;
		} else if (!available.equals(other.available))
			return false;
		if (board_code == null) {
			if (other.board_code != null)
				return false;
		} else if (!board_code.equals(other.board_code))
			return false;
		if (board_name == null) {
			if (other.board_name != null)
				return false;
		} else if (!board_name.equals(other.board_name))
			return false;
		if (board_type == null) {
			if (other.board_type != null)
				return false;
		} else if (!board_type.equals(other.board_type))
			return false;
		if (keyword == null) {
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (offset != other.offset)
			return false;
		if (orderby == null) {
			if (other.orderby != null)
				return false;
		} else if (!orderby.equals(other.orderby))
			return false;
		if (site_code == null) {
			if (other.site_code != null)
				return false;
		} else if (!site_code.equals(other.site_code))
			return false;
		if (site_name == null) {
			if (other.site_name != null)
				return false;
		} else if (!site_name.equals(other.site_name))
			return false;
		return true;
	}

}

package com.nayuta.mosite.admin.model.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.content.model.service.CommandInterface;

@Service("admUpdateSite")
public class AdmUpdateSite extends AbstractAdminCls implements CommandInterface {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void execute(ModelMap modelMap) {

		@SuppressWarnings("unchecked")
		List<Site> dataList = (List<Site>) modelMap.get("dataList");

		for (Site site : dataList) {
			aDao.updateSite(site);
			if (site.getAvailable().equals("N"))
				aDao.updateSitesAllBoardAvailable(site);
		}

		resultObj.put("updatedList", gson.toJson(dataList));
		modelMap.put("result", resultObj);

	}

}

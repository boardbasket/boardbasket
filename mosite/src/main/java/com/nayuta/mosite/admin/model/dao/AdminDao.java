package com.nayuta.mosite.admin.model.dao;

import java.util.List;

import com.nayuta.mosite.admin.model.vo.Board;
import com.nayuta.mosite.admin.model.vo.FileVO;
import com.nayuta.mosite.admin.model.vo.ParseSiteSelector;
import com.nayuta.mosite.admin.model.vo.Site;
import com.nayuta.mosite.admin.model.vo.TableSearchData;
import com.nayuta.mosite.common.util.PageInfo;
import com.nayuta.mosite.content.model.vo.BoardArticle;
import com.nayuta.mosite.content.model.vo.SearchType;

public abstract class AdminDao {

	public abstract List<Board> selectBoard(TableSearchData tsd);

	public abstract int insertBoard(Board board);

	public abstract void insertOrMergeParsedBoard(List<Board> boardList);

	public abstract void insertParsedBoardCategory(List<Board> boardList);

	public abstract int updateBoard(Board board);

	public abstract int deleteBoard(Board board);

	public abstract List<Site> selectSite(TableSearchData tsd);

	public abstract int insertSite(Site site);

	public abstract int deleteSite(Site site);

	public abstract int updateSite(Site site);

	public abstract int updateSitesAllBoardAvailable(Site site);

	public abstract int insertSearchType(SearchType st);

	public abstract int insertFile(FileVO fileVo);

	public abstract int deleteFile(FileVO fileVo);

	public abstract List<ParseSiteSelector> selectParseSiteSelector(Site site);

	public abstract List<ParseSiteSelector> selectParseSiteSelector();

	public abstract List<BoardArticle> selectDBBoardArticle(TableSearchData tsd, PageInfo pageInfo);

	public abstract int selectDBBoardArticleCnt(TableSearchData tsd);

	public abstract List<Board> selectDbBoardList();

	public abstract int insertDbBoardArticle(BoardArticle ba, String board_code, String memberNo);

	public abstract int updateDbBoardArticle(BoardArticle ba);

	public abstract int deleteDbBoardArticle(BoardArticle ba);

	public abstract int updateTabContentValue();

	public abstract List<Board> selectMaintenanceTargetBoard(int day);

	public abstract List<Board> selectCurrentlyUnavailableBoard();

}
